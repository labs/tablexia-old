package org.andengine.opengl.font;

import java.util.ArrayList;

import org.andengine.opengl.font.exception.FontException;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.PixelFormat;
import org.andengine.opengl.util.GLState;
import org.andengine.util.adt.color.Color;
import org.andengine.util.adt.map.SparseArrayUtils;
import org.andengine.util.math.MathUtils;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.util.SparseArray;

/**
 * (c) 2010 Nicolas Gramlich
 * (c) 2011 Zynga Inc.
 *
 * @author Nicolas Gramlich
 * @since 10:39:33 - 03.04.2010
 */
public class Font implements IFont {
    // ===========================================================
    // Constants
    // ===========================================================

    protected static final int LETTER_TEXTURE_PADDING = 1;

    // ===========================================================
    // Fields
    // ===========================================================

    private final FontManager mFontManager;

    private final ITexture mTexture;
    private final int mTextureWidth;
    private final int mTextureHeight;
    private int mCurrentTextureX = Font.LETTER_TEXTURE_PADDING;
    private int mCurrentTextureY = Font.LETTER_TEXTURE_PADDING;
    private int mCurrentTextureYHeightMax;

    private final SparseArray<Letter> mManagedCharacterToLetterMap = new SparseArray<Letter>();
    private final ArrayList<Letter> mLettersPendingToBeDrawnToTexture = new ArrayList<Letter>();

    protected final Paint mPaint;
    private final Paint mBackgroundPaint;

    protected final FontMetrics mFontMetrics;

    protected final Canvas mCanvas = new Canvas();
    protected final Rect mTextBounds = new Rect();
    protected final float[] mTextWidthContainer = new float[1];

    // ===========================================================
    // Constructors
    // ===========================================================

    public Font(final FontManager pFontManager, final ITexture pTexture, final Typeface pTypeface, final float pSize, final boolean pAntiAlias, final Color pColor) {
        this(pFontManager, pTexture, pTypeface, pSize, pAntiAlias, pColor.getARGBPackedInt());
    }

    public Font(final FontManager pFontManager, final ITexture pTexture, final Typeface pTypeface, final float pSize, final boolean pAntiAlias, final int pColorARGBPackedInt) {
        mFontManager = pFontManager;
        mTexture = pTexture;
        mTextureWidth = pTexture.getWidth();
        mTextureHeight = pTexture.getHeight();

        mBackgroundPaint = new Paint();
        mBackgroundPaint.setColor(Color.TRANSPARENT_ARGB_PACKED_INT);
        mBackgroundPaint.setStyle(Style.FILL);

        mPaint = new Paint();
        mPaint.setTypeface(pTypeface);
        mPaint.setColor(pColorARGBPackedInt);
        mPaint.setTextSize(pSize);
        mPaint.setAntiAlias(pAntiAlias);

        mFontMetrics = mPaint.getFontMetrics();
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    /**
     * @return the gap between the lines.
     */
    public float getLeading() {
        return mFontMetrics.leading;
    }

    /**
     * @return the distance from the baseline to the top, which is usually negative.
     */
    @Override
    public float getAscent() {
        return mFontMetrics.ascent;
    }

    /**
     * @return the distance from the baseline to the bottom, which is usually positive.
     */
    public float getDescent() {
        return mFontMetrics.descent;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public ITexture getTexture() {
        return mTexture;
    }

    @Override
    public void load() {
        mTexture.load();
        mFontManager.loadFont(this);
    }

    @Override
    public void unload() {
        mTexture.unload();
        mFontManager.unloadFont(this);
    }

    @Override
    public float getLineHeight() {
        return -getAscent() + getDescent();
    }

    @Override
    public synchronized Letter getLetter(final char pCharacter) throws FontException {
        Letter letter = mManagedCharacterToLetterMap.get(pCharacter);
        if (letter == null) {
            letter = createLetter(pCharacter);

            mLettersPendingToBeDrawnToTexture.add(letter);
            mManagedCharacterToLetterMap.put(pCharacter, letter);
        }
        return letter;
    }

    // ===========================================================
    // Methods
    // ===========================================================

    public synchronized void invalidateLetters() {
        final ArrayList<Letter> lettersPendingToBeDrawnToTexture = mLettersPendingToBeDrawnToTexture;
        final SparseArray<Letter> managedCharacterToLetterMap = mManagedCharacterToLetterMap;

        /* Make all letters redraw to the texture. */
        for (int i = managedCharacterToLetterMap.size() - 1; i >= 0; i--) {
            lettersPendingToBeDrawnToTexture.add(managedCharacterToLetterMap.valueAt(i));
        }
    }

    private float getLetterAdvance(final String pCharacterAsString) {
        mPaint.getTextWidths(pCharacterAsString, mTextWidthContainer);
        return mTextWidthContainer[0];
    }

    protected Bitmap getLetterBitmap(final Letter pLetter) throws FontException {
        final char character = pLetter.mCharacter;
        final String characterAsString = String.valueOf(character);

        final int width = pLetter.mWidth + (2 * Font.LETTER_TEXTURE_PADDING);
        final int height = pLetter.mHeight + (2 * Font.LETTER_TEXTURE_PADDING);

        final Bitmap bitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        mCanvas.setBitmap(bitmap);

        /* Make background transparent. */
        mCanvas.drawRect(0, 0, bitmap.getWidth(), bitmap.getHeight(), mBackgroundPaint);

        /* Actually draw the character. */
        final float drawLetterLeft = -pLetter.mOffsetX;
        final float drawLetterTop = -pLetter.mOffsetY;

        drawLetter(characterAsString, drawLetterLeft, drawLetterTop);

        return bitmap;
    }

    protected void drawLetter(final String pCharacterAsString, final float pLeft, final float pTop) {
        mCanvas.drawText(pCharacterAsString, pLeft + Font.LETTER_TEXTURE_PADDING, pTop + Font.LETTER_TEXTURE_PADDING, mPaint);
    }

    public void prepareLetters(final char... pCharacters) throws FontException {
        for (final char character : pCharacters) {
            getLetter(character);
        }
    }

    private Letter createLetter(final char pCharacter) throws FontException {
        final String characterAsString = String.valueOf(pCharacter);

        final float textureWidth = mTextureWidth;
        final float textureHeight = mTextureHeight;

        updateTextBounds(characterAsString);
        final int letterLeft = mTextBounds.left;
        final int letterTop = mTextBounds.top;
        final int letterWidth = mTextBounds.width();
        final int letterHeight = mTextBounds.height();

        final Letter letter;

        final float advance = getLetterAdvance(characterAsString);

        final boolean whitespace = Character.isWhitespace(pCharacter) || (letterWidth == 0) || (letterHeight == 0);
        if (whitespace) {
            letter = new Letter(pCharacter, advance);
        } else {
            if ((mCurrentTextureX + Font.LETTER_TEXTURE_PADDING + letterWidth) >= textureWidth) {
                mCurrentTextureX = 0;
                mCurrentTextureY += mCurrentTextureYHeightMax + (2 * Font.LETTER_TEXTURE_PADDING);
                mCurrentTextureYHeightMax = 0;
            }

            if ((mCurrentTextureY + letterHeight) >= textureHeight) {
                throw new FontException("Not enough space for " + Letter.class.getSimpleName() + ": '" + pCharacter + "' on the " + mTexture.getClass().getSimpleName() + ". Existing Letters: " + SparseArrayUtils.toString(mManagedCharacterToLetterMap));
            }

            mCurrentTextureYHeightMax = Math.max(letterHeight, mCurrentTextureYHeightMax);

            mCurrentTextureX += Font.LETTER_TEXTURE_PADDING;

            final float u = mCurrentTextureX / textureWidth;
            final float v = mCurrentTextureY / textureHeight;
            final float u2 = (mCurrentTextureX + letterWidth) / textureWidth;
            final float v2 = (mCurrentTextureY + letterHeight) / textureHeight;

            letter = new Letter(pCharacter, mCurrentTextureX - Font.LETTER_TEXTURE_PADDING, mCurrentTextureY - Font.LETTER_TEXTURE_PADDING, letterWidth, letterHeight, letterLeft, letterTop, advance, u, v, u2, v2);
            mCurrentTextureX += letterWidth + Font.LETTER_TEXTURE_PADDING;
        }

        return letter;
    }

    protected void updateTextBounds(final String pCharacterAsString) {
        mPaint.getTextBounds(pCharacterAsString, 0, 1, mTextBounds);
    }

    public synchronized void update(final GLState pGLState) {
        if (mTexture.isLoadedToHardware()) {
            final ArrayList<Letter> lettersPendingToBeDrawnToTexture = mLettersPendingToBeDrawnToTexture;
            if (lettersPendingToBeDrawnToTexture.size() > 0) {
                mTexture.bind(pGLState);
                final PixelFormat pixelFormat = mTexture.getPixelFormat();

                final boolean preMultipyAlpha = mTexture.getTextureOptions().mPreMultiplyAlpha;
                for (int i = lettersPendingToBeDrawnToTexture.size() - 1; i >= 0; i--) {
                    final Letter letter = lettersPendingToBeDrawnToTexture.get(i);
                    if (!letter.isWhitespace()) {
                        final Bitmap bitmap = getLetterBitmap(letter);

                        final boolean useDefaultAlignment = MathUtils.isPowerOfTwo(bitmap.getWidth()) && MathUtils.isPowerOfTwo(bitmap.getHeight()) && (pixelFormat == PixelFormat.RGBA_8888);
                        if (!useDefaultAlignment) {
                            /* Adjust unpack alignment. */
                            GLES20.glPixelStorei(GLES20.GL_UNPACK_ALIGNMENT, 1);
                        }

                        if (preMultipyAlpha) {
                            GLUtils.texSubImage2D(GLES20.GL_TEXTURE_2D, 0, letter.mTextureX, letter.mTextureY, bitmap);
                        } else {
                            pGLState.glTexSubImage2D(GLES20.GL_TEXTURE_2D, 0, letter.mTextureX, letter.mTextureY, bitmap, pixelFormat);
                        }

                        if (!useDefaultAlignment) {
                            /* Restore default unpack alignment. */
                            GLES20.glPixelStorei(GLES20.GL_UNPACK_ALIGNMENT, GLState.GL_UNPACK_ALIGNMENT_DEFAULT);
                        }

                        bitmap.recycle();
                    }
                }
                lettersPendingToBeDrawnToTexture.clear();

                System.gc();
            }
        }
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}