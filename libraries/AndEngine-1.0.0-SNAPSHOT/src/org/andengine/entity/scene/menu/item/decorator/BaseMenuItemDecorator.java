package org.andengine.entity.scene.menu.item.decorator;

import java.util.ArrayList;
import java.util.List;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.IEntityComparator;
import org.andengine.entity.IEntityMatcher;
import org.andengine.entity.IEntityParameterCallable;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierMatcher;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.shader.ShaderProgram;
import org.andengine.opengl.util.GLState;
import org.andengine.opengl.vbo.IVertexBufferObject;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;
import org.andengine.util.adt.transformation.Transformation;

/**
 * I HATE THIS CLASS!
 *
 * (c) 2010 Nicolas Gramlich
 * (c) 2011 Zynga Inc.
 *
 * @author Nicolas Gramlich
 * @since 15:05:44 - 18.11.2010
 */
public abstract class BaseMenuItemDecorator implements IMenuItem {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    protected final IMenuItem mMenuItem;

    // ===========================================================
    // Constructors
    // ===========================================================

    public BaseMenuItemDecorator(final IMenuItem pMenuItem) {
        mMenuItem = pMenuItem;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    protected abstract void onMenuItemSelected(final IMenuItem pMenuItem);
    protected abstract void onMenuItemUnselected(final IMenuItem pMenuItem);
    protected abstract void onMenuItemReset(final IMenuItem pMenuItem);

    @Override
    public int getID() {
        return mMenuItem.getID();
    }

    @Override
    public VertexBufferObjectManager getVertexBufferObjectManager() {
        return mMenuItem.getVertexBufferObjectManager();
    }

    @Override
    public IVertexBufferObject getVertexBufferObject() {
        return mMenuItem.getVertexBufferObject();
    }

    @Override
    public final void onSelected() {
        mMenuItem.onSelected();
        onMenuItemSelected(mMenuItem);
    }

    @Override
    public final void onUnselected() {
        mMenuItem.onUnselected();
        onMenuItemUnselected(mMenuItem);
    }

    @Override
    public float getX() {
        return mMenuItem.getX();
    }

    @Override
    public float getY() {
        return mMenuItem.getY();
    }

    @Override
    public void setX(final float pX) {
        mMenuItem.setX(pX);
    }

    @Override
    public void setY(final float pY) {
        mMenuItem.setY(pY);
    }

    @Override
    public void setPosition(final IEntity pOtherEntity) {
        mMenuItem.setPosition(pOtherEntity);
    }

    @Override
    public void setPosition(final float pX, final float pY) {
        mMenuItem.setPosition(pX, pY);
    }

    @Override
    public float getWidth() {
        return mMenuItem.getWidth();
    }

    @Deprecated
    @Override
    public float getWidthScaled() {
        return mMenuItem.getWidthScaled();
    }

    @Override
    public float getHeight() {
        return mMenuItem.getHeight();
    }

    @Deprecated
    @Override
    public float getHeightScaled() {
        return mMenuItem.getHeightScaled();
    }

    @Override
    public void setWidth(final float pWidth) {
        mMenuItem.setWidth(pWidth);
    }

    @Override
    public void setHeight(final float pHeight) {
        mMenuItem.setHeight(pHeight);
    }

    @Override
    public void setSize(final float pWidth, final float pHeight) {
        mMenuItem.setSize(pWidth, pHeight);
    }

    @Override
    public float getRed() {
        return mMenuItem.getRed();
    }

    @Override
    public float getGreen() {
        return mMenuItem.getGreen();
    }

    @Override
    public float getBlue() {
        return mMenuItem.getBlue();
    }

    @Override
    public float getAlpha() {
        return mMenuItem.getAlpha();
    }

    @Override
    public void setRed(final float pRed) {
        mMenuItem.setRed(pRed);
    }

    @Override
    public void setGreen(final float pGreen) {
        mMenuItem.setGreen(pGreen);
    }

    @Override
    public void setBlue(final float pBlue) {
        mMenuItem.setBlue(pBlue);
    }

    @Override
    public void setAlpha(final float pAlpha) {
        mMenuItem.setAlpha(pAlpha);
    }

    @Override
    public Color getColor() {
        return mMenuItem.getColor();
    }

    @Override
    public void setColor(final Color pColor) {
        mMenuItem.setColor(pColor);
    }

    @Override
    public void setColor(final int pARGBPackedInt) {
        mMenuItem.setColor(pARGBPackedInt);
    }

    @Override
    public void setColor(final float pRed, final float pGreen, final float pBlue) {
        mMenuItem.setColor(pRed, pGreen, pBlue);
    }

    @Override
    public void setColor(final float pRed, final float pGreen, final float pBlue, final float pAlpha) {
        mMenuItem.setColor(pRed, pGreen, pBlue, pAlpha);
    }

    @Override
    public float getOffsetCenterX() {
        return mMenuItem.getOffsetCenterX();
    }

    @Override
    public float getOffsetCenterY() {
        return mMenuItem.getOffsetCenterY();
    }

    @Override
    public void setOffsetCenterX(final float pOffsetCenterX) {
        mMenuItem.setOffsetCenterX(pOffsetCenterX);
    }

    @Override
    public void setOffsetCenterY(final float pOffsetCenterY) {
        mMenuItem.setOffsetCenterY(pOffsetCenterY);
    }

    @Override
    public void setOffsetCenter(final float pOffsetCenterX, final float pOffsetCenterY) {
        mMenuItem.setOffsetCenter(pOffsetCenterX, pOffsetCenterY);
    }

    @Override
    public boolean isRotated() {
        return mMenuItem.isRotated();
    }

    @Override
    public float getRotation() {
        return mMenuItem.getRotation();
    }

    @Override
    public void setRotation(final float pRotation) {
        mMenuItem.setRotation(pRotation);
    }

    @Override
    public float getRotationCenterX() {
        return mMenuItem.getRotationCenterX();
    }

    @Override
    public float getRotationCenterY() {
        return mMenuItem.getRotationCenterY();
    }

    @Override
    public void setRotationCenterX(final float pRotationCenterX) {
        mMenuItem.setRotationCenterX(pRotationCenterX);
    }

    @Override
    public void setRotationCenterY(final float pRotationCenterY) {
        mMenuItem.setRotationCenterY(pRotationCenterY);
    }

    @Override
    public void setRotationCenter(final float pRotationCenterX, final float pRotationCenterY) {
        mMenuItem.setRotationCenter(pRotationCenterX, pRotationCenterY);
    }

    @Override
    public boolean isScaled() {
        return mMenuItem.isScaled();
    }

    @Override
    public float getScaleX() {
        return mMenuItem.getScaleX();
    }

    @Override
    public float getScaleY() {
        return mMenuItem.getScaleY();
    }

    @Override
    public void setScale(final float pScale) {
        mMenuItem.setScale(pScale);
    }

    @Override
    public void setScale(final float pScaleX, final float pScaleY) {
        mMenuItem.setScale(pScaleX, pScaleY);
    }

    @Override
    public void setScaleX(final float pScaleX) {
        mMenuItem.setScaleX(pScaleX);
    }

    @Override
    public void setScaleY(final float pScaleY) {
        mMenuItem.setScaleY(pScaleY);
    }

    @Override
    public float getScaleCenterX() {
        return mMenuItem.getScaleCenterX();
    }

    @Override
    public float getScaleCenterY() {
        return mMenuItem.getScaleCenterY();
    }

    @Override
    public void setScaleCenterX(final float pScaleCenterX) {
        mMenuItem.setScaleCenterX(pScaleCenterX);
    }

    @Override
    public void setScaleCenterY(final float pScaleCenterY) {
        mMenuItem.setScaleCenterY(pScaleCenterY);
    }

    @Override
    public void setScaleCenter(final float pScaleCenterX, final float pScaleCenterY) {
        mMenuItem.setScaleCenter(pScaleCenterX, pScaleCenterY);
    }

    @Override
    public boolean isSkewed() {
        return mMenuItem.isSkewed();
    }

    @Override
    public float getSkewX() {
        return mMenuItem.getSkewX();
    }

    @Override
    public float getSkewY() {
        return mMenuItem.getSkewY();
    }

    @Override
    public void setSkew(final float pSkew) {
        mMenuItem.setSkew(pSkew);
    }

    @Override
    public void setSkew(final float pSkewX, final float pSkewY) {
        mMenuItem.setSkew(pSkewX, pSkewY);
    }

    @Override
    public void setSkewX(final float pSkewX) {
        mMenuItem.setSkewX(pSkewX);
    }

    @Override
    public void setSkewY(final float pSkewY) {
        mMenuItem.setSkewY(pSkewY);
    }

    @Override
    public float getSkewCenterX() {
        return mMenuItem.getSkewCenterX();
    }

    @Override
    public float getSkewCenterY() {
        return mMenuItem.getSkewCenterY();
    }

    @Override
    public void setSkewCenterX(final float pSkewCenterX) {
        mMenuItem.setSkewCenterX(pSkewCenterX);
    }

    @Override
    public void setSkewCenterY(final float pSkewCenterY) {
        mMenuItem.setSkewCenterY(pSkewCenterY);
    }

    @Override
    public void setSkewCenter(final float pSkewCenterX, final float pSkewCenterY) {
        mMenuItem.setSkewCenter(pSkewCenterX, pSkewCenterY);
    }

    @Override
    public boolean isRotatedOrScaledOrSkewed() {
        return mMenuItem.isRotatedOrScaledOrSkewed();
    }

    @Override
    public void setAnchorCenterX(final float pAnchorCenterX) {
        setAnchorCenterX(pAnchorCenterX);
    }

    @Override
    public void setAnchorCenterY(final float pAnchorCenterY) {
        setAnchorCenterY(pAnchorCenterY);
    }

    @Override
    public void setAnchorCenter(final float pAnchorCenterX, final float pAnchorCenterY) {
        mMenuItem.setAnchorCenter(pAnchorCenterX, pAnchorCenterY);
    }

    @Override
    public boolean collidesWith(final IEntity pOtherEntity) {
        return mMenuItem.collidesWith(pOtherEntity);
    }

    @Override
    public float[] getSceneCenterCoordinates() {
        return mMenuItem.getSceneCenterCoordinates();
    }

    @Override
    public float[] getSceneCenterCoordinates(final float[] pReuse) {
        return mMenuItem.getSceneCenterCoordinates(pReuse);
    }

    @Override
    public boolean isCullingEnabled() {
        return mMenuItem.isCullingEnabled();
    }

    @Override
    public void registerEntityModifier(final IEntityModifier pEntityModifier) {
        mMenuItem.registerEntityModifier(pEntityModifier);
    }

    @Override
    public boolean unregisterEntityModifier(final IEntityModifier pEntityModifier) {
        return mMenuItem.unregisterEntityModifier(pEntityModifier);
    }

    @Override
    public boolean unregisterEntityModifiers(final IEntityModifierMatcher pEntityModifierMatcher) {
        return mMenuItem.unregisterEntityModifiers(pEntityModifierMatcher);
    }

    @Override
    public int getEntityModifierCount() {
        return mMenuItem.getEntityModifierCount();
    }

    @Override
    public void clearEntityModifiers() {
        mMenuItem.clearEntityModifiers();
    }

    @Override
    public void resetEntityModifiers() {
        mMenuItem.resetEntityModifiers();
    }

    @Override
    public boolean isBlendingEnabled() {
        return mMenuItem.isBlendingEnabled();
    }

    @Override
    public void setBlendingEnabled(final boolean pBlendingEnabled) {
        mMenuItem.setBlendingEnabled(pBlendingEnabled);
    }

    @Override
    public int getBlendFunctionSource() {
        return mMenuItem.getBlendFunctionSource();
    }

    @Override
    public void setBlendFunctionSource(final int pBlendFunctionSource) {
        mMenuItem.setBlendFunctionSource(pBlendFunctionSource);
    }

    @Override
    public int getBlendFunctionDestination() {
        return mMenuItem.getBlendFunctionDestination();
    }

    @Override
    public void setBlendFunctionDestination(final int pBlendFunctionDestination) {
        mMenuItem.setBlendFunctionDestination(pBlendFunctionDestination);
    }

    @Override
    public void setBlendFunction(final int pBlendFunctionSource, final int pBlendFunctionDestination) {
        mMenuItem.setBlendFunction(pBlendFunctionSource, pBlendFunctionDestination);
    }

    @Override
    public void setCullingEnabled(final boolean pCullingEnabled) {
        mMenuItem.setCullingEnabled(pCullingEnabled);
    }

    @Override
    public int getTag() {
        return mMenuItem.getTag();
    }

    @Override
    public void setTag(final int pTag) {
        mMenuItem.setTag(pTag);
    }

    @Override
    public int getZIndex() {
        return mMenuItem.getZIndex();
    }

    @Override
    public void setZIndex(final int pZIndex) {
        mMenuItem.setZIndex(pZIndex);
    }

    @Override
    public ShaderProgram getShaderProgram() {
        return mMenuItem.getShaderProgram();
    }

    @Override
    public void setShaderProgram(final ShaderProgram pShaderProgram) {
        mMenuItem.setShaderProgram(pShaderProgram);
    }

    @Override
    public void onDraw(final GLState pGLState, final Camera pCamera) {
        mMenuItem.onDraw(pGLState, pCamera);
    }

    @Override
    public void onUpdate(final float pSecondsElapsed) {
        mMenuItem.onUpdate(pSecondsElapsed);
    }

    @Override
    public void reset() {
        mMenuItem.reset();
        onMenuItemReset(mMenuItem);
    }

    @Override
    public boolean isDisposed() {
        return mMenuItem.isDisposed();
    }

    @Override
    public void dispose() {
        mMenuItem.dispose();
    }

    @Override
    public boolean contains(final float pX, final float pY) {
        return mMenuItem.contains(pX, pY);
    }

    @Override
    public float[] convertLocalCoordinatesToParentCoordinates(final float pX, final float pY) {
        return mMenuItem.convertLocalCoordinatesToParentCoordinates(pX, pY);
    }

    @Override
    public float[] convertLocalCoordinatesToParentCoordinates(final float pX, final float pY, final float[] pReuse) {
        return mMenuItem.convertLocalCoordinatesToParentCoordinates(pX, pY, pReuse);
    }

    @Override
    public float[] convertLocalCoordinatesToParentCoordinates(final float[] pCoordinates) {
        return mMenuItem.convertLocalCoordinatesToParentCoordinates(pCoordinates);
    }

    @Override
    public float[] convertLocalCoordinatesToParentCoordinates(final float[] pCoordinates, final float[] pReuse) {
        return mMenuItem.convertLocalCoordinatesToParentCoordinates(pCoordinates, pReuse);
    }

    @Override
    public float[] convertParentCoordinatesToLocalCoordinates(final float pX, final float pY) {
        return mMenuItem.convertParentCoordinatesToLocalCoordinates(pX, pY);
    }

    @Override
    public float[] convertParentCoordinatesToLocalCoordinates(final float pX, final float pY, final float[] pReuse) {
        return mMenuItem.convertParentCoordinatesToLocalCoordinates(pX, pY, pReuse);
    }

    @Override
    public float[] convertParentCoordinatesToLocalCoordinates(final float[] pCoordinates) {
        return mMenuItem.convertParentCoordinatesToLocalCoordinates(pCoordinates);
    }

    @Override
    public float[] convertParentCoordinatesToLocalCoordinates(final float[] pCoordinates, final float[] pReuse) {
        return mMenuItem.convertParentCoordinatesToLocalCoordinates(pCoordinates, pReuse);
    }

    @Override
    public float[] convertLocalCoordinatesToSceneCoordinates(final float pX, final float pY) {
        return mMenuItem.convertLocalCoordinatesToSceneCoordinates(pX, pY);
    }

    @Override
    public float[] convertLocalCoordinatesToSceneCoordinates(final float pX, final float pY, final float[] pReuse) {
        return mMenuItem.convertLocalCoordinatesToSceneCoordinates(pX, pY, pReuse);
    }

    @Override
    public float[] convertLocalCoordinatesToSceneCoordinates(final float[] pCoordinates) {
        return mMenuItem.convertLocalCoordinatesToSceneCoordinates(pCoordinates);
    }

    @Override
    public float[] convertLocalCoordinatesToSceneCoordinates(final float[] pCoordinates, final float[] pReuse) {
        return mMenuItem.convertLocalCoordinatesToSceneCoordinates(pCoordinates, pReuse);
    }

    @Override
    public float[] convertSceneCoordinatesToLocalCoordinates(final float pX, final float pY) {
        return mMenuItem.convertSceneCoordinatesToLocalCoordinates(pX, pY);
    }

    @Override
    public float[] convertSceneCoordinatesToLocalCoordinates(final float pX, final float pY, final float[] pReuse) {
        return mMenuItem.convertSceneCoordinatesToLocalCoordinates(pX, pY, pReuse);
    }

    @Override
    public float[] convertSceneCoordinatesToLocalCoordinates(final float[] pCoordinates) {
        return mMenuItem.convertSceneCoordinatesToLocalCoordinates(pCoordinates);
    }

    @Override
    public float[] convertSceneCoordinatesToLocalCoordinates(final float[] pCoordinates, final float[] pReuse) {
        return mMenuItem.convertSceneCoordinatesToLocalCoordinates(pCoordinates, pReuse);
    }

    @Override
    public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
        return mMenuItem.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
    }

    @Override
    public int getChildCount() {
        return mMenuItem.getChildCount();
    }

    @Override
    public void attachChild(final IEntity pEntity) {
        mMenuItem.attachChild(pEntity);
    }

    @Override
    public IEntity getFirstChild() {
        return mMenuItem.getFirstChild();
    }

    @Override
    public IEntity getLastChild() {
        return mMenuItem.getLastChild();
    }

    @Override
    public IEntity getChildByTag(final int pTag) {
        return mMenuItem.getChildByTag(pTag);
    }

    @Override
    public IEntity getChildByIndex(final int pIndex) {
        return mMenuItem.getChildByIndex(pIndex);
    }

    @Override
    public IEntity getChildByMatcher(final IEntityMatcher pEntityMatcher) {
        return mMenuItem.getChildByMatcher(pEntityMatcher);
    }

    @Override
    public ArrayList<IEntity> query(final IEntityMatcher pEntityMatcher) {
        return mMenuItem.query(pEntityMatcher);
    }

    @Override
    public IEntity queryFirst(final IEntityMatcher pEntityMatcher) {
        return mMenuItem.queryFirst(pEntityMatcher);
    }

    @Override
    public <L extends List<IEntity>> L query(final IEntityMatcher pEntityMatcher, final L pResult) {
        return mMenuItem.query(pEntityMatcher, pResult);
    }

    @Override
    public <S extends IEntity> S queryFirstForSubclass(final IEntityMatcher pEntityMatcher) {
        return mMenuItem.queryFirstForSubclass(pEntityMatcher);
    }

    @Override
    public <S extends IEntity> ArrayList<S> queryForSubclass(final IEntityMatcher pEntityMatcher) throws ClassCastException {
        return mMenuItem.queryForSubclass(pEntityMatcher);
    }

    @Override
    public <L extends List<S>, S extends IEntity> L queryForSubclass(final IEntityMatcher pEntityMatcher, final L pResult) throws ClassCastException {
        return mMenuItem.queryForSubclass(pEntityMatcher, pResult);
    }

    @Override
    public void sortChildren() {
        mMenuItem.sortChildren();
    }

    @Override
    public void sortChildren(final boolean pImmediate) {
        mMenuItem.sortChildren(pImmediate);
    }

    @Override
    public void sortChildren(final IEntityComparator pEntityComparator) {
        mMenuItem.sortChildren(pEntityComparator);
    }

    @Override
    public boolean detachSelf() {
        return mMenuItem.detachSelf();
    }

    @Override
    public boolean detachChild(final IEntity pEntity) {
        return mMenuItem.detachChild(pEntity);
    }

    @Override
    public IEntity detachChild(final int pTag) {
        return mMenuItem.detachChild(pTag);
    }

    @Override
    public IEntity detachChild(final IEntityMatcher pEntityMatcher) {
        return mMenuItem.detachChild(pEntityMatcher);
    }

    @Override
    public boolean detachChildren(final IEntityMatcher pEntityMatcher) {
        return mMenuItem.detachChildren(pEntityMatcher);
    }

    @Override
    public void detachChildren() {
        mMenuItem.detachChildren();
    }

    @Override
    public void callOnChildren(final IEntityParameterCallable pEntityParameterCallable) {
        mMenuItem.callOnChildren(pEntityParameterCallable);
    }

    @Override
    public void callOnChildren(final IEntityParameterCallable pEntityParameterCallable, final IEntityMatcher pEntityMatcher) {
        mMenuItem.callOnChildren(pEntityParameterCallable, pEntityMatcher);
    }

    @Override
    public Transformation getLocalToSceneTransformation() {
        return mMenuItem.getLocalToSceneTransformation();
    }

    @Override
    public Transformation getSceneToLocalTransformation() {
        return mMenuItem.getSceneToLocalTransformation();
    }

    @Override
    public Transformation getLocalToParentTransformation() {
        return mMenuItem.getLocalToParentTransformation();
    }

    @Override
    public Transformation getParentToLocalTransformation() {
        return mMenuItem.getParentToLocalTransformation();
    }

    @Override
    public boolean hasParent() {
        return mMenuItem.hasParent();
    }

    @Override
    public IEntity getParent() {
        return mMenuItem.getParent();
    }

    @Override
    public void setParent(final IEntity pEntity) {
        mMenuItem.setParent(pEntity);
    }

    @Override
    public IEntity getRootEntity() {
        return mMenuItem.getRootEntity();
    }

    @Override
    public boolean isVisible() {
        return mMenuItem.isVisible();
    }

    @Override
    public void setVisible(final boolean pVisible) {
        mMenuItem.setVisible(pVisible);
    }

    @Override
    public boolean isCulled(final Camera pCamera) {
        return mMenuItem.isCulled(pCamera);
    }

    @Override
    public boolean isChildrenVisible() {
        return mMenuItem.isChildrenVisible();
    }

    @Override
    public void setChildrenVisible(final boolean pChildrenVisible) {
        mMenuItem.setChildrenVisible(pChildrenVisible);
    }

    @Override
    public boolean isIgnoreUpdate() {
        return mMenuItem.isIgnoreUpdate();
    }

    @Override
    public void setIgnoreUpdate(final boolean pIgnoreUpdate) {
        mMenuItem.setIgnoreUpdate(pIgnoreUpdate);
    }

    @Override
    public boolean isChildrenIgnoreUpdate() {
        return mMenuItem.isChildrenIgnoreUpdate();
    }

    @Override
    public void setChildrenIgnoreUpdate(final boolean pChildrenIgnoreUpdate) {
        mMenuItem.setChildrenIgnoreUpdate(pChildrenIgnoreUpdate);
    }

    @Override
    public void setUserData(final Object pUserData) {
        mMenuItem.setUserData(pUserData);
    }

    @Override
    public Object getUserData() {
        return mMenuItem.getUserData();
    }

    @Override
    public void onAttached() {
        mMenuItem.onAttached();
    }

    @Override
    public void onDetached() {
        mMenuItem.onDetached();
    }

    @Override
    public void registerUpdateHandler(final IUpdateHandler pUpdateHandler) {
        mMenuItem.registerUpdateHandler(pUpdateHandler);
    }

    @Override
    public boolean unregisterUpdateHandler(final IUpdateHandler pUpdateHandler) {
        return mMenuItem.unregisterUpdateHandler(pUpdateHandler);
    }

    @Override
    public int getUpdateHandlerCount() {
        return mMenuItem.getUpdateHandlerCount();
    }

    @Override
    public void clearUpdateHandlers() {
        mMenuItem.clearUpdateHandlers();
    }

    @Override
    public boolean unregisterUpdateHandlers(final IUpdateHandlerMatcher pUpdateHandlerMatcher) {
        return mMenuItem.unregisterUpdateHandlers(pUpdateHandlerMatcher);
    }

    @Override
    public void toString(final StringBuilder pStringBuilder) {
        mMenuItem.toString(pStringBuilder);
    }

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
