/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.test.games.potme;

import org.junit.Assert;
import org.junit.Test;

import cz.nic.tablexia.game.games.potme.action.ActionUtility;
import cz.nic.tablexia.game.games.potme.creature.Player.PlayerOrientation;
import cz.nic.tablexia.game.games.potme.map.TileMap;
import cz.nic.tablexia.game.games.potme.map.TileMap.TileMapPosition;
import cz.nic.tablexia.game.games.potme.map.mapobstacle.MapObstacleType;
import cz.nic.tablexia.game.games.potme.map.mapobstacle.MapObstacleType.MapObstaclePosition;
import cz.nic.tablexia.game.games.potme.map.tile.Tile;
import cz.nic.tablexia.game.games.potme.map.tile.TileType;

/**
 * Test of MoveUtility class methods
 * 
 * @author Matyáš Latner
 *
 */
public class ActionUtilityTest extends ActionUtility {

	@Test
	public void testCheckMoveDirection() {
		TileMap tileMap;
		
		//--------------------- VERTICAL 1
		//	    0    1    2
		//	  ----------------
		//	0 |    |    |    | 
		//	  |____|__|_|____|
		//	1 |    |  | |    |
		//	  |____|____|____|
		//	2 |    |    |    |
		//	  |____|____|____|
		
		tileMap = new TileMap(3, 3);
		tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_1C));
		tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_1A));
		tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));
		
		Assert.assertTrue(checkMoveDirection(tileMap, 1, 1, 1, 0));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 2, 1));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 1, 2));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 0, 1));
		
		
		//--------------------- HORIZONTAL 1
		//	    0    1    2
		//	  ----------------
		//	0 |    |    |    | 
		//	  |____|____|____|
		//	1 |    |   _|_   |
		//	  |____|____|____|
		//	2 |    |    |    |
		//	  |____|____|____|
		
		tileMap = new TileMap(3, 3);
		tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_1B));
		tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_1D));
		tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));
		
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 1, 0));
		Assert.assertTrue(checkMoveDirection(tileMap, 1, 1, 2, 1));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 1, 2));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 0, 1));
		
		
		//--------------------- VERTICAL 2
		//	    0    1    2
		//	  ----------------
		//	0 |    |    |    | 
		//	  |____|____|____|
		//	1 |    |    |    |
		//	  |____|__|_|____|
		//	2 |    |  | |    |
		//	  |____|____|____|
		
		tileMap = new TileMap(3, 3);
		tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_1C));
		tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_1A));
		tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));
		
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 1, 0));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 2, 1));
		Assert.assertTrue(checkMoveDirection(tileMap, 1, 1, 1, 2));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 0, 1));
		
		
		//--------------------- HORIZONTAL 2
		//	    0    1    2
		//	  ----------------
		//	0 |    |    |    | 
		//	  |____|____|____|
		//	1 |   _|_   |    |
		//	  |____|____|____|
		//	2 |    |    |    |
		//	  |____|____|____|
		
		tileMap = new TileMap(3, 3);
		tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_1D));
		tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_1B));
		
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 1, 0));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 2, 1));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 1, 2));
		Assert.assertTrue(checkMoveDirection(tileMap, 1, 1, 0, 1));
		
		
		//--------------------- CLOSED 1
		//	    0    1    2
		//	  ----------------
		//	0 |    |    |    | 
		//	  |____|____|____|
		//	1 |    | _|_|    |
		//	  |____|__|_|____|
		//	2 |    |    |    |
		//	  |____|____|____|
		
		tileMap = new TileMap(3, 3);
		tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_4));
		tileMap.addTileAtPosition(0, 0, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(2, 0, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(2, 2, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(0, 2, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));
		
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 0, 0));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 1, 0));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 2, 0));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 2, 1));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 2, 2));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 1, 2));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 0, 2));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 0, 1));
		
		
		//--------------------- CLOSED 2
		//	    0    1    2
		//	  ----------------
		//	0 |    |    |    | 
		//	  |____|____|____|
		//	1 |    | _|_|    |
		//	  |____|__|_|____|
		//	2 |    |    |    |
		//	  |____|____|____|
		
		tileMap = new TileMap(3, 3);
		tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_4));
		
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 0, 0));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 1, 0));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 2, 0));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 2, 1));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 2, 2));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 1, 2));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 0, 2));
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 1, 0, 1));
		
		
		//--------------------- OTHER 1
		//	    0    1    2
		//	  ----------------
		//	0 |    |    |    | 
		//	  |____|____|____|
		//	1 |   _|    |_   |
		//	  |____|____|____|
		//	2 |    |    |    |
		//	  |____|____|____|
		
		tileMap = new TileMap(3, 3);
		tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_1D));
		tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_1B));
		
		Assert.assertFalse(checkMoveDirection(tileMap, 0, 1, 2, 1));
		
		
		//--------------------- OTHER 2
		//	    0    1    2
		//	  ----------------
		//	0 |    |    |    | 
		//	  |____|__|_|____|
		//	1 |    |    |    |
		//	  |____|____|____|
		//	2 |    |  | |    |
		//	  |____|____|____|
		
		tileMap = new TileMap(3, 3);
		tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_1C));
		tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_1A));
		tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));
		
		Assert.assertFalse(checkMoveDirection(tileMap, 1, 0, 1, 2));
	}
	
	@Test
	public void testGetObstaclesInDirection() {
		TileMap tileMap;
		
		//--------------------- ALL OBSTACLES
		//	    0    1    2
		//	  ----------------	DR - DOOR
		//	0 |    | DR |    |  DG - DOG
		//	  |____|__|_|____|
		//	1 | DR |__|_|_ DG|
		//	  |____|__|_|____|
		//	2 |    |  DG|    |
		//	  |____|____|____|
		
		tileMap = new TileMap(3, 3);
		tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_4));
		tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_1C));
		tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_1D));
		tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));
		
		tileMap.getTileAtPosition(1, 1).setMapObstacle(MapObstaclePosition.BOTTOM_POSITION, MapObstacleType.DOG_H);
		tileMap.getTileAtPosition(1, 1).setMapObstacle(MapObstaclePosition.LEFT_POSITION, MapObstacleType.DOOR_V);
		tileMap.getTileAtPosition(1, 0).setMapObstacle(MapObstaclePosition.BOTTOM_POSITION, MapObstacleType.DOOR_H);
		tileMap.getTileAtPosition(2, 1).setMapObstacle(MapObstaclePosition.LEFT_POSITION, MapObstacleType.DOG_V);
		
		Assert.assertEquals(MapObstacleType.DOOR_H, getObstacleInDirection(tileMap, new TileMapPosition(1, 1), PlayerOrientation.TOP).getMapObstacleType());
		Assert.assertEquals(MapObstacleType.DOG_V, getObstacleInDirection(tileMap, new TileMapPosition(1, 1), PlayerOrientation.RIGHT).getMapObstacleType());
		Assert.assertEquals(MapObstacleType.DOG_H, getObstacleInDirection(tileMap, new TileMapPosition(1, 1), PlayerOrientation.BOTTOM).getMapObstacleType());
		Assert.assertEquals(MapObstacleType.DOOR_V, getObstacleInDirection(tileMap, new TileMapPosition(1, 1), PlayerOrientation.LEFT).getMapObstacleType());
		
		
		//--------------------- NO OBSTACLES 1
		//	    0    1    2
		//	  ----------------	DR - DOOR
		//	0 |    |    |    |  DG - DOG
		//	  |____|__|_|____|
		//	1 |    |__|_|_   |
		//	  |____|__|_|____|
		//	2 |    |    |    |
		//	  |____|____|____|
		
		tileMap = new TileMap(3, 3);
		tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_4));
		tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_1C));
		tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_1D));
		tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));
		
		Assert.assertNull(getObstacleInDirection(tileMap, new TileMapPosition(1, 1), PlayerOrientation.TOP));
		Assert.assertNull(getObstacleInDirection(tileMap, new TileMapPosition(1, 1), PlayerOrientation.RIGHT));
		Assert.assertNull(getObstacleInDirection(tileMap, new TileMapPosition(1, 1), PlayerOrientation.BOTTOM));
		Assert.assertNull(getObstacleInDirection(tileMap, new TileMapPosition(1, 1), PlayerOrientation.LEFT));
		
		
		//--------------------- NO OBSTACLES 2
		//	    0    1    2
		//	  ----------------	DR - DOOR
		//	0 |    |    |    |  DG - DOG
		//	  |____|____|____|
		//	1 |    |__|_|    |
		//	  |____|__|_|____|
		//	2 |    |    |    |
		//	  |____|____|____|
		
		tileMap = new TileMap(3, 3);
		tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_4));
		tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
		tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));
		
		Assert.assertNull(getObstacleInDirection(tileMap, new TileMapPosition(1, 1), PlayerOrientation.TOP));
		Assert.assertNull(getObstacleInDirection(tileMap, new TileMapPosition(1, 1), PlayerOrientation.RIGHT));
		Assert.assertNull(getObstacleInDirection(tileMap, new TileMapPosition(1, 1), PlayerOrientation.BOTTOM));
		Assert.assertNull(getObstacleInDirection(tileMap, new TileMapPosition(1, 1), PlayerOrientation.LEFT));
		
		
		//--------------------- NO OBSTACLES NO TILES
		//	    0    1    2
		//	  ----------------	DR - DOOR
		//	0 |    |    |    |  DG - DOG
		//	  |____|____|____|
		//	1 |    |__|_|    |
		//	  |____|__|_|____|
		//	2 |    |    |    |
		//	  |____|____|____|
		
		tileMap = new TileMap(3, 3);
		tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_4));
		
		Assert.assertNull(getObstacleInDirection(tileMap, new TileMapPosition(1, 1), PlayerOrientation.TOP));
		Assert.assertNull(getObstacleInDirection(tileMap, new TileMapPosition(1, 1), PlayerOrientation.RIGHT));
		Assert.assertNull(getObstacleInDirection(tileMap, new TileMapPosition(1, 1), PlayerOrientation.BOTTOM));
		Assert.assertNull(getObstacleInDirection(tileMap, new TileMapPosition(1, 1), PlayerOrientation.LEFT));
		
		
		//--------------------- NO OBSTACLES IN CORNER 1
		//	    0    1    2
		//	  ----------------	DR - DOOR
		//	0 |__|_|    |    |  DG - DOG
		//	  |__|_|____|____|
		//	1 |    |    |    |
		//	  |____|____|____|
		//	2 |    |    |    |
		//	  |____|____|____|
		
		tileMap = new TileMap(3, 3);
		tileMap.addTileAtPosition(0, 0, new Tile(TileType.TILE_4));
		
		Assert.assertNull(getObstacleInDirection(tileMap, new TileMapPosition(0, 0), PlayerOrientation.TOP));
		Assert.assertNull(getObstacleInDirection(tileMap, new TileMapPosition(0, 0), PlayerOrientation.LEFT));
		
		
		//--------------------- NO OBSTACLES IN CORNER 2
		//	    0    1    2
		//	  ----------------	DR - DOOR
		//	0 |    |    |    |  DG - DOG
		//	  |____|____|____|
		//	1 |    |    |    |
		//	  |____|____|____|
		//	2 |    |    |__|_|
		//	  |____|____|__|_|
		
		tileMap = new TileMap(3, 3);
		tileMap.addTileAtPosition(2, 2, new Tile(TileType.TILE_4));
		
		Assert.assertNull(getObstacleInDirection(tileMap, new TileMapPosition(2, 2), PlayerOrientation.RIGHT));
		Assert.assertNull(getObstacleInDirection(tileMap, new TileMapPosition(2, 2), PlayerOrientation.BOTTOM));
		
	}
}
