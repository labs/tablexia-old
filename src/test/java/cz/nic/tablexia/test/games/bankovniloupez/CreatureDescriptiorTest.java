/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.test.games.bankovniloupez;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureRoot.AttributeGender;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute.AttributeColor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.ClothingAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.accessories.MTieAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.bottom.BottomAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.bottom.FSkirtAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.bottom.MPantsAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.headgear.FHatAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.headgear.HeadgearAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.headgear.MHatAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.top.FShirtAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.top.MShirtAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.top.MSweaterAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.top.TopAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.glasses.GlassesAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.glasses.MGlassesAttribute;

public class CreatureDescriptiorTest {
	
	@Test
	public void testCheckDescriptionCompactibilitySpecificLevel() {
		CreatureDescriptor creatureDescriptor;
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, MSweaterAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, TopAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, TopAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, TopAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, TopAttribute.class)));
		
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, MShirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, MShirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, MShirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, MShirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, MShirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, MShirtAttribute.class)));
		
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, FShirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, FShirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, FShirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, FShirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, FShirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, FShirtAttribute.class)));
		
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, BottomAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, MPantsAttribute.class)));
		
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, FSkirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, FSkirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, FSkirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, FSkirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, FSkirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, FSkirtAttribute.class)));
	}
	
	@Test
	public void testCheckDescriptionCompactibilityGenericLevel() {
		CreatureDescriptor creatureDescriptor;
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, TopAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, TopAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, TopAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, TopAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, TopAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, MSweaterAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, MSweaterAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, MSweaterAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, MSweaterAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, MSweaterAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, MSweaterAttribute.class)));
		
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, FShirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, FShirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, FShirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, FShirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, FShirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, FShirtAttribute.class)));
		
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, BottomAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, MPantsAttribute.class)));
		
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, FSkirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, FSkirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, FSkirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, FSkirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, FSkirtAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, FSkirtAttribute.class)));
	}
	
	@Test
	public void testCheckDescriptionCompactibilitySuperAndSuperLevel() {
		CreatureDescriptor creatureDescriptor;
		
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(null, null, ClothingAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, ClothingAttribute.class)));
		
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.BLUE, null, ClothingAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, ClothingAttribute.class)));
		
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(null, AttributeGender.MALE, ClothingAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, ClothingAttribute.class)));
		
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, ClothingAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, ClothingAttribute.class)));
	}
	
	@Test
	public void testCheckDescriptionCompactibilityGenricAndSuperLevel() {
		CreatureDescriptor creatureDescriptor;
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, TopAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, ClothingAttribute.class)));
		
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, TopAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, BottomAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.FEMALE, ClothingAttribute.class)));
		
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, TopAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, BottomAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, HeadgearAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.RED, AttributeGender.MALE, MTieAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.ORANGE, null, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.ORANGE, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.ORANGE, AttributeGender.FEMALE, ClothingAttribute.class)));
		
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, TopAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, BottomAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(null, AttributeGender.MALE, HeadgearAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.FEMALE, ClothingAttribute.class)));
	}
	
	@Test
	public void testCheckDescriptionCompactibilitySpecificAndSuperLevel() {
		CreatureDescriptor creatureDescriptor;
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, MSweaterAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, ClothingAttribute.class)));
		
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, MSweaterAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, MPantsAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, ClothingAttribute.class)));
		
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, MSweaterAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, MPantsAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, HeadgearAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.RED, AttributeGender.MALE, MTieAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.ORANGE, null, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.ORANGE, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.ORANGE, AttributeGender.FEMALE, ClothingAttribute.class)));
	}
	
	@Test
	public void testCheckDescriptionCompactibilityCombinationAndSuperLevel() {
		CreatureDescriptor creatureDescriptor;
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, MSweaterAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, BottomAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.FEMALE, ClothingAttribute.class)));
		
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, MSweaterAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, MPantsAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, HeadgearAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.RED, AttributeGender.MALE, MTieAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.ORANGE, null, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.ORANGE, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.ORANGE, AttributeGender.FEMALE, ClothingAttribute.class)));
		
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, TopAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, BottomAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, MHatAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.RED, AttributeGender.MALE, MTieAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, ClothingAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.FEMALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.ORANGE, null, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.ORANGE, AttributeGender.MALE, ClothingAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.ORANGE, AttributeGender.FEMALE, ClothingAttribute.class)));
	}
	
	@Test
	public void testCheckDescriptionCompactibilityCombinations() {
		CreatureDescriptor creatureDescriptor;
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, ClothingAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, TopAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, TopAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, TopAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, TopAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, TopAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, MSweaterAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, MSweaterAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, MSweaterAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, MSweaterAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, MSweaterAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, MSweaterAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BROWN, null, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BROWN, AttributeGender.MALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BROWN, AttributeGender.FEMALE, BottomAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, MPantsAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, HeadgearAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, HeadgearAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, HeadgearAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, HeadgearAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BROWN, null, HeadgearAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BROWN, AttributeGender.MALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BROWN, AttributeGender.FEMALE, HeadgearAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREY, null, HeadgearAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREY, AttributeGender.MALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREY, AttributeGender.FEMALE, HeadgearAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, MHatAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, MHatAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, MHatAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, MHatAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, MHatAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, MHatAttribute.class)));
		
		
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, ClothingAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, TopAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.GREY, AttributeGender.MALE, BottomAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.RED, AttributeGender.MALE, MTieAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, TopAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.FEMALE, TopAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, TopAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, TopAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, MSweaterAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, MSweaterAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, MSweaterAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, MSweaterAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, MSweaterAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, MSweaterAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.FEMALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREY, null, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREY, AttributeGender.MALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREY, AttributeGender.FEMALE, BottomAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, MPantsAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, MPantsAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREY, null, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREY, AttributeGender.MALE, MPantsAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, HeadgearAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, HeadgearAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, HeadgearAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.FEMALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREY, null, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREY, AttributeGender.MALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREY, AttributeGender.FEMALE, HeadgearAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, MHatAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, MHatAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, MHatAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, MHatAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, MHatAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, MHatAttribute.class)));
		
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, ClothingAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, TopAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.GREY, AttributeGender.MALE, BottomAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, HeadgearAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, TopAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.FEMALE, TopAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, TopAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, TopAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, TopAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, MSweaterAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, MSweaterAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, MSweaterAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, MSweaterAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, MSweaterAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, MSweaterAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.FEMALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREY, null, BottomAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREY, AttributeGender.MALE, BottomAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREY, AttributeGender.FEMALE, BottomAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, MPantsAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, MPantsAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREY, null, MPantsAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREY, AttributeGender.MALE, MPantsAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, HeadgearAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, HeadgearAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, HeadgearAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.FEMALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREY, null, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREY, AttributeGender.MALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREY, AttributeGender.FEMALE, HeadgearAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, MHatAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, MHatAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, MHatAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, MHatAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, MHatAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, MHatAttribute.class)));
		
		
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, ClothingAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, MSweaterAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.GREY, AttributeGender.MALE, MPantsAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.RED, AttributeGender.MALE, MTieAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, HeadgearAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, HeadgearAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, HeadgearAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.FEMALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, HeadgearAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, HeadgearAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, HeadgearAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, MHatAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, MHatAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, MHatAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, MHatAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, MHatAttribute.class)));
	}
	
	
	@Test
	public void testCheckDescriptionCompactibilitySpecial() {
		CreatureDescriptor creatureDescriptor;
		
		
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, ClothingAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, MSweaterAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, MPantsAttribute.class));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, GlassesAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, GlassesAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.FEMALE, GlassesAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, GlassesAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, GlassesAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, GlassesAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, GlassesAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, GlassesAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, GlassesAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, GlassesAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, GlassesAttribute.class)));
		assertFalse(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.FEMALE, GlassesAttribute.class)));
		
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, null, MGlassesAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(null, AttributeGender.MALE, MGlassesAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, null, MGlassesAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.BLUE, AttributeGender.MALE, MGlassesAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, null, GlassesAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.GREEN, AttributeGender.MALE, MGlassesAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, null, GlassesAttribute.class)));
		assertTrue(creatureDescriptor.checkDescriptionCompactibility(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.MALE, MGlassesAttribute.class)));
	}
	
	@Test
	public void testGenderCompactibilityOneDescription() {
		CreatureDescriptor creatureDescriptor;
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(null, null, ClothingAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(null, AttributeGender.MALE, MHatAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(null, null, MShirtAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(null, null, TopAttribute.class));
	}
	
	@Test(expected=IllegalStateException.class)
	public void testGenderCompactibilityOneDescriptionException() {
		CreatureDescriptor creatureDescriptor;
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(null, null, ClothingAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(null, AttributeGender.MALE, MHatAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(null, null, MShirtAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(null, null, FSkirtAttribute.class));
	}
	
	@Test(expected=IllegalStateException.class)
	public void testGenderCompactibilityOneDescriptionException2() {
		CreatureDescriptor creatureDescriptor;
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(null, null, ClothingAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(null, AttributeGender.MALE, MHatAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(null, null, MShirtAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(null, AttributeGender.FEMALE, FSkirtAttribute.class));
	}
	
	@Test
	public void testGenderCompactibilityMultiDescription() {
		CreatureDescriptor creatureDescriptor;
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(null, null, ClothingAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(null, AttributeGender.MALE, MHatAttribute.class));
		
		List<AttributeDescription> descriptions = new ArrayList<AttributeDescription>();
		descriptions.add(new AttributeDescription(null, null, TopAttribute.class));
		descriptions.add(new AttributeDescription(null, null, MShirtAttribute.class));
		creatureDescriptor.addDescriptions(descriptions);
	}
	
	@Test
	public void testGenderCompactibilityMultiDescription2() {
		CreatureDescriptor creatureDescriptor;
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(null, null, ClothingAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(null, AttributeGender.MALE, MHatAttribute.class));
		
		List<AttributeDescription> descriptions = new ArrayList<AttributeDescription>();
		descriptions.add(new AttributeDescription(null, null, TopAttribute.class));
		descriptions.add(new AttributeDescription(null, AttributeGender.MALE, MPantsAttribute.class));
		creatureDescriptor.addDescriptions(descriptions);
	}
	
	@Test(expected=IllegalStateException.class)
	public void testGenderCompactibilityMultiDescriptionException() {
		CreatureDescriptor creatureDescriptor;
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(null, null, ClothingAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(null, AttributeGender.MALE, MHatAttribute.class));
		
		List<AttributeDescription> descriptions = new ArrayList<AttributeDescription>();
		descriptions.add(new AttributeDescription(null, AttributeGender.MALE, MShirtAttribute.class));
		descriptions.add(new AttributeDescription(null, AttributeGender.FEMALE, FSkirtAttribute.class));
		creatureDescriptor.addDescriptions(descriptions);
	}
	
	@Test(expected=IllegalStateException.class)
	public void testGenderCompactibilityMultiDescriptionException2() {
		CreatureDescriptor creatureDescriptor;
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(null, null, ClothingAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(null, AttributeGender.MALE, MHatAttribute.class));
		
		List<AttributeDescription> descriptions = new ArrayList<AttributeDescription>();
		descriptions.add(new AttributeDescription(null, AttributeGender.MALE, MShirtAttribute.class));
		descriptions.add(new AttributeDescription(null, null, FSkirtAttribute.class));
		creatureDescriptor.addDescriptions(descriptions);
	}
	
	@Test
	public void testGenderCompactibilityWithColor() {
		CreatureDescriptor creatureDescriptor;
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(null, null, HeadgearAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(null, AttributeGender.FEMALE, FHatAttribute.class));
		// because there are only FEMALE hat with BLUE color
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.BLUE, null, FHatAttribute.class));
	}
	
	@Test(expected=IllegalStateException.class)
	public void testGenderCompactibilityWithColorException() {
		CreatureDescriptor creatureDescriptor;
		creatureDescriptor = new CreatureDescriptor();
		creatureDescriptor.addDescription(new AttributeDescription(null, null, HeadgearAttribute.class));
		creatureDescriptor.addDescription(new AttributeDescription(null, AttributeGender.MALE, FHatAttribute.class));
		// because there are only FEMALE hat with BLUE color
		creatureDescriptor.addDescription(new AttributeDescription(AttributeColor.BLUE, null, FHatAttribute.class));
	}
}
