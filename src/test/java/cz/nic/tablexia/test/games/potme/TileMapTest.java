/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.test.games.potme;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import cz.nic.tablexia.game.games.potme.map.TileMap;
import cz.nic.tablexia.game.games.potme.map.TileMap.TileMapPosition;
import cz.nic.tablexia.game.games.potme.map.TileMap.TileMapQuartals;
import cz.nic.tablexia.game.games.potme.map.tile.Tile;
import cz.nic.tablexia.game.games.potme.map.tile.TileType;

/**
 * Test of map generator for game "Potmě"
 * 
 * @author Matyáš Latner
 *
 */
public class TileMapTest {

    @Test
    public void testGetTileFreeBordersNormal() {
        TileMap tileMap;
        List<TileMapPosition> result;

        //---------------------- ALL EMPTY - USE FREE
        //	    0    1    2
        //	  ----------------     X --> OCCUPIED
        //	0 |    |    |    |     T --> TESTED
        //	  |____|____|____|
        //	1 |    | T  |    |
        //	  |____|____|____|
        //	2 |    |    |    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);

        result = tileMap.getTileFreeBorders(1, 1, true, false);
        Assert.assertEquals(8, result.size());
        Assert.assertEquals(new TileMapPosition(0, 0), result.get(0));
        Assert.assertEquals(new TileMapPosition(0, 1), result.get(1));
        Assert.assertEquals(new TileMapPosition(0, 2), result.get(2));
        Assert.assertEquals(new TileMapPosition(1, 0), result.get(3));
        Assert.assertEquals(new TileMapPosition(1, 2), result.get(4));
        Assert.assertEquals(new TileMapPosition(2, 0), result.get(5));
        Assert.assertEquals(new TileMapPosition(2, 1), result.get(6));
        Assert.assertEquals(new TileMapPosition(2, 2), result.get(7));


        //---------------------- ALL EMPTY - USE NO FREE
        //	    0    1    2
        //	  ----------------     X --> OCCUPIED
        //	0 |    |    |    |     T --> TESTED
        //	  |____|____|____|
        //	1 |    | T  |    |
        //	  |____|____|____|
        //	2 |    |    |    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);

        result = tileMap.getTileFreeBorders(1, 1, false, false);
        Assert.assertEquals(8, result.size());
        Assert.assertEquals(new TileMapPosition(0, 0), result.get(0));
        Assert.assertEquals(new TileMapPosition(0, 1), result.get(1));
        Assert.assertEquals(new TileMapPosition(0, 2), result.get(2));
        Assert.assertEquals(new TileMapPosition(1, 0), result.get(3));
        Assert.assertEquals(new TileMapPosition(1, 2), result.get(4));
        Assert.assertEquals(new TileMapPosition(2, 0), result.get(5));
        Assert.assertEquals(new TileMapPosition(2, 1), result.get(6));
        Assert.assertEquals(new TileMapPosition(2, 2), result.get(7));


        //---------------------- ALL FULL - USE FREE
        //	    0    1    2
        //	  ----------------     X --> OCCUPIED
        //	0 | X  | X  | X  |     T --> TESTED
        //	  |____|____|____|
        //	1 | X  | T  | X  |
        //	  |____|____|____|
        //	2 | X  | X  | X  |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 2, new Tile(TileType.TILE_0));

        result = tileMap.getTileFreeBorders(1, 1, true, false);
        Assert.assertEquals(0, result.size());


        //---------------------- ALL FULL - USE NO FREE
        //	    0    1    2
        //	  ----------------     X --> OCCUPIED
        //	0 | X  | X  | X  |     T --> TESTED
        //	  |____|____|____|
        //	1 | X  | T  | X  |
        //	  |____|____|____|
        //	2 | X  | X  | X  |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 2, new Tile(TileType.TILE_0));

        result = tileMap.getTileFreeBorders(1, 1, false, false);
        Assert.assertEquals(8, result.size());
        Assert.assertEquals(new TileMapPosition(0, 0), result.get(0));
        Assert.assertEquals(new TileMapPosition(0, 1), result.get(1));
        Assert.assertEquals(new TileMapPosition(0, 2), result.get(2));
        Assert.assertEquals(new TileMapPosition(1, 0), result.get(3));
        Assert.assertEquals(new TileMapPosition(1, 2), result.get(4));
        Assert.assertEquals(new TileMapPosition(2, 0), result.get(5));
        Assert.assertEquals(new TileMapPosition(2, 1), result.get(6));
        Assert.assertEquals(new TileMapPosition(2, 2), result.get(7));


        //---------------------- SEMI FULL 1 - USE FREE
        //	    0    1    2
        //	  ----------------     X --> OCCUPIED
        //	0 | X  |    | X  |     T --> TESTED
        //	  |____|____|____|
        //	1 |    | T  |    |
        //	  |____|____|____|
        //	2 | X  |    | X  |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 2, new Tile(TileType.TILE_0));

        result = tileMap.getTileFreeBorders(1, 1, true, false);
        Assert.assertEquals(4, result.size());
        Assert.assertEquals(new TileMapPosition(0, 1), result.get(0));
        Assert.assertEquals(new TileMapPosition(1, 0), result.get(1));
        Assert.assertEquals(new TileMapPosition(1, 2), result.get(2));
        Assert.assertEquals(new TileMapPosition(2, 1), result.get(3));


        //---------------------- SEMI FULL 1 - USE NO FREE
        //	    0    1    2
        //	  ----------------     X --> OCCUPIED
        //	0 | X  |    | X  |     T --> TESTED
        //	  |____|____|____|
        //	1 |    | T  |    |
        //	  |____|____|____|
        //	2 | X  |    | X  |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 2, new Tile(TileType.TILE_0));

        result = tileMap.getTileFreeBorders(1, 1, false, false);
        Assert.assertEquals(8, result.size());
        Assert.assertEquals(new TileMapPosition(0, 0), result.get(0));
        Assert.assertEquals(new TileMapPosition(0, 1), result.get(1));
        Assert.assertEquals(new TileMapPosition(0, 2), result.get(2));
        Assert.assertEquals(new TileMapPosition(1, 0), result.get(3));
        Assert.assertEquals(new TileMapPosition(1, 2), result.get(4));
        Assert.assertEquals(new TileMapPosition(2, 0), result.get(5));
        Assert.assertEquals(new TileMapPosition(2, 1), result.get(6));
        Assert.assertEquals(new TileMapPosition(2, 2), result.get(7));


        //---------------------- SEMI FULL 2 - USE FREE
        //	    0    1    2
        //	  ----------------     X --> OCCUPIED
        //	0 |    | X  |    |     T --> TESTED
        //	  |____|____|____|
        //	1 | X  | T  | X  |
        //	  |____|____|____|
        //	2 |    | X  |    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));

        result = tileMap.getTileFreeBorders(1, 1, true, false);
        Assert.assertEquals(4, result.size());
        Assert.assertEquals(new TileMapPosition(0, 0), result.get(0));
        Assert.assertEquals(new TileMapPosition(0, 2), result.get(1));
        Assert.assertEquals(new TileMapPosition(2, 0), result.get(2));
        Assert.assertEquals(new TileMapPosition(2, 2), result.get(3));


        //---------------------- SEMI FULL 2 - USE NO FREE
        //	    0    1    2
        //	  ----------------     X --> OCCUPIED
        //	0 |    | X  |    |     T --> TESTED
        //	  |____|____|____|
        //	1 | X  | T  | X  |
        //	  |____|____|____|
        //	2 |    | X  |    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));

        result = tileMap.getTileFreeBorders(1, 1, false, false);
        Assert.assertEquals(8, result.size());
        Assert.assertEquals(new TileMapPosition(0, 0), result.get(0));
        Assert.assertEquals(new TileMapPosition(0, 1), result.get(1));
        Assert.assertEquals(new TileMapPosition(0, 2), result.get(2));
        Assert.assertEquals(new TileMapPosition(1, 0), result.get(3));
        Assert.assertEquals(new TileMapPosition(1, 2), result.get(4));
        Assert.assertEquals(new TileMapPosition(2, 0), result.get(5));
        Assert.assertEquals(new TileMapPosition(2, 1), result.get(6));
        Assert.assertEquals(new TileMapPosition(2, 2), result.get(7));


        //---------------------- CORNER 1 - USE FREE
        //	    0    1    2
        //	  ----------------     X --> OCCUPIED
        //	0 |T   |    |    |     T --> TESTED
        //	  |____|____|____|
        //	1 |    | X  |    |
        //	  |____|____|____|
        //	2 |    |    |    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_0));

        result = tileMap.getTileFreeBorders(0, 0, true, false);
        Assert.assertEquals(2, result.size());
        Assert.assertEquals(new TileMapPosition(0, 1), result.get(0));
        Assert.assertEquals(new TileMapPosition(1, 0), result.get(1));

        //---------------------- CORNER 1 - USE NO FREE
        //	    0    1    2
        //	  ----------------     X --> OCCUPIED
        //	0 |T   |    |    |     T --> TESTED
        //	  |____|____|____|
        //	1 |    | X  |    |
        //	  |____|____|____|
        //	2 |    |    |    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_0));

        result = tileMap.getTileFreeBorders(0, 0, false, false);
        Assert.assertEquals(3, result.size());
        Assert.assertEquals(new TileMapPosition(0, 1), result.get(0));
        Assert.assertEquals(new TileMapPosition(1, 0), result.get(1));
        Assert.assertEquals(new TileMapPosition(1, 1), result.get(2));


        //---------------------- CORNER 2 - USE FREE
        //	    0    1    2
        //	  ----------------     X --> OCCUPIED
        //	0 |    |    |    |     T --> TESTED
        //	  |____|____|____|
        //	1 |    |    | X  |
        //	  |____|____|____|
        //	2 |    | X  | T  |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));

        result = tileMap.getTileFreeBorders(2, 2, true, false);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(new TileMapPosition(1, 1), result.get(0));


        //---------------------- CORNER 2 - USE NO FREE
        //	    0    1    2
        //	  ----------------     X --> OCCUPIED
        //	0 |    |    |    |     T --> TESTED
        //	  |____|____|____|
        //	1 |    |    | X  |
        //	  |____|____|____|
        //	2 |    | X  | T  |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));

        result = tileMap.getTileFreeBorders(2, 2, false, false);
        Assert.assertEquals(3, result.size());
        Assert.assertEquals(new TileMapPosition(1, 1), result.get(0));
        Assert.assertEquals(new TileMapPosition(1, 2), result.get(1));
        Assert.assertEquals(new TileMapPosition(2, 1), result.get(2));
    }


    @Test
    public void testGetTileFreeBordersSquare() {
        TileMap tileMap;
        List<TileMapPosition> result;

        //---------------------- ALL EMPTY
        //	    0    1    2
        //	  ----------------     X --> OCCUPIED
        //	0 |    |    |    |     T --> TESTED
        //	  |____|____|____|
        //	1 |    | T  |    |
        //	  |____|____|____|
        //	2 |    |    |    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);

        result = tileMap.getTileFreeBorders(1, 1, true, true);
        Assert.assertEquals(4, result.size());
        Assert.assertEquals(new TileMapPosition(0, 1), result.get(0));
        Assert.assertEquals(new TileMapPosition(1, 0), result.get(1));
        Assert.assertEquals(new TileMapPosition(1, 2), result.get(2));
        Assert.assertEquals(new TileMapPosition(2, 1), result.get(3));


        //---------------------- ALL FULL
        //	    0    1    2
        //	  ----------------     X --> OCCUPIED
        //	0 |    | X  |    |     T --> TESTED
        //	  |____|____|____|
        //	1 | X  | T  | X  |
        //	  |____|____|____|
        //	2 |    | X  |    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));

        result = tileMap.getTileFreeBorders(1, 1, true, true);
        Assert.assertEquals(0, result.size());


        //---------------------- SEMI FULL 1
        //	    0    1    2
        //	  ----------------     X --> OCCUPIED
        //	0 |    | X  |    |     T --> TESTED
        //	  |____|____|____|
        //	1 |    | T  |    |
        //	  |____|____|____|
        //	2 |    | X  |    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));

        result = tileMap.getTileFreeBorders(1, 1, true, true);
        Assert.assertEquals(2, result.size());
        Assert.assertEquals(new TileMapPosition(0, 1), result.get(0));
        Assert.assertEquals(new TileMapPosition(2, 1), result.get(1));


        //---------------------- SEMI FULL 2
        //	    0    1    2
        //	  ----------------     X --> OCCUPIED
        //	0 |    |    |    |     T --> TESTED
        //	  |____|____|____|
        //	1 | X  | T  | X  |
        //	  |____|____|____|
        //	2 |    |    |    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));

        result = tileMap.getTileFreeBorders(1, 1, true, true);
        Assert.assertEquals(2, result.size());
        Assert.assertEquals(new TileMapPosition(1, 0), result.get(0));
        Assert.assertEquals(new TileMapPosition(1, 2), result.get(1));


        //---------------------- CORNER 1
        //	    0    1    2
        //	  ----------------     X --> OCCUPIED
        //	0 | T  | X  |    |     T --> TESTED
        //	  |____|____|____|
        //	1 |    |    |    |
        //	  |____|____|____|
        //	2 |    |    |    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));

        result = tileMap.getTileFreeBorders(0, 0, true, true);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(new TileMapPosition(0, 1), result.get(0));


        //---------------------- CORNER 2
        //	    0    1    2
        //	  ----------------     X --> OCCUPIED
        //	0 |    |    |    |     T --> TESTED
        //	  |____|____|____|
        //	1 |    |    | X  |
        //	  |____|____|____|
        //	2 |    |    | T  |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));

        result = tileMap.getTileFreeBorders(2, 2, true, true);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals(new TileMapPosition(1, 2), result.get(0));
    }


    @Test
    public void testGetMapQuartalPositions() {
        TileMap tileMap;
        List<TileMapPosition> mapQuartalPositions;

        //---------------- SQUARE MAP QUARTALS
        //
        //	  ---------------------
        //	  | Q1 | Q1 | Q2 | Q2 |
        //	  |____|____|____|____|
        //	  | Q1 | Q1 | Q2 | Q2 |
        //	  |____|____|____|____|
        //	  | Q3 | Q3 | Q4 | Q4 |
        //    |____|____|____|____|
        //	  | Q3 | Q3 | Q4 | Q4 |
        //	  |____|____|____|____|

        tileMap = new TileMap(4, 4);
        mapQuartalPositions = tileMap.getMapQuartalPositions(TileMapQuartals.QUARTAL1);
        Assert.assertEquals(4, mapQuartalPositions.size());
        Assert.assertEquals(new TileMapPosition(0, 0), mapQuartalPositions.get(0));
        Assert.assertEquals(new TileMapPosition(0, 1), mapQuartalPositions.get(1));
        Assert.assertEquals(new TileMapPosition(1, 0), mapQuartalPositions.get(2));
        Assert.assertEquals(new TileMapPosition(1, 1), mapQuartalPositions.get(3));

        mapQuartalPositions = tileMap.getMapQuartalPositions(TileMapQuartals.QUARTAL2);
        Assert.assertEquals(4, mapQuartalPositions.size());
        Assert.assertEquals(new TileMapPosition(2, 0), mapQuartalPositions.get(0));
        Assert.assertEquals(new TileMapPosition(2, 1), mapQuartalPositions.get(1));
        Assert.assertEquals(new TileMapPosition(3, 0), mapQuartalPositions.get(2));
        Assert.assertEquals(new TileMapPosition(3, 1), mapQuartalPositions.get(3));

        mapQuartalPositions = tileMap.getMapQuartalPositions(TileMapQuartals.QUARTAL3);
        Assert.assertEquals(4, mapQuartalPositions.size());
        Assert.assertEquals(new TileMapPosition(0, 2), mapQuartalPositions.get(0));
        Assert.assertEquals(new TileMapPosition(0, 3), mapQuartalPositions.get(1));
        Assert.assertEquals(new TileMapPosition(1, 2), mapQuartalPositions.get(2));
        Assert.assertEquals(new TileMapPosition(1, 3), mapQuartalPositions.get(3));

        mapQuartalPositions = tileMap.getMapQuartalPositions(TileMapQuartals.QUARTAL4);
        Assert.assertEquals(4, mapQuartalPositions.size());
        Assert.assertEquals(new TileMapPosition(2, 2), mapQuartalPositions.get(0));
        Assert.assertEquals(new TileMapPosition(2, 3), mapQuartalPositions.get(1));
        Assert.assertEquals(new TileMapPosition(3, 2), mapQuartalPositions.get(2));
        Assert.assertEquals(new TileMapPosition(3, 3), mapQuartalPositions.get(3));


        //---------------- RECTANGLE MAP 1 QUARTALS
        //
        //	  --------------------------
        //	  | Q1 | Q1 | Q2 | Q2 | Q2 |
        //	  |____|____|____|____|____|
        //	  | Q1 | Q1 | Q2 | Q2 | Q2 |
        //	  |____|____|____|____|____|
        //	  | Q3 | Q3 | Q4 | Q4 | Q4 |
        //    |____|____|____|____|____|
        //	  | Q3 | Q3 | Q4 | Q4 | Q4 |
        //	  |____|____|____|____|____|

        tileMap = new TileMap(5, 4);

        mapQuartalPositions = tileMap.getMapQuartalPositions(TileMapQuartals.QUARTAL1);
        Assert.assertEquals(4, mapQuartalPositions.size());
        Assert.assertEquals(new TileMapPosition(0, 0), mapQuartalPositions.get(0));
        Assert.assertEquals(new TileMapPosition(0, 1), mapQuartalPositions.get(1));
        Assert.assertEquals(new TileMapPosition(1, 0), mapQuartalPositions.get(2));
        Assert.assertEquals(new TileMapPosition(1, 1), mapQuartalPositions.get(3));

        mapQuartalPositions = tileMap.getMapQuartalPositions(TileMapQuartals.QUARTAL2);
        Assert.assertEquals(6, mapQuartalPositions.size());
        Assert.assertEquals(new TileMapPosition(2, 0), mapQuartalPositions.get(0));
        Assert.assertEquals(new TileMapPosition(2, 1), mapQuartalPositions.get(1));
        Assert.assertEquals(new TileMapPosition(3, 0), mapQuartalPositions.get(2));
        Assert.assertEquals(new TileMapPosition(3, 1), mapQuartalPositions.get(3));
        Assert.assertEquals(new TileMapPosition(4, 0), mapQuartalPositions.get(4));
        Assert.assertEquals(new TileMapPosition(4, 1), mapQuartalPositions.get(5));

        mapQuartalPositions = tileMap.getMapQuartalPositions(TileMapQuartals.QUARTAL3);
        Assert.assertEquals(4, mapQuartalPositions.size());
        Assert.assertEquals(new TileMapPosition(0, 2), mapQuartalPositions.get(0));
        Assert.assertEquals(new TileMapPosition(0, 3), mapQuartalPositions.get(1));
        Assert.assertEquals(new TileMapPosition(1, 2), mapQuartalPositions.get(2));
        Assert.assertEquals(new TileMapPosition(1, 3), mapQuartalPositions.get(3));

        mapQuartalPositions = tileMap.getMapQuartalPositions(TileMapQuartals.QUARTAL4);
        Assert.assertEquals(6, mapQuartalPositions.size());
        Assert.assertEquals(new TileMapPosition(2, 2), mapQuartalPositions.get(0));
        Assert.assertEquals(new TileMapPosition(2, 3), mapQuartalPositions.get(1));
        Assert.assertEquals(new TileMapPosition(3, 2), mapQuartalPositions.get(2));
        Assert.assertEquals(new TileMapPosition(3, 3), mapQuartalPositions.get(3));
        Assert.assertEquals(new TileMapPosition(4, 2), mapQuartalPositions.get(4));
        Assert.assertEquals(new TileMapPosition(4, 3), mapQuartalPositions.get(5));


        //---------------- RECTANGLE MAP 2 QUARTALS
        //
        //	  ---------------------
        //	  | Q1 | Q1 | Q2 | Q2 |
        //	  |____|____|____|____|
        //	  | Q1 | Q1 | Q2 | Q2 |
        //	  |____|____|____|____|
        //	  | Q3 | Q3 | Q4 | Q4 |
        //    |____|____|____|____|
        //	  | Q3 | Q3 | Q4 | Q4 |
        //	  |____|____|____|____|
        //    | Q3 | Q3 | Q4 | Q4 |
        //	  |____|____|____|____|

        tileMap = new TileMap(4, 5);

        mapQuartalPositions = tileMap.getMapQuartalPositions(TileMapQuartals.QUARTAL1);
        Assert.assertEquals(4, mapQuartalPositions.size());
        Assert.assertEquals(new TileMapPosition(0, 0), mapQuartalPositions.get(0));
        Assert.assertEquals(new TileMapPosition(0, 1), mapQuartalPositions.get(1));
        Assert.assertEquals(new TileMapPosition(1, 0), mapQuartalPositions.get(2));
        Assert.assertEquals(new TileMapPosition(1, 1), mapQuartalPositions.get(3));

        mapQuartalPositions = tileMap.getMapQuartalPositions(TileMapQuartals.QUARTAL2);
        Assert.assertEquals(4, mapQuartalPositions.size());
        Assert.assertEquals(new TileMapPosition(2, 0), mapQuartalPositions.get(0));
        Assert.assertEquals(new TileMapPosition(2, 1), mapQuartalPositions.get(1));
        Assert.assertEquals(new TileMapPosition(3, 0), mapQuartalPositions.get(2));
        Assert.assertEquals(new TileMapPosition(3, 1), mapQuartalPositions.get(3));

        mapQuartalPositions = tileMap.getMapQuartalPositions(TileMapQuartals.QUARTAL3);
        Assert.assertEquals(6, mapQuartalPositions.size());
        Assert.assertEquals(new TileMapPosition(0, 2), mapQuartalPositions.get(0));
        Assert.assertEquals(new TileMapPosition(0, 3), mapQuartalPositions.get(1));
        Assert.assertEquals(new TileMapPosition(0, 4), mapQuartalPositions.get(2));
        Assert.assertEquals(new TileMapPosition(1, 2), mapQuartalPositions.get(3));
        Assert.assertEquals(new TileMapPosition(1, 3), mapQuartalPositions.get(4));
        Assert.assertEquals(new TileMapPosition(1, 4), mapQuartalPositions.get(5));

        mapQuartalPositions = tileMap.getMapQuartalPositions(TileMapQuartals.QUARTAL4);
        Assert.assertEquals(6, mapQuartalPositions.size());
        Assert.assertEquals(new TileMapPosition(2, 2), mapQuartalPositions.get(0));
        Assert.assertEquals(new TileMapPosition(2, 3), mapQuartalPositions.get(1));
        Assert.assertEquals(new TileMapPosition(2, 4), mapQuartalPositions.get(2));
        Assert.assertEquals(new TileMapPosition(3, 2), mapQuartalPositions.get(3));
        Assert.assertEquals(new TileMapPosition(3, 3), mapQuartalPositions.get(4));
        Assert.assertEquals(new TileMapPosition(3, 4), mapQuartalPositions.get(5));
    }
}
