/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.test.games.potme;

import org.junit.Assert;
import org.junit.Test;

import cz.nic.tablexia.game.games.potme.map.tile.TileType;

/**
 * Test of tile type for game "Potmě"
 * 
 * @author Matyáš Latner
 *
 */
public class TileTypeTest {

    @Test
    public void testGetSwipedTileType() {
    	Assert.assertEquals(TileType.TILE_1C, TileType.getSwipedTileType(TileType.TILE_1A));
    	Assert.assertEquals(TileType.TILE_1D, TileType.getSwipedTileType(TileType.TILE_1B));
    	Assert.assertEquals(TileType.TILE_1A, TileType.getSwipedTileType(TileType.TILE_1C));
    	Assert.assertEquals(TileType.TILE_1B, TileType.getSwipedTileType(TileType.TILE_1D));
    	
    	Assert.assertEquals(TileType.TILE_2LC, TileType.getSwipedTileType(TileType.TILE_2LA));
    	Assert.assertEquals(TileType.TILE_2LD, TileType.getSwipedTileType(TileType.TILE_2LB));
    	Assert.assertEquals(TileType.TILE_2LA, TileType.getSwipedTileType(TileType.TILE_2LC));
    	Assert.assertEquals(TileType.TILE_2LB, TileType.getSwipedTileType(TileType.TILE_2LD));
    	
    	Assert.assertEquals(TileType.TILE_3C, TileType.getSwipedTileType(TileType.TILE_3A));
    	Assert.assertEquals(TileType.TILE_3D, TileType.getSwipedTileType(TileType.TILE_3B));
    	Assert.assertEquals(TileType.TILE_3A, TileType.getSwipedTileType(TileType.TILE_3C));
    	Assert.assertEquals(TileType.TILE_3B, TileType.getSwipedTileType(TileType.TILE_3D));
    	
    	Assert.assertNull(TileType.getSwipedTileType(TileType.TILE_0));
    	Assert.assertNull(TileType.getSwipedTileType(TileType.TILE_2IH));
    	Assert.assertNull(TileType.getSwipedTileType(TileType.TILE_4));
    }
}
