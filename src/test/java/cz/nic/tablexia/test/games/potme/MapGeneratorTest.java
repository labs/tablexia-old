/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.test.games.potme;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.potme.map.MapGenerator;
import cz.nic.tablexia.game.games.potme.map.TileMap;
import cz.nic.tablexia.game.games.potme.map.TileMap.TileMapPosition;
import cz.nic.tablexia.game.games.potme.map.tile.Tile;
import cz.nic.tablexia.game.games.potme.map.tile.TileType;

/**
 * Test of map generator for game "Potmě"
 * 
 * @author Matyáš Latner
 *
 */
public class MapGeneratorTest extends MapGenerator {

    public MapGeneratorTest() {
        super(new RandomAccess());
    }

    @Test
    public void testGetRandomTileForMapPositionStrict() {
        Tile result;
        TileMap tileMap;

        //---------------- TILE_0
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|XXXX|____|     ? --> NOT SPECIFIED
        //	1 |    X    X    |
        //	  |____X    X____|
        //	2 |    |XXXX|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        Assert.assertEquals(TileType.TILE_0, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertEquals(TileType.TILE_0, result.getTileType());


        //---------------- TILE_1A
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|DDDD|____|     ? --> NOT SPECIFIED
        //	1 |    X    X    |
        //	  |____X    X____|
        //	2 |    |XXXX|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_1C));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        Assert.assertEquals(TileType.TILE_1A, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertEquals(TileType.TILE_1A, result.getTileType());


        //---------------- TILE_1B
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|XXXX|____|     ? --> NOT SPECIFIED
        //	1 |    X    D    |
        //	  |____X    D____|
        //	2 |    |XXXX|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_1D));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        Assert.assertEquals(TileType.TILE_1B, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertEquals(TileType.TILE_1B, result.getTileType());


        //---------------- TILE_1C
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|XXXX|____|     ? --> NOT SPECIFIED
        //	1 |    X    X    |
        //	  |____X    X____|
        //	2 |    |DDDD|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_1A));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        Assert.assertEquals(TileType.TILE_1C, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertEquals(TileType.TILE_1C, result.getTileType());


        //---------------- TILE_1D
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|XXXX|____|     ? --> NOT SPECIFIED
        //	1 |    D    X    |
        //	  |____D    X____|
        //	2 |    |XXXX|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_1B));

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        Assert.assertEquals(TileType.TILE_1D, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertEquals(TileType.TILE_1D, result.getTileType());


        //---------------- TILE_2IV
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|DDDD|____|     ? --> NOT SPECIFIED
        //	1 |    X    X    |
        //	  |____X    X____|
        //	2 |    |DDDD|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_1C));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_1A));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2IV, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertEquals(TileType.TILE_2IV, result.getTileType());

        //---------------- TILE_2IH
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|XXXX|____|     ? --> NOT SPECIFIED
        //	1 |    D    D    |
        //	  |____D    D____|
        //	2 |    |XXXX|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_1D));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_1B));

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2IH, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertEquals(TileType.TILE_2IH, result.getTileType());


        //---------------- TILE_2LA
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|DDDD|____|     ? --> NOT SPECIFIED
        //	1 |    X    D    |
        //	  |____X    D____|
        //	2 |    |XXXX|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_1C));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_1D));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2LA, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertEquals(TileType.TILE_2LA, result.getTileType());


        //---------------- TILE_2LB
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|XXXX|____|     ? --> NOT SPECIFIED
        //	1 |    X    D    |
        //	  |____X    D____|
        //	2 |    |DDDD|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_1D));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_1A));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2LB, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertEquals(TileType.TILE_2LB, result.getTileType());


        //---------------- TILE_2LC
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|XXXX|____|     ? --> NOT SPECIFIED
        //	1 |    D    X    |
        //	  |____D    X____|
        //	2 |    |DDDD|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_1A));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_1B));

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2LC, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertEquals(TileType.TILE_2LC, result.getTileType());


        //---------------- TILE_2LD
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|DDDD|____|     ? --> NOT SPECIFIED
        //	1 |    D    X    |
        //	  |____D    X____|
        //	2 |    |XXXX|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_1C));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_1B));

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2LD, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertEquals(TileType.TILE_2LD, result.getTileType());


        //---------------- TILE_3A
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|DDDD|____|     ? --> NOT SPECIFIED
        //	1 |    X    D    |
        //	  |____X    D____|
        //	2 |    |DDDD|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_1C));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_1D));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_1A));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_3A, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertEquals(TileType.TILE_3A, result.getTileType());


        //---------------- TILE_3B
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|XXXX|____|     ? --> NOT SPECIFIED
        //	1 |    D    D    |
        //	  |____D    D____|
        //	2 |    |DDDD|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_1D));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_1A));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_1B));

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_3B, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertEquals(TileType.TILE_3B, result.getTileType());


        //---------------- TILE_3C
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|DDDD|____|     ? --> NOT SPECIFIED
        //	1 |    D    X    |
        //	  |____D    X____|
        //	2 |    |DDDD|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_1C));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_1A));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_1B));

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_3C, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertEquals(TileType.TILE_3C, result.getTileType());


        //---------------- TILE_3D
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|DDDD|____|     ? --> NOT SPECIFIED
        //	1 |    D    D    |
        //	  |____D    D____|
        //	2 |    |XXXX|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_1C));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_1D));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_1B));

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_3D, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertEquals(TileType.TILE_3D, result.getTileType());


        //---------------- TILE_4
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|DDDD|____|     ? --> NOT SPECIFIED
        //	1 |    D    D    |
        //	  |____D    D____|
        //	2 |    |DDDD|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_1C));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_1D));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_1A));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_1B));

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertEquals(TileType.TILE_4, result.getTileType());
    }

    @Test
    public void testGetRandomTileForMapPositionRandom() {
        Tile result;
        TileMap tileMap;

        //---------------- TILE_1A OR TILE_2IV
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|DDDD|____|     ? --> NOT SPECIFIED
        //	1 |    X    X    |
        //	  |____X    X____|
        //	2 |    |????|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_1C));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, null);
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertNotSame(TileType.TILE_0, result.getTileType());
        // TILE_1A
        Assert.assertNotSame(TileType.TILE_1B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        // TILE_2IV
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        Assert.assertEquals(TileType.TILE_1A, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2IV, result.getTileType());


        //---------------- TILE_1B OR TILE_2IH
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|XXXX|____|     ? --> NOT SPECIFIED
        //	1 |    ?    D    |
        //	  |____?    D____|
        //	2 |    |XXXX|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_1D));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, null);

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertNotSame(TileType.TILE_0, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        // TILE_1B
        Assert.assertNotSame(TileType.TILE_1C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        // TILE_2IH
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        Assert.assertEquals(TileType.TILE_1B, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2IH, result.getTileType());


        //---------------- TILE_1C OR TILE_2IV
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|????|____|     ? --> NOT SPECIFIED
        //	1 |    X    X    |
        //	  |____X    X____|
        //	2 |    |DDDD|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, null);
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_1A));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertNotSame(TileType.TILE_0, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1B, result.getTileType());
        // TILE_1C
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        // TILE_2IV
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());


        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        Assert.assertEquals(TileType.TILE_1C, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2IV, result.getTileType());


        //---------------- TILE_1D OR TILE_2IH
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|XXXX|____|     ? --> NOT SPECIFIED
        //	1 |    D    ?    |
        //	  |____D    ?____|
        //	2 |    |XXXX|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, null);
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_1B));

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertNotSame(TileType.TILE_0, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1C, result.getTileType());
        // TILE_1D
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        // TILE_2IH
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        Assert.assertEquals(TileType.TILE_1D, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2IH, result.getTileType());


        //---------------- TILE_0 OR TILE_1A OR TILE_1C OR TILE_2IV
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|????|____|     ? --> NOT SPECIFIED
        //	1 |    X    X    |
        //	  |____X    X____|
        //	2 |    |????|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, null);
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, null);
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        // TILE_0
        // TILE_1A
        Assert.assertNotSame(TileType.TILE_1B, result.getTileType());
        // TILE_1C
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        // TILE_2IV
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        // TILE_0
        // TILE_1A
        Assert.assertNotSame(TileType.TILE_1B, result.getTileType());
        // TILE_1C
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2IV, result.getTileType());


        //---------------- TILE_0 OR TILE_1B OR TILE_1D OR TILE_2IH
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|XXXX|____|     ? --> NOT SPECIFIED
        //	1 |    ?    ?    |
        //	  |____?    ?____|
        //	2 |    |XXXX|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, null);
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, null);

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        // TILE_0
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        // TILE_1B
        Assert.assertNotSame(TileType.TILE_1C, result.getTileType());
        // TILE_1D
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        // TILE_2IH
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        // TILE_0
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        // TILE_1B
        Assert.assertNotSame(TileType.TILE_1C, result.getTileType());
        // TILE_1D
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2IH, result.getTileType());


        //---------------- TILE_1A OR TILE_2LA
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|DDDD|____|     ? --> NOT SPECIFIED
        //	1 |    X    ?    |
        //	  |____X    ?____|
        //	2 |    |XXXX|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_1C));
        tileMap.addTileAtPosition(2, 1, null);
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertNotSame(TileType.TILE_0, result.getTileType());
        // TILE_1A
        Assert.assertNotSame(TileType.TILE_1B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        // TILE_2LA
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2LA, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        Assert.assertEquals(TileType.TILE_1A, result.getTileType());


        //---------------- TILE_1B OR TILE_2LB
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|XXXX|____|     ? --> NOT SPECIFIED
        //	1 |    X    D    |
        //	  |____X    D____|
        //	2 |    |????|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_1D));
        tileMap.addTileAtPosition(1, 2, null);
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertNotSame(TileType.TILE_0, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        // TILE_1B
        Assert.assertNotSame(TileType.TILE_1C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        // TILE_2LB
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2LB, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        Assert.assertEquals(TileType.TILE_1B, result.getTileType());


        //---------------- TILE_1C OR TILE_2LC
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|XXXX|____|     ? --> NOT SPECIFIED
        //	1 |    ?    X    |
        //	  |____?    X____|
        //	2 |    |DDDD|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_1A));
        tileMap.addTileAtPosition(0, 1, null);

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertNotSame(TileType.TILE_0, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1B, result.getTileType());
        // TILE_1C
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        // TILE_2LC
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2LC, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        Assert.assertEquals(TileType.TILE_1C, result.getTileType());


        //---------------- TILE_1D OR TILE_2LD
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|????|____|     ? --> NOT SPECIFIED
        //	1 |    D    X    |
        //	  |____D    X____|
        //	2 |    |XXXX|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, null);
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_1B));

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertNotSame(TileType.TILE_0, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1C, result.getTileType());
        // TILE_1D
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        // TILE_2LD
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2LD, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        Assert.assertEquals(TileType.TILE_1D, result.getTileType());


        //---------------- TILE_0 OR TILE_1A OR TILE_1B OR TILE_2LA
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|????|____|     ? --> NOT SPECIFIED
        //	1 |    X    ?    |
        //	  |____X    ?____|
        //	2 |    |XXXX|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, null);
        tileMap.addTileAtPosition(2, 1, null);
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        // TILE_0
        // TILE_1A
        // TILE_1B
        Assert.assertNotSame(TileType.TILE_1C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        // TILE_2LA
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2LA, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        // TILE_0
        // TILE_1A
        // TILE_1B
        Assert.assertNotSame(TileType.TILE_1C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());


        //---------------- TILE_0 OR TILE_1B OR TILE_1C OR TILE_2LB
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|XXXX|____|     ? --> NOT SPECIFIED
        //	1 |    X    ?    |
        //	  |____X    ?____|
        //	2 |    |????|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, null);
        tileMap.addTileAtPosition(1, 2, null);
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        // TILE_0
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        // TILE_1B
        // TILE_1C
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        // TILE_2LB
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2LB, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        // TILE_0
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        // TILE_1A
        // TILE_1C
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());


        //---------------- TILE_0 OR TILE_1C OR TILE_1D OR TILE_2LC
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|XXXX|____|     ? --> NOT SPECIFIED
        //	1 |    ?    X    |
        //	  |____?    X____|
        //	2 |    |????|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, null);
        tileMap.addTileAtPosition(0, 1, null);

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        // TILE_0
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1B, result.getTileType());
        // TILE_1C
        // TILE_1D
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        // TILE_2LC
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2LC, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        // TILE_0
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1B, result.getTileType());
        // TILE_1C
        // TILE_1D
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());


        //---------------- TILE_0 OR TILE_1A OR TILE_1D OR TILE_2LD
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|????|____|     ? --> NOT SPECIFIED
        //	1 |    ?    X    |
        //	  |____?    X____|
        //	2 |    |XXXX|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, null);
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, null);

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        // TILE_0
        // TILE_1A
        Assert.assertNotSame(TileType.TILE_1B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1C, result.getTileType());
        // TILE_1D
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        // TILE_2LD
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertEquals(TileType.TILE_2LD, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        // TILE_0
        // TILE_1A
        Assert.assertNotSame(TileType.TILE_1B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1C, result.getTileType());
        // TILE_1D
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());


        //---------------- TILE_0 OR TILE_1A OR TILE_1B OR TILE_1C OR TILE_2LA OR TILE_2LB OR TILE_2IV OR TILE_3A
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|????|____|     ? --> NOT SPECIFIED
        //	1 |    X    ?    |
        //	  |____X    ?____|
        //	2 |    |????|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, null);
        tileMap.addTileAtPosition(2, 1, null);
        tileMap.addTileAtPosition(1, 2, null);
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        // TILE_0
        // TILE_1A
        // TILE_1B
        // TILE_1C
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        // TILE_2IV
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        // TILE_2LA
        // TILE_2LB
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        // TILE_3A
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertNotSame(TileType.TILE_0, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        // TILE_2IV
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        // TILE_2LA
        // TILE_2LB
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        // TILE_3A
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        // TILE_0
        // TILE_1A
        // TILE_1B
        // TILE_1C
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());


        //---------------- TILE_0 OR TILE_1B OR TILE_1C OR TILE_1D OR TILE_2LB OR TILE_2LC OR TILE_2IH OR TILE_3B
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|XXXX|____|     ? --> NOT SPECIFIED
        //	1 |    ?    ?    |
        //	  |____?    ?____|
        //	2 |    |????|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, null);
        tileMap.addTileAtPosition(1, 2, null);
        tileMap.addTileAtPosition(0, 1, null);

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        // TILE_0
        // TILE_1B
        // TILE_1C
        // TILE_1D
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        // TILE_2IH
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        // TILE_2LB
        // TILE_2LC
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        // TILE_3B
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertNotSame(TileType.TILE_0, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        // TILE_2IH
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        // TILE_2LB
        // TILE_2LC
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        // TILE_3B
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        // TILE_0
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        // TILE_1B
        // TILE_1C
        // TILE_1D
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());


        //---------------- TILE_0 OR TILE_1A OR TILE_1C OR TILE_1D OR TILE_2LC OR TILE_2LD OR TILE_2IV OR TILE_3C
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|????|____|     ? --> NOT SPECIFIED
        //	1 |    ?    X    |
        //	  |____?    X____|
        //	2 |    |????|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, null);
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, null);
        tileMap.addTileAtPosition(0, 1, null);

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        // TILE_0
        // TILE_1A
        Assert.assertNotSame(TileType.TILE_1B, result.getTileType());
        // TILE_1C
        // TILE_1D
        // TILE_2IV
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        // TILE_2LC
        // TILE_2LD
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        // TILE_3C
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertNotSame(TileType.TILE_0, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        // TILE_2IV
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        // TILE_2LC
        // TILE_2LD
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        // TILE_3C
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        // TILE_0
        // TILE_1A
        Assert.assertNotSame(TileType.TILE_1B, result.getTileType());
        // TILE_1C
        // TILE_1D
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());


        //---------------- TILE_0 OR TILE_1A OR TILE_1B OR TILE_1D OR TILE_2LA OR TILE_2LD OR TILE_2IH OR TILE_3D
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|????|____|     ? --> NOT SPECIFIED
        //	1 |    ?    ?    |
        //	  |____?    ?____|
        //	2 |    |XXXX|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, null);
        tileMap.addTileAtPosition(2, 1, null);
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, null);

        result = getRandomTileForMapPosition(tileMap, 1, 1, null);
        // TILE_0
        // TILE_1A
        // TILE_1B
        Assert.assertNotSame(TileType.TILE_1C, result.getTileType());
        // TILE_1D
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        // TILE_2IH
        // TILE_2LA
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        // TILE_2LD
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        // TILE_3D
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertNotSame(TileType.TILE_0, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        // TILE_2IH
        // TILE_2LA
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        // TILE_2LD
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        // TILE_3D
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        // TILE_0
        // TILE_1A
        // TILE_1B
        Assert.assertNotSame(TileType.TILE_1C, result.getTileType());
        // TILE_1D
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());


        //---------------- RANDOM
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|????|____|     ? --> NOT SPECIFIED
        //	1 |    ?    ?    |
        //	  |____?    ?____|
        //	2 |    |????|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, null);
        tileMap.addTileAtPosition(2, 1, null);
        tileMap.addTileAtPosition(1, 2, null);
        tileMap.addTileAtPosition(0, 1, null);

        result = getRandomTileForMapPosition(tileMap, 1, 1, false);
        // TILE_0
        // TILE_1A
        // TILE_1B
        // TILE_1C
        // TILE_1D
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 1, 1, true);
        Assert.assertNotSame(TileType.TILE_0, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
    }

    @Test(expected=IllegalStateException.class)
    public void testGetRandomTileForMapPositionExceptionTile0() {
        TileMap tileMap;

        //---------------- TILE_0
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|XXXX|____|     ? --> NOT SPECIFIED
        //	1 |    X    X    |
        //	  |____X    X____|
        //	2 |    |XXXX|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        getRandomTileForMapPosition(tileMap, 1, 1, true);
    }

    @Test(expected=IllegalStateException.class)
    public void testGetRandomTileForMapPositionExceptionTile1A() {
        TileMap tileMap;

        //---------------- TILE_1A
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|DDDD|____|     ? --> NOT SPECIFIED
        //	1 |    X    X    |
        //	  |____X    X____|
        //	2 |    |XXXX|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_1C));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        getRandomTileForMapPosition(tileMap, 1, 1, true);
    }

    @Test(expected=IllegalStateException.class)
    public void testGetRandomTileForMapPositionExceptionTile2IH() {
        TileMap tileMap;

        //---------------- TILE_2IH
        //	    0    1    2
        //	  ----------------     X --> WALL
        //	0 |    |    |    |     D --> DOOR
        //	  |____|XXXX|____|     ? --> NOT SPECIFIED
        //	1 |    D    D    |
        //	  |____D    D____|
        //	2 |    |XXXX|    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_1D));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_1B));

        getRandomTileForMapPosition(tileMap, 1, 1, false);
    }

    @Test
    public void testGetRandomTileForMapPositionBorder() {
        Tile result;
        TileMap tileMap;

        //---------------- TILE_0
        //	    0    1
        //	  -----------     X --> WALL
        //	0 |    X    |     D --> DOOR
        //	  |    X____|     ? --> NOT SPECIFIED
        //	1 |XXXX|    |
        //	  |____|____|
        //
        //

        tileMap = new TileMap(2, 2);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        result = getRandomTileForMapPosition(tileMap, 0, 0, false);
        Assert.assertEquals(TileType.TILE_0, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 0, 0, null);
        Assert.assertEquals(TileType.TILE_0, result.getTileType());


        //---------------- TILE_1B
        //	    0    1
        //	  -----------     X --> WALL
        //	0 |    D    |     D --> DOOR
        //	  |    D____|     ? --> NOT SPECIFIED
        //	1 |XXXX|    |
        //	  |____|____|
        //
        //

        tileMap = new TileMap(2, 2);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_1D));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        result = getRandomTileForMapPosition(tileMap, 0, 0, false);
        Assert.assertEquals(TileType.TILE_1B, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 0, 0, null);
        Assert.assertEquals(TileType.TILE_1B, result.getTileType());


        //---------------- TILE_1C
        //	    0    1
        //	  -----------     X --> WALL
        //	0 |    X    |     D --> DOOR
        //	  |    X____|     ? --> NOT SPECIFIED
        //	1 |DDDD|    |
        //	  |____|____|
        //
        //

        tileMap = new TileMap(2, 2);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_1A));

        result = getRandomTileForMapPosition(tileMap, 0, 0, false);
        Assert.assertEquals(TileType.TILE_1C, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 0, 0, null);
        Assert.assertEquals(TileType.TILE_1C, result.getTileType());


        //---------------- TILE_2LB
        //	    0    1
        //	  -----------     X --> WALL
        //	0 |    D    |     D --> DOOR
        //	  |    D____|     ? --> NOT SPECIFIED
        //	1 |DDDD|    |
        //	  |____|____|
        //
        //

        tileMap = new TileMap(2, 2);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_1D));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_1A));

        result = getRandomTileForMapPosition(tileMap, 0, 0, true);
        Assert.assertEquals(TileType.TILE_2LB, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 0, 0, null);
        Assert.assertEquals(TileType.TILE_2LB, result.getTileType());


        //---------------- TILE_0 OR TILE_1B TILE_1C OR OR TILE_2LB
        //	    0    1
        //	  -----------     X --> WALL
        //	0 |    ?    |     D --> DOOR
        //	  |    ?____|     ? --> NOT SPECIFIED
        //	1 |????|    |
        //	  |____|____|
        //
        //

        tileMap = new TileMap(2, 2);
        tileMap.addTileAtPosition(1, 0, null);
        tileMap.addTileAtPosition(0, 1, null);

        result = getRandomTileForMapPosition(tileMap, 0, 0, null);
        // TILE_0
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        // TILE_1B
        // TILE_1C
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        // TILE_2LB
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());


        result = getRandomTileForMapPosition(tileMap, 0, 0, false);
        // TILE_0
        Assert.assertNotSame(TileType.TILE_1A, result.getTileType());
        // TILE_1B
        // TILE_1C
        Assert.assertNotSame(TileType.TILE_1D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IV, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2IH, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LA, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LB, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LC, result.getTileType());
        Assert.assertNotSame(TileType.TILE_2LD, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3A, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3B, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3C, result.getTileType());
        Assert.assertNotSame(TileType.TILE_3D, result.getTileType());
        Assert.assertNotSame(TileType.TILE_4, result.getTileType());

        result = getRandomTileForMapPosition(tileMap, 0, 0, true);
        Assert.assertEquals(TileType.TILE_2LB, result.getTileType());
    }

    @Test(expected=IllegalStateException.class)
    public void testGetRandomTileForMapPositionBorderException1() {
        TileMap tileMap;

        //---------------- TILE_0
        //	    0    1
        //	  -----------     X --> WALL
        //	0 |    X    |     D --> DOOR
        //	  |    X____|     ? --> NOT SPECIFIED
        //	1 |XXXX|    |
        //	  |____|____|
        //
        //

        tileMap = new TileMap(2, 2);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        getRandomTileForMapPosition(tileMap, 0, 0, true);
    }

    @Test(expected=IllegalStateException.class)
    public void testGetRandomTileForMapPositionBorderException2() {
        TileMap tileMap;

        //---------------- TILE_1B
        //	    0    1
        //	  -----------     X --> WALL
        //	0 |    D    |     D --> DOOR
        //	  |    D____|     ? --> NOT SPECIFIED
        //	1 |XXXX|    |
        //	  |____|____|
        //
        //

        tileMap = new TileMap(2, 2);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_1D));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));

        getRandomTileForMapPosition(tileMap, 0, 0, true);
    }

    @Test(expected=IllegalStateException.class)
    public void testGetRandomTileForMapPositionBorderException3() {
        TileMap tileMap;

        //---------------- TILE_2LB
        //	    0    1
        //	  -----------     X --> WALL
        //	0 |    D    |     D --> DOOR
        //	  |    D____|     ? --> NOT SPECIFIED
        //	1 |DDDD|    |
        //	  |____|____|
        //
        //

        tileMap = new TileMap(2, 2);
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_1D));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_1A));

        getRandomTileForMapPosition(tileMap, 0, 0, false);
    }


    @Test
    public void testGetBorderTileDirectionTileType() {
        Assert.assertEquals(TileType.TILE_1D, getBorderTileDirectionTileType(0, 0, 1, 0));
        Assert.assertEquals(TileType.TILE_1B, getBorderTileDirectionTileType(1, 0, 0, 0));
        Assert.assertEquals(TileType.TILE_1A, getBorderTileDirectionTileType(0, 0, 0, 1));
        Assert.assertEquals(TileType.TILE_1C, getBorderTileDirectionTileType(0, 1, 0, 0));
        Assert.assertEquals(TileType.TILE_0, getBorderTileDirectionTileType(0, 0, 1, 1));
    }

    @Test
    @SuppressWarnings("serial")
    public void testGetClosestTileMapPosition() {
        List<TileMapPosition> tileMapPositions;

        tileMapPositions = new ArrayList<TileMapPosition>() {
            {
                add(new TileMapPosition(0, 0));
                add(new TileMapPosition(0, 1));
                add(new TileMapPosition(1, 0));
                add(new TileMapPosition(1, 1));
            }
        };
        Assert.assertEquals(new TileMapPosition(1, 1), getClosestTileMapPosition(tileMapPositions, 4, 4));


        tileMapPositions = new ArrayList<TileMapPosition>() {
            {
                add(new TileMapPosition(3, 0));
                add(new TileMapPosition(3, 1));
                add(new TileMapPosition(4, 0));
                add(new TileMapPosition(4, 1));
            }
        };
        Assert.assertEquals(new TileMapPosition(3, 1), getClosestTileMapPosition(tileMapPositions, 0, 4));


        tileMapPositions = new ArrayList<TileMapPosition>() {
            {
                add(new TileMapPosition(3, 3));
                add(new TileMapPosition(3, 4));
                add(new TileMapPosition(4, 3));
                add(new TileMapPosition(4, 4));
            }
        };
        Assert.assertEquals(new TileMapPosition(3, 3), getClosestTileMapPosition(tileMapPositions, 0, 0));


        tileMapPositions = new ArrayList<TileMapPosition>() {
            {
                add(new TileMapPosition(0, 3));
                add(new TileMapPosition(0, 4));
                add(new TileMapPosition(1, 3));
                add(new TileMapPosition(1, 4));
            }
        };
        Assert.assertEquals(new TileMapPosition(1, 3), getClosestTileMapPosition(tileMapPositions, 4, 0));
    }


    @Test
    public void testGetTailGroup() {
        TileMap tileMap;
        List<Tile> result;

        //---------------- FULL PATH
        //	    0    1    2
        //	  ----------------
        //	0 |  _ | ___| _  |
        //	  |_|__|__|_|__|_|
        //	1 | |_ | _|_| _| |
        //	  |_|__|__|_|__|_|
        //	2 | |_ | _|_| _| |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 0, new Tile(TileType.TILE_2LB));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_3A));
        tileMap.addTileAtPosition(0, 2, new Tile(TileType.TILE_2LA));
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_3B));
        tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_4));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_3D));
        tileMap.addTileAtPosition(2, 0, new Tile(TileType.TILE_2LC));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_3C));
        tileMap.addTileAtPosition(2, 2, new Tile(TileType.TILE_2LD));

        result = getTileGroup(new ArrayList<Tile>(), tileMap.getTileAtPosition(0, 0));
        Assert.assertEquals(9, result.size());
        Assert.assertTrue(result.contains(tileMap.getTileAtPosition(0, 0)));
        Assert.assertTrue(result.contains(tileMap.getTileAtPosition(0, 1)));
        Assert.assertTrue(result.contains(tileMap.getTileAtPosition(0, 2)));
        Assert.assertTrue(result.contains(tileMap.getTileAtPosition(1, 0)));
        Assert.assertTrue(result.contains(tileMap.getTileAtPosition(1, 1)));
        Assert.assertTrue(result.contains(tileMap.getTileAtPosition(1, 2)));
        Assert.assertTrue(result.contains(tileMap.getTileAtPosition(2, 0)));
        Assert.assertTrue(result.contains(tileMap.getTileAtPosition(2, 1)));
        Assert.assertTrue(result.contains(tileMap.getTileAtPosition(2, 2)));


        //---------------- EMPTY PATH 1
        //	    0    1    2
        //	  ----------------
        //	0 |    |    |    |
        //	  |____|____|____|
        //	1 |    |    |    |
        //	  |____|____|____|
        //	2 |    |    |    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);

        result = getTileGroup(new ArrayList<Tile>(), tileMap.getTileAtPosition(0, 0));
        Assert.assertEquals(0, result.size());


        //---------------- EMPTY PATH 2
        //	    0    1    2
        //	  ----------------
        //	0 |    |    |    |
        //	  |____|____|____|
        //	1 |    |    |    |
        //	  |____|____|____|
        //	2 |    |    |    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(2, 2, new Tile(TileType.TILE_0));

        result = getTileGroup(new ArrayList<Tile>(), tileMap.getTileAtPosition(0, 0));
        Assert.assertEquals(0, result.size());


        //---------------- EMPTY PATH 3
        //	    0    1    2
        //	  ----------------
        //	0 |    | _  |    |
        //	  |___ |__|_|____|
        //	1 |  __|    |    |
        //	  |____|____|____|
        //	2 |    |    |    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 0, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_1B));
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_2LC));

        result = getTileGroup(new ArrayList<Tile>(), tileMap.getTileAtPosition(0, 0));
        Assert.assertEquals(0, result.size());


        //---------------- EMPTY PATH 4
        //	    0    1    2
        //	  ----------------
        //	0 |    | _  |    |
        //	  |___ |__|_|____|
        //	1 |  __|    |    |
        //	  |____|____|____|
        //	2 |    |    |    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_1B));
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_2LC));

        result = getTileGroup(new ArrayList<Tile>(), tileMap.getTileAtPosition(0, 0));
        Assert.assertEquals(0, result.size());


        //---------------- RANDOM PATH
        //	    0    1    2
        //	  ----------------
        //	0 | __ | _  |    |
        //	  |___ |__|_|__|_|
        //	1 |    |  |_| _| |
        //	  |____|__|_|__|_|
        //	2 |  | |  |_| _| |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 0, new Tile(TileType.TILE_1B));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_0));
        tileMap.addTileAtPosition(0, 2, new Tile(TileType.TILE_1A));
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_2LC));
        tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_3A));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_2LA));
        tileMap.addTileAtPosition(2, 0, new Tile(TileType.TILE_1C));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_3C));
        tileMap.addTileAtPosition(2, 2, new Tile(TileType.TILE_2LD));

        result = getTileGroup(new ArrayList<Tile>(), tileMap.getTileAtPosition(0, 0));
        Assert.assertEquals(7, result.size());
        Assert.assertTrue(result.contains(tileMap.getTileAtPosition(0, 0)));
        Assert.assertTrue(result.contains(tileMap.getTileAtPosition(1, 0)));
        Assert.assertTrue(result.contains(tileMap.getTileAtPosition(1, 1)));
        Assert.assertTrue(result.contains(tileMap.getTileAtPosition(1, 2)));
        Assert.assertTrue(result.contains(tileMap.getTileAtPosition(2, 0)));
        Assert.assertTrue(result.contains(tileMap.getTileAtPosition(2, 1)));
        Assert.assertTrue(result.contains(tileMap.getTileAtPosition(2, 2)));
    }


    @Test
    public void testGetGroupsMinimalDistance() {
        TileMap tileMap;
        TileGroupsDistanceContainer result;

        //---------------- TEST 1
        //	    0    1    2
        //	  ----------------
        //	0 |  __|__  |    |
        //	  |____|__|_|____|
        //	1 |    |    |    |
        //	  |____|__ _|____|
        //	2 |    |  |_|__  |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 0, new Tile(TileType.TILE_1B));
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_2LC));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_2LA));
        tileMap.addTileAtPosition(2, 2, new Tile(TileType.TILE_1D));

        result = getGroupsMinimalDistance(getTileGroup(new ArrayList<Tile>(), tileMap.getTileAtPosition(0, 0)),
        								  getTileGroup(new ArrayList<Tile>(), tileMap.getTileAtPosition(2, 2)),
        								  null);

        Assert.assertEquals(2, result.getDistance());
        Assert.assertEquals(tileMap.getTileAtPosition(1, 0), result.getTile1());
        Assert.assertEquals(tileMap.getTileAtPosition(1, 2), result.getTile2());
        
        
        //---------------- TEST 1 - WITH BANNED POSITIONS
        //	    0    1    2
        //	  ----------------
        //	0 |  __|__  |    |
        //	  |____|__|_|____|
        //	1 |    |    |    |
        //	  |____|__ _|____|
        //	2 |    |  |_|__  |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 0, new Tile(TileType.TILE_1B));
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_2LC));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_2LA));
        tileMap.addTileAtPosition(2, 2, new Tile(TileType.TILE_1D));

        result = getGroupsMinimalDistance(getTileGroup(new ArrayList<Tile>(), tileMap.getTileAtPosition(0, 0)),
        								  getTileGroup(new ArrayList<Tile>(), tileMap.getTileAtPosition(2, 2)),
        								  Arrays.asList(new TileMapPosition[] {new TileMapPosition(1, 0)}));

        Assert.assertEquals(3, result.getDistance());
        Assert.assertEquals(tileMap.getTileAtPosition(0, 0), result.getTile1());
        Assert.assertEquals(tileMap.getTileAtPosition(1, 2), result.getTile2());


        //---------------- TEST 2
        //	    0    1    2
        //	  ----------------
        //	0 |    |    |    |
        //	  |__|_|____|_|__|
        //	1 |  | |   _|_|  |
        //	  |__|_|__|_|____|
        //	2 |  | |  | |    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 0, new Tile(TileType.TILE_1C));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_2IV));
        tileMap.addTileAtPosition(0, 2, new Tile(TileType.TILE_1A));
        tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_2LA));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_1A));
        tileMap.addTileAtPosition(2, 0, new Tile(TileType.TILE_1C));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_2LD));

        result = getGroupsMinimalDistance(getTileGroup(new ArrayList<Tile>(), tileMap.getTileAtPosition(0, 0)),
                						  getTileGroup(new ArrayList<Tile>(), tileMap.getTileAtPosition(2, 0)),
        								  null);

        Assert.assertEquals(1, result.getDistance());
        Assert.assertEquals(tileMap.getTileAtPosition(0, 1), result.getTile1());
        Assert.assertEquals(tileMap.getTileAtPosition(1, 1), result.getTile2());
        
        
        //---------------- TEST 2 - WITH BANNED POSITIONS
        //	    0    1    2
        //	  ----------------
        //	0 |    |    |    |
        //	  |__|_|____|_|__|
        //	1 |  | |   _|_|  |
        //	  |__|_|__|_|____|
        //	2 |  | |  | |    |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 0, new Tile(TileType.TILE_1C));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_2IV));
        tileMap.addTileAtPosition(0, 2, new Tile(TileType.TILE_1A));
        tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_2LA));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_1A));
        tileMap.addTileAtPosition(2, 0, new Tile(TileType.TILE_1C));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_2LD));

        result = getGroupsMinimalDistance(getTileGroup(new ArrayList<Tile>(), tileMap.getTileAtPosition(0, 0)),
                						  getTileGroup(new ArrayList<Tile>(), tileMap.getTileAtPosition(2, 0)),
                						  Arrays.asList(new TileMapPosition[] {new TileMapPosition(1, 1), new TileMapPosition(1, 2)}));

        Assert.assertEquals(2, result.getDistance());
        Assert.assertEquals(tileMap.getTileAtPosition(0, 0), result.getTile1());
        Assert.assertEquals(tileMap.getTileAtPosition(2, 0), result.getTile2());
    }


    @Test
    public void testCalculateTileDistances() {
        TileMap tileMap;

        //---------------- TEST 1
        //	    0    1    2
        //	  ----------------
        //	0 |  __|__  |    |
        //	  |____|__|_|____|
        //	1 |    |  | |    |
        //	  |____|__|_|____|
        //	2 |    |  |_|__  |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 0, new Tile(TileType.TILE_1B));
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_2LC));
        tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_2IV));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_2LA));
        tileMap.addTileAtPosition(2, 2, new Tile(TileType.TILE_1D));

        calculateTileDistances(tileMap.getTileAtPosition(0, 0), 0);

        Assert.assertEquals(0, tileMap.getTileAtPosition(0, 0).getDistance());
        Assert.assertEquals(1, tileMap.getTileAtPosition(1, 0).getDistance());
        Assert.assertEquals(2, tileMap.getTileAtPosition(1, 1).getDistance());
        Assert.assertEquals(3, tileMap.getTileAtPosition(1, 2).getDistance());
        Assert.assertEquals(4, tileMap.getTileAtPosition(2, 2).getDistance());


        //---------------- TEST 2
        //	    0    1    2
        //	  ----------------
        //	0 |    |   _|__  |
        //	  |____|__|_|__|_|
        //	1 |    |  | |  | |
        //	  |____|__|_|__|_|
        //	2 |  __|__|_|__| |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 2, new Tile(TileType.TILE_1B));
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_2LB));
        tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_2IV));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_3D));
        tileMap.addTileAtPosition(2, 0, new Tile(TileType.TILE_2LC));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_2IV));
        tileMap.addTileAtPosition(2, 2, new Tile(TileType.TILE_2LD));

        calculateTileDistances(tileMap.getTileAtPosition(0, 2), 0);

        Assert.assertEquals(0, tileMap.getTileAtPosition(0, 2).getDistance());
        Assert.assertEquals(3, tileMap.getTileAtPosition(1, 0).getDistance());
        Assert.assertEquals(2, tileMap.getTileAtPosition(1, 1).getDistance());
        Assert.assertEquals(1, tileMap.getTileAtPosition(1, 2).getDistance());
        Assert.assertEquals(4, tileMap.getTileAtPosition(2, 0).getDistance());
        Assert.assertEquals(3, tileMap.getTileAtPosition(2, 1).getDistance());
        Assert.assertEquals(2, tileMap.getTileAtPosition(2, 2).getDistance());


        //---------------- TEST 3
        //	    0    1    2
        //	  ----------------
        //	0 |   _|____|__  |
        //	  |__|_|____|__|_|
        //	1 |  |_|__  |  | |
        //	  |____|__|_|__|_|
        //	2 |  __|__|_|__| |
        //	  |____|____|____|

        tileMap = new TileMap(3, 3);
        tileMap.addTileAtPosition(0, 0, new Tile(TileType.TILE_2LB));
        tileMap.addTileAtPosition(0, 1, new Tile(TileType.TILE_2LA));
        tileMap.addTileAtPosition(0, 2, new Tile(TileType.TILE_1B));
        tileMap.addTileAtPosition(1, 0, new Tile(TileType.TILE_2IH));
        tileMap.addTileAtPosition(1, 1, new Tile(TileType.TILE_2LC));
        tileMap.addTileAtPosition(1, 2, new Tile(TileType.TILE_3D));
        tileMap.addTileAtPosition(2, 0, new Tile(TileType.TILE_2LC));
        tileMap.addTileAtPosition(2, 1, new Tile(TileType.TILE_2IV));
        tileMap.addTileAtPosition(2, 2, new Tile(TileType.TILE_2LD));

        calculateTileDistances(tileMap.getTileAtPosition(0, 2), 0);

        Assert.assertEquals(4, tileMap.getTileAtPosition(0, 0).getDistance());
        Assert.assertEquals(3, tileMap.getTileAtPosition(0, 1).getDistance());
        Assert.assertEquals(0, tileMap.getTileAtPosition(0, 2).getDistance());
        Assert.assertEquals(5, tileMap.getTileAtPosition(1, 0).getDistance());
        Assert.assertEquals(2, tileMap.getTileAtPosition(1, 1).getDistance());
        Assert.assertEquals(1, tileMap.getTileAtPosition(1, 2).getDistance());
        Assert.assertEquals(4, tileMap.getTileAtPosition(2, 0).getDistance());
        Assert.assertEquals(3, tileMap.getTileAtPosition(2, 1).getDistance());
        Assert.assertEquals(2, tileMap.getTileAtPosition(2, 2).getDistance());
    }


}
