
package cz.nic.tablexia.widget;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import cz.nic.tablexia.R;

/*
 * Dialog, ktery krome tlacitek umoznuje pridat i vlastni view
 */
public abstract class TablexiaViewDialog extends TablexiaDialog {

    @Override
    protected int getLayourId() {
        return R.layout.tablexiadialog_view;
    }

    protected View getCustomView(LayoutInflater inflater) {
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) super.onCreateView(inflater, container, savedInstanceState);
        View customView = getCustomView(inflater);
        if (customView != null) {
            view.addView(customView);
        }
        return view;
    }
}
