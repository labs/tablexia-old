/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.widget;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import cz.nic.tablexia.R;

public abstract class TablexiaBannerImageDialog extends TablexiaBannerDialog {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    @Override
    protected int getLayourId() {
        return R.layout.tablexiadialog;
    }

    @Override
    protected void prepareDialogContent(LayoutInflater inflater, ViewGroup contentView) {
        addBannerView(inflater, contentView);
        addImageView(inflater, contentView);
        addTextView(inflater, contentView);
    }

    protected void addImageView(LayoutInflater inflater, ViewGroup contentView) {
        int imageResource = getImageResource();
        if (imageResource > 0) {
            ImageView imageView = (ImageView) inflater.inflate(R.layout.tablexiadialog_image, contentView, false);
            imageView.setImageResource(imageResource);
            imageView.setScaleType(ScaleType.CENTER_INSIDE);
            imageView.setAdjustViewBounds(true);
            contentView.addView(imageView);
        }
    }

    protected abstract int getImageResource();

}
