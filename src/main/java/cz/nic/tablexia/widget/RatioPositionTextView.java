/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;
import cz.nic.tablexia.R;

/**
 * TextView for showing text positioned relatively to display size.
 * 
 * @author Matyáš Latner
 */
public class RatioPositionTextView extends TextView {

    boolean               useHeight = false;
    private float         xPositionRatio;
    private float         yPositionRatio;
    private final Context context;

    public RatioPositionTextView(final Context context, final AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.cz_nic_tablexia_widget_RatioPositionImageView, 0, 0);

        try {
            xPositionRatio = a.getFloat(R.styleable.cz_nic_tablexia_widget_RatioPositionImageView_xPositionRatio, 1);
            yPositionRatio = a.getFloat(R.styleable.cz_nic_tablexia_widget_RatioPositionImageView_yPositionRatio, 1);
            useHeight = a.getFloat(R.styleable.cz_nic_tablexia_widget_RatioPositionImageView_displayRatioType, 1) == 1 ? true : false;
        } finally {
            a.recycle();
        }
        this.context = context;
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        setPosition();
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void setPosition() {
        setX(context.getResources().getDisplayMetrics().widthPixels * xPositionRatio * RatioPositionSizeImageView.X_SCALE_CONSTANT);
        setY(context.getResources().getDisplayMetrics().heightPixels * yPositionRatio);

    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        setPosition();
    }

}
