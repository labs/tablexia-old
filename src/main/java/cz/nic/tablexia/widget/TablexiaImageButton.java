/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.widget;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.audio.SoundControl;
import cz.nic.tablexia.audio.resources.PermanentSounds;

/**
 * Image button with tablexia specific behavior
 * 
 * @author Matyáš Latner
 */
public class TablexiaImageButton extends ImageButton {

    public enum ClickSoundEventType {
        ON_CLICK, ON_SELECT;
    }

    private ClickSoundEventType   clickSoundEventType = ClickSoundEventType.ON_CLICK;

    private List<OnClickListener> listeners;
    private SoundControl          soundControl;

    public TablexiaImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TablexiaImageButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TablexiaImageButton(Context context) {
        super(context);
        init();
    }

    public void setClickSoundEventType(ClickSoundEventType clickSoundEventType) {
        this.clickSoundEventType = clickSoundEventType;
    }

    public ClickSoundEventType getClickSoundEventType() {
        return clickSoundEventType;
    }

    private void init() {
        if (!isInEditMode()) {
            listeners = new ArrayList<OnClickListener>();
            soundControl = ((Tablexia) getContext().getApplicationContext()).getGlobalSoundControl();
            super.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View view) {
                    if (clickSoundEventType == ClickSoundEventType.ON_CLICK) {
                        soundControl.playSound(PermanentSounds.BUTTON_SOUND, false);
                    }
                    for (OnClickListener listener : listeners) {
                        listener.onClick(view);
                    }
                }
            });
        }
    }

    @Override
    public void setOnClickListener(OnClickListener listener) {
        listeners.add(listener);
    }

    public void removeOnClickListener(OnClickListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void setSelected(boolean selected) {
        if ((clickSoundEventType == ClickSoundEventType.ON_SELECT) && (selected != isSelected()) && isShown()) {
            soundControl.playSound(PermanentSounds.BUTTON_SOUND, false);
        }
        super.setSelected(selected);
    }

}
