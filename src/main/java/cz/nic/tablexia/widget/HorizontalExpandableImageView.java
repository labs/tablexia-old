/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * ImageView for showing image horizontally expanded keeping image aspect ratio.
 * 
 * @author Matyáš Latner
 *
 */
public class HorizontalExpandableImageView extends ImageView {

    public HorizontalExpandableImageView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }
    
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // TODO scale down image size to maximum system allowed image size
        // System.out.println("MAX BITMAP WIDTH: " + canvas.getMaximumBitmapWidth());
        // System.out.println("MAX BITMAP HEIGHT: " + canvas.getMaximumBitmapHeight());
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        final Drawable drawable = this.getDrawable();

        if (drawable != null) {
            final int height = MeasureSpec.getSize(heightMeasureSpec);
            final int width = (int) Math.ceil(height * (float) drawable.getIntrinsicWidth() / drawable.getIntrinsicHeight());
            this.setMeasuredDimension(width, height);
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
    
}
