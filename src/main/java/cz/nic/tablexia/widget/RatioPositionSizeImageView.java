/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;
import cz.nic.tablexia.R;

/**
 * ImageView for showing image positioned and sized relatively to display size.
 * 
 * @author Matyáš Latner
 */
public class RatioPositionSizeImageView extends ImageView {

    public static float X_SCALE_CONSTANT = 3;

    private float       displaySizeRatio;
    boolean             useHeight        = false;
    private float       xPositionRatio;
    private float       yPositionRatio;
    private Context     context;

    public RatioPositionSizeImageView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.cz_nic_tablexia_widget_RatioPositionImageView, 0, 0);

        try {
            xPositionRatio = a.getFloat(R.styleable.cz_nic_tablexia_widget_RatioPositionImageView_xPositionRatio, 1);
            yPositionRatio = a.getFloat(R.styleable.cz_nic_tablexia_widget_RatioPositionImageView_yPositionRatio, 1);
            displaySizeRatio = a.getFloat(R.styleable.cz_nic_tablexia_widget_RatioPositionImageView_displaySizeRatio, 1);
            useHeight = a.getFloat(R.styleable.cz_nic_tablexia_widget_RatioPositionImageView_displayRatioType, 1) == 1 ? true : false;
        } finally {
            a.recycle();
        }

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        final Drawable drawable = getDrawable();

        int displayWidth = context.getResources().getDisplayMetrics().widthPixels;
        int displayHeight = context.getResources().getDisplayMetrics().heightPixels;

        setPosition();

        if (drawable != null) {

            int width;
            int height;

            if (useHeight) {
                height = (int) (displayHeight * displaySizeRatio);
                width = (int) Math.ceil((height * (float) drawable.getIntrinsicWidth()) / drawable.getIntrinsicHeight());
            } else {
                width = (int) (displayWidth * displaySizeRatio);
                height = (int) Math.ceil((width * (float) drawable.getIntrinsicHeight()) / drawable.getIntrinsicWidth());
            }
            setMeasuredDimension(width, height);

        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        setPosition();
    }

    private void setPosition() {
        setX(getRatioX());
        setY(getRatioY());
    }

    public float getRatioX() {
        return context.getResources().getDisplayMetrics().widthPixels * xPositionRatio * X_SCALE_CONSTANT;
    }

    public float getRatioY() {
        return context.getResources().getDisplayMetrics().heightPixels * yPositionRatio;
    }

}
