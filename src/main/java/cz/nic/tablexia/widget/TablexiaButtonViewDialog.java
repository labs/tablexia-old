
package cz.nic.tablexia.widget;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import cz.nic.tablexia.R;

public abstract class TablexiaButtonViewDialog extends TablexiaViewDialog {
    private static final int BUTTONS_LAYOUT_ID = R.layout.tablexiadialog_include_buttons;

    TextView                 negativeButton;

    TextView                 positiveButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
    }

    protected int getQuestionButtonLayoutId() {
        return BUTTONS_LAYOUT_ID;
    }

    protected View getQuestionButtonsView(LayoutInflater inflater) {
        View imageButtons = inflater.inflate(getQuestionButtonLayoutId(), null);
        // contentView.addView(imageButtons);
        return imageButtons;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        View buttonsView = getQuestionButtonsView(inflater);
        if (buttonsView != null) {
            ((ViewGroup) view).addView(buttonsView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        negativeButton = (TextView) view.findViewById(R.id.tablexiadialog_button_negative);
        positiveButton = (TextView) view.findViewById(R.id.tablexiadialog_button_positive);

        super.onViewCreated(view, savedInstanceState);
        if (negativeButton != null) {
            negativeButton.setBackgroundResource(getNegativeButton());
            negativeButton.setText(getNegativeButtonText());
            negativeButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    negativeAction();
                }

            });
        }

        if (positiveButton != null) {
            positiveButton.setBackgroundResource(getPositiveButton());
            positiveButton.setText(getPositiveButtonText());
            positiveButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    positiveAction();
                }

            });
        }
    }

    protected int getPositiveButton() {
        return R.drawable.button_positiveaction;
    }

    protected int getNegativeButton() {
        return R.drawable.button_negativeaction;
    }

    protected int getPositiveButtonText() {
        return R.string.dialog_question_yes;
    }

    protected int getNegativeButtonText() {
        return R.string.dialog_question_no;
    }

    public void positiveAction() {
        dismiss();
    }

    public void negativeAction() {
        dismiss();
    }

}
