/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.widget;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import cz.nic.tablexia.R;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.audio.SoundControl;
import cz.nic.tablexia.audio.resources.SfxSounds;

public abstract class TablexiaDialog extends DialogFragment {

    private SoundControl soundControll;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(true);
        soundControll = ((Tablexia) getActivity().getApplicationContext()).getGlobalSoundControl();
        soundControll.addSound(SfxSounds.DIALOG_ALERT);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    protected int getLayourId() {
        return R.layout.tablexiadialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        soundControll.playSound(SfxSounds.DIALOG_ALERT, false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        soundControll.stopSound(SfxSounds.DIALOG_ALERT, true, false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayourId(), null);

        View backgroundView = view.findViewById(R.id.tablexiadialog_content);
        if (backgroundView != null) {
            backgroundView.setBackgroundResource(getBackgroundResource());
        }

        prepareDialogContent(inflater, (ViewGroup) view.findViewById(R.id.tablexiadialog_content));

        view.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onDialogClick(v);
            }
        });

        return view;
    }

    protected void prepareDialogContent(LayoutInflater inflater, ViewGroup contenView) {
        if (contenView != null) {
            addTextView(inflater, contenView);
        }
        /*
         * FIX proč připravujeme textView, pro který ani nevíme text, pro někoho kdo to chce rozšířit je to nečekané chování
         * Buď to má mít abstraktní funkce, které mě donutí to implementovat (getTextResource, getTextString), nebo to dělá na pozadí magii.
         */

    }

    protected void addTextView(LayoutInflater inflater, ViewGroup contentView) {
        TextView textView = (TextView) inflater.inflate(R.layout.tablexiadialog_text, contentView, false);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, getTextSize());
        if (textView != null) {

            int textResource = getTextResource();
            String textString = getTextString();
            CharSequence textCharSequence = getTextCharSequence();

            if (textResource > 0) {
                textView.setText(textResource);
            } else if (textString != null) {
                textView.setText(textResource);
            } else if (textCharSequence != null) {
                textView.setText(textCharSequence);
            }
        }
        contentView.addView(textView);
    }

    protected void onDialogClick(View v) {
        // nothing needed
    }

    protected int getTextSize() {
        return 20;
    }

    protected int getBackgroundResource() {
        return R.drawable.dialog_background;
    }

    protected CharSequence getTextCharSequence() {
        return null;
    }

    protected String getTextString() {
        return null;
    }

    protected int getTextResource() {
        return -1;
    }

}
