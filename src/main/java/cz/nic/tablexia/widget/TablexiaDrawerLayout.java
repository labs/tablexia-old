/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.widget;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.audio.SoundControl;
import cz.nic.tablexia.audio.resources.PermanentSounds;

/**
 * Custom drawer layout with multiple listeners support
 * 
 * @author Matyáš Latner
 */
public class TablexiaDrawerLayout extends DrawerLayout implements DrawerListener {
    
    public static class DrawerListenerAdapter implements DrawerListener {

        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {
            // adapter method
        }

        @Override
        public void onDrawerOpened(View drawerView) {
            // adapter method
        }

        @Override
        public void onDrawerClosed(View drawerView) {
            // adapter method
        }

        @Override
        public void onDrawerStateChanged(int newState) {
            // adapter method
        }
        
    }

    public static final int      POSITION       = Gravity.LEFT;
    
    private List<DrawerListener> listeners;
    private SoundControl soundControll;

    private boolean isOpenSoundPlay;
    private boolean isOpening;
    private float   lastDrawerLayoutXPosition;

    public TablexiaDrawerLayout(Context context) {
        super(context);
        init();
    }

    public TablexiaDrawerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TablexiaDrawerLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    
    private void init() {
        prepareSoundControl();
        setClosed();
        listeners = new ArrayList<DrawerListener>();
        setDrawerListener(this);
    }

    /* //////////////////////////////////////////// MULTIPLE DRAWER LISTENERS SUPPORT IMPLEMENTATION */

    /**
     * Adds drawer listener
     * 
     * @param listener new drawer listener
     */
    public void addDrawerListener(DrawerListener listener) {
        listeners.add(listener);
    }

    /**
     * Removes drawer listener
     * 
     * @param listener drawer listener to remove from
     */
    public void removeDrawerListener(DrawerListener listener) {
        listeners.remove(listener);
    }
    
    /* //////////////////////////////////////////// STATE CONTROL */
    
    private void setOpened() {
        isOpening = true;
        isOpenSoundPlay = true;
    }
    
    private void setClosed() {
        isOpening = false;
        isOpenSoundPlay = false;
    }
    
    @Override
    public void setDrawerLockMode(int lockMode) {
        super.setDrawerLockMode(lockMode);
        switch (lockMode) {
            case DrawerLayout.LOCK_MODE_LOCKED_OPEN:
                setOpened();
                break;

            case DrawerLayout.LOCK_MODE_LOCKED_CLOSED:
                setClosed();
                break;
        }
    }
    
    private void setDrawerOpeningState(float drawerLayoutX) {
        if (drawerLayoutX > lastDrawerLayoutXPosition) {
            if (!isOpening && !isOpenSoundPlay) {
                playOpenSound();
                isOpenSoundPlay = true;
            }
            isOpening = true;
        } else {
            if (isOpening && isOpenSoundPlay) {
                playCloseSound();
                isOpenSoundPlay = false;
            }
            isOpening = false;
        }
        lastDrawerLayoutXPosition = drawerLayoutX;
    }
    
    /* //////////////////////////////////////////// SOUNDS */
    
    private void prepareSoundControl() {
        soundControll = ((Tablexia) getContext().getApplicationContext()).getGlobalSoundControl();
    }
    
    private void playOpenSound() {
        soundControll.playSound(PermanentSounds.MENU_OPEN_SOUND, false);
    }
    
    private void playCloseSound() {
        soundControll.playSound(PermanentSounds.MENU_CLOSE_SOUND, false);
    }

    /* //////////////////////////////////////////// DRAWER LISTENER IMPLEMENTATION */

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
        synchronized (listeners) {            
            setDrawerOpeningState(drawerView.getX());
            for (DrawerListener listener : listeners) {
                listener.onDrawerSlide(drawerView, slideOffset);
            }
        }
    }

    @Override
    public void onDrawerOpened(View drawerView) {
        synchronized (listeners) {
            isOpening = true;
            for (DrawerListener listener : listeners) {
                listener.onDrawerOpened(drawerView);
            }
        }
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        synchronized (listeners) {            
            isOpening = false;
            for (DrawerListener listener : listeners) {
                listener.onDrawerClosed(drawerView);
            }
        }
    }

    @Override
    public synchronized void onDrawerStateChanged(int newState) {
        synchronized (listeners) {            
            for (DrawerListener listener : listeners) {
                listener.onDrawerStateChanged(newState);
            }
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if ((getDrawerLockMode(POSITION) == LOCK_MODE_LOCKED_OPEN)) {
                return false;
            }
        }
        return super.onKeyUp(keyCode, event);
    }
}
