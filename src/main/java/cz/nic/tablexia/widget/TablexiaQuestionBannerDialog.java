/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.widget;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import cz.nic.tablexia.R;

public abstract class TablexiaQuestionBannerDialog extends TablexiaBannerDialog {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(true);
    }

    @Override
    protected void prepareDialogContent(LayoutInflater inflater, ViewGroup contentView) {
        super.prepareDialogContent(inflater, contentView);
        addQuestionButtonsView(inflater, contentView);
    }

    protected void addQuestionButtonsView(LayoutInflater inflater, ViewGroup contentView) {
        ViewGroup imageButtons = (ViewGroup) inflater.inflate(R.layout.tablexiadialog_questionbuttons, contentView, false);

        final TextView negativeButton = (TextView) imageButtons.findViewById(getNegativeButton());
        if (negativeButton != null) {
            negativeButton.setText(getResources().getString(getNegativeButtonText()));
            negativeButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    negativeButton.setSelected(true);
                    negativeAction();
                }

            });
        }

        final TextView positiveButton = (TextView) imageButtons.findViewById(getPositiveButton());
        if (positiveButton != null) {
            positiveButton.setText(getResources().getString(getPositiveButtonText()));
            positiveButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    positiveButton.setSelected(true);
                    positiveAction();
                }

            });
        }

        contentView.addView(imageButtons);
    }

    protected int getPositiveButton() {
        return R.id.tablexiadialog_button_positive;
    }

    protected int getNegativeButton() {
        return R.id.tablexiadialog_button_negative;
    }

    protected int getPositiveButtonText(){
        return R.string.dialog_question_yes;
    }

    protected int getNegativeButtonText(){
        return R.string.dialog_question_no;
    }

    @Override
    protected int getTextResource() {
        return R.string.dialog_question;
    }

    public void positiveAction() {
        dismiss();
    }

    public void negativeAction() {
        dismiss();
    }
}
