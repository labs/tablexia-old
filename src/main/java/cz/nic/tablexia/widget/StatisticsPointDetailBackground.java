/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

/**
 * View with background image for statistics point detail
 * 
 * @author Matyáš Latner
 *
 */
public class StatisticsPointDetailBackground extends View {

    public StatisticsPointDetailBackground(Context context) {
        super(context);
    }

    public StatisticsPointDetailBackground(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StatisticsPointDetailBackground(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        
        float middle = getWidth() / 2;
        float arrowHalfWidth = getWidth() / 15;
        float arrowHeight = getHeight() / 7;
        float bottomOffset = getHeight() / 9;
        float bottomBorder = getHeight() - bottomOffset;
        
        Paint paint = new Paint();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2);
        paint.setColor(Color.BLACK);
        
        canvas.drawCircle(middle, getHeight() - bottomOffset, 11, paint);
        
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        drawBackground(canvas, paint, middle, bottomBorder, arrowHeight, arrowHalfWidth);
        
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(1);
        paint.setColor(Color.BLACK);
        drawBackground(canvas, paint, middle, bottomBorder, arrowHeight, arrowHalfWidth);

        super.onDraw(canvas);
    }
    
    private void drawBackground(Canvas canvas, Paint paint, float middle, float bottomBorder, float arrowHeight, float arrowHalfWidth) {
        Path path = new Path();
        path.moveTo(0, 0);
        path.lineTo(getWidth() - 1, 0);
        path.lineTo(getWidth() - 1, bottomBorder - arrowHeight);
        path.lineTo(middle + arrowHalfWidth, bottomBorder - arrowHeight);
        path.lineTo(middle, bottomBorder);
        path.lineTo(middle - arrowHalfWidth, bottomBorder - arrowHeight);
        path.lineTo(0, bottomBorder - arrowHeight);

        path.close();
        canvas.drawPath(path, paint);
    }

}
