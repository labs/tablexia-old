/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.widget;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import cz.nic.tablexia.R;

public abstract class TablexiaBannerDialog extends TablexiaDialog {
    
    @Override
    protected void prepareDialogContent(LayoutInflater inflater, ViewGroup contentView) {
        addBannerView(inflater, contentView);
        super.prepareDialogContent(inflater, contentView);
    }
    
    protected void addBannerView(LayoutInflater inflater, ViewGroup contentView) {
        int imageResource = getBannerResource();
        if (imageResource > 0) {            
            ImageView imageView = (ImageView) inflater.inflate(R.layout.tablexiadialog_image, contentView, false);
            imageView.setImageResource(imageResource);
            contentView.addView(imageView);
        }
    }
    
    protected abstract int getBannerResource();
    
}
