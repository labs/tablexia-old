/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.widget;

import android.content.Context;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import cz.nic.tablexia.R;

/**
 * Right menu border moving with menu.
 * 
 * @author Matyáš Latner
 */
public class MenuBorder extends RelativeLayout implements DrawerListener {

    private TablexiaDrawerLayout    drawerLayout;

    public MenuBorder(Context context) {
        super(context);
    }

    public MenuBorder(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MenuBorder(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onAttachedToWindow() {
        drawerLayout = (TablexiaDrawerLayout) ((RelativeLayout) getParent()).findViewById(R.id.drawer_layout);
        drawerLayout.addDrawerListener(this);

        // place the menu border to proper place
        if(drawerLayout.isDrawerOpen(TablexiaDrawerLayout.POSITION)) {
            setX(getResources().getDimension(R.dimen.mainmenu_width));
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        drawerLayout.removeDrawerListener(this);
    }

    /* //////////////////////////////////////////// DRAWER LISTENER IMPLEMENTATION */

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
        setX(drawerView.getWidth() * slideOffset);
    }

    @Override
    public void onDrawerOpened(View drawerView) {
        // nothing needed
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        // nothing needed
    }

    @Override
    public void onDrawerStateChanged(int newState) {
        // nothing needed
    }

}
