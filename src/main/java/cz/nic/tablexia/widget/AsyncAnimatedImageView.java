/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.widget;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

/**
 * @author lhoracek
 */
public class AsyncAnimatedImageView extends ImageView {

    private static final String  TAG           = AsyncAnimatedImageView.class.getSimpleName();
    private boolean              running       = false;
    private static final boolean RUN_ON_ATTACH = true;                                        // always start animating on attach

    public AsyncAnimatedImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public AsyncAnimatedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AsyncAnimatedImageView(Context context) {
        super(context);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (running || RUN_ON_ATTACH) {
            start();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (running) {
            stop();
        }
    }

    public void loadImageFromResource() {
        new LoadImage(this).execute();
    }

    class LoadImage extends AsyncTask<Void, Void, AnimationDrawable> {

        private ImageView imv;
        private int       resId;

        public LoadImage(ImageView imv) {
            this.imv = imv;
            try {
                resId = Integer.parseInt(imv.getTag().toString().substring(1));
            } catch (Exception e) {
                // nic
            }
        }

        @Override
        protected AnimationDrawable doInBackground(Void... params) {
            AnimationDrawable result = (AnimationDrawable) getContext().getResources().getDrawable(resId);

            for (int i = 0; i < result.getNumberOfFrames(); i++) {
                ((BitmapDrawable) result.getFrame(i)).getBitmap();
            }
            return result;
        }

        @Override
        protected void onPostExecute(final AnimationDrawable result) {
            if (!running) {
                // The image is not same.
                Log.e(TAG, "Image already stopped");
                return;
            }
            imv.setImageDrawable(result);
            post(new Runnable() {
                @Override
                public void run() {
                    result.start();
                }
            });
        }
    }

    private Drawable staticDrawable;

    public void start() {
        if (!running) {
            staticDrawable = getDrawable();
            loadImageFromResource();
            running = true;
        }

    }

    public void stop() {
        if (running) {
            setImageDrawable(staticDrawable);
            running = false;
        }
    }
}
