/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.widget;

import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import cz.nic.tablexia.R;

/**
 * Button for toggle menu state.
 * 
 * @author Matyáš Latner
 */
public class MenuToggleButton extends TablexiaImageButton implements OnClickListener {

    private TablexiaDrawerLayout    drawerLayout;
    private RelativeLayout          parentView;
    private float                   startX;
    private boolean                 isDragged = false;

    public MenuToggleButton(Context context) {
        super(context);
    }

    public MenuToggleButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MenuToggleButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onAttachedToWindow() {
        parentView = (RelativeLayout) getParent().getParent();
        drawerLayout = (TablexiaDrawerLayout) parentView.findViewById(R.id.drawer_layout);
        setOnClickListener(this);
        setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if ((event.getAction() == MotionEvent.ACTION_DOWN) && !isDragged){
                    startX = event.getX();
                    isDragged = true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    isDragged = false;
                }

                event.setLocation((int)(drawerLayout.getX() + (event.getRawX() - startX)), event.getY());
                drawerLayout.onTouchEvent(event);
                drawerLayout.onInterceptTouchEvent(event);

                return false;
            }
        });
    }

    /* //////////////////////////////////////////// BUTTON CLICK LISTENER IMPLEMENTATION */

    @Override
    public void onClick(View v) {
        View menuContent = parentView.findViewById(R.id.main_menu_container);
        if (drawerLayout.getDrawerLockMode(menuContent) == DrawerLayout.LOCK_MODE_UNLOCKED) {
            if (drawerLayout.isDrawerOpen(menuContent)) {
                drawerLayout.closeDrawer(menuContent);
            } else {
                drawerLayout.openDrawer(menuContent);
            }
        }
    }

}
