/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.manager;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.activeandroid.util.Log;
import com.google.gson.annotations.Expose;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.GamesDefinition;
import cz.nic.tablexia.menu.usermenu.User;

/**
 * Game state container
 *
 * @author Matyáš Latner
 */
@Table(name = "GameManagers")
public class GameManager extends Model {

    // TODO ukladat:
    // celkova doba hrani v aplikaci
    // celkova doba hrani ve hre
    // pocet nastev sine slavy
    // doba stravena v encyklopedii

    protected static final String                     GAMEMANAGER_LOG_PREFIX = "[GAMEMANAGER] ";

    @Column(name = "User") private User               user;

    @Expose @Column(name = "StartTime") private long  startTime;

    @Expose @Column(name = "EndTime") private long    endTime;

    @Expose @Column(name = "Difficulty") private int  difficulty;

    @Expose @Column(name = "Counter") private double  counter;

    @Expose @Column(name = "StarsNumber") private int starsNumber;

    @Expose @Column(name = "ExtraInt1") private int   extraInt1;

    @Expose @Column(name = "ExtraInt2") private int   extraInt2;

    @Expose @Column(name = "ExtraInt3") private int   extraInt3;

    @Expose @Column(name = "ExtraInt4") private int   extraInt4;

    @Expose @Column(name = "Game") private int        game;

    @Expose private List<GamePause>                   gamePauses;

    @Column(name = "Exported") private boolean        exported;

    public GameManager() {
        super();
        startTime = 0;
        endTime = 0;
    }

    public GameManager(GamesDefinition game, User user, GameDifficulty difficulty) {
        this();
        this.game = game.ordinal();
        this.user = user;
        this.difficulty = difficulty.ordinal();
        save();
    }

    public void start() {
        Log.d(GAMEMANAGER_LOG_PREFIX + " START recorded");
        startTime = System.currentTimeMillis();
        save();
    }

    public void stop(int starsNumber) {
        Log.d(GAMEMANAGER_LOG_PREFIX + " END recorded");
        endTime = System.currentTimeMillis();
        this.starsNumber = starsNumber;
        save();
    }

    public void pause() {
        if (endTime == 0) {
            Log.d(GAMEMANAGER_LOG_PREFIX + " PAUSE recorded");
            new GamePause(this, System.currentTimeMillis()).save();
        }
    }

    public void resume() {
        if (endTime == 0) {
            Log.d(GAMEMANAGER_LOG_PREFIX + " RESUME recorded");
            GamePause pause = new Select().from(GamePause.class).where("GameManager = ?", getId()).orderBy("Id DESC").executeSingle();
            if (pause != null) {
                pause.setEndTime(System.currentTimeMillis());
                pause.save();
            }
        }
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setCounter(double counter) {
        this.counter = counter;
    }

    public void setCounterAndSave(double counter) {
        setCounter(counter);
        save();
    }

    public double getCounter() {
        return counter;
    }

    public void incrementCounter() {
        counter++;
        save();
    }

    public int getStarsNumber() {
        return starsNumber;
    }

    public void setExtraInt1(int extraInt1) {
        this.extraInt1 = extraInt1;
    }

    public void setExtraInt1AndSave(int extraInt1) {
        setExtraInt1(extraInt1);
        save();
    }

    public int getExtraInt1() {
        return extraInt1;
    }

    public void setExtraInt2(int extraInt2) {
        this.extraInt2 = extraInt2;
    }

    public void setExtraInt2AndSave(int extraInt2) {
        setExtraInt2(extraInt2);
        save();
    }

    public int getExtraInt2() {
        return extraInt2;
    }

    public void setExtraInt3(int extraInt3) {
        this.extraInt3 = extraInt3;
    }

    public void setExtraInt3AndSave(int extraInt3) {
        setExtraInt3(extraInt3);
        save();
    }

    public int getExtraInt3() {
        return extraInt3;
    }

    public void setExtraInt4(int extraInt4) {
        this.extraInt4 = extraInt4;
    }

    public void setExtraInt4AndSave(int extraInt4) {
        setExtraInt4(extraInt4);
        save();
    }

    public int getExtraInt4() {
        return extraInt4;
    }

    public void setGame(int game) {
        this.game = game;
    }

    public void setGameEnumAndSave(GamesDefinition gamesDefinition) {
        game = gamesDefinition.ordinal();
        save();
    }

    public int getGame() {
        return game;
    }

    public void setGamePauses(List<GamePause> gamePauses) {
        this.gamePauses = gamePauses;
    }

    public List<GamePause> getGamePauses() {
        return gamePauses;
    }

    public void setExported(boolean exported) {
        this.exported = exported;
    }

    public void setExportedAndSave(boolean exported) {
        setExported(true);
        save();
    }

    public boolean isExported() {
        return exported;
    }

    public GamesDefinition getGameEnum() {
        return GamesDefinition.getGames()[game];
    }

    public GameDifficulty getDifficultyEnum() {
        return GameDifficulty.values()[difficulty];
    }

    public void loadGamePauses() {
        List<GamePause> result = new Select().from(GamePause.class).where("GameManager = ?", getId()).execute();
        setGamePauses(result);
    }

    public static void prinatAllData() {
        List<GameManager> gameRecords = new Select().from(GameManager.class).execute();
        System.out.println("Game Records: " + gameRecords.size());
        for (GameManager gameRecord : gameRecords) {
            System.out.println(gameRecord);
            System.out.println(gameRecord.getGamePausesInString());
        }
    }

    @Override
    public String toString() {
        String description;
        description = "GameManager[" + getId() + "] " + GamesDefinition.getGames()[game];
        description = description + " USER[" + user + "]";
        description = description + " D[" + String.valueOf(GameDifficulty.values()[difficulty]) + "]";
        description = description + " STime[" + startTime + "]";
        description = description + " ETime[" + endTime + "]";
        description = description + " Counter[" + counter + "]";
        description = description + " ExtraInt1[" + extraInt1 + "]";
        description = description + " ExtraInt2[" + extraInt2 + "]";
        description = description + " ExtraInt3[" + extraInt3 + "]";
        description = description + " ExtraInt4[" + extraInt4 + "]";
        return description;
    }

    public String getGamePausesInString() {
        String description = "GamePauses: [";
        List<GamePause> pauses = new Select().from(GamePause.class).where("GameManager = ?", getId()).orderBy("Id DESC").execute();
        if ((pauses != null) && (pauses.size() > 0)) {
            for (Model pause : pauses) {
                description = description + String.valueOf(pause) + ", ";
            }
        }
        description = description + "]";
        return description;
    }

    /**
     * Returns list of game logs containing specific game history for specific difficulty and user
     *
     * @param user
     * @param gamesDefinition
     * @param gameDifficulty
     * @return list of game logs containing specific game history for specific difficulty and user
     */
    public static List<GameManager> getGameHistoryForUserAndDifficulty(User user, GamesDefinition gamesDefinition, GameDifficulty gameDifficulty) {
        return new Select().from(GameManager.class).where("User = ? AND Game = ? AND Difficulty = ? AND EndTime <> 0", user.getId(), gamesDefinition.ordinal(), gameDifficulty.ordinal()).execute();
    }

    /**
     * Returns list of game logs containing specific game average history values for specific difficulty and user
     *
     * @param user
     * @param gamesDefinition
     * @param gameDifficulty
     * @return list of game logs containing specific game average history values for specific difficulty and user
     */
    public static List<GameManager> getDailyAverageForUserAndDifficulty(User user, GamesDefinition gamesDefinition, GameDifficulty gameDifficulty) {
        return new Select("Game, StartTime, EndTime, AVG(Counter) as Counter").from(GameManager.class).where("User = ? AND Game = ? And Difficulty = ? And EndTime <> 0", user.getId(), gamesDefinition.ordinal(), gameDifficulty.ordinal()).groupBy("(StartTime / 1000 / 86400)").execute();
    }

    /**
     * Returns sum of stars from all GameManagers for game, user and difficulty from parameters.
     *
     * @param user
     * @param gamesDefinition
     * @return sum of stars from all GameManagers
     */
    public static int getStarNumberForUserGame(User user, GamesDefinition gamesDefinition) {
        GameManager result = new Select("Sum(StarsNumber) as StarsNumber").from(GameManager.class).where("User = ? AND Game = ? And EndTime <> 0", user.getId(), gamesDefinition.ordinal()).executeSingle();
        if (result != null) {
            return result.getStarsNumber();
        }
        return 0;
    }

    /**
     * Return sum of stars from all GameManagers for user id from parameter.
     *
     * @param userId id of user for selecting star number
     * @return sum of stars from all GameManagers
     */
    public static int getStarNumberForUser(Long userId) {
        GameManager result = new Select("Sum(StarsNumber) as StarsNumber").from(GameManager.class).where("User = ? And EndTime <> 0", userId).executeSingle();
        if (result != null) {
            return result.getStarsNumber();
        }
        return 0;
    }

    public long getOverallGameTime() {
        long rawTime = endTime - startTime;
        List<GamePause> pauses = new Select().from(GamePause.class).where("GameManager = ?", getId()).orderBy("Id DESC").execute();
        if ((pauses != null) && (pauses.size() > 0)) {
            for (GamePause pause : pauses) {
                rawTime -= pause.getPauseDuration();
            }
        }
        return rawTime;
    }

    /**
     * Returns <code>true</code> if was any game played n days back. Where n is dayBack parameter.
     *
     * @param userId user for game selecting
     * @param daysBack days back for game selecting
     * @return <code>true</code> if was any game played n days back
     */
    public static boolean isGameManagerForDayBack(Long userId, int daysBack) {
        return new Select().from(GameManager.class).where("StartTime <> 0 " + "AND EndTime <> 0 " + "AND User = ? " + "AND DATETIME((StartTime / 1000), 'unixepoch') >= date('now', '" + -daysBack + " days', 'localtime') " + "AND DATETIME((StartTime / 1000), 'unixepoch') < date('now', '" + -(daysBack - 1) + " days', 'localtime')", userId).executeSingle() != null;
    }

    /**
     * Returns <code>true</code> if was played all games with specific score today.
     *
     * @param userId user for game selecting
     * @param starsLimit minimum star limit
     * @return <code>true</code> if was played all games with specific score
     */
    public static boolean existsGameTypeManagersInOneDayWithStars(Long userId, int starsLimit) {
        List<GameManager> result = new Select("Game").from(GameManager.class).where("StartTime <> 0 " + "AND EndTime <> 0 " + "AND User = ? " + "AND DATETIME((StartTime / 1000), 'unixepoch') >= date('now', 'localtime') " + "AND StarsNumber >= ?", userId, starsLimit).groupBy("Game").execute();
        return result.size() == GamesDefinition.getGames().length;
    }

    /**
     * Returns <code>true</code> if was played all games with specific score and difficulty consecutively.
     *
     * @param userId user for game selecting
     * @param difficulty requested game difficulty
     * @param starsLimit minimum star limit
     * @return <code>true</code> if was played all games with specific score
     */
    public static boolean existsGameTypeManagersConsecutivelyWithStarsNumberAndDifficulty(Long userId, int difficulty, int starsNumber) {
        GameManager lastIdGameManager = new Select().from(GameManager.class).where("StartTime <> 0 " + "AND EndTime <> 0 " + "AND User = ? " + "AND Difficulty = ?", userId, difficulty).orderBy("Id DESC").executeSingle();

        if (lastIdGameManager != null) {
            Long lastId = lastIdGameManager.getId();

            // last games
            List<GameManager> result = new Select().from(GameManager.class).where("StartTime <> 0 " + "AND EndTime <> 0 " + "AND User = ? " + "AND Difficulty = ? " + "AND StarsNumber >= ? ", userId, difficulty, starsNumber).orderBy("Id DESC").limit(GamesDefinition.getGames().length).execute();

            Set<Integer> games = new HashSet<Integer>();
            for (GameManager g : result) {
                games.add(g.getGame());
            }

            return games.size() == GamesDefinition.getGames().length;
        }
        return false;

    }

    /**
     * Returns count of game plays for specific user and game
     *
     * @param user
     * @param gamesDefinition
     * @return
     */
    public static int getGamePlayCountForUser(User user, GamesDefinition gamesDefinition) {
        GameManager result = new Select("Count(ID) as ExtraInt1").from(GameManager.class).where("User = ? AND Game = ? ", user.getId(), gamesDefinition.ordinal()).executeSingle();
        if (result != null) {
            return result.getExtraInt1();
        } else {
            return 0;
        }
    }

    /**
     * Returns the GameManager of the last game played for specific user and game
     *
     * @return last gamemanager
     */
    public static GameManager getLastGameManager(User user, GamesDefinition gamesDefinition) {
        return new Select().from(GameManager.class).where("User = ? AND Game = ? AND endTime <> 0", user.getId(), gamesDefinition.ordinal()).orderBy("Id DESC").executeSingle();
    }

    /**
     * Returns the GameManager of the last game played for specific user and game no matter whether the user finished the game or not
     *
     * @return last gamemanager
     */
    public static GameManager getLastGameManagerRegardlessFinishing(User user, GamesDefinition gamesDefinition) {
        return new Select().from(GameManager.class).where("User = ? AND Game = ?", user.getId(), gamesDefinition.ordinal()).orderBy("Id DESC").executeSingle();
    }

    public static List<GameManager> getNotExportedGameManagers() {
        return new Select().from(GameManager.class).where("Exported = ?", 0).execute();
    }

}
