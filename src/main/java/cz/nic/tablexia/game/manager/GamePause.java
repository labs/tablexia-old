/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.manager;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

/**
 * Game pause active android class implementation
 * 
 * @author Matyáš Latner
 *
 */
@Table(name = "GamePauses")
public class GamePause extends Model {
    
    @Expose
    @Column(name = "StartTime")
    private long        startTime;
    
    @Expose
    @Column(name = "EndTime")
    private long        endTime;
    
    @Column(name = "GameManager")
    private GameManager gameManager;
    
    public GamePause() {
        super();
    }
    
    public GamePause(GameManager gameManager, long startTime) {
        super();
        this.gameManager = gameManager;
        this.startTime = startTime;
    }
    
    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }
    
    public long getStartTime() {
        return startTime;
    }
    
    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }
    
    public long getEndTime() {
        return endTime;
    }
    
    public void setParentGame(GameManager parentGame) {
        this.gameManager = parentGame;
    }
    
    public GameManager getParentGame() {
        return gameManager;
    }
    
    @Override
    public String toString() {
        return "Pause [STime: " + startTime + ", ETime: " + endTime + "]";
    }
    public long getPauseDuration(){
    	return endTime-startTime;
    }
}
