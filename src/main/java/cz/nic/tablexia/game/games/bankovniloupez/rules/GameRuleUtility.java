/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.rules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.annotation.SuppressLint;
import android.content.Context;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureRoot;

/**
 * Abstract game rule class. Contains common functions for all game rules.
 * 
 * @author Matyáš Latner
 */
@SuppressLint("UseSparseArrays")
public abstract class GameRuleUtility extends GameRule {
	
	// TODO sjednotit s GameRule
	// TODO promazat GameRule
	// TODO pouzivat nove metody
	
	private static final double BAIT_PROBABILITY = 0.3;
	
	private int thiefGroupSize;
	private Map<Integer, CreatureDescriptor> globalCreatureDescriptors;
	private CreatureDescriptor baitCreatureDescriptor = new CreatureDescriptor();
	
	public GameRuleUtility(Context context, RandomAccess randomAccess, VertexBufferObjectManager vertexBufferObject, int numberOfCreatures, int numberOfThieves, int thiefGroupSize) {
		super(context, randomAccess, vertexBufferObject, numberOfCreatures, numberOfThieves);
		this.thiefGroupSize = thiefGroupSize;
		globalCreatureDescriptors = new HashMap<Integer, CreatureDescriptor>();
    }
	
	protected void prepareCreatureDescriptionsB() {};
	
	/**
	 * Add global creature descriptor with offset from thief.
	 * @param offset offset from thief - 0 = thief
	 * @param creatureDescriptor creature descriptor to add
	 */
	protected void addGlobalCreatureDescriptor(int offset, CreatureDescriptor creatureDescriptor) {
    	globalCreatureDescriptors.put(offset, creatureDescriptor);
    	creatureDescriptor.addCreatureOffset(Integer.valueOf(offset));
	}
	
	/**
	 * Return creature descriptor for specific offset
	 * @param key offset for selecting creature description
	 * @return creature descriptor for specific offset
	 */
	public CreatureDescriptor getGlobalCreatureDescriptor(Integer key) {
		return globalCreatureDescriptors.get(key);
	}
	
	/**
	 * Set creature description from parameter as new bait creature description
	 * @param creatureDescriptor new bait creature description
	 */
	protected void newBaitCreatureDescription(CreatureDescriptor creatureDescriptor) {
		baitCreatureDescriptor = creatureDescriptor;
	}
	
	/**
	 * Creates new bait creature description and add attribute description from parameter to it
	 * @param attributeDescription attribute description for new bait creature description
	 */
	protected void newBaitCreatureDescriptionFromAttribute(AttributeDescription attributeDescription) {
		baitCreatureDescriptor = new CreatureDescriptor();
		baitCreatureDescriptor.addDescription(attributeDescription);
	}
	
	/**
	 * Add description from creature description in parameter to bait creature description
	 * @param creatureDescriptor creature description to add attribute descriptions from
	 */
	protected void addBaitCreatureDescription(CreatureDescriptor creatureDescriptor) {
		baitCreatureDescriptor.addDescriptions(creatureDescriptor.getDescriptions());
	}
	
	/**
	 * Add attribute description to bait creature description
	 * @param attributeDescription attribute description to add
	 */
	protected void addBaitAttributeDescription(AttributeDescription attributeDescription) {
		baitCreatureDescriptor.addDescription(attributeDescription);
	}
	
	/**
	 * Returns attribute description from bait creature description at position 
	 * specified in parameter with probability from BAIT_PROBABILITY constant or <code>null</code>
	 * @param position position of attribute
	 * @return attribute description from bait creature description or <code>null</code>
	 */
	protected AttributeDescription getBaitCreatureDescriptionAttributeAtPosition(int position) {
		if (getRandomAccess().getRandom().nextDouble() <= BAIT_PROBABILITY) {
			if (baitCreatureDescriptor != null) {
				return baitCreatureDescriptor.getDescriptions().get(position);
			}
    	}
		return null;
	}
	
	/**
	 * Returns bait creature description with probability from BAIT_PROBABILITY constant or <code>null</code>
	 * @return bait creature description or <code>null</code>
	 */
	protected CreatureDescriptor getBaitCreatureDescriptionRandomly() {
		if (getRandomAccess().getRandom().nextDouble() > BAIT_PROBABILITY) {
			return null;
    	}
		return baitCreatureDescriptor;
	}
	
	@Override
	public List<CreatureRoot> prepareCreatures() {
		// generate thieves and pair creatures
        for (int i = 0; i < numberOfThieves; i++) {
        	
        	int initialPosition = 0;
        	boolean condition = true;
        	List<Integer> bannedPositions = new ArrayList<Integer>();
        	
        	prepareCreatureDescriptionsB();
        	
        	while (condition) {
        		initialPosition = getThiefRandomPositionInInterval(thiefGroupSize - 1, bannedPositions);
        		
        		if (initialPosition < 0) {
        			throw new IllegalStateException("Cannot generate creature serie! Too many conflicts!");
        		}
        		
        		bannedPositions.add(initialPosition);
        		condition = !checkCreaturePositions(initialPosition);
			}
        	
        	addSpecialCreatures(initialPosition);
            incrementGroupNumber();
        }
        
        return prepareCreatureDescriptionsA();
	}
	
	/**
	 * Prepares all creatures root
	 * @return list of all creatures root
	 */
	protected abstract List<CreatureRoot> prepareCreatureDescriptionsA();
	
	private boolean checkCreaturePositions(int initialPosition) {
		boolean result = true;
		for (Integer creatureOffset : globalCreatureDescriptors.keySet()) {
			int positionToTry = initialPosition - creatureOffset;
			result = result && checkSpecialCreaturePosition(positionToTry, creatureOffset) && checkCreatureCompactibilityForPosition(globalCreatureDescriptors.get(creatureOffset), positionToTry, creatureOffset == CreatureDescriptor.THIEF_OFFSET);
			if (!result) {
				break;
			}
		}
		return result;
	}
	
	protected boolean checkSpecialCreaturePosition(int position, Integer creatureOffset) {
		return true;
	}
	
	private void addSpecialCreatures(int initialPosition) {
		for (Integer creatureOffset : globalCreatureDescriptors.keySet()) {
			addSpecialCreature(initialPosition - creatureOffset, globalCreatureDescriptors.get(creatureOffset));
		}
	}

}
