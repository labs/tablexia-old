/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.potme.creature;

import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.PathModifier;
import org.andengine.entity.modifier.PathModifier.Path;
import org.andengine.entity.modifier.RotationModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.AnimatedSprite.IAnimationListener;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;
import org.andengine.util.modifier.ease.EaseCubicInOut;

import android.annotation.SuppressLint;
import cz.nic.tablexia.game.games.potme.PotmeActivity;
import cz.nic.tablexia.game.games.potme.ResourceManager;
import cz.nic.tablexia.game.games.potme.UtilityAccess;
import cz.nic.tablexia.game.games.potme.map.TileMap;
import cz.nic.tablexia.game.games.potme.map.TileMap.TileMapPosition;
import cz.nic.tablexia.game.games.potme.map.tile.TileType;

/**
 * Player sprite
 * 
 * @author Matyáš Latner
 */
@SuppressLint("RtlHardcoded") // weird behaviour of Eclipse
public class Player extends Entity {

	private static final float 	PLAYER_WALK_SOUND_RATE 			= 1.2f;
	private static final int	PLAYER_WALK_ANIMATION_FRAMES 	= 8;
	private static final int	PLAYER_JUMP_ANIMATION_FRAMES 	= 6;
	private static final int	PLAYER_CRASH_ANIMATION_FRAMES 	= 7;
	private static final float 	PLAYER_JUMP_SCALE 				= 1.1f;

	public enum PlayerOrientation {

        TOP		(0, 1),
		RIGHT	(1, 1),
        BOTTOM	(2, -1),
        LEFT	(3, -1);

        private int rotation;
		private int directionMultiplier;

        private PlayerOrientation(int rotation, int directionMultiplier) {
            this.rotation = rotation;
			this.directionMultiplier = directionMultiplier;
        }

        public int getRotation() {
            return rotation;
        }
        
        public int getDirectionMultiplier() {
			return directionMultiplier;
		}
        
        private boolean isHorizontal() {
        	return this == LEFT || this == RIGHT;
		}
    }

    private static final int               PLAYER_WALK_WIDTH     = 64;
    private static final int               PLAYER_WALK_HEIGHT    = 42;
    private static final int               PLAYER_JUMP_WIDTH     = 64;
    private static final int               PLAYER_JUMP_HEIGHT    = 77;
    private static final int               PLAYER_CRASH_WIDTH    = 64;
    private static final int               PLAYER_CRASH_HEIGHT   = 42;
    
    private static final int               QUARTAL_ROTATION 	 = 90;

    private AnimatedSprite 		   		   actualPlayerSprite;
    private AnimatedSprite 		   		   playerWalkSprite;
    private AnimatedSprite 		   		   playerJumpSprite;
    private AnimatedSprite 		   		   playerCrashSprite;
    
    
    private PlayerOrientation              actualPlayerOrientation;
    private TileMapPosition                actualTileMapPosition;
    private int                            actualFloor;

    private boolean                        hasKey;

    public Player(int actualFloor, VertexBufferObjectManager vertexBufferObject) {
        super(0, 0, 100, 100);
        this.actualFloor = actualFloor;
        
        actualPlayerOrientation = PlayerOrientation.TOP;
        
        playerWalkSprite 	= createPlayerSprite(PLAYER_WALK_WIDTH, PLAYER_WALK_HEIGHT, ResourceManager.CREATURE_PLAYER_WALK_TILED, vertexBufferObject);
        playerJumpSprite 	= createPlayerSprite(PLAYER_JUMP_WIDTH, PLAYER_JUMP_HEIGHT, ResourceManager.CREATURE_PLAYER_JUMP_TILED, vertexBufferObject);
        playerCrashSprite	= createPlayerSprite(PLAYER_CRASH_WIDTH, PLAYER_CRASH_HEIGHT, ResourceManager.CREATURE_PLAYER_CRASH_TILED, vertexBufferObject);
        
        replacePlayerSprite(playerWalkSprite);

        hasKey = false;
    }

    public void setActualFloor(int actualFloor) {
        this.actualFloor = actualFloor;
    }

    public int getActualFloor() {
        return actualFloor;
    }

    public void setKey(boolean hasKey) {
        this.hasKey = hasKey;
    }

    public boolean hasKey() {
        return hasKey;
    }

    /**
     * Set player direction to one of possible directions in current player tile.
     * In priority RIGHT, TOP, BOTTTOM, LEFT
     */
    public void setPossibleDirection(TileMap tileMap) {
    	if (actualTileMapPosition != null) {
    		TileType tileType = tileMap.getTileAtPosition(actualTileMapPosition).getTileType();
    		if (tileType.isLeftDoor()) {
    			setPlayerOrientation(PlayerOrientation.LEFT, null, false);
    		} else if (tileType.isTopDoor()) {
    			setPlayerOrientation(PlayerOrientation.TOP, null, false);
    		} else if (tileType.isBottomDoor()) {
    			setPlayerOrientation(PlayerOrientation.BOTTOM, null, false);
    		} else if (tileType.isRightDoor()) {
    			setPlayerOrientation(PlayerOrientation.RIGHT, null, false);
    		}
    	}
    }
    
    public void crashToTileMapPositionHalfWay(final TileMap tileMap, int mapStartPositionX, int mapStartPositionY, TileMapPosition currentPlayerPosition, TileMapPosition nextPlayerPosition, final boolean withSound, final IEntityModifierListener modifierListener) {
    	final AnimatedSprite lastSprite = actualPlayerSprite;
    	replacePlayerSprite(playerCrashSprite);
    	moveToTileMapPosition(tileMap, mapStartPositionX, mapStartPositionY, currentPlayerPosition, nextPlayerPosition, true, PLAYER_CRASH_ANIMATION_FRAMES, new IEntityModifierListener() {
			
			@Override
			public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
				if (modifierListener != null) {
					modifierListener.onModifierStarted(pModifier, pItem);
				}
				if (withSound) {					
					UtilityAccess.getInstance().playRandomSoundFromGroup(ResourceManager.BUMPS);
				}
			}
			
			@Override
			public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
				replacePlayerSprite(lastSprite);
				if (modifierListener != null) {
					modifierListener.onModifierFinished(pModifier, pItem);
				}
			}
		});
    }
    
    public void jumpToTileMapPosition(final TileMap tileMap, int mapStartPositionX, int mapStartPositionY, TileMapPosition currentPlayerPosition, TileMapPosition nextPlayerPosition, final IEntityModifierListener modifierListener) {
    	final AnimatedSprite lastSprite = actualPlayerSprite;
    	replacePlayerSprite(playerJumpSprite);
    	moveToTileMapPosition(tileMap, mapStartPositionX, mapStartPositionY, currentPlayerPosition, nextPlayerPosition, PLAYER_JUMP_ANIMATION_FRAMES, new IEntityModifierListener() {
			
			@Override
			public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
				if (modifierListener != null) {
					modifierListener.onModifierStarted(pModifier, pItem);
				}
				registerEntityModifier(new SequenceEntityModifier(new ScaleModifier(PotmeActivity.ANIMATION_MOVE_PERIOD / 2, 1f, PLAYER_JUMP_SCALE, EaseCubicInOut.getInstance()),
									   							  new ScaleModifier(PotmeActivity.ANIMATION_MOVE_PERIOD / 2, PLAYER_JUMP_SCALE, 1f, EaseCubicInOut.getInstance())));
				UtilityAccess.getInstance().playRandomSoundFromGroup(ResourceManager.JUMPS);
			}
			
			@Override
			public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
				replacePlayerSprite(lastSprite);
				if (modifierListener != null) {
					modifierListener.onModifierFinished(pModifier, pItem);
				}
			}
		});
	}
    
    public void walkToTileMapPosition(final TileMap tileMap, int mapStartPositionX, int mapStartPositionY, TileMapPosition currentPlayerPosition, TileMapPosition nextPlayerPosition, final IEntityModifierListener modifierListener) {
    	replacePlayerSprite(playerWalkSprite);
    	moveToTileMapPosition(tileMap, mapStartPositionX, mapStartPositionY, currentPlayerPosition, nextPlayerPosition, PLAYER_WALK_ANIMATION_FRAMES, new IEntityModifierListener() {
			
			@Override
			public void onModifierStarted(IModifier<IEntity> modifier, IEntity item) {
				if (modifierListener != null) {
					modifierListener.onModifierStarted(modifier, item);
				}
				UtilityAccess.getInstance().playRandomSoundFromGroup(ResourceManager.STEPS, PLAYER_WALK_SOUND_RATE);
			}
			
			@Override
			public void onModifierFinished(IModifier<IEntity> modifier, IEntity item) {
				if (modifierListener != null) {
					modifierListener.onModifierFinished(modifier, item);
				}
			}
		});
	}

    
    private boolean moveToTileMapPosition(final TileMap tileMap, int mapStartPositionX, int mapStartPositionY, TileMapPosition currentPlayerPosition, TileMapPosition nextPlayerPosition, final int framesNumber, final IEntityModifierListener modifierListener) {
    	return moveToTileMapPosition(tileMap, mapStartPositionX, mapStartPositionY, currentPlayerPosition, nextPlayerPosition, false, framesNumber, modifierListener);
    }
    
    private boolean moveToTileMapPosition(final TileMap tileMap, int mapStartPositionX, int mapStartPositionY, TileMapPosition currentPlayerPosition, TileMapPosition nextPlayerPosition, final boolean halfWay, final int framesNumber, final IEntityModifierListener modifierListener) {
        TileMapPosition centerPositionForCurrentTile = tileMap.getCenterPositionForTile(currentPlayerPosition.getPositionX(), currentPlayerPosition.getPositionY());
        
        float finishPositionCenterOffsetX = halfWay && actualPlayerOrientation.isHorizontal() ? (getWidth() / 2) + (actualPlayerOrientation.getDirectionMultiplier() * (getWidth() / 2 - getWidth() / 10)) : getWidth() / 2;
		float finishPositionCenterOffsetY = halfWay && !actualPlayerOrientation.isHorizontal() ? (getHeight() / 2) + (actualPlayerOrientation.getDirectionMultiplier() * (getWidth() / 2 - getHeight() / 10)) : getHeight() / 2;
		
		float finishDestinationX;
		float finishDestinationY;
		
		if (halfWay) {
			finishDestinationX = mapStartPositionX + centerPositionForCurrentTile.getPositionX() + finishPositionCenterOffsetX;
			finishDestinationY = mapStartPositionY - centerPositionForCurrentTile.getPositionY() + finishPositionCenterOffsetY;
		} else {
			if (!tileMap.isTileAtPosition(nextPlayerPosition.getPositionX(), nextPlayerPosition.getPositionY())) {
				return false;
			}
			TileMapPosition centerPositionForNextTile = tileMap.getCenterPositionForTile(nextPlayerPosition.getPositionX(), nextPlayerPosition.getPositionY());
			finishDestinationX = mapStartPositionX + centerPositionForNextTile.getPositionX() + finishPositionCenterOffsetX;
			finishDestinationY = mapStartPositionY - centerPositionForNextTile.getPositionY() + finishPositionCenterOffsetY;
			actualTileMapPosition = new TileMapPosition(nextPlayerPosition.getPositionX(), nextPlayerPosition.getPositionY());
		}
		
		Path pathToTravel = new Path(2).to(mapStartPositionX + centerPositionForCurrentTile.getPositionX() + (getWidth() / 2),
        										 mapStartPositionY - centerPositionForCurrentTile.getPositionY() + (getHeight() / 2)).to(finishDestinationX,
        												 																				 finishDestinationY);
        
		
        float animationDuration = halfWay ? (PotmeActivity.ANIMATION_MOVE_PERIOD / 2) : PotmeActivity.ANIMATION_MOVE_PERIOD;
		registerEntityModifier(new PathModifier(animationDuration, pathToTravel, new IEntityModifierListener() {
			
			@Override
			public void onModifierStarted(final IModifier<IEntity> modifier, final IEntity item) {
				if (modifierListener != null) {
					modifierListener.onModifierStarted(modifier, item);
				}
				actualPlayerSprite.animate((long)((PotmeActivity.ANIMATION_MOVE_PERIOD / framesNumber) * 1000), false, new IAnimationListener() {
					
					@Override
					public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {}
					
					@Override
					public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) {}
					
					@Override
					public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite,int pOldFrameIndex, int pNewFrameIndex) {}
					
					@Override
					public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
						if (halfWay) {
							finishMove(modifier, item);
						}
					}
				});
			}
			
			@Override
			public void onModifierFinished(IModifier<IEntity> modifier, IEntity item) {
				if (!halfWay) {
					finishMove(modifier, item);
				}
			}
			
			private void finishMove(IModifier<IEntity> modifier, IEntity item) {
				actualPlayerSprite.stopAnimation(0);
				if (modifierListener != null) {
					modifierListener.onModifierFinished(modifier, item);
				}
			}
			
		}));
        return true;
    }

    public boolean resetPlayerMapPosition(TileMap tileMap, int mapStartPositionX, int mapStartPositionY) {
        if (tileMap.isTileAtPosition(tileMap.getMapStartPosition().getPositionX(), tileMap.getMapStartPosition().getPositionY())) {
            TileMapPosition centerPositionForTile = tileMap.getCenterPositionForTile(tileMap.getMapStartPosition().getPositionX(), tileMap.getMapStartPosition().getPositionY());
            setPosition(mapStartPositionX + centerPositionForTile.getPositionX() + (getWidth() / 2), (mapStartPositionY - centerPositionForTile.getPositionY()) + (getHeight() / 2));
            actualTileMapPosition = new TileMapPosition(tileMap.getMapStartPosition().getPositionX(), tileMap.getMapStartPosition().getPositionY());
            return true;
        } else {
            return false;
        }
    }

    private AnimatedSprite createPlayerSprite(int width, int height, String spriteName, VertexBufferObjectManager vertexBufferObject) {
    	AnimatedSprite playerSprite = new AnimatedSprite(0, 0, width, height, ResourceManager.getInstance().getTiledTexture(spriteName), vertexBufferObject);
    	playerSprite.setVisible(false);
        attachChild(playerSprite);
        return playerSprite;
    }

    public void turnPlayerLeft(IEntityModifierListener modifierListener) {
        int nextPlayerOrientationNumber = actualPlayerOrientation.ordinal() - 1;
        if (nextPlayerOrientationNumber < 0) {
            nextPlayerOrientationNumber = PlayerOrientation.values().length + nextPlayerOrientationNumber;
        }
        setPlayerOrientation(PlayerOrientation.values()[nextPlayerOrientationNumber], modifierListener, true);
    }

    public void turnPlayerRight(IEntityModifierListener modifierListener) {
        int nextPlayerOrientationNumber = actualPlayerOrientation.ordinal() + 1;
        if (nextPlayerOrientationNumber >= PlayerOrientation.values().length) {
            nextPlayerOrientationNumber = nextPlayerOrientationNumber % PlayerOrientation.values().length;
        }
        setPlayerOrientation(PlayerOrientation.values()[nextPlayerOrientationNumber], modifierListener, true);
    }

    public TileMapPosition getActualTileMapPosition() {
        return actualTileMapPosition;
    }

    /**
     * Return next tile map position for current player direction
     */
    public TileMapPosition getNextTileMapPosition() {
        switch (actualPlayerOrientation) {
            case TOP:
                return new TileMapPosition(actualTileMapPosition.getPositionX(), actualTileMapPosition.getPositionY() - 1);
            case RIGHT:
                return new TileMapPosition(actualTileMapPosition.getPositionX() + 1, actualTileMapPosition.getPositionY());
            case LEFT:
                return new TileMapPosition(actualTileMapPosition.getPositionX() - 1, actualTileMapPosition.getPositionY());
            case BOTTOM:
                return new TileMapPosition(actualTileMapPosition.getPositionX(), actualTileMapPosition.getPositionY() + 1);
        }
        return null;
    }

    public PlayerOrientation getActualPlayerOrientation() {
        return actualPlayerOrientation;
    }

    private void setPlayerOrientation(PlayerOrientation playerOrientation, IEntityModifierListener modifierListener, boolean animated) {
    	int toRotation = playerOrientation.rotation * QUARTAL_ROTATION;
        if (animated) {
        	int fromRotation = actualPlayerOrientation.rotation * QUARTAL_ROTATION;
        	int fullCircle = PlayerOrientation.values().length * QUARTAL_ROTATION;
        	if (actualPlayerOrientation == PlayerOrientation.LEFT && playerOrientation == PlayerOrientation.TOP) {
        		toRotation = fullCircle;
        	} else if (actualPlayerOrientation == PlayerOrientation.TOP && playerOrientation == PlayerOrientation.LEFT) {
        		fromRotation = fullCircle;
        	}
			actualPlayerSprite.registerEntityModifier(new RotationModifier(PotmeActivity.ANIMATION_MOVE_PERIOD / 4, fromRotation, toRotation, modifierListener));
    	} else {    		
    		actualPlayerSprite.setRotation(toRotation);
    	}
        actualPlayerOrientation = playerOrientation;
    }

    private void replacePlayerSprite(AnimatedSprite playerSprite) {
    	if (actualPlayerSprite != null) {    		
    		actualPlayerSprite.setVisible(false);
    	}
    	actualPlayerSprite = playerSprite;
    	actualPlayerSprite.setRotation(actualPlayerOrientation.rotation * QUARTAL_ROTATION);
    	actualPlayerSprite.setVisible(true);
    }
}
