/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.rules.medium;

import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.Context;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureFactory;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute.AttributeColor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.ClothingAttribute;

/**
 * 
 * @author Matyáš Latner
 */
public class CA_1_CCRule extends CC_1_CCRule {

    public CA_1_CCRule(Context context, RandomAccess randomAccess, VertexBufferObjectManager vertexBufferObject, int numberOfCreatures, int numberOfThieves) {
        super(context, randomAccess, vertexBufferObject, numberOfCreatures, numberOfThieves);
    }
    
    @Override
    public String[] prepareRuleMessageParameters() {
        return new String[] {
        		getAttributeColorName(getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0)),
        		getAttributeName(getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0), false),
        		getAttributeColorName(getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0))
        };
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
    	AttributeColor t1CommonAttributeColor = getRandomColorFromGeneratedCreature(BAN_ATTRIBUTES_SET_FOR_GENERATING, vertexBufferObject);
        CreatureDescriptor t1CreatureDescriptor = new CreatureDescriptor();
        t1CreatureDescriptor.addDescription(new AttributeDescription(t1CommonAttributeColor, null, ClothingAttribute.class));
        addGlobalCreatureDescriptor(T1_OFFSET, t1CreatureDescriptor);
    	
        CreatureDescriptor t0CreatureDescriptor = getRandomCreatureDescriptionWithOneAttribute(CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, vertexBufferObject, getRandomAccess()).getCreatureDescrition());
        addGlobalCreatureDescriptor(T0_OFFSET, t0CreatureDescriptor);
    }

}
