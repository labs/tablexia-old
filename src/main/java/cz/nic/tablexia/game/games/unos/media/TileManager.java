/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.unos.media;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.andengine.opengl.texture.Texture;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;
import android.util.Pair;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.game.games.unos.UnosActivity;
import cz.nic.tablexia.game.games.unos.model.Tile;
import cz.nic.tablexia.util.andengine.ZipFileTexture;

/**
 * @author lhoracek
 */
public class TileManager extends HashMap<Tile, Pair<Texture, TextureRegion>> {

    private static final long   serialVersionUID = 7306787996311196671L;
    private static final String TAG              = TileManager.class.getSimpleName();

    private static final String TILES_DIR        = UnosActivity.BASE_DIR + "gfx/tiles/";
    private static final String TILES_EXTENSION  = ".png";

    private TextureManager      textureManager;
    private AssetManager        assetManager;
    private Context             context;

    public TileManager(TextureManager textureManager, AssetManager assetManager, Context context) {
        super();
        this.textureManager = textureManager;
        this.assetManager = assetManager;
        this.context = context;
    }

    public void loadTileTextures(Collection<Tile> tiles) {
        Tablexia tablexia = (Tablexia) context.getApplicationContext();
        Set<Tile> tileSet = new HashSet<Tile>(tiles);
        for (Tile tile : tileSet) {
            if (!containsKey(tile)) {
                String path = TILES_DIR + tile.getResource() + TILES_EXTENSION;

                Log.v(TAG, "Loading tile " + path);
                Texture tileTexture = new ZipFileTexture(textureManager, tablexia.getZipResourceFile(), path);
                tileTexture.load();
                TextureRegion tileTextureRegion = TextureRegionFactory.extractFromTexture(tileTexture);
                put(tile, new Pair<Texture, TextureRegion>(tileTexture, tileTextureRegion));

            }
        }
    }
}
