/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.strelnice.media;

/**
 * @author lhoracek
 */
public enum TextureType {

    WATCH("hodinky.png"), //
    WATCH_HAND("rucicka.png"), //

    CAROUSEL("karusel.png"), //

    POZADI("pozadi1.png"), //
    FRAME("ramecek.png"), //

    BOX_BAD("bedynkaBAD.png"), //
    BOX_GOOD("bedynkaGOOD.png"), //

    WAVE_1("herni_plan2.png"), //
    WAVE_2("herni_plan1.png"), //
    WAVE_3("herni_plan3.png"), //

    FLOWER_1("kytky/kvitka1.png"), //
    FLOWER_2("kytky/kvitka2.png"), //
    FLOWER_3("kytky/kvitka3.png"), //
    FLOWER_4("kytky/kvitka4.png"), //
    FLOWER_5("kytky/kvitka5.png"), //
    FLOWER_6("kytky/kvitka6.png"), //
    FLOWER_7("kytky/kvitka7.png"), //
    FLOWER_8("kytky/kvitka8.png"), //
    FLOWER_9("kytky/kvitka9.png"), //
    FLOWER_10("kytky/kvitka10.png"), //
    FLOWER_11("kytky/kvitka11.png"), //
    FLOWER_12("kytky/kvitka12.png"), //
    FLOWER_13("kytky/kvitka13.png"), //
    FLOWER_14("kytky/kvitka14.png"), //
    FLOWER_15("kytky/kvitka15.png"), //
    FLOWER_16("kytky/kvitka16.png"), //
    FLOWER_17("kytky/kvitka17.png"), //
    FLOWER_18("kytky/kvitka18.png"), //
    FLOWER_19("kytky/kvitka19.png"), //
    FLOWER_20("kytky/kvitka20.png"), //
    FLOWER_21("kytky/kvitka21.png"), //
    FLOWER_22("kytky/kvitka22.png"), //
    FLOWER_23("kytky/kvitka23.png"), //
    FLOWER_24("kytky/kvitka24.png"), //

    BOX_SMOKE_1("effects/1dym.png"), //
    BOX_SMOKE_2("effects/2dym.png"), //
    BOX_SMOKE_3("effects/3dym.png"), //
    BOX_SMOKE_4("effects/4dym.png"), //
    BOX_SMOKE_5("effects/5dym.png"), //
    BOX_HIT("effects/GOOD_plus1.png"), //
    BOX_TIME_MINUS_7("effects/BAD_minus7.png"), //
    BOX_TIME_MINUS_5("effects/BAD_minus5.png"), //
    BOX_TIME_PLUS_5("effects/GOOD_plus5.png"), //
    BOX_TIME_PLUS_7("effects/GOOD_plus7.png"), //
    BOX_SPEED_UP("effects/BAD_zrychleni.png"), //
    BOX_SPEED_DOWN("effects/GOOD_zpomaleni.png"), //

    ; //

    public static final float         WATCH_CENTER_X_OFFSET = 7;
    public static final float         WATCH_CENTER_Y_OFFSET = -65;

    public static final TextureType[] BOXES                 = { BOX_BAD, BOX_GOOD };
    public static final TextureType[] EFFECTS               = { BOX_SMOKE_1, BOX_SMOKE_2, BOX_SMOKE_3, BOX_SMOKE_4, BOX_SMOKE_5, BOX_TIME_MINUS_5, BOX_TIME_MINUS_7, BOX_TIME_PLUS_5, BOX_TIME_PLUS_7, BOX_SPEED_UP, BOX_SPEED_DOWN };
    public static final TextureType[] FLOWERS_LEVEL_EASY    = { FLOWER_1, FLOWER_6, FLOWER_9, FLOWER_13, FLOWER_17, FLOWER_21 };
    public static final TextureType[] FLOWERS_LEVEL_MEDIUM  = { FLOWER_2, FLOWER_4, FLOWER_5, FLOWER_7, FLOWER_11, FLOWER_12, FLOWER_14, FLOWER_15, FLOWER_17, FLOWER_19, FLOWER_21, FLOWER_22 };
    public static final TextureType[] FLOWERS_LEVEL_HARD    = { FLOWER_1, FLOWER_2, FLOWER_3, FLOWER_4, FLOWER_5, FLOWER_6, FLOWER_7, FLOWER_8, FLOWER_9, FLOWER_10, FLOWER_11, FLOWER_12, FLOWER_13, FLOWER_14, FLOWER_15, FLOWER_16, FLOWER_17, FLOWER_18, FLOWER_19, FLOWER_20, FLOWER_21, FLOWER_22, FLOWER_23, FLOWER_24 };

    private final String              resource;

    private TextureType(String resource) {
        this.resource = resource;

    }

    public String getResource() {
        return resource;
    }
}
