/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.rules.hard;

import java.util.ArrayList;
import java.util.List;

import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.Context;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureFactory;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureRoot;
import cz.nic.tablexia.game.games.bankovniloupez.rules.GameRuleUtility;

/**
 * 
 * @author Matyáš Latner
 */
public class CC_0_CC_0_CCRule extends GameRuleUtility {
    
	private   static final int 	 	GROUP_SIZE	= 3;
	protected static final Integer 	T0_OFFSET 	= CreatureDescriptor.THIEF_OFFSET;
	protected static final Integer 	T1_OFFSET 	= Integer.valueOf(1);
	protected static final Integer 	T2_OFFSET 	= Integer.valueOf(2);

    public CC_0_CC_0_CCRule(Context context, RandomAccess randomAccess, VertexBufferObjectManager vertexBufferObject, int numberOfCreatures, int numberOfThieves) {
        super(context, randomAccess, vertexBufferObject, numberOfCreatures, numberOfThieves, GROUP_SIZE);
    }
    
    @Override
    public String[] prepareRuleMessageParameters() {
        return new String[] {
        		getAttributeColorName(getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0)),
        		getAttributeName(getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0), false),
                getAttributeColorName(getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0)),
                getAttributeName(getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0), false),
                getAttributeColorName(getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0)),
                getAttributeName(getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0), false)
        };
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
        CreatureDescriptor t2CreatureDescriptor = getRandomCreatureDescriptionWithNAttributes(CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, vertexBufferObject, getRandomAccess()).getCreatureDescrition(), 1);
		addGlobalCreatureDescriptor(T2_OFFSET, t2CreatureDescriptor);
        addGlobalCreatureDescriptor(T1_OFFSET, getRandomCreatureDescriptionWithNAttributes(CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, vertexBufferObject, getRandomAccess()).getCreatureDescrition(), 1));
        
        CreatureDescriptor t0CreatureDescriptorToBan = new CreatureDescriptor();
        t0CreatureDescriptorToBan.disableGenderCompatibilityCheck();
        t0CreatureDescriptorToBan.addDescriptions(BAN_ATTRIBUTES_SET_FOR_GENERATING.getDescriptions());
        t0CreatureDescriptorToBan.addDescription(t2CreatureDescriptor.getDescriptions().get(0));
        
        addGlobalCreatureDescriptor(T0_OFFSET, getRandomCreatureDescriptionWithNAttributes(CreatureFactory.getInstance().generateCreature(null, t0CreatureDescriptorToBan, vertexBufferObject, getRandomAccess()).getCreatureDescrition(), 1));
    }
    
    @Override
    protected List<CreatureRoot> prepareCreatureDescriptionsA() {
    	List<CreatureRoot> creatures = new ArrayList<CreatureRoot>();
    	CreatureRoot l1Creature = null;
        CreatureRoot l0Creature = null;
        int l1Position;
        int l0Position;
        // generate random creatures and add thieves and pair creatures to the specific positions
        for (int i = 0; i < numberOfCreatures; i++) {
        	
        	l1Position = creatures.size() - T2_OFFSET;
            if (l1Position >= 0) {
                l1Creature = creatures.get(l1Position);
            }
        	
            l0Position = creatures.size() - T1_OFFSET;
            if (l0Position >= 0) {
                l0Creature = creatures.get(l0Position);
            }

            CreatureDescriptor creatureDescriptor = specialCreatures.get(i);
            if (creatureDescriptor != null) { // add special creature
            	if (l1Creature != null && l0Creature != null
            			&& l1Creature.hasAttribute(getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0))
            			&& l0Creature.hasAttribute(getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0))
            			&& !creatureDescriptor.containsCreatureOffset(T0_OFFSET)) {
            		
            		creatures.add(CreatureFactory.getInstance().generateCreature(creatureDescriptor, getGlobalCreatureDescriptor(T0_OFFSET), vertexBufferObject, getRandomAccess()));
            	} else {            		
            		creatures.add(CreatureFactory.getInstance().generateCreature(creatureDescriptor, null, vertexBufferObject, getRandomAccess()));
            	}
            } else {
            	if (l1Creature != null && l0Creature != null
            			&& l1Creature.hasAttribute(getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0))
            			&& l0Creature.hasAttribute(getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0))) {
            		
            		creatures.add(CreatureFactory.getInstance().generateCreature(null, getGlobalCreatureDescriptor(T0_OFFSET), vertexBufferObject, getRandomAccess()));
            	} else {
            		
            		if (l0Creature != null && l0Creature.hasAttribute(getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0))) {
            			creatures.add(CreatureFactory.getInstance().generateCreature(getGlobalCreatureDescriptor(T1_OFFSET), null, vertexBufferObject, getRandomAccess()));
            		} else {            			
            			newBaitCreatureDescription(getGlobalCreatureDescriptor(T2_OFFSET));
            			creatures.add(CreatureFactory.getInstance().generateCreature(getBaitCreatureDescriptionRandomly(), null, vertexBufferObject, getRandomAccess()));
            		}
            	}
            }
        }

        return creatures;
    }

}
