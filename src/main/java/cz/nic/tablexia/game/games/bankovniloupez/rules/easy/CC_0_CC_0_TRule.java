/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.rules.easy;

import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.Context;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureFactory;

/**
 * 
 * @author Matyáš Latner
 */
public class CC_0_CC_0_TRule extends CA_0_CC_0_TRule {

    public CC_0_CC_0_TRule(Context context, RandomAccess randomAccess, VertexBufferObjectManager vertexBufferObject, int numberOfCreatures, int numberOfThieves) {
        super(context, randomAccess, vertexBufferObject, numberOfCreatures, numberOfThieves);
    }
    
    @Override
    public String[] prepareRuleMessageParameters() {
        return new String[] {
        		getAttributeColorName(getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0)),
        		getAttributeName(getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0), false),
                getAttributeColorName(getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0)),
                getAttributeName(getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0), false),
        };
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
    	
        CreatureDescriptor t2CreatureDescriptor = getRandomCreatureDescriptionWithOneAttribute(CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, vertexBufferObject, getRandomAccess()).getCreatureDescrition());
        AttributeDescription t2AttributeDescription = t2CreatureDescriptor.getDescriptions().get(0);
        addGlobalCreatureDescriptor(T2_OFFSET, t2CreatureDescriptor);
        
        BAN_ATTRIBUTES_SET_FOR_GENERATING.addDescription(t2AttributeDescription);
        addGlobalCreatureDescriptor(T1_OFFSET, getRandomCreatureDescriptionWithOneAttribute(CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, vertexBufferObject, getRandomAccess()).getCreatureDescrition()));
        
        addGlobalCreatureDescriptor(T0_OFFSET, new CreatureDescriptor());
    }

}
