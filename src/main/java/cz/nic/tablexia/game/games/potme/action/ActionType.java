/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.potme.action;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;
import cz.nic.tablexia.game.games.potme.PotmeActivity;
import cz.nic.tablexia.game.games.potme.PotmeDifficulty;
import cz.nic.tablexia.game.games.potme.ResourceManager;
import cz.nic.tablexia.game.games.potme.action.rule.AbstractActionTypeRule;
import cz.nic.tablexia.game.games.potme.action.rule.DogActionRule;
import cz.nic.tablexia.game.games.potme.action.rule.DoorActionRule;
import cz.nic.tablexia.game.games.potme.action.rule.GoActionRule;
import cz.nic.tablexia.game.games.potme.action.rule.KeyActionRule;
import cz.nic.tablexia.game.games.potme.action.rule.LeftActionRule;
import cz.nic.tablexia.game.games.potme.action.rule.RightActionRule;
import cz.nic.tablexia.game.games.potme.action.rule.StairsActionRule;
import cz.nic.tablexia.game.games.potme.creature.Player;
import cz.nic.tablexia.game.games.potme.map.TileMap;

/**
 * Action type definition
 * 
 * @author Matyáš Latner
 *
 */
public enum ActionType {
	
	GO		(ResourceManager.ACTION_GO, 	GoActionRule.class, 	0, false),
	RIGHT	(ResourceManager.ACTION_RIGHT, 	RightActionRule.class, 	0, false),
	LEFT	(ResourceManager.ACTION_LEFT, 	LeftActionRule.class, 	0, false),
	DOOR	(ResourceManager.ACTION_DOOR, 	DoorActionRule.class, 	0, false),
	DOG		(ResourceManager.ACTION_DOG, 	DogActionRule.class, 	0, false),
	STAIRS	(ResourceManager.ACTION_STAIRS, StairsActionRule.class, 2, false),
	KEY		(ResourceManager.ACTION_KEY, 	KeyActionRule.class, 	0, true);
	
	public interface IActionFinishedListener {
		
		public void onActionFinished(boolean result);
		
	}
	
	private String 					texture;
	private AbstractActionTypeRule 	actionTypeRule;
	private int 					minimumFloorCount;
	private boolean 				mustBeInDifficulty;
	
	private ActionType(String texture, Class<? extends AbstractActionTypeRule> clazz, int minimumFloorCount, boolean mustBeInDifficulty) {
		this.texture = texture;
		this.minimumFloorCount = minimumFloorCount;
		this.mustBeInDifficulty = mustBeInDifficulty;
		
		try {
			actionTypeRule = clazz.newInstance();
        } catch (Exception e) {
            Log.e(name(), "Cannot create action type: " + name(), e);
        }
	}
	
	public String getTexture() {
		return texture;
	}
	
	/**
     * Performs rule assigned to specific action
     * 
     * @return <code>true</code> if rule was successfully
     */
    public void performActionTypeRule(PotmeActivity potmeActivity, TileMap tileMap, Player player, int mapStartPositionX, int mapStartPositionY, IActionFinishedListener finishedListener) {
    	actionTypeRule.performActionRule(potmeActivity, tileMap, player, mapStartPositionX, mapStartPositionY, finishedListener);
    }
    
    /**
     * Returns all action types which have different difficulty than the difficulty from parameter
     * 
     * @param gameDifficulty
     * @return
     */
    public static List<ActionType> getActionTypesForGameDifficulty(PotmeDifficulty potmeDifficulty) {
    	List<ActionType> result = new ArrayList<ActionType>(); 
    	for (ActionType actionType : ActionType.values()) {
			if ((actionType.minimumFloorCount == 0 || potmeDifficulty.getFloorCount() >= actionType.minimumFloorCount) && 
				(!actionType.mustBeInDifficulty || potmeDifficulty.hasDifficultyActionType(actionType))) {
				result.add(actionType);
    		}
		}
    	return result;
	}
	
}
