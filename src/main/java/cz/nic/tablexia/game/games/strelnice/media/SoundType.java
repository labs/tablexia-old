/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.strelnice.media;

/**
 * @author lhoracek
 */
public enum SoundType {

    SWOOSH_IN("swoosh_in.mp3", SoundClass.SOUND, true), //
    BELL("bell.mp3", SoundClass.SOUND, true), //

    CAROUSEL_1("otoceni_kola_1.mp3", SoundClass.SOUND, true), //
    CAROUSEL_2("otoceni_kola_2.mp3", SoundClass.SOUND, true), //

    INTRO("uvodni_sekcence.mp3", SoundClass.SOUND, true), //

    HIT_BOX_SMOKE("vystrel_bedna_dymovnice.mp3", SoundClass.SOUND, true), //
    HIT_BOX_SLOW("vystrel_bedna_zpomaleni.mp3", SoundClass.SOUND, true), //
    HIT_BOX_FAST("vystrel_bedna_zrychleni.mp3", SoundClass.SOUND, true), //
    HIT_ERROR("vystrel_spatne.mp3", SoundClass.SOUND, true), //
    HIT_OUT("vystrel_minela.mp3", SoundClass.SOUND, true), //
    HIT_GOOD_1("vystrel_spravne_1.mp3", SoundClass.SOUND, true), //
    HIT_GOOD_2("vystrel_spravne_2.mp3", SoundClass.SOUND, true), //
    HIT_GOOD_3("vystrel_spravne_3.mp3", SoundClass.SOUND, true), //
    HIT_GOOD_4("vystrel_spravne_4.mp3", SoundClass.SOUND, true), //
    ;
    public enum SoundClass {
        SOUND, MUSIC;
    }

    public static final SoundType[] CAROUSEL_SOUNDS = { CAROUSEL_1, CAROUSEL_2 };
    public static final SoundType[] HIT_GOOD_SOUNDS = { HIT_GOOD_1, HIT_GOOD_2, HIT_GOOD_3, HIT_GOOD_4 };

    private SoundType(String resource, SoundClass soundClass, boolean eager) {
        this.resource = resource;
        this.eager = eager;
        this.soundClass = soundClass;
    }

    private final String     resource;
    private final boolean    eager;
    private final SoundClass soundClass;

    public String getResource() {
        return resource;
    }

    public boolean isEager() {
        return eager;
    }

    public SoundClass getSoundClass() {
        return soundClass;
    }
}
