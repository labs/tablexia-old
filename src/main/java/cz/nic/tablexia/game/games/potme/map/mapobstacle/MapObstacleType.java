/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.potme.map.mapobstacle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.potme.PotmeActivity;
import cz.nic.tablexia.game.games.potme.ResourceManager;

/**
 * Map obstacle type definition
 * 
 * @author Matyáš Latner 
 *
 */
public enum MapObstacleType {
	
	DOOR_H(ResourceManager.OBSTACLE_DOOR_CLOSED, false, ResourceManager.OBSTACLE_DOOR_OPENED, 0, ResourceManager.BUMPS, new ArrayList<MapObstaclePosition>(Arrays.asList(new MapObstaclePosition[] {MapObstaclePosition.BOTTOM_POSITION})), 1),
	DOOR_V(ResourceManager.OBSTACLE_DOOR_CLOSED, false, ResourceManager.OBSTACLE_DOOR_OPENED, 1, ResourceManager.BUMPS, new ArrayList<MapObstaclePosition>(Arrays.asList(new MapObstaclePosition[] {MapObstaclePosition.LEFT_POSITION})), 1),
	DOG_H (ResourceManager.OBSTACLE_DOG_SLEEP,   false, ResourceManager.OBSTACLE_DOG_ATTACK, 0, ResourceManager.BARKS, new ArrayList<MapObstaclePosition>(Arrays.asList(new MapObstaclePosition[] {MapObstaclePosition.BOTTOM_POSITION})), 1.5f),
	DOG_V (ResourceManager.OBSTACLE_DOG_SLEEP,   false, ResourceManager.OBSTACLE_DOG_ATTACK, 3, ResourceManager.BARKS, new ArrayList<MapObstaclePosition>(Arrays.asList(new MapObstaclePosition[] {MapObstaclePosition.LEFT_POSITION})), 1.5f);
	
	public enum MapObstaclePosition {
		
		LEFT_POSITION(-PotmeActivity.TILE_SIZE / 2, 0),
		BOTTOM_POSITION(0, PotmeActivity.TILE_SIZE / 2);
		
		private int xOffset;
		private int yOffset;
		
		private MapObstaclePosition(int xOffset, int yOffset) {
			this.xOffset = xOffset;
			this.yOffset = yOffset;
		}
		
		public int getXOffset() {
			return xOffset;
		}
		
		public int getYOffset() {
			return yOffset;
		}
	}
	
	private String 						activeTexture;
	private String 						inactiveTexture;
	private int 						rotation;
	private List<MapObstaclePosition> 	positions;
	private String[] 					soundGroup;
	private float 						obstacleSizeRation;
	private boolean 					animated;
	
	/**
	 * Map obstacle type definition
	 */
	private MapObstacleType(String activeTexture, boolean animated, String inactiveTexture, int rotation, String[] soundGroup, List<MapObstaclePosition> obstaclePosition, float obstacleSizeRatio) {
		this.activeTexture 		= activeTexture;
		this.animated 			= animated;
		this.inactiveTexture 	= inactiveTexture;
		this.rotation 			= rotation;
		this.soundGroup 		= soundGroup;
		this.positions 			= obstaclePosition;
		this.obstacleSizeRation = obstacleSizeRatio;
	}

	public String getActiveTexture() {
		return activeTexture;
	}
	
	public boolean isAnimated() {
		return animated;
	}
	
	public String getInactiveTexture() {
		return inactiveTexture;
	}
	
	public int getRotation() {
		return rotation;
	}
	
	public String[] getSoundGroup() {
		return soundGroup;
	}
	
	public float getObstacleSizeRatio() {
		return obstacleSizeRation;
	}
	
	public List<MapObstaclePosition> getObstacleAvailablePositions() {
		return positions;
	}
	
	public MapObstaclePosition getObstacleRandomAvailablePosition(RandomAccess randomAccess) {
		return positions.get(randomAccess.getRandom().nextInt(positions.size()));
	}
	
	/**
	 * Return map obstacle types for position in parameter
	 * @param positions one or more positions
	 * @return obstacle types for positions in parameter
	 */
	public static List<MapObstacleType> getMapObstacleForPosition(MapObstaclePosition... positions) {
		List<MapObstacleType> result = new ArrayList<MapObstacleType>();
		for (MapObstacleType mapObstacleType : values()) {
			for (MapObstaclePosition position : positions) {				
				if (mapObstacleType.positions.contains(position)) {
					result.add(mapObstacleType);
					break;
				}
			}
		}
		return result;
	}

}
