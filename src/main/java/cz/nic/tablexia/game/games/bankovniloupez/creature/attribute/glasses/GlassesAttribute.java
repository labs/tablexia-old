/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.glasses;

import java.util.ArrayList;
import java.util.List;

import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute.CreatureGenericType;

@CreatureGenericType(isGeneric = true)
public class GlassesAttribute extends Attribute {

    private static final int POSITION_X = 0;
    private static final int POSITION_Y = 0;
    private static final int Z_INDEX    = 6;

    public static List<AttributeDescription> getTextures() {
        return new ArrayList<ResourceManager.AttributeDescription>() {

            private static final long serialVersionUID = -1377013489500837025L;

            {
                addAll(MGlassesAttribute.getTextures());
                addAll(FGlassesAttribute.getTextures());
                add(null);
                add(null);
            }
        };
    }

    public GlassesAttribute(AttributeColor color, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(color, POSITION_X, POSITION_Y, pTextureRegion, pVertexBufferObjectManager);
        setZIndex(Z_INDEX);
    }

    @Override
    public Class<? extends Attribute> getGenericType() {
        return GlassesAttribute.class;
    }

}
