/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.rules.easy;

import java.util.ArrayList;
import java.util.List;

import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.Context;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureFactory;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureRoot;
import cz.nic.tablexia.game.games.bankovniloupez.rules.GameRuleUtility;

/**
 * 
 * @author Matyáš Latner
 */
public class CCCCRule extends GameRuleUtility {
	
	private static final int 	 	GROUP_SIZE	= 1;
	protected static final Integer 	T0_OFFSET 	= Integer.valueOf(0);

    public CCCCRule(Context context, RandomAccess randomAccess, VertexBufferObjectManager vertexBufferObject, int numberOfCreatures, int numberOfThieves) {
        super(context, randomAccess, vertexBufferObject, numberOfCreatures, numberOfThieves, GROUP_SIZE);
    }
    
    @Override
    public String[] prepareRuleMessageParameters() {
        AttributeDescription t0Description0 = getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0);
        AttributeDescription t0Description1 = getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(1);
		return new String[] {
        		getAttributeColorName(t0Description0),
        		getAttributeName(t0Description0, false),
                getAttributeColorName(t0Description1),
                getAttributeName(t0Description1, false),
        };
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
    	addGlobalCreatureDescriptor(0, getRandomCreatureDescriptionWithTwoAttributes(CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, vertexBufferObject, getRandomAccess()).getCreatureDescrition()));
    }

    @Override
    public List<CreatureRoot> prepareCreatureDescriptionsA() {

        List<CreatureRoot> creatures = new ArrayList<CreatureRoot>();
        
        CreatureDescriptor t0CreatureDescription = getGlobalCreatureDescriptor(T0_OFFSET);
        CreatureDescriptor creatureDescriptorToBan1 = new CreatureDescriptor();
		creatureDescriptorToBan1.addDescription(t0CreatureDescription.getDescriptions().get(0));
        CreatureDescriptor creatureDescriptorToBan2 = new CreatureDescriptor();
        creatureDescriptorToBan2.addDescription(t0CreatureDescription.getDescriptions().get(1));
        
        for (int i = 0; i < numberOfCreatures; i++) {
            CreatureDescriptor creatureDescriptor = specialCreatures.get(i);
            if (creatureDescriptor != null) { // add special creature
            	creatures.add(CreatureFactory.getInstance().generateCreature(creatureDescriptor, null, vertexBufferObject, getRandomAccess()));
            } else {
            	
            	CreatureDescriptor creatureDescriptorToBan = null;
            	if (getRandomAccess().getRandom().nextBoolean()) {
            		creatureDescriptorToBan = creatureDescriptorToBan1;
            		newBaitCreatureDescription(creatureDescriptorToBan2);
            	} else {
            		creatureDescriptorToBan = creatureDescriptorToBan2;
            		newBaitCreatureDescription(creatureDescriptorToBan1);
            	}
            	
            	creatures.add(CreatureFactory.getInstance().generateCreature(getBaitCreatureDescriptionRandomly(), creatureDescriptorToBan, vertexBufferObject, getRandomAccess()));
            }
        }

        return creatures;
    }

}
