/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.nocnisledovani;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.andengine.audio.music.Music;
import org.andengine.audio.music.MusicFactory;
import org.andengine.audio.music.MusicManager;

import android.content.Context;
import android.util.Log;
import cz.nic.tablexia.Tablexia;

/**
 * @author lhoracek
 */
public class MfxManager {
    private static final String  TAG                        = MfxManager.class.getSimpleName();

    private static final String  SOUNDS_DIR                 = "nocnisledovani/mfx/";

    public static final String   SOUND_DAY                  = "den.mp3";
    public static final String   SOUND_NIGHT                = "noc.mp3";
    public static final String   SOUND_TRANSITION_MORNING_1 = "prechod_rano_1.mp3";
    public static final String   SOUND_TRANSITION_MORNING_2 = "prechod_rano_2.mp3";
    public static final String   SOUND_TRANSITION_MORNING_3 = "prechod_rano_3.mp3";
    public static final String   SOUND_TRANSITION_EVENING_1 = "prechod_vecer_1.mp3";
    public static final String   SOUND_TRANSITION_EVENING_2 = "prechod_vecer_2.mp3";
    public static final String   SOUND_TRANSITION_EVENING_3 = "prechod_vecer_3.mp3";
    public static final String   SOUND_CLOCK                = "hodiny_1.mp3";
    public static final String   SOUND_CLOCK_LONG           = "hodiny_3.mp3";

    public static final String[] SOUND_TRANSITIONS_MORNING  = { SOUND_TRANSITION_MORNING_1, SOUND_TRANSITION_MORNING_2, SOUND_TRANSITION_MORNING_3 };
    public static final String[] SOUND_TRANSITIONS_EVENING  = { SOUND_TRANSITION_EVENING_1, SOUND_TRANSITION_EVENING_2, SOUND_TRANSITION_EVENING_3 };

    private Map<String, Music>   musics                     = new HashMap<String, Music>();
    private MusicManager         musicManager;
    private Context              context;

    private Music                playingAmbient;

    public MfxManager(MusicManager musicManager, Context context) {
        super();
        this.musicManager = musicManager;
        this.context = context;
        MusicFactory.setAssetBasePath(SOUNDS_DIR);
        loadSounds();
    }

    /**
     * Nahrat zvuky, ktere jsou oznacene jako eage, a nactou se automaticky, ostatni zvuky se nactou az ve chvili prvniho prehrani
     */
    public void loadSounds() {
        loadSound(SOUND_DAY);
        loadSound(SOUND_NIGHT);
        for (String sound : SOUND_TRANSITIONS_MORNING) {
            loadSound(sound);
        }
        for (String sound : SOUND_TRANSITIONS_EVENING) {
            loadSound(sound);
        }
        loadSound(SOUND_CLOCK);
        loadSound(SOUND_CLOCK_LONG);

    }

    private void loadSound(String sound) {
        Tablexia tablexia = (Tablexia) context.getApplicationContext();
        try {
            Log.v(TAG, "Loading sound " + sound);
            Music music = MusicFactory.createMusicFromAssetFileDescriptor(musicManager, tablexia.getZipResourceFile().getAssetFileDescriptor(SOUNDS_DIR + sound));
            musics.put(sound, music);
        } catch (final IOException e) {
            Log.e(TAG, "Error loading sound " + e.getMessage() + " - resource: " + sound, e);
        }
    }

    public Music playSound(String sound) {
        return playSound(sound, false);
    }

    public Music getSound(String sound) {
        return musics.get(sound);
    }

    /**
     * Prehraje zvuk z enumu, ktery muze byt preloadovan
     * 
     * @param sound
     * @param onCompletionListener
     */
    private Music playSound(String sound, boolean looping) {
        if (musics.get(sound) == null) {
            throw new IllegalArgumentException("Sound not found");
        }
        final Music music = musics.get(sound);
        music.setLooping(looping);
        music.play();
        return music;
    }

    public void playAmbientSound(String sound) {
        if (playingAmbient != null) {
            if (playingAmbient.isPlaying()) {
                playingAmbient.pause();
            }
        }
        playingAmbient = playSound(sound, true);
    }

}
