/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.headgear;

import java.util.ArrayList;
import java.util.List;

import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureRoot.AttributeGender;

public class FHatAttribute extends HeadgearAttribute {

    private static final List<AttributeDescription> TEXTURES = new ArrayList<AttributeDescription>() {

        private static final long serialVersionUID = 7580714146149497738L;

        {
        	add(new AttributeDescription(AttributeColor.BLUE, AttributeGender.FEMALE, "f_hat_blue.png", FHatAttribute.class));
            add(new AttributeDescription(AttributeColor.GREEN, AttributeGender.FEMALE, "f_hat_green.png", FHatAttribute.class));
            add(new AttributeDescription(AttributeColor.GREY, AttributeGender.FEMALE, "f_hat_grey.png", FHatAttribute.class));
            add(new AttributeDescription(AttributeColor.PURPLE, AttributeGender.FEMALE, "f_hat_purple.png", FHatAttribute.class));
            add(new AttributeDescription(AttributeColor.RED, AttributeGender.FEMALE, "f_hat_red.png", FHatAttribute.class));
            add(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.FEMALE, "f_hat_yellow.png", FHatAttribute.class));
        }
    };

    public static List<AttributeDescription> getTextures() {
        return TEXTURES;
    }
    
    public static AttributeGender getAttributeGender() {
		return AttributeGender.FEMALE;
	}
    

    public FHatAttribute(AttributeColor color, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(color, pTextureRegion, pVertexBufferObjectManager);
    }

}
