/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.rules;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.Context;
import android.util.Log;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureFactory;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureRoot;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute.AttributeColor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.hair.HairAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.head.HeadAttribute;

/**
 * Abstract game rule class. Contains common functions for all game rules.
 * 
 * @author Matyáš Latner
 */
public abstract class GameRule {


    protected Context                   context;
    private   RandomAccess				randomAccess;
    protected VertexBufferObjectManager vertexBufferObject;

    protected int                        numberOfCreatures;
    protected int                        numberOfThieves;
    
    private int							 groupNumber = 1;

    protected static final int                GENERATOR_TRY_COUNT               = 10000;
    protected static final String             RULE_LOG_PREFIX                   = "[RULE] ";
    protected static final CreatureDescriptor BAN_ATTRIBUTES_SET_FOR_GENERATING = new CreatureDescriptor() {
        {

            addDescription(new AttributeDescription(null, null, HeadAttribute.class));
            addDescription(new AttributeDescription(null, null, HairAttribute.class));
            disableGenderCompatibilityCheck();

        }
    };

    /*
     * Map for special creatures contains thieves and creatures which is important for thieves to be founded.
     */
    protected Map<Integer, CreatureDescriptor> specialCreatures;

    public GameRule(Context context, RandomAccess randomAccess, VertexBufferObjectManager vertexBufferObject, int numberOfCreatures, int numberOfThieves) {
        this.context = context;
        this.randomAccess = randomAccess;
        this.vertexBufferObject = vertexBufferObject;
        this.numberOfCreatures = numberOfCreatures;
        this.numberOfThieves = numberOfThieves;
        specialCreatures = new HashMap<Integer, CreatureDescriptor>();
        
        Log.d(this.getClass().getSimpleName(), " ------- " + RULE_LOG_PREFIX + " " + getClass().getSimpleName() + " starts generating creatures with seed: [" + getRandomAccess().getRandomSeed() + "] -------");
    }
    
    public RandomAccess getRandomAccess() {
		return randomAccess;
	}
    
    protected void incrementGroupNumber() {
    	groupNumber++;
    	Log.d(this.getClass().getSimpleName(), RULE_LOG_PREFIX + "GROUP COMPLETE");
	}
    
    
    /**
     * Add creature to the map of specials creatures at specific position
     * 
     * @param position key to special creatures map
     * @param specialCreature creature to add in
     */
    protected void addSpecialCreature(Integer position, CreatureDescriptor specialCreature) {
        if (specialCreature != null) {
        	CreatureDescriptor existingCreature = specialCreatures.get(position);
    		if (existingCreature != null) {
    			for (AttributeDescription attributeDescription : specialCreature.getDescriptions()) {
    				existingCreature.addDescription(attributeDescription);
    			}
    			Log.d(this.getClass().getSimpleName(), RULE_LOG_PREFIX + " P[" + position + "] SPECIAL added: " + specialCreature);
        	} else {
        		existingCreature = new CreatureDescriptor(specialCreature.getDescriptions());
        		specialCreatures.put(position, existingCreature);
        		Log.d(this.getClass().getSimpleName(), RULE_LOG_PREFIX + " P[" + position + "] SPECIAL generated: " + specialCreature);
        	}
        	existingCreature.setGroupNumber(groupNumber);
        	existingCreature.addCreatureOffsets(specialCreature.getCreatureOffsets());
        	existingCreature.setThief(existingCreature.isThief() || specialCreature.isThief());
        }
    }

    /**
     * Return random number from interval for thief position.
     * 
     * @param intervalStart start of interval
     * @param intervalSize size of interval
     * @return
     */
    protected int getThiefRandomPositionInInterval(int intervalStart, int intervalSize, int minValue) {
        int position = getRandomAccess().getRandom().nextInt(intervalSize);
        if (position < minValue) {
            position = minValue;
        }
        return intervalStart + position;
    }
    
    /**
     * Return random number from interval for thief position.
     * 
     * @param intervalStart start of interval
     * @param intervalSize size of interval
     * @return
     */
    protected int getThiefRandomPositionInInterval(int minValue, List<Integer> bannedPositions) {
    	int result = -1;
    	for (int i = 0; i < GENERATOR_TRY_COUNT; i++) {
    		int position = getRandomAccess().getRandom().nextInt(numberOfCreatures);
    		if (position < minValue) {
    			position = minValue;
    		}
    		if (bannedPositions == null || !bannedPositions.contains(position)) {
    			result = position;
    			break;
    		}
		}
        return result;
    }
    
    /**
     * Returns creature description with two random attributes.
     * 
     * @param descriptor random creature description from is choose random attributes
     * @return creature description with one random attribute
     */
    protected CreatureDescriptor getRandomCreatureDescriptionWithTwoAttributes(CreatureDescriptor descriptor) {
        CreatureDescriptor creatureDescriptor = new CreatureDescriptor();
        
        int randomDescriptionPosition1 = getRandomAccess().getRandom().nextInt(descriptor.getDescriptions().size());
        int randomDescriptionPosition2;
        for (int i = 0; i < GENERATOR_TRY_COUNT; i++) {
        	randomDescriptionPosition2 = getRandomAccess().getRandom().nextInt(descriptor.getDescriptions().size());
        	if (randomDescriptionPosition1 != randomDescriptionPosition2) {
        		creatureDescriptor.addDescription(descriptor.getDescriptions().get(randomDescriptionPosition1));
        		creatureDescriptor.addDescription(descriptor.getDescriptions().get(randomDescriptionPosition2));
        		return creatureDescriptor;
        	}
		}
        throw new IllegalStateException("Cannot select two different descriptions from creature description!");
    }
    
    /**
     * Returns creature description with N random attributes.
     * 
     * @param sourceCreatureDescriptor creature description for generating from
     * @param descriptionsNumber number of used attribute descriptions from creature description
     * @return creature description with N random attributes.
     */
    protected CreatureDescriptor getRandomCreatureDescriptionWithNAttributes(CreatureDescriptor sourceCreatureDescriptor, int descriptionsNumber) {
    	CreatureDescriptor creatureDescriptor = new CreatureDescriptor();
    	
    	for (int i = 0; i < descriptionsNumber; i++) {
    		creatureDescriptor.addDescription(getRandomAttributeDescriptionFromCreatureDescription(sourceCreatureDescriptor, creatureDescriptor.getDescriptions()));
		}
    	
    	return creatureDescriptor;
    }
    
    /**
     * Returns attribute description from source creature description which is not in bannedAttributeDescriptions list parameter
     * 
     * @param creatureDescription creature description for select attribute description from
     * @param bannedAttributeDescriptions list of banned attributes descriptions which is not selected from creature description
     * @return randomly selected attribute description from creature description in parameter
     */
    public AttributeDescription getRandomAttributeDescriptionFromCreatureDescription(CreatureDescriptor creatureDescriptor, List<AttributeDescription> bannedAttributeDescriptions) {
    	for (int i = 0; i < GENERATOR_TRY_COUNT; i++) {
    		AttributeDescription attributeDescription = creatureDescriptor.getDescriptions().get(getRandomAccess().getRandom().nextInt(creatureDescriptor.getDescriptions().size()));
        	if (!bannedAttributeDescriptions.contains(attributeDescription)) {
        		return attributeDescription;
        	}
		}
    	throw new IllegalStateException("Cannot select attribute description from creature description!");
	}

    /**
     * Returns creature description with one random attribute.
     * 
     * @param descriptor random creature description from is choose random attribute
     * @return creature description with one random attribute
     */
    protected CreatureDescriptor getRandomCreatureDescriptionWithOneAttribute(CreatureDescriptor descriptor) {
        CreatureDescriptor creatureDescriptor = new CreatureDescriptor();
        creatureDescriptor.addDescription(getRandomAttributeDescription(descriptor));

        return creatureDescriptor;
    }

    /**
     * Returns random attribute description from creature description
     * 
     * @param descriptor random creature description from is choose random attribute
     * @return random attribute description
     */
    protected AttributeDescription getRandomAttributeDescription(CreatureDescriptor descriptor) {
        List<AttributeDescription> descriptions = descriptor.getDescriptions();
        return descriptions.get(getRandomAccess().getRandom().nextInt(descriptions.size()));
    }

    /**
     * Returns attribute description.
     * 
     * @param attributeDescription attribute description class
     * @return attribute description
     */
    protected String getAttributeName(AttributeDescription attributeDescription, boolean useForcedVersion) {
    	return getAttributeName(attributeDescription.getAttributeClass(), useForcedVersion);
    }
    
    /**
     * Returns attribute description.
     * 
     * @param attributeClass attribute class
     * @return attribute description
     */
    protected String getAttributeName(Class<? extends Attribute> attributeClass, boolean useForcedVersion) {
        return Attribute.getTextDescriptionForAttributeClass(attributeClass, context, useForcedVersion);
    }

    /**
     * Returns color description. If there is no color returns empty string.
     * 
     * @param attributeDescription attribute description for obtain color from
     * @return color description
     */
    protected String getAttributeColorName(AttributeDescription attributeDescription) {
        AttributeColor attributeColor = attributeDescription.getAttributeColor();
        return attributeColor != null ? context.getString(attributeColor.getDescriptionResourceId()) : "";
    }

    /**
     * Returns random color from randomly generated creature without any banned attributes from parameter
     * 
     * @param banAttributes banned attributes
     * @param vertexBufferObject andengine vertex buffer
     * @return randomly generated color
     */
    protected AttributeColor getRandomColorFromGeneratedCreature(CreatureDescriptor banAttributes, VertexBufferObjectManager vertexBufferObject) {
        AttributeColor randomColor = null;
        int tryCount = 0;
        while (randomColor == null) {
        	CreatureDescriptor randomCreature = CreatureFactory.getInstance().generateCreature(null, banAttributes, vertexBufferObject, getRandomAccess()).getCreatureDescrition();
            List<AttributeDescription> randomDescriptions = randomCreature.getDescriptions();
        	AttributeDescription randomDescription = randomDescriptions.get(getRandomAccess().getRandom().nextInt(randomDescriptions.size()));
        	randomColor = randomDescription.getAttributeColor();
        	if (tryCount >= GENERATOR_TRY_COUNT) {
        		throw new IllegalStateException("Cannot generate random color!");
        	}
        	tryCount++;
		}
        
        return randomColor;
    }

    /**
     * Adds random creature which has not thieve attribute when there is creature before with "before thief attribute"
     * 
     * @param creatures list of creatures
     * @param lastCreature last generated creature
     * @param t1CreatureDescription description of creature before thief
     * @param t0CreatureDescription description of thief
     * @param vertexBufferObject andengine vertex buffer
     */
    protected void addRandomCreatureAndCheckCreatureOneBefore(List<CreatureRoot> creatures, CreatureRoot lastCreature, CreatureDescriptor t1CreatureDescription, CreatureDescriptor t0CreatureDescription, VertexBufferObjectManager vertexBufferObject) {
        if ((lastCreature != null) && lastCreature.hasAttributes(t1CreatureDescription)) {
            creatures.add(CreatureFactory.getInstance().generateCreature(null, t0CreatureDescription, vertexBufferObject, getRandomAccess()));
        } else {
            creatures.add(CreatureFactory.getInstance().generateCreature(vertexBufferObject, getRandomAccess()));
        }
    }

    /**
     * Returns array of strings with parameters to the rule message
     * 
     * @return array of strings with parameters to the rule message
     */
    public String[] getRuleMessageParameters() {
        String[] messageParameters = prepareRuleMessageParameters();
        if (Tablexia.IS_LOGGING_ENABLED && (messageParameters != null)) {
            for (String parameter : messageParameters) {
                Log.d(this.getClass().getSimpleName(), RULE_LOG_PREFIX + "parameter: " + parameter);
            }
        }
        return messageParameters;
    }

    /**
     * Generate new creature on n - 1 position without before thief creature attribute for special creature on position n
     * which is not thief and has thief attribute. When creature on n - 1 position has beforeThiefAttribute.
     * n = lastPosition parameter
     * 
     * @param creatures list of creatures to add in
     * @param lastPosition position n
     * @param lastCreature last generated creature
     * @param specialCreature special creature to check
     * @param thiefCreatureDescription description of thief
     * @param beforeThiefAttributeDescription description of creature before thief
     * @param beforeThiefCreatureDescription complete description of creature before thief
     */
    protected void checkBeforeSpecialCreature(List<CreatureRoot> creatures, int lastPosition, CreatureRoot lastCreature, CreatureRoot specialCreature, CreatureDescriptor thiefCreatureDescription, CreatureDescriptor beforeThiefCreatureDescription) {
        if (!specialCreature.isThief() && specialCreature.hasAttributes(thiefCreatureDescription)) {
            if ((lastCreature != null) && lastCreature.hasAttributes(beforeThiefCreatureDescription)) {
                creatures.remove(lastPosition);
                creatures.add(CreatureFactory.getInstance().generateCreature(null, beforeThiefCreatureDescription, vertexBufferObject, getRandomAccess()));
            }
        }
    }
    
    /**
     * Check compatibility for creature at specified position and creature from parameter 
     * @param creatureDescriptor creature to check
     * @param creaturePosition position of creature to check
     * @param isThief <code>true</code> if creature description from parameter is thief
     * @return <code>true</code> if creature description from parameter is compatible with creature in position
     */
    protected boolean checkCreatureCompactibilityForPosition(CreatureDescriptor creatureDescriptor, int creaturePosition, boolean isThief) {
    	CreatureDescriptor specialCreature = specialCreatures.get(creaturePosition);
		return specialCreature != null ? !(specialCreature.isThief() && isThief) && (specialCreature.checkDescriptionCompactibility(creatureDescriptor)) : true;
	}

    /**
     * Returns array of strings with parameters to the rule message. Specific rule class implementation
     * 
     * @return array of strings with parameters to the rule message
     */
    public abstract String[] prepareRuleMessageParameters();

    /**
     * Returns list of creatures for displaying in game. List contains random creatures and thieves.
     * 
     * @return list of random creatures and thieves
     */
    public List<CreatureRoot> generateCreatures() {
    	prepareCreatureDescriptionsC();
        List<CreatureRoot> generatedCreatures = prepareCreatures();
        if (Tablexia.IS_LOGGING_ENABLED) {
        	for (int i = 0; i < generatedCreatures.size(); i++) {
                Log.d(this.getClass().getSimpleName(), RULE_LOG_PREFIX + " ---> [" + i + "] generated creature: " + generatedCreatures.get(i));
            }
        }
        return generatedCreatures;
    }
    
    protected abstract void prepareCreatureDescriptionsC();

    /**
     * Returns list of creatures for displaying in game. List contains random creatures and thieves. Specific rule class implementation.
     * 
     * @return list of random creatures and thieves
     */
    public abstract List<CreatureRoot> prepareCreatures();

}
