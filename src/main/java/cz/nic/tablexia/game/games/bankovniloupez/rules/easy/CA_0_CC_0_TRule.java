/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.rules.easy;

import java.util.ArrayList;
import java.util.List;

import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.Context;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureFactory;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureRoot;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute.AttributeColor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.ClothingAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.rules.GameRuleUtility;

/**
 * 
 * @author Matyáš Latner
 */
public class CA_0_CC_0_TRule extends GameRuleUtility {
    
	private static final double 	BAIT_RANDOM_DISTRIBUTION = 0.3;
	private   static final int 	 	GROUP_SIZE	= 3;
	protected static final Integer 	T0_OFFSET 	= CreatureDescriptor.THIEF_OFFSET;
	protected static final Integer 	T1_OFFSET 	= Integer.valueOf(1);
	protected static final Integer 	T2_OFFSET 	= Integer.valueOf(2);

    public CA_0_CC_0_TRule(Context context, RandomAccess randomAccess, VertexBufferObjectManager vertexBufferObject, int numberOfCreatures, int numberOfThieves) {
        super(context, randomAccess, vertexBufferObject, numberOfCreatures, numberOfThieves, GROUP_SIZE);
    }
    
    @Override
    public String[] prepareRuleMessageParameters() {
        return new String[] {
        		getAttributeColorName(getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0)),
                getAttributeColorName(getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0)),
                getAttributeName(getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0), false),
        };
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
        AttributeColor t2CommonAttributeColor = getRandomColorFromGeneratedCreature(BAN_ATTRIBUTES_SET_FOR_GENERATING, vertexBufferObject);
        CreatureDescriptor t2CreatureDescriptor = new CreatureDescriptor();
        t2CreatureDescriptor.addDescription(new AttributeDescription(t2CommonAttributeColor, null, ClothingAttribute.class));
        addGlobalCreatureDescriptor(T2_OFFSET, t2CreatureDescriptor);
        
        addGlobalCreatureDescriptor(T1_OFFSET, getRandomCreatureDescriptionWithNAttributes(CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, vertexBufferObject, getRandomAccess()).getCreatureDescrition(), 1));
        
        addGlobalCreatureDescriptor(T0_OFFSET, new CreatureDescriptor());
    }
    
    @Override
    protected List<CreatureRoot> prepareCreatureDescriptionsA() {
    	List<CreatureRoot> creatures = new ArrayList<CreatureRoot>();
        CreatureRoot lastCreature = null;
        int lastPosition;
        // generate random creatures and add thieves and pair creatures to the specific positions
        for (int i = 0; i < numberOfCreatures; i++) {
            lastPosition = creatures.size() - 1;
            if (lastPosition >= 0) {
                lastCreature = creatures.get(lastPosition);
            }

            CreatureDescriptor creatureDescriptor = specialCreatures.get(i);
            if (creatureDescriptor != null) { // add special creature
            	if (lastCreature != null && lastCreature.hasAttribute(getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0)) && !creatureDescriptor.containsAttributeDescription(getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0))) {
            		creatures.add(CreatureFactory.getInstance().generateCreature(creatureDescriptor, getGlobalCreatureDescriptor(T1_OFFSET), vertexBufferObject, getRandomAccess()));
            	} else {            		
            		creatures.add(CreatureFactory.getInstance().generateCreature(creatureDescriptor, null, vertexBufferObject, getRandomAccess()));
            	}
            } else {
            	if ((lastCreature != null) && lastCreature.hasAttributes(getGlobalCreatureDescriptor(T2_OFFSET))) {
                    creatures.add(CreatureFactory.getInstance().generateCreature(null, getGlobalCreatureDescriptor(T1_OFFSET), vertexBufferObject, getRandomAccess()));
                } else {
                	
                	if(getRandomAccess().getRandom().nextDouble() < BAIT_RANDOM_DISTRIBUTION) {
                		newBaitCreatureDescription(getGlobalCreatureDescriptor(T2_OFFSET));
                	} else {
                		newBaitCreatureDescription(getGlobalCreatureDescriptor(T1_OFFSET));
                	}
                	
                    creatures.add(CreatureFactory.getInstance().generateCreature(getBaitCreatureDescriptionRandomly(), null, vertexBufferObject, getRandomAccess()));
                }
            }
        }

        return creatures;
    }

}
