
package cz.nic.tablexia.game.games.nocnisledovani;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import android.graphics.Point;
import cz.nic.tablexia.game.games.nocnisledovani.model.Trio;

/*
 * Helper class for Nocni sledovani game
 */
public class NocniSledovaniHelper {
    private static final Map<Trio<Integer, Integer, Integer>, Integer> COLOR_MAP;
    static {
        Map<Trio<Integer, Integer, Integer>, Integer> aMap = new HashMap<Trio<Integer, Integer, Integer>, Integer>();
        aMap.put(new Trio<Integer, Integer, Integer>(110, 120, 130), 0);
        aMap.put(new Trio<Integer, Integer, Integer>(255, 255, 0), 1);
        aMap.put(new Trio<Integer, Integer, Integer>(255, 0, 0), 2);
        aMap.put(new Trio<Integer, Integer, Integer>(255, 0, 255), 3);
        aMap.put(new Trio<Integer, Integer, Integer>(0, 0, 255), 4);
        aMap.put(new Trio<Integer, Integer, Integer>(0, 255, 255), 5);
        aMap.put(new Trio<Integer, Integer, Integer>(0, 255, 0), 6);
        aMap.put(new Trio<Integer, Integer, Integer>(255, 255, 255), 7);
        aMap.put(new Trio<Integer, Integer, Integer>(255, 100, 0), 8);
        aMap.put(new Trio<Integer, Integer, Integer>(255, 0, 100), 9);
        aMap.put(new Trio<Integer, Integer, Integer>(255, 100, 100), 10);
        aMap.put(new Trio<Integer, Integer, Integer>(100, 100, 255), 11);
        aMap.put(new Trio<Integer, Integer, Integer>(0, 255, 100), 12);
        aMap.put(new Trio<Integer, Integer, Integer>(0, 100, 255), 13);
        aMap.put(new Trio<Integer, Integer, Integer>(100, 255, 255), 14);
        aMap.put(new Trio<Integer, Integer, Integer>(100, 100, 100), 15);
        aMap.put(new Trio<Integer, Integer, Integer>(200, 100, 255), 16);
        aMap.put(new Trio<Integer, Integer, Integer>(200, 200, 255), 17);
        aMap.put(new Trio<Integer, Integer, Integer>(100, 200, 100), 18);
        aMap.put(new Trio<Integer, Integer, Integer>(200, 255, 255), 19);
        aMap.put(new Trio<Integer, Integer, Integer>(200, 200, 200), 20);
        aMap.put(new Trio<Integer, Integer, Integer>(100, 200, 255), 21);
        aMap.put(new Trio<Integer, Integer, Integer>(255, 100, 200), 22);
        aMap.put(new Trio<Integer, Integer, Integer>(255, 200, 0), 23);
        aMap.put(new Trio<Integer, Integer, Integer>(150, 255, 200), 24);
        aMap.put(new Trio<Integer, Integer, Integer>(150, 255, 100), 25);
        aMap.put(new Trio<Integer, Integer, Integer>(255, 255, 100), 26);
        aMap.put(new Trio<Integer, Integer, Integer>(255, 150, 100), 27);
        aMap.put(new Trio<Integer, Integer, Integer>(150, 150, 100), 28);
        aMap.put(new Trio<Integer, Integer, Integer>(150, 255, 150), 29);
        aMap.put(new Trio<Integer, Integer, Integer>(255, 150, 150), 30);
        aMap.put(new Trio<Integer, Integer, Integer>(150, 150, 255), 31);
        COLOR_MAP = Collections.unmodifiableMap(aMap);
        if (COLOR_MAP.size() != 32) {
            throw new IllegalStateException("Spatne nadefinovane barvy");
        }
    }
    private static final Map<Integer, Point>                           WINDOWS_POSITION_MAP_SCENE_ZERO;
    static {
        Map<Integer, Point> sceneZeroWindowsPositions = new HashMap<Integer, Point>();
        sceneZeroWindowsPositions.put(0, new Point(77, 111));// DONE
        sceneZeroWindowsPositions.put(1, new Point(77, 250));// DONE
        sceneZeroWindowsPositions.put(2, new Point(180, 258));// DONE
        sceneZeroWindowsPositions.put(3, new Point(220, 208));// DONE
        sceneZeroWindowsPositions.put(4, new Point(305, 150));// DONE
        sceneZeroWindowsPositions.put(5, new Point(331, 156));// DONE
        sceneZeroWindowsPositions.put(6, new Point(362, 165));// DONE
        sceneZeroWindowsPositions.put(7, new Point(399, 183));// DONE
        WINDOWS_POSITION_MAP_SCENE_ZERO = Collections.unmodifiableMap(sceneZeroWindowsPositions);
        if (WINDOWS_POSITION_MAP_SCENE_ZERO.size() != 8) {
            throw new IllegalStateException("Spatne nadefinovane pozice oken");
        }

    }

    private static final Map<Integer, Point>                           WINDOWS_POSITION_MAP_SCENE_ONE;
    static {
        Map<Integer, Point> sceneOneWindowsPositions = new HashMap<Integer, Point>();
        sceneOneWindowsPositions.put(0, new Point(59, 67));// DONE
        sceneOneWindowsPositions.put(1, new Point(51, 208));// DONE
        sceneOneWindowsPositions.put(2, new Point(95, 175));// DONE
        sceneOneWindowsPositions.put(3, new Point(130, 148));// DONE
        sceneOneWindowsPositions.put(4, new Point(105, 265));// DONE
        sceneOneWindowsPositions.put(5, new Point(133, 224));// DONE
        sceneOneWindowsPositions.put(6, new Point(161, 208));// DONE
        sceneOneWindowsPositions.put(7, new Point(172, 272));// DONE
        sceneOneWindowsPositions.put(8, new Point(257, 263));// DONE
        sceneOneWindowsPositions.put(9, new Point(285, 271));// DONE
        sceneOneWindowsPositions.put(10, new Point(256, 197));// DONE
        sceneOneWindowsPositions.put(11, new Point(310, 267));// DONE
        sceneOneWindowsPositions.put(12, new Point(316, 202));// DONE
        sceneOneWindowsPositions.put(13, new Point(348, 260));// DONE
        sceneOneWindowsPositions.put(14, new Point(371, 191));// DONE
        sceneOneWindowsPositions.put(15, new Point(428, 214));// DONE
        WINDOWS_POSITION_MAP_SCENE_ONE = Collections.unmodifiableMap(sceneOneWindowsPositions);
        if (WINDOWS_POSITION_MAP_SCENE_ONE.size() != 16) {
            throw new IllegalStateException("Spatne nadefinovane pozice oken");
        }

    }

    private static final Map<Integer, Point>                           WINDOWS_POSITION_MAP_SCENE_TWO;
    static {
        Map<Integer, Point> sceneOneWindowsPositions = new HashMap<Integer, Point>();
        sceneOneWindowsPositions.put(0, new Point(30, 114));// DONE
        sceneOneWindowsPositions.put(1, new Point(80, 115));// DONE
        sceneOneWindowsPositions.put(2, new Point(63, 253));// DONE
        sceneOneWindowsPositions.put(3, new Point(250, 235));// DONE
        sceneOneWindowsPositions.put(4, new Point(142, 92));// DONE
        sceneOneWindowsPositions.put(5, new Point(150, 159));// DONE
        sceneOneWindowsPositions.put(6, new Point(190, 165));// DONE
        sceneOneWindowsPositions.put(7, new Point(207, 162));// DONE
        sceneOneWindowsPositions.put(8, new Point(190, 123));// DONE
        sceneOneWindowsPositions.put(9, new Point(228, 195));// DONE
        sceneOneWindowsPositions.put(10, new Point(272, 196));// DONE
        sceneOneWindowsPositions.put(11, new Point(236, 151));// DONE
        sceneOneWindowsPositions.put(12, new Point(280, 151));// DONE
        sceneOneWindowsPositions.put(13, new Point(330, 243));// DONE
        sceneOneWindowsPositions.put(14, new Point(336, 185));// DONE
        sceneOneWindowsPositions.put(15, new Point(334, 139));// DONE
        sceneOneWindowsPositions.put(16, new Point(362, 260));// DONE
        sceneOneWindowsPositions.put(17, new Point(356, 193));// DONE
        sceneOneWindowsPositions.put(18, new Point(363, 138));// DONE
        sceneOneWindowsPositions.put(19, new Point(396, 279));// DONE
        sceneOneWindowsPositions.put(20, new Point(402, 195));// DONE
        sceneOneWindowsPositions.put(21, new Point(440, 204));// DONE
        sceneOneWindowsPositions.put(22, new Point(401, 132));// DONE
        sceneOneWindowsPositions.put(23, new Point(449, 110));// DONE
        WINDOWS_POSITION_MAP_SCENE_TWO = Collections.unmodifiableMap(sceneOneWindowsPositions);
        if (WINDOWS_POSITION_MAP_SCENE_TWO.size() != 24) {
            throw new IllegalStateException("Spatne nadefinovane pozice oken");
        }

    }
    private static final Map<Integer, Point>                           WINDOWS_POSITION_MAP_SCENE_THREE;
    static {
        Map<Integer, Point> sceneOneWindowsPositions = new HashMap<Integer, Point>();
        sceneOneWindowsPositions.put(0, new Point(21, 180));// DONE
        sceneOneWindowsPositions.put(1, new Point(58, 178));// DONE
        sceneOneWindowsPositions.put(2, new Point(94, 176));// DONE
        sceneOneWindowsPositions.put(3, new Point(153, 232));// DONE
        sceneOneWindowsPositions.put(4, new Point(80, 247));// DONE
        sceneOneWindowsPositions.put(5, new Point(19, 104));// DONE
        sceneOneWindowsPositions.put(6, new Point(58, 108));// DONE
        sceneOneWindowsPositions.put(7, new Point(92, 111));// DONE
        sceneOneWindowsPositions.put(8, new Point(126, 151));// DONE
        sceneOneWindowsPositions.put(9, new Point(126, 196));// DONE
        sceneOneWindowsPositions.put(10, new Point(153, 195));// DONE
        sceneOneWindowsPositions.put(11, new Point(153, 148));// DONE
        sceneOneWindowsPositions.put(12, new Point(180, 190));// DONE
        sceneOneWindowsPositions.put(13, new Point(185, 147));// DONE
        sceneOneWindowsPositions.put(14, new Point(241, 195));// DONE
        sceneOneWindowsPositions.put(15, new Point(266, 202));// DONE
        sceneOneWindowsPositions.put(16, new Point(296, 208));// DONE
        sceneOneWindowsPositions.put(17, new Point(238, 145));// DONE
        sceneOneWindowsPositions.put(18, new Point(266, 150));// DONE
        sceneOneWindowsPositions.put(19, new Point(296, 149));// DONE
        sceneOneWindowsPositions.put(20, new Point(359, 228));// DONE
        sceneOneWindowsPositions.put(21, new Point(432, 246));// DONE
        sceneOneWindowsPositions.put(22, new Point(337, 173));// DONE
        sceneOneWindowsPositions.put(23, new Point(364, 175));// DONE
        sceneOneWindowsPositions.put(24, new Point(392, 176));// DONE
        sceneOneWindowsPositions.put(25, new Point(424, 180));// DONE
        sceneOneWindowsPositions.put(26, new Point(462, 182));// DONE
        sceneOneWindowsPositions.put(27, new Point(338, 134));// DONE
        sceneOneWindowsPositions.put(28, new Point(364, 134));// DONE
        sceneOneWindowsPositions.put(29, new Point(393, 131));// DONE
        sceneOneWindowsPositions.put(30, new Point(413, 112));// DONE
        sceneOneWindowsPositions.put(31, new Point(462, 127));// DONE
        WINDOWS_POSITION_MAP_SCENE_THREE = Collections.unmodifiableMap(sceneOneWindowsPositions);
        if (WINDOWS_POSITION_MAP_SCENE_THREE.size() != 32) {
            throw new IllegalStateException("Spatne nadefinovane pozice oken");
        }

    }

    public static int getAngleFromLinearApproximationOfPoints(Point pointOfTouch, Point spriteRotationCenter) {
        float deltax = (float) pointOfTouch.x - spriteRotationCenter.x;
        float deltay = (float) pointOfTouch.y - spriteRotationCenter.y;
        int angleInDeg = angle(deltax, -deltay);
        return angleInDeg;
    }

    public static int angle(float x, float y) {
        if ((x == 0) && (y == 0)) {
            return 0;
        }
        float angle = (float) Math.toDegrees(Math.atan(y / x));

        if (x >= 0) {
            angle += 90;
        } else {
            angle += 270;
        }

        return (int) angle;
    }

    public static Map<Trio<Integer, Integer, Integer>, Integer> getColorMap() {
        return COLOR_MAP;
    }

    public static Map<Integer, Point> getWindowsPositionsMap(int sceneNumber) {
        switch (sceneNumber) {
            case 0:
                return WINDOWS_POSITION_MAP_SCENE_ZERO;
            case 1:
                return WINDOWS_POSITION_MAP_SCENE_ONE;
            case 2:
                return WINDOWS_POSITION_MAP_SCENE_TWO;
            case 3:
                return WINDOWS_POSITION_MAP_SCENE_THREE;

            default:
                throw new IllegalArgumentException();
        }
    }

    public static int round(double d) {
        double dAbs = Math.abs(d);
        int i = (int) dAbs;
        double result = dAbs - i;
        if (result < 0.5) {
            return d < 0 ? -i : i;
        } else {
            return d < 0 ? -(i + 1) : i + 1;
        }
    }

}
