/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.pronasledovani;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.audio.sound.SoundManager;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;

import com.activeandroid.util.Log;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.game.games.pronasledovani.model.MapSetup;
import cz.nic.tablexia.game.games.pronasledovani.model.Position;

public class CarDragTouchListener implements IOnSceneTouchListener {
    private static final int       CIRCLE_RADIUS             = 100;
    private static final int       COORDINATES_FOR_DIRECTION = 5;

    private Context                context;
    private Sprite                 carSprite;
    private MapSetup               map;
    private Point                  offsets;
    private Bitmap                 roadMap;
    private double                 mapRatio;
    private boolean                carStillOnTheRoad;
    private Position[]             lastFiveCarCoordinates;
    private int                    coordinationArrayPointer;
    private Rectangle              carRectangle;
    private Rectangle              flagRectangle;
    private OnDragCompleteListener dragCompleteListener;
    private boolean                gameFinished;
    private final Tablexia         tablexia;
    private final SoundManager     soundManager;
    private Sound[]                movingSounds;
    private Timer                  timer;
    private Random                 random                    = new Random();
    private VehicleTypeEnum        vehicleType;

    public CarDragTouchListener(Context context, Point carCenter, Sprite carSprite, Point offsets, MapSetup map, Rectangle carRectangle, Rectangle flagRectangle, OnDragCompleteListener dragCompleteListener, SoundManager soundManager, VehicleTypeEnum vehicleType) {
        super();
        this.vehicleType = vehicleType;
        this.soundManager = soundManager;
        this.context = context;
        tablexia = (Tablexia) context.getApplicationContext();
        this.carSprite = carSprite;
        this.offsets = offsets;
        this.map = map;
        this.carRectangle = carRectangle;
        this.flagRectangle = flagRectangle;
        this.dragCompleteListener = dragCompleteListener;
        lastFiveCarCoordinates = new Position[COORDINATES_FOR_DIRECTION];
        try {
            roadMap = downsampleBitmap(map.getName(), map.getScaleFactor());
            int sMWidth = roadMap.getWidth();
            int largeMapWidth = map.getWidth();

            mapRatio = (double) largeMapWidth / (double) sMWidth;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        switch (vehicleType) {
            case CAR:
                movingSounds = new Sound[4];
                movingSounds[0] = SoundFactory.createSoundFromAssetFileDescriptor(this.soundManager, tablexia.getZipResourceFile().getAssetFileDescriptor("pronasledovani/sfx/auto_1.mp3"));
                movingSounds[1] = SoundFactory.createSoundFromAssetFileDescriptor(this.soundManager, tablexia.getZipResourceFile().getAssetFileDescriptor("pronasledovani/sfx/auto_2.mp3"));
                movingSounds[2] = SoundFactory.createSoundFromAssetFileDescriptor(this.soundManager, tablexia.getZipResourceFile().getAssetFileDescriptor("pronasledovani/sfx/auto_3.mp3"));
                movingSounds[3] = SoundFactory.createSoundFromAssetFileDescriptor(this.soundManager, tablexia.getZipResourceFile().getAssetFileDescriptor("pronasledovani/sfx/auto_4.mp3"));
                break;
            case BOAT:
                movingSounds = new Sound[1];
                movingSounds[0] = SoundFactory.createSoundFromAssetFileDescriptor(this.soundManager, tablexia.getZipResourceFile().getAssetFileDescriptor("pronasledovani/sfx/lod_1.mp3"));
                break;
            case TRAIN:
                movingSounds = new Sound[1];
                movingSounds[0] = SoundFactory.createSoundFromAssetFileDescriptor(this.soundManager, tablexia.getZipResourceFile().getAssetFileDescriptor("pronasledovani/sfx/train_1.mp3"));
                break;
            case MOTORCYCLE:
                movingSounds = new Sound[1];
                movingSounds[0] = SoundFactory.createSoundFromAssetFileDescriptor(this.soundManager, tablexia.getZipResourceFile().getAssetFileDescriptor("pronasledovani/sfx/motorcycle_1.mp3"));
                break;
            default:
                throw new IllegalArgumentException("Illegal vehicle type");
        }

        if (isCar()) {

        } else {

        }
    }

    private boolean isCar() {
        return carSprite.getTag() == -20;
    }

    // (xi-x)^2 + (yi-y)^2 <= d ^2
    private boolean isPointWithinTheCircle(Point p, int circleRadius, Point carCenter) {
        if ((Math.pow((p.x - carCenter.x), 2) + Math.pow((p.y - carCenter.y), 2)) <= Math.pow(circleRadius, 2)) {
            return true;
        }
        return false;
    }

    private boolean isPointOnMap(Point p) {
        if ((p.x >= (offsets.x)) && (p.x <= (map.getWidth() + offsets.x)) && (p.y >= (offsets.y)) && (p.y <= (map.getWidth() + offsets.y))) {
            return true;
        }
        return false;
    }

    private Bitmap downsampleBitmap(String bitmapSrc, int sampleSize) throws FileNotFoundException, IOException {
        String path = bitmapSrc;
        Bitmap resizedBitmap;
        BitmapFactory.Options outBitmap = new BitmapFactory.Options();
        outBitmap.inJustDecodeBounds = false; // the decoder will return a bitmap
        outBitmap.inSampleSize = sampleSize;

        resizedBitmap = BitmapFactory.decodeStream(tablexia.getZipResourceFile().getInputStream(path), null, outBitmap);

        return resizedBitmap;
    }

    /*
     * Vrati uhel mezi dvema body, ktere lezi na primce, ktera aproximuje body predane v poli
     */
    private int getAngleFromLinearApproximationOfPoints(Position[] points) {
        float deltax = (float) points[4].getX() - (float) points[3].getX();
        float deltay = (float) points[4].getY() - (float) points[3].getY();
        int angleInDeg = angle(deltax, -deltay);
        coordinationArrayPointer = 0;
        return angleInDeg;
    }

    private static int angle(float x, float y) {
        if ((x == 0) && (y == 0)) {
            return 0;
        }
        float angle = (float) Math.toDegrees(Math.atan(y / x));

        if (x >= 0) {
            angle += 90;
        } else {
            angle += 270;
        }

        return (int) angle;
    }

    @Override
    public boolean onSceneTouchEvent(Scene arg0, TouchEvent event) {
        if (!gameFinished) {
            Log.d("Touch event");
            Point p = new Point((int) event.getX(), (int) event.getY());

            switch (event.getAction()) {
                case TouchEvent.ACTION_DOWN:
                    Log.d("ON_DOWN");
                    if (isPointWithinTheCircle(p, CIRCLE_RADIUS, new Point((int) carSprite.getX(), (int) carSprite.getY())) && isPointOnMap(p)) {
                        Position pixelTouchedOnMiniMap = new Position((int) ((event.getX() - offsets.x) / mapRatio), (int) ((map.getWidth() - (event.getY() - offsets.y)) / mapRatio));
                        Position pixelTouchedPositionTransformedByMapRotation = PronasledovaniHelper.translatePositionAccordingToRotation(pixelTouchedOnMiniMap, map.getRotation() * 90, new Position(roadMap.getWidth() / 2, roadMap.getHeight() / 2));
                        int pixel = roadMap.getPixel(pixelTouchedPositionTransformedByMapRotation.getX(), pixelTouchedPositionTransformedByMapRotation.getY());
                        if (Color.red(pixel) == 255) {
                            carSprite.setPosition(event.getX(), event.getY());
                            carRectangle.setPosition(event.getX(), event.getY());
                            carStillOnTheRoad = true;
                            startSound();
                            lastFiveCarCoordinates[coordinationArrayPointer] = new Position((int) event.getX(), (int) event.getY());
                        }
                    }
                    break;
                case TouchEvent.ACTION_MOVE:
                    Log.d("ON_MOVE");
                    coordinationArrayPointer = (coordinationArrayPointer + 1) % lastFiveCarCoordinates.length;
                    lastFiveCarCoordinates[coordinationArrayPointer] = new Position((int) event.getX(), (int) event.getY());
                    if (isPointOnMap(p)) {
                        Log.d("point on map");
                        if (carStillOnTheRoad) {
                            Log.d("still on road");
                            if (coordinationArrayPointer == (COORDINATES_FOR_DIRECTION - 1)) {
                                carSprite.setRotation(getAngleFromLinearApproximationOfPoints(lastFiveCarCoordinates));
                            }
                            Position pixelTouchedOnMiniMap = new Position((int) ((event.getX() - offsets.x) / mapRatio), (int) ((map.getWidth() - (event.getY() - offsets.y)) / mapRatio));
                            Position pixelTouchedPositionTransformedByMapRotation = PronasledovaniHelper.translatePositionAccordingToRotation(pixelTouchedOnMiniMap, map.getRotation() * 90, new Position(roadMap.getWidth() / 2, roadMap.getHeight() / 2));
                            int pixel = roadMap.getPixel(pixelTouchedPositionTransformedByMapRotation.getX(), pixelTouchedPositionTransformedByMapRotation.getY());
                            if (Color.red(pixel) == 255) {
                                carSprite.setPosition(event.getX(), event.getY());
                                carRectangle.setPosition(event.getX(), event.getY());
                                if (flagRectangle.collidesWith(carRectangle)) {
                                    Log.d("collides");
                                    dragCompleteListener.onDragComplete();
                                    stopSound(true);
                                    carSprite.setVisible(false);
                                    carStillOnTheRoad = false;
                                    recycleBitmaps();
                                    gameFinished = true;

                                }
                            } else {
                                Log.d("not on road");
                                carStillOnTheRoad = false;
                                stopSound(false);
                            }
                        } else {
                            // jestli jsem v kruhu, jestli jsem na cerveny a pokud jo, tak carstillontheroad a pozice pod prst
                            if (isPointWithinTheCircle(p, CIRCLE_RADIUS, new Point((int) carSprite.getX(), (int) carSprite.getY()))) {
                                Position pixelTouchedOnMiniMap = new Position((int) ((event.getX() - offsets.x) / mapRatio), (int) ((map.getWidth() - (event.getY() - offsets.y)) / mapRatio));
                                Position pixelTouchedPositionTransformedByMapRotation = PronasledovaniHelper.translatePositionAccordingToRotation(pixelTouchedOnMiniMap, map.getRotation() * 90, new Position(roadMap.getWidth() / 2, roadMap.getHeight() / 2));
                                int pixel = roadMap.getPixel(pixelTouchedPositionTransformedByMapRotation.getX(), pixelTouchedPositionTransformedByMapRotation.getY());
                                if (Color.red(pixel) == 255) {
                                    carStillOnTheRoad = true;
                                    startSound();
                                    carSprite.setPosition(event.getX(), event.getY());
                                    carRectangle.setPosition(event.getX(), event.getY());
                                }
                            }
                        }
                    }
                    break;
                default:
                    stopSound(false);
                    break;

            }
        }
        return true;
    }

    /**
     * Začne přehrávat zvuk motoru prostředku, kterým se ohybujeme
     */
    private void startSound() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                movingSounds[random.nextInt(movingSounds.length)].play();
            }
        }, 0, isCar() ? 1800 : 3000);
    }

    /**
     * Zruší opakované přehrávání
     */
    private void stopSound(boolean now) {
        if (timer != null) {
            timer.cancel();
        }
        if (now || !isCar()) {
            for (Sound sound : movingSounds) {
                sound.stop();
            }
        }
    }

    public void recycleBitmaps() {
        roadMap.recycle();
    }

    public static interface OnDragCompleteListener {
        public void onDragComplete();
    }
}
