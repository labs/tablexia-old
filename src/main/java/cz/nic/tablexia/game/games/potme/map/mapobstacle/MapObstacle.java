/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.potme.map.mapobstacle;

import org.andengine.entity.Entity;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import cz.nic.tablexia.game.games.potme.PotmeActivity;
import cz.nic.tablexia.game.games.potme.ResourceManager;
import cz.nic.tablexia.game.games.potme.map.tile.Tile;


/**
 * Map obstacle
 * 
 * @author Matyáš Latner
 *
 */
public class MapObstacle {
	
	private static final int 	ANIMATION_FRAME_DURATION 	= 100;
	private static final int 	QUARTER_ROTATION 			= 90;
	
	private MapObstacleType 	mapObstacleType;
	private Entity				mapObstacleContainer;
	private Sprite				mapObstacleActiveSprite;
	private Sprite				mapObstacleInactiveSprite;
	private Tile 				tile;

	private int 				offsetX;
	private int 				offsetY;
	
	private boolean				active;

	public MapObstacle(Tile tile, int offsetX, int offsetY, MapObstacleType mapObstacleType) {
		this.tile = tile;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		this.mapObstacleType = mapObstacleType;
		this.active = true;
		this.mapObstacleContainer = new Entity();
	}
	
	public MapObstacleType getMapObstacleType() {
		return mapObstacleType;
	}
	
	public boolean isEnabled() {
		return active;
	}
	
	public void setActive() {
		this.active = true;
		if (mapObstacleActiveSprite != null) {			
			mapObstacleContainer.detachChildren();
			mapObstacleContainer.attachChild(mapObstacleActiveSprite);
		}
	}
	
	public void setInactive() {
		this.active = false;
		if (mapObstacleInactiveSprite != null) {			
			mapObstacleContainer.detachChildren();
			mapObstacleContainer.attachChild(mapObstacleInactiveSprite);
		}
	}
	
	public void reactWithPlayer() {
		if (mapObstacleActiveSprite != null && mapObstacleType.isAnimated()) {
			((AnimatedSprite)mapObstacleActiveSprite).animate(ANIMATION_FRAME_DURATION, false);
		} else {
			setInactive();
		}
	}
	
	@Override
	public String toString() {
		return "MapObstacle[" + mapObstacleType.name() + "]";
	}
	
	private Sprite prepareSprite(String texture, VertexBufferObjectManager vertexBufferObjectManager) {
		Sprite sprite;
		if (mapObstacleType.isAnimated()) {
			sprite = new AnimatedSprite(0, 0, PotmeActivity.TILE_SIZE / mapObstacleType.getObstacleSizeRatio(), PotmeActivity.TILE_SIZE / mapObstacleType.getObstacleSizeRatio(), ResourceManager.getInstance().getTiledTexture(texture), vertexBufferObjectManager);
		} else {
			sprite = new Sprite(0, 0, PotmeActivity.TILE_SIZE / mapObstacleType.getObstacleSizeRatio(), PotmeActivity.TILE_SIZE / mapObstacleType.getObstacleSizeRatio(), ResourceManager.getInstance().getTexture(texture), vertexBufferObjectManager);
		}
		
		Sprite tileSprite = tile.getTileSprite(vertexBufferObjectManager);
		float tileSpriteWidth = tileSprite.getWidth();
		float tileSPriteHeight = tileSprite.getHeight();
		
		sprite.setPosition(offsetX + (tile.getMapPositionX() * tileSpriteWidth) + tileSpriteWidth / 2, - offsetY - ((tile.getMapPositionY() * tileSPriteHeight) + tileSPriteHeight / 2));
		sprite.setRotation(mapObstacleType.getRotation() * QUARTER_ROTATION);
		return sprite;
	}
	
	private void prepareObstacleActiveSprite(VertexBufferObjectManager vertexBufferObjectManager) {
		if (mapObstacleActiveSprite == null && mapObstacleType.getActiveTexture() != null) {
			mapObstacleActiveSprite = prepareSprite(mapObstacleType.getActiveTexture(), vertexBufferObjectManager);
		}
	}
	
	private void prepareObstacleInactiveSprite(VertexBufferObjectManager vertexBufferObjectManager) {
		if (mapObstacleInactiveSprite == null && mapObstacleType.getInactiveTexture() != null) {
			mapObstacleInactiveSprite = prepareSprite(mapObstacleType.getInactiveTexture(), vertexBufferObjectManager);
		}
	}
	
	public Entity getMapObstacleContainer(VertexBufferObjectManager vertexBufferObjectManager) {
		prepareObstacleActiveSprite(vertexBufferObjectManager);
		prepareObstacleInactiveSprite(vertexBufferObjectManager);
		return mapObstacleContainer;
	}

}
