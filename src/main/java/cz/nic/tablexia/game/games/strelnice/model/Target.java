/**
 *
 */

package cz.nic.tablexia.game.games.strelnice.model;

import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import cz.nic.tablexia.game.games.strelnice.media.TextureType;

/**
 * @author lhoracek
 */
public class Target extends Sprite {
    private final float startTime;
    private TextureType textureType;
    private final Wave  wave;
    private boolean     shot = false;
    private Sprite      effect;
    private Sprite      overlay;

    public Target(float pX, float pY, float pWidth, float pHeight, ITextureRegion pTextureRegion, Float startTime, Wave wave, TextureType textureType, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pWidth, pHeight, pTextureRegion, pVertexBufferObjectManager);
        this.startTime = startTime;
        this.textureType = textureType;
        this.wave = wave;
        setZIndex(wave.getzIndex());

        overlay = new Sprite(pWidth / 2, pHeight / 2, pWidth, pHeight, pTextureRegion, pVertexBufferObjectManager);
        overlay.setColor(1, 1, 1, 1);
        overlay.setColor(1, 0, 0, 1);
        overlay.setAlpha(0);
        attachChild(overlay);
    }

    public Target(float pX, float pY, ITextureRegion pTextureRegion, Float startTime, Wave wave, TextureType textureType, VertexBufferObjectManager pVertexBufferObjectManager) {
        this(pX, pY, pTextureRegion.getWidth(), pTextureRegion.getHeight(), pTextureRegion, startTime, wave, textureType, pVertexBufferObjectManager);
    }

    public void blink() {
        overlay.registerEntityModifier(new LoopEntityModifier(new SequenceEntityModifier(new AlphaModifier(0.1f, 0f, 1f), new DelayModifier(0.1f), new AlphaModifier(0.1f, 1f, 0f)), 3));
    }

    public float getStartTime() {
        return startTime;
    }

    public void setTextureType(TextureType textureType) {
        this.textureType = textureType;
    }

    public TextureType getTextureType() {
        return textureType;
    }

    public Wave getWave() {
        return wave;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + Float.floatToIntBits(startTime);
        result = (prime * result) + ((textureType == null) ? 0 : textureType.hashCode());
        result = (prime * result) + ((wave == null) ? 0 : wave.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Target other = (Target) obj;
        if (Float.floatToIntBits(startTime) != Float.floatToIntBits(other.startTime)) {
            return false;
        }
        if (textureType != other.textureType) {
            return false;
        }
        if (wave != other.wave) {
            return false;
        }
        return true;
    }

    private OnTargetAreaTouchedListener onAreaTouchedListener;

    public void setOnAreaTouchedListener(OnTargetAreaTouchedListener onAreaTouchedListener) {
        this.onAreaTouchedListener = onAreaTouchedListener;
    }

    public void clearOnAreaTouchedListener() {
        onAreaTouchedListener = null;
    }

    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        if ((onAreaTouchedListener != null) && pSceneTouchEvent.isActionDown()) {
            onAreaTouchedListener.touched(this);
            return true;
        }
        return false;
    }

    public interface OnTargetAreaTouchedListener {
        public void touched(Target flower);
    }

    public boolean isShot() {
        return shot;
    }

    public void setShot(boolean shot) {
        this.shot = shot;
    }

    public Sprite getEffect() {
        return effect;
    }

    public void setEffect(Sprite effect) {
        this.effect = effect;
    }

}
