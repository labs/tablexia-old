/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.potme;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.andengine.engine.Engine;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.FadeInModifier;
import org.andengine.entity.modifier.FadeOutModifier;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;
import org.andengine.util.modifier.IModifier;
import org.andengine.util.modifier.IModifier.IModifierListener;
import org.andengine.util.modifier.ease.EaseBackOut;
import org.andengine.util.modifier.ease.EaseLinear;
import org.andengine.util.modifier.ease.EaseStrongOut;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import cz.nic.tablexia.R;
import cz.nic.tablexia.audio.resources.SpeechSounds;
import cz.nic.tablexia.game.GameActivity;
import cz.nic.tablexia.game.common.EntityModifierListenerAdapter;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.GamesDefinition;
import cz.nic.tablexia.game.games.potme.UtilityAccess.IOnDetachListener;
import cz.nic.tablexia.game.games.potme.action.ActionContainer;
import cz.nic.tablexia.game.games.potme.action.ActionType;
import cz.nic.tablexia.game.games.potme.action.ActionType.IActionFinishedListener;
import cz.nic.tablexia.game.games.potme.action.widget.ActionsStripWidget;
import cz.nic.tablexia.game.games.potme.action.widget.ActionsWidget;
import cz.nic.tablexia.game.games.potme.creature.Player;
import cz.nic.tablexia.game.games.potme.map.TileMap;
import cz.nic.tablexia.game.games.potme.map.TileMap.TileMapPosition;
import cz.nic.tablexia.game.games.potme.map.mapobject.MapObject;
import cz.nic.tablexia.game.games.potme.map.mapobject.MapObjectType;
import cz.nic.tablexia.game.games.potme.map.tile.Tile;
import cz.nic.tablexia.game.games.potme.map.widget.MapWidget;
import cz.nic.tablexia.game.games.potme.map.widget.MapWidget.TileMapClickListener;
import cz.nic.tablexia.menu.usermenu.User;

/**
 * Activity for "Potmě" game
 *
 * @author Matyáš Latner
 */
public class PotmeActivity extends GameActivity implements TileMapClickListener {

	public static enum GameLayer {
		BACKGROUND_TEXTURE_LAYER	(0),
    	BACKGROUND_DRAWING_LAYER	(1),
        BUTTON_LAYER				(2),
        ACTIONS_LAYER				(3),
        FADE_LAYER					(4),
        BONUS_LAYER					(5),
        MAP_LAYER					(6),
        PLAYER_LAYER				(7),
        INFO_LAYER					(8),
        ACTION_STRIP_LAYER			(9);

        private static final int TOP_Z_INDEX = GameLayer.values().length + 1;

        private Entity layerEntity;
        private int layerZIndex;

        private GameLayer(int layerZIndex) {
            this.layerZIndex = layerZIndex;
        }

        private void setLayerEntity(Entity layerEntity) {
            this.layerEntity = layerEntity;
        }

        public Entity getLayerEntity() {
            return layerEntity;
        }

        public int getLayerZIndex() {
            return layerZIndex;
        }

        public static void attachLayersToScene(Scene scene) {
            for (GameLayer gameLayer : GameLayer.values()) {
                Entity entity = new Entity();
                gameLayer.setLayerEntity(entity);
                scene.attachChild(entity);
                entity.setZIndex(gameLayer.getLayerZIndex());
            }
            scene.sortChildren();
        }

        public static void resetLayersZIndexes(Scene scene) {
            for (GameLayer gameLayer : GameLayer.values()) {
                gameLayer.getLayerEntity().setZIndex(gameLayer.getLayerZIndex());
            }
            scene.sortChildren();
        }

        public void sendToFront(Scene scene) {
            getLayerEntity().setZIndex(TOP_Z_INDEX);
            scene.sortChildren();
        }
    }
	
	public static final List<ActionType> TUTORIAL_STEPS = Arrays.asList(new ActionType[]{ActionType.GO,
																						 ActionType.RIGHT,
																						 ActionType.DOOR,
																						 ActionType.GO});

    private static final boolean	SHOW_DEBUGINFO_RULE          = false;
    
    private static final int 	INITIAL_MAP_WIDGET 			   	= 0;
    private static final float 	FADE_DURATION 					= 0.4f;
    private static final float 	FADE_ALPHA 					   	= 0.5f;
    private static final Color 	FADE_COLOR 					   	= Color.BLACK;
    public  static final float  ANIMATION_MOVE_PERIOD      		= 0.4f;
    private static final Color 	BACKGROUND_COLOR			   	= new Color(0.9412f, 0.8235f, 0.6745f);
    
    private static final float 	KEYICON_MOVE_DURATION 			= 0.5f;
	private static final double KEY_ICON_TILE_SIZE_RATION_X 	= 0.7;
	private static final double KEY_ICON_TILE_SIZE_RATION_Y 	= 0.4;
    
    public static final int 	ERROR_ACTION_DELAY 				= 1;
    public static final int 	FINISH_ACTION_DELAY 			= 1;

    private static final int 				MAP_X_SIZE					   = 7;
    private static final int 				MAP_Y_SIZE					   = 6;
    public  static final TileMapPosition 	DEFAULT_MAP_START_POSITION	   = new TileMapPosition(MAP_X_SIZE - 1, MAP_Y_SIZE - 2);
    
    private static final double 			TILE_SIZE_RATIO 			= 0.85;
    private static final double 			MAP_START_X_TILE_SIZE_RATIO = 0.8;
	private static final double 			MAP_START_Y_TILE_SIZE_RATIO = 0.68;
	
	private static final int 				GAME_INFO_TILE_SIZE_MULTIPLIER 	= 5;
	private static final int 				GAME_INFO_FINISH_ALPHA 			= 1;
	private static final int 				GAME_INFO_START_ALPHA 			= 0;
	private static final float 				GAME_INFO_APPEAR_DURATION 		= 0.3f;
	
    public static int TILE_SIZE;
    public static int MAP_START_POSITION_X;
    public static int MAP_START_POSITION_Y;
    public static int ACTION_SIZE_BIGGER;
    public static int ACTION_SIZE_SMALLER;
    
    private int planningStartTime	= 0;
    
    private PotmeDifficulty 	potmeDifficulty;
    
    private boolean				currentFirstPlay = true;
    private RandomAccess 		randomAccess;
    private ActionsStripWidget 	actionsStripWidget;
    private ActionsWidget 		actionsWidget;
    private StartButton			startButton;
    private Player 				player;

    private List<MapWidget> 	mapWidgets;

    private int 				actualMapWidgetNumber;
    private int 				errorCount;
    
	private Sprite 				keyIcon;
	private float 				keyIconPositionX;
	private float 				keyIconPositionY;

	private View 				debugInfoLayout;

    /**
     * Start button
     *
     * @author Matyáš Latner
     *
     */
    public class StartButton extends Entity {

        private static final float BUTTON_BLINK_DELAY = 0.3f;

		private boolean 	enabled = true;
        
        private Entity 		backgroundLayer;
        private Entity 		blinkerLayer;
        private Entity 		textLayer;
        
        private Rectangle 	touchArea;
        
        private Sprite 		actualButtonSprite;
        
        private Sprite 		unpressedButtonSprite;
        private Sprite 		disabledButtonSprite;
        
        private Text  		buttonText;
        private float 		textUnpressedPositionX;
        private float 		textUnpressedPositionY;
        private float 		textPressedPositionX;
        private float 		textPressedPositionY;

		private LoopEntityModifier blinkerModifier;


        public StartButton(VertexBufferObjectManager vertexBufferObject) {
        	// layers
        	backgroundLayer = new Entity();
        	blinkerLayer = new Entity();
        	textLayer = new Entity();
        	attachChild(backgroundLayer);
        	attachChild(blinkerLayer);
        	attachChild(textLayer);
            
        	// backgrounds
            unpressedButtonSprite = new Sprite(0, 0, ResourceManager.getInstance().getTexture(ResourceManager.CONTROL_START_UNPRESSED), vertexBufferObject);
            disabledButtonSprite = new Sprite(0, 0, ResourceManager.getInstance().getTexture(ResourceManager.CONTROL_START_DISABLED), vertexBufferObject);
            
            // touch area
            touchArea = new Rectangle(0, 0, unpressedButtonSprite.getWidth(), unpressedButtonSprite.getHeight(), vertexBufferObject) {
            	
            	@Override
            	public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
            		if (enabled && (pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP)) {
    					startGameSequence();
    				}
            		return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
            	}
            	
            };
            touchArea.setAlpha(0);
            attachChild(touchArea);
            scene.registerTouchArea(touchArea);
            
            // button text
            textUnpressedPositionX = 0;
            textUnpressedPositionY = unpressedButtonSprite.getHeight() / 6;
            textPressedPositionX = -unpressedButtonSprite.getHeight() / 20;
            textPressedPositionY = unpressedButtonSprite.getHeight() / 8;
            buttonText = new Text(textUnpressedPositionX, textUnpressedPositionY, ResourceManager.getInstance().getFont(), getString(R.string.game_potme_start), getVertexBufferObjectManager());
			textLayer.attachChild(buttonText);
			disable();
        }
        
        public void enable() {
            enabled = true;
            changeActualButtonSprite(unpressedButtonSprite);
            buttonText.setPosition(textUnpressedPositionX, textUnpressedPositionY);
        }

        public void disable() {
        	disablePulsing();
            enabled = false;
            changeActualButtonSprite(disabledButtonSprite);
            buttonText.setPosition(textPressedPositionX, textPressedPositionY);
        }
        
        public void enablePulsing() {
        	FadeInModifier fadeIn = new FadeInModifier(BUTTON_BLINK_DELAY);
        	fadeIn.setAutoUnregisterWhenFinished(true);
        	FadeOutModifier fadeOut = new FadeOutModifier(BUTTON_BLINK_DELAY);
        	fadeOut.setAutoUnregisterWhenFinished(true);
        	SequenceEntityModifier sequenceModifier = new SequenceEntityModifier(fadeOut, fadeIn);
        	sequenceModifier.setAutoUnregisterWhenFinished(true);
        	blinkerModifier = new LoopEntityModifier(sequenceModifier);
			unpressedButtonSprite.registerEntityModifier(blinkerModifier);
		}
        
        public void disablePulsing() {
        	if (blinkerModifier != null) {        		
        		unpressedButtonSprite.unregisterEntityModifier(blinkerModifier);
        		blinkerModifier = null;
        	}
		}
        
        public boolean isEnbleda() {
        	return enabled;
		}
        
        private float getBackgroundWidth() {
        	return unpressedButtonSprite.getWidth();
		}
        
        private float getBackgroundHeight() {
        	return unpressedButtonSprite.getHeight();
		}
        
        private void changeActualButtonSprite(final Sprite buttonSprite) {
        	if (actualButtonSprite != null) {
        		UtilityAccess.getInstance().detachAndAttachEntity(actualButtonSprite, null, new IOnDetachListener() {
					
					@Override
					public void onDetach() {
						attachActualButtonSprite(buttonSprite);
					}
				}, true);
        		
        	} else {
        		attachActualButtonSprite(buttonSprite);
        	}
		}
        
        private void attachActualButtonSprite(Sprite buttonSprite) {
        	actualButtonSprite = buttonSprite;
        	backgroundLayer.attachChild(actualButtonSprite);
        	buttonSprite.setVisible(true);
		}
	}

    public PotmeActivity() {
        super(GamesDefinition.POTME);
    }


    /* //////////////////////////////////////////// ANDENGINE LIFECYCLE */
    
    @Override
    public Engine onCreateEngine(EngineOptions pEngineOptions) {
    	pEngineOptions.getAudioOptions().setNeedsSound(true);
    	return super.onCreateEngine(pEngineOptions);
    }

    @Override
    public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws IOException {
        ResourceManager.getInstance().loadTexturesAndSounds(mEngine, getContext(), getDifficulty());
        pOnCreateResourcesCallback.onCreateResourcesFinished();
    }


    /* //////////////////////////////////////////// TABLEXIA LIFECYCLE */

    @Override
    protected void onCreateTablexiaScene() {
        scene.setBackground(new Background(BACKGROUND_COLOR));
    }
    
    @Override
    protected void gameComplete() {
    	if (getPotmeDifficulty() == PotmeDifficulty.TUTORIAL) {
        	User selectedUser = getTablexiaContext().getSelectedUser();
        	if (selectedUser != null) {
        		selectedUser.markTutorialAsPlayed(getGameDefinition());
        	}
        }
    	super.gameComplete();
    }

    @Override
    protected void resetGame() {
        super.resetGame();

        potmeDifficulty = null;
        
        randomAccess = null;
        mapWidgets = null;

        actionsStripWidget = null;
        actionsWidget = null;
        startButton = null;
        player = null;

        hideKeyIcon();
    }

    @Override
    protected void initGame() {
    	if (!getPotmeDifficulty().hasResults()) {
    		disableGameManagerForCurrentGame();
    		currentFirstPlay = false;
    	}
        super.initGame();
        
        // calculate widgets sizes
        ACTION_SIZE_BIGGER 	= displaySize.x / 9;
        ACTION_SIZE_SMALLER = (int)(ACTION_SIZE_BIGGER * 0.7);
        int actionStripWidgetWidth = (int)(ACTION_SIZE_BIGGER * 2);
        int actionPanelWidth = (int)((2 * ACTION_SIZE_SMALLER));
        
        TILE_SIZE = (int)(((displaySize.x - actionStripWidgetWidth - actionPanelWidth) / MAP_X_SIZE) * TILE_SIZE_RATIO);
        MAP_START_POSITION_X = (int)(TILE_SIZE * MAP_START_X_TILE_SIZE_RATIO);
        MAP_START_POSITION_Y = (int)(displaySize.y - (TILE_SIZE * MAP_START_Y_TILE_SIZE_RATIO));
        
        mapWidgets = new ArrayList<MapWidget>();

        errorCount = 0;
        actualMapWidgetNumber = 0;
        randomAccess = new RandomAccess();
        
        UtilityAccess.init(getEngine(), randomAccess);

        GameLayer.attachLayersToScene(scene);

		showMapWidgets(getPotmeDifficulty());
		prepareBackground();

        actionsStripWidget = showActionsStripWidget(actionStripWidgetWidth);
        actionsWidget = showActionsWidget(getPotmeDifficulty(), actionStripWidgetWidth);
        startButton = showStartButton((int)actionsWidget.getX());
        
        //TODO remove and use event bus
        actionsStripWidget.setStartButton(startButton);
        actionsWidget.setStartButton(startButton);
        
        player = showPlayer();

        prepareGameInfos(actionStripWidgetWidth);
        prepareFadeLayer();
        prepareKeyIcon();
        
        resetGameState();

        mapWidgetToFront(INITIAL_MAP_WIDGET);
        showDebugInfo();
    }

    @Override
    protected void showGame() {
    	goToPlanningMode(null);
        super.showGame();
        actionsWidget.showActions();
    }
    
    /* //////////////////////////////////////////// BACKGROUND */
    
    private void prepareBackground() {
    	ITextureRegion backgroundTextureRegion = ResourceManager.getInstance().getTexture(ResourceManager.BACKGROUND_TEXTURE);
    	Sprite backgroundTexture = new Sprite(displaySize.x / 2, displaySize.y / 2, displaySize.x, displaySize.y, backgroundTextureRegion, getVertexBufferObjectManager());
    	GameLayer.BACKGROUND_TEXTURE_LAYER.getLayerEntity().attachChild(backgroundTexture);
    	
    	
    	ITextureRegion backgroundMapDrawingTextureRegion = ResourceManager.getInstance().getTexture(ResourceManager.BACKGROUND_MAP_DRAWING);
    	float backgroundMapDrawingSizeX = (int)(TILE_SIZE * 10.35);
    	float backgroundMapDrawingSizeY= backgroundMapDrawingSizeX * (backgroundMapDrawingTextureRegion.getHeight() / backgroundMapDrawingTextureRegion.getWidth());
    	
		Sprite backgroundMapDrawing = new Sprite(backgroundMapDrawingSizeX / 2, displaySize.y - (backgroundMapDrawingSizeY / 2), backgroundMapDrawingSizeX, backgroundMapDrawingSizeY, backgroundMapDrawingTextureRegion, getVertexBufferObjectManager());
    	GameLayer.BACKGROUND_DRAWING_LAYER.getLayerEntity().attachChild(backgroundMapDrawing);
	}

    /* //////////////////////////////////////////// TILE MAP BOOKMARK CLICK */

    @Override
    public void onBookmarkClick(int mapWidgetNumber) {
        mapWidgetToFront(mapWidgetNumber);
    }



    /* //////////////////////////////////////////// KEY ICON */

    private void prepareKeyIcon() {
    	int keyIconSizeX = (int)(TILE_SIZE * KEY_ICON_TILE_SIZE_RATION_X);
    	int keyIconSizeY = (int)(TILE_SIZE * KEY_ICON_TILE_SIZE_RATION_Y);
    	
        keyIconPositionX = (MAP_START_POSITION_X + mapWidgets.get(actualMapWidgetNumber).getWidth()) - (keyIconSizeX / 2);
		keyIconPositionY = MAP_START_POSITION_Y + (keyIconSizeY / 2) + (keyIconSizeY / 5);
        
		keyIcon = new Sprite(keyIconPositionX, keyIconPositionY, keyIconSizeX, keyIconSizeY, ResourceManager.getInstance().getTexture(ResourceManager.CONTROL_KEY), getVertexBufferObjectManager());
        keyIcon.setVisible(false);
        
        GameLayer.BONUS_LAYER.getLayerEntity().attachChild(keyIcon);
    }

    private void showKeyIcon() {
        if (!keyIcon.isVisible()) {
        	keyIcon.setVisible(true);
        	keyIcon.registerEntityModifier(new MoveModifier(KEYICON_MOVE_DURATION, 
        												   keyIconPositionX,
        												   keyIconPositionY - keyIcon.getHeight(),
        												   keyIconPositionX,
        												   keyIconPositionY,
        												   EaseBackOut.getInstance()));
        }
    }
    
    private void hideKeyIcon() {
        if (keyIcon.isVisible()) {
        	keyIcon.setVisible(false);
        	keyIcon.setPosition(keyIconPositionX, keyIconPositionY - keyIcon.getHeight());
        }
    }
    
    private void resetKey() {
    	player.setKey(false);
    	hideKeyIcon();
	}



    /* //////////////////////////////////////////// WIDGETS */

    private void setMapWidgetsClickable(boolean clickable) {
        for (MapWidget mapWidget : mapWidgets) {
            mapWidget.setClickable(clickable);
        }
    }

    public void mapWidgetToFront(int mapWidgetNumber) {
        for (int i = 0; i < mapWidgets.size(); i++) {
            MapWidget mapWidget = mapWidgets.get(i);

            if (i == mapWidgetNumber) {
                mapWidget.setInFront();
                mapWidget.setZIndex(mapWidgets.size());
            } else {
                mapWidget.setInBottom();
                mapWidget.setZIndex(i);
            }
        }
        GameLayer.PLAYER_LAYER.getLayerEntity().setVisible(player.getActualFloor() == mapWidgetNumber);
        GameLayer.MAP_LAYER.getLayerEntity().sortChildren(true);
    }

    private void showMapWidgets(PotmeDifficulty potmeDifficulty) {
    	try {
			Tile lastFinishPosition = null;
			
			int floorCount = potmeDifficulty.getFloorCount();
			boolean hasKey = potmeDifficulty.hasDifficultyActionType(ActionType.KEY);
			
			for (int i = 0; i < floorCount; i++) {
				
				MapObjectType finishMapObjetcMapObjectType;
				if (i == (floorCount - 1)) {
					finishMapObjetcMapObjectType = MapObjectType.SAFE;
				} else {
					finishMapObjetcMapObjectType = MapObjectType.STAIRS;
				}
				boolean floorHasKey = hasKey && ((floorCount - 1) / 2) == i;
				MapWidget mapWidget = new MapWidget(potmeDifficulty.getMapProviderNewInstance(randomAccess), lastFinishPosition, finishMapObjetcMapObjectType, i, floorCount > 1, MAP_START_POSITION_X, MAP_START_POSITION_Y, MAP_X_SIZE, MAP_Y_SIZE, floorHasKey, getResources().getText(R.string.game_potme_floor).toString(), getVertexBufferObjectManager());
				mapWidgets.add(mapWidget);
				
				if (i == 0) {
					mapWidget.setInFront();
				} else {
					mapWidget.setInBottom();
				}
				
				mapWidget.setClickable(scene, this);
				GameLayer.MAP_LAYER.getLayerEntity().attachChild(mapWidget);
				
				lastFinishPosition = mapWidget.getTileMap().getTileAtPosition(mapWidget.getTileMap().getMapFinishPosition());
			}
    	} catch (Throwable t) {
    		throw new RuntimeException("Error during generating map with random seed: " + randomAccess.getRandomSeed(), t);
    	}
    }

    private ActionsStripWidget showActionsStripWidget(int actionStripWidgetWidth) {
    	Sprite actionStripBackground = new Sprite(displaySize.x - (actionStripWidgetWidth / 2), displaySize.y - (displaySize.y / 2), actionStripWidgetWidth, displaySize.y, ResourceManager.getInstance().getTexture(ResourceManager.BACKGROUND_ACTIONSTRIP_TEXTURE), getVertexBufferObjectManager());
    	GameLayer.BACKGROUND_TEXTURE_LAYER.getLayerEntity().attachChild(actionStripBackground);
        ActionsStripWidget actionStripWidget = new ActionsStripWidget(displaySize.x, displaySize.y, actionStripWidgetWidth, displaySize.y, scene, getVertexBufferObjectManager());
        actionStripWidget.enableActionSorting(getPotmeDifficulty() != PotmeDifficulty.TUTORIAL);
        actionStripWidget.enableStartButtonControl(getPotmeDifficulty() != PotmeDifficulty.TUTORIAL);
        GameLayer.ACTION_STRIP_LAYER.getLayerEntity().attachChild(actionStripWidget);
        return actionStripWidget;
    }

    private ActionsWidget showActionsWidget(PotmeDifficulty potmeDifficulty, int actionStripWidgetWidth) {
        ActionsWidget widget = new ActionsWidget(displaySize.x - actionStripWidgetWidth - (2 * ACTION_SIZE_SMALLER), displaySize.y - ACTION_SIZE_SMALLER, displaySize.x, displaySize.y, potmeDifficulty, scene, actionsStripWidget, actionStripWidgetWidth, getVertexBufferObjectManager());
        GameLayer.ACTIONS_LAYER.getLayerEntity().attachChild(widget);
        
        return widget;
    }

    private StartButton showStartButton(int actionWidgetPositionX) {
        StartButton button = new StartButton(getVertexBufferObjectManager());
        button.setPosition(actionWidgetPositionX + (button.getBackgroundWidth() / 2), MAP_START_POSITION_Y - mapWidgets.get(0).getHeight() + (button.getBackgroundHeight() / 2));
        GameLayer.BUTTON_LAYER.getLayerEntity().attachChild(button);
        return button;
    }
    
    /* //////////////////////////////////////////// FADE LAYER */

    private void prepareFadeLayer() {
        Rectangle fadeRectangle = new Rectangle(displaySize.x / 2, displaySize.y / 2, displaySize.x, displaySize.y, getVertexBufferObjectManager());
        fadeRectangle.setColor(FADE_COLOR);
        fadeRectangle.setAlpha(FADE_ALPHA);
        GameLayer.FADE_LAYER.getLayerEntity().attachChild(fadeRectangle);
    }
    
    private void showFadeLayer() {
    	IEntity fadeLayer = GameLayer.FADE_LAYER.getLayerEntity().getChildByIndex(0);
		fadeLayer.setAlpha(0);
		fadeLayer.setVisible(true);
		AlphaModifier alphaModifier = new AlphaModifier(FADE_DURATION, 0, FADE_ALPHA, EaseLinear.getInstance());
		fadeLayer.registerEntityModifier(alphaModifier);
	}
    
    private void hideFadeLayer() {
    	final IEntity fadeLayer = GameLayer.FADE_LAYER.getLayerEntity().getChildByIndex(0);
		AlphaModifier alphaModifier = new AlphaModifier(FADE_DURATION, FADE_ALPHA, 0, EaseLinear.getInstance());
		alphaModifier.addModifierListener(new EntityModifierListenerAdapter() {
			
			@Override
			public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {				
				fadeLayer.setVisible(false);
			}
			
		});
		fadeLayer.registerEntityModifier(alphaModifier);
	}

    /* //////////////////////////////////////////// PLAYER */

    private Player showPlayer() {
        Player pl = new Player(INITIAL_MAP_WIDGET, getVertexBufferObjectManager());
        GameLayer.PLAYER_LAYER.getLayerEntity().attachChild(pl);
        return pl;
    }

    private void resetPlayer() {
        TileMap tileMap = mapWidgets.get(actualMapWidgetNumber).getTileMap();
        player.resetPlayerMapPosition(tileMap, MAP_START_POSITION_X, MAP_START_POSITION_Y);
        player.setPossibleDirection(tileMap);
    }


    /* //////////////////////////////////////////// GAME CONTROL */
    
    private void resetGameState() {
    	goToFloorNumber(0);
    	resetKey();
    	resetPlayer();
    	hideGameInfos();
    	for (MapWidget mapWidget : mapWidgets) {
    		mapWidget.getTileMap().reset();
		}
	}

    private void goToPlanningMode(Integer actionNumber) {
        hideFadeLayer();
        actionsStripWidget.setStartButtonState();
        actionsWidget.enableActions();
        actionsStripWidget.enableControl();
        actionsStripWidget.displaySelectedActions(actionNumber);
        setMapWidgetsClickable(true);
        planningStartTime = (int)scene.getSecondsElapsedTotal();
    }

    private void goToPlayMode() {
    	if (getGameManager() != null) {    		
    		getGameManager().setExtraInt1(getGameManager().getExtraInt1() + ((int)scene.getSecondsElapsedTotal() - planningStartTime));
    	}
        startButton.disable();
        actionsWidget.disableActions();
        actionsStripWidget.disableControl();
        showFadeLayer();
        setMapWidgetsClickable(false);
    }

    public boolean goToNextFloor(TileMapPosition mapPosition) {
        if (tryGoToFloor(actualMapWidgetNumber + 1, mapPosition)) {
            return true;
        }
        if (tryGoToFloor(actualMapWidgetNumber - 1, mapPosition)) {
            return true;
        }

        return false;
    }

    private boolean tryGoToFloor(int floorNumber, TileMapPosition mapPosition) {
        if ((floorNumber >= 0) && (floorNumber < mapWidgets.size())) {
            MapObject mapObject = mapWidgets.get(floorNumber).getTileMap().getTileAtPosition(mapPosition).getMapObject();
            if ((mapObject != null) && (mapObject.getMapObjectType() == MapObjectType.STAIRS)) {
                goToFloorNumber(floorNumber);
                return true;
            }
        }
        return false;
    }
    
    private void goToFloorNumber(int floorNumber) {
    	actualMapWidgetNumber = floorNumber;
        player.setActualFloor(floorNumber);
        player.setPossibleDirection(mapWidgets.get(floorNumber).getTileMap());
        mapWidgetToFront(actualMapWidgetNumber);
    }

    /**
     * Start the game sequence and periodically do players actions.
     */
    private void startGameSequence() {
        final List<ActionContainer> selectedActions = actionsStripWidget.getSelectedActions();
        mapWidgetToFront(player.getActualFloor());
        if (selectedActions.size() > 0) {
            goToPlayMode();
            
            scene.registerUpdateHandler(new IUpdateHandler() {
            	
                private float	secoundElapsed 			= 0;
                private int 	selectedActionPosition 	= 0;
                private boolean actionFinished 			= true;

                @Override
                public void reset() {
                    // nothing needed
                }

                @Override
                public void onUpdate(float pSecondsElapsed) {
                    if (actionFinished && (secoundElapsed > ANIMATION_MOVE_PERIOD)) {
                        secoundElapsed = 0;
                        if (selectedActionPosition < selectedActions.size()) {
                            final TileMap tileMap = mapWidgets.get(actualMapWidgetNumber).getTileMap();
                            
                            actionFinished = false;
                            actionsStripWidget.displayProcessedActions(selectedActionPosition,
                            										   selectedActionPosition != 0,
                            										   new EntityModifierListenerAdapter() {
                            	
								@Override
								public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
									selectedActions.get(selectedActionPosition).getAction().getActionType().performActionTypeRule(PotmeActivity.this, tileMap, player, MAP_START_POSITION_X, MAP_START_POSITION_Y, new IActionFinishedListener() {
										
										@Override
										public void onActionFinished(boolean result) {
											try {
												if (result) {
													performaNextAction(tileMap);
												} else {
													unregisterUpdateHandler();
													performaErrorAction();
												}
												actionFinished = true;
											} catch (Throwable t) {
						                		throw new RuntimeException("Error during game cycle with random seed: " + randomAccess.getRandomSeed(), t);
						                	}
										}
									});
								}
							});
                        } else {
                        	unregisterUpdateHandler();
                        	ResourceManager.getInstance().getSound(ResourceManager.ERROR).play();
                        	player.registerEntityModifier(new DelayModifier(FINISH_ACTION_DELAY, new EntityModifierListenerAdapter() {
                        		
	                            @Override
	                            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
	                            	performaErrorAction();
	                            }
	                            
	                        }));
                        }
                    } else {
                        secoundElapsed = secoundElapsed + pSecondsElapsed;
                    }
                }
                
                private void performaNextAction(TileMap tileMap) {
                	if (player.hasKey()) {
                        showKeyIcon();
                    }
                    MapObject mapObject = tileMap.getTileAtPosition(player.getActualTileMapPosition()).getMapObject();
                    if ((mapObject != null) &&
                    	(!getPotmeDifficulty().hasDifficultyActionType(ActionType.KEY) || player.hasKey()) &&
                    	(mapObject.getMapObjectType() == MapObjectType.SAFE) &&
                    	(selectedActionPosition == selectedActions.size() - 1)) {
                    	
	                    	ResourceManager.getInstance().getSound(ResourceManager.KEY).play();
	                        scene.unregisterUpdateHandler(this);
	                        showSafeInfo();
	                        
	                        player.registerEntityModifier(new DelayModifier(FINISH_ACTION_DELAY, new EntityModifierListenerAdapter() {
	                            @Override
	                            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
	                            	if (getGameManager() != null) {	                            		
	                            		getGameManager().setCounterAndSave(errorCount);
	                            	}
	                            	gameComplete();
	                            }
	                        }));
                    }
                    selectedActionPosition++;
				}
                
                private void performaErrorAction() {
                	errorCount++;
                	resetGameState();
                    goToPlanningMode(Integer.valueOf(selectedActionPosition));
				}
                
                private void unregisterUpdateHandler() {
                	scene.unregisterUpdateHandler(this);
				}
                
            });
        }
    }
    
    
    /* //////////////////////////////////////////// GAME INFO */
    
    private Map<String, Sprite> gameInfos;
    
    private void prepareGameInfos(int actionStripWidgetWidth) {
    	gameInfos = new HashMap<String, Sprite>();
    	
    	float gameInfoPositionX = MAP_START_POSITION_X + (mapWidgets.get(0).getWidth() / 2);
    	float gameInfoPositionY = MAP_START_POSITION_Y - (mapWidgets.get(0).getHeight() / 2);
    	float gameInfoWidth 	= TILE_SIZE * GAME_INFO_TILE_SIZE_MULTIPLIER;
    	float gameInfoHeight 	= TILE_SIZE * GAME_INFO_TILE_SIZE_MULTIPLIER;
    	createInfoSprite(GameLayer.INFO_LAYER, ResourceManager.INFO_CRASH, gameInfoPositionX, gameInfoPositionY, gameInfoWidth, gameInfoHeight);
    	createInfoSprite(GameLayer.INFO_LAYER, ResourceManager.INFO_SAFE1, gameInfoPositionX, gameInfoPositionY, gameInfoWidth, gameInfoHeight);
    	createInfoSprite(GameLayer.INFO_LAYER, ResourceManager.INFO_SAFE2, gameInfoPositionX, gameInfoPositionY, gameInfoWidth, gameInfoHeight);
	}
    
    public void showCrashInfo() {
    	showGameInfo(ResourceManager.INFO_CRASH, true, GAME_INFO_APPEAR_DURATION, null);
	}
    
    public void showSafeInfo() {
    	showGameInfo(ResourceManager.INFO_SAFE1, true, GAME_INFO_APPEAR_DURATION, new EntityModifierListenerAdapter(){
    		
    		@Override
    		public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
    			showGameInfo(ResourceManager.INFO_SAFE2, false, GAME_INFO_APPEAR_DURATION, null);
    		}
    		
    	});
	}
    
    public void hideGameInfo(String gameInfoTextureName, boolean animated, final IModifierListener<IEntity> entityModifier) {
    	final Sprite gameInfoSprite = gameInfos.get(gameInfoTextureName);
    	if (gameInfoSprite.isVisible()) {    		
    		if (animated) {
    			gameInfoSprite.setAlpha(1.0f);
    			AlphaModifier alphaModifier = new AlphaModifier(GAME_INFO_APPEAR_DURATION, GAME_INFO_FINISH_ALPHA, GAME_INFO_START_ALPHA, EaseStrongOut.getInstance());
    			alphaModifier.addModifierListener(new IModifierListener<IEntity>() {
    				
    				@Override
    				public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
    					if (entityModifier != null) {
    						entityModifier.onModifierStarted(pModifier, pItem);
    					}
    				}
    				
    				@Override
    				public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
    					gameInfoSprite.setVisible(false);
    					if (entityModifier != null) {
    						entityModifier.onModifierFinished(pModifier, pItem);
    					}
    				}
    			});
    			gameInfoSprite.registerEntityModifier(alphaModifier);
    		} else {
    			gameInfoSprite.setVisible(false);
    		}
    	}
    }
    
    public void showGameInfo(String gameInfoTextureName, boolean animated, float animationDuration, IModifierListener<IEntity> entityModifier) {
    	hideGameInfos();
    	
    	Sprite gameInfoSprite = gameInfos.get(gameInfoTextureName);
    	if (animated) {    		
    		gameInfoSprite.setAlpha(0);
    		gameInfoSprite.setVisible(true);
    		AlphaModifier alphaModifier = new AlphaModifier(animationDuration, GAME_INFO_START_ALPHA, GAME_INFO_FINISH_ALPHA, EaseStrongOut.getInstance());
    		alphaModifier.addModifierListener(entityModifier);
    		gameInfoSprite.registerEntityModifier(alphaModifier);
    	} else {
    		gameInfoSprite.setVisible(true);
    	}
	}
    
    private void hideGameInfos() {
    	for (Sprite gameInfoSprite : gameInfos.values()) {
    		gameInfoSprite.setVisible(false);
		}
	}
    
    private void createInfoSprite(GameLayer gameLayer, String textureName, float positionX, float positionY, float width, float height) {
    	Sprite gameInfoSprite = new Sprite(positionX,
    									   positionY,
    									   width,
    									   height,
    									   ResourceManager.getInstance().getTexture(textureName),
    									   getVertexBufferObjectManager());
    	gameInfoSprite.setVisible(false);
    	gameLayer.getLayerEntity().attachChild(gameInfoSprite);
    	gameInfos.put(textureName, gameInfoSprite);
	}
    
    
    /* //////////////////////////////////////////// DEBUG INFO */
    
    @SuppressLint("InflateParams")
	private void showDebugInfo() {
    	if (SHOW_DEBUGINFO_RULE) {    		
    		if (debugInfoLayout == null) {
    			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    			debugInfoLayout = inflater.inflate(R.layout.game_common_debuginfo, null);
    			debugInfoLayout.findViewById(R.id.game_common_debuginfo_separator).setVisibility(View.GONE);
    			
    			runOnUiThread(new Runnable() {
    				@Override
    				public void run() {
    					getGamePanelLayout().addView(debugInfoLayout);
    				}
    			});
    		}
    		
    		TextView seedTextView = (TextView) debugInfoLayout.findViewById(R.id.game_common_debuginfo_seed);
			seedTextView.setText("" + randomAccess.getRandomSeed());
			seedTextView.setPadding(0, 0, 200, 0);
    	}
    }
    
    /* //////////////////////////////////////////// POTME DIFFICULTY */
    
    public PotmeDifficulty getPotmeDifficulty() {
    	if (potmeDifficulty == null) {
    		User selectedUser = getTablexiaContext().getSelectedUser();
			boolean firstPlay = currentFirstPlay && (selectedUser != null && !selectedUser.wasGameTutorialPlayed(getGameDefinition()));
            potmeDifficulty = PotmeDifficulty.getPotmeDifficultyForGameDifficulty(getDifficulty(), firstPlay);
    	}
		return potmeDifficulty;
	}


    /* //////////////////////////////////////////// VICTORY SCREEN CALLBACKS */

    @Override
    public CharSequence getVictoryText(int progress) {
		switch (progress) {
		case -1:
			return getString(R.string.game_potme_victory_tutorial);
		case 0:
			return getString(R.string.game_potme_victory_0);
		case 1:
			return getString(R.string.game_potme_victory_1);
		case 2:
			return getString(R.string.game_potme_victory_2);
		case 3:
			return getString(R.string.game_potme_victory_3);
		default:
			throw new IllegalStateException("Unknown progress");
		}
    }
    @Override
    public String[] getVictorySpeech() {
        return new String[] { SpeechSounds.RESULT_POTME_0, SpeechSounds.RESULT_POTME_1, SpeechSounds.RESULT_POTME_2, SpeechSounds.RESULT_POTME_3 };
    }

    @Override
    public CharSequence getStatsText() {
        return !getPotmeDifficulty().hasResults() ? "" : Html.fromHtml(getString(R.string.game_potme_error_count, errorCount));
    }

    @Override
    public int countProgress() {
    	if (!getPotmeDifficulty().hasResults()) {
    		return -1;
    	} else if (errorCount < 2) {
            return 3;
        } else if (errorCount < 4) {
            return 2;
        } else if (errorCount < 8) {
            return 1;
        } else {
            return 0;
        }
    }

}
