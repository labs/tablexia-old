/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.strelnice.media;

import java.util.HashMap;

import org.andengine.opengl.texture.Texture;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;
import android.util.Pair;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.game.games.strelnice.StrelniceActivity;
import cz.nic.tablexia.util.andengine.ZipFileTexture;

/**
 * @author lhoracek
 */
public class GfxManager extends HashMap<TextureType, Pair<Texture, TextureRegion>> {

    private static final String  TAG         = GfxManager.class.getSimpleName();

    private static final String  TEXTURE_DIR = StrelniceActivity.BASE_DIR + "gfx/";

    private final TextureManager textureManager;
    private final AssetManager   assetManager;
    private final Context        context;

    public GfxManager(TextureManager textureManager, AssetManager assetManager, Context context) {
        super();
        this.textureManager = textureManager;
        this.assetManager = assetManager;
        this.context = context;
    }

    public void loadTextures() {
        Tablexia tablexia = (Tablexia) context.getApplicationContext();
        for (TextureType textureType : TextureType.values()) {
            String path = TEXTURE_DIR + textureType.getResource();

            Log.v(TAG, "Loading texture " + textureType.getResource());
            Texture texture = new ZipFileTexture(textureManager, tablexia.getZipResourceFile(), path);
            texture.load();
            TextureRegion textureRegion = TextureRegionFactory.extractFromTexture(texture);
            put(textureType, new Pair<Texture, TextureRegion>(texture, textureRegion));

        }
    }
}
