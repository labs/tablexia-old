/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.potme.map.tile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.potme.ResourceManager;

/**
 * Tile type definition
 * 
 * @author Matyáš Latner 
 *
 */
public enum TileType {
	
	TILE_0(ResourceManager.TILE_0, 0, false, false, false, false, false),
	TILE_1A(ResourceManager.TILE_1, 1, true, false, false, false, false),
	TILE_1B(ResourceManager.TILE_1, 2, false, true, false, false, false),
	TILE_1C(ResourceManager.TILE_1, 3, false, false, true, false, false),
	TILE_1D(ResourceManager.TILE_1, 0, false, false, false, true, false),
	TILE_2IV(ResourceManager.TILE_2I, 1, true, false, true, false, true),
	TILE_2IH(ResourceManager.TILE_2I, 0, false, true, false, true, true),
	TILE_2LA(ResourceManager.TILE_2L, 1, true, true, false, false, true),
	TILE_2LB(ResourceManager.TILE_2L, 2, false, true, true, false, true),
	TILE_2LC(ResourceManager.TILE_2L, 3, false, false, true, true, true),
	TILE_2LD(ResourceManager.TILE_2L, 0, true, false, false, true, true),
	TILE_3A(ResourceManager.TILE_3, 3, true, true, true, false, true),
	TILE_3B(ResourceManager.TILE_3, 0, false, true, true, true, true),
	TILE_3C(ResourceManager.TILE_3, 1, true, false, true, true, true),
	TILE_3D(ResourceManager.TILE_3, 2, true, true, false, true, true),
	TILE_4(ResourceManager.TILE_4, 0, true, true, true, true, true);
	
	public static final List<TileType> 	WALLS 			= Arrays.asList(new TileType[] { TILE_0 });
	public static final List<TileType> 	END_TILES 		= Arrays.asList(new TileType[] { TILE_1A, TILE_1B, TILE_1C, TILE_1D });
	
	private static final int 		 	MAX_ROTATION 	= 4;
	
	private String 	texture;
	private int 	rotation;
	private boolean topDoor;
	private boolean rightDoor;
	private boolean bottomDoor;
	private boolean leftDoor;
	private boolean walkThrough;
	
	/**
	 * Tile type definition
	 * 
	 * @param texture tile texture
	 * @param rotation tile texture rotation
	 * @param topDoor <code>true</code> for tile with top door
	 * @param rightDoor <code>true</code> for tile with right door
	 * @param bottomDoor <code>true</code> for tile with bottom door
	 * @param leftDoor <code>true</code> for tile with left door
	 * @param walkThrough <code>true</code> for tile with walk through possibility
	 */
	private TileType(String texture, int rotation, boolean topDoor, boolean rightDoor, boolean bottomDoor, boolean leftDoor, boolean walkThrough) {
		this.texture 	= texture;
		this.rotation 	= rotation;
		this.topDoor 	= topDoor;
		this.rightDoor 	= rightDoor;
		this.bottomDoor = bottomDoor;
		this.leftDoor 	= leftDoor;
		this.walkThrough = walkThrough;
	}

	public String getTexture() {
		return texture;
	}

	public int getRotation() {
		return rotation;
	}

	public boolean isTopDoor() {
		return topDoor;
	}

	public boolean isRightDoor() {
		return rightDoor;
	}

	public boolean isBottomDoor() {
		return bottomDoor;
	}

	public boolean isLeftDoor() {
		return leftDoor;
	}
	
	public boolean isWalkThrough() {
		return walkThrough;
	}
	
	public boolean isWall() {
		return WALLS.contains(this);
	}
	
	public static TileType getTileTypeForTextureAndRotation(String texture, int rotation) {
		for (TileType tileType : TileType.values()) {
			if (tileType.getRotation() == rotation && tileType.getTexture().equals(texture)) {
				return tileType;
			}
		}
		return null;
	}
	
	/**
	 * Returns inverted tile type. For tile type which has no inverted type returns <code>null</code>
	 * 
	 * @param tileType
	 * @return
	 */
	public static TileType getSwipedTileType(TileType tileType) {
		return getTileTypeForTextureAndRotation(tileType.getTexture(), (tileType.getRotation() + MAX_ROTATION / 2) % MAX_ROTATION);
	}
	
	/**
	 * Returns random end tile type
	 * 
	 * @param randomAccess
	 * @return random end tile type
	 */
	public static TileType getRandomEndTile(RandomAccess randomAccess) {
		return TileType.END_TILES.get(randomAccess.getRandom().nextInt(TileType.END_TILES.size()));
	}
	
	/**
	 * Return TyleType for specified doors and walkTrough combination.
	 * <code>null</code> for door and walkThrough is wildcard.
	 * If invalid condition is specified method throws IllegalStateException.
	 * 
	 * @param topDoor <code>true</code> for tile with top door
	 * @param rightDoor <code>true</code> for tile with right door
	 * @param bottomDoor <code>true</code> for tile with bottom door
	 * @param leftDoor <code>true</code> for tile with left door
	 * @param walkThrough <code>true</code> for tile with walk through possibility
	 * @param randomAccess {@link RandomAccess} instance for selecting random tiles
	 * 
	 * @return randomly selected TileType for specified door and walkTrough combination
	 */
	public static TileType getRandomTileTypeForAvailableDoors(Boolean topDoor, Boolean rightDoor, Boolean bottomDoor, Boolean leftDoor, Boolean walkThrough, RandomAccess randomAccess) {
		List<TileType> values = new ArrayList<TileType>();
		for (TileType tileType : values()) {
			if ((topDoor == null || tileType.topDoor == topDoor)
					&& (rightDoor == null || tileType.rightDoor == rightDoor)
					&& (bottomDoor == null || tileType.bottomDoor == bottomDoor)
					&& (leftDoor == null || tileType.leftDoor == leftDoor)
					&& (walkThrough == null || tileType.walkThrough == walkThrough)) {
				
				values.add(tileType);
			}
		}
		
		if (values.size() == 0) {
			throw new IllegalStateException("Cannot generate random tile for rule [" + topDoor + ", " + rightDoor + ", " + bottomDoor + ", " + leftDoor + ", " + walkThrough + "]");
		}
		
		return values.get(randomAccess.getRandom().nextInt(values.size()));
	}

}
