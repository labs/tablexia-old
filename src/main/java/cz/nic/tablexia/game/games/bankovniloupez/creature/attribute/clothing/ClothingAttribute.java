/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing;

import java.util.ArrayList;
import java.util.List;

import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute.CreatureSuperGenericType;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.accessories.AccessoriesAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.bottom.BottomAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.headgear.HeadgearAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.top.TopAttribute;

@CreatureSuperGenericType(isGeneric = true)
public abstract class ClothingAttribute extends Attribute {

    public static List<AttributeDescription> getTextures() {
        return new ArrayList<ResourceManager.AttributeDescription>() {

            private static final long serialVersionUID = 8692724100864869650L;

            {
                addAll(HeadgearAttribute.getTextures());
                addAll(TopAttribute.getTextures());
                addAll(BottomAttribute.getTextures());
                addAll(AccessoriesAttribute.getTextures());
            }
        };
    }

    public ClothingAttribute(AttributeColor color, float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(color, pX, pY, pTextureRegion, pVertexBufferObjectManager);
    }

}
