/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.pronasledovani;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.andengine.engine.options.EngineOptions;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.RotationModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.RepeatingSpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.input.touch.controller.MultiTouch;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.util.modifier.IModifier;
import org.andengine.util.modifier.IModifier.IModifierListener;
import org.andengine.util.modifier.ease.EaseLinear;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.MotionEvent.PointerCoords;
import android.view.MotionEvent.PointerProperties;
import android.view.View;
import android.widget.TextView;

import com.activeandroid.util.Log;
import com.splunk.mint.Mint;

import cz.nic.tablexia.R;
import cz.nic.tablexia.audio.resources.SpeechSounds;
import cz.nic.tablexia.game.GameActivity;
import cz.nic.tablexia.game.games.GamesDefinition;
import cz.nic.tablexia.game.games.pronasledovani.CarDragTouchListener.OnDragCompleteListener;
import cz.nic.tablexia.game.games.pronasledovani.model.Card;
import cz.nic.tablexia.game.games.pronasledovani.model.MapSetup;
import cz.nic.tablexia.game.games.pronasledovani.model.MyTime;
import cz.nic.tablexia.game.games.pronasledovani.model.Position;
import cz.nic.tablexia.game.games.pronasledovani.model.TaggedSprite;
import cz.nic.tablexia.game.manager.GameManager;

/**
 * (c) 2013 CZ.NIC
 *
 * @author Vaclav Tarantik
 */
public class PronasledovaniActivity extends GameActivity implements IOnSceneTouchListener {
    // ===========================================================
    // Constants
    // ===========================================================

    private static final int[] DIFFICULTIES = new int[] { 3, 4, 5 };
    private static final String TAG = PronasledovaniActivity.class.getSimpleName();
    public static final String BASE_DIR = "pronasledovani/";
    public static final String                                TEXTURES_DIR                   = "textures/";
    private static final int MAPSCOUNT = 14; // pri pridani mapy vzdy pridat souradnice pro auticko
    private static final boolean                              SHUFFLE_MAP                    = true;
    private static final int MINIMUM_ROTATION_FOR_ANIMATION = 20; // o kolik stupnu se musi dilek minimalne otocit,aby
    // se nedoanimoval
    // zpet na
    private static final int ALPHA_MAP_SCALE_FACTOR = 2;
    private static final int                                  VEHICLE_TEXTURE_TAG            = -21;

    // puvodni hodnotu rotace

    private static final int ASSET_BITMAP_SIZE = 1300;
    private static final int GRID_OFFSET_Y = 10;
    private static final String                               MAP_BITMAPS_EXTENSION          = ".png";

    private static final String[]                             maps                           = new String[] { "mapa1", "mapa5", "mapa3", "mapa4", "mapa2", "mapa6", "mapa7", "mapa8", "mapa9", "mapa10", "mapa11", "mapa12", "mapa13", "mapa14" };

    private static final ArrayList<Entry<Position, Position>> carStartAndEndPoints = new ArrayList<Map.Entry<Position, Position>>();
    {
        carStartAndEndPoints.add(new AbstractMap.SimpleEntry<Position, Position>(new Position(1140, 1230), new Position(340, 250)));//1
        carStartAndEndPoints.add(new AbstractMap.SimpleEntry<Position, Position>(new Position(1215, 1140), new Position(1115, 230)));//5
        carStartAndEndPoints.add(new AbstractMap.SimpleEntry<Position, Position>(new Position(101, 769), new Position(1027, 381)));//3
        carStartAndEndPoints.add(new AbstractMap.SimpleEntry<Position, Position>(new Position(1231, 875), new Position(237, 555)));//4
        carStartAndEndPoints.add(new AbstractMap.SimpleEntry<Position, Position>(new Position(830, 1161), new Position(271, 397)));//2
        carStartAndEndPoints.add(new AbstractMap.SimpleEntry<Position, Position>(new Position(1233, 1075), new Position(589, 885)));//6
        carStartAndEndPoints.add(new AbstractMap.SimpleEntry<Position, Position>(new Position(725, 1230), new Position(465, 230)));//7
        carStartAndEndPoints.add(new AbstractMap.SimpleEntry<Position, Position>(new Position(205, 1049), new Position(1080, 297)));//8
        carStartAndEndPoints.add(new AbstractMap.SimpleEntry<Position, Position>(new Position(180, 1200), new Position(1050, 220)));//9
        carStartAndEndPoints.add(new AbstractMap.SimpleEntry<Position, Position>(new Position(291, 1213), new Position(987, 211)));//10
        carStartAndEndPoints.add(new AbstractMap.SimpleEntry<Position, Position>(new Position(791, 1161), new Position(271, 397)));//11
        carStartAndEndPoints.add(new AbstractMap.SimpleEntry<Position, Position>(new Position(1163, 1083), new Position(263, 193)));//12
        carStartAndEndPoints.add(new AbstractMap.SimpleEntry<Position, Position>(new Position(100, 1220), new Position(1140, 350)));//13
        carStartAndEndPoints.add(new AbstractMap.SimpleEntry<Position, Position>(new Position(233, 1200), new Position(415, 210)));//14

    }

    // ===========================================================
    // Fields
    // ===========================================================

    private float gameTime; // herni cas
    private float startTime;
    private float endTime;

    private int movesCounter; // citac pohybu dilky, pocita si presun a otoceni

    private int gridSize; // pocet dilku v mrizce na jedne souradnici
    private int gridOffsetX; // odsazeni mrizky na ose X
    private int pieceTouched; // poradi posouvaneho dilku v mrizce
    private int pieceHovered; // poradi prave prekruyvaneho dilku v mrizce

    private int cardWidth; // sirka karty samotne
    private int textureID; // cislo mapy, ktera bude zobrazena pri startu
    // aplikace,
    // po dohrani mapy se cislo v DB inkrementuje

    private boolean gameRunning;

    private Position firstFingerFirstTouch;
    private Position secondFingerFirstTouch;

    private HashMap<Card, ITextureRegion> mCardTotextureRegionMap;
    // List, ktery obsahuje vsechny karty a jejich info
    private ArrayList<Card> cards;

    private int fingersCounter; // pocet prstu, ktere se dotykaji obrazovky
    private boolean oneFingerDown;
    private boolean twoFingersDown;
    double distanceMovedWithFirstFinger; // index 0
    double distanceMovedWithSecondFinger; // index 1

    private double initialAngle; // pocatecni uhel mezi prsty po polozeni
    // druheho
    // z nich
    private double currentAngle; // uhel v momente zaregistrovani pohybu

    private MoveModifier moveModifier;
    private RotationModifier rotationModifier;

    ArrayList<Card> cardsLoaded; // list karet ktery se pouziva pro obnovni hry
    // do puvodniho stavu pri zapauzovani
    private boolean gameLoadedFromPreviousState = false;
    Position pieceTouchedCenter;
    float xDiff; // x posun doteku oproti stredu dilku
    float yDiff; // y posun doteku oproti stredu dilku

    double delta; // rozdil uhlu
    float initialRotation; // uhel, ktery sviarly prsty pri prvnim doteku
    float finalRotation; // uhel mezi prsty po dokonceni rotace

    private Position initialFirstFingerPosition;
    private Position initialSecondFingerPosition;

    private PointerCoords firstFingerCoordinates;
    private PointerCoords secondFingerCoordinates;
    private boolean fingersSwitched;

    // textura predstavujici stin pod dilkem
    private TextureRegion shadowTextureRegion;
    private Sprite shadowSprite;

    // pozadi
    private ITexture backgroundTexture;
    private RepeatingSpriteBackground background;

    private long firstFingerReleaseTime; // cas zvednuti prvniho prstu
    private long secondFingerReleaseTime; // cas zvednuti druheho prstu

    private Position firstFingerReleasePosition; // pozice, na ktere byl prvni
    // prst pusten pro pripadne
    // doanimovani z teto

    private Handler handler;
    private boolean waitForAnimation;
    private boolean animationCanceled;

    SfxManager sfxManager;
    private TextureRegion                                     vehicleTextureRegion;                                                               // auticko, ktere se zobraz po dohrani hry
    private TextureRegion flagTextureRegion; // oznaceni sidla lupicu na mape

    int coordinationArrayPointer;
    Position[] lastFiveCarCoordinates;

    private CardDeckGenerator cardDeckGenerator;
    private TexturesGenerator texturesGenerator;

    private VehicleTypeEnum                                   vehicleType;
    private Random                                            random                             = new Random();

    // ===========================================================
    // Constructors
    // ===========================================================

    public PronasledovaniActivity() {
        super(GamesDefinition.PRONASLEDOVANI);
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public EngineOptions onCreateEngineOptions() {
        final EngineOptions engineOptions = super.onCreateEngineOptions();
        engineOptions.getTouchOptions().setNeedsMultiTouch(true);
        engineOptions.getAudioOptions().setNeedsSound(true).setNeedsMusic(true);

        if (MultiTouch.isSupported(this)) {
            if (MultiTouch.isSupportedDistinct(this)) {
                Log.d(TAG, "MultiTouch detected --> Both controls will work properly!");
            } else {
                Log.d(TAG, "MultiTouch detected, but your device has problems distinguishing between fingers.\n\nControls are placed at different vertical locations.");
            }
        } else {
            Log.d(TAG, "Sorry your device does NOT support MultiTouch!\n\n(Falling back to SingleTouch.)\n\nControls are placed at different vertical locations.");
        }
        return engineOptions;

    }

    @Override
    public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws IOException {
        sfxManager = new SfxManager(getSoundManager(), this);
        sfxManager.loadSounds();

        texturesGenerator = new TexturesGenerator(this, getTextureManager(), getAssets());
        // VLAJKA OZNACUJICI SIDLO LUPICU
        flagTextureRegion = texturesGenerator.getFlagTextureRegion();
        // POZADI
        backgroundTexture = texturesGenerator.getBackgroundTexture();
        final ITextureRegion backgroundTextureRegion = TextureRegionFactory.extractFromTexture(backgroundTexture);
        background = new RepeatingSpriteBackground(camera.getCameraSceneWidth(), camera.getCameraSceneHeight(), backgroundTextureRegion, getVertexBufferObjectManager());
        background.getSprite().setTag(-10);
        shadowTextureRegion = texturesGenerator.getShadowTextureRegion();
        pOnCreateResourcesCallback.onCreateResourcesFinished();

    }

    @Override
    protected void onCreateTablexiaScene() {
        // mEngine.registerUpdateHandler(new FPSLogger());
        scene.setOnAreaTouchTraversalFrontToBack();
    }

    /*
     * Metoda nastavi prostredi, nahodne vybere texturu, ktera se pouzije pro
     * skladanou mapu
     */
    private void setEnvironment() {
        GameManager lastGameManager = GameManager.getLastGameManager(getTablexiaContext().getSelectedUser(), GamesDefinition.PRONASLEDOVANI);
        if (lastGameManager != null) {
            int lastPlayedMapID = lastGameManager.getExtraInt1();
            Log.d("MAPID", Integer.toString(lastPlayedMapID));
            textureID = (lastPlayedMapID == (MAPSCOUNT - 1)) ? 0 : lastPlayedMapID + 1;
        } else {
            textureID = 0;// pokud jde o prvni hranou hru, nastavi se ID textury na 0
        }

        coordinationArrayPointer = 0;
        lastFiveCarCoordinates = new Position[5];
        pieceTouched = -1;
        pieceHovered = -1;
        shadowSprite = null;
        startTime = scene.getSecondsElapsedTotal();
        animationCanceled = false;
        firstFingerReleasePosition = new Position(0, 0);
        waitForAnimation = false;
        fingersSwitched = false;
        firstFingerCoordinates = new PointerCoords();
        secondFingerCoordinates = new PointerCoords();
        initialFirstFingerPosition = new Position(0, 0);
        initialSecondFingerPosition = new Position(0, 0);

        //hra byla obnovena
        if (!gameLoadedFromPreviousState) {
            gridSize = DIFFICULTIES[getDifficulty().ordinal()];
            gameTime = 0;
            movesCounter = 0;
        }
        Log.d((textureID) + ".png");

        // vybrani mapy, ktera je aktualne v poradi a pridani do hry
        String currentMapName = maps[textureID] + MAP_BITMAPS_EXTENSION;

        cardDeckGenerator = new CardDeckGenerator(this, gridSize, currentMapName, getTextureManager(), displaySize, GRID_OFFSET_Y);
        cards = cardDeckGenerator.getCards();
        mCardTotextureRegionMap = cardDeckGenerator.getmCardTotextureRegionMap();
        gridOffsetX = cardDeckGenerator.getGridOffset().x;
        cardWidth = cardDeckGenerator.getCardWidth();

        fingersCounter = 0;
        firstFingerFirstTouch = new Position(0, 0);
        secondFingerFirstTouch = new Position(0, 0);
        distanceMovedWithFirstFinger = 0;
        distanceMovedWithSecondFinger = 0;

        gameRunning = true;
        // Pokud byla hra nahrana z predchoziho stavu, nastavi se kartam
        // aktualni pozice podle posledni ulozene
        if (gameLoadedFromPreviousState) {
            for (int i = 0; i < cardsLoaded.size(); i++) {
                cards.get(i).setActualPositionInGrid(cardsLoaded.get(i).getActualPositionInGrid());
                cards.get(i).setRotation(cardsLoaded.get(i).getRotation());
            }

            gameLoadedFromPreviousState = false;
        } else {
            if (SHUFFLE_MAP) {
                shuffleCards();// zamichani karet
            }
        }

        // Pridani karet na scenu
        for (Card c : cards) {
            Position position = getCardPositionInPixels(c.getActualPositionInGrid());
            addCard(c, position.getX(), position.getY());
        }

        scene.setBackground(background);
        scene.setTouchAreaBindingOnActionDownEnabled(true);
        scene.setOnSceneTouchListener(this);
    }

    // ===========================================================
    // Methods
    // ===========================================================

    private void addCard(final Card pCard, final int pX, final int pY) {

        final TaggedSprite taggedSprite = new TaggedSprite(pX, pY, pCard,

        mCardTotextureRegionMap.get(pCard), getVertexBufferObjectManager(), pCard.getActualPositionInGrid()) {

            // Obsluha doteku prvniho prstu
            @Override
            public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
                if (gameRunning == true) {// hra bezi
                    switch (pSceneTouchEvent.getAction()) {
                        case TouchEvent.ACTION_DOWN:
                            onActionDown(pSceneTouchEvent);
                            break;
                        case TouchEvent.ACTION_MOVE:
                            onActionMove(pSceneTouchEvent);
                            break;
                        case TouchEvent.ACTION_UP:
                            onActionUp(pSceneTouchEvent);
                            break;
                        case TouchEvent.ACTION_CANCEL:
                            if (fingersCounter > 0) {
                                fingersCounter--;
                            }
                            if (fingersCounter == 1) {
                                oneFingerDown = true;
                                twoFingersDown = false;
                            } else {
                                oneFingerDown = false;
                                twoFingersDown = false;
                            }

                            break;
                    }
                    return true;

                } else {// obsluha po konci hry
                    return true;
                }

            }

        };
        taggedSprite.setRotation(pCard.getRotation() * 90);
        scene.attachChild(taggedSprite);
        scene.registerTouchArea(taggedSprite);
    }

    // =========================================================================================================
    // Help Methods
    // ========================================================================================================
    /*
     * Metoda vraci poradi karty v mrizce podle souradnic, ktere jsou predany v
     * parametru. Karty se cisluji od leveho horniho rohu Pokud je to pozice
     * mimo mrizku, metoda vraci hodnotu -1
     */
    private int getPieceListPositionFromCoordinates(float x, float y) {
        if (gridSize != 0) {

            if ((x <= (gridOffsetX + (gridSize * cardWidth))) && (x > gridOffsetX) && (y <= (GRID_OFFSET_Y + (gridSize * cardWidth))) && (y > GRID_OFFSET_Y)) {
                int a = ((int) (x - gridOffsetX)) / (cardWidth);
                int b = ((int) (y - GRID_OFFSET_Y)) / cardWidth;
                return a + (gridSize * b);
            } else {
                return -1;
            }

        }
        return -1;
    }

    /*
     * Metoda pro zamichani karet ve mrizce vygeneruje hodnoty od 0 do poctu
     * karet v mrizce -1 nastavi pozice karet na tyto nove hodnoty a rovnez
     * ulozi jejich puvodni pozice
     */
    public void shuffleCards() {
        int puzzlePiecesNumber = gridSize * gridSize;
        int possibleDirections = 4;
        boolean done = false;
        // Pomocny list, do ktereho si ukladame vygenerovane zmenene pozice
        ArrayList<Integer> generatedPositions = new ArrayList<Integer>();

        while (!done) {
            int generated = 0 + (int) (Math.random() * puzzlePiecesNumber);
            if (!generatedPositions.contains(generated) && (generatedPositions.size() < puzzlePiecesNumber)) {
                generatedPositions.add(generated);
            } else if (generatedPositions.size() == puzzlePiecesNumber) {
                done = true;
            }
        }
        // nahodne natoceni dilku
        for (int i = 0; i < cards.size(); i++) {
            cards.get(i).setActualPositionInGrid(generatedPositions.get(i));
            cards.get(i).setRotation(0 + (int) (Math.random() * possibleDirections));
        }
    }

    /*
     * Metoda vraci pozici karty s ID predanym v parametru v pixelech
     */
    public Position getCardPositionInPixels(int cardID) {
        // pricita se polovina velikosti karty kvuli novemu systemu pozic v
        // enginu
        return new Position((gridOffsetX + ((cardID % gridSize) * cardWidth)) + (cardWidth / 2), GRID_OFFSET_Y + ((cardID / gridSize) * cardWidth) + (cardWidth / 2));
    }

    /*
     * Metoda, ktera kontroluje, jestli jsou vsechny dilky na svych pozicich
     */
    public boolean checkWhetherDone() {
        ArrayList<Integer> alternativePositionsInGrid = getCardsCorrectPositionsAsList();
        int puzzleRotation = cards.get(0).getRotation();

        // transponuje aktualni spravne poradi podle toho, jak je natocen prvni
        // dilek
        for (int i = 0; i < puzzleRotation; i++) {
            alternativePositionsInGrid = createListTransposition(alternativePositionsInGrid);
        }

        for (int i = 0; i < cards.size(); i++) {
            if (cards.get(i).getActualPositionInGrid() != alternativePositionsInGrid.get(i)) {
                return false;
            }
            if (cards.get(i).getRotation() != puzzleRotation) {// karta neni
                // natocena
                // spravne
                return false;
            }
        }
        return true;

    }

    /*
     * Metoda dostava jako parametr pozici dilku v mrizce a vraci ID tohoto
     * dilku pokud se aktualni pozice dilku s touto shoduje
     */
    public int getCardIDFromActualPosition(int actualPosition) {
        for (int i = 0; i < cards.size(); i++) {
            if (cards.get(i).getActualPositionInGrid() == actualPosition) {
                return i;
            }
        }
        return -1;
    }

    /*
     * Obsluha hry pri slozeni obrazce
     */
    public void onVictory() {
        endTime = scene.getSecondsElapsedTotal();
        gameTime = endTime - startTime;
        Log.d("GAMETIME", Float.toString(gameTime));
        gameRunning = false;
        getGameManager().setCounterAndSave(gameTime);
        Log.d("SAVEDTEXTUREID", Integer.toString(textureID));
        getGameManager().setExtraInt1AndSave(textureID);

        // Zobrazeni a obsluha auticka po dohrani hry
        String currentRedMapName = BASE_DIR + TEXTURES_DIR + maps[textureID] + "red" + MAP_BITMAPS_EXTENSION;
        Log.d(TAG, "Loading red bitmap: " + currentRedMapName);

        Entry<Position, Position> carCoords = carStartAndEndPoints.get(textureID);
        Position startPosition = carCoords.getKey();
        Position endPosition = carCoords.getValue();
        // Prepocet startovnich pozic dle aktualnich rozmeru displeje
        final int carStartPosX = (int) ((startPosition.getX()) / (ASSET_BITMAP_SIZE / ((float) cardWidth * (float) gridSize))) + gridOffsetX;
        final int carStartPosY = (displaySize.y - (int) ((startPosition.getY()) / (ASSET_BITMAP_SIZE / (cardWidth * (float) gridSize)))) + GRID_OFFSET_Y;
        // translace souradnic podle natoceni mapy

        Position translatedPosCar = PronasledovaniHelper.translatePositionAccordingToRotation(new Position(carStartPosX, carStartPosY), cards.get(0).getRotation() * 90, new Position(
        displaySize.x / 2, displaySize.y / 2));

        final int flagStartPosX = (int) ((endPosition.getX()) / (ASSET_BITMAP_SIZE / (cardWidth * (float) gridSize))) + gridOffsetX;
        final int flagStartPosY = (displaySize.y - (int) ((endPosition.getY()) / (ASSET_BITMAP_SIZE / (cardWidth * (float) gridSize)))) + GRID_OFFSET_Y;
        // prepocet souradnic podle natoceni mapy
        Position translatedPosFlag = PronasledovaniHelper.translatePositionAccordingToRotation(new Position(flagStartPosX, flagStartPosY), cards.get(0).getRotation() * 90, new Position(
        displaySize.x / 2, displaySize.y / 2));

        vehicleType = getVehicleTypeByMap(maps[textureID]);

        vehicleTextureRegion = texturesGenerator.getRandomVehicleTextureRegion(vehicleType);

        final Sprite carSprite = new TaggedSprite(translatedPosCar.getX(), translatedPosCar.getY(), vehicleTextureRegion, getVertexBufferObjectManager(), VEHICLE_TEXTURE_TAG);
        final MapSetup map = new MapSetup(cardWidth * gridSize, cards.get(0).getRotation(), currentRedMapName, new Point(gridOffsetX, GRID_OFFSET_Y), ALPHA_MAP_SCALE_FACTOR);
        runOnUpdateThread(new Runnable() {

            @Override
            public void run() {
                scene.clearTouchAreas();
            }
        });

        // FLAG
        final Sprite flagSprite = new TaggedSprite(translatedPosFlag.getX() + 24.5f, translatedPosFlag.getY() + 48.5f, flagTextureRegion, getVertexBufferObjectManager(), -30);
        flagSprite.setZIndex(10);
        final Rectangle flagRectangle = new Rectangle(flagSprite.getX(), flagSprite.getY(), flagSprite.getWidth(), flagSprite.getHeight(), getVertexBufferObjectManager());
        scene.attachChild(flagRectangle);
        flagRectangle.setAlpha(0f);

        final Rectangle carRectangle = new Rectangle(carSprite.getX(), carSprite.getY(), carSprite.getWidth(), carSprite.getHeight(), getVertexBufferObjectManager());
        scene.attachChild(carRectangle);
        carRectangle.setAlpha(0f);
        carSprite.setZIndex(10);
        scene.sortChildren();

        carSprite.setRotation(cards.get(0).getRotation() * 90);

        final CarDragTouchListener carDragTouchListener = new CarDragTouchListener(PronasledovaniActivity.this, new Point(carStartPosX, carStartPosY), carSprite, new Point(gridOffsetX, GRID_OFFSET_Y), map, carRectangle,
        flagRectangle, new OnDragCompleteListener() {
            @Override
            public void onDragComplete() {
                runOnUpdateThread(new Runnable() {

                    @Override
                    public void run() {
                        carSprite.detachSelf();
                        carRectangle.detachSelf();
                        flagRectangle.detachSelf();
                        flagSprite.detachSelf();
                        scene.setOnSceneTouchListener(null);
                    }
                });
                gameComplete();
            }
        }, getSoundManager(), vehicleType);

        scene.setOnSceneTouchListener(carDragTouchListener);

        scene.attachChild(flagSprite);
        scene.attachChild(carSprite);
    }

    /*
     * Metoda transponuje predany list, abychom dokazali vyhodnotit, jestli je
     * obrazek spravne slozen v jakemkoli smeru
     */
    public ArrayList<Integer> createListTransposition(ArrayList<Integer> listToBeTransposed) {
        ArrayList<Integer> listToBeReturned = new ArrayList<Integer>(listToBeTransposed.size());
        int repetitionCounter = gridSize - 1;
        for (int i = repetitionCounter; i >= 0; i--) {
            for (int j = 0; j < gridSize; j++) {
                listToBeReturned.add(listToBeTransposed.get(listToBeTransposed.size() - 1 - ((gridSize * j) + repetitionCounter)));
            }
            repetitionCounter--;
        }

        return listToBeReturned;
    }

    /*
     * Metoda vrati spravne poradi karet jako list
     */
    ArrayList<Integer> getCardsCorrectPositionsAsList() {
        ArrayList<Integer> cardsCorrectPositions = new ArrayList<Integer>();
        for (int i = 0; i < (gridSize * gridSize); i++) {
            cardsCorrectPositions.add(cards.get(i).getCorrectPositionInGrid());
        }
        return cardsCorrectPositions;
    }

    /*
     * Metoda doanimuje dilek s danym id z pozice original na pozici future
     */
    private void animateInPosition(final int pieceID, final float originalX, final float originalY, final float futureX, final float futureY) {
        if ((pieceID != -1) && (scene.getChildByTag(pieceID) != null)) {
            if (moveModifier != null) {
                scene.getChildByTag(pieceID).unregisterEntityModifier(moveModifier);
                shadowSprite.unregisterEntityModifier(moveModifier);
            }
            moveModifier = new MoveModifier(0.2f, originalX, originalY, futureX, futureY, EaseLinear.getInstance());
            moveModifier.addModifierListener(new IModifierListener<IEntity>() {

                @Override
                public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                    // nothing
                }

                @Override
                public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                    if (gameRunning && checkWhetherDone()) {
                        onVictory();
                    }
                }
            });
            scene.getChildByTag(pieceID).registerEntityModifier(moveModifier);
            shadowSprite.registerEntityModifier(moveModifier);
        }
        if (shadowSprite != null) {
            shadowSprite.setVisible(false);
        }
    }

    private void rotatePieceToAngle(int pieceID, float initialRotation, float endRotation) {
        if ((pieceID != -1) && (scene.getChildByTag(pieceID) != null)) {
            if (rotationModifier != null) {
                scene.getChildByTag(pieceID).unregisterEntityModifier(rotationModifier);
                shadowSprite.unregisterEntityModifier(rotationModifier);
            }
            rotationModifier = new RotationModifier(0.4f, initialRotation, endRotation, EaseLinear.getInstance());
            rotationModifier.addModifierListener(new IModifierListener<IEntity>() {

                @Override
                public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                    // nothing
                }

                @Override
                public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                    if (gameRunning && checkWhetherDone()) {
                        onVictory();
                    }

                }
            });
            scene.getChildByTag(pieceID).registerEntityModifier(rotationModifier);
            shadowSprite.registerEntityModifier(rotationModifier);
        }
        shadowSprite.setVisible(false);
    }

    // TODO pozdeji dodelat aby animace fungovala
    private void scalePieceToSize(int pieceID, float initialScale, float finalScale) {
        if ((pieceID != -1) && (scene.getChildByTag(pieceID) != null)) {
            // if (scaleModifier != null) {
            // scene.getChildByTag(pieceID).unregisterEntityModifier(
            // scaleModifier);
            // }
            // scaleModifier = new ScaleModifier(0.2f, initialScale,
            // finalScale, EaseLinear.getInstance());
            // scene.getChildByTag(pieceID).registerEntityModifier(
            // scaleModifier);
            scene.getChildByTag(pieceID).setScale(finalScale);
        }
    }

    /*
     * Metoda prohodi dva dilky mezi sebou
     */
    private void switchPieces(Point actualTouchPoint, int pieceDraggedID, int pieceReleasedOnID) {
        // vypocet pozic obou dilku pro pozdejsi moznost animovani a prohozeni
        Position firstPiecePosition = getCardPositionInPixels(pieceDraggedID);
        Position secondPiecePosition = getCardPositionInPixels(pieceReleasedOnID);
        // tazenemu dilek doanimujeme na pozici, nad kterou jsme pustili prst
        animateInPosition(pieceDraggedID, actualTouchPoint.x, actualTouchPoint.y, getCardPositionInPixels(pieceReleasedOnID).getX(), getCardPositionInPixels(pieceReleasedOnID).getY());

        // druhy dilek doanimujeme na pozici prvniho
        animateInPosition(pieceReleasedOnID, secondPiecePosition.getX(), secondPiecePosition.getY(), firstPiecePosition.getX(), firstPiecePosition.getY());
        // pomocna promenna pro pozdejsi prohozeni tagu
        int workingTag = -5;

        if (scene.getChildByTag(pieceDraggedID) != null) {
            scene.getChildByTag(pieceDraggedID).setTag(workingTag);
        }
        if (scene.getChildByTag(pieceReleasedOnID) != null) {
            scene.getChildByTag(pieceReleasedOnID).setTag(pieceDraggedID);
        }

        if (scene.getChildByTag(workingTag) != null) {
            scene.getChildByTag(workingTag).setTag(pieceReleasedOnID);
        }

        // scale dilku, ktery jsme meli puvodne pod prstem na puvodni velikost
        scalePieceToSize(pieceDraggedID, 1.2f, 1.0f);
        sfxManager.playSound(SoundsEnum.SOUND_SWITCHCARDS);

    }

    /*
     * Metoda pro zvyrazneni dilku, nad kterym prave projizdime
     */
    private void scaleHoveredPiece(int hoveredPieceID) {
        if ((hoveredPieceID != -1) && (scene.getChildByTag(hoveredPieceID) != null)) {
            for (int i = 0; i < (gridSize * gridSize); i++) {
                if ((i == hoveredPieceID) && (i != pieceTouched)) {
                    if (!scene.getChildByTag(i).isScaled()) {
                        scalePieceToSize(i, 1.0f, 1.1f);
                        scene.getChildByTag(i).setZIndex(2);
                    }

                } else if (i != pieceTouched) {
                    scene.getChildByTag(i).setScale(1.0f);
                    scene.getChildByTag(i).setZIndex(0);
                }
            }
        } else {
            for (int i = 0; i < (gridSize * gridSize); i++) {
                if (i != pieceTouched) {
                    scene.getChildByTag(i).setScale(1.0f);
                    scene.getChildByTag(i).setZIndex(0);
                }
            }
        }
        scene.sortChildren();
    }

    /*
     * Obsluha pri zjisteni zvednuti prstu z obrazovky
     */
    private void onActionUp(TouchEvent pSceneTouchEvent) {
        fingersCounter = pSceneTouchEvent.getMotionEvent().getPointerCount();
        fingersCounter--;
        if (twoFingersDown) {// pred tim, nez byly pusteny vsechny prsty se
            // dotykaly dva prsty

            if ((fingersCounter == 1) && (pieceTouched != -1)) {
                if ((pSceneTouchEvent.getPointerID() == 0) && !fingersSwitched) {
                    firstFingerReleasePosition.setX((int) pSceneTouchEvent.getX());
                    firstFingerReleasePosition.setY((int) pSceneTouchEvent.getY());
                    waitForAnimation = true;
                    firstFingerReleaseTime = System.currentTimeMillis();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            animateInPosition(pieceTouched, firstFingerFirstTouch.getX(), firstFingerFirstTouch.getY(), secondFingerFirstTouch.getX(), secondFingerFirstTouch.getY());
                            waitForAnimation = false;
                        }
                    }, 300);
                    int pieceWithSecondFingerOnID = getPieceListPositionFromCoordinates(secondFingerFirstTouch.getX(), secondFingerFirstTouch.getY());
                    if ((pieceWithSecondFingerOnID == pieceTouched) && (scene.getChildByTag(pieceTouched) != null)) {
                        pieceTouchedCenter = new Position((int) scene.getChildByTag(pieceTouched).getX(), (int) scene.getChildByTag(pieceTouched).getY());
                        xDiff = pieceTouchedCenter.getX() - secondFingerFirstTouch.getX();
                        yDiff = pieceTouchedCenter.getY() - secondFingerFirstTouch.getY();
                    } else {
                        xDiff = 0;
                        yDiff = 0;
                    }
                    fingersSwitched = true;
                } else if ((pSceneTouchEvent.getPointerID() == 1) && fingersSwitched) {
                    firstFingerReleasePosition.setX((int) pSceneTouchEvent.getX());
                    firstFingerReleasePosition.setY((int) pSceneTouchEvent.getY());
                    waitForAnimation = true;
                    firstFingerReleaseTime = System.currentTimeMillis();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            animateInPosition(pieceTouched, firstFingerFirstTouch.getX(), firstFingerFirstTouch.getY(), secondFingerFirstTouch.getX(), secondFingerFirstTouch.getY());
                            waitForAnimation = false;
                        }
                    }, 300);

                    int pieceWithSecondFingerOnID = getPieceListPositionFromCoordinates(firstFingerFirstTouch.getX(), firstFingerFirstTouch.getY());
                    if ((pieceWithSecondFingerOnID == pieceTouched) && (scene.getChildByTag(pieceTouched) != null)) {
                        pieceTouchedCenter = new Position((int) scene.getChildByTag(pieceTouched).getX(), (int) scene.getChildByTag(pieceTouched).getY());
                        xDiff = pieceTouchedCenter.getX() - secondFingerFirstTouch.getX();
                        yDiff = pieceTouchedCenter.getY() - secondFingerFirstTouch.getY();
                    } else {
                        xDiff = 0;
                        yDiff = 0;
                    }
                    fingersSwitched = false;
                }
                finishRotation(pieceTouched);
                delta = 0;
                initialAngle = 0;
                currentAngle = 0;
                twoFingersDown = false;
                oneFingerDown = true;

            }

        } else if (oneFingerDown) {// pred tim, nez byly pusteny vsechny prsty
            // se dotykal pouze jeden prst
            if (fingersCounter == 0) {
                twoFingersDown = false;
                oneFingerDown = false;
                if (pieceTouched != -1) {
                    secondFingerReleaseTime = System.currentTimeMillis();
                    if ((secondFingerReleaseTime - firstFingerReleaseTime) < 300) {
                        handler.removeCallbacksAndMessages(null);
                        animationCanceled = true;
                        waitForAnimation = false;
                    }
                    // dilek je vyndan mimo mrizku, doanimujeme na puvodni
                    // pozici
                    if ((pieceHovered == -1) || (pieceHovered == pieceTouched)) {
                        Position firstPiecePosition = getCardPositionInPixels(pieceTouched);
                        if (!animationCanceled) {
                            animateInPosition(pieceTouched, pSceneTouchEvent.getX() + xDiff, pSceneTouchEvent.getY() + yDiff, firstPiecePosition.getX(), firstPiecePosition.getY());
                        } else {
                            animateInPosition(pieceTouched, firstFingerReleasePosition.getX(), firstFingerReleasePosition.getY(), firstPiecePosition.getX(), firstPiecePosition.getY());
                        }

                    } else {
                        // prohozeni sprite-u
                        switchPieces(new Point((int) (pSceneTouchEvent.getX() + xDiff), (int) (pSceneTouchEvent.getY() + yDiff)), pieceTouched, pieceHovered);

                        // ID dilku, ktery se nachazi na pozici prvniho doteku
                        int draggedPieceID = getCardIDFromActualPosition(pieceTouched);
                        // ID dilku, ktery se nachazi na pozici na kterou
                        // dojedeme
                        int secondPieceID = getCardIDFromActualPosition(pieceHovered);
                        // Prohozeni pozic dilku
                        if ((draggedPieceID != -1) && (secondPieceID != -1)) {
                            cards.get(draggedPieceID).setActualPositionInGrid(pieceHovered);
                            cards.get(secondPieceID).setActualPositionInGrid(pieceTouched);
                        } else {
                            Mint.logEvent("Switching fingers error: " + "Dragged: " + pieceTouched + " Hovered: " + pieceHovered + " draggedPieceID: " + draggedPieceID + " secondPieceId: " + secondPieceID);
                        }
                        getGameManager().setExtraInt2AndSave(++movesCounter);

                    }
                    animationCanceled = false;
                    distanceMovedWithFirstFinger = 0;
                    distanceMovedWithSecondFinger = 0;
                }
            }

        } else {
            if (fingersCounter == 2) {
                twoFingersDown = true;
                oneFingerDown = false;
            }
            if (fingersCounter >= 2) {
                PointerCoords pc = new PointerCoords();
                PointerProperties pp = new PointerProperties();
                pSceneTouchEvent.getMotionEvent().getPointerCoords(1, pc);
                pSceneTouchEvent.getMotionEvent().getPointerProperties(0, pp);

                if (pp.id == pSceneTouchEvent.getPointerID()) {
                    animateInPosition(pieceTouched, pSceneTouchEvent.getX(), pSceneTouchEvent.getY(), pc.x, (displaySize.y - pc.y));
                }
            }
        }
        if (fingersCounter == 0) {
            if (shadowSprite != null) {
                resetScaleAndZIndex(shadowSprite.getTag());
            }
            resetScaleAndZIndex(pieceHovered);
            resetScaleAndZIndex(pieceTouched);
            pieceHovered = -1;
            pieceTouched = -1;
            sfxManager.playSound(SoundsEnum.SOUND_LIFTCARD);
        }

    }

    /*
     * Obsluha pri zjisteni doteku prstu na obrazovce
     */
    private void onActionDown(TouchEvent pSceneTouchEvent) {
        fingersCounter = pSceneTouchEvent.getMotionEvent().getPointerCount();
        if (fingersCounter == 1) {
            fingersSwitched = false;
            pSceneTouchEvent.getMotionEvent().getPointerCoords(0, firstFingerCoordinates);
            firstFingerFirstTouch.setX((int) firstFingerCoordinates.x);
            firstFingerFirstTouch.setY((int) (displaySize.y - firstFingerCoordinates.y));

            initialFirstFingerPosition.setX((int) pSceneTouchEvent.getX());
            initialFirstFingerPosition.setY((int) pSceneTouchEvent.getY());

            pieceTouched = getPieceListPositionFromCoordinates(firstFingerFirstTouch.getX(), firstFingerFirstTouch.getY());
            if ((pieceTouched != -1) && (scene.getChildByTag(pieceTouched) != null)) {
                pieceTouchedCenter = new Position((int) scene.getChildByTag(pieceTouched).getX(), (int) scene.getChildByTag(pieceTouched).getY());
                xDiff = pieceTouchedCenter.getX() - firstFingerFirstTouch.getX();
                yDiff = pieceTouchedCenter.getY() - firstFingerFirstTouch.getY();

                if (shadowSprite == null) {
                    shadowSprite = new TaggedSprite(pieceTouchedCenter.getX() - 10, pieceTouchedCenter.getY() - 10, shadowTextureRegion, getVertexBufferObjectManager(), -2);
                    shadowSprite.setWidth(cardWidth * 1.5f);
                    shadowSprite.setHeight(cardWidth * 1.5f);
                    shadowSprite.setAlpha(0.5f);
                    scene.attachChild(shadowSprite);
                } else {
                    shadowSprite.setPosition(pieceTouchedCenter.getX() - 10, pieceTouchedCenter.getY() - 10);
                    shadowSprite.setVisible(true);
                }
            }
        } else if ((fingersCounter > 1) && (scene.getChildByTag(pieceTouched) != null)) {
            if (pieceTouched != -1) {
                if (!fingersSwitched) {
                    pSceneTouchEvent.getMotionEvent().getPointerCoords(1, secondFingerCoordinates);
                } else if (fingersSwitched) {
                    pSceneTouchEvent.getMotionEvent().getPointerCoords(0, secondFingerCoordinates);
                }

                secondFingerFirstTouch.setX((int) secondFingerCoordinates.x);
                secondFingerFirstTouch.setY((int) (displaySize.y - secondFingerCoordinates.y));

                initialSecondFingerPosition.setX((int) pSceneTouchEvent.getX());
                initialSecondFingerPosition.setY((int) pSceneTouchEvent.getY());

                initialAngle = getAngleBetweenTwoPoints(firstFingerFirstTouch, secondFingerFirstTouch);
                initialRotation = scene.getChildByTag(pieceTouched).getRotation();
            }
        }
        setTouchInformations();
    }

    /*
     * podle poctu dotykajicich se prstu nastavi vnitrni promenne a pokud jde o
     * dotek jednoho prstu, zvyrazni tazeny dilek
     */
    private void setTouchInformations() {
        if (fingersCounter == 1) {
            oneFingerDown = true;
            twoFingersDown = false;
            if (pieceTouched != -1) {
                scalePieceToSize(pieceTouched, 1.0f, 1.2f);
                if (scene.getChildByTag(pieceTouched) != null) {
                    scene.getChildByTag(pieceTouched).setZIndex(4);
                    scene.sortChildren();
                }
            }
        } else if ((fingersCounter == 2) && (pieceTouched != -1)) {
            oneFingerDown = false;
            twoFingersDown = true;
        } else {
            oneFingerDown = false;
            twoFingersDown = false;
        }
    }

    /*
     * Obsluha pri zjisteni pohybu prstem po obrazovce
     */
    private void onActionMove(TouchEvent pSceneTouchEvent) {
        fingersCounter = pSceneTouchEvent.getMotionEvent().getPointerCount();
        if (fingersCounter == 1) {
            if (((pieceTouched != -1) && (scene.getChildByTag(pieceTouched) != null))) {
                pSceneTouchEvent.getMotionEvent().getPointerCoords(0, firstFingerCoordinates);

                if (!waitForAnimation) {
                    firstFingerFirstTouch.setX((int) firstFingerCoordinates.x);
                    firstFingerFirstTouch.setY((int) (displaySize.y - firstFingerCoordinates.y));

                    scene.getChildByTag(pieceTouched).setPosition(firstFingerFirstTouch.getX() + xDiff, firstFingerFirstTouch.getY() + yDiff);
                    pieceHovered = getPieceListPositionFromCoordinates(scene.getChildByTag(pieceTouched).getX(), scene.getChildByTag(pieceTouched).getY());
                    scaleHoveredPiece(pieceHovered);

                    shadowSprite.setPosition(scene.getChildByTag(pieceTouched).getX() - 10, scene.getChildByTag(pieceTouched).getY() - 10);
                    shadowSprite.setZIndex(3);
                    scene.sortChildren();
                }
            }
        } else if (fingersCounter > 1) {
            if ((pieceTouched != -1) && (scene.getChildByTag(pieceTouched) != null)) {
                if (!fingersSwitched) {
                    pSceneTouchEvent.getMotionEvent().getPointerCoords(1, secondFingerCoordinates);
                    pSceneTouchEvent.getMotionEvent().getPointerCoords(0, firstFingerCoordinates);
                } else if (fingersSwitched) {
                    pSceneTouchEvent.getMotionEvent().getPointerCoords(0, secondFingerCoordinates);
                    pSceneTouchEvent.getMotionEvent().getPointerCoords(1, firstFingerCoordinates);
                }
                if (!waitForAnimation) {
                    firstFingerFirstTouch.setX((int) firstFingerCoordinates.x);
                    firstFingerFirstTouch.setY((int) (displaySize.y - firstFingerCoordinates.y));

                    secondFingerFirstTouch.setX((int) secondFingerCoordinates.x);
                    secondFingerFirstTouch.setY((int) (displaySize.y - secondFingerCoordinates.y));

                    currentAngle = getAngleBetweenTwoPoints(firstFingerFirstTouch, secondFingerFirstTouch);

                    pieceTouchedCenter = new Position((int) scene.getChildByTag(pieceTouched).getX(), (int) scene.getChildByTag(pieceTouched).getY());
                    scene.getChildByTag(pieceTouched).setPosition(pieceTouchedCenter.getX(), pieceTouchedCenter.getY());
                    shadowSprite.setPosition(pieceTouchedCenter.getX() - 10, pieceTouchedCenter.getY() - 10);

                    delta = initialAngle - currentAngle;
                    float currentRotation = scene.getChildByTag(pieceTouched).getRotation();
                    // vynulovani delty pro usek, kde se prechazi z 0 st. na 360
                    if (delta > 300) {
                        delta = 0;
                    }
                    scene.getChildByTag(pieceTouched).setRotation(((float) delta + currentRotation));
                    shadowSprite.setRotation(((float) delta + currentRotation));
                    currentRotation = scene.getChildByTag(pieceTouched).getRotation();
                    initialAngle = Double.valueOf(currentAngle);
                }
            }
        }
    }

    private void finishRotation(int pieceID) {
        if (scene.getChildByTag(pieceTouched) != null) {
            finalRotation = scene.getChildByTag(pieceID).getRotation();
            float nextClosestRotation = getClosestRotation(finalRotation, getRotationDirection());
            rotatePieceToAngle(pieceTouched, scene.getChildByTag(pieceID).getRotation(), nextClosestRotation);
            int cardRotation = (int) ((nextClosestRotation % 360) / 90);
            cards.get(getCardIDFromActualPosition(pieceTouched)).setRotation(cardRotation);
            initialRotation = scene.getChildByTag(pieceTouched).getRotation();
            pieceTouchedCenter = new Position((int) scene.getChildByTag(pieceTouched).getX(), (int) scene.getChildByTag(pieceTouched).getY());
            getGameManager().setExtraInt2AndSave(++movesCounter);
            sfxManager.playSound(SoundsEnum.SOUND_ROTATECARD);
        }
    }

    private float getClosestRotation(float rotation, int rotationDirection) {
        if (rotation < 0) {
            rotation += 360;
            scene.getChildByTag(pieceTouched).setRotation(scene.getChildByTag(pieceTouched).getRotation() + 360);
        }
        return (((int) (rotation / 90) + rotationDirection) * 90);
    }

    /*
     * Metoda vypocita smer rotace podle toho, jak se pohnou prsty polozene na
     * dilku v zavislosti na svem originalni pozici
     */
    private int getRotationDirection() {
        if ((finalRotation < 0) && (initialRotation >= 0)) {
            if (initialFirstFingerPosition.getX() > initialSecondFingerPosition.getX()) {
                if ((Math.abs(initialRotation - finalRotation) % 90) > MINIMUM_ROTATION_FOR_ANIMATION) {
                    return 1;
                } else {
                    return 0;
                }

            } else {
                if ((Math.abs(initialRotation - finalRotation) % 90) > MINIMUM_ROTATION_FOR_ANIMATION) {
                    return 0;
                } else {
                    return 1;
                }
            }
        } else if ((finalRotation >= 0) && (initialRotation < 0)) {
            if (initialFirstFingerPosition.getX() > initialSecondFingerPosition.getX()) {
                if ((Math.abs(initialRotation - finalRotation) % 90) > MINIMUM_ROTATION_FOR_ANIMATION) {
                    return 0;
                } else {
                    return 1;
                }
            } else {
                if ((Math.abs(initialRotation - finalRotation) % 90) > MINIMUM_ROTATION_FOR_ANIMATION) {
                    return 1;
                } else {
                    return 0;
                }
            }
        } else {
            if (finalRotation > initialRotation) {
                if ((Math.abs(initialRotation - finalRotation) % 90) > MINIMUM_ROTATION_FOR_ANIMATION) {
                    return 1;
                } else {
                    return 0;
                }
            } else if (initialRotation > finalRotation) {
                if ((Math.abs(initialRotation - finalRotation) % 90) > MINIMUM_ROTATION_FOR_ANIMATION) {
                    return 0;
                } else {
                    return 1;
                }
            }
        }
        return -1;
    }

    /*
     * Metoda se pouziva pro zruseni zvyrazneni dilku
     */
    private void resetScaleAndZIndex(int pieceID) {
        if (pieceID != -1) {
            scalePieceToSize(pieceID, 1.2f, 1.0f);
            if ((scene != null) && (scene.getChildByTag(pieceID) != null)) {
                scene.getChildByTag(pieceID).setZIndex(0);
                scene.sortChildren();
            }
        }
    }

    private double getAngleBetweenTwoPoints(Position firstPoint, Position secondPoint) {
        return ((Math.atan2(-(firstPoint.getY() - secondPoint.getY()), -(firstPoint.getX() - secondPoint.getX())) / Math.PI) * 180) + 180;
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    // nutne pro pripad, ze se hrac dotne obrayovky mimo dlazdice a prejede
    // prstem nad ne pro spravne pocitani doteku
    @Override
    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
        if (gameRunning) {
            switch (pSceneTouchEvent.getAction()) {
                case TouchEvent.ACTION_DOWN:
                    onActionDown(pSceneTouchEvent);
                    break;
                case TouchEvent.ACTION_MOVE:
                    onActionMove(pSceneTouchEvent);
                    break;
                case TouchEvent.ACTION_UP:
                    onActionUp(pSceneTouchEvent);
                    break;
                case TouchEvent.ACTION_CANCEL:
                    if (fingersCounter > 0) {
                        fingersCounter--;
                    }
                    if (fingersCounter == 1) {
                        oneFingerDown = true;
                        twoFingersDown = false;
                    } else {
                        oneFingerDown = false;
                        twoFingersDown = false;
                    }
                    break;
            }
        }

        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putParcelableArrayList("CARDS", cards);
        savedInstanceState.putInt("GRIDSIZE", gridSize);
        savedInstanceState.putInt("MOVECTR", movesCounter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        if (savedInstanceState != null) {
            gameLoadedFromPreviousState = true;
            cardsLoaded = savedInstanceState.getParcelableArrayList("CARDS");
            gridSize = savedInstanceState.getInt("GRIDSIZE");
            movesCounter = savedInstanceState.getInt("MOVECTR");
        }

    }

    @Override
    protected void initGame() {
        super.initGame();
        setEnvironment();
    }

    @Override
    public String getVictoryText(int progress) {
        if (progress == 3) {
            return getResources().getString(R.string.game_pronasledovani_victorytext_threestars);
        } else if (progress == 2) {
            return getResources().getString(R.string.game_pronasledovani_victorytext_twostars);
        } else if (progress == 1) {
            return getResources().getString(R.string.game_pronasledovani_victorytext_onestar);
        } else {
            return getResources().getString(R.string.game_pronasledovani_victorytext_gameover);
        }
    }

    @Override
    public String[] getVictorySpeech() {
        return new String[] { SpeechSounds.RESULT_PRONASLEDOVANI_0, SpeechSounds.RESULT_PRONASLEDOVANI_1, SpeechSounds.RESULT_PRONASLEDOVANI_2, SpeechSounds.RESULT_PRONASLEDOVANI_3 };
    }

    @Override
    public int countProgress() {
        switch (getDifficulty()) {
            case EASY:
                if (gameTime >= 300) {
                    return 0;
                } else if (gameTime >= 160) {
                    return 1;
                } else if (gameTime >= 80) {
                    return 2;
                } else {
                    return 3;
                }
            case MEDIUM:
                if (gameTime >= 600) {
                    return 0;
                } else if (gameTime >= 360) {
                    return 1;
                } else if (gameTime >= 160) {
                    return 2;
                } else {
                    return 3;
                }
            case HARD:
                if (gameTime >= 800) {
                    return 0;
                } else if (gameTime >= 460) {
                    return 1;
                } else if (gameTime >= 320) {
                    return 2;
                } else {
                    return 3;
                }
        }
        return 0;
    }

    @Override
    public CharSequence getStatsText() {
        final MyTime time = new MyTime((float) getGameManager().getCounter());
        TextView tvStats = (TextView) victoryLayout.findViewById(R.id.victoryscreen_result);
        // TADY MUSI BYT DVE MEZERY, jinak se string neappenduje, resp. nevim
        // jak
        SpannableStringBuilder ssb = new SpannableStringBuilder("  ");
        Bitmap clock = BitmapFactory.decodeResource(getResources(), R.drawable.victoryscreen_time);
        ssb.setSpan(new ImageSpan(this, clock), 0, 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        ssb.append(getResources().getString(R.string.victory_text_time));
        ssb.append(time.toString());
        tvStats.setText(ssb);

        SpannableStringBuilder ssb2 = new SpannableStringBuilder("  ");
        Bitmap stats = BitmapFactory.decodeResource(getResources(), R.drawable.victoryscreen_stats);
        ssb2.setSpan(new ImageSpan(this, stats), 0, 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        ssb2.append(getResources().getString(R.string.victory_text_movesCount));
        ssb2.append(Integer.toString(getGameManager().getExtraInt2()));
        return ssb.append(ssb2);

    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
        if (pieceTouched != -1) {
            float originalPieceRotation = cards.get(pieceTouched).getRotation() * 90;
            Position pos = getCardPositionInPixels(pieceTouched);

            scene.getChildByTag(pieceTouched).setPosition(pos.getX(), pos.getY());
            scene.getChildByTag(pieceTouched).setRotation(originalPieceRotation);
            resetScaleAndZIndex(pieceTouched);
            if (pieceHovered != -1) {
                resetScaleAndZIndex(pieceHovered);
            }

            shadowSprite.setPosition(pos.getX(), pos.getY());
            shadowSprite.setRotation(originalPieceRotation);
            if (shadowSprite != null) {
                resetScaleAndZIndex(shadowSprite.getTag());
            }
            shadowSprite.setVisible(false);

            pieceTouched = -1;
        }
        super.onDrawerSlide(drawerView, slideOffset);
    }

    private VehicleTypeEnum getVehicleTypeByMap(String mapName) {
        if (mapName.equals("mapa2") || mapName.equals("mapa13")) {
            return VehicleTypeEnum.BOAT;
        } else if (mapName.equals("mapa7")) {
            return VehicleTypeEnum.TRAIN;
        }
        return random.nextBoolean() ? VehicleTypeEnum.CAR : VehicleTypeEnum.MOTORCYCLE;
    }
}
