/**
 * 
 */

package cz.nic.tablexia.game.games.strelnice.model;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.RotationByModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;

/**
 * @author lhoracek
 */
public class Carousel extends Sprite {
    private Sprite visibleFlower;
    private int    rotation;

    public Carousel(float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX + ((pTextureRegion.getWidth() / 3) * 2), pY + ((pTextureRegion.getHeight() / 3) * 2), (pTextureRegion.getWidth() / 3) * 2, (pTextureRegion.getHeight() / 3) * 2, pTextureRegion, pVertexBufferObjectManager);
    }

    public void nextFlower(Sprite flower) {
        final Sprite oldFlower = visibleFlower;
        visibleFlower = flower;
        attachChild(flower);
        flower.setRotation((360 - rotation) + (250 % 360));
        flower.setX((getWidth() / 10) * ((rotation == 180) || (rotation == 270) ? 3 : 7));
        flower.setY((getHeight() / 10) * ((rotation == 0) || (rotation == 270) ? 3 : 7));
        flower.setScale(0.6f);

        rotation = (rotation + 90) % 360;

        registerEntityModifier(new RotationByModifier(0.3f, 90, new IEntityModifierListener() {
            @Override
            public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                // nothing
                // TODO sound delegate
                // mfxManager.playSound(SoundType.SWOOSH_IN);
            }

            @Override
            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                // TODO remove sprite (detach)
                if (oldFlower != null) {
                    oldFlower.setAlpha(0);
                }
            }
        }));
    }
}
