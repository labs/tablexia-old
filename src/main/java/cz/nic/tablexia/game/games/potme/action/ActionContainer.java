/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.potme.action;

import org.andengine.entity.Entity;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.align.HorizontalAlign;
import org.andengine.util.modifier.ease.EaseBackOut;

import cz.nic.tablexia.game.games.potme.PotmeActivity;
import cz.nic.tablexia.game.games.potme.ResourceManager;
import cz.nic.tablexia.game.games.potme.action.widget.ActionsStripWidget;

/**
 * Action container with action number
 * 
 * @author Matyáš Latner
 *
 */
public class ActionContainer extends Entity {
	
	private Text 			actionNumberText;
	private Action 			action;
	private Rectangle 		reactionRectangle;
	private float			permanentPositionY;
	private float 			startPositionY;
	private MoveModifier 	actualActionContainerModifier;
	private MoveModifier 	reactionRectangleMoveModifier;

	public ActionContainer(float pX, Action action, float startPositionY, VertexBufferObjectManager vertexBufferObjectManager) {
		super(pX, ActionContainer.getPositionYForActionPosition(startPositionY, action.getOrderNumber()));
		this.startPositionY = startPositionY;
		this.actionNumberText = new Text(PotmeActivity.ACTION_SIZE_BIGGER / 2 + 2 * ActionsStripWidget.ACTION_OFFSET,
										PotmeActivity.ACTION_SIZE_BIGGER / 2 - ActionsStripWidget.ACTION_OFFSET,
										ResourceManager.getInstance().getFont(),
										String.valueOf(action.getOrderNumber() + 1),
										300,
										new TextOptions(HorizontalAlign.CENTER),
										vertexBufferObjectManager);
		this.action = action;
		attachChild(action);
		attachChild(actionNumberText);
		
		permanentPositionY = getY();
		
		prepareReactionRectangle(vertexBufferObjectManager);
	}
	
	
	
/* //////////////////////////////////////////// ACTION */
	
	public Action getAction() {
		return action;
	}
	
	public void changeActionNumber(int actionNumber) {
		action.setOrderNumber(actionNumber);
		actionNumberText.setText(String.valueOf(actionNumber + 1));
	}
	
	
	
/* //////////////////////////////////////////// REACTION RECTANGLE */
	
	private void prepareReactionRectangle(VertexBufferObjectManager vertexBufferObjectManager) {
		reactionRectangle = new Rectangle(0, 0, action.getWidth(), ActionsStripWidget.ACTION_OFFSET, vertexBufferObjectManager);
		reactionRectangle.setVisible(false);
		resetReactionRectangle(false);
		attachChild(reactionRectangle);
	}
	
	public Rectangle getReactionRectangle() {
		return reactionRectangle;
	}
	
	public void resetReactionRectangle(boolean animated) {
		moveReactionReactangle(-((action.getHeight() / 2) + (ActionsStripWidget.ACTION_OFFSET / 2)), animated);
	}
	
	public void setReactionRectangleOffset() {
		moveReactionReactangle(-(action.getHeight() + (ActionsStripWidget.ACTION_OFFSET / 2)), true);
	}
	
	private void moveReactionReactangle(float positionY, boolean animated) {
		if (reactionRectangleMoveModifier != null) {
			reactionRectangle.unregisterEntityModifier(reactionRectangleMoveModifier);
			reactionRectangleMoveModifier = null;
		}
		if (animated) {			
			reactionRectangleMoveModifier = new MoveModifier(PotmeActivity.ANIMATION_MOVE_PERIOD,
					0,
					reactionRectangle.getY(),
					0,
					positionY,
					EaseBackOut.getInstance());
			reactionRectangleMoveModifier.setAutoUnregisterWhenFinished(true);
			reactionRectangle.registerEntityModifier(reactionRectangleMoveModifier);
		} else {
			reactionRectangle.setPosition(0, positionY);
		}
	}
	
	
	
/* //////////////////////////////////////////// MOVEMENT */
	
	public void resetPosition(boolean animated) {
		moveToYPosition(permanentPositionY, animated, true);
	}
	
	public void moveTemporalyHalfUp(boolean animated) {
		moveToYPosition(permanentPositionY + (action.getActualSize() / 2) + ActionsStripWidget.ACTION_OFFSET, animated, false);
	}
	
	public void moveTemporalyHalfDown(boolean animated) {
		moveToYPosition(permanentPositionY - (action.getActualSize() / 2) - ActionsStripWidget.ACTION_OFFSET, animated, false);
	}
	
	public void movePermanentlyUp(boolean animated) {
		moveToActionPositionOffset(1, animated, true);
		changeActionNumber(action.getOrderNumber() - 1);
	}
	
	public void movePermanentlyDown(boolean animated) {
		moveToActionPositionOffset(-1, animated, true);
		changeActionNumber(action.getOrderNumber() - 1);
	}
	
	public void moveToActionPositionOffset(int actionPosition, boolean animated, boolean permament) {
		moveToYPosition(permanentPositionY + (actionPosition * action.getActualSize()) + (Math.signum(actionPosition) * ActionsStripWidget.ACTION_OFFSET), animated, permament);
	}
	
	public void moveToActionPosition(boolean animated, boolean permament) {
		moveToYPosition(ActionContainer.getPositionYForActionPosition(startPositionY, action.getOrderNumber()), animated, permament);
	}
	
	public void moveToYPosition(float positionY, boolean animated, boolean permanent) {
		if (actualActionContainerModifier != null) {
			unregisterEntityModifier(actualActionContainerModifier);
			actualActionContainerModifier = null;
		}
		if (animated) {
			actualActionContainerModifier = new MoveModifier(PotmeActivity.ANIMATION_MOVE_PERIOD,
					   getX(),
					   getY(),
					   getX(),
					   positionY,
					   EaseBackOut.getInstance());
			actualActionContainerModifier.setAutoUnregisterWhenFinished(true);
			registerEntityModifier(actualActionContainerModifier);
		} else {
			setPosition(getX(), positionY);
		}
		if (permanent) {
			permanentPositionY = positionY;
		}
	}
	
	private static float getPositionYForActionPosition(float startPositionY, int actionPosition) {
		return ActionsStripWidget.ACTION_OFFSET + (PotmeActivity.ACTION_SIZE_BIGGER / 2) + (actionPosition * (PotmeActivity.ACTION_SIZE_BIGGER + ActionsStripWidget.ACTION_OFFSET)) - startPositionY;
	}
	
}
