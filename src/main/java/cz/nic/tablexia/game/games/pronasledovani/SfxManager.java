/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.pronasledovani;

import java.util.HashMap;
import java.util.Map;

import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.audio.sound.SoundManager;

import android.content.Context;
import android.util.Log;
import cz.nic.tablexia.Tablexia;

public class SfxManager {
    private static final String    TAG              = SfxManager.class.getSimpleName();
    private static final String    SOUNDS_DIR       = PronasledovaniActivity.BASE_DIR + "sfx/";
    public static final String     SOUNDS_EXTENSION = ".mp3";

    private Map<SoundsEnum, Sound> sounds           = new HashMap<SoundsEnum, Sound>();
    private SoundManager           soundManager;
    private Context                context;

    public SfxManager(SoundManager soundManager, Context context) {
        this.soundManager = soundManager;
        this.context = context;
        SoundFactory.setAssetBasePath(SOUNDS_DIR);
    }

    public void loadSounds() {
        for (SoundsEnum sound : SoundsEnum.values()) {
            loadSound(sound);
        }
    }

    private void loadSound(SoundsEnum sound) {

        Tablexia tablexia = (Tablexia) context.getApplicationContext();
        String path = SOUNDS_DIR + sound.getResourceName() + SOUNDS_EXTENSION;
        Log.i(TAG, "Loading sound: " + path);
        Sound soundInstance = SoundFactory.createSoundFromAssetFileDescriptor(soundManager, tablexia.getZipResourceFile().getAssetFileDescriptor(path));
        sounds.put(sound, soundInstance);

    }

    public void playSound(SoundsEnum sound) {
        Sound s = sounds.get(sound);
        s.setVolume(sound.getVolume());
        s.play();
    }

}
