/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.unos;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.input.touch.TouchEvent;

import android.view.MotionEvent;

/**
 * @author lhoracek
 */
public class DragSceneListener implements IOnSceneTouchListener {
    private Camera camera;

    public DragSceneListener(Camera camera) {
        super();
        this.camera = camera;
    }

    private float mTouchX, mTouchY;

    @Override
    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pTouchEvent) {

        if (pTouchEvent.getAction() == MotionEvent.ACTION_DOWN) {
            mTouchX = pTouchEvent.getMotionEvent().getX();
            mTouchY = pTouchEvent.getMotionEvent().getY();
        } else if (pTouchEvent.getAction() == MotionEvent.ACTION_MOVE) {
            float newX = pTouchEvent.getMotionEvent().getX();
            float newY = pTouchEvent.getMotionEvent().getY();

            float mTouchOffsetX = (newX - mTouchX);
            float mTouchOffsetY = (newY - mTouchY);

            float newScrollX = camera.getCenterX() - mTouchOffsetX;
            float newScrollY = camera.getCenterY() + mTouchOffsetY;

            camera.setCenter(newScrollX, newScrollY);

            mTouchX = newX;
            mTouchY = newY;
        }
        return true;
    }
}
