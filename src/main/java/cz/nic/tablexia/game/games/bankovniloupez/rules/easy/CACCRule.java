/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.rules.easy;

import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.Context;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureFactory;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute.AttributeColor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.ClothingAttribute;

/**
 * 
 * @author Matyáš Latner
 */
public class CACCRule extends CCCCRule {

    public CACCRule(Context context, RandomAccess randomAccess, VertexBufferObjectManager vertexBufferObject, int numberOfCreatures, int numberOfThieves) {
        super(context, randomAccess, vertexBufferObject, numberOfCreatures, numberOfThieves);
    }
    
    @Override
    public String[] prepareRuleMessageParameters() {
        AttributeDescription t0Description0 = getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0);
        AttributeDescription t0Description1 = getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(1);
		return new String[] {
        		getAttributeColorName(t0Description1),
                getAttributeColorName(t0Description0),
                getAttributeName(t0Description0, false),
        };
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
    	CreatureDescriptor creatureDescriptor = new CreatureDescriptor();
    	CreatureDescriptor randomCreatureDescrition = CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, vertexBufferObject, getRandomAccess()).getCreatureDescrition();    	
        
        int randomDescriptionPosition1 = getRandomAccess().getRandom().nextInt(randomCreatureDescrition.getDescriptions().size());
        AttributeColor attributeColor = randomCreatureDescrition.getDescriptions().get(randomDescriptionPosition1).getAttributeColor();
        
        for (int i = 0; i < GENERATOR_TRY_COUNT; i++) {
        	AttributeColor randomColor = getRandomColorFromGeneratedCreature(BAN_ATTRIBUTES_SET_FOR_GENERATING, vertexBufferObject);
        	if (attributeColor != randomColor) {
        		creatureDescriptor.addDescription(randomCreatureDescrition.getDescriptions().get(randomDescriptionPosition1));
        		creatureDescriptor.addDescription(new AttributeDescription(randomColor, null, ClothingAttribute.class));
        		addGlobalCreatureDescriptor(T0_OFFSET, creatureDescriptor);
        		return;
        	}
		}
        
        throw new IllegalStateException("Cannot select different color for creature description!");
    }

}
