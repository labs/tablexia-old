/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.unos.model;

import org.andengine.entity.sprite.Sprite;

import cz.nic.tablexia.game.games.unos.entity.DirectionSprite;

/**
 * @author lhoracek
 */
public class TileGroup {

    Sprite tileSprite;
    Sprite flag;

    Sprite roadNorhWest, roadNorhEast;
    DirectionSprite nextNorhWestIn, nextNorhEastIn, nextNorhWestOut, nextNorhEastOut;
    DirectionSprite nextNorhWestInPressed, nextNorhEastInPressed, nextNorhWestOutPressed, nextNorhEastOutPressed;

    public Sprite getTileSprite() {
        return tileSprite;
    }

    public void setTileSprite(Sprite tileSprite) {
        this.tileSprite = tileSprite;
    }

    public Sprite getFlag() {
        return flag;
    }

    public void setFlag(Sprite flag) {
        this.flag = flag;
    }

    public Sprite getRoadNorhWest() {
        return roadNorhWest;
    }

    public void setRoadNorhWest(Sprite roadNorhWest) {
        this.roadNorhWest = roadNorhWest;
    }

    public Sprite getRoadNorhEast() {
        return roadNorhEast;
    }

    public void setRoadNorhEast(Sprite roadNorhEast) {
        this.roadNorhEast = roadNorhEast;
    }

    public DirectionSprite getNextNorhWestIn() {
        return nextNorhWestIn;
    }

    public void setNextNorhWestIn(DirectionSprite nextNorhWestIn) {
        this.nextNorhWestIn = nextNorhWestIn;
    }

    public DirectionSprite getNextNorhEastIn() {
        return nextNorhEastIn;
    }

    public void setNextNorhEastIn(DirectionSprite nextNorhEastIn) {
        this.nextNorhEastIn = nextNorhEastIn;
    }

    public DirectionSprite getNextNorhWestOut() {
        return nextNorhWestOut;
    }

    public void setNextNorhWestOut(DirectionSprite nextNorhWestOut) {
        this.nextNorhWestOut = nextNorhWestOut;
    }

    public DirectionSprite getNextNorhEastOut() {
        return nextNorhEastOut;
    }

    public void setNextNorhEastOut(DirectionSprite nextNorhEastOut) {
        this.nextNorhEastOut = nextNorhEastOut;
    }

    public DirectionSprite getNextNorhWestInPressed() {
        return nextNorhWestInPressed;
    }

    public void setNextNorhWestInPressed(DirectionSprite nextNorhWestInPressed) {
        this.nextNorhWestInPressed = nextNorhWestInPressed;
    }

    public DirectionSprite getNextNorhEastInPressed() {
        return nextNorhEastInPressed;
    }

    public void setNextNorhEastInPressed(DirectionSprite nextNorhEastInPressed) {
        this.nextNorhEastInPressed = nextNorhEastInPressed;
    }

    public DirectionSprite getNextNorhWestOutPressed() {
        return nextNorhWestOutPressed;
    }

    public void setNextNorhWestOutPressed(DirectionSprite nextNorhWestOutPressed) {
        this.nextNorhWestOutPressed = nextNorhWestOutPressed;
    }

    public DirectionSprite getNextNorhEastOutPressed() {
        return nextNorhEastOutPressed;
    }

    public void setNextNorhEastOutPressed(DirectionSprite nextNorhEastOutPressed) {
        this.nextNorhEastOutPressed = nextNorhEastOutPressed;
    }

}
