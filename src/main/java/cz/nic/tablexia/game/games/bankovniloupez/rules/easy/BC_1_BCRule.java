/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.rules.easy;

import java.util.ArrayList;
import java.util.List;

import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.Context;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureFactory;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureRoot;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute.AttributeColor;
import cz.nic.tablexia.game.games.bankovniloupez.rules.GameRuleUtility;

/**
 * 
 * @author Matyáš Latner
 */
public class BC_1_BCRule extends GameRuleUtility {
    
	private   static final int 	 	GROUP_SIZE	= 3;
	protected static final Integer 	T0_OFFSET 	= CreatureDescriptor.THIEF_OFFSET;
	protected 			   Integer 	T1_OFFSET 	= Integer.valueOf(2);
	private AttributeDescription 	commonAttribute;

    public BC_1_BCRule(Context context, RandomAccess randomAccess, VertexBufferObjectManager vertexBufferObject, int numberOfCreatures, int numberOfThieves) {
        super(context, randomAccess, vertexBufferObject, numberOfCreatures, numberOfThieves, GROUP_SIZE);
    }
    
    public BC_1_BCRule(Context context, RandomAccess randomAccess, VertexBufferObjectManager vertexBufferObject, int numberOfCreatures, int numberOfThieves, int groupSize) {
        super(context, randomAccess, vertexBufferObject, numberOfCreatures, numberOfThieves, groupSize);
    }
    
    @Override
    public String[] prepareRuleMessageParameters() {
        return new String[] {
                getAttributeName(getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0), true)
        };
    }
    
    @Override
    protected boolean checkSpecialCreaturePosition(int position, Integer creatureOffset) {
    	if (creatureOffset == T1_OFFSET) {
    		CreatureDescriptor creatureToCheck = specialCreatures.get(position - T1_OFFSET);
    		if (creatureToCheck != null && creatureToCheck.isThief() && creatureToCheck.getDescriptions().get(0).equals(getGlobalCreatureDescriptor(creatureOffset).getDescriptions().get(0))) {
    			return false;
    		}
    	}
    	
    	if (creatureOffset == T0_OFFSET) {
    		CreatureDescriptor creatureToCheck = specialCreatures.get(position + T1_OFFSET);
    		if (creatureToCheck != null && !creatureToCheck.isThief() && creatureToCheck.getDescriptions().get(0).equals(getGlobalCreatureDescriptor(creatureOffset).getDescriptions().get(0))) {
    			return false;
    		}
    	}
    	
    	return true;
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
    	commonAttribute = getRandomAttributeDescription(CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, vertexBufferObject, getRandomAccess()).getCreatureDescrition());
    	
        addGlobalCreatureDescriptor(T1_OFFSET, new CreatureDescriptor());
        addGlobalCreatureDescriptor(T0_OFFSET, new CreatureDescriptor());
    }
    
    @Override
    protected void prepareCreatureDescriptionsB() {
    	AttributeColor commonColor = Attribute.getRandomAvalibleColorFromAttributeClass(commonAttribute.getAttributeClass(), getRandomAccess());
    	
    	getGlobalCreatureDescriptor(T0_OFFSET).clearDescriptions().addDescription(new AttributeDescription(commonColor, null, commonAttribute.getAttributeClass()));
    	getGlobalCreatureDescriptor(T1_OFFSET).clearDescriptions().addDescription(new AttributeDescription(commonColor, null, commonAttribute.getAttributeClass()));
    }
    
    @Override
    protected List<CreatureRoot> prepareCreatureDescriptionsA() {
    	List<CreatureRoot> creatures = new ArrayList<CreatureRoot>();
        for (int i = 0; i < numberOfCreatures; i++) {
        	
            CreatureDescriptor creatureDescriptor = specialCreatures.get(i);
            if (creatureDescriptor != null) { // add special creature
            	creatures.add(CreatureFactory.getInstance().generateCreature(creatureDescriptor, null, vertexBufferObject, getRandomAccess()));
            } else {
            	CreatureDescriptor creatureDescriptorToBan = new CreatureDescriptor();
            	creatureDescriptorToBan.disableGenderCompatibilityCheck();
            	
            	CreatureDescriptor nextSpecialCreature = specialCreatures.get(i + T1_OFFSET);
				if (nextSpecialCreature != null) {
            		creatureDescriptorToBan.addDescription(nextSpecialCreature.getDescriptions().get(0));            		
            	}
				
				int lastPosition = i - T1_OFFSET;
				if (lastPosition >= 0) {					
					CreatureRoot lastCreature = creatures.get(lastPosition);
					if (lastCreature != null) {
						Attribute attributeToBan = lastCreature.getAttributeForType(AttributeDescription.getGenericType(commonAttribute.getAttributeClass()));
						if (attributeToBan != null) {							
							creatureDescriptorToBan.addDescription(attributeToBan.getAttributeDescription());
						}
					}
				}
				
				creatures.add(CreatureFactory.getInstance().generateCreature(null, creatureDescriptorToBan, vertexBufferObject, getRandomAccess()));
            }
        }

        return creatures;
    }

}
