/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.unos.generator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.activeandroid.util.Log;

import cz.nic.tablexia.game.games.unos.model.PathPosition;
import cz.nic.tablexia.game.games.unos.model.Position;
import cz.nic.tablexia.game.games.unos.model.Tile;

/**
 * Generator cesty městem
 * 
 * @author lhoracek
 */
public class PathGenerator {
    private static final String TAG = PathGenerator.class.getSimpleName();

    public enum Direction {
        NW, NE, SE, SW;
        public static Direction getOppositeDirection(Direction direction) {
            if (direction == null) {
                return null;
            }
            switch (direction) {
                case NE:
                    return Direction.SW;
                case NW:
                    return Direction.SE;
                case SE:
                    return Direction.NW;
                case SW:
                    return Direction.NE;
                default:
                    throw new IllegalArgumentException("Wrong direction argument");
            }
        }
    }

    public List<PathPosition> generatePath(Position startingPosition, int pathLength, Map<Position, Tile> map) {
        List<PathPosition> path = new ArrayList<PathPosition>();
        path.add(new PathPosition(startingPosition, true));
        Position lastPosition = startingPosition;
        Direction lastDirection = null;
        for (int i = 0; i < pathLength; i++) {
            Direction newDirection = getRandomDirection(Direction.getOppositeDirection(lastDirection));
            Position newPosition = getPositionInDirection(newDirection, lastPosition);
            path.add(new PathPosition(newPosition, true));
            Log.d(TAG, "Next step " + newDirection + " " + newPosition);

            // TODO pridat kontrolu na velke budovy, a spravne krizovatky - nedelat body na tecku

            // kdyz ve vygenerovanem smeru je null, koukneme jetli na SE a S je dlazdece,
            // pokud ano, tak je tu krizovatka a muzeme to rozjet, jinak musime generovat cestu dal
            // do mapy pridame bod, ale dame false, protoze nebude zastavkou

            // kdyz uz jsme na dalsi krizovatce jednou jeli? zatim ponechame

            lastDirection = newDirection;
            lastPosition = newPosition;
        }

        return path;
    }

    Random random = new Random();

    private Direction getRandomDirection(Direction fromDirection) {
        Direction[] allDirections = Direction.values();
        if (fromDirection == null) {
            return allDirections[random.nextInt(4)];
        }
        int lastDirectionIndex = Arrays.asList(allDirections).indexOf(fromDirection);
        Direction[] directions = new Direction[] { allDirections[(lastDirectionIndex + 1) % 4], allDirections[(lastDirectionIndex + 2) % 4], allDirections[(lastDirectionIndex + 3) % 4] };

        // nastaveni pravdepodobnosti jednotlivých směrů - 20% - 40% - 40%
        float randomIndex = random.nextFloat();
        if (randomIndex < 0.2f) {
            return directions[0];
        } else if (randomIndex < 0.6f) {
            return directions[1];
        } else {
            return directions[2];
        }
    }

    private Position getPositionInDirection(Direction direction, Position currentPosition) {
        switch (direction) {
            case NE:
                return currentPosition.getNorthEast();
            case NW:
                return currentPosition.getNorthWest();
            case SE:
                return currentPosition.getSouthEast();
            case SW:
                return currentPosition.getSouthWest();
            default:
                throw new IllegalArgumentException("Wrong direction argument");
        }
    }

}
