/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.rules.hard;

import java.util.ArrayList;
import java.util.List;

import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.Context;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureFactory;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureRoot;
import cz.nic.tablexia.game.games.bankovniloupez.rules.GameRuleUtility;

/**
 * 
 * @author Matyáš Latner
 */
public class CC_0_notCC_0_TRule extends GameRuleUtility {
	
	private   static final int 	 	GROUP_SIZE	= 3;
	protected static final Integer 	T0_OFFSET 	= CreatureDescriptor.THIEF_OFFSET;
	protected static final Integer 	T1_OFFSET 	= Integer.valueOf(1);
	protected static final Integer 	T2_OFFSET 	= Integer.valueOf(2);
	
	CreatureDescriptor t1CreatureDescriptorToBan = new CreatureDescriptor();

    public CC_0_notCC_0_TRule(Context context, RandomAccess randomAccess, VertexBufferObjectManager vertexBufferObject, int numberOfCreatures, int numberOfThieves) {
        super(context, randomAccess, vertexBufferObject, numberOfCreatures, numberOfThieves, GROUP_SIZE);
    }
    
    @Override
    public String[] prepareRuleMessageParameters() {
        return new String[] {
        		getAttributeColorName(getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0)),
        		getAttributeName(getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0), false),
        		getAttributeColorName(t1CreatureDescriptorToBan.getDescriptions().get(0)),
        		getAttributeName(t1CreatureDescriptorToBan.getDescriptions().get(0), false)
        };
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
    	CreatureDescriptor source = getRandomCreatureDescriptionWithNAttributes(CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, vertexBufferObject, getRandomAccess()).getCreatureDescrition(), 2);
    	
    	CreatureDescriptor t2CreatureDescriptor = new CreatureDescriptor();
    	t2CreatureDescriptor.addDescription(source.getDescriptions().get(0));
        addGlobalCreatureDescriptor(T2_OFFSET, t2CreatureDescriptor);
        
        addGlobalCreatureDescriptor(T1_OFFSET, new CreatureDescriptor());
        
        addGlobalCreatureDescriptor(T0_OFFSET, new CreatureDescriptor());
        
        
        AttributeDescription t1AttributeDescription = source.getDescriptions().get(1);//getRandomAttributeDescription(CreatureFactory.getInstance().generateCreature(null, t1BannedAttributesForGenerator, vertexBufferObject, getRandomAccess()).getCreatureDescrition());
    	t1CreatureDescriptorToBan = new CreatureDescriptor();
    	t1CreatureDescriptorToBan.addDescription(t1AttributeDescription);
    }
    
    @Override
    protected List<CreatureRoot> prepareCreatureDescriptionsA() {
    	
    	AttributeDescription t2AttributeDescription = getGlobalCreatureDescriptor(T2_OFFSET).getDescriptions().get(0);
    	AttributeDescription t1AttributeDescriptionToBan = t1CreatureDescriptorToBan.getDescriptions().get(0);
    	
    	List<CreatureRoot> creatures = new ArrayList<CreatureRoot>();
        for (int i = 0; i < numberOfCreatures; i++) {
        	
        	CreatureRoot lastCreature = null;
        	int lastPosition = i - T1_OFFSET;
			if (lastPosition >= 0) {					
				lastCreature = creatures.get(lastPosition);
			}
			
			CreatureDescriptor nextSpecialCreature = specialCreatures.get(i + T1_OFFSET);
			
			CreatureDescriptor creatureDescriptorToBan = new CreatureDescriptor();
        	creatureDescriptorToBan.disableGenderCompatibilityCheck();
        	
            CreatureDescriptor creatureDescriptorToForce = specialCreatures.get(i);
			if (creatureDescriptorToForce != null) { // add special creature
				
				// Cannot create same condition for T2 ---> T2 and T1 attributes can be incompatible
				if (creatureDescriptorToForce.containsCreatureOffset(T0_OFFSET) && !creatureDescriptorToForce.containsCreatureOffset(T1_OFFSET) && lastCreature.getCreatureDescrition().containsAttributeDescription(t2AttributeDescription)) {
					creatureDescriptorToForce.addDescription(t1AttributeDescriptionToBan);
				}
				
            	if (creatureDescriptorToForce.containsCreatureOffset(T1_OFFSET)) {
                	creatureDescriptorToBan.addDescription(t1AttributeDescriptionToBan);
            	}
            	
            	creatures.add(CreatureFactory.getInstance().generateCreature(creatureDescriptorToForce, creatureDescriptorToBan, vertexBufferObject, getRandomAccess()));
            } else {
            	
            	boolean hasNotBanT2Attribute = true;
            	boolean nextCreatureIsSpecial = true;
            	
            	creatureDescriptorToForce = new CreatureDescriptor();
            	
				if (lastCreature != null && lastCreature.hasAttribute(t2AttributeDescription)) {
					creatureDescriptorToForce.addDescription(t1AttributeDescriptionToBan);
					hasNotBanT2Attribute = false;
				}
				
				// next special creature is T2 -> T2 will can be generated without T1 attribute 
				if (nextSpecialCreature != null) {
					creatureDescriptorToBan.addDescription(t2AttributeDescription);
					nextCreatureIsSpecial = false;
				}
				
				if (nextCreatureIsSpecial && hasNotBanT2Attribute) {
					newBaitCreatureDescription(getGlobalCreatureDescriptor(T2_OFFSET));						
					creatureDescriptorToForce.addDescription(getBaitCreatureDescriptionAttributeAtPosition(0));
				}
				
				creatures.add(CreatureFactory.getInstance().generateCreature(creatureDescriptorToForce, creatureDescriptorToBan, vertexBufferObject, getRandomAccess()));				
            }
        }

        return creatures;
    }

}
