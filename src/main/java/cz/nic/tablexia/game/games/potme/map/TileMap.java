/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.potme.map;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.potme.map.mapobject.MapObject;
import cz.nic.tablexia.game.games.potme.map.mapobstacle.MapObstacle;
import cz.nic.tablexia.game.games.potme.map.tile.Tile;

/**
 * Tile map for storing maps in game "Potmě"
 * 
 * @author Matyáš Latner
 *
 */
public class TileMap {
	
	public static enum TileMapQuartals {
		
		QUARTAL1,
		QUARTAL2,
		QUARTAL3,
		QUARTAL4;
		
		public static TileMapQuartals getRandomTileMapQuartal(RandomAccess randomAccess) {
			return TileMapQuartals.values()[randomAccess.getRandom().nextInt(TileMapQuartals.values().length)];
		}
	}
	
	public static class TileMapPosition {
		
		private int positionX;
		private int positionY;
		
		public TileMapPosition(int positionX, int positionY) {
			this.positionX = positionX;
			this.positionY = positionY;
		}
		
		public int getPositionX() {
			return positionX;
		}
		
		public int getPositionY() {
			return positionY;
		}
		
		@Override
		public boolean equals(Object object) {
			if (object == null) {
				return false;
			}
			if (!(object instanceof TileMapPosition)) {
				return false;
			}
			TileMapPosition tileMapPosition = (TileMapPosition) object;
			return tileMapPosition.positionX == this.positionX && tileMapPosition.positionY == this.positionY;
		}
		
		@Override
		public String toString() {
			return "TileMapPosition[" + positionX + ", " + positionY + "]";
		}
	}
	
	private Tile[][] 		map;
	private int 			mapXSize;
	private int 			mapYSize;
	private TileMapPosition mapStartPosition;
	private TileMapPosition mapFinishPosition;
	
	public TileMap(int mapXSize, int mapYSize) {
		this.mapXSize = mapXSize;
		this.mapYSize = mapYSize;
		
		map = new Tile[mapXSize][mapYSize];
	}
	
	public int getMapXSize() {
		return mapXSize;
	}
	
	public int getMapYSize() {
		return mapYSize;
	}
	
	public void setMapStartPosition(TileMapPosition mapStartPosition) {
		this.mapStartPosition = mapStartPosition;
	}
	
	public TileMapPosition getMapStartPosition() {
		return mapStartPosition;
	}
	
	public TileMapPosition getMapFinishPosition() {
		return mapFinishPosition;
	}
	
	public void setMapFinishPosition(TileMapPosition mapFinishPosition) {
		this.mapFinishPosition = mapFinishPosition;
	}
	
	public boolean addTileAtPosition(TileMapPosition tileMapPosition, Tile tile) {
		return addTileAtPosition(tileMapPosition.getPositionX(), tileMapPosition.getPositionY(), tile);
	}
	
	public boolean addTileAtPosition(int positionX, int positionY, Tile tile) {
		if (checkPositionInMap(positionX, positionY)) {
			map[positionX][positionY] = tile;
			if (tile != null) {
				
				tile.setMapPositionX(positionX);
				tile.setMapPositionY(positionY);
				
				if (tile.getTileType().isTopDoor()) {
					int topPosition = positionY - 1;
					if (topPosition >= 0) {
						Tile topTile = getTileAtPosition(positionX, topPosition);
						if (topTile != null && topTile.getTileType().isBottomDoor()) {
							topTile.setBottomNeighbor(tile);
							tile.setTopNeighbor(topTile);
						}
					}
				}
				if (tile.getTileType().isRightDoor()) {
					int rightPosition = positionX + 1;
					if (rightPosition < getMapXSize()) {
						Tile rightTile = getTileAtPosition(rightPosition, positionY);
						if (rightTile != null && rightTile.getTileType().isLeftDoor()) {
							rightTile.setLeftNeighbor(tile);
							tile.setRightNeighbor(rightTile);
						}
					}
				}
				if (tile.getTileType().isBottomDoor()) {
					int bottomPosition = positionY + 1;
					if (bottomPosition < getMapYSize()) {
						Tile bottomTile = getTileAtPosition(positionX, bottomPosition);
						if (bottomTile != null && bottomTile.getTileType().isTopDoor()) {
							bottomTile.setTopNeighbor(tile);
							tile.setBottomNeighbor(bottomTile);
						}
					}
				}
				if (tile.getTileType().isLeftDoor()) {
					int leftPosition = positionX - 1;
					if (leftPosition >= 0) {
						Tile leftTile = getTileAtPosition(leftPosition, positionY);
						if (leftTile != null && leftTile.getTileType().isRightDoor()) {
							leftTile.setRightNeighbor(tile);
							tile.setLeftNeighbor(leftTile);
						}
					}
				}
			}
			return true;
		} else {
			return false;
		}
	}
	
	public void removeTileAtPosition(int positionX, int positionY) {
		if (checkPositionInMap(positionX, positionY)) {
			Tile tile = map[positionX][positionY];
			if (tile != null) {
				Tile topNeighbor = tile.getTopNeighbor();
				if (topNeighbor != null) {
					tile.setTopNeighbor(null);
					topNeighbor.setBottomNeighbor(null);
				}
				Tile rightNeighbor = tile.getRightNeighbor();
				if (rightNeighbor != null) {
					tile.setRightNeighbor(null);
					rightNeighbor.setLeftNeighbor(null);
				}
				Tile bottomNeighbor = tile.getBottomNeighbor();
				if (bottomNeighbor != null) {
					tile.setBottomNeighbor(null);
					bottomNeighbor.setTopNeighbor(null);
				}
				Tile leftNeighbor = tile.getLeftNeighbor();
				if (leftNeighbor != null) {
					tile.setLeftNeighbor(null);
					leftNeighbor.setRightNeighbor(null);
				}
			}
			
			map[positionX][positionY] = null;
		}
	}
	
	public Tile getTileAtPosition(TileMapPosition tileMapPosition) {
		return getTileAtPosition(tileMapPosition.getPositionX(), tileMapPosition.getPositionY());
	}
	
	public Tile getTileAtPosition(int positionX, int positionY) {
		if (checkPositionInMap(positionX, positionY)) {
			return map[positionX][positionY];
		} else {
			return null;
		}
	}
	
	public boolean isTileAtPosition(int positionX, int positionY) {
		return getTileAtPosition(positionX, positionY) != null;
	}
	
	private boolean checkPositionInMap(int positionX, int positionY) {
		if (positionX >= mapXSize || positionX < 0 || positionY >= mapYSize || positionY < 0) {
			return false;
		}
		return true;
	}
	
	/**
	 * Returns list of free border tiles for tile with position from parameters.
	 * 
	 * For example for position [2, 2] in case:
	 * 
	 *	    	0    1    2
	 *		  ----------------     X --> OCCUPIED
	 *		0 |    |    |    |     T --> TESTED
	 *		  |____|____|____|
	 *		1 |    |    | X  |
	 *		  |____|____|____|
	 *		2 |    | X  | T  |
	 *		  |____|____|____|
	 *
	 *
	 *	method returns only position [1, 1] for parameter <code>useOnlyFreeTiles = true</code>.
	 * 
	 * @param positionX X position of tested tile
	 * @param positionY Y position of tested tile
	 * @param useOnlyFreeTiles for <code>true</code> return only free border tiles
	 * @return list of free border tiles for tile with position from parameters.
	 */
	public List<TileMapPosition> getTileFreeBorders(int positionX, int positionY, boolean useOnlyFreeTiles, boolean onlySquare) {
		int minimum = -1;
		int maximum = 1;
		List<TileMapPosition> result = new ArrayList<TileMapPosition>();
		for (int i = minimum; i <= maximum; i++) {
			int posX = positionX + i;
			if (posX >= 0 && posX < mapXSize) {
				
				for (int j = minimum; j <= maximum; j++) {
					int posY = positionY + j;
					if (posY >= 0 && posY < mapYSize && !(i == 0 && j == 0) && (!onlySquare || (i != j && !(i == minimum && j == maximum) && !(j == minimum && i == maximum)))) {
						
						Tile tile = map[posX][posY];
						if (!useOnlyFreeTiles || tile == null) {
							result.add(new TileMapPosition(posX, posY));
						}
						
					}
				}
				
			}
		}
		return result;
	}
	
	/**
	 * Return list of tile map positions in specific quartal from parameter.
	 * For example for map:
	 *	  
	 *	  ---------------------
	 *	  | Q1 | Q1 | Q2 | Q2 | 		 
	 *	  |____|____|____|____|
	 *	  | Q1 | Q1 | Q2 | Q2 |
	 *	  |____|____|____|____|
	 *	  | Q3 | Q3 | Q4 | Q4 |
	 *    |____|____|____|____|
	 *	  | Q3 | Q3 | Q4 | Q4 |
	 *	  |____|____|____|____|
	 *    | Q3 | Q3 | Q4 | Q4 |
	 *	  |____|____|____|____|
	 *
	 *	returns positions [0, 0], [0, 1], [1, 0], [1, 1] for quartal 1.
	 * 
	 * @param tileMapQuartal quartal for returning positions from
	 * @return list of tile map positions in specific quartal from parameter
	 */
	public List<TileMapPosition> getMapQuartalPositions(TileMapQuartals tileMapQuartal) {
		List<TileMapPosition> result = new ArrayList<TileMapPosition>();
		
		int halfPositionX = getMapXSize() / 2;
		int halfPositionY = getMapYSize() / 2;
		
		int sizeX = 0;
		int sizeY = 0;
		int startX = 0;
		int startY = 0;
		
		int mapQuartal = tileMapQuartal.ordinal() + 1;
		if (mapQuartal % 2 == 1) {
			sizeX = halfPositionX;
			startX = 0;
		} else {
			sizeX = getMapXSize() - halfPositionX;
			startX = halfPositionX;
		}
		
		if (mapQuartal < 3) {
			sizeY = halfPositionY;
			startY = 0;
		} else {
			sizeY = getMapYSize() - halfPositionY;
			startY = halfPositionY;
		}
		
		for (int i = 0; i < sizeX; i++) {
			for (int j = 0; j < sizeY; j++) {
				result.add(new TileMapPosition(startX + i, startY + j));
			}
		}
		
		return result;
	}
	
	public TileMapPosition getCenterPositionForTile(TileMapPosition tileMapPosition) {
		return getTileAtPosition(tileMapPosition.getPositionX(), tileMapPosition.getPositionY()).getCenterPosition();
	}
	
	public TileMapPosition getCenterPositionForTile(int tileMapPositionX, int tileMapPositionY) {
		return getTileAtPosition(tileMapPositionX, tileMapPositionY).getCenterPosition();
	}

	public void reset() {
		for (int i = 0; i < mapXSize; i++) {
			for (int j = 0; j < mapYSize; j++) {
				MapObject mapObject = map[i][j].getMapObject();
				if (mapObject != null) {					
					mapObject.setEnable(true);
				}
				for (MapObstacle mapObstacle : map[i][j].getMapObstacles()) {
					if (mapObstacle != null) {						
						mapObstacle.setActive();
					}
				}
			}
		}
	}

}
