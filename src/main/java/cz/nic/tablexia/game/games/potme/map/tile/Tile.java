/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.potme.map.tile;

import java.util.ArrayList;
import java.util.List;

import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.potme.PotmeActivity;
import cz.nic.tablexia.game.games.potme.ResourceManager;
import cz.nic.tablexia.game.games.potme.map.TileMap.TileMapPosition;
import cz.nic.tablexia.game.games.potme.map.mapobject.MapObject;
import cz.nic.tablexia.game.games.potme.map.mapobject.MapObjectType;
import cz.nic.tablexia.game.games.potme.map.mapobstacle.MapObstacle;
import cz.nic.tablexia.game.games.potme.map.mapobstacle.MapObstacleType;
import cz.nic.tablexia.game.games.potme.map.mapobstacle.MapObstacleType.MapObstaclePosition;

/**
 * Tile for map generating
 * 
 * @author Matyáš Latner
 *
 */
public class Tile {
	
	private  static final int TOP_NEIGHBOR_POSITION_IN_ARRAY 	= 0;
	private  static final int RIGHT_NEIGHBOR_POSITION_IN_ARRAY 	= 1;
	private  static final int BOTTOM_NEIGHBOR_POSITION_IN_ARRAY = 2;
	private  static final int LEFT_NEIGHBOR_POSITION_IN_ARRAY 	= 3;
	
	private static final int MAX_NEIGHBORS_COUNT 				= 4;
	private static final int MAX_OBSTACLES_COUNT				= 2;
	
	private static final int  QUARTER_ROTATION 					= 90;
	
	private TileType 			tileType;
	private Sprite				tileSprite;
	private MapObject			mapObject;
	private MapObstacle[]		mapObstacles;
	
	private float				alpha = 1;
	
	private Tile[] 				neighbors;
	
	private int 				distance;
	private int					mapPositionX;
	private int					mapPositionY;

	public Tile(TileType tileType) {
		this.tileType = tileType;
		neighbors = new Tile[MAX_NEIGHBORS_COUNT];
		mapObstacles = new MapObstacle[MAX_OBSTACLES_COUNT];
		resetDistance();
	}
	
	public void resetDistance() {
		distance = Integer.MAX_VALUE;
	}
	
	public TileType getTileType() {
		return tileType;
	}
	
	public int getMapPositionX() {
		return mapPositionX;
	}
	
	public int getMapPositionY() {
		return mapPositionY;
	}
	
	public TileMapPosition getTileMapPosition() {
		return new TileMapPosition(mapPositionX, mapPositionY);
	}
	
	public void setMapPositionX(int mapPositionX) {
		this.mapPositionX = mapPositionX;
	}
	
	public void setMapPositionY(int mapPositionY) {
		this.mapPositionY = mapPositionY;
	}
	
	public void setMapObject(MapObjectType mapObjectType) {
		mapObject = new MapObject(this, mapObjectType);
	}
	
	public MapObject getMapObject() {
		return mapObject;
	}
	
	public void setMapObstacle(MapObstaclePosition mapObstaclePosition, MapObstacleType mapObstacleType) {
		mapObstacles[mapObstaclePosition.ordinal()] = new MapObstacle(this, mapObstaclePosition.getXOffset(), mapObstaclePosition.getYOffset(), mapObstacleType);
	}
	
	public void disableMapObstacle(MapObstaclePosition mapObstaclePosition) {
		mapObstacles[mapObstaclePosition.ordinal()].setInactive();
	}
	
	public MapObstacle getMapObstacle(MapObstaclePosition mapObstaclePosition) {
		return mapObstacles[mapObstaclePosition.ordinal()];
	}
	
	public MapObstacle getMapObstacle(int mapObstaclePosition) {
		return mapObstacles[mapObstaclePosition];
	}
	
	public MapObstacle[] getMapObstacles() {
		return mapObstacles;
	}
	
	public void setTileAlpha(float alpha) {
		this.alpha = alpha;
	}
	
	public void setTopNeighbor(Tile topNeighbor) {
		neighbors[TOP_NEIGHBOR_POSITION_IN_ARRAY] = topNeighbor;
	}
	
	public Tile getTopNeighbor() {
		return neighbors[TOP_NEIGHBOR_POSITION_IN_ARRAY];
	}
	
	public void setRightNeighbor(Tile rightNeighbor) {
		neighbors[RIGHT_NEIGHBOR_POSITION_IN_ARRAY] = rightNeighbor;
	}
	
	public Tile getRightNeighbor() {
		return neighbors[RIGHT_NEIGHBOR_POSITION_IN_ARRAY];
	}
	
	public void setBottomNeighbor(Tile bottomNeighbor) {
		neighbors[BOTTOM_NEIGHBOR_POSITION_IN_ARRAY] = bottomNeighbor;
	}
	
	public Tile getBottomNeighbor() {
		return neighbors[BOTTOM_NEIGHBOR_POSITION_IN_ARRAY];
	}
	
	public void setLeftNeighbor(Tile leftNeighbor) {
		neighbors[LEFT_NEIGHBOR_POSITION_IN_ARRAY] = leftNeighbor;
	}
	
	public Tile getLeftNeighbor() {
		return neighbors[LEFT_NEIGHBOR_POSITION_IN_ARRAY];
	}
	
	public Tile[] getNeighbors() {
		return neighbors;
	}
	
	public void setDistance(int distance) {
		this.distance = distance;
	}
	
	public int getDistance() {
		return distance;
	}
	
	/**
	 * Returns sprite of current tile for rendering.
	 * @param initialPositionX X initial position to render from
	 * @param initialPositionY Y initial position to render from
	 * @param vertexBufferObjectManager
	 * @return sprite of current tile for rendering
	 */
	public Sprite getTileSprite(VertexBufferObjectManager vertexBufferObjectManager) {
		if (tileSprite == null) {
			tileSprite = new Sprite(0, 0, PotmeActivity.TILE_SIZE, PotmeActivity.TILE_SIZE, ResourceManager.getInstance().getTexture(tileType.getTexture()), vertexBufferObjectManager);
			
			TileMapPosition tileCenterPosition = getCenterPosition();
			tileSprite.setPosition(tileCenterPosition.getPositionX(), -tileCenterPosition.getPositionY());
			tileSprite.setRotation(tileType.getRotation() * QUARTER_ROTATION);
		}
		tileSprite.setAlpha(alpha);
		return tileSprite;
	}
	
	public TileMapPosition getCenterPosition() {
		int tileHalfSize = PotmeActivity.TILE_SIZE / 2;
		return new TileMapPosition((mapPositionX * PotmeActivity.TILE_SIZE) + tileHalfSize, ((mapPositionY * PotmeActivity.TILE_SIZE) + tileHalfSize));
	}
	
	/**
	 * Generates random obstacle to this tile with probability from parameter.
	 * Creates only a obstacle that fits tile door specification.
	 * 
	 * @param randomAccess
	 * @param probability the probability of generating the obstacle to this tile
	 */
	public void generateRandomObstacle(RandomAccess randomAccess, double probability) {
		if (!tileType.isWall() && randomAccess.getRandom().nextDouble() <= probability) {			
			List<MapObstaclePosition> possibleObstaclesPositions = new ArrayList<MapObstaclePosition>();
			if (getTileType().isLeftDoor()) {
				possibleObstaclesPositions.add(MapObstaclePosition.LEFT_POSITION);
			}
			if (getTileType().isBottomDoor()) {
				possibleObstaclesPositions.add(MapObstaclePosition.BOTTOM_POSITION);
			}
			if (possibleObstaclesPositions.size() > 0) {
				MapObstaclePosition selectedObstaclePosition = possibleObstaclesPositions.get(randomAccess.getRandom().nextInt(possibleObstaclesPositions.size()));
				List<MapObstacleType> randomMapObstaclerForPosition = MapObstacleType.getMapObstacleForPosition(selectedObstaclePosition);
				setMapObstacle(selectedObstaclePosition, randomMapObstaclerForPosition.get(randomAccess.getRandom().nextInt(randomMapObstaclerForPosition.size())));
			}
		}
	}
	
	@Override
	public String toString() {
		return "Tile[" + mapPositionX + ", " + mapPositionY + "]";
	}

}
