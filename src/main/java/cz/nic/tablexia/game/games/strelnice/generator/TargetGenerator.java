/**
 * 
 */

package cz.nic.tablexia.game.games.strelnice.generator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.andengine.opengl.vbo.VertexBufferObjectManager;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.strelnice.media.GfxManager;
import cz.nic.tablexia.game.games.strelnice.media.TextureType;
import cz.nic.tablexia.game.games.strelnice.model.Target;
import cz.nic.tablexia.game.games.strelnice.model.Wave;

/**
 * @author lhoracek
 */
public class TargetGenerator {
    private static final String TAG                     = TargetGenerator.class.getSimpleName();

    private static final float  EASY_BOX_PROBABILITY    = 0.05f;                                // 5% krabic
    private static final float  MEDIUM_BOX_PROBABILITY  = 0.05f;                                // 5% krabic
    private static final float  HARD_BOX_PROBABILITY    = 0.1f;                                 // 10% krabic

    public static final float   EASY_GOOD_PROBABILITY   = 0.7f;
    public static final float   MEDIUM_GOOD_PROBABILITY = 0.5f;
    public static final float   HARD_GOOD_PROBABILITY   = 0.3f;

    private GfxManager          gfxManager;
    private GameDifficulty      gameDifficulty;
    private Random              random                  = new Random();

    public TargetGenerator(GfxManager gfxManager, GameDifficulty gameDifficulty) {
        super();
        this.gfxManager = gfxManager;
        this.gameDifficulty = gameDifficulty;
    }

    public TextureType getRandomFlowerType(GameDifficulty gameDifficulty) {
        List<TextureType> textureTypeBag = getTextureTypeBag(gameDifficulty);
        int random = (int) (Math.random() * textureTypeBag.size());
        return textureTypeBag.get(random);
    }

    private Map<Wave, Target> lastTargetsForWave = new HashMap<Wave, Target>();

    public Target getLastTargetInWave(Wave wave) {
        return lastTargetsForWave.get(wave);
    }

    public Target addNewTargetToWave(Wave wave, VertexBufferObjectManager pVertexBufferObjectManager, TextureType requiredTextureType) {
        Target prevLastTarget = getLastTargetInWave(wave);
        float targetPeriod = wave.getRunningTime() / wave.getFlowerOnScreen();
        return addNewTargetToWave(wave, pVertexBufferObjectManager, requiredTextureType, prevLastTarget.getStartTime() + targetPeriod);
    }

    public Target addNewTargetToWave(Wave wave, VertexBufferObjectManager pVertexBufferObjectManager, TextureType requiredTextureType, float startTime) {
        return addNewTargetToWave(wave, pVertexBufferObjectManager, requiredTextureType, startTime, true);
    }

    public Target addNewTargetToWave(Wave wave, VertexBufferObjectManager pVertexBufferObjectManager, TextureType requiredTextureType, float startTime, boolean asLast) {
        List<TextureType> textureTypeBag = new ArrayList<TextureType>(requiredTextureType == null ? getTextureTypeBag(gameDifficulty) : Arrays.asList((new TextureType[] { requiredTextureType })));
        Target target = getRandomTarget(textureTypeBag, startTime, wave, pVertexBufferObjectManager);
        if (asLast) {
            lastTargetsForWave.put(wave, target);
        }
        return target;
    }

    /*
     * Init row on start
     */
    public List<Target> generateFlowerRow(Wave wave, VertexBufferObjectManager pVertexBufferObjectManager) {
        List<Target> flowers = new ArrayList<Target>();
        List<TextureType> textureTypeBag = new ArrayList<TextureType>(getTextureTypeBag(gameDifficulty));

        int flowerNumber = wave.getFlowerOnScreen();
        float targetPeriod = wave.getRunningTime() / wave.getFlowerOnScreen();

        // přidáme jeden terč za okraj na prvn obrazovce
        for (int i = -flowerNumber; i <= 0; i++) {

            Target target = getRandomTarget(textureTypeBag, targetPeriod * i, wave, pVertexBufferObjectManager);
            flowers.add(target);
            if (i == 0) {
                lastTargetsForWave.put(wave, target);
            }
        }
        return flowers;
    }

    private TextureType getRandomBoxType() {
        float boxTypeIndex = random.nextFloat();
        if ((gameDifficulty == GameDifficulty.EASY) && (boxTypeIndex > EASY_GOOD_PROBABILITY)) {
            return TextureType.BOX_BAD;
        } else if ((gameDifficulty == GameDifficulty.MEDIUM) && (boxTypeIndex > HARD_GOOD_PROBABILITY)) {
            return TextureType.BOX_BAD;
        } else if ((gameDifficulty == GameDifficulty.HARD) && (boxTypeIndex > HARD_GOOD_PROBABILITY)) {
            return TextureType.BOX_BAD;
        }

        return TextureType.BOX_GOOD;
    }

    private Target getRandomTarget(List<TextureType> textureTypeBag, float startTime, Wave wave, VertexBufferObjectManager pVertexBufferObjectManager) {
        float boxIndex = random.nextFloat();
        TextureType textureType = getRandomBoxType();
        if (boxIndex > getBoxProbability(wave.getDifficulty())) {
            int random = (int) (Math.random() * textureTypeBag.size());
            textureType = textureTypeBag.get(random);
        }
        Target target = new Target(0, 0, gfxManager.get(textureType).second, startTime, wave, textureType, pVertexBufferObjectManager);
        return target;
    }

    private float getBoxProbability(GameDifficulty difficulty) {
        switch (gameDifficulty) {
            case EASY:
                return EASY_BOX_PROBABILITY;
            case MEDIUM:
                return MEDIUM_BOX_PROBABILITY;
            case HARD:
                return HARD_BOX_PROBABILITY;
            default:
                throw new IllegalStateException("Unknown difficulty");
        }
    }

    private List<TextureType> getTextureTypeBag(GameDifficulty gameDifficulty) {
        switch (gameDifficulty) {
            case EASY:
                return Arrays.asList(TextureType.FLOWERS_LEVEL_EASY);
            case MEDIUM:
                return Arrays.asList(TextureType.FLOWERS_LEVEL_MEDIUM);
            case HARD:
                return Arrays.asList(TextureType.FLOWERS_LEVEL_HARD);
            default:
                throw new IllegalStateException("Unknown difficulty");
        }
    }

    public List<Target> initWaves(GameDifficulty difficulty, VertexBufferObjectManager pVertexBufferObjectManager) {
        List<Target> targets = new ArrayList<Target>();
        for (int i = 0; i < 3; i++) {
            targets.addAll(generateFlowerRow(Wave.getWave(gameDifficulty, i), pVertexBufferObjectManager));
        }
        return targets;
    }
}
