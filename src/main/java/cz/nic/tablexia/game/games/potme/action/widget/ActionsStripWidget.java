/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.potme.action.widget;

import java.util.ArrayList;
import java.util.List;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;
import org.andengine.util.modifier.ease.EaseBackOut;
import org.andengine.util.modifier.ease.EaseLinear;

import cz.nic.tablexia.game.games.potme.PotmeActivity;
import cz.nic.tablexia.game.games.potme.PotmeActivity.StartButton;
import cz.nic.tablexia.game.games.potme.ResourceManager;
import cz.nic.tablexia.game.games.potme.UtilityAccess;
import cz.nic.tablexia.game.games.potme.action.Action;
import cz.nic.tablexia.game.games.potme.action.Action.ActionListener;
import cz.nic.tablexia.game.games.potme.action.ActionContainer;
import cz.nic.tablexia.game.games.potme.action.ActionType;

/**
 * Widget for showing selected actions list
 * 
 * @author Matyáš Latner
 *
 */
public class ActionsStripWidget extends Entity implements ActionListener {
	
	private static final float 	ACTION_DETACH_WIDTH_RATIO 			= 0.5f;
	private	static final float 	MAXIMUM_ACTIONS_COUNT				= 200;
	public 	static final int 	ACTION_OFFSET 						= PotmeActivity.ACTION_SIZE_SMALLER / 10;
	private static final int 	NO_SELECTED_POSITION				= -1;
	
	private static boolean 				actionsEnabled 	= true;
	private boolean 					scrollEnabled  	= true;
	
	private int 						height;
	private int 						width;
	private Entity 						scrollPane;
	private Entity 						overlay;
	private Entity 						nextActionField;
	private Entity	 					nextActionFieldReactionArea;
	private int							actualSelectedActionPosition;
	
	private Scene 						scene;
	private StartButton 				startButton;
	private VertexBufferObjectManager 	vertexBufferObjectManager;
	
	private List<ActionContainer>		selectedActions;
	private int 						nextActionFieldActualPosition;
	private MoveModifier 				nextActionFieldMoveModifier;
	private MoveModifier 				scrollPaneMoveModifier;
	private int 						actionOffsetX;
	private boolean 					startButtonControl;
	private boolean 					enableActionSorting;

	public ActionsStripWidget(int positionX,
							  int positionY,
							  int width,
							  int height,
							  Scene scene,
							  VertexBufferObjectManager vertexBufferObjectManager) {
		
		super(positionX - width / 2, positionY - height / 2);
		
		this.width = width;
		this.height = height;
		this.scene = scene;
		this.vertexBufferObjectManager = vertexBufferObjectManager;
		this.actionOffsetX = width / 8;
		
		enableActionSorting = true;
		startButtonControl = true;
		
		actualSelectedActionPosition = NO_SELECTED_POSITION;
		selectedActions = new ArrayList<ActionContainer>();
		
		createBackground();
		createScrollPane();
		createNextActionField();
		createOverlay();
		
		displaySelectedActions(null);
	}
	
	
	
/* //////////////////////////////////////////// CONTROL */
	
	public void enableControl() {
		actionsEnabled = true;
		scrollEnabled  = true;
		for (ActionContainer actionContainer : selectedActions) {
			actionContainer.getAction().enable();
		}
	}
	
	public void disableControl() {
		actionsEnabled = false;
		scrollEnabled  = false;
		for (ActionContainer actionContainer : selectedActions) {
			actionContainer.getAction().disable();
		}
	}
	
	public boolean isNotFull() {
		return selectedActions.size() < MAXIMUM_ACTIONS_COUNT;
	}
	
	public void enableStartButtonControl(boolean startButtonControl) { 
		this.startButtonControl = startButtonControl; 
	}
	
	public void enableActionSorting(boolean enableActionSorting) {
		this.enableActionSorting = enableActionSorting;
	}
	
	
	
/* //////////////////////////////////////////// BACKGROUND AND SCROLLING */
	
	private static final float 	SCROLL_FREQ 			= 120.0f;
	private static final float 	SCROLL_MAX_ACCEL_LIMIT 	= 5000;
	private static final float  SCROLL_FRICTION 		= 0.96f;
	private static final int 	SCROLL_ACCEL_SPEEDUP 	= 7;
	
	private boolean			scrollPaneTracked			= false;
	
	private float 			accel0;
	private float 			accel1;
	private float 			accel2;
	private float			accel3;
	private float			accel4;
	private float			accel5;
	private float 			scrollPaneLastPosition;
	private float 			scrollPaneActualPosition;
	private float 			scrollPaneTrackedPosition;
	
	public boolean isScrollEnabled() {
		return isScreenFilledWithActions() && scrollEnabled;
	}
	
	private void createBackground() {
		Rectangle background = new Rectangle(0, 0, width, height, vertexBufferObjectManager) {

			@Override
			public boolean onAreaTouched(TouchEvent touchEvent, float touchAreaLocalX, float touchAreaLocalY) {
				boolean result = false;
				if (isScrollEnabled()) {
					
					float offsetY = scrollPaneLastPosition - touchAreaLocalY;
					scrollPaneLastPosition = touchAreaLocalY;
					
					accel5 = (accel0 + accel1 + accel2 + accel3 + accel4 + offsetY) / 6;
					accel0 = accel1;
					accel1 = accel2;
					accel3 = accel4;
					accel4 = accel5;
					
					accel5 = accel5 * SCROLL_ACCEL_SPEEDUP;
					
					
					if (touchEvent.getAction() == TouchEvent.ACTION_MOVE) {
						scrollPaneTrackedPosition = scrollPane.getY() - offsetY;
						result = true;
					} else if (touchEvent.getAction() == TouchEvent.ACTION_DOWN) {
						scrollPaneTracked = true;
					} else if (touchEvent.getAction() == TouchEvent.ACTION_UP) {
						scrollPaneTracked = false;
					}
					
				}
				return result;
			}
			
		};
		background.setColor(Color.TRANSPARENT);
		scene.registerTouchArea(background);
		attachChild(background);
		
		scene.registerUpdateHandler(new TimerHandler(1.0f / SCROLL_FREQ, true, new ITimerCallback() {
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler) {
				performScrollPanePositionChange();
			}
		}));
	}
	
	protected synchronized void performScrollPanePositionChange() {
		if (isScrollEnabled()) {			
			// bottom scroll limit
			if (scrollPaneActualPosition > 0) {
				scrollPaneActualPosition = 0;
				accel0 = accel1 = accel2 = accel3 = accel4 = accel5 = 0;
			}
			// top scroll limit
			int actionSize = isNotFull() ? selectedActions.size() + 1 : selectedActions.size();
			int topLimit = height - ((actionSize * (PotmeActivity.ACTION_SIZE_BIGGER + ACTION_OFFSET) + ACTION_OFFSET) + ACTION_OFFSET);
			if (scrollPaneActualPosition < topLimit) {
				scrollPaneActualPosition = topLimit;
				accel0 = accel1 = accel2 = accel3 = accel4 = accel5 = 0;
			}
			
			scrollPane.setPosition(0, scrollPaneActualPosition);
			
			
			if (accel5 < 0 && accel5 < -SCROLL_MAX_ACCEL_LIMIT)
				accel0 = accel1 = accel5 = - SCROLL_MAX_ACCEL_LIMIT;
			if (accel5 > 0 && accel5 > SCROLL_MAX_ACCEL_LIMIT)
				accel0 = accel1 = accel5 = SCROLL_MAX_ACCEL_LIMIT;
			
			if (accel5 >= -1 && accel5 <= 1) {
				accel0 = accel1 = accel2 = accel3 = accel4 = accel5 = 0;
				return;
			}
			
			if (scrollPaneTracked) {
				scrollPaneActualPosition = scrollPaneTrackedPosition;			
			} else {
				if (! (Double.isNaN(accel5) || Double.isInfinite(accel5))) {
					scrollPaneTrackedPosition = scrollPaneActualPosition = scrollPaneActualPosition - accel5;
				}
			}
			accel5 = (accel5 * SCROLL_FRICTION);
		}
	}
	
	
	
/* //////////////////////////////////////////// OVERLAY */
	
	private void createOverlay() {
		Sprite actualPointer = new Sprite(-PotmeActivity.ACTION_SIZE_BIGGER / 2 - ACTION_OFFSET,
										  ACTION_OFFSET + getCurrentActionOffset(0),
										  ResourceManager.getInstance().getTexture(ResourceManager.CONTROL_ACTUAL),
										  vertexBufferObjectManager);
		
		overlay = new Entity();
		overlay.attachChild(actualPointer);
		attachChild(overlay);
	}
	
	private void showOverlay(boolean visible) {
		overlay.setVisible(visible);
	}
	
	
	
/* //////////////////////////////////////////// SCROLLPANE */
	
	private void createScrollPane() {
		scrollPane = new Entity();
		attachChild(scrollPane);
	}
	
	private void moveScrollOffset(float offset) {
		moveScrollPane(scrollPane.getY() + offset, null, false, false);
	}
	
	private void moveScrollPaneTopToAction(int position, IEntityModifierListener modifierListener) {
		moveScrollPane(getScrollPaneTopPosition(position), modifierListener, true, true);
	}
	
	private void moveScrollPaneBottomToAction(int position, IEntityModifierListener modifierListener, boolean useEaseFunction) {
		moveScrollPane(getScrollPaneBottomPosition(position), modifierListener, true, useEaseFunction);
	}
	
	private void moveScrollPane(float positionY, IEntityModifierListener modifierListener, boolean animated, boolean useEaseFunction) {
		if (scrollPaneMoveModifier != null) {
			scrollPane.unregisterEntityModifier(scrollPaneMoveModifier);
			scrollPaneMoveModifier = null;
		}
		if (animated) {			
			scrollPaneMoveModifier = new MoveModifier(PotmeActivity.ANIMATION_MOVE_PERIOD,
					scrollPane.getX(),
					scrollPane.getY(),
					scrollPane.getX(),
					positionY,
					modifierListener,
					useEaseFunction ? EaseBackOut.getInstance() : EaseLinear.getInstance());
			scrollPaneMoveModifier.setAutoUnregisterWhenFinished(true);
			scrollPane.registerEntityModifier(scrollPaneMoveModifier);
		} else {
			scrollPane.setPosition(scrollPane.getX(), positionY);
		}
		scrollPaneActualPosition = scrollPaneTrackedPosition = positionY;
	}
	
	private int getScrollPaneTopPosition(int position) {
		return height - (position * (PotmeActivity.ACTION_SIZE_BIGGER + ACTION_OFFSET) + 2 * ACTION_OFFSET);
	}
	
	private int getScrollPaneBottomPosition(int position) {
		return -position * (PotmeActivity.ACTION_SIZE_BIGGER + ACTION_OFFSET);
	}
	
	
	
/* //////////////////////////////////////////// NEXT ACTION FIELD */
	
	private void createNextActionField() {
		nextActionField = new Sprite(actionOffsetX,
									 0,
									 PotmeActivity.ACTION_SIZE_BIGGER,
									 PotmeActivity.ACTION_SIZE_BIGGER,
									 ResourceManager.getInstance().getTexture(ResourceManager.CONTROL_NEXT),
									 vertexBufferObjectManager);
		
		nextActionFieldReactionArea = new Rectangle(nextActionField.getWidth() / 2,
				height / 2,
				PotmeActivity.ACTION_SIZE_BIGGER,
				height,
				vertexBufferObjectManager);
		nextActionFieldReactionArea.setVisible(false);
		nextActionField.attachChild(nextActionFieldReactionArea);
		
		moveNextActionFieldPermanently(0, false);
		scrollPane.attachChild(nextActionField);
	}
	
	private void moveNextActionFieldPermanently(int position, boolean animated) {
		nextActionFieldActualPosition = ACTION_OFFSET + getCurrentActionOffset(position);
		moveNextActionField(nextActionFieldActualPosition, animated);
	}
	
	private void resetNextActionTemporalyPosition() {
		moveNextActionField(nextActionFieldActualPosition, true);
	}
	
	private void moveNextActionTemporalyHalfUp() {
		moveNextActionField(nextActionFieldActualPosition + (nextActionField.getHeight() / 2) + ActionsStripWidget.ACTION_OFFSET, true);
	}
	
	private void moveNextActionField(float positionY, boolean animated) {
		nextActionField.setVisible(isNotFull());
		if (nextActionFieldMoveModifier != null) {
			nextActionField.unregisterEntityModifier(nextActionFieldMoveModifier);
			nextActionFieldMoveModifier = null;
		}
		if (animated) {
			nextActionFieldMoveModifier = new MoveModifier(PotmeActivity.ANIMATION_MOVE_PERIOD,
					nextActionField.getX(),
					nextActionField.getY(),
					nextActionField.getX(),
					positionY,
					EaseBackOut.getInstance());
			nextActionFieldMoveModifier.setAutoUnregisterWhenFinished(true);
			nextActionField.registerEntityModifier(nextActionFieldMoveModifier);
		} else {
			nextActionField.setPosition(nextActionField.getX(), positionY);
		}
	}
	
	
	
/* //////////////////////////////////////////// COLLISIONS */
	
	public List<IEntity> getReactionEntities() {
		List<IEntity> reactionEntities = new ArrayList<IEntity>();
		for (ActionContainer actionContainer : selectedActions) {
			if (actionContainer.getAction().getOrderNumber() != actualSelectedActionPosition) {				
				reactionEntities.add(actionContainer.getReactionRectangle());
			}
		}
		reactionEntities.add(nextActionFieldReactionArea);
		return reactionEntities;
	}
	
	public void performCollisionWithNumberStart(int collisionNumber) {
		if (isNotFull() && enableActionSorting) {
			if (collisionNumber > 0 && collisionNumber < selectedActions.size()) {
				moveNextActionTemporalyHalfUp();
				moveActionsFromPositionHalfDown(collisionNumber);
				moveActionsFromPositionHalfUp(collisionNumber);
				selectedActions.get(collisionNumber).setReactionRectangleOffset();
			}
		}
	}
	
	public void performCollisionWithNumberFinish(int collisionNumber) {
		resetNextActionTemporalyPosition();
		resetSelectedActionsPositions();
		
		if (collisionNumber < selectedActions.size() && collisionNumber > 0) {
			selectedActions.get(collisionNumber).resetReactionRectangle(true);
		}
	}

	
/* //////////////////////////////////////////// START BUTTON */
	
	public void setStartButton(StartButton startButton) {
		this.startButton = startButton;
	}
	
	private void enableStartButton() {
		if (startButton != null && !startButton.isEnbleda()) {
			startButton.enable();
		}
	}
	
	private void disableStartButton() {
		if (startButton != null && startButton.isEnbleda()) {
			startButton.disable();
		}
	}
	
	public void setStartButtonState() {
		if (startButtonControl) {			
			if (selectedActions.size() > 0) {
				enableStartButton();
			} else {
				disableStartButton();
			}
		}
	}
	
	
/* //////////////////////////////////////////// ACTIONS */
	
	private void moveActionsFromPositionHalfUp(int actionNumber) {
		for (int i = actionNumber; i < selectedActions.size(); i++) {
			selectedActions.get(i).moveTemporalyHalfUp(true);
		}
	}
	
	private void moveActionsFromPositionHalfDown(int actionNumber) {
		for (int i = actionNumber; i >= 0; i--) {
			selectedActions.get(i).moveTemporalyHalfDown(true);
		}
	}
	
	private void resetSelectedActionsPositions() {
		for (ActionContainer actionContainer : selectedActions) {
			actionContainer.resetPosition(true);
		}
	}
	
	public void addSelectedAction(ActionType selectedActionType, int selectedActionPosition) {
		if (isNotFull()) {
			// reset collision animation
			if (selectedActionPosition < selectedActions.size()) {
				selectedActions.get(selectedActionPosition).resetReactionRectangle(false);
			}
			
			// add new action
			ActionContainer actionContainer = createActionContainer(createAction(selectedActionType, selectedActionPosition));
			actionContainer.getAction().setEnbaled(enableActionSorting);
			if (selectedActionPosition > selectedActions.size() || !enableActionSorting) {
				selectedActions.add(actionContainer);
			} else {
				selectedActions.add(selectedActionPosition, actionContainer);
			}
			scrollPane.attachChild(actionContainer);
			
			// reset actions position
			for (int i = 0; i < selectedActions.size(); i++) {
				ActionContainer selectedAction = selectedActions.get(i);
				selectedAction.changeActionNumber(i);
				selectedAction.moveToActionPosition(false, true);
			}
			
			// move with scroll pane
			boolean animatedNextActionField = false;
			if (selectedActionPosition >= selectedActions.size() - 1){
				if (isNotFull()) {					
					if(getScrollPaneTopPosition(selectedActionPosition + 2) < 0) {
						moveScrollPaneTopToAction(selectedActionPosition + 2, null);
					}
					animatedNextActionField = true;
				}
			} else {
				moveScrollOffset(-(PotmeActivity.ACTION_SIZE_BIGGER / 2) - ActionsStripWidget.ACTION_OFFSET);
				if(getScrollPaneTopPosition(selectedActions.size() + 1) > 0) {
					moveScrollPaneBottomToAction(0, null, true);
				}
			}
			
			// move next action field
			moveNextActionFieldPermanently(selectedActions.size(), animatedNextActionField);
			
			setStartButtonState();
		}
	}
	
	private void removeAction(int orderNumber) {
		UtilityAccess.getInstance().detachAndAttachEntity(selectedActions.get(orderNumber), null, null, false);
		for (int i = orderNumber + 1; i < selectedActions.size(); i++) {			
			selectedActions.get(i).movePermanentlyDown(true);
		}
		
		selectedActions.remove(orderNumber);
		moveNextActionFieldPermanently(selectedActions.size(), true);
		if (scrollPane.getY() < getScrollPaneTopPosition(selectedActions.size() + 1)) {
			if (isScreenFilledWithActions()) {
				moveScrollPaneTopToAction(selectedActions.size() + 1, null);
			} else {				
				moveScrollPaneBottomToAction(0, null, true);
			}
		}
		
		setStartButtonState();
	}
	
	public List<ActionContainer> getSelectedActions() {
		return selectedActions;
	}
	
	public void displaySelectedActions(Integer actionNumber) {
		showOverlay(false);
		int position = 0;
		if (actionNumber != null && isScreenFilledWithActions(selectedActions.size())) {
			position = actionNumber.intValue();
			if (!isScreenFilledWithActions(selectedActions.size() - position)) {
				moveScrollPaneTopToAction(selectedActions.size() + 1, null);
				return;
			}
		}
		moveScrollPaneBottomToAction(position, null, true);
	}
	
	public void displayProcessedActions(int actionNumber, boolean useEaseFunction, IEntityModifierListener modifierListener) {	
		showOverlay(true);
		moveScrollPaneBottomToAction(actionNumber, modifierListener, useEaseFunction);
	}
	
	private ActionContainer createActionContainer(Action action) {
		return new ActionContainer(actionOffsetX,
								   action,
								   height / 2,
								   vertexBufferObjectManager);
	}
	
	private Action createAction(ActionType actionType, final int position) {
		Action action = new Action(actionType,
				   				   position,
				   				   PotmeActivity.ACTION_SIZE_BIGGER,
				   				   0,
				   				   0,
				   				   true,
				   				   vertexBufferObjectManager) {
			
			private boolean detached	= false;
			
			private Float lastPositonX  = null;
			private Float lastPositonY  = null;
			private float detachLimit   = -width * ACTION_DETACH_WIDTH_RATIO;
			
			@Override
			protected boolean downAction() {
				if (actionsEnabled) {
					PotmeActivity.GameLayer.ACTION_STRIP_LAYER.sendToFront(scene);
					actualSelectedActionPosition = position;
					return super.downAction();
				}
				return false;
			}
			
			@Override
			protected boolean upAction() {
				PotmeActivity.GameLayer.resetLayersZIndexes(scene);
				actualSelectedActionPosition = NO_SELECTED_POSITION;
				if (detached) {
					// dismiss action
					return super.upAction();
				} else {
					// return back to action position
					for (ActionContainer actionContainer : selectedActions) {						
						actionContainer.getAction().resetPositionX(true);
					}
					return false;
				}
			}
			
			@Override
			protected boolean dragAction(float dragPositionX, float dragPositionY) {
				boolean result = false;
				setVisible(true);
				if (lastPositonX != null && lastPositonY != null) {
					if (detached || getX() < detachLimit) {
						// detached
						if (!detached) {
							detached = true;
							resizeWithAnimation((float)PotmeActivity.ACTION_SIZE_SMALLER);
							UtilityAccess.getInstance().detachAndAttachEntity(this, ActionsStripWidget.this, null, false);
							removeAction(getOrderNumber());
						}
						super.dragAction(dragPositionX, dragPositionY);							
						result = true;
					} else {
						// fixated
						if (dragPositionX <= getInitialPositionX()) {								
							movePositionX(dragPositionX, false);
						} else {
							resetPositionX(true);
						}
					}
				}
				lastPositonX = dragPositionX;
				lastPositonY = dragPositionY;
				
				return result;
			}
		};

		action.setCullingEnabled(true);
		action.setClickable(scene);
		action.setCollisionEntity(this);
		action.addActionListener(this);
		
		return action;
	}
	
	private boolean isScreenFilledWithActions() {
		return isScreenFilledWithActions(selectedActions.size() + 1);
	}
	
	private boolean isScreenFilledWithActions(int actionCount) {
		return (actionCount * (PotmeActivity.ACTION_SIZE_BIGGER + ACTION_OFFSET)) > height;
	}
	
	public int getCurrentActionOffset(int position) {
		return PotmeActivity.ACTION_SIZE_BIGGER / 2 + position * (PotmeActivity.ACTION_SIZE_BIGGER + ACTION_OFFSET) - height / 2;
	}
	
	
	
/* //////////////////////////////////////////// ACTION LISTENER */
	
	@Override
	public void onActionDrag(Action lastAction) {
		// nothing needed
	}
	
	@Override
	public void onActionDrop(Action action, int collidesWithNumber) {
		if (collidesWithNumber != Action.NO_COLLISION_NUMBER) {
			addSelectedAction(action.getActionType(), collidesWithNumber);
		}
	}
}
