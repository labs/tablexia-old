/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.creature;

import org.andengine.opengl.vbo.VertexBufferObjectManager;

import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureRoot.AttributeGender;

/**
 * Factory for creating creatures
 * 
 * @author Matyáš Latner
 */
public class CreatureFactory {
	
	private static final int GENERATOR_TRY_COUNT = 1000;
    private static CreatureFactory instance;

    public static CreatureFactory getInstance() {
        if (instance == null) {
            instance = new CreatureFactory();
        }
        return instance;
    }

    /**
     * Returns random creature with specific forced description and banned description.
     * If description is <code>null</code> or field gender on description is <code>null</code>, then is generated creature with random gender.
     * All creature attributes which are not defined in description are generated.
     * 
     * @param vertexBufferObject andengine vertex buffer
     * @return randomly generated creature
     */
    public CreatureRoot generateCreature(VertexBufferObjectManager vertexBufferObject, RandomAccess randomAccess) {
        return generateCreature(null, null, vertexBufferObject, randomAccess);
    }

    /**
     * Returns random creature with specific forced description and banned description.
     * If description is <code>null</code> or field gender on description is <code>null</code>, then is generated creature with random gender.
     * All creature attributes which are not defined in description are generated.
     * 
     * @param descriptionsToForce list of forced attributes, for <code>null</code> value is no attribute forced
     * @param descriptionToBan list of banned attributes, for <code>null</code> value is no attribute banned
     * @param vertexBufferObject andengine vertex buffer
     * @return randomly generated creature
     */
    public CreatureRoot generateCreature(CreatureDescriptor descriptionsToForce, CreatureDescriptor descriptionToBan, VertexBufferObjectManager vertexBufferObject, RandomAccess randomAccess) {
        return generateCreature(0, 0, descriptionsToForce, descriptionToBan, vertexBufferObject, randomAccess);
    }

    /**
     * Returns random creature with specific forced description and banned description.
     * If description is <code>null</code> or field gender on description is <code>null</code>, then is generated creature with random gender.
     * All creature attributes which are not defined in description are generated.
     * 
     * @param pX creature position x
     * @param pY creature position y
     * @param descriptionsToForce list of forced attributes, for <code>null</code> value is no attribute forced
     * @param descriptionToBan list of banned attributes, for <code>null</code> value is no attribute banned
     * @param vertexBufferObject andengine vertex buffer
     * @return randomly generated creature
     */
    public CreatureRoot generateCreature(float pX, float pY, CreatureDescriptor descriptionsToForce, CreatureDescriptor descriptionToBan, VertexBufferObjectManager vertexBufferObject, RandomAccess randomAccess) {

        CreatureRoot creatureRoot = createCreatureAndAttributes(pX, pY, descriptionsToForce, descriptionToBan, vertexBufferObject, randomAccess);
        creatureRoot.generateCreature(descriptionToBan, vertexBufferObject, randomAccess.getRandom());
        creatureRoot.setAsThief(descriptionsToForce != null ? descriptionsToForce.isThief() : false);
        creatureRoot.setGroupNumber(descriptionsToForce != null ? descriptionsToForce.getGroupNumber() : CreatureRoot.CREATURE_GROUP_NUMBER_NOGROUP);
        return creatureRoot;
    }

    /**
     * Returns creature with specific description from parameter. If description is not valid, method returns <code>null</code>.
     * 
     * @param descriptionsToForce list of forced attributes, for <code>null</code> value is no attribute forced
     * @param descriptionToBan list of banned attributes, for <code>null</code> value is no attribute banned
     * @param copyThiefAttribute <code>true</code> copy thief flag from descriptionToForce
     * @param vertexBufferObject andengine vertex buffer
     * @return randomly generated creature
     */
    public CreatureRoot getCreatureWithAttributes(CreatureDescriptor descriptionsToForce, CreatureDescriptor descriptionToBan, boolean copyThiefAttribute, VertexBufferObjectManager vertexBufferObject, RandomAccess randomAccess) {

        return getCreatureWithAttributes(0, 0, descriptionsToForce, descriptionToBan, copyThiefAttribute, vertexBufferObject, randomAccess);
    }

    /**
     * Returns creature with specific description from parameter. If description is not valid, method returns <code>null</code>.
     * 
     * @param pX creature position x
     * @param pY creature position y
     * @param descriptionsToForce list of forced attributes, for <code>null</code> value is no attribute forced
     * @param descriptionToBan list of banned attributes, for <code>null</code> value is no attribute banned
     * @param copyThiefAttribute <code>true</code> copy thief flag from descriptionToForce
     * @param vertexBufferObject andengine vertex buffer
     * @return randomly generated creature
     */
    public CreatureRoot getCreatureWithAttributes(float pX, float pY, CreatureDescriptor descriptionsToForce, CreatureDescriptor descriptionToBan, boolean copyThiefAttribute, VertexBufferObjectManager vertexBufferObject, RandomAccess randomAccess) {

        if ((descriptionsToForce != null) && descriptionsToForce.hasValidDescription()) {
            CreatureRoot creatureRoot = createCreatureAndAttributes(pX, pY, descriptionsToForce, descriptionToBan, vertexBufferObject, randomAccess);
            if (copyThiefAttribute && descriptionsToForce.isThief()) {
                creatureRoot.setAsThief(true);
            }
            return creatureRoot;
        }
        return null;
    }

    /**
     * Creates creature with attributes in description parameter
     */
    private CreatureRoot createCreatureAndAttributes(float pX, float pY, CreatureDescriptor descriptionsToForce, CreatureDescriptor descriptionToBan, VertexBufferObjectManager vertexBufferObject, RandomAccess randomAccess) {
        AttributeGender genre = (descriptionsToForce != null) && (descriptionsToForce.getGender() != null) ? descriptionsToForce.getGender() : AttributeGender.getRandomGender(randomAccess.getRandom());
        CreatureRoot creatureRoot = new CreatureRoot(genre, pX, pY);
        if (descriptionsToForce != null) {
            for (AttributeDescription attributeDescriptionForce : descriptionsToForce.getDescriptions()) {
            	for (int i = 0; i < GENERATOR_TRY_COUNT; i++) {
            		if (creatureRoot.generateAttribute(attributeDescriptionForce, descriptionToBan, false, vertexBufferObject, randomAccess.getRandom())) {
            			break;
            		}
				}
            }
        }
        return creatureRoot;
    }
}
