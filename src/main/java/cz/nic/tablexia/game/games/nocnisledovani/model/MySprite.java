/**
 * 
 */

package cz.nic.tablexia.game.games.nocnisledovani.model;

import org.andengine.entity.scene.IOnAreaTouchListener;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.vbo.ISpriteVertexBufferObject;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.shader.ShaderProgram;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.DrawType;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import cz.nic.tablexia.game.games.nocnisledovani.listener.OnHandDragCompleteListener;

/**
 * @author lhoracek
 *
 */
public class MySprite extends Sprite {
    private OnHandDragCompleteListener onHandDragCompleteListener;

    /**
     * @param pX
     * @param pY
     * @param pTextureRegion
     * @param pVertexBufferObjectManager
     */
    public MySprite(float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param pX
     * @param pY
     * @param pTextureRegion
     * @param pVertexBufferObject
     */
    public MySprite(float pX, float pY, ITextureRegion pTextureRegion, ISpriteVertexBufferObject pVertexBufferObject) {
        super(pX, pY, pTextureRegion, pVertexBufferObject);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param pX
     * @param pY
     * @param pTextureRegion
     * @param pVertexBufferObjectManager
     * @param pShaderProgram
     */
    public MySprite(float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, ShaderProgram pShaderProgram) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager, pShaderProgram);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param pX
     * @param pY
     * @param pTextureRegion
     * @param pVertexBufferObjectManager
     * @param pDrawType
     */
    public MySprite(float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, DrawType pDrawType) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager, pDrawType);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param pX
     * @param pY
     * @param pTextureRegion
     * @param pVertexBufferObject
     * @param pShaderProgram
     */
    public MySprite(float pX, float pY, ITextureRegion pTextureRegion, ISpriteVertexBufferObject pVertexBufferObject, ShaderProgram pShaderProgram) {
        super(pX, pY, pTextureRegion, pVertexBufferObject, pShaderProgram);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param pX
     * @param pY
     * @param pTextureRegion
     * @param pVertexBufferObjectManager
     * @param pDrawType
     * @param pShaderProgram
     */
    public MySprite(float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, DrawType pDrawType, ShaderProgram pShaderProgram) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager, pDrawType, pShaderProgram);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param pX
     * @param pY
     * @param pWidth
     * @param pHeight
     * @param pTextureRegion
     * @param pVertexBufferObjectManager
     */
    public MySprite(float pX, float pY, float pWidth, float pHeight, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, OnHandDragCompleteListener onHandDragCompleteListener) {
        super(pX, pY, pWidth, pHeight, pTextureRegion, pVertexBufferObjectManager);
        this.onHandDragCompleteListener = onHandDragCompleteListener;
    }

    /**
     * @param pX
     * @param pY
     * @param pWidth
     * @param pHeight
     * @param pTextureRegion
     * @param pSpriteVertexBufferObject
     */
    public MySprite(float pX, float pY, float pWidth, float pHeight, ITextureRegion pTextureRegion, ISpriteVertexBufferObject pSpriteVertexBufferObject) {
        super(pX, pY, pWidth, pHeight, pTextureRegion, pSpriteVertexBufferObject);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param pX
     * @param pY
     * @param pWidth
     * @param pHeight
     * @param pTextureRegion
     * @param pVertexBufferObjectManager
     * @param pShaderProgram
     */
    public MySprite(float pX, float pY, float pWidth, float pHeight, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, ShaderProgram pShaderProgram) {
        super(pX, pY, pWidth, pHeight, pTextureRegion, pVertexBufferObjectManager, pShaderProgram);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param pX
     * @param pY
     * @param pWidth
     * @param pHeight
     * @param pTextureRegion
     * @param pVertexBufferObjectManager
     * @param pDrawType
     */
    public MySprite(float pX, float pY, float pWidth, float pHeight, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, DrawType pDrawType) {
        super(pX, pY, pWidth, pHeight, pTextureRegion, pVertexBufferObjectManager, pDrawType);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param pX
     * @param pY
     * @param pWidth
     * @param pHeight
     * @param pTextureRegion
     * @param pSpriteVertexBufferObject
     * @param pShaderProgram
     */
    public MySprite(float pX, float pY, float pWidth, float pHeight, ITextureRegion pTextureRegion, ISpriteVertexBufferObject pSpriteVertexBufferObject, ShaderProgram pShaderProgram) {
        super(pX, pY, pWidth, pHeight, pTextureRegion, pSpriteVertexBufferObject, pShaderProgram);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param pX
     * @param pY
     * @param pWidth
     * @param pHeight
     * @param pTextureRegion
     * @param pVertexBufferObjectManager
     * @param pDrawType
     * @param pShaderProgram
     */
    public MySprite(float pX, float pY, float pWidth, float pHeight, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, DrawType pDrawType, ShaderProgram pShaderProgram) {
        super(pX, pY, pWidth, pHeight, pTextureRegion, pVertexBufferObjectManager, pDrawType, pShaderProgram);
        // TODO Auto-generated constructor stub
    }

    public OnHandDragCompleteListener getOnHandDragCompleteListener() {
        return onHandDragCompleteListener;
    }

    private IOnAreaTouchListener areaTouchListener;

    public void setAreaTouchListener(IOnAreaTouchListener areaTouchListener) {
        this.areaTouchListener = areaTouchListener;
    }

    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        if (areaTouchListener != null) {
            return areaTouchListener.onAreaTouched(pSceneTouchEvent, this, pTouchAreaLocalX, pTouchAreaLocalY);
        }
        return false;
    }
}
