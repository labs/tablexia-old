/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.rules.medium;

import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.Context;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureFactory;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute.AttributeColor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.ClothingAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.rules.easy.CC_0_notCCRule;

/**
 * 
 * @author Matyáš Latner
 */
public class CC_1_notCARule extends CC_0_notCCRule {
    
	private   static final int 	 	GROUP_SIZE	= 3;

    public CC_1_notCARule(Context context, RandomAccess randomAccess, VertexBufferObjectManager vertexBufferObject, int numberOfCreatures, int numberOfThieves) {
        super(context, randomAccess, vertexBufferObject, numberOfCreatures, numberOfThieves, GROUP_SIZE);
        T1_OFFSET = Integer.valueOf(2);
    }
    
    @Override
    public String[] prepareRuleMessageParameters() {
        return new String[] {
        		getAttributeColorName(t0CreatureDescriptorToBan.getDescriptions().get(0)),
                getAttributeColorName(getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0)),
                getAttributeName(getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0), false)
        };
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
    	
    	addGlobalCreatureDescriptor(T0_OFFSET, new CreatureDescriptor());
    	
    	AttributeColor t0CommonAttributeColor = getRandomColorFromGeneratedCreature(BAN_ATTRIBUTES_SET_FOR_GENERATING, vertexBufferObject);
    	t0CreatureDescriptorToBan = new CreatureDescriptor();
        AttributeDescription t0AttributeDescription = new AttributeDescription(t0CommonAttributeColor, null, ClothingAttribute.class);
        t0CreatureDescriptorToBan.addDescription(t0AttributeDescription);
        
        
        CreatureDescriptor t1BannedAttributesForGenerator = new CreatureDescriptor();
        t1BannedAttributesForGenerator.disableGenderCompatibilityCheck();
        t1BannedAttributesForGenerator.addDescriptions(BAN_ATTRIBUTES_SET_FOR_GENERATING.getDescriptions());
        t1BannedAttributesForGenerator.addDescription(t0AttributeDescription);
    	
    	AttributeDescription t1AttributeDescription = getRandomAttributeDescription(CreatureFactory.getInstance().generateCreature(null, t1BannedAttributesForGenerator, vertexBufferObject, getRandomAccess()).getCreatureDescrition());
    	CreatureDescriptor t1CreatureDescriptor = new CreatureDescriptor();
    	t1CreatureDescriptor.addDescription(t1AttributeDescription);
        addGlobalCreatureDescriptor(T1_OFFSET, t1CreatureDescriptor);
    }

}
