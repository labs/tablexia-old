/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.potme;

import java.util.HashMap;
import java.util.Map;

import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.Texture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.opengl.texture.region.TiledTextureRegion;

import com.activeandroid.util.Log;

import android.content.Context;
import android.graphics.Typeface;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.util.andengine.ZipFileTexture;

/**
 * Resource manager for game "Potme"
 * 
 * @author Matyáš Latner
 */
public class ResourceManager {

    private static final String                   ASSET_GAME            = "potme/";
    private static final String                   ASSET_GFX_SOURCE      = ASSET_GAME + "gfx/";

    private static final String                   ASSET_TILES           = "tile/";
    public static final String                    TILE_0                = ASSET_TILES + "tile0.png";
    public static final String                    TILE_1                = ASSET_TILES + "tile1.png";
    public static final String                    TILE_2L               = ASSET_TILES + "tile2L.png";
    public static final String                    TILE_2I               = ASSET_TILES + "tile2I.png";
    public static final String                    TILE_3                = ASSET_TILES + "tile3.png";
    public static final String                    TILE_4                = ASSET_TILES + "tile4.png";

    private static final String                   ASSET_OBJECTS         = "objects/";
    public static final String                    MAPOBJECT_SAFE        = ASSET_OBJECTS + "safe.png";
    public static final String                    MAPOBJECT_KEY         = ASSET_OBJECTS + "key.png";
    public static final String                    MAPOBJECT_STAIRS      = ASSET_OBJECTS + "stairs.png";

    private static final String                   ASSET_OBSTACLES       = "obstacles/";
    public static final String                    OBSTACLE_DOOR_CLOSED  = ASSET_OBSTACLES + "door_closed.png";
    public static final String                    OBSTACLE_DOOR_OPENED  = ASSET_OBSTACLES + "door_opened.png";
    public static final String                    OBSTACLE_DOG_SLEEP    = ASSET_OBSTACLES + "dog_sleep.png";
    public static final String                    OBSTACLE_DOG_ATTACK   = ASSET_OBSTACLES + "dog_attack.png";

    private static final String                   ASSET_ACTIONS         = "actions/";
    public static final String                    ACTION_GO             = ASSET_ACTIONS + "go.png";
    public static final String                    ACTION_LEFT           = ASSET_ACTIONS + "left.png";
    public static final String                    ACTION_RIGHT          = ASSET_ACTIONS + "right.png";
    public static final String                    ACTION_DOOR           = ASSET_ACTIONS + "door.png";
    public static final String                    ACTION_STAIRS         = ASSET_ACTIONS + "stairs.png";
    public static final String                    ACTION_DOG            = ASSET_ACTIONS + "dog.png";
    public static final String                    ACTION_KEY            = ASSET_ACTIONS + "key.png";

    private static final String                   ASSET_CONTROL         	= "control/";
    public static final String                    CONTROL_NEXT          	= ASSET_CONTROL + "next.png";
    public static final String                    CONTROL_START_UNPRESSED 	= ASSET_CONTROL + "start_button_unpressed.png";
    public static final String                    CONTROL_START_DISABLED 	= ASSET_CONTROL + "start_button_disabled.png";
    public static final String                    CONTROL_ACTUAL        	= ASSET_CONTROL + "actual.png";
    public static final String                    CONTROL_KEY     			= ASSET_CONTROL + "key_icon.png";

    private static final String                   ASSET_CREATURE        		= "creature/";
    public static final String                    CREATURE_PLAYER_WALK_TILED 	= ASSET_CREATURE + "player_walk_tiled.png";
    public static final String                    CREATURE_PLAYER_JUMP_TILED 	= ASSET_CREATURE + "player_jump_tiled.png";
    public static final String                    CREATURE_PLAYER_CRASH_TILED 	= ASSET_CREATURE + "player_crash_tiled.png";
    
    private static final String                   ASSET_BACKGROUND        			= "background/";
    public static final String                    BACKGROUND_TEXTURE				= ASSET_BACKGROUND + "background_texture.jpg";
    public static final String                    BACKGROUND_MAP_DRAWING			= ASSET_BACKGROUND + "map_background_drawing.png";
    public static final String                    BACKGROUND_ACTIONSTRIP_TEXTURE	= ASSET_BACKGROUND + "actionstrip_background_texture.jpg";
    public static final String                    BACKGROUND_ACTIONS_TEXTURE		= ASSET_BACKGROUND + "background_actions_texture.png";
    
    private static final String                   ASSET_INFO        				= "info/";
    public static final String                    INFO_CRASH						= ASSET_INFO + "crash.png";
    public static final String                    INFO_SAFE1						= ASSET_INFO + "safe1.png";
    public static final String                    INFO_SAFE2						= ASSET_INFO + "safe2.png";
    public static final String                    INFO_START_ARROW					= ASSET_INFO + "start_arrow.png";
    
    
    private static final String         		  ASSET_SFX_SOURCE      = ASSET_GAME + "sfx/";

    public static final String[]          		  STEPS   				= new String[]{"steps1.mp3", "steps2.mp3", "steps3.mp3"};
    public static final String[]          		  JUMPS   				= new String[]{"jump1.mp3", "jump2.mp3"};
    public static final String[]          		  BARKS   				= new String[]{"bark1.mp3", "bark2.mp3"};
    public static final String[]          		  BUMPS   				= new String[]{"bump1.mp3", "bump2.mp3"};
    public static final String          		  DOOR   				= "door.mp3";
    public static final String          		  KEY   				= "key.mp3";
    public static final String          		  SAFE   				= "safe.mp3";
    public static final String          		  ERROR   				= "error.mp3";

    private final Map<String, ITextureRegion>     gameTextures;
    private final Map<String, TiledTextureRegion> tiledTextures;
    private final Map<String, Sound>          	  gameSounds;

    private Font                                  font;

    private static ResourceManager                instance;

    private ResourceManager() {
        gameTextures 	= new HashMap<String, ITextureRegion>();
        tiledTextures 	= new HashMap<String, TiledTextureRegion>();
        gameSounds 		= new HashMap<String, Sound>();
    }

    public static ResourceManager getInstance() {
        if (instance == null) {
            instance = new ResourceManager();
        }
        return instance;
    }

    /**
     * Returns texture object for specific asset name
     * 
     * @param assetName
     *            name of asset
     * @return texture for asset name
     */
    public ITextureRegion getTexture(String assetName) {
        return gameTextures.get(assetName);
    }
    
    /**
     * Returns sound object for specific asset name
     * 
     * @param assetName name of sound
     * @return sound for asset name
     */
    public Sound getSound(String assetName) {
        return gameSounds.get(assetName);
    }

    /**
     * Returns tiled texture object for specific asset name
     * 
     * @param assetName
     *            name of asset
     * @return texture for asset name
     */
    public TiledTextureRegion getTiledTexture(String assetName) {
        return tiledTextures.get(assetName);
    }

    /**
     * Loads all textures
     * 
     * @param engine
     * @param context
     */
    public synchronized void loadTexturesAndSounds(Engine engine, Context context, GameDifficulty gameDifficulty) {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath(ASSET_GFX_SOURCE);

        loadTexture(engine, context, TILE_0);
        loadTexture(engine, context, TILE_1);
        loadTexture(engine, context, TILE_2I);
        loadTexture(engine, context, TILE_2L);
        loadTexture(engine, context, TILE_3);
        loadTexture(engine, context, TILE_4);

        loadTexture(engine, context, MAPOBJECT_SAFE);
        loadTexture(engine, context, MAPOBJECT_KEY);
        loadTexture(engine, context, MAPOBJECT_STAIRS);

        loadTexture(engine, context, OBSTACLE_DOOR_CLOSED);
        loadTexture(engine, context, OBSTACLE_DOOR_OPENED);
        loadTexture(engine, context, OBSTACLE_DOG_SLEEP);
        loadTexture(engine, context, OBSTACLE_DOG_ATTACK);

        loadTexture(engine, context, ACTION_GO);
        loadTexture(engine, context, ACTION_LEFT);
        loadTexture(engine, context, ACTION_RIGHT);
        loadTexture(engine, context, ACTION_DOOR);
        loadTexture(engine, context, ACTION_STAIRS);
        loadTexture(engine, context, ACTION_DOG);
        loadTexture(engine, context, ACTION_KEY);

        loadTexture(engine, context, CONTROL_NEXT);
        loadTexture(engine, context, CONTROL_START_UNPRESSED);
        loadTexture(engine, context, CONTROL_START_DISABLED);
        loadTexture(engine, context, CONTROL_ACTUAL);
        loadTexture(engine, context, CONTROL_KEY);

        loadTiledTexture(engine, context, CREATURE_PLAYER_WALK_TILED, 4, 2);
        loadTiledTexture(engine, context, CREATURE_PLAYER_JUMP_TILED, 3, 2);
        loadTiledTexture(engine, context, CREATURE_PLAYER_CRASH_TILED, 7, 1);
        
        loadTexture(engine, context, BACKGROUND_TEXTURE);
        loadTexture(engine, context, BACKGROUND_MAP_DRAWING);
        loadTexture(engine, context, BACKGROUND_ACTIONSTRIP_TEXTURE);
        loadTexture(engine, context, BACKGROUND_ACTIONS_TEXTURE);
        
        loadTexture(engine, context, INFO_CRASH);
        loadTexture(engine, context, INFO_SAFE1);
        loadTexture(engine, context, INFO_SAFE2);
        loadTexture(engine, context, INFO_START_ARROW);
        
        
        SoundFactory.setAssetBasePath(ASSET_SFX_SOURCE);
        loadSoundGroup(engine, context, STEPS);
        loadSoundGroup(engine, context, JUMPS);
        loadSoundGroup(engine, context, BARKS);
        loadSoundGroup(engine, context, BUMPS);
        loadSound(engine, context, DOOR);
        loadSound(engine, context, KEY);
        loadSound(engine, context, SAFE);
        loadSound(engine, context, ERROR);
        

        loadFont(engine);
    }

    /**
     * Loads texture to texture map
     */
    private void loadTexture(Engine engine, Context context, String assetPath) {
        Tablexia tablexia = (Tablexia) context.getApplicationContext();
        
        Texture texture = new ZipFileTexture(engine.getTextureManager(), tablexia.getZipResourceFile(), ASSET_GFX_SOURCE + assetPath, TextureOptions.BILINEAR);
        texture.load();
        
        gameTextures.put(assetPath, TextureRegionFactory.extractFromTexture(texture));
    }

    private void loadTiledTexture(Engine engine, Context context, String assetPath, int columns, int rows) {
        Tablexia tablexia = (Tablexia) context.getApplicationContext();
        
        Texture texture = new ZipFileTexture(engine.getTextureManager(), tablexia.getZipResourceFile(), ASSET_GFX_SOURCE + assetPath, TextureOptions.BILINEAR);
        texture.load();
        
        tiledTextures.put(assetPath, TextureRegionFactory.extractTiledFromTexture(texture, columns, rows));
    }
    
    private void loadSoundGroup(Engine engine, Context context, String[] soundGroup) {
    	for (String sound : soundGroup) {			
        	loadSound(engine, context, sound);
		}
	}
    
    private void loadSound(Engine engine, Context context, String assetPath) {
        Tablexia tablexia = (Tablexia) context.getApplicationContext();
        try {
            gameSounds.put(assetPath, SoundFactory.createSoundFromAssetFileDescriptor(engine.getSoundManager(), tablexia.getZipResourceFile().getAssetFileDescriptor(ASSET_SFX_SOURCE + assetPath)));
        } catch (Exception e) {
            Log.e("Cannot load sound: " + assetPath, e);
        }
    }

    private void loadFont(Engine engine) {
        font = FontFactory.create(engine.getFontManager(), engine.getTextureManager(), 256, 256, Typeface.create(Typeface.DEFAULT, Typeface.NORMAL), 20);
        font.load();
    }

    public Font getFont() {
        return font;
    }

}
