
package cz.nic.tablexia.game.games.pronasledovani;

import java.util.ArrayList;
import java.util.Random;

import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TextureRegion;

import android.content.Context;
import android.content.res.AssetManager;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.util.andengine.ZipFileBitmapTextureAtlasSource;
import cz.nic.tablexia.util.andengine.ZipFileTexture;

public class TexturesGenerator {
    private static final String      TEXTURES_BASE_PATH = PronasledovaniActivity.BASE_DIR + "textures/";
    private static final String      BACKGROUND_PATH    = "background2.png";
    private static final String      SHADOW_PATH        = "shadow.png";
    private static final String      CAR_PATH           = "car.png";
    private static final String      BOAT_PATH          = "boat.png";
    private static final String      MOTORCYCLE_PATH    = "motorcycle.png";
    private static final String      TRAIN_PATH         = "train.png";
    private static final String      FLAG_PATH          = "flag.png";

    private ArrayList<TextureRegion> carsTextureRegionList;
    private ArrayList<TextureRegion> motorcyclesTextureRegionList;
    private ArrayList<TextureRegion> boatsTextureRegionList;
    private ArrayList<TextureRegion> trainsTextureRegionList;

    // vozidlo, ktere se zobraz po dohrani hry
    private BitmapTextureAtlas       carTexture;
    private BitmapTextureAtlas       boatTexture;
    private BitmapTextureAtlas       motorcycleTexture;
    private BitmapTextureAtlas       trainTexture;
    private TextureRegion            carTextureRegion;
    private TextureRegion            boatTextureRegion;
    private TextureRegion            motorcycleTextureRegion;
    private TextureRegion            trainTextureRegion;
    // oznaceni sidla lupicu na mape
    private BitmapTextureAtlas       flagTexture;
    private TextureRegion            flagTextureRegion;
    // textura predstavujici stin pod dilkem
    private BitmapTextureAtlas       shadowTexture;
    private TextureRegion            shadowTextureRegion;
    // pozadi
    private ITexture                 backgroundTexture;

    public TexturesGenerator(Context context, TextureManager textureManager, AssetManager assetManager) {
        Tablexia tablexia = (Tablexia) context.getApplicationContext();

        carsTextureRegionList = new ArrayList<TextureRegion>();
        motorcyclesTextureRegionList = new ArrayList<TextureRegion>();
        boatsTextureRegionList = new ArrayList<TextureRegion>();
        trainsTextureRegionList = new ArrayList<TextureRegion>();

        // AUTICKO
        carTexture = new BitmapTextureAtlas(textureManager, 54, 92, TextureOptions.BILINEAR);
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath(TEXTURES_BASE_PATH);
        carTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromSource(carTexture, ZipFileBitmapTextureAtlasSource.create(tablexia.getZipResourceFile(), TEXTURES_BASE_PATH + CAR_PATH), 0, 0);
        carTexture.load();

        carsTextureRegionList.add(carTextureRegion);

        //LODICKA
        boatTexture = new BitmapTextureAtlas(textureManager, 36, 92, TextureOptions.BILINEAR);
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath(TEXTURES_BASE_PATH);
        boatTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromSource(boatTexture, ZipFileBitmapTextureAtlasSource.create(tablexia.getZipResourceFile(), TEXTURES_BASE_PATH + BOAT_PATH), 0, 0);
        boatTexture.load();

        boatsTextureRegionList.add(boatTextureRegion);

        // MOTORKA
        motorcycleTexture = new BitmapTextureAtlas(textureManager, 34, 75, TextureOptions.BILINEAR);
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath(TEXTURES_BASE_PATH);
        motorcycleTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromSource(motorcycleTexture, ZipFileBitmapTextureAtlasSource.create(tablexia.getZipResourceFile(), TEXTURES_BASE_PATH + MOTORCYCLE_PATH), 0, 0);
        motorcycleTexture.load();

        motorcyclesTextureRegionList.add(motorcycleTextureRegion);

        // VLAK
        trainTexture = new BitmapTextureAtlas(textureManager, 35, 92, TextureOptions.BILINEAR);
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath(TEXTURES_BASE_PATH);
        trainTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromSource(trainTexture, ZipFileBitmapTextureAtlasSource.create(tablexia.getZipResourceFile(), TEXTURES_BASE_PATH + TRAIN_PATH), 0, 0);
        trainTexture.load();

        trainsTextureRegionList.add(trainTextureRegion);

        // VLAJKA OZNACUJICI SIDLO LUPICU
        flagTexture = new BitmapTextureAtlas(textureManager, 127, 101, TextureOptions.BILINEAR);
        flagTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromSource(flagTexture, ZipFileBitmapTextureAtlasSource.create(tablexia.getZipResourceFile(), TEXTURES_BASE_PATH + FLAG_PATH), 0, 0);
        flagTexture.load();

        shadowTexture = new BitmapTextureAtlas(textureManager, 352, 316, TextureOptions.BILINEAR);

        shadowTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromSource(shadowTexture, ZipFileBitmapTextureAtlasSource.create(tablexia.getZipResourceFile(), TEXTURES_BASE_PATH + SHADOW_PATH), 0, 0);
        shadowTexture.load();

        // POZADI

        backgroundTexture = new ZipFileTexture(textureManager, tablexia.getZipResourceFile(), TEXTURES_BASE_PATH + BACKGROUND_PATH, TextureOptions.REPEATING_BILINEAR_PREMULTIPLYALPHA);

        backgroundTexture.load();

    }
    public TextureRegion getFlagTextureRegion() {
        return flagTextureRegion;
    }

    public TextureRegion getShadowTextureRegion() {
        return shadowTextureRegion;
    }

    public ITexture getBackgroundTexture() {
        return backgroundTexture;
    }

    private TextureRegion getRandomGroundVehicleTextureRegion() {
        int max = carsTextureRegionList.size() - 1;
        int min = 0;
        return carsTextureRegionList.get(getRandomNumber(min, max));
    }

    private TextureRegion getRandomBoatTextureRegion() {
        //        int max = boatsTextureRegionList.size() - 1;
        //        int min = 0;
        //        return boatsTextureRegionList.get(getRandomNumber(min, max));
        return boatsTextureRegionList.get(0);
    }

    private TextureRegion getRandomTrainTextureRegion() {
        //        int max = trainsTextureRegionList.size() - 1;
        //        int min = 0;
        //        return trainsTextureRegionList.get(getRandomNumber(min, max));
        return trainsTextureRegionList.get(0);
    }

    private TextureRegion getRandomCarTextureRegion() {
        return carsTextureRegionList.get(0);
    }

    private TextureRegion getRandomMotorcycleTextureRegion() {
        return motorcyclesTextureRegionList.get(0);
    }

    private int getRandomNumber(int min, int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }

    public TextureRegion getRandomVehicleTextureRegion(VehicleTypeEnum vehicleType) {
        switch (vehicleType) {
            case BOAT:
                return getRandomBoatTextureRegion();
            case TRAIN:
                return getRandomTrainTextureRegion();
            case CAR:
                return getRandomCarTextureRegion();
            case MOTORCYCLE:
                return getRandomMotorcycleTextureRegion();
            default:
                throw new IllegalArgumentException("Chybny typ vozidla");
        }
    }
}
