/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.strelnice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.engine.handler.IUpdateHandler.IUpdateHandlerMatcher;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierMatcher;
import org.andengine.entity.modifier.MoveByModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.util.adt.align.HorizontalAlign;
import org.andengine.util.adt.color.Color;
import org.andengine.util.modifier.IModifier;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;
import android.util.Pair;
import cz.nic.tablexia.R;
import cz.nic.tablexia.audio.resources.SpeechSounds;
import cz.nic.tablexia.game.GameActivity;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.GamesDefinition;
import cz.nic.tablexia.game.games.strelnice.events.GameOverUpdateHandler;
import cz.nic.tablexia.game.games.strelnice.events.TargetPositionUpdateHandler;
import cz.nic.tablexia.game.games.strelnice.generator.TargetGenerator;
import cz.nic.tablexia.game.games.strelnice.media.BoxType;
import cz.nic.tablexia.game.games.strelnice.media.GfxManager;
import cz.nic.tablexia.game.games.strelnice.media.MfxManager;
import cz.nic.tablexia.game.games.strelnice.media.SoundType;
import cz.nic.tablexia.game.games.strelnice.media.TextureType;
import cz.nic.tablexia.game.games.strelnice.model.Carousel;
import cz.nic.tablexia.game.games.strelnice.model.Target;
import cz.nic.tablexia.game.games.strelnice.model.Target.OnTargetAreaTouchedListener;
import cz.nic.tablexia.game.games.strelnice.model.WatchHand;
import cz.nic.tablexia.game.games.strelnice.model.effects.Cloud;
import cz.nic.tablexia.game.games.strelnice.model.effects.Cloud.OnCloudOverListener;
import cz.nic.tablexia.game.games.strelnice.model.effects.Effect;
import cz.nic.tablexia.game.games.strelnice.model.effects.Effect.OnEffectOverListener;
import cz.nic.tablexia.util.ScalingHelper;

public class StrelniceActivity extends GameActivity implements OnTargetAreaTouchedListener, IOnSceneTouchListener {
    private static final String       TAG                  = StrelniceActivity.class.getSimpleName();
    public static final String        BASE_DIR             = "strelnice/";

    public static final int[]         CUPS_EASY            = { 25, 45, 75 };
    public static final int[]         CUPS_MEDIUM          = { 20, 40, 70 };
    public static final int[]         CUPS_HARD            = { 20, 35, 60 };

    private static final int          CAMERA_WIDTH         = 1280;

    public static final int           GAME_TIME            = 60;
    public static final int           WARNING_TIME         = 5;
    public static final float         WAVE_VERTICAL_OFFSET = 0.2f;                                   // procento výšky obrazovky

    private static final int          WATCH_HAND_ZINDEX    = 60;
    private static final int          WATCH_ZINDEX         = 60;
    private static final int          FRAME_ZINDEX         = 50;
    private static final int          CLOUD_ZINDEX         = 55;
    private static final int          WAVE1_ZINDEX         = 40;
    private static final int          WAVE2_ZINDEX         = 30;
    private static final int          WAVE3_ZINDEX         = 20;
    public static final int           FLOWERS_WAVE1_ZINDEX = 35;
    public static final int           FLOWERS_WAVE2_ZINDEX = 25;
    public static final int           FLOWERS_WAVE3_ZINDEX = 15;
    public static final int           CAROUSEL_ZINDEX      = WATCH_HAND_ZINDEX;
    public static final int           SCORE_ZINDEX         = WATCH_HAND_ZINDEX;

    private float                     cameraHeight;
    private boolean                   baseInitDone;                                                  // info,jestliuz bylo nainicializova (replay uz nebude znovu davat pozadi atd)
    private GfxManager                gfxManager;
    private MfxManager                mfxManager;

    private Font                      mFont;

    private Sprite                    watch;
    private WatchHand                 watchHand;
    private Carousel                  carousel;
    private Text                      scoreText;

    private List<Target>              targets              = new ArrayList<Target>();
    private Pair<TextureType, Sprite> currentTarget;
    private TargetGenerator           targetGenerator;

    private GameOverUpdateHandler     timeHandler;

    public StrelniceActivity() {
        super(GamesDefinition.STRELNICE);
    }

    public Pair<TextureType, Sprite> getCurrentTarget() {
        return currentTarget;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            try {
                // TODO restore state
                Log.i(TAG, "State restored");
            } catch (Exception e) {
                Log.e(TAG, "Error saving state" + e.getMessage());
            }
        }
    }

    @Override
    public Camera onCreateCamera() {
        float scale = displaySize.x / (float) displaySize.y;
        cameraHeight = (CAMERA_WIDTH / scale);
        return new Camera(0, 0, CAMERA_WIDTH, cameraHeight);
    }

    /**
     * Zapneme si podporu zvuku a zvukových efektů
     */
    @Override
    public EngineOptions onCreateEngineOptions() {
        EngineOptions engineOptions = super.onCreateEngineOptions();
        engineOptions.getAudioOptions().setNeedsSound(true).setNeedsMusic(true);
        return engineOptions;
    }

    @Override
    public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws IOException {

        gfxManager = new GfxManager(getTextureManager(), getAssets(), this);
        gfxManager.loadTextures();

        // load zvuku
        mfxManager = new MfxManager(getMusicManager(), getSoundManager(), this);
        mfxManager.loadSounds();

        mFont = FontFactory.create(getFontManager(), getTextureManager(), 256, 256, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 70, Color.WHITE_ARGB_PACKED_INT);
        mFont.load();
        pOnCreateResourcesCallback.onCreateResourcesFinished();
    }

    @Override
    protected void initGame() {
        super.initGame();
        if (!baseInitDone) {

            Sprite background = new Sprite(camera.getWidth() / 2, camera.getHeight() / 2, camera.getWidth(), camera.getHeight(), gfxManager.get(TextureType.POZADI).second, getVertexBufferObjectManager());
            scene.setBackground(new SpriteBackground(background));

            Sprite frame = new Sprite(camera.getWidth() / 2, camera.getHeight() / 2, camera.getWidth(), camera.getHeight(), gfxManager.get(TextureType.FRAME).second, getVertexBufferObjectManager());
            frame.setZIndex(FRAME_ZINDEX);
            scene.attachChild(frame);

            float watchWidth = (int) camera.getWidth() / 5;
            float watchHeight = ScalingHelper.getHeightFromWidth(gfxManager.get(TextureType.WATCH).second.getWidth(), gfxManager.get(TextureType.WATCH).second.getHeight(), watchWidth);
            watch = new Sprite(watchWidth / 2, -watchHeight / 2, watchWidth, watchHeight, gfxManager.get(TextureType.WATCH).second, getVertexBufferObjectManager());
            watch.setZIndex(WATCH_ZINDEX);
            scene.attachChild(watch);

            float watchHandWidth = gfxManager.get(TextureType.WATCH_HAND).second.getWidth() * ScalingHelper.getScale(gfxManager.get(TextureType.WATCH).second.getWidth(), watchWidth);
            float watchHandHeight = ScalingHelper.getHeightFromWidth(gfxManager.get(TextureType.WATCH_HAND).second.getWidth(), gfxManager.get(TextureType.WATCH_HAND).second.getHeight(), watchHandWidth);
            float watchCenterOffsetX = TextureType.WATCH_CENTER_X_OFFSET * ScalingHelper.getScale(gfxManager.get(TextureType.WATCH).second.getWidth(), watchWidth);
            float watchCenterOffsetY = TextureType.WATCH_CENTER_Y_OFFSET * ScalingHelper.getScale(gfxManager.get(TextureType.WATCH).second.getWidth(), watchWidth);
            watchHand = new WatchHand((watchWidth / 2) + watchCenterOffsetX, ((watchHeight / 2) + (watchHandHeight / 2)) + watchCenterOffsetY, watchHandWidth, watchHandHeight, gfxManager.get(TextureType.WATCH_HAND).second, getVertexBufferObjectManager());
            watchHand.setRotationCenter(0.5f, 0.12f);
            watchHand.setZIndex(WATCH_HAND_ZINDEX);
            watch.attachChild(watchHand);

            carousel = new Carousel(camera.getWidth(), camera.getHeight(), gfxManager.get(TextureType.CAROUSEL).second, getVertexBufferObjectManager());
            carousel.setZIndex(CAROUSEL_ZINDEX);
            scene.attachChild(carousel);

            float wave1Height = ScalingHelper.getHeightFromWidth(gfxManager.get(TextureType.WAVE_1).second.getWidth(), gfxManager.get(TextureType.WAVE_1).second.getHeight(), camera.getWidth());
            Sprite wave1 = new Sprite(camera.getWidth() / 2, getBaseHeight(), camera.getWidth(), wave1Height, gfxManager.get(TextureType.WAVE_1).second, getVertexBufferObjectManager());
            wave1.setZIndex(WAVE1_ZINDEX);
            scene.attachChild(wave1);

            float wave2Height = ScalingHelper.getHeightFromWidth(gfxManager.get(TextureType.WAVE_2).second.getWidth(), gfxManager.get(TextureType.WAVE_2).second.getHeight(), camera.getWidth());
            Sprite wave2 = new Sprite(camera.getWidth() / 2, getBaseHeight() + (camera.getHeight() * WAVE_VERTICAL_OFFSET), camera.getWidth(), wave2Height, gfxManager.get(TextureType.WAVE_2).second, getVertexBufferObjectManager());
            wave2.setZIndex(WAVE2_ZINDEX);
            scene.attachChild(wave2);

            float wave3Height = ScalingHelper.getHeightFromWidth(gfxManager.get(TextureType.WAVE_3).second.getWidth(), gfxManager.get(TextureType.WAVE_3).second.getHeight(), camera.getWidth());
            Sprite wave3 = new Sprite(camera.getWidth() / 2, getBaseHeight() + ((2 * camera.getHeight()) * WAVE_VERTICAL_OFFSET), camera.getWidth(), wave3Height, gfxManager.get(TextureType.WAVE_3).second, getVertexBufferObjectManager());
            wave3.setZIndex(WAVE3_ZINDEX);
            scene.attachChild(wave3);

            scoreText = new Text(camera.getWidth() - (camera.getWidth() / 10), -(camera.getHeight() / 15), mFont, "0", 1000, new TextOptions(HorizontalAlign.RIGHT), getVertexBufferObjectManager());
            scoreText.setZIndex(SCORE_ZINDEX);
            scene.attachChild(scoreText);

            baseInitDone = true;
        }

        targetGenerator = new TargetGenerator(gfxManager, getDifficulty());
        targets.addAll(targetGenerator.initWaves(getDifficulty(), getVertexBufferObjectManager()));
        scene.sortChildren();
        updateScrore();
    }

    private float getBaseHeight() {
        // return ((cameraHeight * WAVE_VERTICAL_OFFSET) - ((cameraHeight * (WAVE_VERTICAL_OFFSET / 2)) * getDifficulty().ordinal()));
        return ((cameraHeight * WAVE_VERTICAL_OFFSET) - ((cameraHeight * (WAVE_VERTICAL_OFFSET / 2)) * 2));
    }

    private TargetPositionUpdateHandler targetPositionUpdateHandler;

    public enum FlowerToggle {
        SHOW, HIDE;
    }

    private void toggleFlowers(List<Target> targets, FlowerToggle operation) {
        for (Target target : targets) {
            if ((target.getParent() != null) && !target.isShot()) {
                target.setScaleCenterY(0);
                target.registerEntityModifier(operation == FlowerToggle.SHOW ? new ScaleModifier(0.3f, 1, 1, 0, 1) : new ParallelEntityModifier(new ScaleModifier(0.3f, 1, 1, 1, 0), new MoveByModifier(0.3f, 0, -target.getHeight() / 2)));
            }
        }
    }

    private int currentHitLimit = 0;

    private int getNewHitLimit() {
        if (getDifficulty() == GameDifficulty.EASY) {
            // currentHitLimit = 3;
            return random.nextInt(4) + 1;
        } else if (getDifficulty() == GameDifficulty.MEDIUM) {
            // currentHitLimit = 2;
            return random.nextInt(4) + 1;
        } else if (getDifficulty() == GameDifficulty.HARD) {
            return random.nextInt(4) + 1;
        } else {
            throw new IllegalStateException("Undefined");
        }
    }

    @Override
    protected void showGame() {
        super.showGame();
        targetPositionUpdateHandler = new TargetPositionUpdateHandler(targets, camera.getWidth(), camera.getHeight(), getBaseHeight() + (camera.getHeight() / 3), scene, StrelniceActivity.this, targetGenerator);
        scene.registerUpdateHandler(new TimerHandler(1.5f, false, new ITimerCallback() {
            @Override
            public void onTimePassed(final TimerHandler pTimerHandler) {
                targetPositionUpdateHandler.onUpdate(0);
                toggleFlowers(targets, FlowerToggle.SHOW);
            }
        }));

        scene.registerUpdateHandler(new TimerHandler(2f, false, new ITimerCallback() {
            @Override
            public void onTimePassed(final TimerHandler pTimerHandler) {
                toggleEntity(watch);
                toggleEntity(scoreText);
                toggleCarousel();
                changeCurrentFlower();
                mfxManager.playSound(SoundType.BELL);
                scene.registerUpdateHandler(timeHandler = new GameOverUpdateHandler(GAME_TIME) {
                    @Override
                    public void onTimeStart() {
                        scene.registerUpdateHandler(targetPositionUpdateHandler);
                        scene.setOnSceneTouchListener(StrelniceActivity.this);
                    }

                    @Override
                    public void onTimeOver() {
                        toggleFlowers(targets, FlowerToggle.HIDE);
                        mfxManager.playSound(SoundType.BELL);
                        scene.unregisterUpdateHandler(targetPositionUpdateHandler); // přestaneme hýbat květinama
                        scene.setOnSceneTouchListener(null);
                        watchHand.setRotation(0);
                        toggleCarousel();
                        scene.registerUpdateHandler(new TimerHandler(1f, false, new ITimerCallback() {
                            @Override
                            public void onTimePassed(final TimerHandler pTimerHandler) {
                                watchHand.unregisterEntityModifiers(new IEntityModifierMatcher() {
                                    @Override
                                    public boolean matches(IModifier<IEntity> pItem) {
                                        return true;
                                    }
                                });
                                watchHand.setAlpha(1);
                                toggleEntity(watch);
                                toggleEntity(scoreText);
                                gameComplete();
                            }
                        }));

                    }

                    @Override
                    public void onTimeUpdate(float time) {
                        watchHand.setRotation((time * 360) / GAME_TIME);
                        if (time > (GAME_TIME - WARNING_TIME)) {
                            watchHand.startBlinking();
                        } else {
                            watchHand.stopBlinking();
                        }
                    }
                });
            }
        }));
    }

    private int currentTypeHits = 0;

    private void changeCurrentFlower() {
        mfxManager.playSound(SoundType.CAROUSEL_SOUNDS[random.nextInt(SoundType.CAROUSEL_SOUNDS.length)]);
        if (currentHitLimit == currentTypeHits) {
            currentHitLimit = getNewHitLimit();
            currentTypeHits = 0;
            Pair<TextureType, Sprite> oldTarget = currentTarget;
            do {
                currentTarget = getRandomFlower();
            } while ((oldTarget != null) && oldTarget.first.equals(currentTarget.first));
            carousel.nextFlower(currentTarget.second);
        }
        currentTypeHits++;
    }

    private void toggleCarousel() {
        mfxManager.playSound(SoundType.SWOOSH_IN);
        if (carousel.getY() == camera.getHeight()) {
            carousel.registerEntityModifier(new MoveModifier(0.2f, carousel.getX(), carousel.getY(), carousel.getX() + carousel.getWidth(), carousel.getY() + carousel.getHeight()));
        } else {
            carousel.registerEntityModifier(new MoveModifier(0.2f, carousel.getX(), carousel.getY(), camera.getWidth(), camera.getHeight()));
        }

    }

    private void toggleEntity(Entity sprite) {
        mfxManager.playSound(SoundType.SWOOSH_IN);
        sprite.registerEntityModifier(new MoveModifier(0.2f, sprite.getX(), sprite.getY(), sprite.getX(), -sprite.getY()));
    }

    private Pair<TextureType, Sprite> getRandomFlower() {
        TextureType flowerType = targetGenerator.getRandomFlowerType(getDifficulty());
        Sprite currentFlowerSprite = new Sprite(camera.getWidth() - gfxManager.get(flowerType).first.getWidth(), -gfxManager.get(flowerType).first.getHeight() / 2, gfxManager.get(flowerType).second, getVertexBufferObjectManager());
        currentFlowerSprite.setZIndex(CAROUSEL_ZINDEX);
        return new Pair<TextureType, Sprite>(flowerType, currentFlowerSprite);
    }

    @Override
    public CharSequence getVictoryText(int progress) {
        switch (progress) {
            case 0:
                return getString(R.string.game_strelnice_victory_0);
            case 1:
                return getString(R.string.game_strelnice_victory_1);
            case 2:
                return getString(R.string.game_strelnice_victory_2);
            case 3:
                return getString(R.string.game_strelnice_victory_3);
            default:
                throw new IllegalStateException("Unknown progress");
        }
    }

    @Override
    public String[] getVictorySpeech() {
        return new String[] { SpeechSounds.RESULT_STRELNICE_0, SpeechSounds.RESULT_STRELNICE_1, SpeechSounds.RESULT_STRELNICE_2, SpeechSounds.RESULT_STRELNICE_3 };
    }

    @Override
    public CharSequence getStatsText() {
        SpannableStringBuilder ssb = new SpannableStringBuilder("  ");
        Bitmap stats = BitmapFactory.decodeResource(getResources(), R.drawable.victoryscreen_stats);
        ssb.setSpan(new ImageSpan(this, stats), 0, 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        ssb.append(Html.fromHtml(String.format(getString(R.string.game_strelnice_stats_text), ((int) getGameManager().getCounter()))));
        return ssb;
    }

    @Override
    public int countProgress() {
        double score = getGameManager().getCounter();
        int[][] CUPS = { CUPS_EASY, CUPS_MEDIUM, CUPS_HARD };

        if (score > CUPS[getDifficulty().ordinal()][2]) {
            return 3;
        } else if (score > CUPS[getDifficulty().ordinal()][1]) {
            return 2;
        } else if (score > CUPS[getDifficulty().ordinal()][0]) {
            return 1;
        }
        return 0;
    }

    Random random = new Random();

    public class GameSpeedTimeHandler extends TimerHandler {
        public GameSpeedTimeHandler() {
            super(TargetPositionUpdateHandler.GAME_SPEED_TIMEOUT, new ITimerCallback() {
                @Override
                public void onTimePassed(TimerHandler pTimerHandler) {
                    targetPositionUpdateHandler.resetGameSpeed();
                    timeHandler.resetGameSpeed();
                }
            });
        }
    }

    private void setGameSpeed(float speed) {
        targetPositionUpdateHandler.setGameSpeed(speed);
        timeHandler.setGameSpeed(speed);
        runOnUpdateThread(new Runnable() {
            @Override
            public void run() {
                scene.unregisterUpdateHandlers(new IUpdateHandlerMatcher() {
                    @Override
                    public boolean matches(IUpdateHandler pItem) {
                        return pItem instanceof GameSpeedTimeHandler;
                    }
                });
                scene.registerUpdateHandler(new GameSpeedTimeHandler());
            }
        });
    }

    private void showEffect(Target target, TextureType effect) {
        Effect effectSprite = new Effect(target.getWave(), target.getX(), target.getY(), gfxManager.get(effect).second.getWidth(), gfxManager.get(effect).second.getHeight(), gfxManager.get(effect).second, getVertexBufferObjectManager());
        effectSprite.setOnEffectoverListener(new OnEffectOverListener() {
            @Override
            public void onEffectOver(final Effect effectInstace) {
                runOnUpdateThread(new Runnable() {
                    @Override
                    public void run() {
                        effectInstace.detachSelf();
                    }
                });

            }
        });
        target.setEffect(effectSprite);
        scene.attachChild(effectSprite);
        scene.sortChildren();
    }

    private void showCloud(final float x, final float y, final int number) {
        Cloud effectSprite = new Cloud(number, x, y, gfxManager.get(Cloud.TEXTURES[number]).second.getWidth() * 2, gfxManager.get(Cloud.TEXTURES[number]).second.getHeight() * 2, gfxManager.get(Cloud.TEXTURES[number]).second, getVertexBufferObjectManager());
        effectSprite.setOnCloudOverListener(new OnCloudOverListener() {
            @Override
            public void onEffectOver(final Cloud effectInstace) {
                runOnUpdateThread(new Runnable() {
                    @Override
                    public void run() {
                        if (effectInstace.getCloudIndex() < (Cloud.TEXTURES.length - 1)) {
                            showCloud(x, y, number + 1);
                        }
                        effectInstace.detachSelf();
                    }
                });

            }
        });
        effectSprite.setZIndex(CLOUD_ZINDEX);
        scene.attachChild(effectSprite);
        scene.sortChildren();
    }

    /**
     * Pokud se nedotkneme zadne touchArea, tak jako fallback se dotkneme sceny, coz znamena, ze jsme nic netrefili.
     *
     * @param pScene
     * @param pSceneTouchEvent
     * @return
     */
    @Override
    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
        if (pSceneTouchEvent.isActionDown()) {
            new PointsMoveTask().execute(-1, 0);
            mfxManager.playSound(SoundType.HIT_OUT);
            new PointsMoveTask().execute(2, 1);
        }
        return false;
    }

    @Override
    public void touched(Target target) {
        if (!target.isShot()) {
            if (currentTarget != null) {
                if (target.getTextureType().equals(TextureType.BOX_GOOD)) {
                    switch (BoxType.GOOD_BOXES[random.nextInt(BoxType.GOOD_BOXES.length)]) {
                        case SLOW_DOWN:
                            setGameSpeed(TargetPositionUpdateHandler.GAME_SPEED_SLOW);
                            showEffect(target, TextureType.BOX_SPEED_DOWN);
                            mfxManager.playSound(SoundType.HIT_BOX_SLOW);
                            hideTarget(target);
                            break;
                        case TIME_SUB:
                            timeHandler.substractTime(GameOverUpdateHandler.SUB_TIME);
                            mfxManager.playSound(SoundType.BELL);
                            showEffect(target, TextureType.BOX_TIME_PLUS_5);
                            hideTarget(target);
                            break;
                        case HIT:
                            showEffect(target, TextureType.BOX_HIT);
                            mfxManager.playSound(SoundType.HIT_GOOD_SOUNDS[random.nextInt(SoundType.HIT_GOOD_SOUNDS.length)]);
                            hitTarget(target);
                            break;
                        default:
                            throw new IllegalStateException("Unknown state");
                    }
                    new PointsMoveTask().execute(4, 1);
                } else if (target.getTextureType().equals(TextureType.BOX_BAD)) {
                    switch (BoxType.BAD_BOXES[random.nextInt(BoxType.BAD_BOXES.length)]) {
                        case SPEED_UP:
                            setGameSpeed(TargetPositionUpdateHandler.GAME_SPEED_FAST);
                            mfxManager.playSound(SoundType.HIT_BOX_FAST);
                            showEffect(target, TextureType.BOX_SPEED_UP);
                            hideTarget(target);
                            break;
                        case TIME_ADD:
                            timeHandler.addTime(GameOverUpdateHandler.ADD_TIME);
                            mfxManager.playSound(SoundType.BELL);
                            showEffect(target, TextureType.BOX_TIME_MINUS_5);
                            hideTarget(target);
                            break;
                        case CLOUD:
                            showCloud(target.getX(), target.getY() + 100, 0); // mraky lehce zvedneme
                            mfxManager.playSound(SoundType.HIT_BOX_SMOKE);
                            hideTarget(target);
                            break;
                        default:
                            throw new IllegalStateException("Unknown state");
                    }
                    new PointsMoveTask().execute(4, 1);
                } else if (currentTarget.first.equals(target.getTextureType())) {
                    hitTarget(target);
                    mfxManager.playSound(SoundType.HIT_GOOD_SOUNDS[random.nextInt(SoundType.HIT_GOOD_SOUNDS.length)]);
                    new PointsMoveTask().execute(1, 1);
                } else {
                    target.blink();
                    new PointsMoveTask().execute(-1, 0);
                    mfxManager.playSound(SoundType.HIT_ERROR);
                    new PointsMoveTask().execute(1, 2);
                }
            } else {
                Log.e(TAG, "Current flower empty");
            }
        }
    }

    private void hitTarget(Target target) {
        new PointsMoveTask().execute(2, 0);
        hideTarget(target);
        changeCurrentFlower();
    }

    private void hideTarget(Target target) {
        target.registerEntityModifier(new ScaleModifier(0.1f, 1, 0, 1, 1));
        target.setShot(true);
    }

    private void updateScrore() {
        scoreText.setText(String.valueOf(Double.valueOf(getGameManager().getCounter()).intValue()));
    }

    @Override
    protected void resetGame() {

        final List<Target> oldFlowers = targets;
        targets = new ArrayList<Target>();
        runOnUpdateThread(new Runnable() {

            @Override
            public void run() {
                for (Target flower : oldFlowers) {
                    flower.detachSelf();
                    scene.unregisterTouchArea(flower);
                }
            }
        });
        gameManager = null;
    }

    public class PointsMoveTask extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... params) {
            switch (params[1]) {
                case 0:
                    getGameManager().setCounterAndSave(getGameManager().getCounter() + params[0]);
                    break;
                case 1:
                    getGameManager().setExtraInt1AndSave(getGameManager().getExtraInt1() + params[0]);
                    break;
                case 2:
                    getGameManager().setExtraInt2AndSave(getGameManager().getExtraInt2() + params[0]);
                    break;
                case 3:
                    getGameManager().setExtraInt3AndSave(getGameManager().getExtraInt3() + params[0]);
                    break;
                case 4:
                    getGameManager().setExtraInt4AndSave(getGameManager().getExtraInt4() + params[0]);
                    break;

                default:
                    break;
            }

            updateScrore();
            return null;
        }

    }

}
