/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.unos.model;

import java.io.Serializable;

/**
 * Pozice dlaždice na mapě. Používá převod X-Y souřadnic do isomapy
 * 
 * @author Luboš Horáček
 */
public class Position implements Serializable, Comparable<Position> {

    private static final long serialVersionUID = 8289536053605298012L;

    private final Integer     x;
    private final Integer     y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Position getNorth() {
        return new Position(x, y - 1);
    }

    public Position getSouth() {
        return new Position(x, y + 1);
    }

    public Position getWest() {
        return new Position(x - 2, y);
    }

    public Position getEast() {
        return new Position(x + 2, y);
    }

    public Position getNorthWest() {
        if ((x % 2) == 0) {
            return new Position(x - 1, y);
        }
        return new Position(x - 1, y - 1);
    }

    public Position getNorthEast() {
        if ((x % 2) == 0) {
            return new Position(x + 1, y);
        }
        return new Position(x + 1, y - 1);
    }

    public Position getSouthWest() {
        if ((x % 2) == 0) {
            return new Position(x - 1, y + 1);
        }
        return new Position(x - 1, y);
    }

    public Position getSouthEast() {
        if ((x % 2) == 0) {
            return new Position(x + 1, y + 1);
        }
        return new Position(x + 1, y);
    }

    /**
     * Pozice se rovná, pokud má stejnou x a y souřadnici
     */
    @Override
    public boolean equals(Object o) {
        return (o != null) && (o instanceof Position) && ((getX() == ((Position) o).getX()) && (getY() == ((Position) o).getY()));
    }

    @Override
    public int hashCode() {
        return Math.abs(getX());
    }

    @Override
    public String toString() {
        return "[" + getX() + ":" + getY() + "]";
    }

    /**
     * Řazení dlaždic po řádcích
     * Seřazeni primárně podle y-nové souřadnice, nejdříve liché x-ové v ordinálním pořadí a poté sudé
     * Zaručuje vykreslení iso mapy se správným pořadím z-indexů
     * Není možné dokreslovat dlaždice zpětně, poté je nutné opravit všechny zIndexy
     */
    @Override
    public int compareTo(Position o) {
        if (o == null) {
            return 1;
        }
        return (y - o.y) != 0 ? y - o.y : (((x % 2) - (o.x % 2)) != 0) ? ((x % 2) == 0 ? 1 : -1) : x - o.x;
    }

}
