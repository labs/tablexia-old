/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.pronasledovani;

import cz.nic.tablexia.game.games.pronasledovani.model.Position;

public class PronasledovaniHelper {

    /*
     * Prepocet souradnic na mape podle natoceni slozene mapy
     * (x'-xc) = Kc*(x-xc) - Ks*(y-yc)
     * (y'-yc) = Ks*(x-xc) + Kc*(y-yc)
     */
    public static Position translatePositionAccordingToRotation(Position zeroRotationPosition, float rotation, Position rotationCenter) {
        rotation = rotation % 360;
        rotation = 360 - rotation;
        rotation = (float) Math.toRadians(rotation);

        int x = (int) (Math.cos(rotation) * (zeroRotationPosition.getX() - rotationCenter.getX())) - (int) (Math.sin(rotation) * (zeroRotationPosition.getY() - rotationCenter.getY()));
        x += rotationCenter.getX();
        int y = (int) (Math.sin(rotation) * (zeroRotationPosition.getX() - rotationCenter.getX())) + (int) (Math.cos(rotation) * (zeroRotationPosition.getY() - rotationCenter.getY()));
        y += rotationCenter.getY();

        Position toReturn = new Position(x, y);
        return toReturn;
    }

}
