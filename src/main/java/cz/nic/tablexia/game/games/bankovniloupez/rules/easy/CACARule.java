/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.rules.easy;

import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.Context;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute.AttributeColor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.ClothingAttribute;

/**
 * 
 * @author Matyáš Latner
 */
public class CACARule extends CCCCRule {

    public CACARule(Context context, RandomAccess randomAcces, VertexBufferObjectManager vertexBufferObject, int numberOfCreatures, int numberOfThieves) {
        super(context, randomAcces, vertexBufferObject, numberOfCreatures, numberOfThieves);
    }
    
    @Override
    public String[] prepareRuleMessageParameters() {
		return new String[] {
				getAttributeColorName(getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0)),
        		getAttributeColorName(getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(1))
        };
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
    	CreatureDescriptor t0CreatureDescriptor = new CreatureDescriptor();    	
        AttributeColor t0Attribute1Color = getRandomColorFromGeneratedCreature(BAN_ATTRIBUTES_SET_FOR_GENERATING, vertexBufferObject);
        
        for (int i = 0; i < GENERATOR_TRY_COUNT; i++) {
        	AttributeColor t0Attribute2Color = getRandomColorFromGeneratedCreature(BAN_ATTRIBUTES_SET_FOR_GENERATING, vertexBufferObject);
        	if (t0Attribute1Color != t0Attribute2Color) {
        		t0CreatureDescriptor.addDescription(new AttributeDescription(t0Attribute1Color, null, ClothingAttribute.class));
        		t0CreatureDescriptor.addDescription(new AttributeDescription(t0Attribute2Color, null, ClothingAttribute.class));
        		addGlobalCreatureDescriptor(T0_OFFSET, t0CreatureDescriptor);
        		return;
        	}
		}
        
        throw new IllegalStateException("Cannot select different color for creature description!");
    }

}
