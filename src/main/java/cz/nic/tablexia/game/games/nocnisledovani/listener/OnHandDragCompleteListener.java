
package cz.nic.tablexia.game.games.nocnisledovani.listener;

/*
 * listens to the end of setting time
 */
public interface OnHandDragCompleteListener {
    public void onHandDragComplete();
}
