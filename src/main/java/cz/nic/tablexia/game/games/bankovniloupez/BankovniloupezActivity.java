/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.bankovniloupez;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.andengine.audio.sound.Sound;
import org.andengine.engine.Engine;
import org.andengine.engine.FixedStepEngine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.FadeInModifier;
import org.andengine.entity.modifier.FadeOutModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.util.GLState;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.align.HorizontalAlign;
import org.andengine.util.adt.color.Color;
import org.andengine.util.modifier.IModifier;
import org.andengine.util.modifier.IModifier.IModifierListener;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import cz.nic.tablexia.R;
import cz.nic.tablexia.audio.resources.SpeechSounds;
import cz.nic.tablexia.game.GameActivity;
import cz.nic.tablexia.game.common.EntityModifierListenerAdapter;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.GamesDefinition;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureFactory;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureRoot;
import cz.nic.tablexia.game.games.bankovniloupez.rules.GameRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.GameRulesDefinition;
import cz.nic.tablexia.menu.usermenu.User;

/**
 * Activity for "Bankovni loupez" game
 * 
 * @author Matyáš Latner
 */
public class BankovniloupezActivity extends GameActivity {

    // TODO vyresit -> zvuky: kroky

    // TODO otevrim menu jiz pri zapauzovani

    // TODO pokud se zalistuje aplikace s otevrenym dialogem tak spadne

    // TODO zapauzovat hru jiz pri kratkem vytazeni menu
    // TODO po restorovani hry nejde pouzit tlacitko vybrat novou hru

    private static class DitherSprite extends Sprite {

        public DitherSprite(float pX, float pY, float pWidth, float pHeight, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
            super(pX, pY, pWidth, pHeight, pTextureRegion, pVertexBufferObjectManager);
        }

        @Override
        protected void preDraw(GLState pGLState, Camera pCamera) {
            super.preDraw(pGLState, pCamera);
            pGLState.enableDither();
        }

    }

    private static final String           GAMERULE_KEY                 = "gameRule";
    private static final String           CREATURES_KEY                = "creatures";
    private static final String           GAMESTATE_KEY                = "gamestate";
    private static final String           CREATURENUMBER_KEY           = "creaturenumber";

    public static final boolean           SHOW_DEBUGINFO_CODE          = false;
    public static final boolean           SHOW_DEBUGINFO_RULE          = false;
    public static final boolean           DEBUG                        = false;

    private final static int              NUMBER_OF_CREATURES          = 50;
    private final static int              NUMBER_OF_THIEVES            = 8;
    private static final float            CREATURE_STRAT_MOVE_DURATION = 1.5f;
    private static final float            CREATURE_MOVE_DURATION_STEP  = 0.013f;

    private static final Color            BACKGROUND_COLOR             = new Color(0.46f, 0.42f, 0.40f);
    private static final Color            ARRESTED_BACKGROUND_COLOR    = new Color(0.22f, 0.71f, 0.29f);
    private static final Color            INNOCENCE_BACKGROUND_COLOR   = new Color(0.76f, 0.15f, 0.18f);

    private static final float            VICTORY_SCREEN_DELAY         = 1f;
    private static final float            INFOITEM_FADEIN_DURATION     = 0.2f;
    private static final float            INFOITEM_FADEOUT_DURATION    = 0.1f;
    private static final float            BACKGROUND_FADEIN_DURATION   = 0.1f;

    private static final int              STEPS_PER_SECOUND            = 60;

    private static final int              REFERENCE_CAMERA_HEIGHT      = 800;

    private GameRulesDefinition           rule;
    private GameRule                      gameRule;
    private List<CreatureRoot>            creatures;
    private Integer                       creatureNumber;

    private Entity                        infoItem;
    private Sprite                        alarm;
    private Sprite                        shackles;
    private Sprite                        error;

    private ArrayList<CreatureDescriptor> creatureDescriptors;

    private GameState                     gameState;
    private Entity                        creaturesLayer;
    private Entity                        backgroundLayer;
    private Entity                        middleLayer;
    private Rectangle                     redBackground;
    private Rectangle                     greenBackground;
    private Entity                        actualBackground;
    private Text                          scoreText;

    private RandomAccess                  randomAccess;

    private static final float            CREATURE_FROM_SCALE          = 0.1f;
    private static final float            CREATURE_TO_SCALE            = 1.05f;
    private static final float            CREATURE_FINAL_SCALE         = 0.9f;
    private float                         creatureStartPositionX;
    private float                         creatureStartPositionY;
    private float                         creatureMiddlePositionX;
    private float                         creatureMiddlePositionY;
    private float                         creatureFinishPositionX;
    private float                         creatureFinishPositionY;
    private float                         scorePositionX;
    private float                         scorePositionY;

    private Sound                         alarmSound;
    private Sound                         shacklesSound;
    private Sound                         errorSound;

    private enum GameState {
        RuleScreen, GameScreen, VictoryScreen
    }

    private enum DebugGroupColors {
        RED(Color.RED), GREEN(Color.GREEN), BLUE(Color.BLUE), YELLOW(Color.YELLOW), WHITE(Color.WHITE), PINK(Color.PINK), CYAN(Color.CYAN), LIGHTBLUE(new Color(0.569f, 0.612f, 0.976f)), LIGHTRED(new Color(0.976f, 0.569f, 0.569f)), LIGHTGREEN(new Color(0.569f, 0.976f, 0.631f)), LIGHTPURPLE(new Color(0.969f, 0.569f, 0.976f));

        Color color;

        private DebugGroupColors(Color color) {
            this.color = color;
        }

        public Color getColor() {
            return color;
        }
    }

    public BankovniloupezActivity() {
        super(GamesDefinition.BANKOVNILOUPEZ);
    }

    /* //////////////////////////////////////////// TABLEXIA LIFECYCLE */

    @Override
    public Camera onCreateCamera() {
        float scale = (float) displaySize.y / (float) displaySize.x;
        int cameraWidth = (int) (REFERENCE_CAMERA_HEIGHT / scale);
        return new Camera(0, 0, cameraWidth, REFERENCE_CAMERA_HEIGHT);
    }

    @Override
    protected void onCreateTablexiaScene() {
        scene.setBackground(new Background(BACKGROUND_COLOR));
    }

    @Override
    protected void showGame() {
        if (gameState == GameState.GameScreen) {
            startGame();
        } else {
            showRuleScreen();
        }
    }

    @Override
    protected void initGame() {
        super.initGame();
        loadSounds();
        loadVariables();
        loadRule();
        loadCreatures();
        loadGameScreen();
    }

    @Override
    protected void resetGame() {
        super.resetGame();
        creatureNumber = null;
        rule = null;
        gameRule = null;
        creatures = null;
        creatureDescriptors = null;
        creaturesLayer = null;
        getGamePanelLayout().removeAllViews();
    }

    @Override
    protected void gameComplete() {
        gameState = GameState.VictoryScreen;
        getGameManager().setCounterAndSave(creatureNumber - getNumberOfMistakesInGame());
        super.gameComplete();
    }

    @Override
    public void onBtnReplayClick() {
        getTablexiaContext().hideScreenContent(false, new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                BankovniloupezActivity.super.hideVictoryScreen(0);
                BankovniloupezActivity.super.onBtnReplayClick();
            }
        });
    }

    @Override
    protected void hideVictoryScreen() {
        // nothing needed
    }

    /* //////////////////////////////////////////// ANDENGINE LIFECYCLE */

    @Override
    public Engine onCreateEngine(EngineOptions pEngineOptions) {
        pEngineOptions.getAudioOptions().setNeedsSound(true);
        pEngineOptions.getAudioOptions().setNeedsMusic(true);
        return new FixedStepEngine(pEngineOptions, STEPS_PER_SECOUND);
    }

    @Override
    public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws IOException {
        ResourceManager.getInstance().loadTexturesAndSounds(mEngine, getContext(), getDifficulty());
        //setGameMusic(ResourceManager.getInstance().getMusic(ResourceManager.AMBIENT_MUSIC_ASSET));
        pOnCreateResourcesCallback.onCreateResourcesFinished();
    }

    /* //////////////////////////////////////////// ANDROID LIFECYCLE */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        creatureNumber = null;
        if (savedInstanceState != null) {
            creatureDescriptors = savedInstanceState.getParcelableArrayList(CREATURES_KEY);
            rule = GameRulesDefinition.values()[savedInstanceState.getInt(GAMERULE_KEY)];
            creatureNumber = Integer.valueOf(savedInstanceState.getInt(CREATURENUMBER_KEY));
            gameState = GameState.values()[savedInstanceState.getInt(GAMESTATE_KEY)];
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (gameState == GameState.GameScreen) {
            ArrayList<CreatureDescriptor> creatureDescriptors = new ArrayList<CreatureDescriptor>();
            for (CreatureRoot creature : creatures) {
                creatureDescriptors.add(creature.getCreatureDescrition());
            }
            outState.putParcelableArrayList(CREATURES_KEY, creatureDescriptors);
            outState.putInt(GAMERULE_KEY, rule.ordinal());
            outState.putInt(GAMESTATE_KEY, gameState.ordinal());
            outState.putInt(CREATURENUMBER_KEY, creatureNumber != null ? creatureNumber.intValue() : null);
        }
    }

    /* //////////////////////////////////////////// GAME CONTROL */

    /**
     * Start game
     */
    private void startGame() {
        gameState = GameState.GameScreen;
        if (!DEBUG) {
            prepareCreaturePositions();
            showNextCreature();
        }
    }

    private void prepareCreaturePositions() {
        creatureStartPositionX = camera.getWidth();
        creatureStartPositionY = ((camera.getHeight() / 5) * 2) + (((camera.getHeight() / 5) * 2) * 0.25f);
        creatureMiddlePositionX = (camera.getWidth() / 3) * 2;
        creatureMiddlePositionY = ((camera.getHeight() / 5) * 2);
        creatureFinishPositionX = camera.getWidth() / 2;
        creatureFinishPositionY = creatureStartPositionY;
    }

    /**
     * Load rule
     */
    private void loadRule() {
        if (rule == null) {
            rule = GameRulesDefinition.getRandomGameRuleForDifficulty(getDifficulty(), new Random(System.currentTimeMillis()));
        }
        randomAccess = new RandomAccess();
        getGameManager().setExtraInt4AndSave(rule.ordinal());
        gameRule = rule.getGameRuleInstance(getApplicationContext(), randomAccess, getVertexBufferObjectManager(), NUMBER_OF_CREATURES, NUMBER_OF_THIEVES);
    }

    /**
     * Load creatures
     */
    private void loadCreatures() {
        if ((creatureDescriptors != null) && (creatureDescriptors.size() > 0)) {
            creatures = new ArrayList<CreatureRoot>();
            for (CreatureDescriptor creatureDescriptor : creatureDescriptors) {
                creatures.add(CreatureFactory.getInstance().getCreatureWithAttributes(creatureDescriptor, null, true, getVertexBufferObjectManager(), randomAccess));
            }
        } else {
            creatures = gameRule.generateCreatures();
        }
    }

    /**
     * Load sounds
     */
    private void loadSounds() {
        alarmSound = ResourceManager.getInstance().getSound(ResourceManager.ALARM_SOUND_ASSET);
        shacklesSound = ResourceManager.getInstance().getSound(ResourceManager.SHACKLES_SOUND_ASSET);
        errorSound = ResourceManager.getInstance().getSound(ResourceManager.ERROR_SOUND_ASSET);
    }

    /**
     * Load game variables
     */
    private void loadVariables() {
        if (creatureNumber == null) {
            creatureNumber = Integer.valueOf(0);
        }
        infoItem = null;
        actualBackground = null;
    }

    /**
     * Shows game scene
     */
    private void loadGameScreen() {
        if (SHOW_DEBUGINFO_CODE) {
            showDebugInfo();
        }
        if (DEBUG) {
            displayAllCreatures(creatures);
        } else {
            backgroundLayer = new Entity();
            middleLayer = new Entity();
            creaturesLayer = new Entity();
            Entity gameScreenLayer = new Entity();
            scene.attachChild(backgroundLayer);
            scene.attachChild(middleLayer);
            scene.attachChild(creaturesLayer);
            scene.attachChild(gameScreenLayer);

            showGameLayer(gameScreenLayer);
            loadInfoItems();
        }
    }

    /**
     * Shows next creature and enable animating
     */
    private void showNextCreature() {
        if ((creatureNumber < creatures.size()) && (getNumberOfMistakesInGame() < 3)) {
            final CreatureRoot creature = creatures.get(creatureNumber);
            creature.setClickable(scene, this, getVertexBufferObjectManager());
            creature.setPosition(creatureStartPositionX, creatureStartPositionY);

            creature.detachSelf();
            creaturesLayer.attachChild(creature);

            IModifierListener<IEntity> firstAnimListener = new EntityModifierListenerAdapter() {
                @Override
                public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                    hideAlarmInfoItem();
                }
            };
            IModifierListener<IEntity> secoundAnimListener = new EntityModifierListenerAdapter() {
                @Override
                public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                    if (creature.isThief() && !creature.isRevealed()) {
                        showAlarm();
                        getGameManager().setExtraInt2AndSave(getGameManager().getExtraInt2() + 1);
                    }
                    hideBackground();
                    hideNotAlarmInfoItem();
                    creatureNumber++;
                    showNextCreature();

                    scoreText.setText(String.valueOf(creatureNumber - getNumberOfMistakesInGame()));
                }
            };
            IModifierListener<IEntity> thirdAnimListener = new EntityModifierListenerAdapter() {
                @Override
                public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                    detachEntityFromLayer(creaturesLayer, creature);
                }
            };

            creature.enableMove(getCreatureMoveDurationForCreatureNumber(creatureNumber), creatureStartPositionX, creatureStartPositionY, creatureMiddlePositionX, creatureMiddlePositionY, creatureFinishPositionX, creatureFinishPositionY, CREATURE_FROM_SCALE, CREATURE_TO_SCALE, CREATURE_FINAL_SCALE, firstAnimListener, secoundAnimListener, thirdAnimListener);
        } else {
            scene.registerEntityModifier(new DelayModifier(VICTORY_SCREEN_DELAY) {

                @Override
                protected void onModifierFinished(IEntity pItem) {
                    gameComplete();
                }

            });
        }
    }

    /**
     * Return move duration for creature with specific number
     * 
     * @param creatureNumber number of creature
     * @return move duration of creature with number from parameter
     */
    private float getCreatureMoveDurationForCreatureNumber(int creatureNumber) {
        return CREATURE_STRAT_MOVE_DURATION - (CREATURE_MOVE_DURATION_STEP * creatureNumber);
    }

    /**
     * Detach entity from another entity in update thread
     * 
     * @param layer entity to detach from
     * @param entity entity to detach
     */
    private void detachEntityFromLayer(final Entity layer, final Entity entity) {
        // child has to be detached from update thread
        runOnUpdateThread(new Runnable() {

            @Override
            public void run() {
                layer.detachChild(entity);
            }
        });
    }

    /**
     * Show alarm sprite and play alarm sound
     */
    private void showAlarm() {
        showAlarmInfoItem();
        alarmSound.play();
    }

    /**
     * Show shackles sprite and play chain sound
     */
    public void showShackles() {
        showInfoItem(shackles);
        showBackground(greenBackground);
        shacklesSound.play();
    }

    /**
     * Show error sprite and play error sound
     */
    public void showError() {
        showInfoItem(error);
        showBackground(redBackground);
        errorSound.play();
    }

    /**
     * Initialize all info items sprites
     */
    private void loadInfoItems() {

        redBackground = createBackgroundRectangle(INNOCENCE_BACKGROUND_COLOR);
        backgroundLayer.attachChild(redBackground);
        redBackground.setVisible(false);

        greenBackground = createBackgroundRectangle(ARRESTED_BACKGROUND_COLOR);
        backgroundLayer.attachChild(greenBackground);
        greenBackground.setVisible(false);

        float infoItemPositionX = 0f;
        float infoItemPositionY = 0f;
        float infoItemScale = 1f;

        switch (getDifficulty()) {

            case EASY:
                infoItemPositionX = (camera.getWidth() / 3) - (camera.getWidth() / 40);
                infoItemPositionY = (camera.getHeight() / 2) + (camera.getHeight() / 20);
                infoItemScale = 0.7f;
                break;

            case MEDIUM:
                infoItemPositionX = (camera.getWidth() / 3) + (camera.getWidth() / 30);
                infoItemPositionY = (camera.getHeight() / 2) + (camera.getHeight() / 20);
                infoItemScale = 0.7f;
                break;

            case HARD:
                infoItemPositionX = (camera.getWidth() / 3) - (camera.getWidth() / 80);
                infoItemPositionY = (camera.getHeight() / 2) + (camera.getHeight() / 10);
                infoItemScale = 0.6f;
                break;
        }

        alarm = addInfoItemToScene(ResourceManager.THIEF_ASSET, infoItemPositionX, infoItemPositionY, infoItemScale);
        shackles = addInfoItemToScene(ResourceManager.ARRESTED_ASSET, infoItemPositionX, infoItemPositionY, infoItemScale);
        error = addInfoItemToScene(ResourceManager.INNOCENCE_ASSET, infoItemPositionX, infoItemPositionY, infoItemScale);

        scorePositionX = (camera.getWidth() / 6) * 5;
        scorePositionY = (camera.getHeight() / 4) * 1;
        scoreText = new Text(scorePositionX, scorePositionY, ResourceManager.getInstance().getFont(), " ", 3, new TextOptions(HorizontalAlign.RIGHT), getVertexBufferObjectManager());

        scene.attachChild(scoreText);
    }

    /**
     * Create new info item and add it to scene
     * 
     * @param infoItemPositionX x position
     * @param infoItemPositionY y position
     * @param infoItemScale scale of info item
     * @return prepared info item
     */
    private Sprite addInfoItemToScene(String assetName, float infoItemPositionX, float infoItemPositionY, float infoItemScale) {
        Sprite infoItem = new Sprite(infoItemPositionX, infoItemPositionY, ResourceManager.getInstance().getTexure(assetName), getVertexBufferObjectManager());
        infoItem.setScale(infoItemScale);
        scene.attachChild(infoItem);
        infoItem.setVisible(false);
        return infoItem;
    }

    /**
     * Show info item
     */
    private void showAlarmInfoItem() {
        if (infoItem != null) {
            hideInfoItem();
        }

        infoItem = alarm;
        alarm.setVisible(true);
        alarm.registerEntityModifier(new FadeInModifier(INFOITEM_FADEIN_DURATION));
    }

    /**
     * Show info item for time defined in parameter
     */
    private void showInfoItem(final Sprite itemToShow) {
        if (infoItem != null) {
            hideInfoItem();
        }

        infoItem = itemToShow;
        itemToShow.setVisible(true);
        itemToShow.registerEntityModifier(new FadeInModifier(INFOITEM_FADEIN_DURATION));
    }

    /**
     * Show background entity
     */
    private void showBackground(Entity background) {
        actualBackground = background;
        background.setVisible(true);
        background.registerEntityModifier(new FadeInModifier(BACKGROUND_FADEIN_DURATION));
    }

    /**
     * Hide actual background entity
     */
    private void hideBackground() {
        if (actualBackground != null) {
            fadeOutEntity(actualBackground);
            actualBackground = null;
        }
    }

    /**
     * Hide alarm info item
     */
    private void hideAlarmInfoItem() {
        if ((infoItem != null) && (infoItem == alarm)) {
            fadeOutInfoItem();
        }
    }

    /**
     * Hide actual info item which is not the alarm
     */
    private void hideNotAlarmInfoItem() {
        if ((infoItem != null) && (infoItem != alarm)) {
            fadeOutInfoItem();
        }
    }

    /**
     * Hide actual info item with fade-out animation
     */
    private void fadeOutInfoItem() {
        fadeOutEntity(infoItem);
        infoItem = null;
    }

    private void fadeOutEntity(final Entity entity) {
        FadeOutModifier fadeOutModifier = new FadeOutModifier(INFOITEM_FADEOUT_DURATION);
        fadeOutModifier.setAutoUnregisterWhenFinished(true);
        fadeOutModifier.addModifierListener(new EntityModifierListenerAdapter() {
            @Override
            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                entity.setVisible(false);
            }
        });
        entity.registerEntityModifier(fadeOutModifier);
    }

    /**
     * Hide actual info item
     */
    private void hideInfoItem() {
        if (infoItem != null) {
            infoItem.setVisible(false);
            infoItem = null;
        }
    }

    /**
     * Shows game screen
     */
    private void showGameLayer(Entity frontLayer) {

        float width = camera.getWidth();
        float height = camera.getHeight();
        float halfWidth = width / 2;
        float halfHeight = height / 2;

        Sprite background = new Sprite(halfWidth, halfHeight, width, height, ResourceManager.getInstance().getTexure(ResourceManager.BACKGROUND), getVertexBufferObjectManager());
        Sprite backgroundBottom = new Sprite(halfWidth, halfHeight, width, height, ResourceManager.getInstance().getTexure(ResourceManager.BACKGROUND_BOTTOM), getVertexBufferObjectManager());
        DitherSprite backgroundDoorBackground = new DitherSprite(halfWidth, halfHeight, width, height, ResourceManager.getInstance().getTexure(ResourceManager.BACKGROUND_DOORBACKGROUND), getVertexBufferObjectManager());

        backgroundLayer.attachChild(backgroundDoorBackground);
        middleLayer.attachChild(backgroundBottom);
        frontLayer.attachChild(background);
    }

    /**
     * Create background rectangle with specific color
     * 
     * @param color color of rectangle
     * @return rectangle with color from parameter
     */
    private Rectangle createBackgroundRectangle(Color color) {
        Rectangle rectangle = new Rectangle((camera.getWidth() / 4) * 3, camera.getHeight() / 2, camera.getWidth() / 2, camera.getHeight(), getVertexBufferObjectManager());
        rectangle.setColor(color);
        return rectangle;
    }

    /**
     * Create and shows debug info
     */
    @SuppressWarnings("unused")
    private void showDebugInfo() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View debugInfoLayout = inflater.inflate(R.layout.game_common_debuginfo, null);
        ((TextView) debugInfoLayout.findViewById(R.id.game_common_debuginfo_code)).setText(rule.name());
        ((TextView) debugInfoLayout.findViewById(R.id.game_common_debuginfo_seed)).setText("" + randomAccess.getRandomSeed());
        String ruleText = rule.getRuleText(getApplicationContext());
        Object[] messageParameters = gameRule.getRuleMessageParameters();
        if (SHOW_DEBUGINFO_RULE && (messageParameters != null) && (messageParameters.length > 0)) {
            ruleText = String.format(rule.getRuleText(getApplicationContext()), messageParameters);
            ((TextView) debugInfoLayout.findViewById(R.id.game_common_debuginfo_message)).setText(getResources().getText(R.string.game_common_debuginfo_separatortext2) + " " + Html.fromHtml(ruleText));
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getGamePanelLayout().addView(debugInfoLayout);
            }
        });

    }

    /**
     * Create and shows screen with rule text
     */
    private void showRuleScreen() {
        String ruleText = rule.getRuleText(getApplicationContext());
        Object[] messageParameters = gameRule.getRuleMessageParameters();

        if ((messageParameters != null) && (messageParameters.length > 0)) {
            ruleText = String.format(rule.getRuleText(getApplicationContext()), messageParameters);
        }

        final String finalRuleText = ruleText;

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View ruleMessageLayout = inflater.inflate(R.layout.game_bankovniloupez_rulemessage, null);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((TextView) ruleMessageLayout.findViewById(R.id.game_bankovniloupez_rulemessage_text)).setText(Html.fromHtml(finalRuleText));
                getGamePanelLayout().addView(ruleMessageLayout);
                gameState = GameState.RuleScreen;
            }
        });

        ((TextView) ruleMessageLayout.findViewById(R.id.game_bankovniloupez_rulemessage_button)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                getTablexiaContext().hideScreenContent(false, new AnimatorListenerAdapter() {

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        callSuperShowGame();
                        getGamePanelLayout().removeView(ruleMessageLayout);
                        getTablexiaContext().showScreenContent(new AnimatorListenerAdapter() {

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                startGame();
                            }

                        });
                    }

                });
            }

        });
    }

    private void callSuperShowGame() {
        super.showGame();
    }

    /**
     * Show all creatures on the screen at once. Only for debugging.
     * 
     * @param creatures list of creatures to show
     */
    private void displayAllCreatures(List<CreatureRoot> creatures) {
        float positionX = 80;
        float positionY = 500;
        for (int i = 0; i < creatures.size(); i++) {
            CreatureRoot creatureRoot = creatures.get(i);
            creatureRoot.setPosition(positionX, positionY);
            creatureRoot.setScale(0.5f);

            int groupNumber = creatureRoot.getGroupNumber();
            if (groupNumber >= 0) {
                Rectangle coloredRect = new Rectangle(0, 0, 180, 410, getVertexBufferObjectManager());
                coloredRect.setColor(DebugGroupColors.values()[groupNumber].getColor());
                coloredRect.setZIndex(-1);
                creatureRoot.attachChild(coloredRect);
                creatureRoot.sortChildren();
            }

            scene.attachChild(creatureRoot);

            positionX = positionX + 50;
            if (i == 24) {
                positionX = 80;
                positionY = positionY - 220;
            }
        }
    }

    /* //////////////////////////////////////////// VICTORY SCREEN CALLBACKS */

    @Override
    public String getVictoryText(int progress) {
        switch (progress) {
            case 3:
                return getResources().getString(R.string.game_bankovniloupez_victorytext_threestars);
            case 2:
                return getResources().getString(R.string.game_bankovniloupez_victorytext_twostar);
            case 1:
                return getResources().getString(R.string.game_bankovniloupez_victorytext_onestar);
            default:
                return getResources().getString(R.string.game_bankovniloupez_victorytext_gameover);
        }
    }

    @Override
    public String[] getVictorySpeech() {
        return new String[] { SpeechSounds.RESULT_BANKOVNILOUPEZ_0, SpeechSounds.RESULT_BANKOVNILOUPEZ_1, SpeechSounds.RESULT_BANKOVNILOUPEZ_2, SpeechSounds.RESULT_BANKOVNILOUPEZ_3 };
    }

    @Override
    public int countProgress() {
        int creaturesCount = creatureNumber + 1;
        if (creaturesCount > 49) {
            return 3;
        } else if (creaturesCount > 29) {
            return 2;
        } else if (creaturesCount > 15) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Returns number of players mistakes in game
     * 
     * @return
     */
    private int getNumberOfMistakesInGame() {
        return getGameManager().getExtraInt3() + getGameManager().getExtraInt2();
    }

    @Override
    public CharSequence getStatsText() {
        String indetificationString = getResources().getString(R.string.game_bankovniloupez_victorytext_stats_identification);

        User selectedUser = getTablexiaContext().getSelectedUser();
        if (selectedUser != null) {
            if (selectedUser.isMale()) {
                indetificationString = getResources().getString(R.string.game_bankovniloupez_victorytext_stats_identification_male);
            } else {
                indetificationString = getResources().getString(R.string.game_bankovniloupez_victorytext_stats_identification_female);
            }
        }
        return Html.fromHtml(String.format(getString(R.string.game_bankovniloupez_victorytext_stats), indetificationString, (int) getGameManager().getCounter(), NUMBER_OF_CREATURES));
    }

}
