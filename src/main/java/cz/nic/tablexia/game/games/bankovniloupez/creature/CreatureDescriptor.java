/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.creature;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureRoot.AttributeGender;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute;

/**
 * Description of creature. Contains list of creature attributes and creature gender.
 * 
 * @author Matyáš Latner
 */
public class CreatureDescriptor implements Parcelable {
	
	public static final int 		   THIEF_OFFSET = 0;

    private boolean                    isThief = false;
    private int				  		   groupNumber = CreatureRoot.CREATURE_GROUP_NUMBER_NOGROUP;
    private List<AttributeDescription> descriptions;
	private boolean 				   checkGenderCompatibility;
	private List<Integer>			   creatureOffsets;

    public CreatureDescriptor() {
    	this.checkGenderCompatibility = true;
        descriptions = new ArrayList<AttributeDescription>();
        creatureOffsets = new ArrayList<Integer>();
    }

    public CreatureDescriptor(List<AttributeDescription> descriptions) {
        this(descriptions, false);
    }

    public CreatureDescriptor(List<AttributeDescription> descriptions, boolean isThief) {
    	this();
        this.isThief = isThief;
        this.checkGenderCompatibility = true;
        addDescriptions(descriptions);
    }
    
    public void disableGenderCompatibilityCheck() {
    	checkGenderCompatibility = false;
	}
    
    /**
     * Add creature offset number
     * @param creatureOffset creature offset number
     */
    public void addCreatureOffset(Integer creatureOffset) {
    	creatureOffsets.add(creatureOffset);
    	if (creatureOffset == THIEF_OFFSET) {
    		setThief(true);
    	}
	}
    
    /**
     * Check if creature description contains creature ofsset from parameter
     * @param creatureOffset creature offset to check
     * @return <code>true</code> if creature description contains creature ofsset from parameter
     */
    public boolean containsCreatureOffset(Integer creatureOffset) {
    	return creatureOffsets.contains(creatureOffset);
	}
    
    public List<Integer> getCreatureOffsets() {
		return creatureOffsets;
	}
    
    public void addCreatureOffsets(List<Integer> creatureOffsets) {
		this.creatureOffsets.addAll(creatureOffsets);
	}
    
    /**
     * Return number of creature group.
     * <code>-1</code> when creature is not in group
     * <code>0</code> when creature is special and is not in group 
     * 
     * @return <code>true</code> number of thief group.
     */
    public int getGroupNumber() {
		return groupNumber;
	}
    
    /**
     * Set number of creature group.
     * <code>-1</code> when creature is not in group
     * <code>0</code> when creature is special and is not in group 
     * 
     * @param groupNumber number of creature group or <code>0</code> or <code>-1</code>
     */
    public void setGroupNumber(int groupNumber) {
		this.groupNumber = groupNumber;
	}

    /**
     * Returns gender from existing attributes
     * @return gender from existing attributes
     */
    public AttributeGender getGender() {
    	AttributeGender result = null;
    	if (descriptions != null) {
    		for (AttributeDescription attributeDescription : descriptions) {
				if (attributeDescription.hasSpecificGender()) {
					result = attributeDescription.getAttributeGender();
					break;
				}
			}
    	}
    	
        return result;
    }
    
    /**
     * Return <code>true</code> if is gender specified
     * @return <code>true</code> if is gender specified
     */
    public boolean hasSpecificGender() {
        return AttributeGender.isGenderSpecified(getGender());
    }
    
    /**
     * Return <code>true</code> if attributes in creature description from parameter are compatible with current creature
     * @param creatureDescriptor creature description to check compatibility with current creature
     * @return <code>true</code> if attributes in creature description from parameter are compatible with current creature
     */
    public boolean checkDescriptionCompactibility(CreatureDescriptor creatureDescriptor) {
    	if (creatureDescriptor != null) {    		
    		for (AttributeDescription descriptor : creatureDescriptor.getDescriptions()) {
    			if (!checkDescriptionCompactibility(descriptor)) {
    				return false;
    			}
    		}
    	}
    	return true;
    }
    
    /**
     * Check if attribute description from parameter is compatible with current creature description
     * @param description attribute description to check
     * @return <code>true</code> if attribute description is compatible with current creature description else returns <code>false</code>
     */
    public boolean checkDescriptionCompactibility(AttributeDescription description) {
		if (getGender() != null && getGender() != AttributeGender.ANY && description.hasSpecificGender() && getGender() != description.getAttributeGender()) {
			return false;
		}
    		
    	for (AttributeDescription attributeDescription : descriptions) {
    		if (attributeDescription.hasSpecificGender() && description.hasSpecificGender() && attributeDescription.getAttributeGender() != description.getAttributeGender()) {
    			return false;
    		}
    		
    		if (attributeDescription.getAttributeClass() == description.getAttributeClass()
    				&& description.hasSpecificColor() && attributeDescription.hasSpecificColor()
    				&& attributeDescription.getAttributeColor() != description.getAttributeColor()) {
    			return false;
    		}
    		if (attributeDescription.isSameGenericType(description.getAttributeClass())) {
    			if (attributeDescription.hasSpecificColor() && description.hasSpecificColor()
						&& attributeDescription.getAttributeColor() != description.getAttributeColor()) {
					return false;
				}
				if (!AttributeDescription.isClassGeneric(attributeDescription.getAttributeClass()) && !AttributeDescription.isClassGeneric(description.getAttributeClass())) {
					if (attributeDescription.getAttributeClass() != description.getAttributeClass()) {
    					return false;
    				}
				}
			}
    		
    		if (!checkSuperLevelAttributeCompactibility(attributeDescription, description)) {
    			return false;
    		}
		}
    	
    	return checkSuperLevelAttributeCompactibility(description, null);
    }
    
    /**
     * Checks if super level attribute description is compatible with creature description plus second parameter attribute description
     * 
     * @param superLevelDescription super level attribute description to check
     * @param superLevelDescription attribute description which is added to current creature description
     * @return <code>true</code> if super level attribute description is compatible with current creature description else returns <code>false</code>
     */
    private boolean checkSuperLevelAttributeCompactibility(AttributeDescription superLevelDescription, AttributeDescription additionalDescription) {
    	List<Class<?>> genericClasses = new ArrayList<Class<?>>();
		if (AttributeDescription.isClassSuperGeneric(superLevelDescription.getAttributeClass()) && superLevelDescription.hasSpecificColor()) {	
			for (Class<?> attributeClass : Attribute.getAllGenericAttributesClasses()) {
    			if (attributeClass.getSuperclass() == superLevelDescription.getAttributeClass()) {
    				genericClasses.add(attributeClass);
    			}
			}
			List<Class<?>> usedGenericClasses = new ArrayList<Class<?>>();
			List<AttributeDescription> descriptionsToCheck = new ArrayList<AttributeDescription>(descriptions);
			if (additionalDescription != null) {
				descriptionsToCheck.add(additionalDescription);
			}
			for (AttributeDescription filledAttributeDescriptionToCheck : descriptionsToCheck) {
				if (genericClasses.contains(filledAttributeDescriptionToCheck.getGenericType())) {
					if (!filledAttributeDescriptionToCheck.hasSpecificColor() || filledAttributeDescriptionToCheck.getAttributeColor() == superLevelDescription.getAttributeColor()) {
						return true;
					} else {
						if (!usedGenericClasses.contains(filledAttributeDescriptionToCheck.getGenericType())) {							
							usedGenericClasses.add(filledAttributeDescriptionToCheck.getGenericType());
						}
					}
				}
			}
			if (usedGenericClasses.size() == genericClasses.size()) {
				return false;
			}
		}
		return true;
	}

    /**
     * Add new description and check gender compatibility. If description is incompatible throws {@link IllegalStateException}
     * @param description description to add
     */
    public void addDescription(AttributeDescription description) {
    	if (description != null) {    		
    		if (checkGenderCompatibility && hasSpecificGender() && description.hasSpecificGender()) {
    			AttributeGender descriptionGender = description.getAttributeGender();
    			if (getGender() != descriptionGender) {
    				throw new IllegalStateException("Incompactible gender type " + description + "[" + descriptionGender + "] for CreatureDescriptor " + this);
    			}
    		}
    		descriptions.add(description);
    	}
    }

    /**
     * Add new descriptions and check gender compatibility. If one of descriptions is incompatible throws {@link IllegalStateException}
     * @param descriptions list of descriptions to add
     */
    public void addDescriptions(List<AttributeDescription> descriptions) {
    	for (AttributeDescription attributeDescription : descriptions) {
			addDescription(attributeDescription);
		}
    }
    
    /**
     * Add attribute descriptions from creature description in parameter
     * @param creatureDescriptor creature description to add attribute descriptions from
     */
    public void addDescriptionsFromCreatureDescriptor(CreatureDescriptor creatureDescriptor) {
    	if (creatureDescriptor != null) {
    		addDescriptions(creatureDescriptor.getDescriptions());
    	}
    }

    /**
     * Return list of all attributes descriptions
     * @return list of all attributes descriptions
     */
    public List<AttributeDescription> getDescriptions() {
        return descriptions;
    }

    /**
     * Set current creature as thief
     * @param isThief <code>true</code> for thief flag
     */
    public void setThief(boolean isThief) {
        this.isThief = isThief;
    }

    /**
     * Return <code>true</code> if current creature is thief
     * @return <code>true</code> if current creature is thief
     */
    public boolean isThief() {
        return isThief;
    }

    /**
     * Return <code>true</code> if current creature has valid description
     * @return <code>true</code> if current creature has valid description
     */
    public boolean hasValidDescription() {
        return descriptions.size() > 0;
    }
    
    /**
     * Return <code>true</code> if current creature contains attribute description from parameter
     * @param attributeDescription to check if current creature description contains it
     * @return <code>true</code> if current creature contains attribute description from parameter
     */
    public boolean containsAttributeDescription(AttributeDescription attributeDescription) {
    	for (AttributeDescription description : descriptions) {
			if (description.equals(attributeDescription)) {
				return true;
			}
		}
    	
    	return false;
	}

    @Override
    public boolean equals(Object o) {
        if (o != null) {
            if (o instanceof CreatureDescriptor) {
                CreatureDescriptor creatureDescriptionToCheck = ((CreatureDescriptor) o);

                if ((creatureDescriptionToCheck.getGender() == getGender()) && (creatureDescriptionToCheck.descriptions.size() == descriptions.size())) {

                    for (AttributeDescription attributeDescriptionToCheck : creatureDescriptionToCheck.descriptions) {
                        if (!checkDescriptionList(attributeDescriptionToCheck, descriptions)) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
        return false;
    }

    private boolean checkDescriptionList(AttributeDescription attributeDescriptionToCheck, List<AttributeDescription> descriptions) {
        for (AttributeDescription attributeDescription : descriptions) {
            if (attributeDescription.equals(attributeDescriptionToCheck)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Clear all attribute descriptions
     * @return this instance
     */
    public CreatureDescriptor clearDescriptions() {
    	descriptions = new ArrayList<AttributeDescription>();
    	return this;
	}

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + "[";
        int counter = 0;
        for (AttributeDescription description : descriptions) {
            result = result + String.valueOf(description);
            if (counter < (descriptions.size() - 1)) {
                result = result + ", ";
            }
        }
        return result + "]";
    }

    /* //////////////////////////////////////////// PARCELABLE */

    public static final Parcelable.Creator<CreatureDescriptor> CREATOR = new Parcelable.Creator<CreatureDescriptor>() {

        @Override
        public CreatureDescriptor createFromParcel(Parcel in) {
            boolean readedIsThief = in.readInt() == 1 ? true : false;
            List<AttributeDescription> readedDescriptions = new ArrayList<AttributeDescription>();
            in.readTypedList(readedDescriptions, AttributeDescription.CREATOR);
            return new CreatureDescriptor(readedDescriptions, readedIsThief);
        }

        @Override
        public CreatureDescriptor[] newArray(int size) {
            return new CreatureDescriptor[size];
        }

    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(isThief ? 1 : 0);
        out.writeTypedList(descriptions);
    }

}
