
package cz.nic.tablexia.game.games.nocnisledovani;

import org.andengine.entity.scene.background.EntityBackground;
import org.andengine.entity.sprite.batch.SpriteGroup;

/*
 * nejsprávnější by bylo asi extendnout od SpriteBackgroud, udělat podporu pro přidání druhýho a potom oddědit BackgroundModifier tak, aby zajistil to prolnutí
 */
public class MyAnimatedSpriteBackground extends EntityBackground {
    private SpriteGroup spriteGroup;

    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    public MyAnimatedSpriteBackground(final SpriteGroup pSprite) {
        super(pSprite);
    }

    public MyAnimatedSpriteBackground(final float pRed, final float pGreen, final float pBlue, final SpriteGroup spriteGroup) {
        super(pRed, pGreen, pBlue, spriteGroup);
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public SpriteGroup getSprites() {
        return (SpriteGroup) mEntity;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
