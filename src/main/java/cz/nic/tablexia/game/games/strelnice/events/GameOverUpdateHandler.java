/**
 * 
 */

package cz.nic.tablexia.game.games.strelnice.events;

import org.andengine.engine.handler.IUpdateHandler;

/**
 * ModifielListener, sloužící k vyvolání událostí podle časovaného entity modifieru
 * 
 * @author lhoracek
 */
public abstract class GameOverUpdateHandler implements IUpdateHandler {
    private static final String TAG         = TargetPositionUpdateHandler.class.getSimpleName();

    public static final float   ADD_TIME    = 5f;
    public static final float   SUB_TIME    = 5f;

    private float               elapsedTime = 0;
    private float               gameTime;
    private boolean             done        = false;
    private boolean             running     = false;
    private float               gameSpeed   = 1;

    public void setGameSpeed(float gameSpeed) {
        this.gameSpeed = gameSpeed;
    }

    public void resetGameSpeed() {
        gameSpeed = TargetPositionUpdateHandler.GAME_SPEED_NORMAL;
    }

    public GameOverUpdateHandler(float gameTime) {
        super();
        this.gameTime = gameTime;
    }

    @Override
    public void onUpdate(float pSecondsElapsed) {
        if (!running) {
            onTimeStart();
        }
        elapsedTime += (pSecondsElapsed * gameSpeed);
        if (!done) {
            if (elapsedTime >= gameTime) {
                onTimeOver();
                done = true;
            } else {
                onTimeUpdate(elapsedTime);
            }
        }
        running = true;
    }

    public void addTime(float time) {
        elapsedTime = Math.min(elapsedTime + time, gameTime);
        onUpdate(0);
    }

    public void substractTime(float time) {
        elapsedTime = Math.max(elapsedTime - time, 0);
        onUpdate(0);
    }

    @Override
    public void reset() {
        // nothing
    }

    abstract public void onTimeUpdate(float time);

    abstract public void onTimeOver();

    abstract public void onTimeStart();
}
