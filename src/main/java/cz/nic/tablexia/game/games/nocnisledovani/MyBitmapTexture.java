package cz.nic.tablexia.game.games.nocnisledovani;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.bitmap.BitmapTexture;
import org.andengine.util.adt.io.in.IInputStreamOpener;
import org.andengine.util.debug.Debug;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;

import com.activeandroid.util.Log;

public class MyBitmapTexture extends BitmapTexture {
    private static final String TAG = MyBitmapTexture.class.getSimpleName();
    private int color;
    private List<Integer>colorsToFilter;
    private Bitmap bitmapToBeFiltered;
    private GfxManager gfxManager;
    private boolean colorBitmapCreated;

    private static class FakeInpuStreamOpener implements IInputStreamOpener {

        @Override
        public InputStream open() throws IOException {
            return new ByteArrayInputStream(new byte[0]);
        }

    }

    public MyBitmapTexture(TextureManager pTextureManager, int color, Bitmap bitmap,GfxManager gfxManager) throws IOException {
        super(pTextureManager, new FakeInpuStreamOpener());
        this.color = color;
        bitmapToBeFiltered = bitmap;
        this.gfxManager = gfxManager;
    }

    public MyBitmapTexture(TextureManager pTextureManager, List<Integer> colorsToFilter, Bitmap bitmap, Bitmap bitmapToDecorateWith,GfxManager gfxManager) throws IOException {
        super(pTextureManager, new FakeInpuStreamOpener());
        this.colorsToFilter = colorsToFilter;
        bitmapToBeFiltered = bitmap;
        this.gfxManager = gfxManager;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    @Override
    public int getWidth() {
        return bitmapToBeFiltered.getWidth();
    }

    @Override
    public int getHeight() {
        return bitmapToBeFiltered.getHeight();
    }

    @Override
    protected Bitmap onGetBitmap(final Config pBitmapConfig) throws IOException {
        Log.d(TAG, "In onGetBitmap");
        colorBitmapCreated = false;
        Bitmap bitmap = bitmapToBeFiltered.copy(pBitmapConfig, true);
        //filterOtherColors(bitmap, color);
        final Canvas canvas = new Canvas(bitmap);
        try {
            onDecorateBitmap(canvas);
        } catch (final Exception e) {
            Debug.e(e);
        }
        return bitmap;
    }

    protected void onDecorateBitmap(final Canvas pCanvas) throws Exception {
        Log.d(TAG, "In onDecorateBitmap");
        Paint xferPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        xferPaint.setColorFilter(new LightingColorFilter(color, Color.TRANSPARENT));
        xferPaint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        pCanvas.drawBitmap(bitmapToBeFiltered, 0, 0, xferPaint);
    }

}
