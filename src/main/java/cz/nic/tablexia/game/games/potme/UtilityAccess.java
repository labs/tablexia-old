package cz.nic.tablexia.game.games.potme;

import org.andengine.audio.sound.Sound;
import org.andengine.engine.Engine;
import org.andengine.entity.IEntity;

import cz.nic.tablexia.game.common.RandomAccess;

/**
 * Utility class
 * 
 * @author Matyáš Latner
 *
 */
public class UtilityAccess {
	
	public interface IOnDetachListener {
		
		public void onDetach();
		
	}
	
	private static UtilityAccess instance;
	private Engine engine;
	private RandomAccess randomAccess;
	
	private UtilityAccess(Engine engine, RandomAccess randomAccess) {
		this.engine 		= engine;
		this.randomAccess 	= randomAccess;
	}
	
	public static void init(Engine engine, RandomAccess randomAccess) {
		instance = new UtilityAccess(engine, randomAccess);
	}
	
	public static UtilityAccess getInstance() {
		return instance;
	}
	
	public Engine getEngine() {
		return engine;
	}
	
	public void detachAndAttachEntity(final IEntity entityToDetach, final IEntity entityToAttachIn, final IOnDetachListener detachListener, final boolean setVisible) {
		if (entityToDetach != null && entityToDetach.hasParent()) {	
			entityToDetach.setVisible(false);
			engine.runOnUpdateThread(new Runnable() {
				
				@Override
				public void run() {
					if(entityToDetach.detachSelf()) {						
						if (entityToAttachIn != null) {
							entityToAttachIn.attachChild(entityToDetach);
							entityToDetach.setVisible(setVisible);
						}
						if (detachListener != null) {						
							detachListener.onDetach();
						} 
					}
				}
			});
		}
	}
	
	public void playRandomSoundFromGroup(String[] soundGroup) {
		playRandomSoundFromGroup(soundGroup, 1.0f);
	};
	
	public void playRandomSoundFromGroup(String[] soundGroup, float rate) {
		Sound sound = ResourceManager.getInstance().getSound(soundGroup[randomAccess.getRandom().nextInt(soundGroup.length)]);
		sound.setRate(rate);
		sound.play();
	}
	
}
