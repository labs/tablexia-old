/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.potme.map.widget;

import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Line;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.primitive.vbo.IRectangleVertexBufferObject;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.DrawType;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;

import cz.nic.tablexia.game.games.potme.PotmeActivity;
import cz.nic.tablexia.game.games.potme.ResourceManager;
import cz.nic.tablexia.game.games.potme.map.IMapProvider;
import cz.nic.tablexia.game.games.potme.map.TileMap;
import cz.nic.tablexia.game.games.potme.map.mapobject.MapObject;
import cz.nic.tablexia.game.games.potme.map.mapobject.MapObjectType;
import cz.nic.tablexia.game.games.potme.map.mapobstacle.MapObstacle;
import cz.nic.tablexia.game.games.potme.map.tile.Tile;

public class MapWidget extends Entity {
	
	private static final float 			BOOKMARKBUTTON_TILE_SIZE_RATIO_HEIGHT 	= 0.3f;
	private static final float 			BOOKMARKBUTTON_TILE_SIZE_RATIO_WIDTH 	= 1.5f;
	private static final float 			BOOKMARKBUTTON_ALPHA_INFRONT 			= 1;
	private static final float 			BOOKMARKBUTTON_ALPHA_INBOTTOM 			= 0.5f;
	public  static final Color 			BOOKMARKBUTTON_COLOR 		   			= new Color(0.588f, 0.529f, 0.510f);
	private static final float 			RASTER_ALPHA 				   			= 0.5f;
	private static final Color 			RASTER_COLOR 				   			= Color.BLACK;
	
	private Entity 						mapLayer;
	private Entity 						backgroundLayer;
	private BookmarkButton 				bookmarkButton;
	
	private TileMap 					tileMap;
	
	private String 						buttonText;
	private int 						floorNumber;
	
	private TileMapClickListener 		tileMapClickListener;
	
	public interface TileMapClickListener {
		void onBookmarkClick(int mapWidgetNumber);
	}
	
	private class BookmarkButton extends Rectangle {

		private boolean clickable;

		public BookmarkButton(float pX, float pY, float pWidth, float pHeight, IRectangleVertexBufferObject pRectangleVertexBufferObject) {
			super(pX, pY, pWidth, pHeight, pRectangleVertexBufferObject);
			clickable = true;
		}

		public BookmarkButton(float pX, float pY, float pWidth, float pHeight, VertexBufferObjectManager pVertexBufferObjectManager) {
			super(pX, pY, pWidth, pHeight, pVertexBufferObjectManager);
			clickable = true;
		}

		public BookmarkButton(float pX, float pY, float pWidth, float pHeight, VertexBufferObjectManager pVertexBufferObjectManager, DrawType pDrawType) {
			super(pX, pY, pWidth, pHeight, pVertexBufferObjectManager, pDrawType);
			clickable = true;
		}
		
		public void setClickable(boolean clickable) {
			this.clickable = clickable;
		}
		
		@Override
		public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
			if (clickable && pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
				if (tileMapClickListener != null) {
					tileMapClickListener.onBookmarkClick(MapWidget.this.floorNumber);
				}
			}
			return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
		}
		
	}
	
	public MapWidget(IMapProvider mapProvider, Tile lastFinishTile, MapObjectType finishMapObjetcType, int floorNumber, boolean hasBookmark, int positionX, int positionY, int mapXSize, int mapYSize, boolean hasKey, String buttonText, VertexBufferObjectManager vertexBufferObjectManager) {
		super();
		this.buttonText  = buttonText;
		this.floorNumber = floorNumber;
		
        setWidth((mapXSize * PotmeActivity.TILE_SIZE));
        setHeight((mapYSize * PotmeActivity.TILE_SIZE));
		
		backgroundLayer = new Entity();
		attachChild(backgroundLayer);
		mapLayer = new Entity();
		attachChild(mapLayer);

		tileMap = mapProvider.prepareMap(lastFinishTile, mapXSize, mapYSize, finishMapObjetcType, hasKey);
		if (hasBookmark) {
			showBookmarkButton(vertexBufferObjectManager);
		}
        showTileMap(vertexBufferObjectManager);
        
        setPosition(positionX + (getWidth() / 2), positionY + (getHeight() / 2));
	}
	
	public void setClickable(boolean clickable) {
		if (bookmarkButton != null) {
			bookmarkButton.setClickable(clickable);
		}
	}
	
	public TileMap getTileMap() {
		return tileMap;
	}
	
	public void setClickable(Scene scene, TileMapClickListener tileMapClickListener) {
		this.tileMapClickListener = tileMapClickListener;
		if (bookmarkButton != null) {
			scene.registerTouchArea(bookmarkButton);
		}
	}
	
	public void setInFront() {
		if (bookmarkButton != null) {
			bookmarkButton.setAlpha(BOOKMARKBUTTON_ALPHA_INFRONT);
		}
	}
	
	public void setInBottom() {
		if (bookmarkButton != null) {
			bookmarkButton.setAlpha(BOOKMARKBUTTON_ALPHA_INBOTTOM);
		}
	}
	
	private void showBookmarkButton(VertexBufferObjectManager vertexBufferObjectManager) {
		
		float bookmarkWidth = PotmeActivity.TILE_SIZE * BOOKMARKBUTTON_TILE_SIZE_RATIO_WIDTH;
		float bookmarkHeight = PotmeActivity.TILE_SIZE * BOOKMARKBUTTON_TILE_SIZE_RATIO_HEIGHT;
		
		bookmarkButton = new BookmarkButton(bookmarkWidth / 2 + floorNumber * (bookmarkWidth + (bookmarkWidth / 20)),
														  -(getHeight() + (bookmarkHeight / 2)),
														  bookmarkWidth,
														  bookmarkHeight,
														  vertexBufferObjectManager);
		
		bookmarkButton.setColor(BOOKMARKBUTTON_COLOR);
		bookmarkButton.attachChild(new Text(bookmarkWidth / 2,
										   bookmarkHeight / 2,
										   ResourceManager.getInstance().getFont(),
										   (floorNumber + 1) + ". " + buttonText,
										   vertexBufferObjectManager));
		
		setHeight(getHeight() + bookmarkHeight);
		backgroundLayer.attachChild(bookmarkButton);
	}
	
	private void showTileMap(VertexBufferObjectManager vertexBufferObjectManager) {

        Entity tilesLayer = new Entity();
        Entity mapObjectsLayer = new Entity();
        Entity mapObstaclesLayer = new Entity();
        Entity rasterLayer = new Entity();

        int tileMapXSize = (tileMap.getMapXSize() * PotmeActivity.TILE_SIZE);
        int tileMapYSize = (tileMap.getMapYSize() * PotmeActivity.TILE_SIZE);

        drawRasterLine(rasterLayer, 0, 0, 0, -tileMapYSize, vertexBufferObjectManager);
        drawRasterLine(rasterLayer, 0, 0, tileMapXSize, 0, vertexBufferObjectManager);

        for (int i = 0; i < tileMap.getMapXSize(); i++) {

            int actualTileMapXPosition = ((i + 1) * PotmeActivity.TILE_SIZE);
            drawRasterLine(rasterLayer, actualTileMapXPosition, 0, actualTileMapXPosition, -tileMapYSize, vertexBufferObjectManager);

            for (int j = 0; j < tileMap.getMapYSize(); j++) {

                int actualTileMapYPosition = -(i * PotmeActivity.TILE_SIZE);
                drawRasterLine(rasterLayer, 0, actualTileMapYPosition, tileMapXSize, actualTileMapYPosition, vertexBufferObjectManager);

                Tile tile = tileMap.getTileAtPosition(i, j);
                if (tile != null) {
                    // tiles
                    tilesLayer.attachChild(tile.getTileSprite(vertexBufferObjectManager));

                    // map objects
                    MapObject mapObject = tile.getMapObject();
                    if (mapObject != null) {
                        mapObjectsLayer.attachChild(mapObject.getMapObjectSprite(vertexBufferObjectManager));
                    }

                    // map obstacles
                    for (MapObstacle mapObstacle : tile.getMapObstacles()) {
                        if (mapObstacle != null) {
                            mapObstaclesLayer.attachChild(mapObstacle.getMapObstacleContainer(vertexBufferObjectManager));
                        }
                    }
                }
            }
        }

        mapLayer.attachChild(tilesLayer);
        mapLayer.attachChild(mapObjectsLayer);
        mapLayer.attachChild(mapObstaclesLayer);
        mapLayer.attachChild(rasterLayer);
    }
	
	private void drawRasterLine(Entity layer, int fromX, int fromY, int toX, int toY, VertexBufferObjectManager vertexBufferObjectManager) {
        Line line = new Line(fromX, fromY, toX, toY, vertexBufferObjectManager);
        line.setColor(RASTER_COLOR);
        line.setAlpha(RASTER_ALPHA);
        layer.attachChild(line);
    }

}
