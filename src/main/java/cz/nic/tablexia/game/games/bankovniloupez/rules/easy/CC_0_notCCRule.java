/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.rules.easy;

import java.util.ArrayList;
import java.util.List;

import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.Context;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureFactory;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureRoot;
import cz.nic.tablexia.game.games.bankovniloupez.rules.GameRuleUtility;

/**
 * 
 * @author Matyáš Latner
 */
public class CC_0_notCCRule extends GameRuleUtility {
    
	private   static final int 	 	GROUP_SIZE	= 2;
	protected static final Integer 	T0_OFFSET 	= CreatureDescriptor.THIEF_OFFSET;
	protected 			   Integer 	T1_OFFSET 	= Integer.valueOf(1);
	protected CreatureDescriptor t0CreatureDescriptorToBan;

    public CC_0_notCCRule(Context context, RandomAccess randomAccess, VertexBufferObjectManager vertexBufferObject, int numberOfCreatures, int numberOfThieves) {
        super(context, randomAccess, vertexBufferObject, numberOfCreatures, numberOfThieves, GROUP_SIZE);
    }
    
    public CC_0_notCCRule(Context context, RandomAccess randomAccess, VertexBufferObjectManager vertexBufferObject, int numberOfCreatures, int numberOfThieves, int groupSize) {
        super(context, randomAccess, vertexBufferObject, numberOfCreatures, numberOfThieves, groupSize);
    }
    
    @Override
    public String[] prepareRuleMessageParameters() {
        return new String[] {
        		getAttributeColorName(t0CreatureDescriptorToBan.getDescriptions().get(0)),
        		getAttributeName(t0CreatureDescriptorToBan.getDescriptions().get(0), false),
                getAttributeColorName(getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0)),
                getAttributeName(getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0), false)
        };
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
    	
    	AttributeDescription t1AttributeDescription = getRandomAttributeDescription(CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, vertexBufferObject, getRandomAccess()).getCreatureDescrition());
    	CreatureDescriptor t1CreatureDescriptor = new CreatureDescriptor();
    	t1CreatureDescriptor.addDescription(t1AttributeDescription);
        addGlobalCreatureDescriptor(T1_OFFSET, t1CreatureDescriptor);
        
        
        addGlobalCreatureDescriptor(T0_OFFSET, new CreatureDescriptor());
        
        
        CreatureDescriptor t2BannedAttributesForGenerator = new CreatureDescriptor();
        t2BannedAttributesForGenerator.disableGenderCompatibilityCheck();
        t2BannedAttributesForGenerator.addDescriptions(BAN_ATTRIBUTES_SET_FOR_GENERATING.getDescriptions());
        t2BannedAttributesForGenerator.addDescription(t1AttributeDescription);
        
        AttributeDescription t2AttributeDescription = getRandomAttributeDescription(CreatureFactory.getInstance().generateCreature(null, t2BannedAttributesForGenerator, vertexBufferObject, getRandomAccess()).getCreatureDescrition());
    	t0CreatureDescriptorToBan = new CreatureDescriptor();
    	t0CreatureDescriptorToBan.addDescription(t2AttributeDescription);
    }
    
    @Override
    protected List<CreatureRoot> prepareCreatureDescriptionsA() {
    	
    	AttributeDescription t1AttributeDescription = getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0);
    	AttributeDescription t0AttributeDescriptionToBan = t0CreatureDescriptorToBan.getDescriptions().get(0);
    	
    	List<CreatureRoot> creatures = new ArrayList<CreatureRoot>();
        for (int i = 0; i < numberOfCreatures; i++) {
        	
        	CreatureDescriptor creatureDescriptorToForce = new CreatureDescriptor();
        	creatureDescriptorToForce.disableGenderCompatibilityCheck();
        	CreatureDescriptor creatureDescriptorToBan = new CreatureDescriptor();
        	creatureDescriptorToBan.disableGenderCompatibilityCheck();
        	
        	
        	CreatureRoot lastCreature = null;
        	int lastPosition = i - T1_OFFSET;
			if (lastPosition >= 0) {					
				lastCreature = creatures.get(lastPosition);
			}
			
			CreatureDescriptor nextSpecialCreature = specialCreatures.get(i + T1_OFFSET);
			
        	
            CreatureDescriptor creatureDescriptor = specialCreatures.get(i);
			if (creatureDescriptor != null) { // add special creature
            	if (creatureDescriptor.isThief()) {
            		
                	creatureDescriptorToBan.addDescription(t0AttributeDescriptionToBan);
            		
            		if (nextSpecialCreature != null && !nextSpecialCreature.isThief() 
            				&& !creatureDescriptor.getDescriptions().contains(t1AttributeDescription)) {
            			
            			creatureDescriptorToBan.addDescription(t1AttributeDescription);
            		}
            		
            	}
            	
            	creatures.add(CreatureFactory.getInstance().generateCreature(creatureDescriptor, creatureDescriptorToBan, vertexBufferObject, getRandomAccess()));
            } else {
            	
            	boolean hasNotT0Attribute = true;
            	boolean hasNotBanT1Attribute = true;
            	
				if (lastCreature != null && lastCreature.hasAttribute(t1AttributeDescription)) {
					creatureDescriptorToForce.addDescription(t0AttributeDescriptionToBan);
					hasNotT0Attribute = false;
				}
				
				if (nextSpecialCreature != null) {
					creatureDescriptorToBan.addDescription(t1AttributeDescription);
					hasNotBanT1Attribute = false;
				}
				
				if (hasNotT0Attribute && hasNotBanT1Attribute) {
					newBaitCreatureDescription(getGlobalCreatureDescriptor(T1_OFFSET));						
					creatureDescriptorToForce.addDescription(getBaitCreatureDescriptionAttributeAtPosition(0));
				}
				
				creatures.add(CreatureFactory.getInstance().generateCreature(creatureDescriptorToForce, creatureDescriptorToBan, vertexBufferObject, getRandomAccess()));				
            }
        }

        return creatures;
    }

}
