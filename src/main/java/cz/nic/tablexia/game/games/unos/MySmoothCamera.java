/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.unos;

import org.andengine.engine.camera.SmoothCamera;

/**
 * @author lhoracek
 */
public class MySmoothCamera extends SmoothCamera {

    private static final int TRASHOLD = 10;

    private float            mTargetCenterX;
    private float            mTargetCenterY;

    public interface OnSmoothCenterFinishedListener {
        void onFinished();
    }

    public MySmoothCamera(float pX, float pY, float pWidth, float pHeight, float pMaxVelocityX, float pMaxVelocityY, float pMaxZoomFactorChange) {
        super(pX, pY, pWidth, pHeight, pMaxVelocityX, pMaxVelocityY, pMaxZoomFactorChange);
    }

    private OnSmoothCenterFinishedListener mOnSmoothCenterFinishedListener;

    public void setCenter(float pCenterX, float pCenterY, OnSmoothCenterFinishedListener onSmoothCenterFinishedListener) {
        mTargetCenterX = pCenterX;
        mTargetCenterY = pCenterY;
        mOnSmoothCenterFinishedListener = onSmoothCenterFinishedListener;
        super.setCenter(pCenterX, pCenterY);
    }

    private boolean isCentered() {
        return (Math.abs(getCenterX() - mTargetCenterX) < TRASHOLD) && (Math.abs(getCenterY() - mTargetCenterY) < TRASHOLD);
    }

    @Override
    public void onUpdate(float pSecondsElapsed) {
        boolean notifyIfCentered = !isCentered();
        super.onUpdate(pSecondsElapsed);
        if (notifyIfCentered && isCentered()) {
            if (mOnSmoothCenterFinishedListener != null) {
                mOnSmoothCenterFinishedListener.onFinished();
                mOnSmoothCenterFinishedListener = null;
            }
        }
    }
}
