/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


package cz.nic.tablexia.game.games.potme.action.rule;

import org.andengine.entity.IEntity;
import org.andengine.util.modifier.IModifier;

import cz.nic.tablexia.game.common.EntityModifierListenerAdapter;
import cz.nic.tablexia.game.games.potme.PotmeActivity;
import cz.nic.tablexia.game.games.potme.ResourceManager;
import cz.nic.tablexia.game.games.potme.action.ActionType.IActionFinishedListener;
import cz.nic.tablexia.game.games.potme.action.ActionUtility;
import cz.nic.tablexia.game.games.potme.creature.Player;
import cz.nic.tablexia.game.games.potme.creature.Player.PlayerOrientation;
import cz.nic.tablexia.game.games.potme.map.TileMap;
import cz.nic.tablexia.game.games.potme.map.TileMap.TileMapPosition;
import cz.nic.tablexia.game.games.potme.map.mapobstacle.MapObstacle;
import cz.nic.tablexia.game.games.potme.map.mapobstacle.MapObstacleType;

/**
 * Rule for DOG action
 * 
 * @author Matyáš Latner
 * 
 */
public class DogActionRule extends AbstractActionTypeRule {

    @Override
    public void performActionRule(PotmeActivity potmeActivity, TileMap tileMap, Player player, int mapStartPositionX, int mapStartPositionY, final IActionFinishedListener finishedListener) {
        PlayerOrientation actualPlayerOrientation = player.getActualPlayerOrientation();
        TileMapPosition actualTileMapPosition = player.getActualTileMapPosition();
        MapObstacle obstacleInDirection = ActionUtility.getObstacleInDirection(tileMap, actualTileMapPosition, actualPlayerOrientation);
        TileMapPosition nextTileMapPosition = player.getNextTileMapPosition();

        if (obstacleInDirection != null && (obstacleInDirection.getMapObstacleType() == MapObstacleType.DOG_H || obstacleInDirection.getMapObstacleType() == MapObstacleType.DOG_V)) {
            player.jumpToTileMapPosition(tileMap, mapStartPositionX, mapStartPositionY, actualTileMapPosition, nextTileMapPosition, new EntityModifierListenerAdapter() {
				@Override
				public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
					performOnActionFinished(finishedListener, true);
				}
			});
        } else {
        	playSound(ResourceManager.ERROR);
        	potmeActivity.showCrashInfo();
        	performOnActionFinishWithDelay(player, finishedListener, false, PotmeActivity.ERROR_ACTION_DELAY);
        }
    }

}
