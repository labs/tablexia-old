/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.pronasledovani.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * (c) 2013 Vaclav Tarantik (c) 2013 CZ.NIC
 * 
 */
public class Card implements Parcelable {
    // ===========================================================
    // Elements
    // ===========================================================

    // ===========================================================
    // Constants
    // ===========================================================

    public static final int UP = 0;
    public static final int LEFT = 1;
    public static final int DOWN = 2;
    public static final int RIGHT = 3;

    // ===========================================================
    // Fields
    // ===========================================================

    private int actualPositionInGrid;
    private int correctPositionInGrid;
    private int gridPositionX;
    private int gridPositionY;
    private int cardWidth;
    private int cardHeight;
    private int rotation;

    // ===========================================================
    // Constructors
    // ===========================================================

    public Card(int positionInGrid, int gridSize) {
        rotation = UP;
        setCorrectPositionInGrid(Integer.valueOf(positionInGrid));
        actualPositionInGrid = Integer.valueOf(positionInGrid);
        setGridPositionX(actualPositionInGrid % gridSize);
        setGridPositionY(actualPositionInGrid / gridSize);
    }

    public Card(Parcel p){
        setActualPositionInGrid(p.readInt());
        setCorrectPositionInGrid(p.readInt());
        setGridPositionX(p.readInt());
        setGridPositionY(p.readInt());
        setCardWidth(p.readInt());
        setCardHeight(p.readInt());
        setRotation(p.readInt());

    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public int getActualPositionInGrid() {
        return actualPositionInGrid;
    }

    public void setActualPositionInGrid(int newPosition) {
        actualPositionInGrid = newPosition;
    }

    // Tady jenom getter, setuje se pri vytvoreni karty, aby nedoslo ke zmene
    // omylem
    public int getCorrectPositionInGrid() {
        return correctPositionInGrid;
    }

    public int getRotation() {
        return rotation;
    }

    public void setRotation(int rotation) {
        this.rotation = rotation;
    }

    public int getCardWidth() {
        return cardWidth;
    }

    public void setCardWidth(int cardWidth) {
        this.cardWidth = cardWidth;
    }

    public int getCardHeight() {
        return cardHeight;
    }

    public void setCardHeight(int cardHeight) {
        this.cardHeight = cardHeight;
    }

    // ===========================================================
    // Methods from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    public void rotateLeft() {
        setRotation(getRotation() + 1);
        if (getRotation() > RIGHT) {
            setRotation(UP);
        }
    }

    public void rotateRight() {
        setRotation(getRotation() - 1);
        if (getRotation() < UP) {
            setRotation(RIGHT);
        }
    }
    public int getGridPositionX() {
        return gridPositionX;
    }

    public void setGridPositionX(int gridPositionX) {
        this.gridPositionX = gridPositionX;
    }

    public int getGridPositionY() {
        return gridPositionY;
    }

    public void setGridPositionY(int gridPositionY) {
        this.gridPositionY = gridPositionY;
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(actualPositionInGrid);
        dest.writeInt(correctPositionInGrid);
        dest.writeInt(gridPositionX);
        dest.writeInt(gridPositionY);
        dest.writeInt(cardWidth);
        dest.writeInt(cardHeight);
        dest.writeInt(rotation);

    }

    private void setCorrectPositionInGrid(int correctPositionInGrid) {
        this.correctPositionInGrid = correctPositionInGrid;
    }

    public static final Parcelable.Creator<Card> CREATOR = new Creator<Card>() {

        @Override
        public Card createFromParcel(Parcel source) {
            return new Card(source);
        }

        @Override
        public Card[] newArray(int size) {
            return new Card[size];
        }

    };



}
