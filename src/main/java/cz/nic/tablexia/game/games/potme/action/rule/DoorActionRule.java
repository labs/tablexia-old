/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.potme.action.rule;

import cz.nic.tablexia.game.games.potme.PotmeActivity;
import cz.nic.tablexia.game.games.potme.ResourceManager;
import cz.nic.tablexia.game.games.potme.action.ActionType.IActionFinishedListener;
import cz.nic.tablexia.game.games.potme.action.ActionUtility;
import cz.nic.tablexia.game.games.potme.creature.Player;
import cz.nic.tablexia.game.games.potme.creature.Player.PlayerOrientation;
import cz.nic.tablexia.game.games.potme.map.TileMap;
import cz.nic.tablexia.game.games.potme.map.TileMap.TileMapPosition;
import cz.nic.tablexia.game.games.potme.map.mapobstacle.MapObstacle;
import cz.nic.tablexia.game.games.potme.map.mapobstacle.MapObstacleType;

/**
 * Rule for DOOR action
 * 
 * @author Matyáš Latner
 *
 */
public class DoorActionRule extends AbstractActionTypeRule {

	@Override
	public void performActionRule(PotmeActivity potmeActivity, TileMap tileMap, Player player, int mapStartPositionX, int mapStartPositionY, IActionFinishedListener finishedListener) {
		PlayerOrientation actualPlayerOrientation = player.getActualPlayerOrientation();
		TileMapPosition actualTileMapPosition = player.getActualTileMapPosition();
		MapObstacle obstacleInDirection = ActionUtility.getObstacleInDirection(tileMap, actualTileMapPosition, actualPlayerOrientation);
		
		MapObstacleType mapObstacleDoorType = actualPlayerOrientation == PlayerOrientation.BOTTOM || actualPlayerOrientation == PlayerOrientation.TOP ? MapObstacleType.DOOR_H : MapObstacleType.DOOR_V;
		if (obstacleInDirection != null && obstacleInDirection.getMapObstacleType() == mapObstacleDoorType && obstacleInDirection.isEnabled()) {
			ActionUtility.disbaleObstacleInDirection(tileMap, actualTileMapPosition, actualPlayerOrientation);
			playSound(ResourceManager.DOOR);
			performOnActionFinished(finishedListener, true);
		} else {
			playSound(ResourceManager.ERROR);
			potmeActivity.showCrashInfo();
			performOnActionFinishWithDelay(player, finishedListener, false, PotmeActivity.ERROR_ACTION_DELAY);
		}
	}

}
