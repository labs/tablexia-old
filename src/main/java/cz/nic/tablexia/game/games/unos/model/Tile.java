/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.unos.model;

import java.io.Serializable;

/**
 * @author lhoracek
 */
public class Tile implements Serializable {

    private static final long serialVersionUID = -8921743061706906125L;
    private final Integer swSize, seSize;
    private final String  resource;
    private final int     yOffset;

    public Tile(Integer swSize, Integer seSize, String resource, int yOffset) {
        super();
        this.swSize = swSize;
        this.seSize = seSize;
        this.resource = resource;
        this.yOffset = yOffset;
    }

    public Integer getSWSize() {
        return swSize;
    }

    public Integer getSESize() {
        return seSize;
    }

    public String getResource() {
        return resource;
    }

    public int getYOffset() {
        return yOffset;
    }

    @Override
    public String toString() {
        return "Tile[" + swSize + ":" + seSize + ", resource=" + resource + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((resource == null) ? 0 : resource.hashCode());
        result = (prime * result) + ((seSize == null) ? 0 : seSize.hashCode());
        result = (prime * result) + ((swSize == null) ? 0 : swSize.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Tile other = (Tile) obj;
        if (resource == null) {
            if (other.resource != null) {
                return false;
            }
        } else if (!resource.equals(other.resource)) {
            return false;
        }
        if (seSize == null) {
            if (other.seSize != null) {
                return false;
            }
        } else if (!seSize.equals(other.seSize)) {
            return false;
        }
        if (swSize == null) {
            if (other.swSize != null) {
                return false;
            }
        } else if (!swSize.equals(other.swSize)) {
            return false;
        }
        return true;
    }

}
