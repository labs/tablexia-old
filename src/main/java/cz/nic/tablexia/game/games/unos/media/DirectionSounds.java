/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.unos.media;

import java.io.Serializable;
import java.util.Random;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.unos.model.enums.Direction;

/**
 * @author lhoracek
 */
public class DirectionSounds {

    private static Random random = new Random();

    public static SoundPack getRandomSoundPack(GameDifficulty gameDifficulty, Direction fromDirection, Direction nextDirection) {
        if (gameDifficulty == GameDifficulty.EASY) {
            return Level1.getRandomSoundPack(fromDirection, nextDirection);
        } else if (gameDifficulty == GameDifficulty.MEDIUM) {
            return Level2.getRandomSoundPack(fromDirection, nextDirection);
        } else {
            return Level3.getRandomSoundPack(fromDirection, nextDirection);
        }
    }

    public static boolean isCorrectDirection(Direction direction, Direction correctDirection) {
        return direction == correctDirection;
    }

    public static class SoundPack implements Serializable {

        private static final long serialVersionUID = -4529735716574493739L;

        public SoundPack(int soundNumber, String example, String correct, String wrong1, String wrong2) {
            super();
            this.soundNumber = soundNumber;
            this.example = example;
            this.correct = correct;
            this.wrong1 = wrong1;
            this.wrong2 = wrong2;
        }

        private Integer soundNumber;

        private String  example, correct, wrong1, wrong2;

        public String getCorrect() {
            return correct;
        }

        public String getExample() {
            return example;
        }

        public String getWrong1() {
            return wrong1;
        }

        public String getWrong2() {
            return wrong2;
        }

        public String[] getSoundsInOrder(Direction first, Direction second, Direction third, Direction next) {
            String[] sounds = new String[3];

            sounds[0] = (first == next) ? correct : wrong1;
            sounds[1] = (first == next) ? wrong1 : ((second == next) ? correct : wrong2);
            sounds[2] = (third == next) ? correct : wrong2;

            return sounds;
        }

        public Integer getSoundNumber() {
            return soundNumber;
        }

    }

    public static class Level1 {

        private static final int    SOUNDS        = 20;
        private static final String FOLDER_PREFIX = "level1/";

        public static SoundPack getRandomSoundPack(Direction fromDirection, Direction nextDirection) {
            int num = random.nextInt(SOUNDS) + 1;

            String correct = FOLDER_PREFIX + num + "-" + getRandomLetter();

            String wrong1 = null;
            do {
                wrong1 = FOLDER_PREFIX + num + "-" + getRandomLetter();
            } while (wrong1.equals(correct));

            String wrong2 = null;
            do {
                wrong2 = FOLDER_PREFIX + num + "-" + getRandomLetter();
            } while (wrong2.equals(correct) || wrong2.equals(wrong1));

            return new SoundPack(num, correct, correct, wrong1, wrong2);
        }
    }

    public static class Level2 {

        private static final int    SOUNDS        = 20;
        private static final String FOLDER_PREFIX = "level2/";

        public static SoundPack getRandomSoundPack(Direction fromDirection, Direction nextDirection) {
            int num = random.nextInt(SOUNDS) + 1;

            String correct = FOLDER_PREFIX + num + "-" + getRandomLetter();

            String wrong1 = null;
            do {
                wrong1 = FOLDER_PREFIX + num + "-" + getRandomLetter();
            } while (wrong1.equals(correct));

            String wrong2 = null;
            do {
                wrong2 = FOLDER_PREFIX + num + "-" + getRandomLetter();
            } while (wrong2.equals(correct) || wrong2.equals(wrong1));

            boolean previewSpelled = random.nextBoolean();

            if (previewSpelled) {
                return new SoundPack(num, correct + "-HL", correct, wrong1, wrong2);
            }
            return new SoundPack(num, correct, correct + "-HL", wrong1 + "-HL", wrong2 + "-HL");
        }
    }

    public static class Level3 {

        private static final int    SOUNDS        = 31;
        private static final String FOLDER_PREFIX = "level3/";

        public static SoundPack getRandomSoundPack(Direction fromDirection, Direction nextDirection) {
            int num = random.nextInt(SOUNDS) + 1;

            String example = FOLDER_PREFIX + num + "-VZ";
            String correct = FOLDER_PREFIX + num + "-A";

            String wrong1 = null;
            do {
                wrong1 = FOLDER_PREFIX + num + "-" + getRandomLetter();
            } while (wrong1.equals(correct));

            String wrong2 = null;
            do {
                wrong2 = FOLDER_PREFIX + num + "-" + getRandomLetter();
            } while (wrong2.equals(correct) || wrong2.equals(wrong1));

            return new SoundPack(num, example, correct, wrong1, wrong2);
        }

    }

    protected static String getRandomLetter() {
        int rand = random.nextInt(3);
        switch (rand) {
            case 1:
                return "A";
            case 2:
                return "B";
            default:
                return "C";
        }
    }

}
