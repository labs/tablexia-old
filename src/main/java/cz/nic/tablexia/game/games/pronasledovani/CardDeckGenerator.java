
package cz.nic.tablexia.game.games.pronasledovani;

import java.util.ArrayList;
import java.util.HashMap;

import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;

import android.content.Context;
import android.graphics.Point;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.game.games.pronasledovani.model.Card;
import cz.nic.tablexia.util.andengine.ZipFileBitmapTextureAtlasSource;

public class CardDeckGenerator {
    private static final String           TEXTURES_BASE_PATH = PronasledovaniActivity.BASE_DIR + "textures/";

    private int                           cardTextureWidth;
    private int                           cardWidth;
    private Point                         gridOffset;

    private BitmapTextureAtlas            mCardDeckTexture;

    private HashMap<Card, ITextureRegion> mCardTotextureRegionMap;
    // List, ktery obsahuje vsechny karty a jejich info
    private ArrayList<Card>               cards;

    public CardDeckGenerator(Context context, int cardGridSize, String currentMapName, TextureManager textureManager, Point displaySize, int gridOffsetY) {
        Tablexia tablexia = (Tablexia) context.getApplicationContext();
        gridOffset = new Point();
        gridOffset.y = gridOffsetY;

        cardWidth = (displaySize.y - (2 * gridOffset.y)) / cardGridSize;

        // Odsazeni na ose x se vypocita z velikosti karet a sirky displeje
        gridOffset.x = (displaySize.x - (cardGridSize * cardWidth)) / 2;

        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath(TEXTURES_BASE_PATH);
        mCardDeckTexture = new BitmapTextureAtlas(textureManager, 1300, 1300, TextureOptions.BILINEAR);
        BitmapTextureAtlasTextureRegionFactory.createFromSource(mCardDeckTexture, ZipFileBitmapTextureAtlasSource.create(tablexia.getZipResourceFile(), TEXTURES_BASE_PATH + currentMapName), 0, 0);
        mCardDeckTexture.load();

        cards = new ArrayList<Card>();
        mCardTotextureRegionMap = new HashMap<Card, ITextureRegion>();
        cardTextureWidth = mCardDeckTexture.getWidth() / cardGridSize;

        mCardTotextureRegionMap.clear();
        cards.clear();
        /*
         * Pro vsechny karty vytvori textury z predaneho obrazku, ulozi je do
         * mCardToTextureRegionMap Rovnez se zde nastavi pocatecni otoceni karet
         * a nastavi jejich pozice
         */
        for (int i = 0; i < (cardGridSize * cardGridSize); i++) {
            Card card = new Card(i, cardGridSize);
            final ITextureRegion cardTextureRegion = TextureRegionFactory.extractFromTexture(mCardDeckTexture, card.getGridPositionX() * cardTextureWidth, ((cardGridSize - 1) * cardTextureWidth) - (card.getGridPositionY() * cardTextureWidth), cardTextureWidth, cardTextureWidth);
            mCardTotextureRegionMap.put(card, cardTextureRegion);
            card.setCardWidth(cardWidth);
            card.setCardHeight(cardWidth);
            cards.add(card);
        }
    }

    public HashMap<Card, ITextureRegion> getmCardTotextureRegionMap() {
        return mCardTotextureRegionMap;
    }

    public ArrayList<Card> getCards() {
        return cards;
    }

    public Point getGridOffset() {
        return gridOffset;
    }

    public int getCardWidth() {
        return cardWidth;
    }

}
