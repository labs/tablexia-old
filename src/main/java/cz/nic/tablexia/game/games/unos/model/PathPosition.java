/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.unos.model;


/**
 * @author lhoracek
 */
public class PathPosition extends Position {
    private static final long serialVersionUID = -6260467907731135797L;
    private final boolean     accessible;

    public PathPosition(int x, int y, boolean accessible) {
        super(x, y);
        this.accessible = accessible;
    }

    public PathPosition(Position position, boolean accessible) {
        this(position.getX(), position.getY(), accessible);
    }

    public boolean isAccessible() {
        return accessible;
    }

}
