
package cz.nic.tablexia.game.games.strelnice.model.effects;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;
import org.andengine.util.modifier.IModifier.IModifierListener;

import cz.nic.tablexia.game.games.strelnice.media.TextureType;

public class Cloud extends Sprite {

    private static final float        EFFECT_DURATION = 1f;
    public static final TextureType[] TEXTURES        = new TextureType[] { TextureType.BOX_SMOKE_1, TextureType.BOX_SMOKE_2, TextureType.BOX_SMOKE_3, TextureType.BOX_SMOKE_4, TextureType.BOX_SMOKE_5 };

    private int                       cloudIndex;

    public Cloud(int cloudNumber, float pX, float pY, float pWidth, float pHeight, ITextureRegion pTextureRegion, VertexBufferObjectManager vertexBufferObjectManager) {
        super(pX, pY, pWidth, pHeight, pTextureRegion, vertexBufferObjectManager);
        cloudIndex = cloudNumber;
    }

    @Override
    public void onAttached() {
        super.onAttached();
        registerEntityModifier((cloudIndex == (TEXTURES.length - 1)) ? entityModifierAlpha : entityModifier);
        entityModifier.addModifierListener(new IModifierListener<IEntity>() {

            @Override
            public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
            }

            @Override
            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                if (effectOverListener != null) {
                    effectOverListener.onEffectOver(Cloud.this);
                }
            }
        });
    }

    public int getCloudIndex() {
        return cloudIndex;
    }

    private IEntityModifier     entityModifier      = new ParallelEntityModifier(new ScaleModifier(EFFECT_DURATION, 1f, 1.3f));
    private IEntityModifier     entityModifierAlpha = new ParallelEntityModifier(new AlphaModifier(EFFECT_DURATION, 1, 0));

    private OnCloudOverListener effectOverListener;

    public void setOnCloudOverListener(OnCloudOverListener effectOverListener) {
        this.effectOverListener = effectOverListener;
    }

    public static interface OnCloudOverListener {
        public void onEffectOver(Cloud effect);
    }
}
