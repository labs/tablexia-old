/**
 *
 */

package cz.nic.tablexia.game.games.strelnice.events;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.scene.Scene;

import cz.nic.tablexia.game.games.strelnice.StrelniceActivity;
import cz.nic.tablexia.game.games.strelnice.generator.TargetGenerator;
import cz.nic.tablexia.game.games.strelnice.media.TextureType;
import cz.nic.tablexia.game.games.strelnice.model.Direction;
import cz.nic.tablexia.game.games.strelnice.model.Target;
import cz.nic.tablexia.game.games.strelnice.model.Wave;

/**
 * Updater pozice
 * V každém ticku enginu projde květiny a těm které jsou na obrazovce aktualizuje pozici podle uběhnutého času.
 * Květiny které by měly přijet do obrazu attachne na wcénu a odjíždějící naopak odstraní
 *
 * @author lhoracek
 */
public class TargetPositionUpdateHandler implements IUpdateHandler {
    private static final String     TAG                = TargetPositionUpdateHandler.class.getSimpleName();
    public static final float       GAME_SPEED_FAST    = 2.0f;
    public static final float       GAME_SPEED_NORMAL  = 1f;
    public static final float       GAME_SPEED_SLOW    = 0.5f;
    public static final float       GAME_SPEED_TIMEOUT = 5f;

    private float                   elapsedTime        = 0;
    private final List<Target>      targets;
    private final float             width;
    private final float             height;
    private final float             baseHeight;
    private final Scene             scene;
    private final StrelniceActivity parent;
    private float                   gameSpeed          = 1;
    private final TargetGenerator   targetGenerator;

    private final Random            random             = new Random();

    public TargetPositionUpdateHandler(List<Target> targets, float width, float height, float baseHeight, Scene scene, StrelniceActivity parent, TargetGenerator targetGenerator) {
        super();
        this.targets = targets;
        this.width = width;
        this.height = height;
        this.baseHeight = baseHeight;
        this.scene = scene;
        this.parent = parent;
        this.targetGenerator = targetGenerator;
    }

    public void setGameSpeed(float gameSpeed) {
        this.gameSpeed = gameSpeed;
    }

    public void resetGameSpeed() {
        gameSpeed = GAME_SPEED_NORMAL;
    }

    /*
     * (non-Javadoc)
     * @see org.andengine.engine.handler.IUpdateHandler#onUpdate(float)
     */
    @Override
    public void onUpdate(float pSecondsElapsed) {
        elapsedTime += (pSecondsElapsed * gameSpeed);

        List<Target> toRemove = new ArrayList<Target>();
        List<Target> toAdd = new ArrayList<Target>();
        List<Target> invisibles = new ArrayList<Target>();
        Set<TextureType> availableTypes = new HashSet<TextureType>();
        Set<Wave> waves = new HashSet<Wave>();
        for (Target target : targets) {
            float x = target.getWave().getX(elapsedTime - target.getStartTime(), width);
            if ((x > 0) && (x < width)) {
                // cil je na scene
                if (target.getParent() == null) {
                    // jeste nebyl vykreslen, tak ho pridame do sceny
                    scene.attachChild(target);
                    scene.registerTouchArea(target);
                    scene.sortChildren();
                    target.setOnAreaTouchedListener(parent);
                    if (targetGenerator.getLastTargetInWave(target.getWave()).equals(target)) {
                        toAdd.add(targetGenerator.addNewTargetToWave(target.getWave(), parent.getVertexBufferObjectManager(), null));
                    }
                }
                target.setX(x);
                target.setY((baseHeight - ((height * StrelniceActivity.WAVE_VERTICAL_OFFSET) / 2)) + target.getWave().getY(elapsedTime - target.getStartTime(), height) + (target.getWave().getVerticalOrder() * (height * StrelniceActivity.WAVE_VERTICAL_OFFSET)));

                if (target.getEffect() != null) {
                    target.getEffect().setX(x);
                }
                // koukame pouze na první polovinu obrazovky a ukladama si dostupne kytky
                if ((x > (Direction.RIGHT == target.getWave().getDirection() ? 0 : width / 2)) && (x < (Direction.RIGHT == target.getWave().getDirection() ? width / 2 : width))) {
                    // pouze nesestřelené
                    if (!target.isShot()) {
                        availableTypes.add(target.getTextureType());
                    }
                }
                waves.add(target.getWave());
            } else {
                if (target.getParent() != null) {
                    // je na scene, ale je mimo jeji sirku, tak ho odebereme ze sceny
                    final Target flowerToRemove = target;
                    parent.runOnUpdateThread(new Runnable() {
                        @Override
                        public void run() {
                            scene.unregisterTouchArea(flowerToRemove);
                            flowerToRemove.detachSelf();
                        }
                    });
                } else {
                    if (((target.getWave().getDirection() == Direction.LEFT) && (x < 0)) || ((target.getWave().getDirection() == Direction.RIGHT) && (x > width))) {
                        // už je mimo obraz
                        toRemove.add(target);
                    } else {
                        // teprve přichází do obrazu
                        availableTypes.add(target.getTextureType()); // TODO zkusime vypnout ocekavane kytky, aby jich rovnou pridaval vic
                        invisibles.add(target);
                    }
                }
            }
        }

        targets.removeAll(toRemove);
        targets.addAll(toAdd);
        invisibles.addAll(toAdd);

        if ((parent.getCurrentTarget() != null) && !availableTypes.contains(parent.getCurrentTarget().first)) {
            if (invisibles.size() > 0) {
                int index = random.nextInt(invisibles.size());
                Target swap = invisibles.get(index);
                Target swapTo = targetGenerator.addNewTargetToWave(swap.getWave(), parent.getVertexBufferObjectManager(), parent.getCurrentTarget().first, swap.getStartTime(), targetGenerator.getLastTargetInWave(swap.getWave()).equals(swap));
                targets.remove(swap);
                targets.add(swapTo);
            } else {
                targets.add(targetGenerator.addNewTargetToWave(waves.toArray(new Wave[0])[random.nextInt(waves.size())], parent.getVertexBufferObjectManager(), parent.getCurrentTarget().first));
            }
        }

    }

    /*
     * (non-Javadoc)
     * @see org.andengine.engine.handler.IUpdateHandler#reset()
     */
    @Override
    public void reset() {}

}
