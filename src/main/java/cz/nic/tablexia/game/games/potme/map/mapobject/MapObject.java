/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.potme.map.mapobject;

import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import cz.nic.tablexia.game.games.potme.PotmeActivity;
import cz.nic.tablexia.game.games.potme.ResourceManager;
import cz.nic.tablexia.game.games.potme.map.tile.Tile;


/**
 * Map object
 * 
 * @author Matyáš Latner
 *
 */
public class MapObject {
	
	private static final int 	QUARTER_ROTATION 	= 90;

	
	private MapObjectType 		mapObjectType;
	private Sprite				mapObjectSprite;
	private Tile 				tile;
	private boolean				enabled;

	public MapObject(Tile tile, MapObjectType mapObjectType) {
		this.tile = tile;
		this.mapObjectType = mapObjectType;
		setEnable(true);
	}

	public MapObjectType getMapObjectType() {
		return mapObjectType;
	}
	
	@Override
	public String toString() {
		return "MapObject[" + mapObjectType.name() + "]";
	}
	
	public void setEnable(boolean enabled) {
		this.enabled = enabled;
		if (mapObjectSprite != null) {
			mapObjectSprite.setVisible(enabled);
		}
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	/**
	 * Removes current object from map
	 * @param potmeActivity
	 */
	public void removeFromMap(PotmeActivity potmeActivity) {
		if (mapObjectSprite != null) {			
			mapObjectSprite.setVisible(false);
			potmeActivity.runOnUpdateThread(new Runnable() {
				
				@Override
				public void run() {
					mapObjectSprite.detachSelf();
					mapObjectSprite = null;
				}
			});
		}
	}
	
	/**
	 * Returns sprite of current map object for rendering.
	 * @param initialPositionX X initial position to render from
	 * @param initialPositionY Y initial position to render from
	 * @param vertexBufferObjectManager
	 * @return
	 */
	public Sprite getMapObjectSprite(VertexBufferObjectManager vertexBufferObjectManager) {
		if (mapObjectSprite == null) {
			mapObjectSprite = new Sprite(0, 0, PotmeActivity.TILE_SIZE * mapObjectType.getObjectSizeRation(), PotmeActivity.TILE_SIZE * mapObjectType.getObjectSizeRation(), ResourceManager.getInstance().getTexture(mapObjectType.getTexture()), vertexBufferObjectManager);
			
			Sprite tileSprite = tile.getTileSprite(vertexBufferObjectManager);
			float tileSpriteWidth = tileSprite.getWidth();
			float tileSPriteHeight = tileSprite.getHeight();
			
			mapObjectSprite.setPosition((tile.getMapPositionX() * tileSpriteWidth) + tileSpriteWidth / 2, - ((tile.getMapPositionY() * tileSPriteHeight) + tileSPriteHeight / 2));
			if (getMapObjectType().isRotable()) {				
				mapObjectSprite.setRotation(tile.getTileType().getRotation() * QUARTER_ROTATION);
			}
		}
		return mapObjectSprite;
	}

}
