/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.pronasledovani.model;

import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.vbo.ISpriteVertexBufferObject;
import org.andengine.opengl.shader.ShaderProgram;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.DrawType;
import org.andengine.opengl.vbo.VertexBufferObjectManager;


public class TaggedSprite extends Sprite {


    /*
     * Pridany konstruktor, ktery umoznuje vytvorit sprite s Tagem
     */

    public TaggedSprite(final float pX, final float pY,final Card pCard, final ITextureRegion pTextureRegion, final VertexBufferObjectManager pVertexBufferObjectManager,int spriteTag) {
        this(pX, pY, pCard.getCardWidth(), pCard.getCardHeight(), pTextureRegion, pVertexBufferObjectManager, DrawType.STATIC);

        setTag(spriteTag);
    }

    public TaggedSprite(final float pX, final float pY, final ITextureRegion pTextureRegion, final VertexBufferObjectManager pVertexBufferObjectManager, int spriteTag) {
        this(pX, pY, pTextureRegion, pVertexBufferObjectManager, DrawType.STATIC);

        setTag(spriteTag);
    }

    public TaggedSprite(float pX, float pY, ITextureRegion pTextureRegion,
            VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
    }

    public TaggedSprite(float pX, float pY, ITextureRegion pTextureRegion,
            ISpriteVertexBufferObject pVertexBufferObject) {
        super(pX, pY, pTextureRegion, pVertexBufferObject);
    }

    public TaggedSprite(float pX, float pY, ITextureRegion pTextureRegion,
            VertexBufferObjectManager pVertexBufferObjectManager,
            ShaderProgram pShaderProgram) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager,
                pShaderProgram);
    }

    public TaggedSprite(float pX, float pY, ITextureRegion pTextureRegion,
            VertexBufferObjectManager pVertexBufferObjectManager,
            DrawType pDrawType) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager, pDrawType);
    }

    public TaggedSprite(float pX, float pY, ITextureRegion pTextureRegion,
            ISpriteVertexBufferObject pVertexBufferObject,
            ShaderProgram pShaderProgram) {
        super(pX, pY, pTextureRegion, pVertexBufferObject, pShaderProgram);
    }

    public TaggedSprite(float pX, float pY, ITextureRegion pTextureRegion,
            VertexBufferObjectManager pVertexBufferObjectManager,
            DrawType pDrawType, ShaderProgram pShaderProgram) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager, pDrawType,
                pShaderProgram);

    }

    public TaggedSprite(float pX, float pY, float pWidth, float pHeight,
            ITextureRegion pTextureRegion,
            VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pWidth, pHeight, pTextureRegion,
                pVertexBufferObjectManager);

    }

    public TaggedSprite(float pX, float pY, float pWidth, float pHeight,
            ITextureRegion pTextureRegion,
            ISpriteVertexBufferObject pSpriteVertexBufferObject) {
        super(pX, pY, pWidth, pHeight, pTextureRegion,
                pSpriteVertexBufferObject);

    }

    public TaggedSprite(float pX, float pY, float pWidth, float pHeight,
            ITextureRegion pTextureRegion,
            VertexBufferObjectManager pVertexBufferObjectManager,
            ShaderProgram pShaderProgram) {
        super(pX, pY, pWidth, pHeight, pTextureRegion,
                pVertexBufferObjectManager, pShaderProgram);

    }

    public TaggedSprite(float pX, float pY, float pWidth, float pHeight,
            ITextureRegion pTextureRegion,
            VertexBufferObjectManager pVertexBufferObjectManager,
            DrawType pDrawType) {
        super(pX, pY, pWidth, pHeight, pTextureRegion,
                pVertexBufferObjectManager, pDrawType);

    }

    public TaggedSprite(float pX, float pY, float pWidth, float pHeight,
            ITextureRegion pTextureRegion,
            ISpriteVertexBufferObject pSpriteVertexBufferObject,
            ShaderProgram pShaderProgram) {
        super(pX, pY, pWidth, pHeight, pTextureRegion,
                pSpriteVertexBufferObject, pShaderProgram);

    }

    public TaggedSprite(float pX, float pY, float pWidth, float pHeight,
            ITextureRegion pTextureRegion,
            VertexBufferObjectManager pVertexBufferObjectManager,
            DrawType pDrawType, ShaderProgram pShaderProgram) {
        super(pX, pY, pWidth, pHeight, pTextureRegion,
                pVertexBufferObjectManager, pDrawType, pShaderProgram);

    }

}
