/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.pronasledovani.model;

import java.util.concurrent.TimeUnit;

public class MyTime {
    private long days;
    private long hours;
    private long minutes;
    private long seconds;

    public MyTime(float secs) {
        setSeconds((long) secs);
        setDays(TimeUnit.SECONDS.toDays(getSeconds()));
        setHours(TimeUnit.SECONDS.toHours(getSeconds()) - (getDays() * 24));
        setMinutes(TimeUnit.SECONDS.toMinutes(getSeconds()) - (TimeUnit.SECONDS.toHours(getSeconds()) * 60));
        setSeconds(TimeUnit.SECONDS.toSeconds(getSeconds()) - (TimeUnit.SECONDS.toMinutes(getSeconds()) * 60));
    }

    public long getDays() {
        return days;
    }

    public void setDays(long days) {
        this.days = days;
    }

    public long getHours() {
        return hours;
    }

    public void setHours(long hours) {
        this.hours = hours;
    }

    public long getMinutes() {
        return minutes;
    }

    public void setMinutes(long minutes) {
        this.minutes = minutes;
    }

    public long getSeconds() {
        return seconds;
    }

    public void setSeconds(long seconds) {
        this.seconds = seconds;
    }

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder(" ");
        if(hours!=0) {
            builder.append((int) hours + " hod ");
        }
        if(minutes!=0) {
            builder.append((int) minutes + " min ");
        }
        if(seconds!=0) {
            builder.append((int) seconds + " s ");
        }
        return builder.toString();
    }


}
