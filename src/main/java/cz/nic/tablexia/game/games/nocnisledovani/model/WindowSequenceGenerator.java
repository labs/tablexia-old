/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.nocnisledovani.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import android.util.Pair;

public class WindowSequenceGenerator {
    private static final int            MIN_TIME = 1;
    private static final int            MAX_TIME = 11;
    private Map<Integer, List<Integer>> generatedTimesAndWindows;

    public WindowSequenceGenerator() {
        generatedTimesAndWindows = new HashMap<Integer, List<Integer>>();
    }

    public Map<Integer, List<Integer>> getRandomWindowAndTimeList(int numberOfWindowsInScene, int numberOfWindowsLightenedAtOnce, int numberOfRoundsPerScene) {
        generatedTimesAndWindows.clear();
        int time;
        ArrayList<Integer> windowsLightenedUp = new ArrayList<Integer>();

        while (generatedTimesAndWindows.size() < numberOfRoundsPerScene) {
            Random r = new Random();
            time = r.nextInt((MAX_TIME - MIN_TIME) + 1) + MIN_TIME;
            while (windowsLightenedUp.size() < numberOfWindowsLightenedAtOnce) {
                int next = r.nextInt(numberOfWindowsInScene);
                if (!windowsLightenedUp.contains(next)) {
                    windowsLightenedUp.add(next);
                }
            }
            generatedTimesAndWindows.put(time, new ArrayList<Integer>(windowsLightenedUp));
            windowsLightenedUp.clear();
        }
        return generatedTimesAndWindows;
    }
}
