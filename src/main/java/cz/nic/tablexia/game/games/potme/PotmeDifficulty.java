package cz.nic.tablexia.game.games.potme;

import java.util.Arrays;
import java.util.List;

import android.util.Log;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.potme.action.ActionType;
import cz.nic.tablexia.game.games.potme.map.IMapProvider;
import cz.nic.tablexia.game.games.potme.map.MapGenerator;
import cz.nic.tablexia.game.games.potme.map.MapTutorial;

/**
 * Game potme difficulty definition and mapping to game difficulty
 * 
 * @author Matyáš Latner
 *
 */
public enum PotmeDifficulty {
	
	TUTORIAL(null, 					false,	MapTutorial.class,  1, Arrays.asList(new ActionType[]{})),
	EASY	(GameDifficulty.EASY, 	true, 	MapGenerator.class, 1, Arrays.asList(new ActionType[]{})),
	MEDIUM	(GameDifficulty.MEDIUM, true, 	MapGenerator.class, 1, Arrays.asList(new ActionType[]{ActionType.KEY})),
	HARD	(GameDifficulty.HARD, 	true, 	MapGenerator.class, 2, Arrays.asList(new ActionType[]{ActionType.KEY}));
	
	private GameDifficulty gameDifficulty;
	private int floorCount;
	private List<ActionType> difficultyActionTypes;
	private Class<? extends IMapProvider> mapProviderClass;
	private boolean hasResults;

	private PotmeDifficulty(GameDifficulty gameDifficulty, boolean hasResults, Class<? extends IMapProvider> mapProviderClass, int floorCount, List<ActionType> difficultyActionTypes) {
		this.gameDifficulty			= gameDifficulty;
		this.hasResults 			= hasResults;
		this.mapProviderClass 		= mapProviderClass;
		this.floorCount 			= floorCount;
		this.difficultyActionTypes 	= difficultyActionTypes;
	}
	
	public boolean hasResults() {
		return hasResults;
	}
	
	public int getFloorCount() {
		return floorCount;
	}
	
	public boolean hasDifficultyActionType(ActionType actionType) {
		return difficultyActionTypes.contains(actionType);
	}
	
	public static PotmeDifficulty getPotmeDifficultyForGameDifficulty(GameDifficulty gameDifficulty, boolean isFirstGame) {
		if (isFirstGame) {
			return TUTORIAL;
		}
		for (PotmeDifficulty potmeDifficulty : PotmeDifficulty.values()) {
			if (potmeDifficulty.gameDifficulty == gameDifficulty) {
				return potmeDifficulty;
			}
		}
		return null;
	}
	
	public IMapProvider getMapProviderNewInstance(RandomAccess randomAccess) {
		try {
			return mapProviderClass.getConstructor(RandomAccess.class).newInstance(randomAccess);
		} catch (Exception e) {
			Log.e(PotmeDifficulty.class.getCanonicalName(), "Cannot create instance for IMapProvider class: " + mapProviderClass, e);
		}
		return null;
	}
	
}
