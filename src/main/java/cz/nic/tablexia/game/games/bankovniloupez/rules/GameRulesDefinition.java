/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.rules;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.Context;

import com.activeandroid.util.Log;

import cz.nic.tablexia.R;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.bankovniloupez.rules.easy.BC_1_BCRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.easy.Bb_0_BbRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.easy.Bt_0_BtRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.easy.CACARule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.easy.CACCRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.easy.CA_0_CC_0_TRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.easy.CCCCRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.easy.CC_0_CC_0_TRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.easy.CC_0_notCCRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.hard.Bb_1_BbRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.hard.Bt_1_BtRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.hard.CCCCnotCCRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.hard.CC_0_CC_0_CCRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.hard.CC_0_notCA_0_TRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.hard.CC_0_notCC_0_TRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.hard.CC_2_CCRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.medium.BC_2_BCRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.medium.CA_1_CCRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.medium.CCCCCCRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.medium.CC_1_CCRule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.medium.CC_1_notCARule;
import cz.nic.tablexia.game.games.bankovniloupez.rules.medium.CC_1_notCCRule;


/**
 * Game rules definition enum
 * 
 * @author Matyáš Latner
 */
public enum GameRulesDefinition {

	/*
	 *	Generované atributy:
	 *		A - náhodný atribut (generovaný pro každou postavu zlášť)
	 *		B - náhodný atribut stejný u skupiny (generuje se zvlášť pro každou skupinu postav, která obsahuje zloděje a postavy, které identifikují zloděje --> v rámci této skupiny je atribut stejný)
	 *		C - stejný atribut (generovaný jen na začátku hry a pro všechny postavy v rámci jedné hry je stejný --> vygenerovaný atribut je součástí textu pravidla)
     *
	 *	Konstantní atributy pro komponenty:
	 *		t  - skupina komponent TOP
	 *		b  - skupina komponent BOTTOM
     *
     *
     *
     *
	 *	Gramatika pro sestavování pravidel:
	 *	
	 *	    s  --> w
	 *	    w  --> gx
	 *	    x  --> e | w | y
	 *		y  --> _n_w | _n_T
	 *	    n  --> 0, 1, 2, ...
	 *	    g  --> CA | notCA
	 *	    C  --> A | B | C          (barva komponenty)
	 *	    A  --> A | B | C | t | b  (komponenta)
     *
	 *
	 *
	 *
	 *	Ukázka pravidla: "1.1 Pokud po člověku, který má na sobě něco zeleného, přichází člověk s hnědými vlasy, tak za ním v pořadí je zloděj."
	 *
	 *
	 *		 barva atributu
	 *	    	 \
	 *	      	  \   typ komponenty
	 *	       	   \  / 
	 *		        CA_0_CC_0_T <--- poslední postava (zloděj je vždy poslední postava)
	 *	            /   \ \
	 *	           /     \ druhá postava
	 *	          /       \
	 *	         /      počet náhodných postav mezi první a druhou postavou
	 *	        /
	 *	     první postava
	 *	
	 *	
	 *	     C -> stejná barva celou hru ("má na sobě něco zeleného") --> barva se generuje jednou na začátku hry
	 *	      \
     *         \    A -> náhodně vygenerovaná komponenta zlášť pro každou postavu ("má na sobě něco zeleného") --> každá postava může mít jinou komponentu
     *          \  / 
     *           CA_0_CC_0_T   <--- T -> zloděj namá určeno jak bude vypadat, jen pořadí ("tak za ním v pořadí je zloděj")
     *               /  \
     *              /    C -> stejná komponenta po celou hru ("přichází člověk s hnědými vlasy") --> komponenta se generuje jednou na začátku hry
     *             /
     *            C -> stejná barva celou hru ("přichází člověk s hnědými vlasy") --> barva se generuje jednou na začátku hry
	 */
    CA_0_CC_0_T(GameDifficulty.EASY, CA_0_CC_0_TRule.class, R.string.game_bankovniloupez_rule_ca0cc0t),
    CC_0_CC_0_T(GameDifficulty.EASY, CC_0_CC_0_TRule.class, R.string.game_bankovniloupez_rule_cc0cc0t),
    CCCC(GameDifficulty.EASY, CCCCRule.class, R.string.game_bankovniloupez_rule_cccc),
    CACC(GameDifficulty.EASY, CACCRule.class, R.string.game_bankovniloupez_rule_cacc),
    CACA(GameDifficulty.EASY, CACARule.class, R.string.game_bankovniloupez_rule_caca),
    Bt_0_Bt(GameDifficulty.EASY, Bt_0_BtRule.class, R.string.game_bankovniloupez_rule_bx0bx),
    Bb_0_Bb(GameDifficulty.EASY, Bb_0_BbRule.class, R.string.game_bankovniloupez_rule_bx0bx),
    BC_1_BC(GameDifficulty.EASY, BC_1_BCRule.class, R.string.game_bankovniloupez_rule_bc1bc),
    CC_0_notCC(GameDifficulty.EASY, CC_0_notCCRule.class, R.string.game_bankovniloupez_rule_cc0notcc),
    
    CC_1_CC(GameDifficulty.MEDIUM, CC_1_CCRule.class, R.string.game_bankovniloupez_rule_cc1cc),
    CA_1_CC(GameDifficulty.MEDIUM, CA_1_CCRule.class, R.string.game_bankovniloupez_rule_ca1cc),
    BC_2_BC(GameDifficulty.MEDIUM, BC_2_BCRule.class, R.string.game_bankovniloupez_rule_bc2bc),
    CC_1_notCC(GameDifficulty.MEDIUM, CC_1_notCCRule.class, R.string.game_bankovniloupez_rule_cc1notcc),
    CC_1_notCA(GameDifficulty.MEDIUM, CC_1_notCARule.class, R.string.game_bankovniloupez_rule_cc1notca),
    CCCCCC(GameDifficulty.MEDIUM, CCCCCCRule.class, R.string.game_bankovniloupez_rule_cccccc),
    
    CC_2_CC(GameDifficulty.HARD, CC_2_CCRule.class, R.string.game_bankovniloupez_rule_cc2cc),
    CC_0_notCC_0_T(GameDifficulty.HARD, CC_0_notCC_0_TRule.class, R.string.game_bankovniloupez_rule_cc0notcc0t),
    CC_0_notCA_0_T(GameDifficulty.HARD, CC_0_notCA_0_TRule.class, R.string.game_bankovniloupez_rule_cc0notca0t),
    CC_0_CC_0_CC(GameDifficulty.HARD, CC_0_CC_0_CCRule.class, R.string.game_bankovniloupez_rule_cc0cc0cc),
    CCCCnotCC(GameDifficulty.HARD, CCCCnotCCRule.class, R.string.game_bankovniloupez_rule_ccccnotcc),
    Bt_1_Bt(GameDifficulty.HARD, Bt_1_BtRule.class, R.string.game_bankovniloupez_rule_bx1bx),
    Bb_1_Bb(GameDifficulty.HARD, Bb_1_BbRule.class, R.string.game_bankovniloupez_rule_bx1bx);

    private Class<? extends GameRule> gameRuleClass;
    private int                       gameRuleStringId;
    private GameDifficulty            difficulty;

    private GameRulesDefinition(GameDifficulty difficulty, Class<? extends GameRule> gameRuleClass, int gameRuleStringId) {
        this.difficulty = difficulty;
        this.gameRuleClass = gameRuleClass;
        this.gameRuleStringId = gameRuleStringId;
    }

    /**
     * Returns difficulty for rule
     * 
     * @return rule difficulty
     */
    public GameDifficulty getDifficulty() {
        return difficulty;
    }

    /**
     * Returns game rule class
     * 
     * @return rule class
     */
    public Class<? extends GameRule> getGameRuleClass() {
        return gameRuleClass;
    }

    /**
     * Returns game rule text
     * 
     * @return rule text
     */
    public String getRuleText(Context context) {
        return context.getString(gameRuleStringId);
    }

    /**
     * Returns new instance of game rule class
     * 
     * @return new instance of game rule class
     */
    public GameRule getGameRuleInstance(Context context, RandomAccess randomAccess, VertexBufferObjectManager vertexBufferObjectManager, int numberOfCreatures, int numberOfThieves) {
        try {
            return getGameRuleClass().getConstructor(Context.class, RandomAccess.class, VertexBufferObjectManager.class, int.class, int.class).newInstance(context, randomAccess, vertexBufferObjectManager, numberOfCreatures, numberOfThieves);
        } catch (Exception e) {
            Log.e("Cannot create instance of game rule: " + getGameRuleClass(), e);
        }
        return null;
    }

    /**
     * Returns random game rule for difficulty in parameter
     * 
     * @return random game rule for difficulty
     */
    public static GameRulesDefinition getRandomGameRuleForDifficulty(GameDifficulty difficulty, Random random) {
        List<GameRulesDefinition> gameRules = new ArrayList<GameRulesDefinition>();
        for (GameRulesDefinition gameRuleDefinition : GameRulesDefinition.values()) {
            if (gameRuleDefinition.getDifficulty() == difficulty) {
                gameRules.add(gameRuleDefinition);
            }
        }
        if (gameRules.size() > 0) {
            return gameRules.get(random.nextInt(gameRules.size()));
        }
        return null;
    }


}
