
package cz.nic.tablexia.game.games.strelnice.model;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.strelnice.StrelniceActivity;

public enum Wave {
    /* --------------- LEVEL EASY --------------------- */
    WAVE_1_1(GameDifficulty.EASY, 0, 7, 5, Direction.LEFT, new PositionProvider() {
        @Override
        public float getY(float elapsedTime, float maxHeight) {
            return (float) ((Math.sin((((elapsedTime / WAVE_1_1.getRunningTime()) * Math.PI * 2) * 1.5f) - 0.5) * maxHeight) / 30);
        }
    }), //
    WAVE_1_2(GameDifficulty.EASY, 1, 5, 7, Direction.LEFT, new PositionProvider() {
        @Override
        public float getY(float elapsedTime, float maxHeight) {
            return (float) ((Math.sin((((elapsedTime / WAVE_1_2.getRunningTime()) * Math.PI * 2) * 2.3f) + 0.5) * maxHeight) / 30);
        }
    }), //
    WAVE_1_3(GameDifficulty.EASY, 2, 6, 7, Direction.LEFT, new PositionProvider() {
        @Override
        public float getY(float elapsedTime, float maxHeight) {
            return (float) ((Math.sin((((elapsedTime / WAVE_3_3.getRunningTime()) * Math.PI * 2) * 1.5f) - 0.5) * maxHeight) / 20);
        }
    }), //

    /* --------------- LEVEL MEDIUM --------------------- */
    WAVE_2_1(GameDifficulty.MEDIUM, 0, 6, 5, Direction.LEFT, new PositionProvider() {
        @Override
        public float getY(float elapsedTime, float maxHeight) {
            return (float) ((Math.sin((((elapsedTime / WAVE_2_1.getRunningTime()) * Math.PI * 2) * 1.5f) - 0.5) * maxHeight) / 30);
        }
    }), //
    WAVE_2_2(GameDifficulty.MEDIUM, 1, 4, 7, Direction.RIGHT, new PositionProvider() {
        @Override
        public float getY(float elapsedTime, float maxHeight) {
            return (float) ((Math.sin((((elapsedTime / WAVE_2_2.getRunningTime()) * Math.PI * 2) * 2.3f) - 0.5) * maxHeight) / 30);
        }
    }), //
    WAVE_2_3(GameDifficulty.MEDIUM, 2, 5, 7, Direction.RIGHT, new PositionProvider() {
        @Override
        public float getY(float elapsedTime, float maxHeight) {
            return (float) ((Math.sin((((elapsedTime / WAVE_3_3.getRunningTime()) * Math.PI * 2) * 1.5f) - 0.5) * maxHeight) / 20);
        }
    }), //

    /* --------------- LEVEL MEDIUM --------------------- */
    WAVE_3_1(GameDifficulty.HARD, 0, 4, 10, Direction.LEFT, new PositionProvider() {
        @Override
        public float getY(float elapsedTime, float maxHeight) {
            return (float) ((Math.sin((((elapsedTime / WAVE_3_1.getRunningTime()) * Math.PI * 2) * 1.5f) - 0.5) * maxHeight) / 30);
        }
    }), //
    WAVE_3_2(GameDifficulty.HARD, 1, 3, 7, Direction.RIGHT, new PositionProvider() {
        @Override
        public float getY(float elapsedTime, float maxHeight) {
            return (float) ((Math.sin((((elapsedTime / WAVE_3_2.getRunningTime()) * Math.PI * 2) * 2.3f) - 0.5) * maxHeight) / 20);
        }
    }), //
    WAVE_3_3(GameDifficulty.HARD, 2, 5, 5, Direction.LEFT, new PositionProvider() {
        @Override
        public float getY(float elapsedTime, float maxHeight) {
            return (float) ((Math.sin((((elapsedTime / WAVE_3_3.getRunningTime()) * Math.PI * 2) * 1.5f) - 0.5) * maxHeight) / 20);
        }
    }); //

    private static final Wave[] EASY_WAVES   = new Wave[] { WAVE_1_1, WAVE_1_2, WAVE_1_3 };
    private static final Wave[] MEDIUM_WAVES = new Wave[] { WAVE_2_1, WAVE_2_2, WAVE_2_3 };
    private static final Wave[] HARD_WAVES   = new Wave[] { WAVE_3_1, WAVE_3_2, WAVE_3_3 };

    public static Wave getWave(GameDifficulty difficulty, int wavePosition) {
        switch (difficulty) {
            case EASY:
                return EASY_WAVES[wavePosition];
            case MEDIUM:
                return MEDIUM_WAVES[wavePosition];
            case HARD:
                return HARD_WAVES[wavePosition];
            default:
                throw new IllegalStateException("Unknow difficulty");
        }
    }

    private final int              verticalOrder;
    private final float            runningTime;
    private final Direction        direction;
    private final PositionProvider positionProvider;
    private final int              flowerOnScreen;
    private final GameDifficulty   difficulty;

    private Wave(GameDifficulty difficulty, int verticalOrder, float runningTime, int flowersOnScreen, Direction direction, PositionProvider positionProvider) {
        this.verticalOrder = verticalOrder;
        this.runningTime = runningTime;
        this.direction = direction;
        this.positionProvider = positionProvider;
        flowerOnScreen = flowersOnScreen;
        this.difficulty = difficulty;
    }

    public float getRunningTime() {
        return runningTime;
    }

    public int getVerticalOrder() {
        return verticalOrder;
    }

    public float getY(float elapsedTime, float maxHeight) {
        return positionProvider.getY(elapsedTime, maxHeight);
    }

    public float getX(float elapsedTime, float maxWidth) {
        return ((Direction.LEFT == direction ? -1 : 1) * (maxWidth * (elapsedTime / getRunningTime()))) + (Direction.LEFT == direction ? maxWidth : 0);
    }

    public int getzIndex() {
        switch (verticalOrder) {
            case 0:
                return StrelniceActivity.FLOWERS_WAVE1_ZINDEX;
            case 1:
                return StrelniceActivity.FLOWERS_WAVE2_ZINDEX;
            case 2:
                return StrelniceActivity.FLOWERS_WAVE3_ZINDEX;
            default:
                throw new IllegalArgumentException("Unknown z-index");
        }
    }

    public int getFlowerOnScreen() {
        return flowerOnScreen;
    }

    public Direction getDirection() {
        return direction;
    }

    public GameDifficulty getDifficulty() {
        return difficulty;
    }

}
