/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.unos.media;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.andengine.audio.music.Music;
import org.andengine.audio.music.MusicFactory;
import org.andengine.audio.music.MusicManager;
import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.audio.sound.SoundManager;
import org.andengine.audio.sound.exception.SoundReleasedException;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.AsyncTask;

import com.activeandroid.util.Log;
import com.splunk.mint.Mint;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.game.games.unos.UnosActivity;
import cz.nic.tablexia.game.games.unos.media.SoundType.SoundClass;

/**
 * @author lhoracek
 */
public class MfxManager {
    private static final String   TAG              = MfxManager.class.getSimpleName();

    private static final String   SOUNDS_DIR       = UnosActivity.BASE_DIR + "mfx/";
    public static final String    SOUNDS_EXTENSION = ".mp3";

    private Map<SoundType, Music> musics           = new HashMap<SoundType, Music>();
    private Map<SoundType, Sound> sounds           = new HashMap<SoundType, Sound>();

    private MusicManager          musicManager;
    private SoundManager          soundManager;
    private Context               context;

    public MfxManager(MusicManager musicManager, SoundManager soundManager, Context context) {
        super();
        this.musicManager = musicManager;
        this.soundManager = soundManager;
        this.context = context;
        SoundFactory.setAssetBasePath(SOUNDS_DIR);
        MusicFactory.setAssetBasePath(SOUNDS_DIR);
    }

    /**
     * Nahrat zvuky, ktere jsou oznacene jako eage, a nactou se automaticky, ostatni zvuky se nactou az ve chvili prvniho prehrani
     */
    public void loadSounds() {
        for (SoundType sound : SoundType.values()) {
            if (sound.isEager()) {
                loadSound(sound);
            }
        }
    }

    private void loadSound(SoundType sound) {

        Tablexia tablexia = (Tablexia) context.getApplicationContext();

        if (sound.getSoundClass() == SoundClass.SOUND) {

            Sound soundInstance = SoundFactory.createSoundFromAssetFileDescriptor(soundManager, tablexia.getZipResourceFile().getAssetFileDescriptor(SOUNDS_DIR + sound.getResource() + SOUNDS_EXTENSION));
            sounds.put(sound, soundInstance);

        } else {
            try {
                Log.v(TAG, "Loading sound " + sound.getResource());
                Music music = MusicFactory.createMusicFromAssetFileDescriptor(musicManager, tablexia.getZipResourceFile().getAssetFileDescriptor(SOUNDS_DIR + sound.getResource() + SOUNDS_EXTENSION));
                musics.put(sound, music);
            } catch (final IOException e) {
                Log.e(TAG, "Error loading sound " + e.getMessage() + " - resource: " + sound.getResource());
                // TODO remove TestFlight.log("Error loading sound " + e.getMessage() + " - resource " + sound.getResource());
            }
        }
    }

    public void playSound(SoundType sound) {
        playSound(sound, null);
    }

    /**
     * Prehraje zvuk z enumu, ktery muze byt preloadovan
     *
     * @param sound
     * @param onCompletionListener
     */
    public void playSound(SoundType sound, OnCompletionListener onCompletionListener) {
        if (sound.getSoundClass() == SoundClass.SOUND) {
            sounds.get(sound).play();
        } else {
            if (musics.get(sound) == null) {
                loadSound(sound);
            }
            Music music = musics.get(sound);
            if (onCompletionListener != null) {
                music.setOnCompletionListener(onCompletionListener);
            }
            music.play();
        }
    }

    /**
     * Prehraje zvuk z assetu definovany nazvem souboru
     *
     * @param sound
     * @param onCompletionListener
     */
    public void playSound(String sound, final OnCompletionListener onCompletionListener) {
        playSound(sound, onCompletionListener, false);
    }

    /**
     * Prehraje zvuk z assetu definovany nazvem souboru, pokud je parametr isRetry true, tak uz se nepokusi prehrat pri chybe znovu
     *
     * @param sound
     * @param onCompletionListener
     */
    public void playSound(String sound, final OnCompletionListener onCompletionListener, boolean isRetry) {
        Tablexia tablexia = (Tablexia) context.getApplicationContext();
        final String path = SOUNDS_DIR + sound + SOUNDS_EXTENSION;
        try {
            Music music = MusicFactory.createMusicFromAssetFileDescriptor(musicManager, tablexia.getZipResourceFile().getAssetFileDescriptor(path));
            music.setOnCompletionListener(new OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    if (onCompletionListener != null) {
                        onCompletionListener.onCompletion(mp);
                    }
                    mp.release();
                }
            });

            music.play();
        } catch (final IOException e) {
            Log.e(TAG, "Unable to load music file " + path);
            e.printStackTrace();
            if (!isRetry) {
                // FIX hack propripad chyby prehravani zvuku
                Mint.logException(e);

                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        try {
                            Sound sound = SoundFactory.createSoundFromPath(soundManager, path);
                            while (!sound.isLoaded()) {
                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    // TODO log?
                                }
                            }
                            sound.play();
                        } catch (SoundReleasedException e1) {
                            e1.printStackTrace();
                            Mint.logException(e1);
                        } catch (IOException e1) {
                            e1.printStackTrace();
                            Mint.logException(e1);
                        }

                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            // TODO log?

                        }

                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        super.onPostExecute(result);
                        if (onCompletionListener != null) {
                            onCompletionListener.onCompletion(null);
                        }
                    }
                }.execute();
                // playSound(sound, onCompletionListener, true);
            }
        }
    }
}
