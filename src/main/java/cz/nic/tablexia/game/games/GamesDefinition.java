/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import cz.nic.tablexia.R;
import cz.nic.tablexia.audio.resources.SpeechSounds;
import cz.nic.tablexia.game.GameActivity;
import cz.nic.tablexia.main.MainActivity;
import cz.nic.tablexia.menu.AbstractMenu.MenuChangeListener;
import cz.nic.tablexia.menu.MenuActivity;
import cz.nic.tablexia.menu.MenuEnum;
import cz.nic.tablexia.menu.mainmenu.MainMenuDefinition;
import cz.nic.tablexia.menu.mainmenu.screen.GameMenu;
import cz.nic.tablexia.menu.mainmenu.screen.gamemenu.BaseGameMenuFragment;

/**
 * Tablexia games definition
 *
 * @author Matyáš Latner
 */
public enum GamesDefinition implements MenuEnum {

    BANKOVNILOUPEZ(0, R.string.game_bankovniloupez, R.string.game_bankovniloupez_description, new int[] { R.drawable.screen_gamemenu_bankovniloupez_back, R.drawable.screen_gamemenu_bankovniloupez_mid, R.drawable.screen_gamemenu_bankovniloupez_fore }, R.drawable.screen_gamemenu_bankovniloupez_startbutton, R.drawable.screen_gamemenu_lupici_title, R.layout.game_bankovniloupez_loading, SpeechSounds.LOADING_BANKOVNILOUPEZ, false, R.string.game_bankovniloupez_score, R.string.game_bankovniloupez_averagescore, "menu/sfx/gamemenu_bankovniloupez_intro.mp3", cz.nic.tablexia.game.games.bankovniloupez.BankovniloupezActivity.class, BaseGameMenuFragment.class, 1),

    PRONASLEDOVANI(1, R.string.game_pronasledovani, R.string.game_pronasledovani_description, new int[] { R.drawable.screen_gamemenu_pronasledovani_back, R.drawable.screen_gamemenu_pronasledovani_mid, R.drawable.screen_gamemenu_pronasledovani_fore }, R.drawable.screen_gamemenu_pronasledovani_startbutton, R.drawable.screen_gamemenu_pronasledovani_title, R.layout.game_pronasledovani_loading, SpeechSounds.LOADING_PRONASLEDOVANI, true, R.string.game_score_duration, R.string.game_averagescore_duration, "menu/sfx/gamemenu_pronasledovani_intro.mp3", cz.nic.tablexia.game.games.pronasledovani.PronasledovaniActivity.class, BaseGameMenuFragment.class),

    UNOS(2, R.string.game_unos, R.string.game_unos_description, new int[] { R.drawable.screen_gamemenu_unos_back, R.drawable.screen_gamemenu_unos_mid, R.drawable.screen_gamemenu_unos_fore }, R.drawable.screen_gamemenu_unos_startbutton, R.drawable.screen_gamemenu_unos_title, R.layout.game_unos_loading, SpeechSounds.LOADING_UNOS, false, R.string.game_score_error, R.string.game_averagescore_error, "menu/sfx/gamemenu_unos_intro.mp3", cz.nic.tablexia.game.games.unos.UnosActivity.class, BaseGameMenuFragment.class),

    NOCNISLEDOVANI(3, R.string.game_nocnisledovani, R.string.game_nocnisledovani_description, new int[] { R.drawable.screen_gamemenu_nocnisledovani_back, R.drawable.screen_gamemenu_nocnisledovani_mid, R.drawable.screen_gamemenu_nocnisledovani_fore }, R.drawable.screen_gamemenu_sledovani_startbutton, R.drawable.screen_gamemenu_nocnisledovani_title, R.layout.game_nocnisledovani_loading, SpeechSounds.LOADING_NOCNISLEDOVANI, false, R.string.game_score_error, R.string.game_averagescore_error, "menu/sfx/gamemenu_nocnisledovani_intro.mp3", cz.nic.tablexia.game.games.nocnisledovani.NocniSledovaniActivity.class, BaseGameMenuFragment.class),

    STRELNICE(4, R.string.game_strelnice, R.string.game_strelnice_description, new int[] { R.drawable.screen_gamemenu_strelnice_back, R.drawable.screen_gamemenu_strelnice_mid, R.drawable.screen_gamemenu_strelnice_fore }, R.drawable.screen_gamemenu_strelnice_startbutton_unpressed, R.drawable.screen_gamemenu_strelnice_title, R.layout.game_strelnice_loading, SpeechSounds.LOADING_STRELNICE, false, R.string.game_score, R.string.game_averagescore, "menu/sfx/gamemenu_strelnice_intro.mp3", cz.nic.tablexia.game.games.strelnice.StrelniceActivity.class, BaseGameMenuFragment.class, 1),

    POTME(5, R.string.game_potme, R.string.game_potme_description, new int[] { R.drawable.screen_gamemenu_potme_back, R.drawable.screen_gamemenu_potme_mid, R.drawable.screen_gamemenu_potme_fore }, R.drawable.screen_gamemenu_potme_startbutton, R.drawable.screen_gamemenu_potme_title, R.layout.game_potme_loading, SpeechSounds.LOADING_POTME, false, R.string.game_score_error, R.string.game_averagescore_error, "menu/sfx/gamemenu_potme_intro.mp3", cz.nic.tablexia.game.games.potme.PotmeActivity.class, BaseGameMenuFragment.class);

    private static final int DEFAULT_CHART_ORIENTATION = -1;

    public static GamesDefinition[] getGames() {
        return new GamesDefinition[] { BANKOVNILOUPEZ, PRONASLEDOVANI, UNOS, NOCNISLEDOVANI, STRELNICE, POTME };
    }

    private int                           position;
    private int                           gameNameResourceId;
    private int                           gameDescriptionResourceId;
    private int[]                         gameImageResourceId;
    private int                           gameStartImageResourceId;
    private int                           gameTitleImageResourceId;
    private Class<? extends GameActivity> gameClass;
    private Class<? extends Fragment>     gameMenuFragmentClass;
    private int                           gameLoadingLayoutResourceId;
    private String                        gameLoadingSoundPath;
    private boolean                       isScoreDuration;
    private int                           scoreTextResourceId;
    private int                           averagescoreTextResourceId;
    private String                        introSoundPath;
    private int                           chartOrientation;

    /**
     * Creates new game menu item
     *
     * @param position
     *            game position in menu
     * @param gameNameResourceId
     *            game name resource id
     * @param gameClass
     *            game class
     */
    private GamesDefinition(int position, int gameNameResourceId, int gameDescriptionResourceId, int[] gameImageResourceId, int gameStartImageResourceId, int gameTitleImageResourceId, int gameLoadingLayoutResourceId, String gameLoadingSoundPath, boolean isScoreDuration, int scoreTextResourceId, int averagescoreTextResourceId, String introSoundPath, Class<? extends GameActivity> gameClass,
        Class<? extends Fragment> gameMenuFragmentClass) {
        this(position, gameNameResourceId, gameDescriptionResourceId, gameImageResourceId, gameStartImageResourceId, gameTitleImageResourceId, gameLoadingLayoutResourceId, gameLoadingSoundPath, isScoreDuration, scoreTextResourceId, averagescoreTextResourceId, introSoundPath, gameClass, gameMenuFragmentClass, DEFAULT_CHART_ORIENTATION);
    }

    private GamesDefinition(int position, int gameNameResourceId, int gameDescriptionResourceId, int[] gameImageResourceId, int gameStartImageResourceId, int gameTitleImageResourceId, int gameLoadingLayoutResourceId, String gameLoadingSoundPath, boolean isScoreDuration, int scoreTextResourceId, int averagescoreTextResourceId, String introSoundPath, Class<? extends GameActivity> gameClass,
        Class<? extends Fragment> gameMenuFragmentClass, int chartOrientation) {

        this.position = position;
        this.gameNameResourceId = gameNameResourceId;
        this.gameDescriptionResourceId = gameDescriptionResourceId;
        this.gameStartImageResourceId = gameStartImageResourceId;
        this.gameImageResourceId = gameImageResourceId;
        this.gameTitleImageResourceId = gameTitleImageResourceId;
        this.gameLoadingLayoutResourceId = gameLoadingLayoutResourceId;
        this.gameLoadingSoundPath = gameLoadingSoundPath;
        this.isScoreDuration = isScoreDuration;
        this.scoreTextResourceId = scoreTextResourceId;
        this.averagescoreTextResourceId = averagescoreTextResourceId;
        this.introSoundPath = introSoundPath;
        this.gameClass = gameClass;
        this.gameMenuFragmentClass = gameMenuFragmentClass;
        this.chartOrientation = chartOrientation;
    }

    /**
     * Returns game class
     *
     * @return game class
     */
    public Class<? extends GameActivity> getGameClass() {
        return gameClass;
    }

    public int getChartOrientation() {
        return chartOrientation;
    }

    /**
     * Returns game name resource id
     *
     * @return game name resource id
     */
    public int getGameNameResourceId() {
        return gameNameResourceId;
    }

    /**
     * Returns game description resource id
     *
     * @return game description resource id
     */
    public int getGameDescriptionResourceId() {
        return gameDescriptionResourceId;
    }

    /**
     * Returns game image resource id
     *
     * @return game image resource id
     */
    public int[] getGameImageResourceIds() {
        return gameImageResourceId;
    }

    /**
     * Returns start button image resource id
     *
     * @return start button image resource id
     */
    public int getGameStartImageResourceId() {
        return gameStartImageResourceId;
    }

    /**
     * Returns game title image resource id
     *
     * @return game title image resource id
     */
    public int getGameTitleImageResourceId() {
        return gameTitleImageResourceId;
    }

    /**
     * Returns game loading image resource id
     *
     * @return game loading image resource id
     */
    public int getGameLoadingLayoutResourceId() {
        return gameLoadingLayoutResourceId;
    }

    /**
     * Returns <code>true</code> if current game score is time duration
     *
     * @return <code>true</code> if current game score is time duration
     */
    public boolean isScoreDuration() {
        return isScoreDuration;
    }

    /**
     * Returns id of text resource for score text
     *
     * @return id of text resource for score text
     */
    public int getScoreTextResourceId() {
        return scoreTextResourceId;
    }

    /**
     * Returns id of text resource for average score text
     *
     * @return id of text resource for average score text
     */
    public int getAveragescoreTextResourceId() {
        return averagescoreTextResourceId;
    }

    /**
     * Returns path to game intro sound file
     *
     * @return path to game intro sound file
     */
    public String getIntroSoundPath() {
        return introSoundPath;
    }

    public String getGameLoadingSoundPath() {
        return gameLoadingSoundPath;
    }

    /**
     * Position of current game in menu
     *
     * @return game position
     */
    public int getPosition() {
        return position;
    }

    /**
     * Return number of games.
     *
     * @return number of games
     */
    public static int getGamesCount() {
        return GamesDefinition.getGames().length;
    }

    @Override
    public boolean showMenuScreen(MenuActivity menuActivity, boolean forceReplace, Integer parameter, boolean forceQuickTransition, final MenuChangeListener menuChangeListener) {
        GameMenu gameMenu = new GameMenu();

        if (parameter != null) {
            Bundle bundle = new Bundle();
            bundle.putInt(MainMenuDefinition.MAIN_MENU_SCREEN_PARAMETER_KEY, parameter);
            gameMenu.setArguments(bundle);
        }

        Fragment usedFragment = menuActivity.replaceScreenFragment(this, gameMenu, forceReplace, forceQuickTransition, menuChangeListener);
        if (usedFragment != null) {
            if (menuActivity instanceof MainActivity) {
                // gameMenu != usedFragment -> smooth scroll if fragment is already displayed
                ((GameMenu) usedFragment).selectGame(getPosition(), menuActivity, gameMenu != usedFragment);
            }
            return true;
        }
        return false;
    }

    /**
     * Returns game for position from parameter or <code>null</code>
     *
     * @param position position of game
     * @return game for position or <code>null</code>
     */
    public static GamesDefinition getGameAtPosition(int position) {
        for (GamesDefinition game : GamesDefinition.values()) {
            if (game.getPosition() == position) {
                return game;
            }
        }
        return null;
    }

    /**
     * No menu action needed
     */
    @Override
    public boolean performMenuAction(MenuActivity menuActivity) {
        return false;
    }

    public Class<? extends Fragment> getGameMenuFragmentClass() {
        return gameMenuFragmentClass;
    }

}
