/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.unos;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.util.modifier.IModifier;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import cz.nic.tablexia.R;
import cz.nic.tablexia.audio.resources.SpeechSounds;
import cz.nic.tablexia.game.GameActivity;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.GamesDefinition;
import cz.nic.tablexia.game.games.unos.MySmoothCamera.OnSmoothCenterFinishedListener;
import cz.nic.tablexia.game.games.unos.entity.DirectionSprite;
import cz.nic.tablexia.game.games.unos.generator.PathGenerator;
import cz.nic.tablexia.game.games.unos.generator.TextureType;
import cz.nic.tablexia.game.games.unos.generator.TileMapGenerator;
import cz.nic.tablexia.game.games.unos.media.DirectionSounds;
import cz.nic.tablexia.game.games.unos.media.DirectionSounds.SoundPack;
import cz.nic.tablexia.game.games.unos.media.GfxManager;
import cz.nic.tablexia.game.games.unos.media.MfxManager;
import cz.nic.tablexia.game.games.unos.media.SoundType;
import cz.nic.tablexia.game.games.unos.model.PathPosition;
import cz.nic.tablexia.game.games.unos.model.Position;
import cz.nic.tablexia.game.games.unos.model.Tile;
import cz.nic.tablexia.game.games.unos.model.TileGroup;
import cz.nic.tablexia.game.games.unos.model.enums.Direction;
import cz.nic.tablexia.util.BitmapHelper;
import cz.nic.tablexia.util.SerializableHelper;

public class UnosActivity extends GameActivity {
    private static final String      TAG                     = UnosActivity.class.getSimpleName();
    public static final String       BASE_DIR                = "unos/";

    private static final int         CAMERA_WIDTH            = 1900;

    public static final int          TILE_BASE_WIDTH         = 528;
    public static final int          TILE_BASE_HEIGHT        = 264;

    public static final int          ROAD_WIDTH              = 264;
    public static final int          ROAD_HEIGHT             = 132;

    public static final int          GAME_STOPS              = 5;                                                                                                                   // 5

    public static final int          MAP_WIDTH               = Math.max((3 * GAME_STOPS) + 2, 10);
    public static final int          MAP_HEIGHT              = Math.max((2 * GAME_STOPS) + 2, 5);

    private static final String      GAMESTATE_MAP_KEY       = "gamestate_map";
    private static final String      GAMESTATE_PATH_KEY      = "gamestate_path";
    private static final String      GAMESTATE_POSITION_KEY  = "gamestate_position";
    private static final String      GAMESTATE_SOUNDPACK_KEY = "gamestate_soundpack";
    private static final String      GAMESTATE_USED_KEY      = "gamestate_used";

    private static final int[]       GAMERULES_CUPS_EASY     = { 0, 1, 3 };
    private static final int[]       GAMERULES_CUPS_MEDIUM   = { 0, 2, 4 };
    private static final int[]       GAMERULES_CUPS_HARD     = GAMERULES_CUPS_MEDIUM;

    private static final String[]    GAMERULE_HELP_SOUNDS    = new String[] { SpeechSounds.UNOS_RULEHELP_EASY, SpeechSounds.UNOS_RULEHELP_MEDIUM, SpeechSounds.UNOS_RULEHELP_HARD };

    private Map<Position, Tile>      tiles;
    private Map<Position, TileGroup> tileGroups;
    private MfxManager               mfxManager;
    private GfxManager               gfxManager;
    private List<PathPosition>       path;

    private int                      position                = 0;
    private SoundPack                currentSoundPack;
    private List<Integer>            usedSounds;
    private ViewGroup                layoutInstructions;
    private TextView                 buttonReplay;

    private ViewGroup                layoutRulemessage;
    private TextView                 textViewRuleMessage;
    private TextView                 buttonRuleMessageConfirm;

    private ImageView                wrongSign, rightSign;

    public UnosActivity() {
        super(GamesDefinition.UNOS);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        wrongSign = (ImageView) findViewById(R.id.imageView_wrong);
        rightSign = (ImageView) findViewById(R.id.imageView_right);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutRulemessage = (ViewGroup) inflater.inflate(R.layout.game_unos_rulemessage, null);
        layoutInstructions = (ViewGroup) inflater.inflate(R.layout.game_unos_instructions, null);

        getGamePanelLayout().addView(layoutInstructions);
        getGamePanelLayout().addView(layoutRulemessage);

        textViewRuleMessage = (TextView) layoutRulemessage.findViewById(R.id.game_unos_rulemessage_text);
        buttonRuleMessageConfirm = (TextView) layoutRulemessage.findViewById(R.id.game_unos_rulemessage_button);

        buttonReplay = (TextView) findViewById(R.id.imageButton_replay);
        buttonReplay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setNoListeners(position);
                playSound(position);
            }
        });

        try {
            Bitmap bm = BitmapHelper.getBitmapFromStream(this, getTablexiaContext().getZipResourceFile().getInputStream(BASE_DIR + "gfx/ear.png"));
            ImageView imageViewEar = (ImageView) layoutInstructions.findViewById(R.id.imageView_ear);
            imageViewEar.setImageBitmap(bm);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
            // nothing
        }

        if (savedInstanceState != null) {
            try {
                tiles = (Map<Position, Tile>) SerializableHelper.fromString(savedInstanceState.getString(GAMESTATE_MAP_KEY));
                path = (List<PathPosition>) SerializableHelper.fromString(savedInstanceState.getString(GAMESTATE_PATH_KEY));
                currentSoundPack = (SoundPack) SerializableHelper.fromString(savedInstanceState.getString(GAMESTATE_SOUNDPACK_KEY));
                position = savedInstanceState.getInt(GAMESTATE_POSITION_KEY);
                usedSounds = (List<Integer>) SerializableHelper.fromString(savedInstanceState.getString(GAMESTATE_USED_KEY));
                Log.i(TAG, "State restored");
            } catch (Exception e) {
                Log.e(TAG, "Error saving state" + e.getMessage());
            }
        }
    }

    @Override
    protected synchronized void onResume() {
        super.onResume();
        loadGameRuleHelpSounds();
    }

    private void loadGameRuleHelpSounds() {
        for (String gameRuleHelpSound : GAMERULE_HELP_SOUNDS) {
            getSoundControl().addSound(gameRuleHelpSound);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        try {
            String mapB64 = SerializableHelper.toString((TreeMap<Position, Tile>) tiles);
            String pathB64 = SerializableHelper.toString((ArrayList<PathPosition>) path);
            String packB64 = SerializableHelper.toString(currentSoundPack);
            String usedB64 = SerializableHelper.toString((ArrayList<Integer>) usedSounds);

            outState.putString(GAMESTATE_MAP_KEY, mapB64);
            outState.putString(GAMESTATE_PATH_KEY, pathB64);
            outState.putString(GAMESTATE_SOUNDPACK_KEY, packB64);
            outState.putString(GAMESTATE_USED_KEY, usedB64);
            outState.putInt(GAMESTATE_POSITION_KEY, position);
        } catch (IOException e) {
            Log.e(TAG, "Error saving state" + e.getMessage());
        }
    }

    /**
     * Zapneme si podporu zvuku
     */
    @Override
    public EngineOptions onCreateEngineOptions() {
        EngineOptions engineOptions = super.onCreateEngineOptions();
        engineOptions.getAudioOptions().setNeedsSound(true).setNeedsMusic(true);
        return engineOptions;
    }

    @Override
    public Camera onCreateCamera() {
        float scale = displaySize.x / (float) displaySize.y;
        int cameraHeight = (int) (CAMERA_WIDTH / scale);
        return new MySmoothCamera(0, 0, CAMERA_WIDTH, cameraHeight, 1000, 1000, 1f);
    }

    private MySmoothCamera getCamera() {
        return (MySmoothCamera) camera;
    }

    @Override
    protected void initGame() {
        super.initGame();

        if ((tiles == null) || (path == null)) {
            TileMapGenerator tileMapGenerator = new TileMapGenerator();
            tiles = tileMapGenerator.generate(MAP_WIDTH, MAP_HEIGHT);

            PathGenerator pathGenerator = new PathGenerator();
            path = pathGenerator.generatePath(new Position(MAP_WIDTH / 2, MAP_HEIGHT / 2), GAME_STOPS, tiles);
        }

        fromDirection = getLastDirection(position);
        nextDirection = getNextDirection(position);

        if (usedSounds == null) {
            usedSounds = new ArrayList<Integer>();
        }

        if (currentSoundPack == null) {
            currentSoundPack = DirectionSounds.getRandomSoundPack(getDifficulty(), fromDirection, nextDirection);
            usedSounds.add(currentSoundPack.getSoundNumber());
        }

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (getDifficulty() == GameDifficulty.EASY) {
                    textViewRuleMessage.setText(R.string.game_unos_rule_easy);
                } else if (getDifficulty() == GameDifficulty.MEDIUM) {
                    textViewRuleMessage.setText(R.string.game_unos_rule_medium);
                } else if (getDifficulty() == GameDifficulty.HARD) {
                    textViewRuleMessage.setText(R.string.game_unos_rule_hardcore);
                }
            }
        });

        gfxManager.loadTextures(tiles.values());
        populateMap();
    }

    @Override
    protected void showGame() {
        super.showGame();
        if (position > 0) {
            playNextPosition(true);
        } else {
            toggleView(true, layoutRulemessage);
            getSoundControl().playSound(GAMERULE_HELP_SOUNDS[getDifficulty().ordinal()], false);
            buttonRuleMessageConfirm.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    getTablexiaContext().hideScreenContent(false, new AnimatorListenerAdapter() {

                        @Override
                        public void onAnimationStart(Animator animation) {
                            getSoundControl().stopSound(GAMERULE_HELP_SOUNDS[getDifficulty().ordinal()], false, true);
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            toggleView(false, layoutRulemessage);
                            getTablexiaContext().showScreenContent(new AnimatorListenerAdapter() {

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    playNextPosition(true);
                                }

                            });
                        }

                    });
                }
            });
        }
    }

    @Override
    public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws IOException {

        gfxManager = new GfxManager(getTextureManager(), getAssets(), this);
        gfxManager.loadTextures(new ArrayList<Tile>());

        // load zvuku
        mfxManager = new MfxManager(getMusicManager(), getSoundManager(), this);
        mfxManager.loadSounds();

        pOnCreateResourcesCallback.onCreateResourcesFinished();
    }

    @Override
    public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) throws IOException {
        // pScene.setOnSceneTouchListener(new DragSceneListener(camera));
        pScene.setBackground(new Background(0.7f, 0.7f, 0.7f));
        super.onPopulateScene(pScene, pOnPopulateSceneCallback);
    }

    private int getBaseX(Position position) {
        return (position.getX() * (TILE_BASE_WIDTH + ROAD_WIDTH)) / 2;
    }

    private int getBaseY(Position position) {
        return -1 * ((position.getY() * (TILE_BASE_HEIGHT + ROAD_HEIGHT)) - ((position.getX() % 2) * ((TILE_BASE_HEIGHT + ROAD_HEIGHT) / 2)));
    }

    private Position getMaxPosition(Tile tile, Position position) {
        Position maxPosition = position;
        if ((tile.getSESize() > 1) || (tile.getSWSize() > 1)) {
            for (int i = 1; i < tile.getSWSize(); i++) {
                maxPosition = maxPosition.getSouthWest();
            }
            for (int i = 1; i < tile.getSESize(); i++) {
                maxPosition = maxPosition.getSouthEast();
            }
        }
        return maxPosition;
    }

    /**
     * Naplní scénu dlaždicemi
     */
    private void populateMap() {
        /* Create the sprite and add it to the scene. */
        tileGroups = new HashMap<Position, TileGroup>();
        // Log.d(TAG, "Populating map");
        for (final Position position : tiles.keySet()) {
            final Tile tile = tiles.get(position);
            // Log.d(TAG, "Trying position " + position);
            if (tile != null) {
                TileGroup tileGroup = new TileGroup();

                // vypocet zakladni pozice tileGroup
                int baseX = getBaseX(position);
                int baseY = getBaseY(position);

                if ((tile.getSESize() > 1) || (tile.getSWSize() > 1)) {
                    Position maxPosition = getMaxPosition(tile, position);

                    int maxBaseX = getBaseX(maxPosition);
                    int maxBaseY = getBaseY(maxPosition);

                    baseX = (baseX + maxBaseX) / 2;
                    baseY = (baseY + maxBaseY) / 2;
                }

                final int baseTileY = baseY + (tile.getYOffset() / 2);
                // do tileGroup umistime dlazdici
                final Sprite tileSprite = new Sprite(baseX, baseTileY, gfxManager.getTile(tile).second, getVertexBufferObjectManager());
                scene.attachChild(tileSprite);
                tileGroup.setTileSprite(tileSprite);

                if (path.contains(position)) {
                    // TODO vsechny pozice brat od max position, aby sme podporovali velke tily
                    // vykreleni cest
                    for (int i = 0; i < path.size(); i++) {
                        Position pathPosition = path.get(i);
                        if (pathPosition.equals(position)) {
                            Position prev = i > 0 ? path.get(i - 1) : null;
                            Position next = i < (path.size() - 1) ? path.get(i + 1) : null;

                            if ((position.getNorthEast().equals(prev) || position.getNorthEast().equals(next)) && (tileGroup.getRoadNorhEast() == null)) {
                                final Sprite pathSprite = new Sprite(baseX + ((ROAD_WIDTH / 4) * 3), baseY - (ROAD_HEIGHT / 4), gfxManager.get(TextureType.POSITION_LINE_NESW).second, getVertexBufferObjectManager());
                                pathSprite.setAlpha(0);
                                scene.attachChild(pathSprite);
                                tileGroup.setRoadNorhEast(pathSprite);
                            }

                            if ((position.getNorthWest().equals(prev) || position.getNorthWest().equals(next)) && (tileGroup.getRoadNorhWest() == null)) {
                                final Sprite pathSprite = new Sprite(baseX - ((ROAD_WIDTH / 4) * 3), baseY - (ROAD_HEIGHT / 4), gfxManager.get(TextureType.POSITION_LINE_NWSE).second, getVertexBufferObjectManager());
                                pathSprite.setAlpha(0);
                                scene.attachChild(pathSprite);
                                tileGroup.setRoadNorhWest(pathSprite);
                            }
                        }
                    }

                    final DirectionSprite directionNESprite = new DirectionSprite((baseX + ((ROAD_WIDTH / 4) * 3)) + 75, (baseY - (ROAD_HEIGHT / 4)) + 60, gfxManager.get(TextureType.POSITION_NEXT_NE).second, getVertexBufferObjectManager());
                    directionNESprite.setAlpha(0);
                    scene.attachChild(directionNESprite);
                    scene.registerTouchArea(directionNESprite);
                    tileGroup.setNextNorhEastOut(directionNESprite);

                    final DirectionSprite directionNEPLayingSprite = new DirectionSprite((baseX + ((ROAD_WIDTH / 4) * 3)) + 75, (baseY - (ROAD_HEIGHT / 4)) + 60, gfxManager.get(TextureType.POSITION_NEXT_NE_PRESSED).second, getVertexBufferObjectManager());
                    directionNEPLayingSprite.setAlpha(0);
                    scene.attachChild(directionNEPLayingSprite);
                    tileGroup.setNextNorhEastOutPressed(directionNEPLayingSprite);

                    final DirectionSprite directionNWSprite = new DirectionSprite((baseX - ((ROAD_WIDTH / 4) * 3)) - 70, (baseY - (ROAD_HEIGHT / 4)) + 60, gfxManager.get(TextureType.POSITION_NEXT_NW).second, getVertexBufferObjectManager());
                    directionNWSprite.setAlpha(0);
                    scene.attachChild(directionNWSprite);
                    scene.registerTouchArea(directionNWSprite);
                    tileGroup.setNextNorhWestOut(directionNWSprite);

                    final DirectionSprite directionNWPlayingSprite = new DirectionSprite((baseX - ((ROAD_WIDTH / 4) * 3)) - 70, (baseY - (ROAD_HEIGHT / 4)) + 60, gfxManager.get(TextureType.POSITION_NEXT_NW_PRESSED).second, getVertexBufferObjectManager());
                    directionNWPlayingSprite.setAlpha(0);
                    scene.attachChild(directionNWPlayingSprite);
                    tileGroup.setNextNorhWestOutPressed(directionNWPlayingSprite);

                    // vykresleni bodu na krizovatky
                    final Sprite flagSprite = new Sprite(baseX + 50, (baseY - (ROAD_HEIGHT)) + 65, gfxManager.get(TextureType.POSITION_CURRENT).second, getVertexBufferObjectManager());
                    flagSprite.setAlpha(0);
                    scene.attachChild(flagSprite);
                    tileGroup.setFlag(flagSprite);

                }

                if (path.contains(position.getNorthEast())) {
                    final DirectionSprite directionNESprite = new DirectionSprite((baseX + ((ROAD_WIDTH / 4) * 3)) - 75, (baseY - (ROAD_HEIGHT / 4)) - 10, gfxManager.get(TextureType.POSITION_NEXT_SW).second, getVertexBufferObjectManager());
                    directionNESprite.setAlpha(0);
                    scene.attachChild(directionNESprite);
                    scene.registerTouchArea(directionNESprite);
                    tileGroup.setNextNorhEastIn(directionNESprite);

                    final DirectionSprite directionNEPlayingSprite = new DirectionSprite((baseX + ((ROAD_WIDTH / 4) * 3)) - 75, (baseY - (ROAD_HEIGHT / 4)) - 10, gfxManager.get(TextureType.POSITION_NEXT_SW_PRESSED).second, getVertexBufferObjectManager());
                    directionNEPlayingSprite.setAlpha(0);
                    scene.attachChild(directionNEPlayingSprite);
                    tileGroup.setNextNorhEastInPressed(directionNEPlayingSprite);
                }
                if (path.contains(position.getNorthWest())) {
                    final DirectionSprite directionNWSprite = new DirectionSprite((baseX - ((ROAD_WIDTH / 4) * 3)) + 100, (baseY - (ROAD_HEIGHT / 4)) - 15, gfxManager.get(TextureType.POSITION_NEXT_SE).second, getVertexBufferObjectManager());
                    directionNWSprite.setAlpha(0);
                    scene.attachChild(directionNWSprite);
                    scene.registerTouchArea(directionNWSprite);
                    tileGroup.setNextNorhWestIn(directionNWSprite);

                    final DirectionSprite directionNWPlayingSprite = new DirectionSprite((baseX - ((ROAD_WIDTH / 4) * 3)) + 100, (baseY - (ROAD_HEIGHT / 4)) - 15, gfxManager.get(TextureType.POSITION_NEXT_SE_PRESSED).second, getVertexBufferObjectManager());
                    directionNWPlayingSprite.setAlpha(0);
                    scene.attachChild(directionNWPlayingSprite);
                    tileGroup.setNextNorhWestInPressed(directionNWPlayingSprite);
                }

                // Log.d(TAG, "Landing  " + position + " " + tile);
                tileGroups.put(position, tileGroup);
            } else {
                Log.v(TAG, "No texture for position " + position);
            }
        }

        if (position > 0) {
            for (int i = 0; i < position; i++) {
                showRoad(i);
            }
        }

        PathPosition currentPosition = path.get(position);
        TileGroup currentTileGroup = tileGroups.get(currentPosition);
        Sprite flag = currentTileGroup.getFlag();
        getCamera().setCenterDirect(flag.getX(), flag.getY());
    }

    private void playNextPosition(boolean directCenter) {

        final PathPosition currentPosition = path.get(position);
        final TileGroup currentTileGroup = tileGroups.get(currentPosition);

        // pokud jsme na prvni pozici, nahodne vybereme predchozi smer, ktery schovame
        if (fromDirection == null) {
            Random random = new Random();
            fromDirection = Direction.values()[random.nextInt(4)];
            while (fromDirection == nextDirection) {
                fromDirection = Direction.values()[random.nextInt(4)];
            }
        }

        // pokud jsme na posledni pozici, zvolime nahodne
        if (nextDirection == null) {
            Random random = new Random();
            nextDirection = Direction.values()[random.nextInt(4)];
            while (fromDirection == nextDirection) {
                nextDirection = Direction.values()[random.nextInt(4)];
            }
        }

        OnSmoothCenterFinishedListener onSmoothCenterFinishedListener = new OnSmoothCenterFinishedListener() {
            @Override
            public void onFinished() {
                // get current cross
                showFlags(position, new IEntityModifierListener() {

                    @Override
                    public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                        // nothing
                    }

                    @Override
                    public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                        playSound(position);
                    }
                });
                showRoad(position);

                final DirectionSprite ne = currentTileGroup.getNextNorhEastOut();
                final DirectionSprite nep = currentTileGroup.getNextNorhEastOutPressed();
                final DirectionSprite nw = currentTileGroup.getNextNorhWestOut();
                final DirectionSprite nwp = currentTileGroup.getNextNorhWestOutPressed();
                final DirectionSprite se = tileGroups.get(currentPosition.getSouthEast()).getNextNorhWestIn();
                final DirectionSprite sep = tileGroups.get(currentPosition.getSouthEast()).getNextNorhWestInPressed();
                final DirectionSprite sw = tileGroups.get(currentPosition.getSouthWest()).getNextNorhEastIn();
                final DirectionSprite swp = tileGroups.get(currentPosition.getSouthWest()).getNextNorhEastInPressed();

                if (fromDirection == Direction.NORTH_WEST) {
                    hideSprite(nw);
                    hideSprite(nwp);
                } else if (fromDirection == Direction.NORTH_EAST) {
                    hideSprite(ne);
                    hideSprite(nep);
                } else if (fromDirection == Direction.SOUTH_EAST) {
                    hideSprite(se);
                    hideSprite(sep);
                } else if (fromDirection == Direction.SOUTH_WEST) {
                    hideSprite(sw);
                    hideSprite(swp);
                }
            }
        };

        Sprite flag = currentTileGroup.getFlag();
        if (directCenter) {
            getCamera().setCenterDirect(flag.getX(), flag.getY());
            onSmoothCenterFinishedListener.onFinished();
        } else {
            getCamera().setCenter(flag.getX(), flag.getY(), onSmoothCenterFinishedListener);
        }
    }

    /**
     * Správnému směru na aktuálním stanovišti nastaví akci, která posune uživatele dál.
     *
     * @param pathPosition
     */
    private void activateRightDirection(int pathPosition) {
        if (nextDirection != null) {
            final PathPosition position = path.get(pathPosition);
            final TileGroup tileGroup = tileGroups.get(position);
            if (nextDirection == Direction.NORTH_WEST) {
                tileGroup.getNextNorhWestOut().setOnAreaTouchedListener(rightDirectionAreaTouchedListener);
            } else if (nextDirection == Direction.NORTH_EAST) {
                tileGroup.getNextNorhEastOut().setOnAreaTouchedListener(rightDirectionAreaTouchedListener);
            } else if (nextDirection == Direction.SOUTH_EAST) {
                tileGroups.get(position.getSouthEast()).getNextNorhWestIn().setOnAreaTouchedListener(rightDirectionAreaTouchedListener);
            } else if (nextDirection == Direction.SOUTH_WEST) {
                tileGroups.get(position.getSouthWest()).getNextNorhEastIn().setOnAreaTouchedListener(rightDirectionAreaTouchedListener);
            }
        }
    }

    /**
     * Schová ukazatel aktuální pozice a směrů
     *
     * @param pathPosition
     */
    private void hideFlags(final int pathPosition) {
        final PathPosition position = path.get(pathPosition);
        final TileGroup tileGroup = tileGroups.get(position);

        hideSprite(tileGroup.getFlag());
        hideSprite(tileGroup.getNextNorhEastOut());
        hideSprite(tileGroup.getNextNorhWestOut());
        hideSprite(tileGroups.get(position.getSouthEast()).getNextNorhWestIn());
        hideSprite(tileGroups.get(position.getSouthWest()).getNextNorhEastIn());
        hideSprite(tileGroup.getNextNorhEastOutPressed());
        hideSprite(tileGroup.getNextNorhWestOutPressed());
        hideSprite(tileGroups.get(position.getSouthEast()).getNextNorhWestInPressed());
        hideSprite(tileGroups.get(position.getSouthWest()).getNextNorhEastInPressed());
    }

    /**
     * Zobrazí seznam spritů
     *
     * @param pathPosition
     */
    private void showArrows(Sprite... sprites) {
        for (Sprite sprite : sprites) {
            sprite.setAlpha(1);
        }
    }

    /**
     * Schová sprite vyhodí z něj touchlistener, aby neviditelný sprite nereagoval na pohyby
     *
     * @param sprite
     */
    private void hideSprite(final Sprite sprite) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sprite.clearEntityModifiers();
                sprite.setAlpha(0);
            }
        });

        if (sprite instanceof DirectionSprite) {
            ((DirectionSprite) sprite).clearOnAreaTouchedListener();
        }
    }

    // Listener, ktery dame ukazatelum spatneho smeru
    private OnAreaTouchedListener wrongDirectionAreaTouchedListener = new OnAreaTouchedListener() {
        @Override
        public void touched() {
            wrongDirection();
        }
    };

    // Listener, ktery nastavime pouze jednomu ukazateli smeru, ktery odpovida smeru na dalsi pozici na ceste.
    private OnAreaTouchedListener rightDirectionAreaTouchedListener = new OnAreaTouchedListener() {
        @Override
        public void touched() {
            hideReplay();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    rightSign.setAlpha(1.0f);
                    rightSign.setVisibility(View.VISIBLE);
                    toggleView(false, rightSign, getResources().getInteger(android.R.integer.config_mediumAnimTime));
                }
            });
            nextPosition();
            if (position > 0) {
                hideFlags(position - 1);
            }
        }
    };

    /**
     * Nastavi vsem smerum z dane pozici listener spatneho smeru
     *
     * @param pathPosition
     */
    private void setWrongListeners(final int pathPosition) {
        PathPosition currentPosition = path.get(pathPosition);
        TileGroup currentTileGroup = tileGroups.get(currentPosition);
        currentTileGroup.getNextNorhEastOut().setOnAreaTouchedListener(wrongDirectionAreaTouchedListener);
        currentTileGroup.getNextNorhEastOutPressed().setOnAreaTouchedListener(wrongDirectionAreaTouchedListener);
        currentTileGroup.getNextNorhWestOut().setOnAreaTouchedListener(wrongDirectionAreaTouchedListener);
        currentTileGroup.getNextNorhWestOutPressed().setOnAreaTouchedListener(wrongDirectionAreaTouchedListener);
        tileGroups.get(currentPosition.getSouthEast()).getNextNorhWestIn().setOnAreaTouchedListener(wrongDirectionAreaTouchedListener);
        tileGroups.get(currentPosition.getSouthEast()).getNextNorhWestInPressed().setOnAreaTouchedListener(wrongDirectionAreaTouchedListener);
        tileGroups.get(currentPosition.getSouthWest()).getNextNorhEastIn().setOnAreaTouchedListener(wrongDirectionAreaTouchedListener);
        tileGroups.get(currentPosition.getSouthWest()).getNextNorhEastInPressed().setOnAreaTouchedListener(wrongDirectionAreaTouchedListener);
    }

    private void setNoListeners(final int pathPosition) {
        PathPosition currentPosition = path.get(pathPosition);
        TileGroup currentTileGroup = tileGroups.get(currentPosition);
        currentTileGroup.getNextNorhEastOut().clearOnAreaTouchedListener();
        currentTileGroup.getNextNorhEastOutPressed().clearOnAreaTouchedListener();
        currentTileGroup.getNextNorhWestOut().clearOnAreaTouchedListener();
        currentTileGroup.getNextNorhWestOutPressed().clearOnAreaTouchedListener();
        tileGroups.get(currentPosition.getSouthEast()).getNextNorhWestIn().clearOnAreaTouchedListener();
        tileGroups.get(currentPosition.getSouthEast()).getNextNorhWestInPressed().clearOnAreaTouchedListener();
        tileGroups.get(currentPosition.getSouthWest()).getNextNorhEastIn().clearOnAreaTouchedListener();
        tileGroups.get(currentPosition.getSouthWest()).getNextNorhEastInPressed().clearOnAreaTouchedListener();
    }

    /**
     * obrazi ukazatel aktualni pozice a indikatory smeru z dane pozice. Zobrazei vzdy vsechny ctyri smery asynchrone pomoci entityModifieru
     *
     * @param pathPosition
     * @param entityModifierListener
     */
    private void showFlags(final int pathPosition, IEntityModifierListener entityModifierListener) {
        // zobrazit vlajky kolem
        SequenceEntityModifier entityModifier = new SequenceEntityModifier(entityModifierListener, new AlphaModifier(0.8f, 0, 1), new DelayModifier(1));
        PathPosition currentPosition = path.get(pathPosition);
        TileGroup currentTileGroup = tileGroups.get(currentPosition);
        currentTileGroup.getFlag().registerEntityModifier(entityModifier);
        // vlajky nezobrazime hned, ale az po odehrani zvuku
        // currentTileGroup.getNextNorhEast().registerEntityModifier(entityModifier.deepCopy());
        // currentTileGroup.getNextNorhWest().registerEntityModifier(entityModifier.deepCopy());
        // tileGroups.get(currentPosition.getSouthEast()).getNextNorhWest().registerEntityModifier(entityModifier.deepCopy());
        // tileGroups.get(currentPosition.getSouthWest()).getNextNorhEast().registerEntityModifier(entityModifier.deepCopy());
    }

    /**
     * Zobrazi silnici do pozice dane parametrem z predchozi pozice
     *
     * @param position
     */
    private void showRoad(int position) {
        // show last road
        SequenceEntityModifier entityModifier = new SequenceEntityModifier(new AlphaModifier(0.8f, 0, 1), new DelayModifier(1));

        if (position > 0) {
            PathPosition currentPosition = path.get(position);
            TileGroup currentTileGroup = tileGroups.get(currentPosition);
            final PathPosition lastPosition = path.get(position - 1);
            final TileGroup lastTileGroup = tileGroups.get(lastPosition);
            if (lastPosition.getNorthEast().equals(currentPosition)) {
                lastTileGroup.getRoadNorhEast().registerEntityModifier(entityModifier.deepCopy());
            } else if (lastPosition.getNorthWest().equals(currentPosition)) {
                lastTileGroup.getRoadNorhWest().registerEntityModifier(entityModifier.deepCopy());
            } else if (currentPosition.getNorthEast().equals(lastPosition)) {
                currentTileGroup.getRoadNorhEast().registerEntityModifier(entityModifier.deepCopy());
            } else if (currentPosition.getNorthWest().equals(lastPosition)) {
                currentTileGroup.getRoadNorhWest().registerEntityModifier(entityModifier.deepCopy());
            }
        }
    }

    /**
     * Akce, ktera presune uzivatele na dalsi pozici a zavola playNextPosition, tim rovnou zobrazi smery a prehraje zvuky
     */
    private void nextPosition() {
        position++;
        Log.i(TAG, "Next position " + position);

        if (position >= path.size()) {
            getGameManager().setCounterAndSave(getGameManager().getCounter());
            gameComplete();
        } else {
            fromDirection = getLastDirection(position);
            nextDirection = getNextDirection(position);
            // zajistime aby zvuk nebyl jiz pouzit u predchozich krizovatek
            do {
                currentSoundPack = DirectionSounds.getRandomSoundPack(getDifficulty(), fromDirection, nextDirection);
            } while (usedSounds.contains(currentSoundPack.getSoundNumber()));
            usedSounds.add(currentSoundPack.getSoundNumber());
            playNextPosition(false);
        }
    }

    /**
     * Akce při kliku na nesprávný směr
     */
    private void wrongDirection() {
        getGameManager().setCounterAndSave(getGameManager().getCounter() + 1);
        mfxManager.playSound(SoundType.SOUND_ERROR);
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                wrongSign.animate().cancel();
                wrongSign.setAlpha(1.0f);
                wrongSign.setVisibility(View.VISIBLE);
                toggleView(false, wrongSign, getResources().getInteger(android.R.integer.config_mediumAnimTime));
            }
        });

        Log.i(TAG, "Wrong direction !");
    }

    private Direction nextDirection, fromDirection;

    /**
     * Zobrazi ucho, prehraje zvuk, ucho zase schova a spusti prehrani zvuku jednotlivymi smery.
     * Nikam neposune, takže je možné volat vícekrát na jednom stanovišti - přehrát znovu
     */
    private void playSound(final int position) {
        hideReplay();
        showInstructions();
        scene.registerUpdateHandler(new TimerHandler(1f, false, new ITimerCallback() {
            @Override
            public void onTimePassed(final TimerHandler pTimerHandler) {
                mfxManager.playSound(currentSoundPack.getExample(), new OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        scene.registerUpdateHandler(new TimerHandler(1, false, new ITimerCallback() {
                            @Override
                            public void onTimePassed(final TimerHandler pTimerHandler) {
                                hideInstructions();

                                PathPosition currentPosition = path.get(position);
                                TileGroup currentTileGroup = tileGroups.get(currentPosition);

                                final DirectionSprite ne = currentTileGroup.getNextNorhEastOut();
                                final DirectionSprite nep = currentTileGroup.getNextNorhEastOutPressed();
                                final DirectionSprite nw = currentTileGroup.getNextNorhWestOut();
                                final DirectionSprite nwp = currentTileGroup.getNextNorhWestOutPressed();
                                final DirectionSprite se = tileGroups.get(currentPosition.getSouthEast()).getNextNorhWestIn();
                                final DirectionSprite sep = tileGroups.get(currentPosition.getSouthEast()).getNextNorhWestInPressed();
                                final DirectionSprite sw = tileGroups.get(currentPosition.getSouthWest()).getNextNorhEastIn();
                                final DirectionSprite swp = tileGroups.get(currentPosition.getSouthWest()).getNextNorhEastInPressed();

                                if (fromDirection == Direction.NORTH_WEST) {
                                    // NORTHEAST SOUTHEAST SOUTHWEST
                                    String[] sounds = currentSoundPack.getSoundsInOrder(Direction.NORTH_EAST, Direction.SOUTH_EAST, Direction.SOUTH_WEST, nextDirection);
                                    showArrows(ne, se, sw);
                                    playSounds(sounds, ne, nep, se, sep, sw, swp);
                                } else if (fromDirection == Direction.NORTH_EAST) {
                                    // SOUTHEAST SOUTHWEST NORTH WEST
                                    String[] sounds = currentSoundPack.getSoundsInOrder(Direction.SOUTH_EAST, Direction.SOUTH_WEST, Direction.NORTH_WEST, nextDirection);
                                    showArrows(se, sw, nw);
                                    playSounds(sounds, se, sep, sw, swp, nw, nwp);
                                } else if (fromDirection == Direction.SOUTH_EAST) {
                                    // SOUTHWEST NORTHWEST NORTHEAST
                                    String[] sounds = currentSoundPack.getSoundsInOrder(Direction.SOUTH_WEST, Direction.NORTH_WEST, Direction.NORTH_EAST, nextDirection);
                                    showArrows(sw, nw, ne);
                                    playSounds(sounds, sw, swp, nw, nwp, ne, nep);
                                } else if (fromDirection == Direction.SOUTH_WEST) {
                                    // NORTHWEST NORTHEAST SOUTHEAST
                                    String[] sounds = currentSoundPack.getSoundsInOrder(Direction.NORTH_WEST, Direction.NORTH_EAST, Direction.SOUTH_EAST, nextDirection);
                                    showArrows(nw, ne, se);
                                    playSounds(sounds, nw, nwp, ne, nep, se, sep);
                                }
                            }
                        }));
                    }
                });
            }
        }));
    }

    /**
     * Prehraje zvyky v poradi podle predanych spritu
     *
     * @param first
     * @param firtsPlaying
     * @param second
     * @param secondPlaying
     * @param third
     * @param thirdPlaying
     */
    private void playSounds(final String[] sounds, final DirectionSprite first, final DirectionSprite firtsPlaying, final DirectionSprite second, final DirectionSprite secondPlaying, final DirectionSprite third, final DirectionSprite thirdPlaying) {
        Log.i(TAG, "Prehravam zvuky v poradi");
        playDirection(sounds[0], first, firtsPlaying, new DirectionPlaybackFinished() {
            @Override
            public void playbackFinished() {
                playDirection(sounds[1], second, secondPlaying, new DirectionPlaybackFinished() {
                    @Override
                    public void playbackFinished() {
                        playDirection(sounds[2], third, thirdPlaying, new DirectionPlaybackFinished() {

                            @Override
                            public void playbackFinished() {
                                setWrongListeners(position);
                                activateRightDirection(position);
                                showReplay();
                            }
                        });
                    }
                });
            }
        });

    }

    /**
     * Prehraje jeden zvuk a zvyrazni pri tom jeden konkretni smer ktery je dany predanymi Sprity
     *
     * @param sound
     * @param normal
     * @param playingr
     * @param finishedListener
     */
    private void playDirection(final String sound, final DirectionSprite normal, final DirectionSprite playing, final DirectionPlaybackFinished finishedListener) {
        normal.setAlpha(0);
        playing.registerEntityModifier(new LoopEntityModifier(new SequenceEntityModifier(new AlphaModifier(0.5f, 1, 0.3f), new AlphaModifier(0.2f, 0.3f, 1))));
        scene.registerUpdateHandler(new TimerHandler(0.8f, new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                mfxManager.playSound(sound, new OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        scene.registerUpdateHandler(new TimerHandler(0.8f, new ITimerCallback() {
                            @Override
                            public void onTimePassed(TimerHandler pTimerHandler) {
                                normal.setAlpha(1);
                                runOnUpdateThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        playing.clearEntityModifiers();
                                        playing.setAlpha(0);
                                    }
                                });

                                if (finishedListener != null) {
                                    finishedListener.playbackFinished();
                                }
                            }
                        }));

                    }
                });
            }
        }));
    }

    /**
     * Brátí hodnotu enumu pro příští smer z cesty
     *
     * @param position
     * @return
     */
    public Direction getNextDirection(int position) {
        Direction nextDirection = null;
        if (position < (path.size() - 1)) {
            boolean nextNE = path.get(position).getNorthEast().equals(path.get(position + 1));
            boolean nextNW = path.get(position).getNorthWest().equals(path.get(position + 1));
            boolean nextSE = path.get(position).getSouthEast().equals(path.get(position + 1));
            boolean nextSW = path.get(position).getSouthWest().equals(path.get(position + 1));

            if (nextSW) {
                nextDirection = Direction.SOUTH_WEST;
            } else if (nextSE) {
                nextDirection = Direction.SOUTH_EAST;
            } else if (nextNW) {
                nextDirection = Direction.NORTH_WEST;
            } else if (nextNE) {
                nextDirection = Direction.NORTH_EAST;
            }
        }
        return nextDirection;
    }

    /**
     * Brátí hodnotu enumu pro předchozí směr z cesty. bere se z aktuální pozice směrem na předchozí.
     *
     * @param position
     * @return
     */
    private Direction getLastDirection(int position) {
        Direction fromDirection = null;
        if (position > 0) {
            boolean preNE = path.get(position).getNorthEast().equals(path.get(position - 1));
            boolean preNW = path.get(position).getNorthWest().equals(path.get(position - 1));
            boolean preSE = path.get(position).getSouthEast().equals(path.get(position - 1));
            boolean preSW = path.get(position).getSouthWest().equals(path.get(position - 1));

            if (preSW) {
                fromDirection = Direction.SOUTH_WEST;
            } else if (preSE) {
                fromDirection = Direction.SOUTH_EAST;
            } else if (preNW) {
                fromDirection = Direction.NORTH_WEST;
            } else if (preNE) {
                fromDirection = Direction.NORTH_EAST;
            }
        }
        return fromDirection;
    }

    @Override
    public String getVictoryText(int progress) {
        int resId = R.string.game_unos_stars_zero;
        if (progress == 3) {
            resId = R.string.game_unos_stars_three;
        } else if (progress == 2) {
            resId = R.string.game_unos_stars_two;
        } else if (progress == 1) {
            resId = R.string.game_unos_stars_one;
        }

        return getResources().getString(resId);
    }

    @Override
    public String[] getVictorySpeech() {
        return new String[] { SpeechSounds.RESULT_UNOS_0, SpeechSounds.RESULT_UNOS_1, SpeechSounds.RESULT_UNOS_2, SpeechSounds.RESULT_UNOS_3 };
    }

    @Override
    public int countProgress() {
        double errorsNumber = getGameManager().getCounter();
        int[] levels = null;
        switch (getDifficulty()) {
            case EASY:
                levels = GAMERULES_CUPS_EASY;
                break;
            case MEDIUM:
                levels = GAMERULES_CUPS_MEDIUM;
                break;
            case HARD:
                levels = GAMERULES_CUPS_HARD;
                break;
            default:
                throw new IllegalStateException("Undefined difficulty");
        }

        if (errorsNumber <= levels[0]) {
            return 3;
        } else if (errorsNumber <= levels[1]) {
            return 2;
        } else if (errorsNumber <= levels[2]) {
            return 1;
        }
        return 0;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.game_unos;
    }

    private void hideInstructions() {
        toggleView(false, layoutInstructions);
    }

    private void showInstructions() {
        toggleView(true, layoutInstructions);
    }

    private void hideReplay() {
        toggleView(false, buttonReplay);
    }

    private void showReplay() {
        toggleView(true, buttonReplay);
    }

    private void toggleView(final boolean show, final View view) {
        toggleView(show, view, getResources().getInteger(android.R.integer.config_shortAnimTime));
    }

    /**
     * Změní viditelnost view pomocí animace průhlednosti a poté nastaví visible/gone
     *
     * @param show
     * @param view
     */
    private void toggleView(final boolean show, final View view, final long duration) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!show && (view.getVisibility() == View.GONE)) {
                    return;
                }
                view.setVisibility(View.VISIBLE);
                view.animate().setDuration(duration).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(show ? View.VISIBLE : View.GONE);
                    }
                });
            }
        });
    }

    @Override
    protected void resetGame() {

        runOnUpdateThread(new Runnable() {

            @Override
            public void run() {
                scene.reset();
                scene.clearEntityModifiers();
                scene.clearUpdateHandlers();
                scene.clearTouchAreas();
                scene.detachChildren();
            }
        });

        hideReplay();
        usedSounds = null;
        position = 0;
        tiles = null;
        path = null;
        currentSoundPack = null;

        gameManager = null;
    }

    @Override
    public CharSequence getStatsText() {
        SpannableStringBuilder ssb = new SpannableStringBuilder("  ");
        Bitmap stats = BitmapFactory.decodeResource(getResources(), R.drawable.victoryscreen_stats);
        ssb.setSpan(new ImageSpan(this, stats), 0, 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        ssb.append(Html.fromHtml(String.format(getString(R.string.game_unos_stats), ((int) getGameManager().getCounter()))));
        return ssb;
    }
}
