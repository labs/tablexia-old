/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.potme.map.mapobject;

import cz.nic.tablexia.game.games.potme.ResourceManager;

/**
 * Map object type definition
 * 
 * @author Matyáš Latner 
 *
 */
public enum MapObjectType {
	
	SAFE	(ResourceManager.MAPOBJECT_SAFE, 	false, 	0.5f),
	KEY		(ResourceManager.MAPOBJECT_KEY,		false, 	0.5f),
	STAIRS	(ResourceManager.MAPOBJECT_STAIRS, 	true, 	1f);
	
	private String 	texture;
	private boolean rotable;
	private float 	objectSizeRation;
	
	/**
	 * Map object type definition
	 * 
	 * @param texture tile texture
	 * @param rotation tile texture rotation
	 */
	private MapObjectType(String texture, boolean rotable, float objectSizeRation) {
		this.texture 			= texture;
		this.rotable 			= rotable;
		this.objectSizeRation 	= objectSizeRation;
	}

	public String getTexture() {
		return texture;
	}
	
	public boolean isRotable() {
		return rotable;
	}
	
	public float getObjectSizeRation() {
		return objectSizeRation;
	}

}
