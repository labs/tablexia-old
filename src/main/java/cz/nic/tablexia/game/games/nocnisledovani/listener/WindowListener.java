
package cz.nic.tablexia.game.games.nocnisledovani.listener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.MotionEvent;

import com.activeandroid.util.Log;

import cz.nic.tablexia.game.GameActivity;
import cz.nic.tablexia.game.games.nocnisledovani.GfxManager;
import cz.nic.tablexia.game.games.nocnisledovani.MyBitmapTexture;
import cz.nic.tablexia.game.games.nocnisledovani.NocniSledovaniActivity;
import cz.nic.tablexia.game.games.nocnisledovani.NocniSledovaniHelper;
import cz.nic.tablexia.game.games.nocnisledovani.model.Trio;

public class WindowListener implements IOnSceneTouchListener {
    private static final int                              BACKGOUND_IMAGE_WIDTH   = 960;
    private static final int                              BACKGROUND_IMAGE_HEIGHT = 564;
    private static final String                           TAG                     = WindowListener.class.getSimpleName();
    private static final String                           WINDOWS_DIR             = NocniSledovaniActivity.BASE_DIR + "gfx/windows/";
    private static final String                           COLOR_BITMAP_NAME       = "colors";
    private static final String                           ASSETS_EXTENSION        = ".png";
    private List<Integer>                                 lightWindows;
    private Bitmap                                        colorBitmap;
    private Bitmap                                        windowBitmap;
    private GfxManager                                    gfxManager;
    private Point                                         originalBackgroundSize;
    private Map<Trio<Integer, Integer, Integer>, Integer> colorMap;
    private Map<Integer, Point>                           windowsPositionsMap;
    protected Paint                                       mPaint                  = new Paint();
    private TextureManager                                textureManager;
    private AssetManager                                  assetManager;
    private VertexBufferObjectManager                     vertexBufferObjectManager;
    private Camera                                        camera;
    private Scene                                         scene;

    private Map<Integer, Sprite>                          lidWindowsSprites;
    private MyBitmapTexture                               colorBitmapTexture;

    private Trio<Integer, Integer, Integer>               touchedColorTrio;
    private GameActivity parentActivity;

    public WindowListener(GameActivity parentActivity,int difficulty, GfxManager gfxManager, Point originalBackgroundSize, List<Integer> lightWindows, TextureManager textureManager, VertexBufferObjectManager vertexBufferObjectManager, Camera camera, AssetManager assetManager, Scene scene) {
        this.parentActivity = parentActivity;

        this.lightWindows = lightWindows;
        this.gfxManager = gfxManager;
        this.originalBackgroundSize = originalBackgroundSize;

        colorMap = NocniSledovaniHelper.getColorMap();

        this.textureManager = textureManager;
        this.assetManager = assetManager;
        this.camera = camera;
        this.vertexBufferObjectManager = vertexBufferObjectManager;
        this.scene = scene;

        windowsPositionsMap = NocniSledovaniHelper.getWindowsPositionsMap(difficulty+1);
        colorBitmap = gfxManager.getColorBmp();
        windowBitmap = gfxManager.getLightWindowsBitmap();

        lidWindowsSprites = new ConcurrentHashMap<Integer, Sprite>();
    }

    @Override
    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
        switch (pSceneTouchEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:

                double colorMapRatioX = (double) originalBackgroundSize.x / (double) colorBitmap.getWidth();
                double colorMapRatioY = (double) originalBackgroundSize.y / (double) colorBitmap.getHeight();

                int pixel = colorBitmap.getPixel((int) (pSceneTouchEvent.getX() / colorMapRatioX), (int) ((originalBackgroundSize.y - pSceneTouchEvent.getY()) / colorMapRatioY));

                touchedColorTrio = new Trio<Integer, Integer, Integer>(android.graphics.Color.red(pixel), android.graphics.Color.green(pixel), android.graphics.Color.blue(pixel));
                if (colorMap.containsKey(touchedColorTrio)) {
                    int touchedWindowId = colorMap.get(touchedColorTrio);

                    if (lightWindows.contains(Integer.valueOf(touchedWindowId))) {

                        onLightWindowClickAction(touchedWindowId);

                    } else {
                        onDarkWindowClickAction(touchedWindowId);

                    }
                }

                break;
        }

        return true;
    }

    protected void onDarkWindowClickAction(int windowIndex) {
        lightUpWindow(windowIndex);
        lightWindows.add(Integer.valueOf(windowIndex));
    }

    protected void onLightWindowClickAction(int windowIndex) {
        lightWindows.remove(Integer.valueOf(windowIndex));
        lightDownWindow(windowIndex);
    }

    public void lightUpWindow(final int windowIndex) {
        lightUpWindow(windowIndex, Color.WHITE);
    }

    /*
     * Lights up the specified window
     */
    // public void lightUpWindow(final int windowIndex, boolean hideOnCreate) {
    // lightUpWindow(windowIndex, false, Color.WHITE);
    // }
    /*
     * lights up specified window and creates color filter over the window texture
     */
    public void lightUpWindow(final int windowIndex, int color) {
        getLidWindowColor(windowIndex);
        MyBitmapTexture windowTexture = gfxManager.getWindowColorBitmapTexture(windowIndex, color);

        TextureRegion colorBitmapTextureRegion = TextureRegionFactory.extractFromTexture(windowTexture);

        Point windowPos = windowsPositionsMap.get(windowIndex);
        double xRatio = (double) BACKGOUND_IMAGE_WIDTH / (double) camera.getCameraSceneWidth();
        double yRatio = (double) BACKGROUND_IMAGE_HEIGHT / (double) camera.getCameraSceneHeight();
        Sprite window = new Sprite(windowPos.x, windowPos.y, (float) (windowTexture.getWidth() / xRatio), (float) (windowTexture.getHeight() / yRatio), colorBitmapTextureRegion, vertexBufferObjectManager);
        window.setTag(windowIndex);
        lidWindowsSprites.put(windowIndex, window);
        window.setAlpha(0);
        scene.attachChild(window);
        window.registerEntityModifier(new AlphaModifier(0.3f, 0, 1));

    }

    /*
     * Lights down specifiedWindow
     */
    public void lightDownWindow(final int windowIndex) {
        if (scene.getChildByTag(windowIndex) != null) {
            final IEntity spriteToBeDetached = scene.getChildByTag(windowIndex);
            Log.d(TAG, "ENGINE: " + parentActivity.getEngine());
            parentActivity.runOnUpdateThread(new Runnable() {
                @Override
                public void run() {
                    spriteToBeDetached.detachSelf();
                }
            });

            lidWindowsSprites.remove(windowIndex);
        }
    }

    /*
     * Lights down all windows on scene
     */
    public void lightDownAllWindows() {
        if (lidWindowsSprites != null) {
            for (Integer windowID : lidWindowsSprites.keySet()) {
                lightDownWindow(windowID);
            }
        }
        // FIX o kousek nahre pri zhasinani odstranujeme i z lidWindowsSprites, tak tady chyby clean na konci
    }

    /*
     * Sets color trio value according to the window index to be lightened
     */
    private void getLidWindowColor(int windowIndex) {
        for (Trio<Integer, Integer, Integer> trio : colorMap.keySet()) {
            if (colorMap.get(trio) == windowIndex) {
                touchedColorTrio = trio;
            }
        }
    }

    // TODO remove
    public boolean texturesLoadedToHW() {
        return true;
    }

    public Map<Integer, Sprite> getLidWindowsSprites() {
        return lidWindowsSprites;
    }

    public void showSolution(List<Integer> generatedWindowNumbers) {
        Map<Integer, Sprite> lidWindowsSpritesCopy = new HashMap<Integer, Sprite>(lidWindowsSprites);
        lightDownAllWindows();

        for (int i = 0; i < generatedWindowNumbers.size(); i++) {
            lightUpWindow(generatedWindowNumbers.get(i), Color.GREEN);
        }
        for (Integer window : lidWindowsSpritesCopy.keySet()) {
            if (!generatedWindowNumbers.contains(window)) {
                lightUpWindow(window, Color.RED);
            }
        }
    }

    public void clearScene() {
        lightDownAllWindows();
        lidWindowsSprites.clear();
    }

}
