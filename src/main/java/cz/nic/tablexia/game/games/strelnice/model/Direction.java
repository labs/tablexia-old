
package cz.nic.tablexia.game.games.strelnice.model;

public enum Direction {

    LEFT, RIGHT;
}
