/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.potme.action;

import cz.nic.tablexia.game.games.potme.creature.Player.PlayerOrientation;
import cz.nic.tablexia.game.games.potme.map.TileMap;
import cz.nic.tablexia.game.games.potme.map.TileMap.TileMapPosition;
import cz.nic.tablexia.game.games.potme.map.mapobstacle.MapObstacle;
import cz.nic.tablexia.game.games.potme.map.mapobstacle.MapObstacleType.MapObstaclePosition;
import cz.nic.tablexia.game.games.potme.map.tile.Tile;

/**
 * Utility class for actions
 * 
 * @author Matyáš Latner
 * 
 */
public class ActionUtility {
	
	/**
	 * Check if is possible to move with player in specific direction. Check direction move with tile map.
	 * 
	 * @param tileMap actual tile map
	 * @param fromTileMapPosition move start position
	 * @param toTileMapPosition destination position 
	 * @return <code>true</code> if move in specific direction is possible
	 */
	public static boolean checkMoveDirection(TileMap tileMap, TileMapPosition fromTileMapPosition, TileMapPosition toTileMapPosition) {
		return checkMoveDirection(tileMap, fromTileMapPosition.getPositionX(), fromTileMapPosition.getPositionY(), toTileMapPosition.getPositionX(), toTileMapPosition.getPositionY());
	}

	/**
	 * Check if is possible to move with player in specific direction. Check direction move with tile map.
	 * 
	 * @param tileMap actual tile map
	 * @param fromTileMapX move start position X
	 * @param fromTileMapY move start position Y
	 * @param toTileMapX destination position X 
	 * @param toTileMapY destination position Y
	 * @return <code>true</code> if move in specific direction is possible
	 */
	protected static boolean checkMoveDirection(TileMap tileMap, int fromTileMapX, int fromTileMapY, int toTileMapX, int toTileMapY) {
		if (fromTileMapX != toTileMapX && fromTileMapY != toTileMapY
				|| fromTileMapX > toTileMapX + 1
				|| fromTileMapX < toTileMapX - 1
				|| fromTileMapY > toTileMapY + 1
				|| fromTileMapY < toTileMapY - 1
				|| !tileMap.isTileAtPosition(fromTileMapX, fromTileMapY)
				|| !tileMap.isTileAtPosition(toTileMapX, toTileMapY)) {
			
			return false;
		} else if (fromTileMapX == toTileMapX && fromTileMapY == toTileMapY) {
			return true;
		} else {
			if (toTileMapX > fromTileMapX) {
				return tileMap.getTileAtPosition(fromTileMapX, fromTileMapY).getTileType().isRightDoor() && tileMap.getTileAtPosition(toTileMapX, toTileMapY).getTileType().isLeftDoor();
			} else if (toTileMapX < fromTileMapX) {
				return tileMap.getTileAtPosition(fromTileMapX, fromTileMapY).getTileType().isLeftDoor() && tileMap.getTileAtPosition(toTileMapX, toTileMapY).getTileType().isRightDoor();
			} else if (toTileMapY < fromTileMapY) {
				return tileMap.getTileAtPosition(fromTileMapX, fromTileMapY).getTileType().isTopDoor() && tileMap.getTileAtPosition(toTileMapX, toTileMapY).getTileType().isBottomDoor();
			} else if (toTileMapY > fromTileMapY) {
				return tileMap.getTileAtPosition(fromTileMapX, fromTileMapY).getTileType().isBottomDoor() && tileMap.getTileAtPosition(toTileMapX, toTileMapY).getTileType().isTopDoor();
			}
			return false;
		}
	}
	
	public static MapObstacle getObstacleInDirection(TileMap tileMap, TileMapPosition fromTileMapPosition, PlayerOrientation playerOrientation) {
		Tile tile = tileMap.getTileAtPosition(fromTileMapPosition);
		switch (playerOrientation) {
			case TOP:
				Tile topNeighbor = tile.getTopNeighbor();
				if (topNeighbor != null) {
					MapObstacle mapObstacle = topNeighbor.getMapObstacle(MapObstaclePosition.BOTTOM_POSITION);
					if (mapObstacle != null) {
						return mapObstacle;
					}
				}
				return null;
				
			case RIGHT:
				Tile rightNeighbor = tile.getRightNeighbor();
				if (rightNeighbor != null) {
					MapObstacle mapObstacle = rightNeighbor.getMapObstacle(MapObstaclePosition.LEFT_POSITION);
					if (mapObstacle != null) {
						return mapObstacle;
					}
				}
				return null;
				
			case BOTTOM:
				MapObstacle bottomMapObstacle = tile.getMapObstacle(MapObstaclePosition.BOTTOM_POSITION);
				if (bottomMapObstacle != null) {
					return bottomMapObstacle;
				}
				return null;
				
			case LEFT:
				MapObstacle leftMapObstacle = tile.getMapObstacle(MapObstaclePosition.LEFT_POSITION);
				if (leftMapObstacle != null) {
					return leftMapObstacle;
				}
				return null;
		}
		return null;
	}
	
	/**
	 * Disable obstacle in player direction next tile
	 * 
	 * @param tileMap actual tile map
	 * @param tileMapPosition actual tile map position
	 * @param playerOrientation actual player orientation
	 * @return <code>true</code> if is the obstacle successfully disabled
	 */
	public static boolean disbaleObstacleInDirection(TileMap tileMap, TileMapPosition tileMapPosition, PlayerOrientation playerOrientation) {
		Tile tile = tileMap.getTileAtPosition(tileMapPosition);
		switch (playerOrientation) {
			case TOP:
				Tile topNeighbor = tile.getTopNeighbor();
				if (topNeighbor != null) {
					topNeighbor.disableMapObstacle(MapObstaclePosition.BOTTOM_POSITION);
					return true;
				}
				return false;
				
			case RIGHT:
				Tile rightNeighbor = tile.getRightNeighbor();
				if (rightNeighbor != null) {
					rightNeighbor.disableMapObstacle(MapObstaclePosition.LEFT_POSITION);
					return true;
				}
				return false;
				
			case BOTTOM:
				tile.disableMapObstacle(MapObstaclePosition.BOTTOM_POSITION);
				return true;
				
			case LEFT:
				tile.disableMapObstacle(MapObstaclePosition.LEFT_POSITION);
				return true;
		}
		return false;
	}
	
}
