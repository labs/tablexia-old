package cz.nic.tablexia.game.games.potme.action.rule;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.util.modifier.IModifier;

import cz.nic.tablexia.game.common.EntityModifierListenerAdapter;
import cz.nic.tablexia.game.games.potme.PotmeActivity;
import cz.nic.tablexia.game.games.potme.ResourceManager;
import cz.nic.tablexia.game.games.potme.UtilityAccess;
import cz.nic.tablexia.game.games.potme.action.ActionType.IActionFinishedListener;
import cz.nic.tablexia.game.games.potme.creature.Player;
import cz.nic.tablexia.game.games.potme.map.TileMap;

/**
 * Abstract action type rule
 * 
 * @author Matyáš Latner
 */
public abstract class AbstractActionTypeRule {
	
	public abstract void performActionRule(PotmeActivity potmeActivity, TileMap tileMap, Player player, int mapStartPositionX, int mapStartPositionY, IActionFinishedListener actionFinishedListener);
	
	protected void performOnActionFinished(IActionFinishedListener finishedListener, boolean result) {			
		if (finishedListener != null) {
			finishedListener.onActionFinished(result);
		}
	}
	
	protected void performOnActionFinishWithDelay(IEntity entity, final IActionFinishedListener finishedListener, final boolean result, final int delay) {
    	entity.registerEntityModifier(new DelayModifier(delay, new EntityModifierListenerAdapter() {
            @Override
            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
            	performOnActionFinished(finishedListener, result);
            }
        }));
    }
	
	protected void playSound(String sound) {
		ResourceManager.getInstance().getSound(sound).play();
	}
	
	protected void playRandomSoundFromGroup(String[] soundGroup) {
		UtilityAccess.getInstance().playRandomSoundFromGroup(soundGroup);
	}
	
}
