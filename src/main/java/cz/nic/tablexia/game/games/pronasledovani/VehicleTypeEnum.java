package cz.nic.tablexia.game.games.pronasledovani;

public enum VehicleTypeEnum {
    CAR,
    MOTORCYCLE,
    TRAIN,
    BOAT

}
