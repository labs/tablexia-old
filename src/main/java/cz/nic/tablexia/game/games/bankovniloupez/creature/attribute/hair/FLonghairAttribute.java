/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.hair;

import java.util.ArrayList;
import java.util.List;

import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureRoot.AttributeGender;

public class FLonghairAttribute extends HairAttribute {

    private static final List<AttributeDescription> TEXTURES = new ArrayList<AttributeDescription>() {

        private static final long serialVersionUID = -969047970752296076L;

        {
        	add(new AttributeDescription(AttributeColor.BLACK, AttributeGender.FEMALE, "f_longhair_black.png", FLonghairAttribute.class));
            add(new AttributeDescription(AttributeColor.BROWN, AttributeGender.FEMALE, "f_longhair_brown.png", FLonghairAttribute.class));
            add(new AttributeDescription(AttributeColor.YELLOW, AttributeGender.FEMALE, "f_longhair_yellow.png", FLonghairAttribute.class));
        }
    };

    public static List<AttributeDescription> getTextures() {
        return TEXTURES;
    }
    
    public static AttributeGender getAttributeGender() {
		return AttributeGender.FEMALE;
	}

    public FLonghairAttribute(AttributeColor color, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(color, pTextureRegion, pVertexBufferObjectManager);
    }

}
