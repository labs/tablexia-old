/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.nocnisledovani;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.scene.IOnAreaTouchListener;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.graphics.Point;
import android.util.Log;
import android.util.Pair;
import android.view.MotionEvent;
import cz.nic.tablexia.game.games.nocnisledovani.listener.OnHandDragCompleteListener;
import cz.nic.tablexia.game.games.nocnisledovani.model.MySprite;

/*
 * custom scene containing complete watch and providing metods for manipulation with it
 */
public class MyWatchScene extends Scene {
    private static final String       TAG                       = MyWatchScene.class.getSimpleName();
    private static final float        SET_TIME_DEGREE_TOLERANCE = 10f;                               // degree by which user can move hand behind exact hour and
    // still get the right time
    private static final float        WATCH_SCALE               = 0.25f;
    private static final float        HOUR_HAND_SCALE           = 0.17f;
    private static final float        MINUTE_HAND_SCALE         = 0.25f;

    private Sprite                    watchSprite;
    private MySprite                  hourHandSprite;
    private MySprite                  minuteHandSprite;
    private boolean                   hourHandTouched;

    private GfxManager                gfxManager;
    private MfxManager                mfxManager;
    private TextureManager            textureManager;
    private VertexBufferObjectManager vertexBufferObjectManager;

    private Sprite                    hourHandCorrectTime;
    private Sprite                    hourHandWrongTime;

    private IOnSceneTouchListener     onSceneTouchListener;

    public MyWatchScene(GfxManager gfxManager, MfxManager mfxManager, TextureManager textureManager, VertexBufferObjectManager vertexBufferObjectManager, Camera camera, Point displaySize) {
        this.gfxManager = gfxManager;
        this.mfxManager = mfxManager;
        this.textureManager = textureManager;
        this.vertexBufferObjectManager = vertexBufferObjectManager;

        watchSprite = gfxManager.getWatchSprite(WATCH_SCALE, camera.getCameraSceneWidth() / 2, 0, vertexBufferObjectManager);
        hourHandSprite = gfxManager.getHourHandSprite(HOUR_HAND_SCALE, camera.getCameraSceneWidth() / 2, gfxManager.getClockCenterYPosition(WATCH_SCALE), vertexBufferObjectManager, new OnHandDragCompleteListener() {

            @Override
            public void onHandDragComplete() {
                int[] time = getTimeFromAngle(hourHandSprite.getRotation(), 0);
                setTimeOnWatch(time, 0);
            }
        });

        hourHandSprite.setRotationCenter(0.5f, 0);

        minuteHandSprite = gfxManager.getMinuteHandSprite(MINUTE_HAND_SCALE, camera.getCameraSceneWidth() / 2, gfxManager.getClockCenterYPosition(WATCH_SCALE), vertexBufferObjectManager, new OnHandDragCompleteListener() {

            @Override
            public void onHandDragComplete() {
                // nothing
            }
        });
        hourHandSprite.setAreaTouchListener(getHandTouchListener());
        attachChild(watchSprite);
        attachChild(minuteHandSprite);
        attachChild(hourHandSprite);
        setBackgroundEnabled(false);

        onSceneTouchListener = new IOnSceneTouchListener() {

            @Override
            public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
                if (hourHandTouched) {
                    switch (pSceneTouchEvent.getAction()) {
                        case MotionEvent.ACTION_MOVE:
                            playClockSound();
                            hourHandSprite.setRotation(NocniSledovaniHelper.getAngleFromLinearApproximationOfPoints(new Point((int) pSceneTouchEvent.getX(), (int) pSceneTouchEvent.getY()), new Point((int) hourHandSprite.getX(), (int) (hourHandSprite.getY() - (hourHandSprite.getHeight() / 2)))));
                            break;
                        case MotionEvent.ACTION_UP:
                            playClockSound();
                            hourHandSprite.getOnHandDragCompleteListener().onHandDragComplete();
                            hourHandTouched = false;
                            break;
                    }
                    return true;
                } else {// ruka se nedotkne rucicky, ale jen sceny s hodinami
                    switch (pSceneTouchEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            if (watchSprite.contains(pSceneTouchEvent.getX(), pSceneTouchEvent.getY())) {
                                hourHandSprite.setRotation(NocniSledovaniHelper.getAngleFromLinearApproximationOfPoints(new Point((int) pSceneTouchEvent.getX(), (int) pSceneTouchEvent.getY()), new Point((int) hourHandSprite.getX(), (int) (hourHandSprite.getY() - (hourHandSprite.getHeight() / 2)))));
                                hourHandTouched = true;
                            }
                            break;
                    }
                }
                return false;
            }
        };

        setOnSceneTouchListener(onSceneTouchListener);
    }

    private void playClockSound() {
        if (!MyWatchScene.this.mfxManager.getSound(MfxManager.SOUND_CLOCK).isPlaying()) {
            MyWatchScene.this.mfxManager.playSound(MfxManager.SOUND_CLOCK);
        }
    }

    /*
     * sets the time on watch a, current version sets hours only hand index
     * 0->hour hand,1->minute hand
     */

    public void setTimeOnWatch(int[] time, int handIndex) {
        Log.d(TAG, "Setting time on watch");
        if (handIndex == 0) {// hour hand
            int hour = time[0] % 12;
            hourHandSprite.setRotation(hour * 30);
        } else if (handIndex == 1) {// minute hand
            int minute = time[1] % 60;
            minuteHandSprite.setRotation(minute * 6);
        }
        Log.d(TAG, "Time on watch set");
    }

    /*
     * gets the time from the angle of hour hands [0]->hours,[1]->minutes
     */
    private int[] getTimeFromAngle(float angle, int handIndex) {
        int[] arrayToReturn = new int[2];
        int hours = 0;
        int minutes = 0;
        switch (handIndex) {
            case 0:// hour hand
                float angleInsideThreeSixty = angle % 360;
                if ((angleInsideThreeSixty % 30) > SET_TIME_DEGREE_TOLERANCE) {
                    hours = (int) ((angle / 30) + 1);
                } else {
                    hours = (int) (angle / 30);
                }
                break;
            case 1:
                // TODO in need of setting minutes add code here
                minutes = 0;
        }
        arrayToReturn[0] = hours;
        arrayToReturn[1] = minutes;
        return arrayToReturn;
    }

    /*
     * returns the time which is currently set on watch(checks only hour hand)
     */
    public int[] getCurrentlySetTime() {
        return getTimeFromAngle(hourHandSprite.getRotation(), 0);
    }

    private IOnAreaTouchListener getHandTouchListener() {
        return new IOnAreaTouchListener() {

            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, ITouchArea pTouchArea, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                hourHandTouched = true;
                return false;
            }
        };
    }

    public void enableHourhand() {
        registerTouchArea(hourHandSprite);
        registerTouchArea(this);
        setOnSceneTouchListener(onSceneTouchListener);
    }

    public void disableHourHand() {
        unregisterTouchArea(hourHandSprite);
        unregisterTouchArea(this);
        setOnSceneTouchListener(null);
    }

    /*
     * Method shows colored hour hands on watch to present correct solution to user
     */
    public void showSolution(int[] generatedTime) {
        disableHourHand();
        Pair<MyBitmapTexture, TextureRegion> correctHHTextures = gfxManager.getCorrectTimeHourHandTextures();

        hourHandCorrectTime = new Sprite(hourHandSprite.getX(), hourHandSprite.getY(), hourHandSprite.getWidth() * hourHandSprite.getScaleX(), hourHandSprite.getHeight() * hourHandSprite.getScaleY(), correctHHTextures.second, vertexBufferObjectManager);
        int hour = generatedTime[0] % 12;
        hourHandCorrectTime.setRotationCenter(hourHandSprite.getRotationCenterX(), hourHandSprite.getRotationCenterY());
        hourHandCorrectTime.setRotation(hour * 30);
        attachChild(hourHandCorrectTime);

        Pair<MyBitmapTexture, TextureRegion> wrongHHTextures = gfxManager.getWrongTimeHourHandTextures();

        hourHandWrongTime = new Sprite(hourHandSprite.getX(), hourHandSprite.getY(), hourHandSprite.getWidth() * hourHandSprite.getScaleX(), hourHandSprite.getHeight() * hourHandSprite.getScaleY(), wrongHHTextures.second, vertexBufferObjectManager);
        int wrongHour = (int) (hourHandSprite.getRotation() / 30);
        hourHandWrongTime.setRotationCenter(hourHandSprite.getRotationCenterX(), hourHandSprite.getRotationCenterY());
        hourHandWrongTime.setRotation(wrongHour * 30);
        // shows the red hour hand only in case that the user did not guess the time correctly
        if (wrongHour != hour) {
            attachChild(hourHandWrongTime);
        }
    }

    /*
     * Method removes hour hands showing the correct solution from the scene
     */
    public void clearSolution() {
        registerUpdateHandler(new IUpdateHandler() {
            @Override
            public void onUpdate(float pSecondsElapsed) {
                if (hourHandCorrectTime != null) {
                    hourHandCorrectTime.detachSelf();
                }
                if (hourHandWrongTime != null) {
                    hourHandWrongTime.detachSelf();
                }
                unregisterUpdateHandler(this);
            }

            @Override
            public void reset() {
            }
        });
    }

}
