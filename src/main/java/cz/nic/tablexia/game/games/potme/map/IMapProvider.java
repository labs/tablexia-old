package cz.nic.tablexia.game.games.potme.map;

import cz.nic.tablexia.game.games.potme.map.mapobject.MapObjectType;
import cz.nic.tablexia.game.games.potme.map.tile.Tile;

public interface IMapProvider {
	
	public TileMap prepareMap(Tile lastFinishTile, int mapXSize, int mapYSize, MapObjectType finishMapObject, boolean hasKey);

}
