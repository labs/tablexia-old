/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.rules.medium;

import java.util.ArrayList;
import java.util.List;

import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.Context;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureFactory;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureRoot;
import cz.nic.tablexia.game.games.bankovniloupez.rules.GameRuleUtility;

/**
 * 
 * @author Matyáš Latner
 */
public class CC_1_CCRule extends GameRuleUtility {
    
	private   static final int 	 	GROUP_SIZE	= 3;
	protected static final Integer 	T0_OFFSET 	= CreatureDescriptor.THIEF_OFFSET;
	protected 			   Integer 	T1_OFFSET 	= Integer.valueOf(2);

    public CC_1_CCRule(Context context, RandomAccess randomAccess, VertexBufferObjectManager vertexBufferObject, int numberOfCreatures, int numberOfThieves) {
        super(context, randomAccess, vertexBufferObject, numberOfCreatures, numberOfThieves, GROUP_SIZE);
    }
    
    public CC_1_CCRule(Context context, RandomAccess randomAccess, VertexBufferObjectManager vertexBufferObject, int numberOfCreatures, int numberOfThieves, int creatureNumber) {
        super(context, randomAccess, vertexBufferObject, numberOfCreatures, numberOfThieves, creatureNumber);
    }
    
    @Override
    public String[] prepareRuleMessageParameters() {
        return new String[] {
        		getAttributeColorName(getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0)),
        		getAttributeName(getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0), false),
        		getAttributeColorName(getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0)),
        		getAttributeName(getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0), false)
        };
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
    	CreatureDescriptor t1CreatureDescriptor = getRandomCreatureDescriptionWithOneAttribute(CreatureFactory.getInstance().generateCreature(null, BAN_ATTRIBUTES_SET_FOR_GENERATING, vertexBufferObject, getRandomAccess()).getCreatureDescrition());
        addGlobalCreatureDescriptor(T1_OFFSET, t1CreatureDescriptor);
        
        CreatureDescriptor creatureDescriptorToBan = new CreatureDescriptor();
    	creatureDescriptorToBan.disableGenderCompatibilityCheck();
    	creatureDescriptorToBan.addDescriptions(BAN_ATTRIBUTES_SET_FOR_GENERATING.getDescriptions());
    	creatureDescriptorToBan.addDescriptions(t1CreatureDescriptor.getDescriptions());
    	
        CreatureDescriptor t0CreatureDescriptor = getRandomCreatureDescriptionWithOneAttribute(CreatureFactory.getInstance().generateCreature(null, creatureDescriptorToBan, vertexBufferObject, getRandomAccess()).getCreatureDescrition());
        addGlobalCreatureDescriptor(T0_OFFSET, t0CreatureDescriptor);
    }
    
    @Override
    protected List<CreatureRoot> prepareCreatureDescriptionsA() {
    	
    	AttributeDescription t1AttributeDescription = getGlobalCreatureDescriptor(T1_OFFSET).getDescriptions().get(0);
    	AttributeDescription t0AttributeDescription = getGlobalCreatureDescriptor(T0_OFFSET).getDescriptions().get(0);
    	
    	List<CreatureRoot> creatures = new ArrayList<CreatureRoot>();
        for (int i = 0; i < numberOfCreatures; i++) {
        	
        	CreatureDescriptor creatureDescriptorToBan = new CreatureDescriptor();
        	creatureDescriptorToBan.disableGenderCompatibilityCheck();
        	
        	CreatureRoot lastCreature = null;
			int lastPosition = i - T1_OFFSET;
			if (lastPosition >= 0) {					
				lastCreature = creatures.get(lastPosition);
			}
        	
            CreatureDescriptor creatureDescriptor = specialCreatures.get(i);
			if (creatureDescriptor != null) {
            	
            	// SPECIAL CREATURE
            	
            	if (!creatureDescriptor.isThief() && lastCreature != null && lastCreature.hasAttribute(t1AttributeDescription)) {
            		// T1 SPECIAL CREATURE
					creatureDescriptorToBan.addDescription(t0AttributeDescription);
				}
            	
            	creatures.add(CreatureFactory.getInstance().generateCreature(creatureDescriptor, creatureDescriptorToBan, vertexBufferObject, getRandomAccess()));
            } else {
            	
            	// RANDOM CREATURE
				
				boolean lastCreatureHasT1Attribute = lastCreature != null && lastCreature.hasAttribute(t1AttributeDescription);
				if (lastCreatureHasT1Attribute) {
					creatureDescriptorToBan.addDescription(t0AttributeDescription);
				}
				
				// BAIT ATTRIBUTE
				if (getRandomAccess().getRandom().nextBoolean() || lastCreatureHasT1Attribute) {					
					newBaitCreatureDescriptionFromAttribute(t1AttributeDescription);
				} else {
					newBaitCreatureDescriptionFromAttribute(t0AttributeDescription);
				}
				
				creatures.add(CreatureFactory.getInstance().generateCreature(getBaitCreatureDescriptionRandomly(), creatureDescriptorToBan, vertexBufferObject, getRandomAccess()));
            }
        }

        return creatures;
    }

}
