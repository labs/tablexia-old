/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.potme.action.widget;

import java.util.List;

import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.FadeInModifier;
import org.andengine.entity.modifier.FadeOutModifier;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;
import org.andengine.util.modifier.IModifier;

import android.os.Handler;
import android.util.SparseArray;
import cz.nic.tablexia.game.common.EntityModifierListenerAdapter;
import cz.nic.tablexia.game.games.potme.PotmeActivity;
import cz.nic.tablexia.game.games.potme.PotmeActivity.StartButton;
import cz.nic.tablexia.game.games.potme.PotmeDifficulty;
import cz.nic.tablexia.game.games.potme.ResourceManager;
import cz.nic.tablexia.game.games.potme.action.Action;
import cz.nic.tablexia.game.games.potme.action.Action.ActionListener;
import cz.nic.tablexia.game.games.potme.action.ActionType;

/**
 * Widget for showing action icons
 * 
 * @author Matyáš Latner
 *
 */
public class ActionsWidget extends Entity implements ActionListener {

	public enum ActionLayer {
		BACKGROUND_LAYER			(0),
        ACTIONS_LAYER				(1),
		INFO_LAYER					(2);

        private Entity layerEntity;
        private int layerZIndex;

        private ActionLayer(int layerZIndex) {
            this.layerZIndex = layerZIndex;
        }

        private void setLayerEntity(Entity layerEntity) {
            this.layerEntity = layerEntity;
        }

        public Entity getLayerEntity() {
            return layerEntity;
        }

        public int getLayerZIndex() {
            return layerZIndex;
        }

        public static void attachLayersToEntity(IEntity conteinerEntity) {
            for (ActionLayer actionLayer : ActionLayer.values()) {
                Entity entity = new Entity();
                actionLayer.setLayerEntity(entity);
                conteinerEntity.attachChild(entity);
                entity.setZIndex(actionLayer.getLayerZIndex());
            }
            conteinerEntity.sortChildren();
        }
    }
	
	private static final float 	TUTORIAL_INFO_ARROW_BLINK_DURATION 	= 0.3f;
	private static final float 	TUTORIAL_INFO_ARROW_FADEIN_DELAY	= 0.5f;
	private static final int 	TOP_Z_INDEX 						= ActionType.values().length + 1;
	private static final float 	BACKGROUND_WIDTH_RATIO 				= 1.3f;
	private static final double BACKGROUND_Y_POSITION_RATIO 		= 0.9;
	private static final int 	START_ANIMATION_DELAY 				= 200;
	private static final int 	NUMBER_OF_COLUMNS 					= 2;
	public  static final int 	ACTION_OFFSET 						= PotmeActivity.ACTION_SIZE_SMALLER / 10;
	private static final float 	DIMMER_ALPHA 						= 0.5f;
	private static final Color 	DIMMER_COLOR 						= Color.BLACK;
	private static final float 	START_ARROW_X_OFFSET 				= PotmeActivity.ACTION_SIZE_SMALLER * 1.3f;
	private static final int 	START_ARROW_Y_OFFSET 				= PotmeActivity.ACTION_SIZE_SMALLER / 2;
	private static final float 	START_ARROW_WIDTH 					= PotmeActivity.TILE_SIZE * 1.5f;
	private static final float 	START_ARROW_HEIGHT 					= PotmeActivity.TILE_SIZE;
	
	private ActionsStripWidget 			actionsStripWidget;
	private SparseArray<Action>			actions;
	private VertexBufferObjectManager 	vertexBufferObjectManager;
	private final Scene 				scene;
	private Rectangle 					dimmer;
	private PotmeDifficulty 			potmeDifficulty;
	private int 						currentStepNumber;
	private StartButton 				startButton;
	

	public ActionsWidget(float positionX,
						 float positionY,
						 float displaySizeX,
						 float displaySizeY,
						 PotmeDifficulty potmeDifficulty,
						 Scene scene,
						 ActionsStripWidget actionsStripWidget,
						 float actionStripWidgetWidth,
						 VertexBufferObjectManager vertexBufferObjectManager) {
		
		super(positionX, positionY);
		this.potmeDifficulty = potmeDifficulty;
		this.scene = scene;
		this.actionsStripWidget = actionsStripWidget;
		this.vertexBufferObjectManager = vertexBufferObjectManager;
		this.actions = new SparseArray<Action>();
		
		currentStepNumber = 0;
		
		List<ActionType> actionTypes = ActionType.getActionTypesForGameDifficulty(potmeDifficulty);
		
		// BACKGROUND
		ITextureRegion backgroundTexture = ResourceManager.getInstance().getTexture(ResourceManager.BACKGROUND_ACTIONS_TEXTURE);
		int backgroundWidth = (int)((NUMBER_OF_COLUMNS * PotmeActivity.ACTION_SIZE_SMALLER) * BACKGROUND_WIDTH_RATIO);
		int backgroundHeight = (int)(backgroundWidth * (backgroundTexture.getHeight() / backgroundTexture.getWidth()));
		Sprite actionsBackground = new Sprite((NUMBER_OF_COLUMNS * (PotmeActivity.ACTION_SIZE_SMALLER + (2 * ACTION_OFFSET))) / 2,
											  -(int)((((Math.round(actionTypes.size() / 2) + (actionTypes.size() % 2)) * (PotmeActivity.ACTION_SIZE_SMALLER)) / 2) * BACKGROUND_Y_POSITION_RATIO),
											  backgroundWidth,
											  backgroundHeight,
											  backgroundTexture, vertexBufferObjectManager);
		
		
		// DIMMER
		float actionStripWidgetOffset = (actionStripWidgetWidth / 2) * 0.95f;
		dimmer = new Rectangle(-positionX + (displaySizeX / 2) - actionStripWidgetOffset,
										 -positionY + (displaySizeY / 2),
										 displaySizeX - actionStripWidgetOffset,
										 displaySizeY,
										 vertexBufferObjectManager);
		dimmer.setColor(DIMMER_COLOR);
		dimmer.setAlpha(DIMMER_ALPHA);
		dimmer.setVisible(false);
		
		ActionLayer.attachLayersToEntity(this);
		for (int i = 0; i < actionTypes.size(); i++) {
			ActionType actionType = actionTypes.get(i);
			createAction(actionType, i, actionType.ordinal(), false, scene);
		}
		ActionLayer.ACTIONS_LAYER.getLayerEntity().attachChild(dimmer);
		ActionLayer.BACKGROUND_LAYER.getLayerEntity().attachChild(actionsBackground);
		tryDimmAllActions();
	}
	
	public void setStartButton(StartButton startButton) {
		this.startButton = startButton;
	}
	
	private Action createAction(ActionType actionType, int position, int orderNumber, boolean visible, Scene scene) {
		Action action = new Action(actionType,
				   				   orderNumber,
				   				   PotmeActivity.ACTION_SIZE_SMALLER,
				   				   2 * ACTION_OFFSET + PotmeActivity.ACTION_SIZE_SMALLER / 2 + (position % NUMBER_OF_COLUMNS) * (PotmeActivity.ACTION_SIZE_SMALLER + ACTION_OFFSET),
				   				   - 2 * ACTION_OFFSET - (((int) (position / NUMBER_OF_COLUMNS)) * (PotmeActivity.ACTION_SIZE_SMALLER + ACTION_OFFSET)),
				   				   visible,
				   				   vertexBufferObjectManager);
		
		action.addActionListener(this);
		action.setClickable(scene);
		ActionLayer.ACTIONS_LAYER.getLayerEntity().attachChild(action);
		actions.put(action.getOrderNumber(), action);
		return action;
	}
	
	
	/* //////////////////////////////////////////// COLLISIONS */
	
	private void prepareActionsCollisionEntity(Action action) {
		action.setCollisionEntity(actionsStripWidget);
	}
	
	
	/* //////////////////////////////////////////// ANIMATIONS */
	
	public void showActions() {
		int actualDelay = 0;
		for (int i = 0; i < actions.size(); i++) {
			final Action action = actions.get(actions.keyAt(i));
			(new Handler()).postDelayed(new Runnable() {
				
				@Override
				public void run() {
					action.showWithAnimation();
					prepareActionsCollisionEntity(action);
				}
				
			}, actualDelay);
			actualDelay = actualDelay + START_ANIMATION_DELAY;
		}
		(new Handler()).postDelayed(new Runnable() {
			
			@Override
			public void run() {
				tryToPerformNextTutorialStep();
			}
			
		}, actualDelay + START_ANIMATION_DELAY);
	}
	
	
	/* //////////////////////////////////////////// ACTION STATE */
	
	public void enableActions() {
		if (potmeDifficulty != PotmeDifficulty.TUTORIAL) {
			for (int i = 0; i < actions.size(); i++) {
				Action action = actions.get(actions.keyAt(i));
				action.enable();
			}
		}
	}
	
	public void disableActions() {
		for (int i = 0; i < actions.size(); i++) {
			Action action = actions.get(actions.keyAt(i));
			action.disable();
		}
	}
	
	
	/* //////////////////////////////////////////// ACTION LISTENER */

	@Override
	public void onActionDrag(Action lastAction) {
		PotmeActivity.GameLayer.ACTIONS_LAYER.sendToFront(scene);
		Action action = createAction(lastAction.getActionType(), actions.indexOfKey(lastAction.getOrderNumber()), lastAction.getOrderNumber(), false, scene);
		action.showWithAnimation();
		prepareActionsCollisionEntity(action);
		disableActions();
		hideArrowSprite();
	}
	
	@Override
	public void onActionDrop(Action action, int collidesWithNumber) {
		PotmeActivity.GameLayer.resetLayersZIndexes(scene);
		if (collidesWithNumber != Action.NO_COLLISION_NUMBER) {
			actionsStripWidget.addSelectedAction(action.getActionType(), collidesWithNumber);
			currentStepNumber++;
		}
		tryToPerformNextTutorialStep();
	}
	
	
	/* //////////////////////////////////////////// TUTORIAL STEPS */
	
	private void tryToPerformNextTutorialStep() {
		if (potmeDifficulty == PotmeDifficulty.TUTORIAL) {
        	if (currentStepNumber < PotmeActivity.TUTORIAL_STEPS.size()) {	        		
        		highliteAction(PotmeActivity.TUTORIAL_STEPS.get(currentStepNumber));
        	} else {
        		PotmeActivity.GameLayer.BUTTON_LAYER.sendToFront(scene);
        		if (startButton != null) {
        			startButton.enable();
        			startButton.enablePulsing();
        		}
        	}
        } else {
        	enableActions();
        }
	}
	
	private void tryDimmAllActions() {
		if (potmeDifficulty == PotmeDifficulty.TUTORIAL) {
			dimmer.setVisible(true);
			for (int i = 0; i < actions.size(); i++) {
				Action action = actions.get(actions.keyAt(i));
				action.setZIndex(action.getOrderNumber());
				action.disable();
			}
			dimmer.setZIndex(actions.size());
			ActionLayer.ACTIONS_LAYER.getLayerEntity().sortChildren();
		}
	}
	
	private void highliteAction(ActionType actionType) {
		if (actionType.ordinal() < actions.size()) {			
			tryDimmAllActions();
			Action action = actions.get(actionType.ordinal());
			action.setZIndex(TOP_Z_INDEX);
			action.enable();
			showArrowSprite(action.getX(), action.getY(), vertexBufferObjectManager);
			ActionLayer.ACTIONS_LAYER.getLayerEntity().sortChildren();
		}
	}
	
	private void showArrowSprite(float positionX, float positionY, VertexBufferObjectManager vertexBufferObjectManager) {
		hideArrowSprite();
		final Sprite infoArrowSprite = new Sprite(positionX + START_ARROW_X_OFFSET,
								   	 			  positionY - START_ARROW_Y_OFFSET,
								   	 			  START_ARROW_WIDTH,
								   	 			  START_ARROW_HEIGHT,
								   	 			  ResourceManager.getInstance().getTexture(ResourceManager.INFO_START_ARROW),
								   	 			  vertexBufferObjectManager);
		
		infoArrowSprite.setAlpha(0);
    	ActionLayer.INFO_LAYER.getLayerEntity().attachChild(infoArrowSprite);
    	
    	FadeInModifier fadeIn = new FadeInModifier(TUTORIAL_INFO_ARROW_BLINK_DURATION);
    	fadeIn.setAutoUnregisterWhenFinished(true);
    	FadeOutModifier fadeOut = new FadeOutModifier(TUTORIAL_INFO_ARROW_BLINK_DURATION);
    	fadeOut.setAutoUnregisterWhenFinished(true);
    	final SequenceEntityModifier sequenceModifier = new SequenceEntityModifier(fadeIn, fadeOut);
    	sequenceModifier.setAutoUnregisterWhenFinished(true);
    	
    	DelayModifier delayModifier = new DelayModifier(TUTORIAL_INFO_ARROW_FADEIN_DELAY);
    	delayModifier.setAutoUnregisterWhenFinished(true);
		delayModifier.addModifierListener(new EntityModifierListenerAdapter() {
    		
    		@Override
    		public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {    			
    			infoArrowSprite.registerEntityModifier(new LoopEntityModifier(sequenceModifier));
    		}
    		
    	});
		infoArrowSprite.registerEntityModifier(delayModifier);
	}
	
	private void hideArrowSprite() {
		ActionLayer.INFO_LAYER.getLayerEntity().detachChildren();
	}
}
