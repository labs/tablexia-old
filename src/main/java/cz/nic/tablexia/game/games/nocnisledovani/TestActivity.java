package cz.nic.tablexia.game.games.nocnisledovani;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.RotationModifier;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.util.FPSLogger;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.source.EmptyBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.bitmap.source.decorator.BaseBitmapTextureAtlasSourceDecorator;
import org.andengine.opengl.texture.bitmap.AssetBitmapTexture;
import org.andengine.opengl.texture.bitmap.BitmapTexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.modifier.IModifier.DeepCopyNotSupportedException;

import cz.nic.tablexia.game.games.nocnisledovani.GfxManager.OnInitFinished;
import cz.nic.tablexia.game.games.nocnisledovani.listener.WindowListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.view.Display;

/**
 * (c) 2010 Nicolas Gramlich
 * (c) 2011 Zynga
 *
 * @author Nicolas Gramlich
 * @since 11:54:51 - 03.04.2010
 */
public class TestActivity extends SimpleBaseGameActivity {
	// ===========================================================
	// Constants
	// ===========================================================

	private static final String BACKGROUNDS_DIR = NocniSledovaniActivity.BASE_DIR + "gfx/background/";
	private static final String WINDOWS_DIR = NocniSledovaniActivity.BASE_DIR + "gfx/windows/";
	private static final String WINDOWS_BITMAP_FILE_NAME = "windows";
	private static final String ASSETS_DIR = NocniSledovaniActivity.BASE_DIR + "gfx/";
	private static final String TAG = "GFXMANAGER_STAKEOUT";
	private static final String EIGHT_WINDOWS_FOLDER = "8/";
	private static final String SIXTEEN_WINDOWS_FOLDER = "16/";
	private static final String TWENTYFOUR_WINDOWS_FOLDER = "24/";
	private static final String THIRTYTWO_WINDOWS_FOLDER = "32/";
	private static final int NUMBER_OF_FRAMES_FOR_BACKGROUND_ANIMATION = 7; // pocet
																			// pozadi
																			// pro
																			// animaci
																			// ze
																			// dne/noc
																			// resp.
																			// noci/den
	private static final String COLOR_BITMAP_NAME = "colors";

	private static final int CLOCK_CENTER_Y = 183;
	private static final int HOUR_HAND_Y_SHIFT = 9; // stred rotace rucicky pro
													// shodu s grafikou
	private static final int MINUTE_HAND_Y_SHIFT = 17;

	private static final String BACKGROUND_EXTENSION = ".png";
	private static final String ASSETS_EXTENSION = ".png";

	private static final String WATCH_BITMAP_NAME = "watch";
	private static final String HOUR_HAND_NAME = "hour_hand_moving";
	private static final String MINUTE_HAND_NAME = "minute_hand_static";

	private static final int BACKGROUND_LARGER_SIDE_SIZE = 1920;
	
	private static final int CAMERA_WIDTH = 480;
	private static final int CAMERA_HEIGHT = 320;
	private GfxManager gfxManager;
	private Camera camera;
	protected Point                displaySize;
	
	private WindowListener windowCLickListener;
	private List<Integer> lightWindows = new ArrayList<Integer>();

	// ===========================================================
	// Fields
	// ===========================================================

	private BitmapTextureAtlas mBitmapTextureAtlas;
	private ITextureRegion mDecoratedBalloonTextureRegion;

	// ===========================================================
	// Constructors
	// ===========================================================

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	@Override
	public EngineOptions onCreateEngineOptions() {
		camera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		return new EngineOptions(true, ScreenOrientation.LANDSCAPE_SENSOR, new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), camera);
	}

	@Override
	public void onCreateResources() {
        Display display = getWindowManager().getDefaultDisplay();
        displaySize = new Point();
        display.getSize(displaySize);
		gfxManager = new GfxManager(getTextureManager(), getAssets(), getApplicationContext(), getVertexBufferObjectManager(), camera, displaySize);
		this.mBitmapTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 190, 190, TextureOptions.BILINEAR);
		
		final IBitmapTextureAtlasSource baseTextureSource = new EmptyBitmapTextureAtlasSource(190, 190);
		final IBitmapTextureAtlasSource decoratedTextureAtlasSource = new BaseBitmapTextureAtlasSourceDecorator(baseTextureSource) {
			@Override
			protected void onDecorateBitmap(Canvas pCanvas) throws Exception {
				
//				this.mPaint.setColorFilter(new LightingColorFilter(Color.argb(128, 128, 128, 255), Color.TRANSPARENT));
//				final Bitmap balloon = BitmapFactory.decodeStream(TestActivity.this.getAssets().open("gfx/texturecompositing/balloon.png"));
//				pCanvas.drawBitmap(balloon, 0, 0, this.mPaint);
//				this.mPaint.setColorFilter(null);
//
//				this.mPaint.setXfermode(new PorterDuffXfermode(Mode.SRC_ATOP));
//				final Bitmap alphamask = BitmapFactory.decodeStream(TestActivity.this.getAssets().open("gfx/texturecompositing/alphamask.png"));
//				pCanvas.drawBitmap(alphamask, 0, 0, this.mPaint);
//				this.mPaint.setXfermode(null);
//
//				final Bitmap zynga = BitmapFactory.decodeStream(TestActivity.this.getAssets().open("gfx/texturecompositing/zynga.png"));
//				this.mPaint.setColorFilter(new LightingColorFilter(Color.RED, Color.TRANSPARENT));
//				pCanvas.drawBitmap(zynga, 0, 0, this.mPaint);
			}

			@Override
			public BaseBitmapTextureAtlasSourceDecorator deepCopy() {
				throw new DeepCopyNotSupportedException();
			}
		};

		this.mDecoratedBalloonTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromSource(this.mBitmapTextureAtlas, decoratedTextureAtlasSource, 0, 0);
		this.mBitmapTextureAtlas.load();
	}

	@Override
	public Scene onCreateScene() {
		this.mEngine.registerUpdateHandler(new FPSLogger());
		final Scene scene = new Scene();
		gfxManager.initTextures(3,  7, new OnInitFinished() {

			@Override
			public void onFinished() {
				String path = BACKGROUNDS_DIR + "32/" + 0 + ASSETS_EXTENSION;
				final float centerX = CAMERA_WIDTH / 2;
				final float centerY = CAMERA_HEIGHT / 2;
				BitmapTexture bgTexture = null;
				try {
					bgTexture = new AssetBitmapTexture(getTextureManager(), getAssets(), path);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				bgTexture.load();
				TextureRegion bgTextureRegion = TextureRegionFactory.extractFromTexture(bgTexture);
				Sprite bgSprite = new Sprite(camera.getCameraSceneWidth() / 2,
						camera.getCameraSceneHeight() / 2, camera.getCameraSceneWidth(), camera.getCameraSceneHeight(), bgTextureRegion,getVertexBufferObjectManager());
				
				scene.setBackground(new SpriteBackground(bgSprite));

				windowCLickListener = new WindowListener(null,3, gfxManager, new Point((int) bgSprite.getWidth(), (int) bgSprite.getHeight()), lightWindows,getTextureManager(),getVertexBufferObjectManager(),camera,getAssets(),scene);
				
				scene.setOnSceneTouchListener(windowCLickListener);
			}
		});

		
		return scene;
	}


	// ===========================================================
	// Methods
	// ===========================================================

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
}
