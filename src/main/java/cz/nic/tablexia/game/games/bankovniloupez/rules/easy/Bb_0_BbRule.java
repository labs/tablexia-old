/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.rules.easy;

import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.Context;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.bottom.BottomAttribute;

/**
 * 
 * @author Matyáš Latner
 */
public class Bb_0_BbRule extends Bt_0_BtRule {

    public Bb_0_BbRule(Context context, RandomAccess randomAccess, VertexBufferObjectManager vertexBufferObject, int numberOfCreatures, int numberOfThieves) {
        super(context, randomAccess, vertexBufferObject, numberOfCreatures, numberOfThieves);
    }
    
    @Override
    protected void prepareCreatureDescriptionsC() {
    	super.prepareCreatureDescriptionsC();
    	attributeClass = BottomAttribute.class;
    }

}
