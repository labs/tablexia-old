
package cz.nic.tablexia.game.games.strelnice.model;

public interface PositionProvider {
    public float getY(float elapseTime, float maxHeight);

    public static final PositionProvider PROVIDER_FLAT = new PositionProvider() {

        @Override
        public float getY(float elapsedTime, float maxHeight) {
            return 0;
        }
    };
}
