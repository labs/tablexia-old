package cz.nic.tablexia.game.games.potme.map;

import android.util.Log;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.potme.PotmeActivity;
import cz.nic.tablexia.game.games.potme.map.TileMap.TileMapPosition;
import cz.nic.tablexia.game.games.potme.map.mapobject.MapObjectType;
import cz.nic.tablexia.game.games.potme.map.mapobstacle.MapObstacleType;
import cz.nic.tablexia.game.games.potme.map.mapobstacle.MapObstacleType.MapObstaclePosition;
import cz.nic.tablexia.game.games.potme.map.tile.Tile;
import cz.nic.tablexia.game.games.potme.map.tile.TileType;

public class MapTutorial implements IMapProvider {
	
	protected RandomAccess randomAccess;
	
	public MapTutorial(RandomAccess randomAccess) {
		this.randomAccess = randomAccess;
	}

	@Override
	public TileMap prepareMap(Tile lastFinishTile, int mapXSize, int mapYSize, MapObjectType finishMapObject, boolean hasKey) {
		
		Log.d(MapGenerator.class.getCanonicalName(), "Start preparing tutorial map with random seed: " + randomAccess.getRandomSeed());
		TileMap tileMap = new TileMap(mapXSize, mapYSize);
		
		TileMapPosition position1 = PotmeActivity.DEFAULT_MAP_START_POSITION;
		TileMapPosition position2 = new TileMapPosition(position1.getPositionX() - 1, position1.getPositionY());
		TileMapPosition position3 = new TileMapPosition(position1.getPositionX() - 1, position1.getPositionY() - 1);
		tileMap.setMapStartPosition(position1);
		tileMap.setMapFinishPosition(position3);
		
		Tile tile1 = new Tile(TileType.TILE_2IH);
		Tile tile2 = new Tile(TileType.TILE_2LA);
		Tile tile3 = new Tile(TileType.TILE_1C);
		tileMap.addTileAtPosition(position1, tile1);
		tileMap.addTileAtPosition(position2, tile2);
		tileMap.addTileAtPosition(position3, tile3);
		
		tile3.setMapObstacle(MapObstaclePosition.BOTTOM_POSITION, MapObstacleType.DOOR_H);
		tile3.setMapObject(finishMapObject);
		
		// fill all map
		for (int i = 0; i < tileMap.getMapXSize(); i++) {
			for (int j = 0; j < tileMap.getMapYSize(); j++) {
				if (!tileMap.isTileAtPosition(i, j)) {
					Tile tile = new Tile(TileType.TILE_0);
					tileMap.addTileAtPosition(i, j, tile);
				}
			}
		}
		
		return tileMap;
	}
	
	

}