/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.unos.media;

import java.util.Random;

import cz.nic.tablexia.game.games.unos.model.Tile;

/**
 * Factory pro generování náhodných dlaždic
 * 
 * @author lhoracek
 */
public class TileFactory {

    private Tile[] tiles  = new Tile[] { // tile definitions

            new Tile(1, 1, "domek1", 78), //
            new Tile(1, 1, "domek2", 78), //
            new Tile(1, 1, "domek3", 78), //
            new Tile(1, 1, "domek4", 78), //
            new Tile(1, 1, "domek5", 78), //
            new Tile(1, 1, "domek6", 78), //
            new Tile(1, 1, "domek7", 78), //
            new Tile(1, 1, "domek8", 78), //
            new Tile(1, 1, "hriste", 78), //
            new Tile(1, 1, "park", 78), //
            new Tile(1, 1, "posta", 78), //
            new Tile(1, 1, "skola", 78), //
            new Tile(1, 1, "vila1", 78), //
            new Tile(1, 1, "vila2", 78), //
            new Tile(1, 1, "vila3", 78), //
            new Tile(1, 1, "vila4", 78), //
    };

    Random         random = new Random();

    public Tile getRandomTile() {
        return tiles[random.nextInt(tiles.length)];
    }

}
