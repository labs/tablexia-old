/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.bankovniloupez.creature.attribute;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;

import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.content.Context;

import com.activeandroid.util.Log;
import com.google.common.reflect.ClassPath;

import cz.nic.tablexia.R;
import cz.nic.tablexia.game.common.RandomAccess;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureDescriptor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureRoot.AttributeGender;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.ClothingAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.glasses.GlassesAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.hair.HairAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.head.HeadAttribute;

/**
 * Creature attribute class
 * 
 * @author Matyáš Latner
 */
public abstract class Attribute extends Sprite {
	 
	private static final String CREATURE_ATTRIBUTES_PACKAGE = "cz.nic.tablexia.game.games.bankovniloupez.creature.attribute";

	@Target(ElementType.TYPE)
	@Retention(RetentionPolicy.RUNTIME)
	public static @interface CreatureGenericType {
	 
	    public boolean isGeneric() default false;	
	 
	}
	
	@Target(ElementType.TYPE)
	@Retention(RetentionPolicy.RUNTIME)
	public static @interface CreatureSuperGenericType {
	 
	    public boolean isGeneric() default false;	
	 
	}

    private static final String STRING_RESOURCE_PREFIX = "game_bankovniloupez_attribute_";

    /**
     * Attributes color types
     * 
     * @author Matyáš Latner
     */
    public enum AttributeColor {

        RED(R.string.game_bankovniloupez_color_red),
        GREEN(R.string.game_bankovniloupez_color_green),
        BLUE(R.string.game_bankovniloupez_color_blue),
        BROWN(R.string.game_bankovniloupez_color_brown),
        GREY(R.string.game_bankovniloupez_color_grey),
        ORANGE(R.string.game_bankovniloupez_color_orange),
        PURPLE(R.string.game_bankovniloupez_color_purple),
        PINK(R.string.game_bankovniloupez_color_pink),
        YELLOW(R.string.game_bankovniloupez_color_yellow),
        BLACK(R.string.game_bankovniloupez_color_black),
        WHITE(R.string.game_bankovniloupez_color_white);

        private int descriptionResourceId;

        private AttributeColor(int descriptionResourceId) {
            this.descriptionResourceId = descriptionResourceId;
        }

        public int getDescriptionResourceId() {
            return descriptionResourceId;
        }

        public static AttributeColor getRandomColor(Random random) {
            AttributeColor[] values = AttributeColor.values();
            return values[random.nextInt(values.length)];
        }
    }

    private AttributeColor attributeColor = null;

    /**
     * Attributes constructor
     * 
     * @param attributeColor current attributes property
     */
    public Attribute(AttributeColor attributeColor, float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
        this.attributeColor = attributeColor;
    }

    /**
     * Returns attributes generic type
     */
    public Class<? extends Attribute> getGenericType() {
        return Attribute.class;
    }
    
    /**
     * Returns current attribute gender
     * 
     * @return current attribute gender
     */
    public static AttributeGender getAttributeGender() {
		return AttributeGender.ANY;
	}
    
    /**
     * Returns current attribute color
     * 
     * @return current attribute color
     */
    public AttributeColor getAttributeColor() {
        return attributeColor;
    }

    /**
     * Returns attribute description for this attribute
     * 
     * @return attribute description for this attribute
     */
    public AttributeDescription getAttributeDescription() {
        return new AttributeDescription(this);
    }
    /**
     * Returns all available textures for current attribute.
     * Generic types returns all textures from child types.
     * 
     * @return list of attributes textures
     */
    public static List<AttributeDescription> getTextures() {
        return new ArrayList<ResourceManager.AttributeDescription>() {

            private static final long serialVersionUID = 3710521203238454467L;

            {
                addAll(GlassesAttribute.getTextures());
                addAll(HairAttribute.getTextures());
                addAll(HeadAttribute.getTextures());
                addAll(ClothingAttribute.getTextures());
            }
        };
    }

    /**
     * Returns text description from <code>strings.xml</code> for attribute defined by a class in parameter
     * 
     * @param attributeClass class defining attribute
     * @param context current context
     * @return description of attribute
     */
    public static String getTextDescriptionForAttributeClass(Class<? extends Attribute> attributeClass, Context context) {
        return getTextDescriptionForAttributeClass(attributeClass, context, false);
    }

    /**
     * Returns text description from <code>strings.xml</code> for attribute defined by a class in parameter
     * 
     * @param attributeClass class defining attribute
     * @param context current context
     * @param useForcedVersion for <code>true</code> value use simple version of string
     * @return description of attribute
     */
    public static String getTextDescriptionForAttributeClass(Class<? extends Attribute> attributeClass, Context context, boolean useForcedVersion) {
        String key = STRING_RESOURCE_PREFIX + attributeClass.getSimpleName().toLowerCase(Locale.getDefault());
        int resourceId = context.getResources().getIdentifier(key, "string", context.getPackageName());
        if (useForcedVersion) {
            key = key + "_forced";
            int resourceTempId = context.getResources().getIdentifier(key, "string", context.getPackageName());
            if (resourceTempId > 0) {
                resourceId = resourceTempId;
            }
        }
        return context.getString(resourceId);
    }

    /**
     * Returns random attribute from first parameter for specified color and gender. For <code>null</code> color and gender is use random value.
     * 
     * @param globalGender global creature gender, is used if there is no attributeDescriptionForce gender
     * @param attributeDescriptionForce forced attribute
     * @param descriptionsToBan list of banned attributes
     * @param generateNull for <code>true</code> value can be chosen <code>null</code> value as result
     * @param random instance of {@link Random}
     * @param vertexBufferObjectManager andengine vertex buffer
     * @return random attribute
     */
    public static Attribute getRandomAttribute(AttributeGender globalGender,
            AttributeDescription attributeDescriptionForce,
            CreatureDescriptor descriptionsToBan,
            boolean generateNull,
            Random random,
            VertexBufferObjectManager vertexBufferObjectManager) {

        List<AttributeDescription> textures = getTexturesFromAttributeClass(attributeDescriptionForce.getAttributeClass());
        List<AttributeDescription> resultList = textures;
        AttributeColor  color  = attributeDescriptionForce.getAttributeColor();
        AttributeGender gender = attributeDescriptionForce.hasSpecificGender() ? attributeDescriptionForce.getAttributeGender() : globalGender;

        // is set color or gender filter or throw out null values or there is some banned attribute else skip this block and use all textures list
        if ((color != null) || (gender != null) || !generateNull || ((descriptionsToBan != null) && (descriptionsToBan.getDescriptions().size() > 0))) {
            resultList = new ArrayList<AttributeDescription>();
            for (AttributeDescription textureContainer : textures) {

                if ((textureContainer == null) && generateNull) {
                    resultList.add(textureContainer);
                } else if (textureContainer != null) {

                    // check if this color is wanted
                    boolean sameColor = (color != null) && (textureContainer.getAttributeColor() == color); // color is banned and color property of attribute is not same as parameter

                    // check if this gender is wanted
                    boolean sameGender = (gender != null) && ((gender == AttributeGender.ANY) || // any gender parameter (same as null gender parameter)
                            (textureContainer.getAttributeGender() == AttributeGender.ANY) ||    // any gender property of attribute
                            (textureContainer.getAttributeGender() == gender));                  // gender parameter is same as gender property of attribute

                    // check if this attribute is banned
                    boolean isAttributeBan = isAttributeBan(descriptionsToBan, textureContainer);

                    if ((((color == null) && sameGender) || ((gender == null) && sameColor) || (sameColor && sameGender) || ((color == null) && (gender == null) && !generateNull)) && !isAttributeBan) {

                        resultList.add(textureContainer);
                    }

                }
            }
        }

        // randomly select one item from list of attributes
        if (resultList.size() > 0) {
            AttributeDescription textureContainer = resultList.get(random.nextInt(resultList.size()));
            if (textureContainer != null) {
                try {
                    return textureContainer.getAttributeClass().getConstructor(AttributeColor.class,
                                                                               ITextureRegion.class,
                                                                               VertexBufferObjectManager.class).newInstance(textureContainer.getAttributeColor(),
                                                                                                                            textureContainer.getTextureRegion(),
                                                                                                                            vertexBufferObjectManager);
                } catch (Exception e) {
                    Log.e("Cannot create character attribute: " + String.valueOf(textureContainer.getAttributeClass()), e);
                }
            }
        }


        return null;
    }

    /**
     * Check if current attribute is banned in parameter list
     * 
     * @param descriptionsToBan list of banned attributes
     * @param attributeDescription current attribute description
     * @return <code>true</code> if current attribute is not banned
     */
    private static boolean isAttributeBan(CreatureDescriptor descriptionsToBan, AttributeDescription attributeDescription) {
        boolean isAttributeBan = false;
        if (descriptionsToBan != null) {
            for (AttributeDescription attributeDescriptionBan : descriptionsToBan.getDescriptions()) {
                Class<? extends Attribute> attributeDescriptionBanClass = attributeDescriptionBan.getAttributeClass();
                AttributeColor attributeDescriptionBanColor = attributeDescriptionBan.getAttributeColor();

                isAttributeBan = isAttributeBan || (isClassGenericType(attributeDescription.getAttributeClass(), attributeDescriptionBanClass) && ((attributeDescriptionBanColor == null) || (attributeDescriptionBanColor == attributeDescription.getAttributeColor())));
                if (isAttributeBan) {
                    break;
                }
            }
        }
        return isAttributeBan;
    }
    
    /**
     * Call getAttributeGender static method from attribute class in parameter
     * 
     * @param attributeClass attribute class for calling method getGender from
     * @return parameter attribute gender
     */
    public static AttributeGender getGenderFromAttributeClass(Class<? extends Attribute> attributeClass) {
        try {
            return (AttributeGender) attributeClass.getMethod("getAttributeGender").invoke(null);
        } catch (Exception e) {
            Log.e("Cannot get gender from class: " + attributeClass, e);
            return null;
        }
    }

    /**
     * Call getTextures static method from attribute class in parameter
     * 
     * @param attributeClass attribute class for calling method getTextures from
     * @return list of attribute descriptions containing textures
     */
    @SuppressWarnings("unchecked")
    public static List<AttributeDescription> getTexturesFromAttributeClass(Class<? extends Attribute> attributeClass) {
        try {
            return (List<AttributeDescription>) attributeClass.getMethod("getTextures").invoke(null);
        } catch (Exception e) {
            Log.e("Cannot get textures from class: " + attributeClass, e);
            return new ArrayList<AttributeDescription>();
        }
    }

    /**
     * Returns all available colors from {@link Attribute} class
     * 
     * @param attributeClass attribute class for obtain colors
     * @return list of available colors
     */
    public static List<AttributeColor> getAvalibleColorsFromAttributeClass(Class<? extends Attribute> attributeClass) {
        List<AttributeDescription> texturesFromAttributeClass = getTexturesFromAttributeClass(attributeClass);
        List<AttributeColor> attributeColors = new ArrayList<AttributeColor>();
        for (AttributeDescription attributeDescription : texturesFromAttributeClass) {
            attributeColors.add(attributeDescription.getAttributeColor());
        }
        return attributeColors;
    }
    
    /**
     * Returns available gender for attribute color and attribute class parameters
     * 
     * @param attributeColor attribute color for obtain gender
     * @param attributeClass attribute class for obtain gender
     * @return available genders
     */
    public static AttributeGender getAvalibleGenderForColorAndClass(AttributeColor attributeColor, Class<? extends Attribute> attributeClass) {
        List<AttributeDescription> texturesFromAttributeClass = getTexturesFromAttributeClass(attributeClass);
        List<AttributeGender> attributeGenders = new ArrayList<AttributeGender>();
        for (AttributeDescription attributeDescription : texturesFromAttributeClass) {
        	if (attributeDescription != null && attributeDescription.getAttributeColor() == attributeColor) {        		
        		if (attributeDescription.hasSpecificGender() && !attributeGenders.contains(attributeDescription.getAttributeGender())) {        		
        			attributeGenders.add(attributeDescription.getAttributeGender());
        		}
        	}
        }
        if (attributeGenders.size() == AttributeGender.values().length) {        	
        	return AttributeGender.ANY;
        } else if (attributeGenders.size() == 1) {
        	return attributeGenders.get(0);
        }
        return null;
    }
    
    /**
     * Returns all generic attribute classes
     * @return all generic attribute classes
     */
    public static List<Class<?>> getAllGenericAttributesClasses() {
    	List<Class<?>> result = new ArrayList<Class<?>>();
    	
    	final ClassLoader loader = Thread.currentThread().getContextClassLoader();
    	try {
			Set<ClassPath.ClassInfo> infoSet = ClassPath.from(loader).getTopLevelClassesRecursive(CREATURE_ATTRIBUTES_PACKAGE);
			for (ClassPath.ClassInfo info : infoSet) {
				Class<?> classToCheck = Class.forName(info.getName());
				if (classToCheck.isAnnotationPresent(CreatureGenericType.class)) {					
					result.add(classToCheck);
				}
			}
		} catch (Exception e) {
			Log.e("Cannot get generic attribute classes!", e);
		}
    	
    	return result;
	}

    /**
     * Returns random color from available colors in {@link Attribute} class
     * 
     * @param attributeClass attribute class for obtain color
     * @return random available color
     */
    public static AttributeColor getRandomAvalibleColorFromAttributeClass(Class<? extends Attribute> attributeClass, RandomAccess randomAccess) {
        List<AttributeColor> colorsFromAttributeClass = getAvalibleColorsFromAttributeClass(attributeClass);
        return colorsFromAttributeClass.get(randomAccess.getRandom().nextInt(colorsFromAttributeClass.size()));
    }

    /**
     * Returns <code>true</code> if class in attribute1 parameter has class in attribute2 parameter as generic type
     */
    public static boolean isClassGenericType(Class<? extends Attribute> attribute1, Class<? extends Attribute> attribute2) {
        Class<?> attributeToCheck = attribute1;
        while (attributeToCheck != null) {
            if (attributeToCheck.equals(attribute2)) {
                return true;
            }
            if (attributeToCheck == Attribute.class) {
                break;
            }
            attributeToCheck = attributeToCheck.getSuperclass();
        }
        return false;
    }

}

