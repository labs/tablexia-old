/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.potme.action.rule;

import org.andengine.entity.IEntity;
import org.andengine.util.modifier.IModifier;

import cz.nic.tablexia.game.common.EntityModifierListenerAdapter;
import cz.nic.tablexia.game.games.potme.PotmeActivity;
import cz.nic.tablexia.game.games.potme.action.ActionType.IActionFinishedListener;
import cz.nic.tablexia.game.games.potme.action.ActionUtility;
import cz.nic.tablexia.game.games.potme.creature.Player;
import cz.nic.tablexia.game.games.potme.creature.Player.PlayerOrientation;
import cz.nic.tablexia.game.games.potme.map.TileMap;
import cz.nic.tablexia.game.games.potme.map.TileMap.TileMapPosition;
import cz.nic.tablexia.game.games.potme.map.mapobstacle.MapObstacle;

/**
 * Rule for GO action
 * 
 * @author Matyáš Latner
 *
 */
public class GoActionRule extends AbstractActionTypeRule {

	@Override
    public void performActionRule(final PotmeActivity potmeActivity, TileMap tileMap, Player player, int mapStartPositionX, int mapStartPositionY, final IActionFinishedListener finishedListener) {
        PlayerOrientation actualPlayerOrientation = player.getActualPlayerOrientation();
        TileMapPosition actualTileMapPosition = player.getActualTileMapPosition();
        TileMapPosition nextTileMapPosition = player.getNextTileMapPosition();

        MapObstacle obstacleInDirection = ActionUtility.getObstacleInDirection(tileMap, actualTileMapPosition, actualPlayerOrientation);
        
        if (!ActionUtility.checkMoveDirection(tileMap, actualTileMapPosition, nextTileMapPosition)) {
        	crashToTileMapPosition(potmeActivity, true, tileMap, player, mapStartPositionX, mapStartPositionY, actualTileMapPosition, nextTileMapPosition);
        	performOnActionFinishWithDelay(player, finishedListener, false, PotmeActivity.ERROR_ACTION_DELAY);
        } else if (obstacleInDirection != null && obstacleInDirection.isEnabled()){
        	crashToTileMapPosition(potmeActivity, true, tileMap, player, mapStartPositionX, mapStartPositionY, actualTileMapPosition, nextTileMapPosition);
        	playRandomSoundFromGroup(obstacleInDirection.getMapObstacleType().getSoundGroup());
        	obstacleInDirection.reactWithPlayer();
        	performOnActionFinishWithDelay(player, finishedListener, false, PotmeActivity.ERROR_ACTION_DELAY);
        } else {
        	player.walkToTileMapPosition(tileMap, mapStartPositionX, mapStartPositionY, actualTileMapPosition, nextTileMapPosition, new EntityModifierListenerAdapter() {
				@Override
				public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
					performOnActionFinished(finishedListener, true);
				}
			});
        }
    }
	
	private void crashToTileMapPosition(final PotmeActivity potmeActivity, boolean withSound, TileMap tileMap, Player player, int mapStartPositionX, int mapStartPositionY, TileMapPosition actualTileMapPosition, TileMapPosition nextTileMapPosition) {
		player.crashToTileMapPositionHalfWay(tileMap, mapStartPositionX, mapStartPositionY, actualTileMapPosition, nextTileMapPosition, withSound, new EntityModifierListenerAdapter() {
    		
    		@Override
    		public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
    			potmeActivity.showCrashInfo();
    		}
    		
    	});
	}
	
}
