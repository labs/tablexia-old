/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.bankovniloupez;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.microedition.khronos.opengles.GL10;

import org.andengine.audio.music.Music;
import org.andengine.audio.music.MusicFactory;
import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.util.adt.color.Color;

import android.content.Context;
import android.graphics.Typeface;
import android.opengl.GLES10;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.DisplayMetrics;

import com.activeandroid.util.Log;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.bankovniloupez.creature.CreatureRoot.AttributeGender;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute.AttributeColor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute.CreatureGenericType;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute.CreatureSuperGenericType;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.accessories.FBeadsAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.accessories.FScarfAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.accessories.MScarfAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.accessories.MTieAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.bottom.FPantsAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.bottom.FSkirtAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.bottom.MPantsAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.bottom.MShortsAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.headgear.FHatAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.headgear.MHatAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.top.FCoatAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.top.FShirtAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.top.FSweaterAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.top.FVestAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.top.MCoatAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.top.MShirtAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.top.MSweaterAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.top.MVestAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.glasses.FGlassesAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.glasses.MGlassesAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.hair.FBraidhairAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.hair.FLonghairAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.hair.MHairAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.head.FHead1Attribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.head.FHead2Attribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.head.MHead1Attribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.head.MHead2Attribute;
import cz.nic.tablexia.util.andengine.ZipFileBitmapTextureAtlasSource;

/**
 * Resource manager for game "Bankovni loupez"
 * 
 * @author Matyáš Latner
 */
public class ResourceManager {

    private static final String         ASSET_GAME            = "bankovniloupez/";

    private static final String         ASSET_GFX_SOURCE      = ASSET_GAME + "gfx/";
    private static final String         ASSET_CHARACTER       = ASSET_GFX_SOURCE + "character/";
    public static final String          ASSET_GAMESCREEN      = "gamescreen/";
    public static final String          THIEF_ASSET           = ASSET_GAMESCREEN + "thief.png";
    public static final String          ARRESTED_ASSET        = ASSET_GAMESCREEN + "arrested.png";
    public static final String          INNOCENCE_ASSET       = ASSET_GAMESCREEN + "innocence.png";

    public static String                BACKGROUND;
    public static String                BACKGROUND_BOTTOM;
    public static String                BACKGROUND_DOORBACKGROUND;

    private static final String         ASSET_SFX_SOURCE      = ASSET_GAME + "sfx/";
    public static final String          ALARM_SOUND_ASSET     = "alarm.mp3";

    public static final String          STEPS_IN_MALE_SOUND_ASSET_1   = "steps_male_in_1.mp3";
    public static final String          STEPS_IN_MALE_SOUND_ASSET_2   = "steps_male_in_2.mp3";
    public static final String          STEPS_IN_MALE_SOUND_ASSET_3   = "steps_male_in_3.mp3";
    public static final String          STEPS_IN_FEMALE_SOUND_ASSET_1 = "steps_female_in_1.mp3";
    public static final String          STEPS_IN_FEMALE_SOUND_ASSET_2 = "steps_female_in_2.mp3";
    public static final String          STEPS_OUT_MALE_SOUND_ASSET_1   = "steps_male_out_1.mp3";
    public static final String          STEPS_OUT_MALE_SOUND_ASSET_2   = "steps_male_out_2.mp3";
    public static final String          STEPS_OUT_MALE_SOUND_ASSET_3   = "steps_male_out_3.mp3";
    public static final String          STEPS_OUT_FEMALE_SOUND_ASSET_1 = "steps_female_out_1.mp3";
    public static final String          STEPS_OUT_FEMALE_SOUND_ASSET_2 = "steps_female_out_2.mp3";

    public static final String[][]      STEPS_MALE                     = { { STEPS_IN_MALE_SOUND_ASSET_1, STEPS_OUT_MALE_SOUND_ASSET_1 }, { STEPS_IN_MALE_SOUND_ASSET_2, STEPS_OUT_MALE_SOUND_ASSET_2 }, { STEPS_IN_MALE_SOUND_ASSET_3, STEPS_OUT_MALE_SOUND_ASSET_3 } };
    public static final String[][]      STEPS_FEMALE                   = { { STEPS_IN_FEMALE_SOUND_ASSET_1, STEPS_OUT_FEMALE_SOUND_ASSET_1 }, { STEPS_IN_FEMALE_SOUND_ASSET_2, STEPS_OUT_FEMALE_SOUND_ASSET_2 } };
    public static final String          SHACKLES_SOUND_ASSET  = "chains.mp3";
    public static final String          ERROR_SOUND_ASSET     = "error.mp3";

    private static final String         ASSET_MFX_SOURCE      = ASSET_GAME + "mfx/";
    public static final String          AMBIENT_MUSIC_ASSET   = "ambient.mp3";

    private Map<String, ITextureRegion> gameTextures;
    private Map<String, Sound>          gameSounds;
    private Map<String, Music>          gameMusics;
    private Font                        mFont;

    /**
     * Attribute description object for description of specific attribute to force or ban.
     * Is used to store loaded texture for specific attribute.
     * 
     * @author Matyáš Latner
     */
    public static final class AttributeDescription implements Parcelable {

        private AttributeColor             attributeColor;
        private AttributeGender            attributeGender;
        private String                     texturePath;
        private TextureRegion              textureRegion;
        private Class<? extends Attribute> attributeClass;

        public AttributeDescription(AttributeColor attributeColor, AttributeGender attributeGender, String texturePath, Class<? extends Attribute> attributeClass) {
            if ((attributeGender != null) && (attributeClass != null)) {
                AttributeGender attributeClassGender = Attribute.getGenderFromAttributeClass(attributeClass);
                if ((attributeClassGender != null) && (attributeClassGender != AttributeGender.ANY)) {
                    if (attributeGender != attributeClassGender) {
                        throw new IllegalStateException("Incompactible genders are set for attribute description. GENDER: " + attributeGender + " - ATTRIBUTE CLASS: (" + attributeClassGender + ") " + attributeClass.getSimpleName());
                    }
                }
            }

            this.texturePath = texturePath;
            this.attributeClass = attributeClass;
            this.attributeColor = attributeColor;
            this.attributeGender = attributeGender;
        }

        public AttributeDescription(AttributeColor attributeColor, AttributeGender attributeGender, Class<? extends Attribute> attributeClass) {
            this(attributeColor, attributeGender, null, attributeClass);
        }

        public AttributeDescription(Attribute attribute) {
            this(attribute.getAttributeColor(), null, attribute.getClass());
        }

        public String getTexturePath() {
            return texturePath;
        }

        public void setTextureRegion(TextureRegion textureRegion) {
            this.textureRegion = textureRegion;
        }

        public TextureRegion getTextureRegion() {
            return textureRegion;
        }

        public Class<? extends Attribute> getAttributeClass() {
            return attributeClass;
        }

        public AttributeColor getAttributeColor() {
            return attributeColor;
        }

        /**
         * Return attribute gender. If gender is not explicitly set returns gender from attribute class or available gender for color.
         * 
         * @return
         */
        public AttributeGender getAttributeGender() {

            AttributeGender result = attributeGender;
            if (!AttributeGender.isGenderSpecified(result)) {
                result = Attribute.getGenderFromAttributeClass(getAttributeClass());
            }
            if (!AttributeGender.isGenderSpecified(result)) {
                result = Attribute.getAvalibleGenderForColorAndClass(getAttributeColor(), getAttributeClass());
            }
            return result;
        }

        /**
         * Return <code>true</code> if is gender specified
         * 
         * @return <code>true</code> if is gender specified
         */
        public boolean hasSpecificGender() {
            return AttributeGender.isGenderSpecified(getAttributeGender());
        }

        public boolean hasSpecificColor() {
            return getAttributeColor() != null;
        }

        /**
         * Check class from parameter with current attribute class. Returns <code>true</code> if compared classes have same generic type.
         * 
         * @param attributeClassToCheck class to check with current class
         * @return <code>true</code> if class from parameter has same genric type as current class
         */
        public boolean isSameGenericType(Class<? extends Attribute> attributeClassToCheck) {
            Class<?> currentClass = getGenericType();
            Class<?> classToCheck = getGenericType(attributeClassToCheck);

            if ((currentClass == null) || (classToCheck == null)) {
                return false;
            }

            return currentClass.equals(classToCheck);
        }

        /**
         * Returns generic type of current attribute class
         * 
         * @return generic type of current attribute class
         */
        public Class<? extends Attribute> getGenericType() {
            return getGenericType(getAttributeClass());
        }

        /**
         * Returns <code>true</code> if class from parameter is generic
         * 
         * @param attributeClassToCheck checked attribute class
         * @return <code>true</code> if class from parameter is generic
         */
        public static boolean isClassGeneric(Class<? extends Attribute> attributeClassToCheck) {
            if (attributeClassToCheck.isAnnotationPresent(CreatureGenericType.class) && attributeClassToCheck.getAnnotation(CreatureGenericType.class).isGeneric()) {
                return true;
            }
            return false;
        }

        /**
         * Returns <code>true</code> if class from parameter is super generic
         * 
         * @param attributeClassToCheck checked attribute class
         * @return <code>true</code> if class from parameter is super generic
         */
        public static boolean isClassSuperGeneric(Class<? extends Attribute> attributeClassToCheck) {
            if (attributeClassToCheck.isAnnotationPresent(CreatureSuperGenericType.class) && attributeClassToCheck.getAnnotation(CreatureSuperGenericType.class).isGeneric()) {
                return true;
            }
            return false;
        }

        /**
         * Returns generic type for attribute class in parameter
         * 
         * @param classToCheck attribute class to obtain generic type from
         * @return generic type for attribute class in parameter
         */
        @SuppressWarnings("unchecked")
        public static Class<? extends Attribute> getGenericType(Class<? extends Attribute> classToCheck) {
            Class<? extends Attribute> currentClass = classToCheck;
            while ((currentClass != null) && !isClassGeneric(currentClass)) {
                currentClass = (Class<? extends Attribute>) currentClass.getSuperclass();
            }

            return currentClass == null ? null : (Class<? extends Attribute>) currentClass;
        }

        @Override
        public boolean equals(Object o) {
            if (o != null) {
                if (o instanceof AttributeDescription) {
                    AttributeDescription attributeDescriptionToCheck = (AttributeDescription) o;
                    return (attributeDescriptionToCheck.getAttributeClass() == attributeClass) && (attributeDescriptionToCheck.getAttributeColor() == attributeColor) && (attributeDescriptionToCheck.getAttributeGender() == getAttributeGender());
                }
            }
            return false;
        }

        @Override
        public String toString() {
            return getAttributeClass().getSimpleName() + "[G: " + String.valueOf(getAttributeGender()) + " C: " + String.valueOf(attributeColor) + "]";
        }

        public static final Parcelable.Creator<AttributeDescription> CREATOR = new Parcelable.Creator<AttributeDescription>() {

            @Override
            @SuppressWarnings("unchecked")
            public AttributeDescription createFromParcel(Parcel in) {
                Class<? extends Attribute> attributeClass;
                String className = in.readString();
                try {
                    attributeClass = (Class<? extends Attribute>) Class.forName(className);
                    AttributeColor attributeColor = null;
                    int colorNumber = in.readInt();
                    if (colorNumber >= 0) {
                        attributeColor = AttributeColor.values()[colorNumber];
                    }
                    AttributeGender attributeGender = null;
                    int genderNumber = in.readInt();
                    if (genderNumber >= 0) {
                        attributeGender = AttributeGender.values()[genderNumber];
                    }
                    return new AttributeDescription(attributeColor, attributeGender, attributeClass);
                } catch (ClassNotFoundException e) {
                    Log.e("Cannot load parcelable attribute with name: " + className, e);
                }
                return null;
            }

            @Override
            public AttributeDescription[] newArray(int size) {
                return new AttributeDescription[size];
            }

        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(getAttributeClass().getName());
            dest.writeInt(getAttributeColor() != null ? getAttributeColor().ordinal() : -1);
            dest.writeInt(getAttributeGender() != null ? getAttributeGender().ordinal() : -1);
        }

    }

    private enum BackgroundImage {

        NEWSSTAND_S1(GameDifficulty.EASY, 512, 510, "newsstand_s1.png", "newsstand_s1_bottom.png", "door_background_s1.png"), NEWSSTAND_S2(GameDifficulty.EASY, 1024, 800, "newsstand_s2.png", "newsstand_s2_bottom.png", "door_background_s2.png"), NEWSSTAND_S3(GameDifficulty.EASY, 1024, 1023, "newsstand_s3.png", "newsstand_s3_bottom.png", "door_background_s3.png"), NEWSSTAND_S4(GameDifficulty.EASY,
        2048, 1280, "newsstand_s4.png", "newsstand_s4_bottom.png", "door_background_s4.png"), NEWSSTAND_S5(GameDifficulty.EASY, 2048, 1920, "newsstand_s5.png", "newsstand_s5_bottom.png", "door_background_s5.png"),

        JEWELLERY_S1(GameDifficulty.MEDIUM, 512, 510, "jewellery_s1.png", "jewellery_s1_bottom.png", "door_background_s1.png"), JEWELLERY_S2(GameDifficulty.MEDIUM, 1024, 800, "jewellery_s2.png", "jewellery_s2_bottom.png", "door_background_s2.png"), JEWELLERY_S3(GameDifficulty.MEDIUM, 1024, 1023, "jewellery_s3.png", "jewellery_s3_bottom.png", "door_background_s3.png"), JEWELLERY_S4(
        GameDifficulty.MEDIUM, 2048, 1280, "jewellery_s4.png", "jewellery_s4_bottom.png", "door_background_s4.png"), JEWELLERY_S5(GameDifficulty.MEDIUM, 2048, 2048, "jewellery_s5.png", "jewellery_s5_bottom.png", "door_background_s5.png"),

        BANK_S1(GameDifficulty.HARD, 512, 510, "bank_s1.png", "bank_s1_bottom.png", "door_background_s1.png"), BANK_S2(GameDifficulty.HARD, 1024, 800, "bank_s2.png", "bank_s2_bottom.png", "door_background_s2.png"), BANK_S3(GameDifficulty.HARD, 1024, 1023, "bank_s3.png", "bank_s3_bottom.png", "door_background_s3.png"), BANK_S4(GameDifficulty.HARD, 2048, 1080, "bank_s4.png", "bank_s4_bottom.png",
        "door_background_s4.png"), BANK_S5(GameDifficulty.HARD, 2048, 2048, "bank_s5.png", "bank_s5_bottom.png", "door_background_s5.png");

        private GameDifficulty gameDifficulty;
        private int            memoryWidth;
        private int            imageWidth;
        private String         imageName;
        private String         imageBottomName;
        private String         imageDoorBackgroundName;

        private BackgroundImage(GameDifficulty gameDifficulty, int memoryWidth, int imageWidth, String imageName, String imageBottomName, String imageDoorBackgroundName) {
            this.gameDifficulty = gameDifficulty;
            this.memoryWidth = memoryWidth;
            this.imageWidth = imageWidth;
            this.imageName = imageName;
            this.imageBottomName = imageBottomName;
            this.imageDoorBackgroundName = imageDoorBackgroundName;
        }

        public static BackgroundImage getBackgroundForDifficultyAndSize(GameDifficulty gameDifficulty, int imageWidth) {
            BackgroundImage result = null;
            int lastImageHeight = Integer.MAX_VALUE;
            for (BackgroundImage backgroundImage : values()) {
                if ((backgroundImage.gameDifficulty == gameDifficulty) && ((result == null) || (Math.abs(backgroundImage.imageWidth - imageWidth) < lastImageHeight))) {
                    lastImageHeight = Math.abs(backgroundImage.imageWidth - imageWidth);
                    result = backgroundImage;
                }
            }

            return result;
        }

    }

    private static ResourceManager instance;

    private ResourceManager() {
        gameTextures = new HashMap<String, ITextureRegion>();
        gameSounds = new HashMap<String, Sound>();
        gameMusics = new HashMap<String, Music>();
    }

    public static ResourceManager getInstance() {
        if (instance == null) {
            instance = new ResourceManager();
        }
        return instance;
    }

    /**
     * Returns texture object for specific asset name
     * 
     * @param assetName name of asset
     * @return texture for asset name
     */
    public ITextureRegion getTexure(String assetName) {
        return gameTextures.get(assetName);
    }

    public Font getFont() {
        return mFont;
    }

    /**
     * Returns sound object for specific asset name
     * 
     * @param assetName name of sound
     * @return sound for asset name
     */
    public Sound getSound(String assetName) {
        return gameSounds.get(assetName);
    }

    /**
     * Returns music object for specific asset name
     * 
     * @param assetName name of music
     * @return music for asset name
     */
    public Music getMusic(String assetName) {
        return gameMusics.get(assetName);
    }

    /**
     * Loads all textures for creatures
     * 
     * @param engine
     * @param context
     */
    public synchronized void loadTexturesAndSounds(Engine engine, Context context, GameDifficulty gameDifficulty) {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath(ASSET_GFX_SOURCE);

        loadTexturesToMap(FPantsAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(FSkirtAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(MPantsAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(MShortsAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(FHatAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(MHatAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(FShirtAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(FSweaterAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(FVestAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(FCoatAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(MSweaterAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(MVestAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(MCoatAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(MShirtAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(MGlassesAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(FGlassesAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(FBraidhairAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(FLonghairAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(MHairAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(FHead1Attribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(FHead2Attribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(MHead1Attribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(MHead2Attribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(FBeadsAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(FScarfAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(MScarfAttribute.getTextures(), engine, context, 512, 512);
        loadTexturesToMap(MTieAttribute.getTextures(), engine, context, 512, 512);

        loadTexture(engine, context, 512, 512, THIEF_ASSET);
        loadTexture(engine, context, 512, 512, ARRESTED_ASSET);
        loadTexture(engine, context, 512, 512, INNOCENCE_ASSET);

        loadOptimalBackgroundImage(engine, gameDifficulty, context);

        SoundFactory.setAssetBasePath(ASSET_SFX_SOURCE);
        loadSound(engine, context, ALARM_SOUND_ASSET);

        for (String[] steps : STEPS_MALE) {
            for (String step : steps) {
                loadSound(engine, context, step);
            }
        }
        for (String[] steps : STEPS_FEMALE) {
            for (String step : steps) {
                loadSound(engine, context, step);
            }
        }

        loadSound(engine, context, SHACKLES_SOUND_ASSET);
        loadSound(engine, context, ERROR_SOUND_ASSET);

        MusicFactory.setAssetBasePath(ASSET_MFX_SOURCE);
        loadMusic(engine, context, AMBIENT_MUSIC_ASSET);

        mFont = FontFactory.create(engine.getFontManager(), engine.getTextureManager(), 256, 256, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 70, Color.WHITE_ARGB_PACKED_INT);
        mFont.load();
    }

    private void loadOptimalBackgroundImage(Engine engine, GameDifficulty gameDifficulty, Context context) {
        int[] maxTextureSize = new int[1];
        GLES10.glGetIntegerv(GL10.GL_MAX_TEXTURE_SIZE, maxTextureSize, 0);
        int maxMemoryTextureSize = maxTextureSize[0];

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int maxDisplayTextureSize = Math.max(displayMetrics.widthPixels, displayMetrics.heightPixels);

        BackgroundImage backgroundImage;
        if (maxDisplayTextureSize < maxMemoryTextureSize) {
            backgroundImage = BackgroundImage.getBackgroundForDifficultyAndSize(gameDifficulty, displayMetrics.widthPixels);
        } else {
            backgroundImage = BackgroundImage.getBackgroundForDifficultyAndSize(gameDifficulty, maxMemoryTextureSize);
        }

        Log.i("Using background image: " + backgroundImage.imageWidth + " for maximal memory: " + backgroundImage.memoryWidth);

        BACKGROUND = ASSET_GAMESCREEN + backgroundImage.imageName;
        BACKGROUND_BOTTOM = ASSET_GAMESCREEN + backgroundImage.imageBottomName;
        BACKGROUND_DOORBACKGROUND = ASSET_GAMESCREEN + backgroundImage.imageDoorBackgroundName;
        loadTexture(engine, context, backgroundImage.memoryWidth, backgroundImage.memoryWidth, BACKGROUND);
        loadTexture(engine, context, backgroundImage.memoryWidth, backgroundImage.memoryWidth, BACKGROUND_BOTTOM);
        loadTexture(engine, context, backgroundImage.memoryWidth, backgroundImage.memoryWidth, BACKGROUND_DOORBACKGROUND);
    }

    /**
     * Fills texture map
     */
    private void loadTexturesToMap(List<AttributeDescription> textureMap, Engine engine, Context context, int width, int height) {
        Tablexia tablexia = (Tablexia) context.getApplicationContext();
        for (AttributeDescription textureContainer : textureMap) {
            BuildableBitmapTextureAtlas characterAtlas = new BuildableBitmapTextureAtlas(engine.getTextureManager(), width, height, TextureOptions.BILINEAR);
            textureContainer.setTextureRegion(BitmapTextureAtlasTextureRegionFactory.createFromSource(characterAtlas, ZipFileBitmapTextureAtlasSource.create(tablexia.getZipResourceFile(), ASSET_CHARACTER + textureContainer.getTexturePath())));
            try {
                characterAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(1, 1, 1));
            } catch (TextureAtlasBuilderException e) {
                Log.e("Cannot build textures", e);
                throw new RuntimeException("Cannot build textures", e);
            }
            characterAtlas.load();
        }
    }

    /**
     * Loads texture to texture map
     */
    private void loadTexture(Engine engine, Context context, int width, int height, String assetPath) {
        Tablexia tablexia = (Tablexia) context.getApplicationContext();
        BuildableBitmapTextureAtlas characterAtlas = new BuildableBitmapTextureAtlas(engine.getTextureManager(), width, height, TextureOptions.BILINEAR);

        ITextureRegion textureRegion = BitmapTextureAtlasTextureRegionFactory.createFromSource(characterAtlas, ZipFileBitmapTextureAtlasSource.create(tablexia.getZipResourceFile(), ASSET_GFX_SOURCE + assetPath));
        try {
            characterAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(1, 1, 1));
        } catch (TextureAtlasBuilderException e) {
            Log.e("Cannot build textures", e);
        }
        characterAtlas.load();

        gameTextures.put(assetPath, textureRegion);
    }

    /**
     * Loads sound to sound map
     */
    private void loadSound(Engine engine, Context context, String assetPath) {
        Tablexia tablexia = (Tablexia) context.getApplicationContext();
        try {
            gameSounds.put(assetPath, SoundFactory.createSoundFromAssetFileDescriptor(engine.getSoundManager(), tablexia.getZipResourceFile().getAssetFileDescriptor(ASSET_SFX_SOURCE + assetPath)));
        } catch (Exception e) {
            Log.e("Cannot load sound: " + assetPath, e);
        }
    }

    /**
     * Loads music to music map
     */
    private void loadMusic(Engine engine, Context context, String assetPath) {
        Tablexia tablexia = (Tablexia) context.getApplicationContext();
        try {
            gameMusics.put(assetPath, MusicFactory.createMusicFromAssetFileDescriptor(engine.getMusicManager(), tablexia.getZipResourceFile().getAssetFileDescriptor(ASSET_MFX_SOURCE + assetPath)));
        } catch (Exception e) {
            Log.e("Cannot load sound: " + assetPath, e);
        }
    }
}
