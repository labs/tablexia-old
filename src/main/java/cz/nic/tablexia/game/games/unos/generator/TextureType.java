/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.unos.generator;

/**
 * @author lhoracek
 */
public enum TextureType {

    POSITION_CURRENT("zde-jsi", 127, 101), //
    POSITION_LINE_NWSE("lineNWSE-2", 398, 206), //
    POSITION_LINE_NESW("lineNESW-2", 398, 206), //

    POSITION_NEXT_NW("arrow-left-top-unpressed", 158, 153), //
    POSITION_NEXT_NE("arrow-right-top-unpressed", 158, 153), //
    POSITION_NEXT_SW("arrow-left-down-unpressed", 158, 153), //
    POSITION_NEXT_SE("arrow-right-down-unpressed", 158, 153), //

    POSITION_NEXT_NW_PRESSED("arrow-left-top-pressed", 158, 153), //
    POSITION_NEXT_NE_PRESSED("arrow-right-top-pressed", 158, 153), //
    POSITION_NEXT_SW_PRESSED("arrow-left-down-pressed", 158, 153), //
    POSITION_NEXT_SE_PRESSED("arrow-right-down-pressed", 158, 153); //

    private final String resource;

    private final int    x, y;

    private TextureType(String resource, int x, int y) {
        this.resource = resource;
        this.x = x;
        this.y = y;
    }

    public String getResource() {
        return resource;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}
