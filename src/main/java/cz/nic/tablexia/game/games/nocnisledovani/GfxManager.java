/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.nocnisledovani;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.Texture;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.bitmap.BitmapTexture;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.io.in.IInputStreamOpener;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.game.games.nocnisledovani.listener.OnHandDragCompleteListener;
import cz.nic.tablexia.game.games.nocnisledovani.model.MySprite;
import cz.nic.tablexia.game.games.nocnisledovani.model.Trio;
import cz.nic.tablexia.util.andengine.ZipFileTexture;

public class GfxManager {
    private static final String                                                   BACKGROUNDS_DIR                           = NocniSledovaniActivity.BASE_DIR + "gfx/background/";
    private static final String                                                   WINDOWS_DIR                               = NocniSledovaniActivity.BASE_DIR + "gfx/windows/";
    private static final String                                                   WINDOWS_BITMAP_FILE_NAME                  = "windows";
    private static final String                                                   ASSETS_DIR                                = NocniSledovaniActivity.BASE_DIR + "gfx/";
    private static final String                                                   TAG                                       = "GFXMANAGER_STAKEOUT";
    private static final String                                                   SIXTEEN_WINDOWS_FOLDER                    = "16/";
    private static final String                                                   TWENTYFOUR_WINDOWS_FOLDER                 = "24/";
    private static final String                                                   THIRTYTWO_WINDOWS_FOLDER                  = "32/";
    private static final int                                                      NUMBER_OF_FRAMES_FOR_BACKGROUND_ANIMATION = 7;                                                  // pocet pozadi pro
    // animaci ze dne noc
    // resp.
    // noci den
    private static final String                                                   COLOR_BITMAP_NAME                         = "colors";

    private static final int                                                      CLOCK_CENTER_Y                            = 183;
    private static final int                                                      HOUR_HAND_Y_SHIFT                         = 9;                                                  // stred rotace
    // rucicky pro
    // shodu s grafikou
    private static final int                                                      MINUTE_HAND_Y_SHIFT                       = 17;

    private static final String                                                   BACKGROUND_EXTENSION                      = ".png";
    private static final String                                                   ASSETS_EXTENSION                          = ".png";

    private static final String                                                   WATCH_BITMAP_NAME                         = "watch";
    private static final String                                                   HOUR_HAND_NAME                            = "hour_hand_moving";
    private static final String                                                   MINUTE_HAND_NAME                          = "minute_hand_static";

    private static final int                                                      BACKGROUND_LARGER_SIDE_SIZE               = 1920;

    private HashMap<Integer, Pair<Texture, TextureRegion>>                        backgroundTextures;
    private List<Bitmap>                                                          windowsBitmaps;

    private TextureManager                                                        textureManager;

    private int                                                                   imagesScaleValue;                                                                               // value by which all
    // images will be
    // scaled
    // down

    private Bitmap                                                                colorBitmap;
    private Bitmap                                                                lightWindowsBitmap;
    private Bitmap hourHandBitmap;
    private Bitmap correctTimeHourHandBitmap;
    private Bitmap wrongTimeHourHandBitmap;
    private Pair<MyBitmapTexture, TextureRegion> correctTimeHourHandTextures;
    private Pair<MyBitmapTexture,TextureRegion> wrongTimeHourHandTextures;
    private float                                                                 yCoef;

    private Map<Trio<Integer, Integer, Integer>, List<Integer>>                   colorIndexes;
    private int[]                                                                 pixels;

    private Map<Integer, Trio<MyBitmapTexture, MyBitmapTexture, MyBitmapTexture>> windowsMyBitmapTextures;
    private final Tablexia                                                        tablexia;

    /*
     * Creates new GfxManager, sceneNumber: 0->8 windows,1->16 windows,2->24
     * windows,3->32 windows
     */
    public GfxManager(TextureManager textureManager, AssetManager assetManager, Context context, VertexBufferObjectManager vertexBufferObjectManager, Camera camera, Point displaySize) {
        this.textureManager = textureManager;
        float xRatio = displaySize.x / camera.getCameraSceneWidth();
        float widthInOriginalratio = camera.getCameraSceneHeight() * xRatio;
        yCoef = displaySize.y / widthInOriginalratio;

        imagesScaleValue = sampleSizeAccordingToMaxTextureMemorySize(displaySize);
        tablexia = (Tablexia) context.getApplicationContext();
    }

    /*
     * Metoda vrati sprite s hodinkami
     */
    public Sprite getWatchSprite(float watchScale, float pX, float pY, VertexBufferObjectManager vertexBufferObjectManager) {
        String path = ASSETS_DIR + WATCH_BITMAP_NAME + ASSETS_EXTENSION;
        Log.v(TAG, "Loading watch " + path);
        Pair<Texture, TextureRegion> textures = createTextureAndTextureRegionFromPath(path);
        Sprite watchSprite = new Sprite(pX, pY + ((textures.first.getHeight() * watchScale) / 2), textures.first.getWidth() * yCoef * watchScale, (textures.first.getHeight() * watchScale), textures.second, vertexBufferObjectManager);
        return watchSprite;

    }

    public MySprite getHourHandSprite(float imageScale, float pX, float pY, VertexBufferObjectManager vertexBufferObjectManager, final OnHandDragCompleteListener handDragCompleteListener) {
        String path = ASSETS_DIR + HOUR_HAND_NAME + ASSETS_EXTENSION;
        Log.v(TAG, "Loading hour hand " + path);
        Pair<Texture, TextureRegion> textures = createTextureAndTextureRegionFromPath(path);
        MySprite hourHandSprite = new MySprite(pX, (pY + ((textures.first.getHeight() / 2) * imageScale)) - (HOUR_HAND_Y_SHIFT * imageScale), textures.first.getWidth() * yCoef * imageScale, (textures.first.getHeight() * imageScale), textures.second, vertexBufferObjectManager, handDragCompleteListener);

        return hourHandSprite;
    }

    public Bitmap getHourHandBitmap() {
    	return hourHandBitmap;
    }
    
    public MySprite getMinuteHandSprite(float imageScale, float pX, float pY, VertexBufferObjectManager vertexBufferObjectManager, OnHandDragCompleteListener handDragCompleteListener) {
        String path = ASSETS_DIR + MINUTE_HAND_NAME + ASSETS_EXTENSION;
        Log.v(TAG, "Loading minute hand " + path);
        Pair<Texture, TextureRegion> textures = createTextureAndTextureRegionFromPath(path);
        MySprite minuteHandSprite = new MySprite(pX, (pY + ((textures.first.getHeight() / 2) * imageScale)) - (MINUTE_HAND_Y_SHIFT * imageScale), textures.first.getWidth() * yCoef * imageScale, (textures.first.getHeight() * imageScale), textures.second, vertexBufferObjectManager, handDragCompleteListener);
        minuteHandSprite.setRotationCenter(0.5f, 0);
        return minuteHandSprite;
    }

    public float getClockCenterYPosition(float watchScale) {
        return CLOCK_CENTER_Y * watchScale;
    }

    private Bitmap downsampleBitmap(String bitmapSrc, int sampleSize) throws IOException {
        Bitmap resizedBitmap;
        BitmapFactory.Options outBitmap = new BitmapFactory.Options();
        outBitmap.inJustDecodeBounds = false; // the decoder will return a
        // bitmap
        outBitmap.inSampleSize = sampleSize;
        resizedBitmap = BitmapFactory.decodeStream(tablexia.getZipResourceFile().getInputStream(bitmapSrc), null, outBitmap);
        return resizedBitmap;
    }

    public Bitmap getDownsampledColorBitmap(int sceneNumber, int sampleSize) {
        String windowsFolder = getWindowsFolder(sceneNumber);

        String path = WINDOWS_DIR + windowsFolder + COLOR_BITMAP_NAME + ASSETS_EXTENSION;
        Bitmap bmpToReturn = null;
        try {
            bmpToReturn = downsampleBitmap(path, sampleSize);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpToReturn;

    }

    public String getWindowsFolderForDifficulty(int difficulty){
    	switch (difficulty) {
        case 0:
            return SIXTEEN_WINDOWS_FOLDER;
        case 1:
            return TWENTYFOUR_WINDOWS_FOLDER;
        case 2:
            return THIRTYTWO_WINDOWS_FOLDER;
        default:
            return null;
    }
    }

    /*
     * return the value by which will the image be scaled down to fill device
     * requirements on max texture size, rounds background ration up
     */
    private int sampleSizeAccordingToMaxTextureMemorySize(Point displaySize) {
        int largerSide = getLargerDisplaySide(displaySize);
        double ratio = (double) BACKGROUND_LARGER_SIDE_SIZE / (double) largerSide;

        return NocniSledovaniHelper.round(ratio);
    }

    private int getLargerDisplaySide(Point displaySize) {
        if (displaySize.x >= displaySize.y) {
            return displaySize.x;
        }
        return displaySize.y;
    }


    private HashMap<Integer, Pair<Texture, TextureRegion>> createWindowTextures(int sceneNumber, int numberOfWindows) {
        HashMap<Integer, Pair<Texture, TextureRegion>> textures = new HashMap<Integer, Pair<Texture, TextureRegion>>();
        for (int i = 0; i < numberOfWindows; i++) {
            String path = WINDOWS_DIR + getWindowsFolder(sceneNumber) + i + ASSETS_EXTENSION;
            Log.v(TAG, "Loading window texture " + path + " scaled from original by: " + imagesScaleValue);
            textures.put(i, createTextureAndTextureRegionFromPath(path));
        }

        return textures;
    }

    public static interface OnInitFinished {
        public void onFinished();
    }
    

    public boolean allTexturesLoadedOnHW() {
        for (Pair<Texture, TextureRegion> pair : backgroundTextures.values()) {
            if (!pair.first.isLoadedToHardware()) {
                return false;
            }
        }
        for (Integer windowIndex : windowsMyBitmapTextures.keySet()) {
            Trio t = windowsMyBitmapTextures.get(windowIndex);
            if (!((MyBitmapTexture) t.getFirst()).isLoadedToHardware() || !((MyBitmapTexture) t.getSecond()).isLoadedToHardware() || !((MyBitmapTexture) t.getThird()).isLoadedToHardware()) {
                return false;
            }
        }
        if(!correctTimeHourHandTextures.first.isLoadedToHardware()){
        	return false;
        }
        
        if(!wrongTimeHourHandTextures.first.isLoadedToHardware()){
        	return false;
        }
        Log.d(TAG, "All textures loaded on HW");
        return true;
    }

    /*
     * Creates initial BG textures for all scenes
     */
    private HashMap<Integer, Pair<Texture, TextureRegion>> createInitialBGTextures(int numberOfGameScenes) {
        HashMap<Integer, Pair<Texture, TextureRegion>> textures = new HashMap<Integer, Pair<Texture, TextureRegion>>();
        for (int i = 0; i < numberOfGameScenes; i++) {
            String path = BACKGROUNDS_DIR + getWindowsFolder(i) + "0" + BACKGROUND_EXTENSION;
            Log.v(TAG, "Loading background " + path + " scaled from original by: " + imagesScaleValue);
            textures.put(i, createTextureAndTextureRegionFromPath(path));
        }

        return textures;
    }

    /*
     * Returns sprite containing initial backgrounds for all scenes to be
     * switched quickly
     */
    public Sprite getInitBackgroundsSprite(int numberOfGameScenes, float pX, float pY, float pWidth, float pHeight, VertexBufferObjectManager vertexBufferObjectManager) {
        HashMap<Integer, Pair<Texture, TextureRegion>> textures = createInitialBGTextures(numberOfGameScenes);
        Sprite bgSprite = new Sprite(pX, pY, pWidth, pHeight, textures.get(0).second, vertexBufferObjectManager);
        for (Integer key : textures.keySet()) {
            Sprite bg = new Sprite(pX, pY, pWidth, pHeight, textures.get(key).second, vertexBufferObjectManager);
            bg.setTag(key);
            // bg.setVisible(false);
            bg.setZIndex(20 - key);
            bgSprite.attachChild(bg);
        }
        bgSprite.sortChildren();
        return bgSprite;
    }

    public BitmapTexture createBitmapTextureFromBitmap(final Bitmap input) {
        try {
            IInputStreamOpener iiso = new IInputStreamOpener() {

                @Override
                public InputStream open() throws IOException {
                    OutputStream os = new ByteArrayOutputStream();
                    input.compress(CompressFormat.PNG, 0, os);
                    return new ByteArrayInputStream(((ByteArrayOutputStream) os).toByteArray());

                }
            };
            BitmapTexture backGroudnTexture = new BitmapTexture(textureManager, iiso);
            return backGroudnTexture;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "Error loading texture " + input.toString());
            return null;
        }
    }

    /*
     * Method goes to every pixel in bitmap and searches for colors specified in
     * NocniSledovaniHelper then stores the indexes of every color into list
     */
    private void findColors(Bitmap colorBitmap) {
        colorIndexes = new HashMap<Trio<Integer, Integer, Integer>, List<Integer>>();
        Map<Trio<Integer, Integer, Integer>, Integer> colorMap = NocniSledovaniHelper.getColorMap();
        int[] bitmapPixels = new int[colorBitmap.getWidth() * colorBitmap.getHeight()];
        colorBitmap.getPixels(bitmapPixels, 0, colorBitmap.getWidth(), 0, 0, colorBitmap.getWidth(), colorBitmap.getHeight());
        for (int i = 0; i < bitmapPixels.length; i++) {
            Trio<Integer, Integer, Integer> t = new Trio<Integer, Integer, Integer>(Color.red(bitmapPixels[i]), Color.green(bitmapPixels[i]), Color.blue(bitmapPixels[i]));
            if (colorMap.containsKey(t)) {
                if (colorIndexes.containsKey(t)) {
                    colorIndexes.get(t).add(i);
                } else {
                    List<Integer> indexes = new ArrayList<Integer>();
                    indexes.add(i);
                    colorIndexes.put(t, indexes);
                }
            }
        }

    }

    /*
     * Method returns array only containing only pixels with specified color
     */
    public int[] getColorPixels(int color) {
        Arrays.fill(pixels, 0);
        Log.d(TAG, "Started creating color pixel array");
        Trio<Integer, Integer, Integer> t = new Trio<Integer, Integer, Integer>(Color.red(color), Color.green(color), Color.blue(color));
        List<Integer> searchedColorPixelIndexes = colorIndexes.get(t);

        for (int i = 0; i < searchedColorPixelIndexes.size(); i++) {
            pixels[searchedColorPixelIndexes.get(i)] = color;
        }
        Log.d(TAG, "Finished creating color pixel array");
        return pixels;
    }

    /*
     * Method returns array only containing only pixels with colors specified in
     * list
     */
    public int[] getColorPixels(List<Integer> color) {
        Log.d(TAG, "Started creating color pixel array");
        Arrays.fill(pixels, 0);
        for (Integer currentColor : color) {
            Trio<Integer, Integer, Integer> t = new Trio<Integer, Integer, Integer>(Color.red(currentColor), Color.green(currentColor), Color.blue(currentColor));
            List<Integer> searchedColorPixelIndexes = colorIndexes.get(t);

            for (int i = 0; i < searchedColorPixelIndexes.size(); i++) {
                pixels[searchedColorPixelIndexes.get(i)] = currentColor;
            }

        }
        Log.d(TAG, "Finished creating color pixel array");
        return pixels;
    }

    /*
     * Getters and setters
     */
    public Bitmap getLightWindowsBitmap() {
        return lightWindowsBitmap;
    }

    public Bitmap getColorBmp() {
        return colorBitmap;
    }

    public Texture getWindowBitmapTexture(int sceneNumber, int windowIndex) {
        String path = WINDOWS_DIR + getWindowsFolder(sceneNumber) + windowIndex + BACKGROUND_EXTENSION;
        Log.v(TAG, "Loading background " + path + " scaled from original by: " + imagesScaleValue);
        Texture backgroundTexture = null;

        backgroundTexture = new ZipFileTexture(textureManager, tablexia.getZipResourceFile(), path);

        return backgroundTexture;
    }

    private List<Bitmap> createWindowsBitmaps(int sceneNumber, int numberOfWindows) {
        List<Bitmap> listToReturn = new ArrayList<Bitmap>();
        for (int i = 0; i < numberOfWindows; i++) {
            listToReturn.add(getWindowBitmap(i, sceneNumber));
        }
        return listToReturn;
    }


    public Bitmap getWindowBitmap(int windowIndex) {
        return windowsBitmaps.get(windowIndex);
    }

    public MyBitmapTexture getWindowColorBitmapTexture(int windowIndex, int color) {
        Trio<MyBitmapTexture, MyBitmapTexture, MyBitmapTexture> t = windowsMyBitmapTextures.get(windowIndex);
        switch (color) {
            case Color.WHITE:
                return t.getFirst();
            case Color.RED:
                return t.getSecond();
            case Color.GREEN:
                return t.getThird();

            default:
                throw new IllegalArgumentException();
        }
    }

    private Pair<Texture, TextureRegion> createTextureAndTextureRegionFromPath(String path) {
        Texture texture = null;
        TextureRegion textureRegion = null;

        texture = new ZipFileTexture(textureManager, tablexia.getZipResourceFile(), path);
        texture.load();
        textureRegion = TextureRegionFactory.extractFromTexture(texture);

        return new Pair<Texture, TextureRegion>(texture, textureRegion);
    }

    private Bitmap createConfiguredMutableBitmap(String path, Config config) {
        Bitmap bmpToReturn = null;
        try {
            bmpToReturn = BitmapFactory.decodeStream(tablexia.getZipResourceFile().getInputStream(path));
            bmpToReturn = bmpToReturn.copy(config, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bmpToReturn;
    }
    
    
    //NEW METHODS
    public int getWindowsNumber(int difficulty){
		switch (difficulty) {
		case 0:
			return 16;
		case 1:
			return 24;
		case 2:
			return 32;
		default:
			throw new IllegalArgumentException();
		}
	}
    
    public String getWindowsFolder(int difficulty){
    	switch (difficulty) {
		case 0:
			return SIXTEEN_WINDOWS_FOLDER;
		case 1:
			return TWENTYFOUR_WINDOWS_FOLDER;
		case 2:
			return THIRTYTWO_WINDOWS_FOLDER;
		default:
			throw new IllegalArgumentException();
		}
    }
    
    public void initTextures(final int difficulty, final int numberOfBackgrounds, final OnInitFinished onInitFinished) {
    	(new Thread() {
    		
    		@Override
    		public void run() {
    			android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
    			
    			Log.d("TAG", "Started creating textures");
                if (backgroundTextures != null) {
                    backgroundTextures.clear();
                }
                backgroundTextures = createBackgroundTextures(difficulty, numberOfBackgrounds);
                windowsBitmaps = createWindowsBitmaps(difficulty);
                windowsMyBitmapTextures = createWindowsMyBitmapTextures(difficulty);
                lightWindowsBitmap = getWindowsBitmap(difficulty);
                colorBitmap = getColorBitmap(difficulty);
                createHourHandsBitmaps();
                createSolutionHourHandsTextures();
                pixels = new int[colorBitmap.getWidth() * colorBitmap.getHeight()];

                findColors(colorBitmap);

                while (!allTexturesLoadedOnHW()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        Log.e(TAG, "InterruptedException");
                    }
                }
                Log.d("TAG", "Ended creating textures");
                
                if (onInitFinished != null) {
                    onInitFinished.onFinished();
                }
    		}
    		
    	}).start();
    }
    
    // TODO zmensit textury, pokud jsou vetsi nez aktualni displej
    private HashMap<Integer, Pair<Texture, TextureRegion>> createBackgroundTextures(int difficulty, int numberOfBackgrounds) {
        HashMap<Integer, Pair<Texture, TextureRegion>> textures = new HashMap<Integer, Pair<Texture, TextureRegion>>();
        for (int i = 0; i < numberOfBackgrounds; i++) {
            String path = BACKGROUNDS_DIR + getWindowsFolder(difficulty) + i + BACKGROUND_EXTENSION;
            Log.v(TAG, "Loading background " + path + " scaled from original by: " + imagesScaleValue);
            textures.put(i, createTextureAndTextureRegionFromPath(path));
        }

        return textures;
    }
	
    private List<Bitmap> createWindowsBitmaps(int difficulty) {
        List<Bitmap> listToReturn = new ArrayList<Bitmap>();
        int numberOfWindows  = getWindowsNumber(difficulty);
        for (int i = 0; i < numberOfWindows; i++) {
            listToReturn.add(getWindowBitmap(i, difficulty));
        }
        return listToReturn;
    }
    /*
     * Creates bitmap for single window to be lid later on
     */
    
    private Bitmap getWindowBitmap(int windowIndex, int difficulty) {
        String windowsFolder = getWindowsFolderForDifficulty(difficulty);
        String path = WINDOWS_DIR + windowsFolder + windowIndex + ASSETS_EXTENSION;
        Bitmap bmpToReturn = createConfiguredMutableBitmap(path, Bitmap.Config.ARGB_8888);
        return bmpToReturn;
    }
    
    private Map<Integer, Trio<MyBitmapTexture, MyBitmapTexture, MyBitmapTexture>> createWindowsMyBitmapTextures(int difficulty) {
        Log.d(TAG, "Loading windows color textures");
        Map<Integer, Trio<MyBitmapTexture, MyBitmapTexture, MyBitmapTexture>> mapToReturn = new HashMap<Integer, Trio<MyBitmapTexture, MyBitmapTexture, MyBitmapTexture>>();
        int numberOfWindows = getWindowsNumber(difficulty);
        for (int i = 0; i < numberOfWindows; i++) {
            try {
                MyBitmapTexture windowTextureWhite = new MyBitmapTexture(textureManager, Color.WHITE, getWindowBitmap(i), this);
                windowTextureWhite.load();
                MyBitmapTexture windowTextureRed = new MyBitmapTexture(textureManager, Color.RED, getWindowBitmap(i), this);
                windowTextureRed.load();
                MyBitmapTexture windowTextureGreen = new MyBitmapTexture(textureManager, Color.GREEN, getWindowBitmap(i), this);
                windowTextureGreen.load();
                Trio<MyBitmapTexture, MyBitmapTexture, MyBitmapTexture> windowTrio = new Trio<MyBitmapTexture, MyBitmapTexture, MyBitmapTexture>(windowTextureWhite, windowTextureRed, windowTextureGreen);
                mapToReturn.put(i, windowTrio);
            } catch (IOException e) {
                Log.e(TAG, "Error creating window bitmap texture");
            }
        }
        return mapToReturn;
    }
    
    /*
     * Creates bitmap with lid windows for current scene
     */
    private Bitmap getWindowsBitmap(int difficulty) {
        String windowsFolder = getWindowsFolder(difficulty);
        String path = WINDOWS_DIR + windowsFolder + WINDOWS_BITMAP_FILE_NAME + ASSETS_EXTENSION;
        Bitmap bmpToReturn = createConfiguredMutableBitmap(path, Bitmap.Config.ARGB_8888);
        return bmpToReturn;
    }
    
    /*
     * Creates bitmap with color map specifying positions of windows in current
     * scene
     */
    private Bitmap getColorBitmap(int difficulty) {
        String windowsFolder = getWindowsFolder(difficulty);
        String path = WINDOWS_DIR + windowsFolder + COLOR_BITMAP_NAME + ASSETS_EXTENSION;
        Bitmap bmpToReturn = createConfiguredMutableBitmap(path, Bitmap.Config.ARGB_8888);
        return bmpToReturn;
    }
    
    public List<Sprite> getBackgroundAnimationSprites(int numberOfBackgroundsPerScene, float pX, float pY, float pWidth, float pHeight, VertexBufferObjectManager vertexBufferObjectManager) {
        Log.d("TAG", "Started creating BG sprites");
        List<Sprite> backGroundAnimationSprites = new ArrayList<Sprite>();
        for (int i = 0; i < (NUMBER_OF_FRAMES_FOR_BACKGROUND_ANIMATION); i++) {
            Sprite ts = new Sprite(pX, pY, pWidth, pHeight, backgroundTextures.get(i).second, vertexBufferObjectManager);
            backGroundAnimationSprites.add(ts);
        }
        Log.d("TAG", "Ended creating BG sprites");
        return backGroundAnimationSprites;
    }
    private void createClassicHourHandBitmap(){
    	String path = ASSETS_DIR + HOUR_HAND_NAME + ASSETS_EXTENSION;
        hourHandBitmap = createConfiguredMutableBitmap(path, Bitmap.Config.ARGB_8888);
    }
    private void createCorrectTimeHourHandBitmap(){
    	correctTimeHourHandBitmap = Bitmap.createBitmap(getHourHandBitmap());
    }
    private void createWrongTimeHourHandBitmap(){
    	wrongTimeHourHandBitmap = Bitmap.createBitmap(getHourHandBitmap());
    }
    private Bitmap getCorrectTimeHourHandBitmap(){
    	return correctTimeHourHandBitmap;
    }
    private Bitmap getWrongTimeHourHandBitmap(){
    	return wrongTimeHourHandBitmap;
    }
    
    private void createHourHandsBitmaps(){
        createClassicHourHandBitmap();
        createCorrectTimeHourHandBitmap();
        createWrongTimeHourHandBitmap();
    }
    
    private void createSolutionHourHandsTextures(){
    	//SPRAVNE NASTAVENY CAS
    	Bitmap correctTimeHandBitmap = getCorrectTimeHourHandBitmap();
		MyBitmapTexture handTexture = null;
		try {
			 handTexture = new MyBitmapTexture(textureManager, Color.GREEN, correctTimeHandBitmap, this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		handTexture.load();
		TextureRegion colorBitmapTextureRegion = TextureRegionFactory.extractFromTexture(handTexture);
		correctTimeHourHandTextures = new Pair<MyBitmapTexture, TextureRegion>(handTexture, colorBitmapTextureRegion);
	
		//SPATNE NASTAVAENY CAS	
		Bitmap wrongTimeHandBitmap = getWrongTimeHourHandBitmap();
		MyBitmapTexture wrongTimeHandTexture = null;
		try {
			wrongTimeHandTexture = new MyBitmapTexture(textureManager, Color.RED, wrongTimeHandBitmap, this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		wrongTimeHandTexture.load();
		TextureRegion wrongHHColorBitmapTextureRegion = TextureRegionFactory.extractFromTexture(wrongTimeHandTexture);
		wrongTimeHourHandTextures = new Pair<MyBitmapTexture, TextureRegion>(wrongTimeHandTexture, wrongHHColorBitmapTextureRegion);
    }
    public Pair<MyBitmapTexture,TextureRegion> getCorrectTimeHourHandTextures(){
    	return correctTimeHourHandTextures;
    }
    public Pair<MyBitmapTexture,TextureRegion> getWrongTimeHourHandTextures(){
    	return wrongTimeHourHandTextures;
    }
     
}
