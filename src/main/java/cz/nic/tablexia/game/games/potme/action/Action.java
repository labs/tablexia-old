/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.potme.action;

import java.util.ArrayList;
import java.util.List;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;
import org.andengine.util.modifier.IModifier.IModifierListener;
import org.andengine.util.modifier.ease.EaseBackOut;
import org.andengine.util.modifier.ease.EaseLinear;
import org.andengine.util.modifier.ease.EaseQuartOut;

import cz.nic.tablexia.game.games.potme.PotmeActivity;
import cz.nic.tablexia.game.games.potme.ResourceManager;
import cz.nic.tablexia.game.games.potme.UtilityAccess;
import cz.nic.tablexia.game.games.potme.action.widget.ActionsStripWidget;

/**
 * Action icon
 * 
 * @author Matyáš Latner
 *
 */
public class Action extends Sprite {
	
	public 	static final int	NO_COLLISION_NUMBER 			= -1;
	
	private static final float 	DETACH_ANIM_SCALE_DURATION 		= 0.5f;
	private static final float 	DETACH_ANIM_SCALE_FINISH_VALUE 	= 0.1f;
	
	private static final float 	DETACH_ANIM_ALPHA_DURATION 		= 0.5f;
	private static final float 	DETACH_ANIM_ALPHA_START_VALUE 	= 1f;
	private static final float 	DETACH_ANIM_ALPHA_FINISH_VALUE 	= 0f;
	
	private static final float 	ATTACH_ANIM_SCALE_DURATION 		= 0.5f;
	private static final float 	ATTACH_ANIM_SCALE_START_VALUE 	= 0.1f;
	private static final float 	ATTACH_ANIM_SCALE_FINISH_VALUE 	= 1f;
	
	private static final float 	ATTACH_ANIM_ALPHA_DURATION 		= 0.5f;
	private static final float 	ATTACH_ANIM_ALPHA_START_VALUE 	= 0f;
	private static final float 	ATTACH_ANIM_ALPHA_FINISH_VALUE 	= 1f;
	
	private static final float 	RESIZE_ANIM_SCALE_DURATION 		= 0.3f;

	public interface ActionListener {
		void onActionDrag(Action action);
		void onActionDrop(Action action, int collidesWithNumber);
	}
	
	private ActionType actionType;
	private List<ActionListener> actionListeners;
	private int orderNumber;
	
	private static 	boolean enabled			= true;
	private 		boolean active			= false;
	
	protected 	boolean clickable 			= false;
	protected 	boolean draggable 			= false;
	private 	int		collidesWithNumber	= NO_COLLISION_NUMBER;
	
	private Scene 				scene;
	private ActionsStripWidget 	collisionEntity;
	private ScaleModifier 		actualScaleModifier;
	private int 				actualSize;
	private Float 				initialPositionX;
	
	private List<IEntity> 		reactionEntities;

	private IEntityModifier 	positionXMoveModifier;
	
	public Action(ActionType actionType, int orderNumber, int actionSize, float positionX, float positionY, boolean visible, VertexBufferObjectManager vertexBufferObjectManager) {
		super(positionX, positionY, actionSize, actionSize, ResourceManager.getInstance().getTexture(actionType.getTexture()), vertexBufferObjectManager);
		this.actionType  = actionType;
		this.orderNumber = orderNumber;
		this.actualSize  = actionSize;
		
		if (visible) {
			draggable = true;
			setVisible(true);
		} else {
			draggable = false;
			setVisible(false);
		}
		
		initialPositionX = Float.valueOf(0);
		actionListeners  = new ArrayList<ActionListener>();
	}
	
	public int getActualSize() {
		return actualSize;
	}
	
	public ActionType getActionType() {
		return actionType;
	}
	
	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}
	
	public int getOrderNumber() {
		return orderNumber;
	}
	
	public void setCollisionEntity(ActionsStripWidget collisionEntity) {
		this.collisionEntity = collisionEntity;
	}
	
	public void setInitialPositionX(Float initialPositionX) {
		this.initialPositionX = initialPositionX;
	}
	
	public Float getInitialPositionX() {
		return initialPositionX;
	}
	
	public void resetPositionX(boolean animated) {
		movePositionX(initialPositionX, animated);
	}
	
	public void movePositionX(float positionX, boolean animated) {
		if (positionXMoveModifier != null) {
			unregisterEntityModifier(positionXMoveModifier);
			positionXMoveModifier = null;
		}
		if (animated) {			
			positionXMoveModifier = new MoveModifier(PotmeActivity.ANIMATION_MOVE_PERIOD,
					getX(),
					getY(),
					positionX,
					getY(),
					EaseBackOut.getInstance());
			positionXMoveModifier.setAutoUnregisterWhenFinished(true);
			registerEntityModifier(positionXMoveModifier);
		} else {
			setPosition(positionX, getY());
		}
	}
	
	/* //////////////////////////////////////////// ACTION STATE */
	
	public void setEnbaled(boolean enabled) {
		clickable = enabled;
	}
	
	public void disable() {
		setEnbaled(false);
	}
	
	public void enable() {
		setEnbaled(true);
	}
	
	
	/* //////////////////////////////////////////// CLICK LISTENER */
	
	public void addActionListener(ActionListener actionClickListener) {
		actionListeners.add(actionClickListener);
	}
	
	public void removeActionListener(ActionListener actionClickListener) {
		actionListeners.remove(actionClickListener);
	}
	
	public void setClickable(Scene scene) {
		this.scene = scene;
		scene.registerTouchArea(this);
		scene.setTouchAreaBindingOnActionDownEnabled(true); 
		clickable = true;
	}
	
	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
		if (clickable && pSceneTouchEvent.getAction() == TouchEvent.ACTION_DOWN) {
			enabled = false;
			active 	= true;
			return downAction();
		} else if ((enabled || active) && clickable && draggable && pSceneTouchEvent.getAction() == TouchEvent.ACTION_MOVE) {
			
			float positionX = 0;
			float positionY = 0;
			
			IEntity parentEntity = this;
			while (parentEntity.hasParent()) {
				parentEntity = parentEntity.getParent();
				positionX = positionX - parentEntity.getX();
				positionY = positionY - parentEntity.getY();
			}
			
			return dragAction(pSceneTouchEvent.getX() + positionX, pSceneTouchEvent.getY() + positionY);
		} else if (clickable && draggable && pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
			enabled = true;
			active 	= false;
			return upAction();
		}
		
		return false;
	}
	
	protected boolean downAction() {				
		reactionEntities = collisionEntity.getReactionEntities();
		for (ActionListener actionListener : actionListeners) {
			actionListener.onActionDrag(Action.this);
		}
		this.setZIndex(100);
		getParent().sortChildren(true);
		draggable = true;
		return true;
	}
	
	protected boolean dragAction(float dragPositionX, float dragPositionY) {
		setPosition(dragPositionX, dragPositionY);
		if (collisionEntity.isNotFull()) {			
			if (reactionEntities != null) {				
				for (int i = 0; i < reactionEntities.size(); i++) {
					IEntity reactionEntity = reactionEntities.get(i);
					boolean collidesWith = collidesWith(reactionEntity);
					if (collidesWithNumber == NO_COLLISION_NUMBER && collidesWith) {
						collisionEntity.performCollisionWithNumberStart(i);
						collidesWithNumber = i;
						resizeWithAnimation((float)PotmeActivity.ACTION_SIZE_BIGGER);
						return true;
					} else if (collidesWithNumber == i && !collidesWith) {
						collidesWithNumber = NO_COLLISION_NUMBER;
						collisionEntity.performCollisionWithNumberFinish(i);
						resizeWithAnimation((float)PotmeActivity.ACTION_SIZE_SMALLER);
						return true;
					}
				}
			}
		}
		return true;
	}
	
	protected boolean upAction() {
		clickable = false;
		draggable = false;
		
		if (collidesWithNumber != NO_COLLISION_NUMBER) {
			performDropAction();
			detachFromScene();
		} else {
			hideWithAnimation();
		}
		
		collidesWithNumber = NO_COLLISION_NUMBER;
		if (reactionEntities != null) {				
			reactionEntities = null;
		}
		
		return true;
	}
	
	
	/* //////////////////////////////////////////// ANIMATIONS */
	
	public void resizeWithAnimation(float newSize) {
		resizeWithAnimation(getScaleX(), newSize / actualSize);
	}
	
	public void resizeWithAnimation(float fromScale, float toScale) {
		if (actualScaleModifier != null) {			
			unregisterEntityModifier(actualScaleModifier);
		}
		actualScaleModifier = new ScaleModifier(RESIZE_ANIM_SCALE_DURATION, fromScale, toScale, EaseBackOut.getInstance());
		actualScaleModifier.setAutoUnregisterWhenFinished(true);
		this.registerEntityModifier(actualScaleModifier);
	}
	
	public void showWithAnimation() {
		final ScaleModifier scaleModifier = new ScaleModifier(ATTACH_ANIM_SCALE_DURATION, ATTACH_ANIM_SCALE_START_VALUE, ATTACH_ANIM_SCALE_FINISH_VALUE, EaseBackOut.getInstance());
		final AlphaModifier alphaModifier = new AlphaModifier(ATTACH_ANIM_ALPHA_DURATION, ATTACH_ANIM_ALPHA_START_VALUE, ATTACH_ANIM_ALPHA_FINISH_VALUE, EaseLinear.getInstance());
		alphaModifier.addModifierListener(new IModifierListener<IEntity>() {
			
			@Override
			public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
				Action.this.setAlpha(ATTACH_ANIM_ALPHA_START_VALUE);
				Action.this.setVisible(true);
			}
			
			@Override
			public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
				Action.this.registerUpdateHandler(new IUpdateHandler() {
					
					@Override
					public void reset() {
						// nothing needed
					}
					
					@Override
					public void onUpdate(float pSecondsElapsed) {
						Action.this.unregisterUpdateHandler(this);
						Action.this.unregisterEntityModifier(scaleModifier);
						Action.this.unregisterEntityModifier(alphaModifier);
					}
				});
			}
		});
		this.registerEntityModifier(alphaModifier);
		this.registerEntityModifier(scaleModifier);
	}
	
	private void hideWithAnimation() {
		final ScaleModifier scaleModifier = new ScaleModifier(DETACH_ANIM_SCALE_DURATION, getScaleX(), DETACH_ANIM_SCALE_FINISH_VALUE, EaseQuartOut.getInstance());
		final AlphaModifier alphaModifier = new AlphaModifier(DETACH_ANIM_ALPHA_DURATION, DETACH_ANIM_ALPHA_START_VALUE, DETACH_ANIM_ALPHA_FINISH_VALUE, EaseLinear.getInstance());
		scaleModifier.setAutoUnregisterWhenFinished(true);
		alphaModifier.setAutoUnregisterWhenFinished(true);
		alphaModifier.addModifierListener(new IModifierListener<IEntity>() {
			
			@Override
			public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
				performDropAction();
			}
			
			@Override
			public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
				detachFromScene();
			}
		});
		this.registerEntityModifier(alphaModifier);
		this.registerEntityModifier(scaleModifier);
	}
	
	
	/* //////////////////////////////////////////// UTILITY */
	
	private void detachFromScene() {
		if (scene != null) {							
			scene.unregisterTouchArea(Action.this);
		}
		UtilityAccess.getInstance().detachAndAttachEntity(this, null, null, false);
	}
	
	protected void onDetachFomScene() {
		detachSelf();
	}
	
	
	private void performDropAction() {
		for (ActionListener actionListener : actionListeners) {
			actionListener.onActionDrop(Action.this, collidesWithNumber);
		}
	}
}
