/**
 * 
 */

package cz.nic.tablexia.game.games.strelnice.model;

import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

/**
 * @author lhoracek
 */
public class WatchHand extends Sprite {

    public WatchHand(float pX, float pY, float pWidth, float pHeight, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pWidth, pHeight, pTextureRegion, pVertexBufferObjectManager);

    }

    private boolean blinking = false;
    private IEntityModifier entityModifier = new LoopEntityModifier(new SequenceEntityModifier(new AlphaModifier(0.15f, 1, 0), new AlphaModifier(0.15f, 0, 1)));

    public boolean isBlinking() {
        return blinking;
    }

    public void startBlinking() {
        if (!blinking) {
            blinking = true;
            registerEntityModifier(entityModifier);
        }
    }

    public void stopBlinking() {
        if (blinking) {
            blinking = false;
            unregisterEntityModifier(entityModifier);
            setAlpha(1f);
        }
    }
}
