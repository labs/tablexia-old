package cz.nic.tablexia.game.games.nocnisledovani.listener;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.util.modifier.IModifier;

public class MyDelayedImageSwitchModifierListener implements IEntityModifierListener {
	private static final String TAG = MyDelayedImageSwitchModifierListener.class.getSimpleName();
	private Sprite sprite;
	private Scene scene;

	public MyDelayedImageSwitchModifierListener(Scene scene, Sprite sprite) {
		this.scene = scene;
		this.sprite = sprite;
	}

	@Override
	public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {

	}

	@Override
	public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
		scene.setBackground(new SpriteBackground(sprite));
	}

}
