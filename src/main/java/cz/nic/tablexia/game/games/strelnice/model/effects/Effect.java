
package cz.nic.tablexia.game.games.strelnice.model.effects;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;
import org.andengine.util.modifier.IModifier.IModifierListener;

import cz.nic.tablexia.game.games.strelnice.model.Wave;

public class Effect extends Sprite {

    private static final float EFFECT_DURATION = 1f;

    public Effect(Wave wave, float pX, float pY, float pWidth, float pHeight, ITextureRegion pTextureRegion, VertexBufferObjectManager vertexBufferObjectManager) {
        super(pX, pY, pWidth, pHeight, pTextureRegion, vertexBufferObjectManager);
        setZIndex(wave.getzIndex());
    }

    @Override
    public void onAttached() {
        super.onAttached();
        registerEntityModifier(entityModifier);
        entityModifier.addModifierListener(new IModifierListener<IEntity>() {

            @Override
            public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
            }

            @Override
            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                if (effectOverListener != null) {
                    effectOverListener.onEffectOver(Effect.this);
                }
            }
        });
    }

    private IEntityModifier      entityModifier = new ParallelEntityModifier(new AlphaModifier(EFFECT_DURATION, 1f, 0f), new ScaleModifier(EFFECT_DURATION, 1f, 0.4f));

    private OnEffectOverListener effectOverListener;

    public void setOnEffectoverListener(OnEffectOverListener effectOverListener) {
        this.effectOverListener = effectOverListener;
    }

    public static interface OnEffectOverListener {
        public void onEffectOver(Effect effect);
    }
}
