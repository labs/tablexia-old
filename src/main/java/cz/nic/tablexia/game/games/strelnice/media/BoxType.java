/**
 * 
 */

package cz.nic.tablexia.game.games.strelnice.media;

/**
 * @author lhoracek
 */
public enum BoxType {

    TIME_ADD(TextureType.BOX_BAD), //
    TIME_SUB(TextureType.BOX_GOOD), //
    SLOW_DOWN(TextureType.BOX_GOOD), //
    SPEED_UP(TextureType.BOX_BAD), //
    HIT(TextureType.BOX_GOOD), //
    CLOUD(TextureType.BOX_BAD);

    public static final BoxType[] GOOD_BOXES = new BoxType[] { HIT, TIME_SUB, SLOW_DOWN };
    public static final BoxType[] BAD_BOXES  = new BoxType[] { TIME_ADD, SPEED_UP, CLOUD };

    private final TextureType     textureType;

    private BoxType(TextureType textureType) {
        this.textureType = textureType;
    }

    public TextureType getTextureType() {
        return textureType;
    }

}
