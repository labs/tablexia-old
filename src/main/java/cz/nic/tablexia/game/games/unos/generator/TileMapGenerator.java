/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.unos.generator;

import java.util.Map;
import java.util.TreeMap;

import cz.nic.tablexia.game.games.unos.media.TileFactory;
import cz.nic.tablexia.game.games.unos.model.Position;
import cz.nic.tablexia.game.games.unos.model.Tile;

/**
 * @author lhoracek
 */
public class TileMapGenerator {

    TileFactory                 tileFactory = new TileFactory();

    public Map<Position, Tile> generate(int width, int height) {
        // Log.v(TAG, "Generating map " + width + "x" + height);
        boolean[][] freePositions = new boolean[width][height];

        // oznacime vsechna mista za prazdna
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                freePositions[x][y] = true;
                // Log.v(TAG, "Marking position free " + new Position(x, y));
            }
        }

        // objekty budou razene v mape podle pozice - po radcich
        Map<Position, Tile> tiles = new TreeMap<Position, Tile>();
        // zacnemem generovat dlazdice
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {

                Position position = new Position(x, y);
                // pokud mame dlazdici prazdnou
                if (freePositions[x][y]) {
                    Tile tile = tileFactory.getRandomTile();
                    // zkontrolovat, jestli se tile vejde
                    while (!fits(position, tile, freePositions)) {
                        // Log.v(TAG, "Does not fit " + position + " " + tile);
                        tile = tileFactory.getRandomTile();
                    }

                    // oznacit pozice za obsazene podle velikosti dlazdice
                    markOccupiend(position, tile, freePositions);

                    // pridat dlazdici do vysledne mapy na danou pozici
                    tiles.put(position, tile);
                    // Log.d(TAG, "Placed tile " + position + " " + tile);
                }
            }
        }

        return tiles;
    }

    /**
     * Zkontroluje, ze dana dlazdice se vejde do seznamu pozic pri sve velikosti
     * 
     * @param position
     * @param tile
     * @param freePositions
     * @return
     */
    private boolean fits(Position position, Tile tile, boolean[][] freePositions) {
        Position swPosition = position;
        for (int sw = 0; sw < tile.getSWSize(); sw++) {
            Position sePosition = swPosition;
            for (int se = 0; se < tile.getSESize(); se++) {
                // check AIOOB
                if ((sePosition.getX() >= freePositions.length) || (sePosition.getX() < 0) || (sePosition.getY() >= freePositions[0].length) || (sePosition.getY() < 0)) {
                    return false;
                }
                if (!freePositions[sePosition.getX()][sePosition.getY()]) {
                    return false;
                }
                sePosition = sePosition.getSouthEast();
            }
            swPosition = swPosition.getSouthWest();
        }
        return true;
    }

    /**
     * Oznaci prislusne pole za obsazene, aby nebylo mozne tam stavet dalsi dlazdice
     * 
     * @param position
     * @param tile
     * @param freePositions
     * @return
     */
    private void markOccupiend(Position position, Tile tile, boolean[][] freePositions) {
        Position swPosition = position;
        for (int sw = 0; sw < tile.getSWSize(); sw++) {
            Position sePosition = swPosition;
            for (int se = 0; se < tile.getSESize(); se++) {
                freePositions[sePosition.getX()][sePosition.getY()] = false;
                // Log.d(TAG, "Marked occupied " + sePosition + " by " + tile);
                sePosition = sePosition.getSouthEast();
            }
            swPosition = swPosition.getSouthWest();
        }
    }
}
