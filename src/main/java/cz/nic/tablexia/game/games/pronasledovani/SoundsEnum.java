/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.games.pronasledovani;


public enum SoundsEnum {
    SOUND_LIFTCARD("lift", 0.4f), SOUND_ROTATECARD("rotate", 0.4f), SOUND_SWITCHCARDS("switch", 0.4f);

    private SoundsEnum(String resourceName, float volume) {
        this.resourceName = resourceName;
        this.volume = volume;
    }

    private final String resourceName;
    private float        volume;

    public String getResourceName() {
        return resourceName;
    }

    public float getVolume() {
        return volume;
    }
}
