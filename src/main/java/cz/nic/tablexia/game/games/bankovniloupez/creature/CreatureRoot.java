/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.bankovniloupez.creature;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.andengine.audio.sound.Sound;
import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.primitive.vbo.IRectangleVertexBufferObject;
import org.andengine.entity.scene.Scene;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.DrawType;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;
import org.andengine.util.modifier.IModifier;
import org.andengine.util.modifier.IModifier.IModifierListener;
import org.andengine.util.modifier.ease.EaseLinear;
import org.andengine.util.modifier.ease.EaseQuartIn;
import org.andengine.util.modifier.ease.EaseQuartOut;

import cz.nic.tablexia.game.games.bankovniloupez.BankovniloupezActivity;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager;
import cz.nic.tablexia.game.games.bankovniloupez.ResourceManager.AttributeDescription;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.Attribute.AttributeColor;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.accessories.AccessoriesAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.bottom.BottomAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.headgear.HeadgearAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.clothing.top.TopAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.glasses.GlassesAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.hair.HairAttribute;
import cz.nic.tablexia.game.games.bankovniloupez.creature.attribute.head.HeadAttribute;

/**
 * Root of creature, defines creature gender
 * 
 * @author Matyáš Latner
 */
public class CreatureRoot extends Entity {

    public static final int CREATURE_GROUP_NUMBER_NOGROUP = -1;

    /**
     * Creatures gender types
     */
    public enum AttributeGender {
        MALE, FEMALE, ANY;

        /**
         * Returns inversive gender for gender in parameter
         */
        public static AttributeGender getInverseveGender(AttributeGender gender) {
            switch (gender) {
                case MALE:
                    return AttributeGender.FEMALE;

                case FEMALE:
                    return AttributeGender.MALE;

                default:
                    return AttributeGender.ANY;
            }

        }

        /**
         * Returns <code>true</code> if gender attribute from parameter is specified
         * 
         * @param attributeGender gender attribute to check
         * @return <code>true</code> if gender attribute from parameter is specified
         */
        public static boolean isGenderSpecified(AttributeGender attributeGender) {
            return (attributeGender != null) && (attributeGender != ANY);
        }

        /**
         * Returns random gender
         */
        public static AttributeGender getRandomGender(Random random) {
            return random.nextBoolean() ? AttributeGender.MALE : AttributeGender.FEMALE;
        }
    }

    /**
     * Clickable area for creature
     * 
     * @author Matyáš Latner
     */
    private class ClickableArea extends Rectangle {

        public ClickableArea(float pX, float pY, float pWidth, float pHeight, IRectangleVertexBufferObject pRectangleVertexBufferObject) {
            super(pX, pY, pWidth, pHeight, pRectangleVertexBufferObject);
        }

        public ClickableArea(float pX, float pY, float pWidth, float pHeight, VertexBufferObjectManager pVertexBufferObjectManager) {
            super(pX, pY, pWidth, pHeight, pVertexBufferObjectManager);
        }

        public ClickableArea(float pX, float pY, float pWidth, float pHeight, VertexBufferObjectManager pVertexBufferObjectManager, DrawType pDrawType) {
            super(pX, pY, pWidth, pHeight, pVertexBufferObjectManager, pDrawType);
        }

        @Override
        public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
            if (isClickable) {
                isClickable = false;
                if (isThief()) {
                    isRevealed = true;
                    bankovniloupezActivity.showShackles();
                    bankovniloupezActivity.getGameManager().setExtraInt1AndSave(bankovniloupezActivity.getGameManager().getExtraInt1() + 1);
                } else {
                    bankovniloupezActivity.showError();
                    bankovniloupezActivity.getGameManager().setExtraInt3AndSave(bankovniloupezActivity.getGameManager().getExtraInt3() + 1);
                }
                return true;
            }
            return false;
        }

    }

    private boolean                isClickable = false;
    private boolean                isRevealed  = false;
    private boolean                isThief     = false;
    private int                    groupNumber = CREATURE_GROUP_NUMBER_NOGROUP;

    protected List<Attribute>      attributes;
    private AttributeGender        attributeGender;
    private BankovniloupezActivity bankovniloupezActivity;

    private Random                 random      = new Random();

    public CreatureRoot(AttributeGender attributeGender, float pX, float pY) {
        super(pX, pY);
        this.attributeGender = attributeGender;
        attributes = new ArrayList<Attribute>();
    }

    /**
     * Generates random creature attributes for gender specified in constructor.
     * Already sets attributes are not generated again.
     * 
     * @param descriptionToBan list of banned attributes
     * @param vertexBufferObject andengine vertex buffer
     * @param random instance of {@link Random}
     * @return instance of creature root
     */
    public CreatureRoot generateCreature(CreatureDescriptor descriptionToBan, VertexBufferObjectManager vertexBufferObject, Random random) {
        generateAttribute(new AttributeDescription(null, attributeGender, null, BottomAttribute.class), descriptionToBan, true, vertexBufferObject, random);
        generateAttribute(new AttributeDescription(null, attributeGender, null, TopAttribute.class), descriptionToBan, true, vertexBufferObject, random);
        generateAttribute(new AttributeDescription(null, attributeGender, null, HeadAttribute.class), descriptionToBan, true, vertexBufferObject, random);
        generateAttribute(new AttributeDescription(null, attributeGender, null, HairAttribute.class), descriptionToBan, true, vertexBufferObject, random);
        generateAttribute(new AttributeDescription(null, attributeGender, null, HeadgearAttribute.class), descriptionToBan, true, vertexBufferObject, random);
        generateAttribute(new AttributeDescription(null, attributeGender, null, GlassesAttribute.class), descriptionToBan, true, vertexBufferObject, random);
        generateAttribute(new AttributeDescription(null, attributeGender, null, AccessoriesAttribute.class), descriptionToBan, true, vertexBufferObject, random);

        sortChildren();
        return this;
    }

    /**
     * Returns <code>true</code> if creature is thief.
     * 
     * @return <code>true</code> if creature is thief.
     */
    public boolean isThief() {
        return isThief;
    }

    /**
     * Returns number of creature group. <code>-1</code> when creature is not in group <code>0</code> when creature is special and is not in group
     * 
     * @return <code>true</code> number of thief group.
     */
    public int getGroupNumber() {
        return groupNumber;
    }

    /**
     * Returns <code>true</code> when current creature is thief and is revealed
     * 
     * @return <code>true</code> when current creature is thief and is revealed
     */
    public boolean isRevealed() {
        return isRevealed;
    }

    /**
     * Set creature as thief.
     */
    public void setAsThief(boolean isThief) {
        this.isThief = isThief;
    }

    /**
     * Set number of creature group. <code>-1</code> when creature is not in group <code>0</code> when creature is special and is not in group
     * 
     * @param groupNumber number of creature group or <code>0</code> or <code>-1</code>
     */
    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    /**
     * Generate new creature attribute. If creature attribute already exists do nothing.
     * 
     * @param attributeDescriptionForce forced attribute
     * @param descriptionToBan list of banned attributes
     * @param generateNull <code>true</code> for generating empty attributes
     * @param vertexBufferObject andengine vertex buffer
     * @param random instance of {@link Random}
     */
    public boolean generateAttribute(AttributeDescription attributeDescriptionForce, CreatureDescriptor descriptionToBan, boolean generateNull, VertexBufferObjectManager vertexBufferObject, Random random) {
        Attribute attribute = Attribute.getRandomAttribute(getAttributeGender(), attributeDescriptionForce, descriptionToBan, generateNull, random, vertexBufferObject);
        if ((attribute != null) && !hasAttributeGenericType(attribute.getGenericType())) {
            attachAttribute(attribute);
            return true;
        }
        return false;
    }

    /**
     * Checks if attribute with generic type is already attached
     */
    private boolean hasAttributeGenericType(Class<? extends Attribute> genericAttributeType) {
        for (Attribute attribute : attributes) {
            if (attribute.getGenericType().equals(genericAttributeType)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Attaches attribute and sprite
     */
    public void attachAttribute(Attribute attributCreature) throws IllegalStateException {
        if (attributCreature != null) {
            attributes.add(attributCreature);
            super.attachChild(attributCreature);
        }
    }

    /**
     * Detaches attribute and sprite
     */
    public boolean detachAttribute(Attribute attributCreature) {
        if (attributCreature != null) {
            attributes.remove(attributCreature);
            super.detachChild(attributCreature);
        }
        return false;
    }

    /**
     * Check if current creature has all attributes from parameter
     */
    public boolean hasAttributes(CreatureDescriptor creatureDescriptor) {
        for (AttributeDescription attributeDescription : creatureDescriptor.getDescriptions()) {
            if (!hasAttribute(attributeDescription)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check if current creature has attribute from parameter
     */
    public boolean hasAttribute(AttributeDescription attributeToCheck) {
        for (Attribute attribute : attributes) {
            if (attributeToCheck.getAttributeClass().equals(attribute.getClass()) || Attribute.isClassGenericType(attribute.getClass(), attributeToCheck.getAttributeClass())) {

                if ((attributeToCheck.getAttributeColor() == null) || attributeToCheck.getAttributeColor().equals(attribute.getAttributeColor())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns <code>true</code> if creature has any attribute with specific color
     */
    public boolean hasColor(AttributeColor color) {
        if (color != null) {
            for (Attribute attribute : attributes) {
                if (color.equals(attribute.getAttributeColor())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns current attributes property
     */
    public AttributeGender getAttributeGender() {
        return attributeGender;
    }

    /**
     * Returns attribute for specific attribute type in parameter if exists
     * 
     * @return attribute for specific type else <code>null</code>
     */
    public Attribute getAttributeForType(Class<? extends Attribute> attributeTypeClass) {
        for (Attribute attribute : attributes) {
            if (attribute.getAttributeDescription().getAttributeClass().equals(attributeTypeClass) || (AttributeDescription.isClassGeneric(attributeTypeClass) && AttributeDescription.getGenericType(attribute.getAttributeDescription().getAttributeClass()).equals(attributeTypeClass))) {
                return attribute;
            }
        }
        return null;
    }

    /**
     * Returns current creatures description
     * 
     * @return description object for current creature
     */
    public CreatureDescriptor getCreatureDescrition() {
        CreatureDescriptor creatureDescriptor = new CreatureDescriptor();
        for (Attribute attribute : attributes) {
            creatureDescriptor.addDescription(new AttributeDescription(attribute));
        }
        creatureDescriptor.setThief(isThief);
        return creatureDescriptor;
    }

    /**
     * Move creature from specific position to specific position
     * 
     * @param duration duration of move
     * @param fromX from x
     * @param fromY from y
     * @param toX to x
     * @param toY to y
     * @param fromScale start and finish scale
     * @param toScale middle state scale
     * @param firstAnimListener listener for middle state
     * @param secoundAnimListener listener for finish state
     */
    public void enableMove(final float duration, final float fromX, final float fromY, final float middleX, final float middleY, final float toX, final float toY, final float fromScale, final float toScale, final float finalScale, final IModifierListener<IEntity> firstAnimListener, final IModifierListener<IEntity> secoundAnimListener, final IModifierListener<IEntity> thirdAnimListener) {

        ScaleModifier scaleModifierIn = new ScaleModifier(duration, fromScale, toScale, EaseQuartOut.getInstance());
        MoveModifier animationModifierIn = new MoveModifier(duration, fromX, fromY, middleX, middleY, EaseQuartOut.getInstance());
        animationModifierIn.addModifierListener(firstAnimListener);
        animationModifierIn.addModifierListener(new IModifierListener<IEntity>() {

            @Override
            public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                Sound sound = ResourceManager.getInstance().getSound(getCreatureDescrition().getGender().equals(AttributeGender.FEMALE) ? ResourceManager.STEPS_FEMALE[random.nextInt(ResourceManager.STEPS_FEMALE.length)][0] : ResourceManager.STEPS_MALE[random.nextInt(ResourceManager.STEPS_MALE.length)][0]);
                sound.setRate((float) Math.max(0.5f, Math.min(2.0, (1.6f / duration)))); // hodnota mela být délka zvuku, ale musel jsem ji snížiy, aby zvuk nebyl příliš zrychlený
                sound.play();
            }

            @Override
            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                final ScaleModifier scaleModifierOut = new ScaleModifier(duration, toScale, fromScale, EaseQuartIn.getInstance());
                MoveModifier animationModifierOut = new MoveModifier(duration / 2, middleX, middleY, toX, toY, EaseQuartIn.getInstance());
                if (secoundAnimListener != null) {
                    animationModifierOut.addModifierListener(secoundAnimListener);
                }
                animationModifierOut.addModifierListener(new IModifierListener<IEntity>() {

                    @Override
                    public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                        Sound sound = ResourceManager.getInstance().getSound(getCreatureDescrition().getGender().equals(AttributeGender.FEMALE) ? ResourceManager.STEPS_FEMALE[random.nextInt(ResourceManager.STEPS_FEMALE.length)][1] : ResourceManager.STEPS_MALE[random.nextInt(ResourceManager.STEPS_MALE.length)][1]);
                        sound.setRate((float) Math.max(0.5f, Math.min(2.0, (2.0 / duration))));
                        sound.play();
                    }

                    @Override
                    public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                        unregisterEntityModifier(scaleModifierOut);

                        isClickable = false;
                        CreatureRoot.this.setScale(finalScale);
                        MoveModifier animationModifierFinish = new MoveModifier(duration / 4, toX, toY, toX / 4, toY, EaseLinear.getInstance());
                        animationModifierFinish.addModifierListener(thirdAnimListener);
                        registerEntityModifier(animationModifierFinish);
                    }
                });

                registerEntityModifier(animationModifierOut);
                registerEntityModifier(scaleModifierOut);
            }
        });
        registerEntityModifier(animationModifierIn);
        registerEntityModifier(scaleModifierIn);
    }

    /**
     * Enable clicking on this creature
     * 
     * @param scene current game scene
     * @param vertexBufferObjectManager current andengine vertex buffer object
     */
    public void setClickable(Scene scene, BankovniloupezActivity bankovniloupezActivity, VertexBufferObjectManager vertexBufferObjectManager) {
        this.bankovniloupezActivity = bankovniloupezActivity;
        if (!isClickable) {
            isClickable = true;
            ClickableArea clickableArea = new ClickableArea(0, 0, 200, 500, vertexBufferObjectManager);
            clickableArea.setColor(Color.TRANSPARENT);
            attachChild(clickableArea);
            scene.registerTouchArea(clickableArea);
        }
    }

    @Override
    public String toString() {
        return getCreatureDescrition().toString();
    }

}
