/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game.games.nocnisledovani;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.util.modifier.IModifier;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;

import cz.nic.tablexia.R;
import cz.nic.tablexia.audio.resources.SpeechSounds;
import cz.nic.tablexia.game.GameActivity;
import cz.nic.tablexia.game.games.GamesDefinition;
import cz.nic.tablexia.game.games.nocnisledovani.GfxManager.OnInitFinished;
import cz.nic.tablexia.game.games.nocnisledovani.listener.MyDelayModifierListener;
import cz.nic.tablexia.game.games.nocnisledovani.listener.MyDelayedImageSwitchModifierListener;
import cz.nic.tablexia.game.games.nocnisledovani.listener.WindowListener;
import cz.nic.tablexia.game.games.nocnisledovani.model.WindowSequenceGenerator;
import cz.nic.tablexia.util.PauseableTimerHandler;

public class NocniSledovaniActivity extends GameActivity {
    private static final int                  CAMERA_WIDTH                          = 480;
    private static final int                  CAMERA_HEIGHT                         = 320;
    private static final String               TAG                                   = NocniSledovaniActivity.class.getSimpleName();
    public static final String                BASE_DIR                              = "nocnisledovani/";
    private static final int                  NUMBER_OF_ROUNDS_PER_DIFFICULTY_LEVEL = 8;
    private static final int                  NUMBER_OF_LEVELS_PER_GAME             = 2;
    //time difference is divided by number of rounds in each level showing the same number of windows
    private static final float                STARTING_LID_TIME                     = 1.5f;
    private static final float                END_LID_TIME                          = 0.7f;
    private static final float                SOLUTION_SHOW_TIME                    = 2.0f;

    private static final int                  NUMBER_OF_SCENES_PER_ANIMATION        = 7;

    private static final float                ANIMATION_TO_SHOWTIME_DELAY           = 2;                                           // doba mezi animaci ze dne na noc a obracene a rozsvicenim oken
    private static final float                BACKGROUND_SWITCH_DELAY               = 0.1f;
    private static final float                START_DELAY                           = 2f;

    private GfxManager                        gfxManager;
    private int                               currentLevel;                                                                        // level of current scene
    private int                               currentRound;
    private int                               difficulty;
    private boolean                           gameLoadedFromPreviousState;
    private Sprite                            background;
    private List<Sprite>                      backGroundAnimationSprites;

    private WindowSequenceGenerator           sequenceGenerator;
    private Map<Integer, Sprite>              windowSprites;
    private List<Integer>                     generatedWindowNumbers;                                                              // list of time values
    // generated for this scene
    private int                               currentRoundGeneratedTime;                                                           // time generated for current round
    private List<Integer>                     generatedTimeValues;
    private List<Map<Integer, List<Integer>>> generatedSequencesForLevels;

    private MyWatchScene                      subscene;
    private List<Integer>                     lightWindows                          = new ArrayList<Integer>();

    private TextView                          btnContinue;
    private int                               numberOfWrongTimeValuesGuessed;
    private int                               numberOfWrongWindowsGuessed;

    private WindowListener                    onWindowClickListener;
    private MfxManager                        mfxManager;
    private Random                            random                                = new Random();

    private List<PauseableTimerHandler>       pauseableHandlers;

    public NocniSledovaniActivity() {
        super(GamesDefinition.NOCNISLEDOVANI);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            gameLoadedFromPreviousState = true;
        }
        btnContinue = (TextView) findViewById(R.id.nocnisledovani_button_continue);
        btnContinue.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //nastavil jsem okna
                hideContinue();
                scene.setOnSceneTouchListener(null);

                AsyncTask<Void, Void, Void> showRoundSolution = new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        try {
                            showRoundSolution();
                            Thread.sleep((long) (SOLUTION_SHOW_TIME * 1000));
                        } catch (InterruptedException e) {
                            Log.e(TAG, "Thread interrupted");
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        Log.d(TAG, "Finished showing solution");
                        onWindowClickListener.clearScene();
                        subscene.clearSolution();
                        currentRound++;
                        if (currentRound == (getRoundsPerLevelCount(difficulty, currentLevel))) {
                            currentLevel++;
                            currentRound = 0;
                            if (!(currentLevel == NUMBER_OF_LEVELS_PER_GAME)) {
                                switchRound();
                            }
                        } else if (currentRound < (getRoundsPerLevelCount(difficulty, currentLevel))) {
                            switchRound();
                        }
                        if (currentLevel == (NUMBER_OF_LEVELS_PER_GAME)) {
                            evaluateRound();
                            clearScene();
                            gameComplete();

                        }
                    }
                };
                showRoundSolution.execute();
            }
        });
    }

    @Override
    public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws IOException {
        gfxManager = new GfxManager(getTextureManager(), getAssets(), this, getVertexBufferObjectManager(), camera, displaySize);
        mfxManager = new MfxManager(getMusicManager(), this);
        pOnCreateResourcesCallback.onCreateResourcesFinished();
    }

    public interface LoaderCallback {
        public void onLoadFinished();
    }

    @Override
    public void onPopulateScene(Scene pScene, final OnPopulateSceneCallback pOnPopulateSceneCallback) throws IOException {
        initGame(new LoaderCallback() {
            @Override
            public void onLoadFinished() {
                pOnPopulateSceneCallback.onPopulateSceneFinished();
            }
        });
    }

    /**
     * Handler for replay game button
     */
    @Override
    public void onBtnReplayClick() {
        EasyTracker.getInstance(this).send(MapBuilder.createEvent("game_action", "replay", getGameDefinition().name(), 0l).build());
        resetGame();
        initGame();
        showGame();
        hideVictoryScreen();
    }

    @Override
    protected void resetGame() {
        super.resetGame();

        numberOfWrongWindowsGuessed = 0;
        numberOfWrongTimeValuesGuessed = 0;
        currentRoundGeneratedTime = 0;
        currentLevel = 0;
        currentRound = 0;
        difficulty = getDifficulty().ordinal();
        pauseableHandlers = new ArrayList<PauseableTimerHandler>();

        sequenceGenerator = new WindowSequenceGenerator();
        generatedSequencesForLevels = new ArrayList<Map<Integer, List<Integer>>>();
        for (int i = 0; i < NUMBER_OF_LEVELS_PER_GAME; i++) {
            Map<Integer, List<Integer>> currentLevelGeneratedValues = sequenceGenerator.getRandomWindowAndTimeList(getNumberOfWindowsForDifficulty(difficulty), getNumberOfWindowsLightenedInCurrentRound(difficulty, i), getRoundsPerLevelCount(difficulty, i));
            generatedSequencesForLevels.add(new HashMap<Integer, List<Integer>>(currentLevelGeneratedValues));
        }
        generatedTimeValues = new ArrayList<Integer>();

        subscene = new MyWatchScene(gfxManager, mfxManager, getTextureManager(), getVertexBufferObjectManager(), camera, displaySize);
        scene.setChildScene(subscene);
    }

    protected void initGame(final LoaderCallback loaderCallback) {
        Log.d(TAG, "In MYINIT GAME");
        super.initGame();
        //TODO otestovat obnoveni hry po obnoveni
        if (!gameLoadedFromPreviousState) {
            numberOfWrongTimeValuesGuessed = 0;
            currentRoundGeneratedTime = 0;
            currentLevel = 0;
            currentRound = 0;
        }

        difficulty = getDifficulty().ordinal();
        pauseableHandlers = new ArrayList<PauseableTimerHandler>();

        sequenceGenerator = new WindowSequenceGenerator();
        generatedSequencesForLevels = new ArrayList<Map<Integer, List<Integer>>>();
        for (int i = 0; i < NUMBER_OF_LEVELS_PER_GAME; i++) {
            Map<Integer, List<Integer>> currentLevelGeneratedValues = sequenceGenerator.getRandomWindowAndTimeList(getNumberOfWindowsForDifficulty(difficulty), getNumberOfWindowsLightenedInCurrentRound(difficulty, i), getRoundsPerLevelCount(difficulty, i));
            generatedSequencesForLevels.add(new HashMap<Integer, List<Integer>>(currentLevelGeneratedValues));
        }
        generatedTimeValues = new ArrayList<Integer>();
        windowSprites = new HashMap<Integer, Sprite>();
        subscene = new MyWatchScene(gfxManager, mfxManager, getTextureManager(), getVertexBufferObjectManager(), camera, displaySize);
        scene.setChildScene(subscene);

        gfxManager.initTextures(difficulty, NUMBER_OF_SCENES_PER_ANIMATION, new OnInitFinished() {

            @Override
            public void onFinished() {
                backGroundAnimationSprites = gfxManager.getBackgroundAnimationSprites(NUMBER_OF_SCENES_PER_ANIMATION, camera.getCameraSceneWidth() / 2, camera.getCameraSceneHeight() / 2, camera.getCameraSceneWidth(), camera.getCameraSceneHeight(), getVertexBufferObjectManager());
                background = backGroundAnimationSprites.get(0);

                scene.setBackground(new SpriteBackground(background));
                setWindowListener();
                if (loaderCallback != null) {
                    loaderCallback.onLoadFinished();
                }
            }
        });

    }

    @Override
    protected void showGame() {
        super.showGame();
        DelayModifier modifier = new DelayModifier(START_DELAY);
        mfxManager.playAmbientSound(MfxManager.SOUND_DAY);
        modifier.addModifierListener(new MyDelayModifierListener() {
            @Override
            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                Log.d(TAG, "IN SHOW GAME PAUSE END");
                animateDayToNight(BACKGROUND_SWITCH_DELAY);
            }
        });
        scene.registerEntityModifier(modifier);
    }

    /*
     * game requires sounds
     */
    @Override
    public EngineOptions onCreateEngineOptions() {
        EngineOptions engineOptions = super.onCreateEngineOptions();
        engineOptions.getAudioOptions().setNeedsSound(true).setNeedsMusic(true);
        return engineOptions;
    }

    @Override
    public Camera onCreateCamera() {
        return new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
    }

    @Override
    public CharSequence getVictoryText(int progress) {
        switch (progress) {
            case 0:
                return getString(R.string.game_nocnisledovani_victory_text_0);
            case 1:
                return getString(R.string.game_nocnisledovani_victory_text_1);
            case 2:
                return getString(R.string.game_nocnisledovani_victory_text_2);
            case 3:
                return getString(R.string.game_nocnisledovani_victory_text_3);
            default:
                throw new IllegalStateException("Unknown progress");
        }
    }

    @Override
    public String[] getVictorySpeech() {
        return new String[] { SpeechSounds.RESULT_NOCNISLEDOVANI_0, SpeechSounds.RESULT_NOCNISLEDOVANI_1, SpeechSounds.RESULT_NOCNISLEDOVANI_2, SpeechSounds.RESULT_NOCNISLEDOVANI_3 };
    }

    @Override
    public CharSequence getStatsText() {
        TextView tvStats = (TextView) victoryLayout.findViewById(R.id.victoryscreen_result);
        // TADY MUSI BYT DVE MEZERY, jinak se string neappenduje, resp. nevim
        // jak
        SpannableStringBuilder ssb = new SpannableStringBuilder("  ");
        Bitmap clock = BitmapFactory.decodeResource(getResources(), R.drawable.victoryscreen_time);
        ssb.setSpan(new ImageSpan(this, clock), 0, 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        ssb.append(getResources().getString(R.string.victory_text_window_mistakes));
        ssb.append(Integer.toString(getGameManager().getExtraInt1()));
        tvStats.setText(ssb);

        SpannableStringBuilder ssb2 = new SpannableStringBuilder("  ");
        Bitmap stats = BitmapFactory.decodeResource(getResources(), R.drawable.victoryscreen_stats);
        ssb2.setSpan(new ImageSpan(this, stats), 0, 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        ssb2.append(getResources().getString(R.string.victory_text_time_mistakes));
        ssb2.append(Integer.toString(getGameManager().getExtraInt2()));
        return ssb.append(ssb2);
    }

    @Override
    public int countProgress() {
        int points = (16 - numberOfWrongTimeValuesGuessed) - numberOfWrongWindowsGuessed;
        if (points > 13) {
            return 3;
        } else if (points > 8) {
            return 2;
        } else if (points > 4) {
            return 1;
        }
        return 0;
    }

    @Override
    protected void gameComplete() {
        getGameManager().setCounterAndSave(numberOfWrongTimeValuesGuessed + numberOfWrongWindowsGuessed);
        super.gameComplete();
    }

    /*
     * Method animates the background from day sprite to night sprite
     */
    private void animateDayToNight(float delay) {
        subscene.disableHourHand();
        mfxManager.playSound(MfxManager.SOUND_TRANSITIONS_EVENING[random.nextInt(MfxManager.SOUND_TRANSITIONS_EVENING.length)]);
        DelayModifier dm = new DelayModifier(delay, new MyDelayedImageSwitchModifierListener(scene, backGroundAnimationSprites.get(1)));
        DelayModifier dm2 = new DelayModifier(delay, new MyDelayedImageSwitchModifierListener(scene, backGroundAnimationSprites.get(2)));
        DelayModifier dm3 = new DelayModifier(delay, new MyDelayedImageSwitchModifierListener(scene, backGroundAnimationSprites.get(3)) {
            @Override
            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                super.onModifierFinished(pModifier, pItem);
                lightUpWindowsAndShowTime();
                mfxManager.playAmbientSound(MfxManager.SOUND_NIGHT);
            }
        });

        SequenceEntityModifier sequence = new SequenceEntityModifier(dm, dm2, dm3);
        scene.registerEntityModifier(sequence);
    }

    /*
     * Method animates the background from night sprite to day sprite
     */
    private void animateNightToDay(final float delay) {
        mfxManager.playSound(MfxManager.SOUND_TRANSITIONS_MORNING[random.nextInt(MfxManager.SOUND_TRANSITIONS_MORNING.length)]);
        DelayModifier delayBeforeAnimationStart = new DelayModifier(ANIMATION_TO_SHOWTIME_DELAY);
        DelayModifier dm = new DelayModifier(delay, new MyDelayedImageSwitchModifierListener(scene, backGroundAnimationSprites.get(4)));
        DelayModifier dm1 = new DelayModifier(delay, new MyDelayedImageSwitchModifierListener(scene, backGroundAnimationSprites.get(5)));
        DelayModifier dm2 = new DelayModifier(delay, new MyDelayedImageSwitchModifierListener(scene, backGroundAnimationSprites.get(6)));
        DelayModifier dm3 = new DelayModifier(delay, new MyDelayedImageSwitchModifierListener(scene, backGroundAnimationSprites.get(0)) {
            @Override
            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                super.onModifierFinished(pModifier, pItem);
                // enables hourhand to be set on clock
                subscene.enableHourhand();
                scene.setOnSceneTouchListener(onWindowClickListener);
                mfxManager.playAmbientSound(MfxManager.SOUND_DAY);
            }
        });

        SequenceEntityModifier sequence = new SequenceEntityModifier(delayBeforeAnimationStart, dm, dm1, dm2, dm3);
        scene.registerEntityModifier(sequence);
    }

    private void lightUpWindowsAndShowTime() {
        final float windowLidTime = getCurrentLightTime(difficulty, currentLevel);
        final Map<Integer, List<Integer>> generatedSequence = generatedSequencesForLevels.get(currentLevel);
        Set<Integer> sequenceKeySet = generatedSequence.keySet();
        generatedTimeValues = new ArrayList<Integer>(sequenceKeySet);

        PauseableTimerHandler animationToAnimationPauseableHandler = new PauseableTimerHandler(ANIMATION_TO_SHOWTIME_DELAY, false, new ITimerCallback() {

            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                Log.d("generatedTimeValuesSize", Integer.toString(generatedTimeValues.size()));
                currentRoundGeneratedTime = generatedTimeValues.get(currentRound);
                generatedWindowNumbers = generatedSequence.get(currentRoundGeneratedTime);

                for (Integer windowIndex : generatedWindowNumbers) {
                    onWindowClickListener.lightUpWindow(windowIndex);
                }
                subscene.setTimeOnWatch(new int[] { currentRoundGeneratedTime, 0 }, 0);
                PauseableTimerHandler windowHandler = new PauseableTimerHandler(windowLidTime, false, new ITimerCallback() {

                    @Override
                    public void onTimePassed(TimerHandler pTimerHandler) {
                        Log.d(TAG, "Finished lighting up windows");
                        //hideWindows();
                        onWindowClickListener.lightDownAllWindows();
                        subscene.setTimeOnWatch(new int[] { 0, 0 }, 0);
                        animateNightToDay(BACKGROUND_SWITCH_DELAY);
                    }
                });
                scene.registerUpdateHandler(windowHandler);
                pauseableHandlers.add(windowHandler);
            }
        });
        scene.registerUpdateHandler(animationToAnimationPauseableHandler);
        pauseableHandlers.add(animationToAnimationPauseableHandler);
    }

    private void hideContinue() {
        toggleView(false, btnContinue);
    }

    private void showContinue() {
        toggleView(true, btnContinue);
    }

    private void toggleView(final boolean show, final View view) {
        toggleView(show, view, getResources().getInteger(android.R.integer.config_shortAnimTime));
    }

    /**
     * Změní viditelnost view pomocí animace průhlednosti a poté nastaví
     * visible/gone
     * 
     * @param show
     * @param view
     */
    private void toggleView(final boolean show, final View view, final long duration) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!show && (view.getVisibility() == View.GONE)) {
                    return;
                }
                view.setVisibility(View.VISIBLE);
                view.animate().setDuration(duration).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(show ? View.VISIBLE : View.GONE);
                    }
                });
            }
        });
    }

    @Override
    protected int getLayoutID() {
        return R.layout.game_nocnisledovani;
    }

    /*
     * Method evaluates users results according to the correct solution
     */
    private void evaluateRound() {
        Log.d(TAG, "Started evaluating round");
        boolean someWindowWrong = false;
        for (int i = 0; i < lightWindows.size(); i++) {
            if (!generatedWindowNumbers.contains(lightWindows.get(i))) {
                someWindowWrong = true;
            }
        }
        if (someWindowWrong) {
            getGameManager().setExtraInt1AndSave(++numberOfWrongWindowsGuessed);
        }

        int timeGuessedByUser = subscene.getCurrentlySetTime()[0];
        if (timeGuessedByUser != currentRoundGeneratedTime) {
            getGameManager().setExtraInt2AndSave(++numberOfWrongTimeValuesGuessed);
        }
        Log.d("RESULT", "WW: " + numberOfWrongWindowsGuessed + " WT: " + numberOfWrongTimeValuesGuessed);
    }

    /*
     * Method switches level in current scene
     */
    private void switchRound() {
        evaluateRound();
        clearScene();
        hideContinue();
        DelayModifier dm = new DelayModifier(ANIMATION_TO_SHOWTIME_DELAY, new MyDelayModifierListener() {
            @Override
            public void onModifierFinished(org.andengine.util.modifier.IModifier<IEntity> pModifier, IEntity pItem) {
                animateDayToNight(BACKGROUND_SWITCH_DELAY);
            };
        });
        scene.registerEntityModifier(dm);
    }

    private void clearScene() {
        subscene.setTimeOnWatch(new int[] { 0, 0 }, 0);
        onWindowClickListener.lightDownAllWindows();
        scene.setOnSceneTouchListener(null);
        lightWindows.clear();
    }

    private void setWindowListener() {
        onWindowClickListener = new WindowListener(this, difficulty, gfxManager, new Point((int) background.getWidth(), (int) background.getHeight()), lightWindows, getTextureManager(), getVertexBufferObjectManager(), camera, getAssets(), scene) {
            @Override
            protected void onLightWindowClickAction(int windowIndex) {
                super.onLightWindowClickAction(windowIndex);
                if (lightWindows.size() < getNumberOfWindowsLightenedInCurrentRound(difficulty, currentLevel)) {
                    // uzivatel zrusil jedno okno a nejsou jiz oznacena tri
                    hideContinue();
                }
            }

            @Override
            protected void onDarkWindowClickAction(int windowIndex) {
                if (lightWindows.size() < getNumberOfWindowsLightenedInCurrentRound(difficulty, currentLevel)) {
                    super.onDarkWindowClickAction(windowIndex);
                    if (lightWindows.size() == getNumberOfWindowsLightenedInCurrentRound(difficulty, currentLevel)) {
                        // uzivatel naklikal tri rozsvicena okna
                        showContinue();
                    }
                }
            }
        };

    }

    /*
     * Method shows users answers and correct answers on screen
     */
    private void showRoundSolution() {
        onWindowClickListener.showSolution(generatedWindowNumbers);
        subscene.showSolution(new int[] { currentRoundGeneratedTime, 0 });
    }

    private int getRoundsPerLevelCount(int difficulty, int level) {
        switch (difficulty) {
            case 0:
                if (level == 0) {
                    return 4;
                } else if (level == 1) {
                    return 4;
                }
            case 1:
                if (level == 0) {
                    return 3;
                } else if (level == 1) {
                    return 5;
                }
            case 2:
                if (level == 0) {
                    return 5;
                } else if (level == 1) {
                    return 3;
                }
            default:
                throw new IllegalArgumentException();
        }
    }

    private int getNumberOfWindowsLightenedInCurrentRound(int difficulty, int level) {
        switch (difficulty) {
            case 0:
                if (level == 0) {
                    return 1;
                } else if (level == 1) {
                    return 2;
                }
            case 1:
                if (level == 0) {
                    return 1;
                } else if (level == 1) {
                    return 2;
                }
            case 2:
                if (level == 0) {
                    return 2;
                } else if (level == 1) {
                    return 3;
                }
            default:
                throw new IllegalArgumentException();
        }
    }

    /*
     * returns the number of the windows in scene according to current scene
     * number
     */
    public int getNumberOfWindowsForDifficulty(int difficulty) {
        switch (difficulty) {
            case 0:
                return 16;
            case 1:
                return 24;
            case 2:
                return 32;
            default:
                throw new IllegalStateException("Chyba");
        }
    }

    /*
     * returns how long is the window lightened according to current level
     */
    public float getCurrentLightTime(int difficulty, int level) {
        int numberOfRoundsInCurrentLevel = getRoundsPerLevelCount(difficulty, level);
        float timeUnit = (STARTING_LID_TIME - END_LID_TIME) / numberOfRoundsInCurrentLevel;
        return STARTING_LID_TIME - (timeUnit * currentRound);
    }

    @Override
    protected void pause() {
        super.pause();
        for (PauseableTimerHandler handler : pauseableHandlers) {
            handler.pause();
        }
    }

    @Override
    protected void resume() {
        super.resume();
        for (PauseableTimerHandler handler : pauseableHandlers) {
            handler.resume();
        }
    }

}
