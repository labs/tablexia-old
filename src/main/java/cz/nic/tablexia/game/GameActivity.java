/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.game;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.andengine.audio.music.Music;
import org.andengine.engine.Engine;
import org.andengine.engine.FixedStepEngine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.IEntity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.view.RenderSurfaceView;
import org.andengine.ui.activity.LayoutGameActivity;
import org.andengine.util.adt.color.Color;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;

import cz.nic.tablexia.R;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.Tablexia.OnDataMountedListener;
import cz.nic.tablexia.audio.SoundControl;
import cz.nic.tablexia.audio.resources.PermanentSounds;
import cz.nic.tablexia.audio.resources.SfxSounds;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.GamesDefinition;
import cz.nic.tablexia.game.manager.GameManager;
import cz.nic.tablexia.main.MainActivity;
import cz.nic.tablexia.menu.AbstractMenu.MenuChangeListener;
import cz.nic.tablexia.menu.MenuActivity;
import cz.nic.tablexia.menu.MenuEnum;
import cz.nic.tablexia.menu.mainmenu.MainMenuDefinition;
import cz.nic.tablexia.menu.mainmenu.screen.AbstractMenuFragment;
import cz.nic.tablexia.menu.usermenu.User;
import cz.nic.tablexia.trophy.GameTrophy;
import cz.nic.tablexia.trophy.TrophyEnum;
import cz.nic.tablexia.trophy.UserTrophyDefinition;
import cz.nic.tablexia.util.MintHelper;
import cz.nic.tablexia.widget.TablexiaDrawerLayout;
import cz.nic.tablexia.widget.TablexiaQuestionBannerDialog;

public abstract class GameActivity extends LayoutGameActivity implements MenuActivity, DrawerListener, OnClickListener {

    private enum GameState {
        STARTED,
        IDLE;
    }

    private final static Integer[] VICTORY_SCREEN_TROPHIES_LAYOUTS           = new Integer[] { R.id.victoryscreen_trophy1, R.id.victoryscreen_trophy2, R.id.victoryscreen_trophy3, R.id.victoryscreen_trophy4, R.id.victoryscreen_trophy5, };

    protected static final String  GAMEACTIVITY_LOG_PREFIX                   = "[GAMEACTIVITY] ";
    private static final int       VICTORYSCREEN_HIDE_DURATION               = 800;
    private static final int       VICTORYSCREEN_STAR1_DELAY                 = 500;
    private static final int       VICTORYSCREEN_STAR2_DELAY                 = 1000;
    private static final int       VICTORYSCREEN_STAR3_DELAY                 = 1500;
    private static final int       USER_GAME_LOADING_HELPSOUND_PLAY_MAXCOUNT = 2;

    private static final String    TAG                                       = GameActivity.class.getSimpleName();

    protected Scene                scene;
    private TablexiaDrawerLayout   menuDrawer;
    protected Point                displaySize;
    protected Camera               camera;
    protected boolean              closeResume                               = true;
    protected boolean              afterResume                               = false;
    protected boolean              paused                                    = false;
    private boolean                playVictorySpeech                         = false;
    private boolean                useGameManager                            = true;

    private static final int       STEPS_PER_SECOUND                         = 60;
    protected static final int     VICTORY_SCREEN_STARS_COUNT                = 3;

    private static final String    GAMEMANAGER_KEY                           = "gamemanager";

    protected GameManager          gameManager;
    private GamesDefinition        gamesDefinition;
    private Rectangle              hider;
    private Music                  gameMusic;

    protected RelativeLayout       victoryLayout;
    private FrameLayout            loadindScreen;
    private ViewGroup              gamePanelLayout;
    private FrameLayout            renderViewParent;

    private TextView               loadingScreenButton;

    private GameState              gameState;
    private SoundControl           soundControl;

    public GameActivity(GamesDefinition gamesDefinition) {
        this.gamesDefinition = gamesDefinition;
    }

    public GameManager getGameManager() {
        return gameManager;
    }

    public GamesDefinition getGameDefinition() {
        return gamesDefinition;
    }

    protected ViewGroup getGamePanelLayout() {
        return gamePanelLayout;
    }

    public void setGameMusic(Music gameMusic) {
        this.gameMusic = gameMusic;
        if (gameMusic != null) {
            gameMusic.setLooping(true);
        }
    }

    @Override
    protected void onSetContentView() {
        setContentView(getLayoutID());
    }

    @Override
    public boolean isGameActive(GamesDefinition gameDefinitionToCheck) {
        return (gamesDefinition == gameDefinitionToCheck) && (gameState == GameState.STARTED);
    }

    protected void disableGameManagerForCurrentGame() {
        useGameManager = false;
    }

    /* //////////////////////////////////////////// TABLEXIA LIFECYCLE */

    /**
     * Custom game scene settings
     */
    protected void onCreateTablexiaScene() {
        // nothing needed
    }

    private void lowProfileMode() {
        try {
            LayoutParams lp = getWindow().getAttributes();
            lp.dimAmount = LayoutParams.FLAG_DIM_BEHIND;
            getWindow().setAttributes(lp);
        } catch (Throwable e) {
            // nic nedelat
            Log.e(this.getClass().getName(), "Dim buttons: " + e.getMessage());
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            final View rootView = getWindow().getDecorView();
            rootView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
            rootView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                @Override
                public void onSystemUiVisibilityChange(int visibility) {
                    if (visibility == View.SYSTEM_UI_FLAG_VISIBLE) {
                        Log.i(TAG, "UI Visible");
                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                rootView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
                            }
                        }, 2000);
                    }
                }
            });
        }
    }

    /**
     * Reset game state
     */
    protected void resetGame() {
        scene.reset();
        scene.clearEntityModifiers();
        scene.clearUpdateHandlers();
        scene.clearTouchAreas();
        clearScene();

        useGameManager = true;
        gameManager = null;
    }

    /**
     * Remove all child from scene
     */
    private void clearScene() {
        final List<IEntity> allChildToDetach = new ArrayList<IEntity>();
        for (int i = 0; i < scene.getChildCount(); i++) {
            IEntity childToDetach = scene.getChildByIndex(i);
            childToDetach.setVisible(false);
            allChildToDetach.add(childToDetach);
        }
        runOnUpdateThread(new Runnable() {

            @Override
            public void run() {
                for (IEntity iEntity : allChildToDetach) {
                    iEntity.detachSelf();
                }
            }
        });
    }

    /**
     * Game initialization callback. Is call when is game created and when is replay button clicked.
     * For scene content initialization and game manager initialization.
     *
     * @return initialized game manager
     */
    protected void initGame() {
        getTablexiaContext().showScreenContent(null);
        if ((gameManager == null) && useGameManager) {
            gameManager = new GameManager(gamesDefinition, getTablexiaContext().getSelectedUser(), getDifficulty());
        }
    }

    /**
     * For showing game screen components
     */
    protected void showGame() {
        EasyTracker.getInstance(this).send(MapBuilder.createEvent("game_action", "show", getGameDefinition().name(), 0l).build());
        if ((gameManager != null) && (gameManager.getStartTime() == 0)) {
            gameManager.start();
        }
        playMusic();
        gameState = GameState.STARTED;
        getTablexiaContext().prepareMainMenu().refreshMainMenu();
        // show all scene
        runOnUpdateThread(new Runnable() {

            @Override
            public void run() {
                hider.detachSelf();
            }
        });

    }

    /**
     * Call in case of game is completed
     */
    protected void gameComplete() {
        int score = countProgress();
        EasyTracker.getInstance(this).send(MapBuilder.createEvent("game_action", "finish", getGameDefinition().name(), (long) score).build());
        Log.d(GameActivity.class.toString(), GAMEACTIVITY_LOG_PREFIX + " -> END GAME");

        pauseMusic();

        if (gameManager != null) {
            gameManager.stop(score);
        }
        gameState = GameState.IDLE;

        User selectedUser = getTablexiaContext().getSelectedUser();
        Long userId = selectedUser.getId();
        int gameNumber = gamesDefinition.ordinal();

        GameTrophy gameTrophy = GameTrophy.getGameTrophyForUserAndGame(userId, gameNumber);
        if (gameTrophy == null) {
            gameTrophy = GameTrophy.createGameTrophyAndSave(userId, gameNumber);
        }
        List<TrophyEnum> trophies = null;
        if (score >= 0) {
            trophies = gameTrophy.nextGameComplete(getDifficulty(), score);
            trophies.addAll(UserTrophyDefinition.processUserTrophies(selectedUser));
        }
        showVictoryScreen(score, trophies);
        GameTrophy.logAllGameTrophysForUser(userId);
    }

    /* //////////////////////////////////////////// ANDENGINE LIFECYCLE */

    @Override
    public Engine onCreateEngine(EngineOptions pEngineOptions) {
        return new FixedStepEngine(pEngineOptions, STEPS_PER_SECOUND);
    }

    public Camera onCreateCamera() {
        return new Camera(0, 0, displaySize.x, displaySize.y);
    }

    @Override
    public EngineOptions onCreateEngineOptions() {
        Display display = getWindowManager().getDefaultDisplay();
        displaySize = new Point();
        display.getSize(displaySize);

        camera = onCreateCamera();
        final EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_SENSOR, new FillResolutionPolicy(), camera);

        engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_DIM);

        return engineOptions;
    }

    @Override
    public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws IOException {
        pOnCreateResourcesCallback.onCreateResourcesFinished();
    }

    @Override
    public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) throws IOException {

        // hide all scene until showGame is called
        final HUD hud = new HUD();
        hider = new Rectangle(camera.getCenterX(), camera.getCenterY(), camera.getWidth(), camera.getHeight(), getVertexBufferObjectManager());
        hider.setColor(Color.BLACK);
        hud.attachChild(hider);
        camera.setHUD(hud);

        scene = new Scene() {

            boolean shown = false;

            @Override
            protected void onManagedUpdate(float pSecondsElapsed) {
                super.onManagedUpdate(pSecondsElapsed);
                if (!shown) {
                    shown = true;
                    enableStartButton();
                }
            }

        };
        onCreateTablexiaScene();
        pOnCreateSceneCallback.onCreateSceneFinished(scene);
    }

    @Override
    public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) throws IOException {
        initGame();
        pOnPopulateSceneCallback.onPopulateSceneFinished();
    }

    @Override
    protected int getLayoutID() {
        return R.layout.game;
    }

    @Override
    protected int getRenderSurfaceViewID() {
        return R.id.gameSurfaceView;
    }

    @Override
    public synchronized void onResumeGame() {
        super.onResumeGame();
        if (afterResume) {
            pause(false);
        }
    }

    /* //////////////////////////////////////////// ACTIVITY LIFECYCLE */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MintHelper.startBugSense(this);

        getTablexiaContext().setCurreActivity(this);
        getTablexiaContext().hideScreenContentFast(false);

        gameState = GameState.IDLE;
        renderViewParent = (FrameLayout) findViewById(R.id.renderViewParent);
        // menu drawer
        menuDrawer = (TablexiaDrawerLayout) findViewById(R.id.drawer_layout);
        menuDrawer.addDrawerListener(this);

        gamePanelLayout = (ViewGroup) findViewById(R.id.gamePanelLayout);

        prepareLoadingScreen();
        prepareVictoryScreen();
        // restore saved state
        afterResume = false;
        if (savedInstanceState != null) {
            // set flag for pause game and open menu
            afterResume = true;
            getTablexiaContext().restore();
            // game manager restore
            long gameManagerId = savedInstanceState.getLong(GAMEMANAGER_KEY);
            if (gameManagerId > 0) {
                gameManager = GameManager.load(GameManager.class, gameManagerId);
            }
        }
        lowProfileMode();

        getTablexiaContext().startMount(new OnDataMountedListener() {
            @Override
            public void onDataMounted() {
                onObbMounted();
            }

            @Override
            public void onMissingFile() {
                // TODO Auto-generated method stub

            }
        });
    }

    public void onObbMounted() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
                RenderSurfaceView renderSurfaceView = new RenderSurfaceView(GameActivity.this);
                renderSurfaceView.setId(R.id.gameSurfaceView);
                renderViewParent.addView(renderSurfaceView, params);
                mRenderSurfaceView = renderSurfaceView;
                mRenderSurfaceView.setRenderer(mEngine, GameActivity.this);
                showLoadingScreen();
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // set flag for pause game and open menu
        afterResume = true;
    }

    @Override
    protected void onDestroy() {
        menuDrawer.removeDrawerListener(this);
        getTablexiaContext().resetCurrentActivity(this);
        super.onDestroy();
    }

    @Override
    public void onPause() {
        // disables activity close animation
        unloadSoundControl();
        overridePendingTransition(0, 0);
        if (gameState == GameState.STARTED) {
            pause();
        }
        super.onPause();
    }

    @Override
    protected synchronized void onResume() {
        // menu have to be refreshed after activity resume
        getTablexiaContext().setCurreActivity(this);
        getTablexiaContext().initMenu();
        loadSoundControl();
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if ((gameManager != null) && (gameManager.getId() != null)) {
            outState.putLong(GAMEMANAGER_KEY, gameManager.getId());
        }
    }

    @Override
    public void onBackPressed() {
        if (gameState == GameState.STARTED) {
            new CancelGameDialog(null).show((this).getFragmentManager(), null);
        } else {
            getTablexiaContext().prepareMainMenu().changeMenuItem(gamesDefinition, true, true);
        }
    }

    @SuppressLint("ValidFragment")
    private class CancelGameDialog extends TablexiaQuestionBannerDialog {

        private MenuEnum menuEnum;

        public CancelGameDialog(MenuEnum menuEnum) {
            this.menuEnum = menuEnum;
        }

        @Override
        public void show(FragmentManager manager, String tag) {
            closeResume = true;
            pause();
            super.show(manager, tag);
        }

        @Override
        public void positiveAction() {
            super.positiveAction();
            menuEnum = menuEnum != null ? menuEnum : gamesDefinition;

            // disable menu opening on onPause event
            gameState = GameState.IDLE;

            getTablexiaContext().prepareMainMenu().changeMenuItem(menuEnum, true, true);
        }

        @Override
        protected int getTextResource() {
            return R.string.game_dialog_cancelGame;
        }

        @Override
        protected int getBannerResource() {
            return R.drawable.dialog_banner_pozor;
        }
    }

    /* //////////////////////////////////////////// SOUNDS */

    private void loadSoundControl() {
        soundControl = getTablexiaContext().getGlobalSoundControl();
        soundControl.addSound(SfxSounds.VICTORYSCREEN_SOUND);
        soundControl.addSound(SfxSounds.VICTORYSCREEN_STAR1_SOUND);
        soundControl.addSound(SfxSounds.VICTORYSCREEN_STAR2_SOUND);
        soundControl.addSound(SfxSounds.VICTORYSCREEN_STAR3_SOUND);
        for (String victorySpeechSoundPath : getVictorySpeech()) {
            soundControl.addSound(victorySpeechSoundPath);
        }
    }

    private void unloadSoundControl() {
        soundControl.stopSound(SfxSounds.VICTORYSCREEN_SOUND, true, false);
        soundControl.stopSound(SfxSounds.VICTORYSCREEN_STAR1_SOUND, true, false);
        soundControl.stopSound(SfxSounds.VICTORYSCREEN_STAR2_SOUND, true, false);
        soundControl.stopSound(SfxSounds.VICTORYSCREEN_STAR3_SOUND, true, false);
        soundControl.stop(Arrays.asList(getVictorySpeech()), null, true, false);
    }

    protected SoundControl getSoundControl() {
        return soundControl;
    }

    /* //////////////////////////////////////////// MENU ACTIVITY */

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public Tablexia getTablexiaContext() {
        return (Tablexia) getApplicationContext();
    }

    public void openMenuDrawer() {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                menuDrawer.openDrawer(TablexiaDrawerLayout.POSITION);
            }

        });
    }

    @Override
    public void lockCloseMenuDrawer() {
        getTablexiaContext().lockCloseMenuDrawer(menuDrawer);
    }

    @Override
    public void closeMenuDrawer() {
        getTablexiaContext().closeMenuDrawer(menuDrawer);
    }

    @Override
    public void lockOpenMenuDrawer() {
        getTablexiaContext().lockOpenMenuDrawer(menuDrawer);
    }

    @Override
    public void unlockMenuDrawer() {
        getTablexiaContext().unlockOpenMenuDrawer(menuDrawer);
    }

    @Override
    public void addMenuDrawerListener(DrawerListener drawerListener) {
        getTablexiaContext().addMenuDrawerListener(menuDrawer, drawerListener);
    }

    @Override
    public void removeMenuDrawerListener(DrawerListener drawerListener) {
        getTablexiaContext().removeMenuDrawerListener(menuDrawer, drawerListener);
    }

    @Override
    public android.support.v4.app.Fragment replaceScreenFragment(final MenuEnum menuEnum, final AbstractMenuFragment fragment, boolean forceReplace, boolean forceQuickTransition, final MenuChangeListener menuChangeListener) {
        if (forceReplace || (gameState != GameState.STARTED)) {
            closeMenuDrawer();
            getTablexiaContext().hideScreenContent(false, new AnimatorListenerAdapter() {

                @Override
                public void onAnimationStart(Animator animation) {
                    soundControl.muteAll();
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (menuEnum != null) {
                        getTablexiaContext().prepareMainMenu().changeActiveMenuItem(menuEnum);
                    }
                    Intent intent = new Intent(getContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);

                    Bundle argument = fragment.getArguments();
                    if (argument != null) {
                        intent.putExtras(argument);
                    }
                    getContext().startActivity(intent);
                }

            });

        } else {
            new CancelGameDialog(menuEnum).show((this).getFragmentManager(), null);
        }
        return null;
    }

    @Override
    public boolean isForceMenuRefresh() {
        // disable force refresh for AbstractGameActivity
        return false;
    }

    @Override
    public boolean isForceRedirectToCurrentMenu() {
        // enable force redirect tu current menu
        return true;
    }

    /* //////////////////////////////////////////// DRAWER LISTENER IMPLEMENTATION */

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
        // nothing needed
    }

    @Override
    public void onDrawerOpened(View drawerView) {
        closeResume = true;
        pause();
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        if (closeResume) {
            resume();
        }
    }

    @Override
    public void onDrawerStateChanged(int newState) {
        // nothing needed
    }

    /* //////////////////////////////////////////// ONCLICK LISTENER IMPLEMENTATION */

    @Override
    public void onClick(View v) {
        if (menuDrawer.isDrawerOpen(TablexiaDrawerLayout.POSITION)) {
            resume();
        } else {
            pause();
        }
    }

    /* //////////////////////////////////////////// GAME PAUSING */

    /**
     * Pause game
     *
     * @param logGameManager <code>false</code> for do not log pause activity in game manager
     */
    protected void pause(boolean logGameManager) {
        if (!paused) {
            paused = true;
            openMenuDrawer();
            if (scene != null) {
                if (logGameManager && (gameManager != null)) {
                    gameManager.pause();
                }
                scene.setIgnoreUpdate(true);
                if (getEngine().getEngineOptions().getAudioOptions().needsSound()) {
                    getEngine().getSoundManager().onPause();
                }
                if (getEngine().getEngineOptions().getAudioOptions().needsMusic()) {
                    pauseMusic();
                }
            }
        }
    }

    /**
     * Pause game
     */
    protected void pause() {
        pause(true);
    }

    /**
     * Resume game
     */
    protected void resume() {
        if (paused) {
            paused = false;
            closeMenuDrawer();
            if (scene != null) {
                scene.setIgnoreUpdate(false);
                if (getEngine().getEngineOptions().getAudioOptions().needsSound()) {
                    getEngine().getSoundManager().onResume();
                }
                if (getEngine().getEngineOptions().getAudioOptions().needsMusic()) {
                    playMusic();
                }
                if (gameManager != null) {
                    gameManager.resume();
                }
            }
        }
    }

    /* //////////////////////////////////////////// DIFFICULTY */

    /**
     * Returns current game difficulty
     *
     * @return current game difficulty
     */
    protected GameDifficulty getDifficulty() {
        return GameDifficulty.extractFromIntentExtra(getIntent());
    }

    /* //////////////////////////////////////////// MUSIC */

    private void playMusic() {
        if ((gameMusic != null) && !gameMusic.isPlaying()) {
            gameMusic.play();
        }
    }

    private void pauseMusic() {
        if ((gameMusic != null) && gameMusic.isPlaying()) {
            gameMusic.pause();
        }
    }

    /* //////////////////////////////////////////// LOADING SCREEN */

    private void prepareLoadingScreen() {
        loadindScreen = (FrameLayout) findViewById(R.id.gameloadingscreen);

        // ImageView backgroundImage = (ImageView) loadindScreen.findViewById(R.id.gameloadingscreen_background);
        // backgroundImage.setImageResource(gamesDefinition.getGameLoadingImageResourceId());

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View content = inflater.inflate(gamesDefinition.getGameLoadingLayoutResourceId(), null);
        ViewGroup dialog = (ViewGroup) loadindScreen.findViewById(R.id.gameloadingscreen_content);
        dialog.addView(content);

        loadingScreenButton = (TextView) loadindScreen.findViewById(R.id.gameloadingscreen_button);
        loadingScreenButton.setEnabled(false);
        loadingScreenButton.setClickable(false);
        loadingScreenButton.setGravity(Gravity.CENTER);
        loadingScreenButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!loadingScreenButton.isSelected()) {
                    loadingScreenButton.setSelected(true);
                    hideLoadingScreen();
                }
            }

        });

    }

    /**
     * Enables start button
     */
    private void enableStartButton() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingScreenButton.setText(R.string.gameloading_gotit);
                loadingScreenButton.setEnabled(true);
                loadingScreenButton.setClickable(true);
            }
        });
    }

    /**
     * Shows loading screen
     */
    private void showLoadingScreen() {
        loadindScreen.setAlpha(0);
        loadindScreen.setVisibility(View.VISIBLE);
        loadindScreen.animate().setDuration(800).alpha(1).setListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(Animator animation) {
                if ((getTablexiaContext().getSelectedUser() != null) && (GameManager.getGamePlayCountForUser(getTablexiaContext().getSelectedUser(), gamesDefinition) <= USER_GAME_LOADING_HELPSOUND_PLAY_MAXCOUNT)) {
                    if (!TextUtils.isEmpty(gamesDefinition.getGameLoadingSoundPath())) {
                        Log.d(TAG, "Loading sound " + gamesDefinition.getGameLoadingSoundPath());
                        // soundControl.addSound(gamesDefinition.getGameLoadingSoundPath());
                        soundControl.playMusic(gamesDefinition.getGameLoadingSoundPath(), false);
                    }
                }
            }

        });
    }

    /**
     * Shows loading screen
     */
    private void hideLoadingScreen() {
        getTablexiaContext().hideScreenContent(false, new AnimatorListenerAdapter() {

            @Override
            public void onAnimationStart(Animator animation) {
                soundControl.stopSound(gamesDefinition.getGameLoadingSoundPath(), true, true);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                loadindScreen.setVisibility(View.GONE);
                showGame();
                getTablexiaContext().showScreenContent(null);
            }

        });
    }

    /* //////////////////////////////////////////// VICTORY SCREEN */

    private void prepareVictoryScreen() {
        victoryLayout = (RelativeLayout) findViewById(R.id.victoryscreen);
        TextView btnReplay = (TextView) victoryLayout.findViewById(R.id.victoryscreen_replaybutton);
        btnReplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBtnReplayClick();
            }
        });
        TextView btnChangeGame = (TextView) victoryLayout.findViewById(R.id.victoryscreen_changegamebutton);
        btnChangeGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBtnChangeGameClick();
            }
        });
    }

    /**
     * Shows victory screen
     *
     * @param score played game score (number of stars)
     * @param trophies played game trophies
     */
    private void showVictoryScreen(final int score, final List<TrophyEnum> trophies) {
        playVictorySpeech = true;
        soundControl.playSound(SfxSounds.VICTORYSCREEN_SOUND, false);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                victoryLayout.setAlpha(0);
                victoryLayout.setVisibility(View.VISIBLE);
                showTrophies(trophies);
                setVictoriousTextsAndProgress(score);
                victoryLayout.animate().setDuration(800).alpha(1).setListener(null);
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        if (playVictorySpeech == true) {
                            if ((score >= 0) && (score < getVictorySpeech().length)) {
                                soundControl.playSound(getVictorySpeech()[score], false);
                            }
                        }
                    }

                }, 2000);
            }
        });

    }

    protected void hideVictoryScreen(final int duration) {
        playVictorySpeech = false;
        soundControl.muteAll();
        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                victoryLayout.animate().setDuration(duration).alpha(0).setListener(new AnimatorListenerAdapter() {

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        victoryLayout.setVisibility(View.GONE);
                        resetVictoryScreenScore();
                        hideTrophies();
                    }

                });
            }

        });
    }

    /**
     * Hides victory screen
     */
    protected void hideVictoryScreen() {
        hideVictoryScreen(VICTORYSCREEN_HIDE_DURATION);
    }

    /**
     * Shows game trophies on victory screen
     *
     * @param trophies game trophies
     */
    private void showTrophies(List<TrophyEnum> trophies) {
        if (trophies != null) {
            for (int i = 0; i < trophies.size(); i++) {
                if (i < VICTORY_SCREEN_TROPHIES_LAYOUTS.length) {
                    showTrophyImage(VICTORY_SCREEN_TROPHIES_LAYOUTS[i], trophies.get(i));
                }
            }
        }
    }

    /**
     * Hides game trophies from victory screen
     */
    private void hideTrophies() {
        hideTrophyImage(R.id.victoryscreen_trophy1);
        hideTrophyImage(R.id.victoryscreen_trophy2);
    }

    /**
     * Shows trophy image on specific layout
     *
     * @param trophyLaoutResourceId layout to show trophy in
     * @param trophyDefinition trophy to show
     */
    private void showTrophyImage(int trophyLaoutResourceId, final TrophyEnum trophyDefinition) {
        View trophyLayout = victoryLayout.findViewById(trophyLaoutResourceId);
        trophyLayout.setVisibility(View.VISIBLE);

        ((ImageView) trophyLayout.findViewById(R.id.victoryscreen_trophy_foreground)).setImageResource(trophyDefinition.getSmallImageResourceId());
        ((TextView) trophyLayout.findViewById(R.id.victoryscreen_trophy_name)).setText(trophyDefinition.getNameResourceId());
        ((TextView) trophyLayout.findViewById(R.id.victoryscreen_trophy_description)).setText(trophyDefinition.getDescriptionResourceId());

        trophyLayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                soundControl.playSound(PermanentSounds.BUTTON_SOUND, false);
                getTablexiaContext().prepareMainMenu().changeMenuItem(MainMenuDefinition.HALL_OF_FAME, true, false, false, trophyDefinition.getTrophyImageId(), null);
            }
        });
    }

    /**
     * Hides trophy image from specific layout
     *
     * @param trophyLaoutResourceId layout to hide trophy image from
     */
    private void hideTrophyImage(int trophyLaoutResourceId) {
        View trophyLayout = victoryLayout.findViewById(trophyLaoutResourceId);
        trophyLayout.setVisibility(View.GONE);
    }

    /**
     * Show score on victory screen
     *
     * @param score score to show in victory screen (number of stars)
     */
    private void setVictoryScreenScore(int score) {
        if (score < 0) {
            victoryLayout.findViewById(R.id.victoryscreen_ratingstar1).setVisibility(View.GONE);
            victoryLayout.findViewById(R.id.victoryscreen_ratingstar2).setVisibility(View.GONE);
            victoryLayout.findViewById(R.id.victoryscreen_ratingstar3).setVisibility(View.GONE);
        } else {
            victoryLayout.findViewById(R.id.victoryscreen_ratingstar1).setVisibility(View.VISIBLE);
            victoryLayout.findViewById(R.id.victoryscreen_ratingstar2).setVisibility(View.VISIBLE);
            victoryLayout.findViewById(R.id.victoryscreen_ratingstar3).setVisibility(View.VISIBLE);
            if (score > 0) {
                showStar((FrameLayout) victoryLayout.findViewById(R.id.victoryscreen_ratingstar1), SfxSounds.VICTORYSCREEN_STAR1_SOUND, VICTORYSCREEN_STAR1_DELAY);
            }
            if (score > 1) {
                showStar((FrameLayout) victoryLayout.findViewById(R.id.victoryscreen_ratingstar2), SfxSounds.VICTORYSCREEN_STAR2_SOUND, VICTORYSCREEN_STAR2_DELAY);
            }
            if (score > 2) {
                showStar((FrameLayout) victoryLayout.findViewById(R.id.victoryscreen_ratingstar3), SfxSounds.VICTORYSCREEN_STAR3_SOUND, VICTORYSCREEN_STAR3_DELAY);
            }
        }
    }

    /**
     * Hides score from victory screen
     */
    private void resetVictoryScreenScore() {
        hideStar((FrameLayout) victoryLayout.findViewById(R.id.victoryscreen_ratingstar1));
        hideStar((FrameLayout) victoryLayout.findViewById(R.id.victoryscreen_ratingstar2));
        hideStar((FrameLayout) victoryLayout.findViewById(R.id.victoryscreen_ratingstar3));
    }

    /**
     * Show specific star image with time delay from parameter and play star sound from parameter
     *
     * @param ratingStar start image view
     * @param starSoundPath path to the sound for this star
     * @param pause delay for star displaying
     */
    private void showStar(final ViewGroup ratingStar, final String starSoundPath, final int pause) {

        new Thread() {

            @Override
            public void run() {
                try {
                    sleep(pause);
                } catch (InterruptedException e) {
                    Log.w(GAMEACTIVITY_LOG_PREFIX + "Cannot make delay for victoryscreen star", e);
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ratingStar.findViewById(R.id.victoryscreen_star).setVisibility(View.VISIBLE);
                    }
                });
                soundControl.playSound(starSoundPath, false);
            }

        }.start();

    }

    /**
     * Hide specific star
     *
     * @param ratingStar star view
     */
    private void hideStar(final ViewGroup ratingStar) {
        ratingStar.findViewById(R.id.victoryscreen_star).setVisibility(View.GONE);
    }

    /**
     * Shows texts and score on victory screen
     *
     * @param score score to show on victory screen
     */
    public void setVictoriousTextsAndProgress(int score) {
        TextView txtvVictory = (TextView) victoryLayout.findViewById(R.id.victoryscreen_message);
        txtvVictory.setText(getVictoryText(score));

        TextView tvStats = (TextView) victoryLayout.findViewById(R.id.victoryscreen_result);
        tvStats.setText(getStatsText());

        resetVictoryScreenScore();
        setVictoryScreenScore(score);
    }

    /**
     * Handler for change game button
     */
    public void onBtnChangeGameClick() {
        // do not open game menu
        EasyTracker.getInstance(this).send(MapBuilder.createEvent("game_action", "change", getGameDefinition().name(), 0l).build());
        paused = true;
        getTablexiaContext().prepareMainMenu().changeMenuItem(gamesDefinition, true, true);
    }

    /**
     * Handler for replay game button
     */
    public void onBtnReplayClick() {
        EasyTracker.getInstance(this).send(MapBuilder.createEvent("game_action", "replay", getGameDefinition().name(), 0l).build());
        hideVictoryScreen();
        resetGame();
        initGame();
        showGame();
    }

    // Nastavi pro kazdou hru vitezny text v zavislosti na poctu hvezdicek(0-3)
    public abstract CharSequence getVictoryText(int progress);

    public abstract String[] getVictorySpeech();

    // nastavi text a ikony statistik podle namerenych vysledku
    public abstract CharSequence getStatsText();

    public abstract int countProgress();

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}
