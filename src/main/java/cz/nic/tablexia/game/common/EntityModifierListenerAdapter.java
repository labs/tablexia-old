package cz.nic.tablexia.game.common;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.util.modifier.IModifier;

/**
 * Adapter for IEntityModifierListener
 * 
 * @author Matyáš Latner
 *
 */
public class EntityModifierListenerAdapter implements IEntityModifierListener {

	@Override
	public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
		// to override
	}

	@Override
	public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
		// to override
	}

}
