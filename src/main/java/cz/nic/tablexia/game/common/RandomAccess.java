/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.common;

import java.util.Random;

/**
 * 
 * @author Matyáš Latner
 */
public class RandomAccess {
	
	private long randomSeed;
	private	Random random;
	
	public RandomAccess() {
		randomSeed = System.currentTimeMillis();
		random = new Random(randomSeed);
	}
	
	public Random getRandom() {
		return random;
	}
	
	public long getRandomSeed() {
		return randomSeed;
	}

}
