/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.game.difficulty;

import java.io.Serializable;

import android.content.Intent;
import cz.nic.tablexia.R;

/**
 * Game difficulty types
 * 
 * @author Matyáš Latner
 */
public enum GameDifficulty {

    EASY(R.string.difficulty_easy, R.color.gamedifficulty_easy, R.drawable.screen_gamemenu_difficultyseekbar_thumb_easy, R.drawable.screen_statistics_difficultyradio_easy),
    MEDIUM(R.string.difficulty_normal, R.color.gamedifficulty_medium, R.drawable.screen_gamemenu_difficultyseekbar_thumb_medium, R.drawable.screen_statistics_difficultyradio_medium),
    HARD(R.string.difficulty_hard, R.color.gamedifficulty_hard, R.drawable.screen_gamemenu_difficultyseekbar_thumb_hard, R.drawable.screen_statistics_difficultyradio_hard);

    private int descriptionResourceId;
    private int colorResourceId;
    private int seekbarThumImageId;
    private int statisticsRadioButtonResourceId;

    private GameDifficulty(int descriptionResourceId, int colorResourceId, int seekbarThumImageId, int statisticsRadioButtonResourceId) {
        this.descriptionResourceId = descriptionResourceId;
        this.colorResourceId = colorResourceId;
        this.seekbarThumImageId = seekbarThumImageId;
        this.statisticsRadioButtonResourceId = statisticsRadioButtonResourceId;
    }

    /**
     * Returns id of description for current difficulty
     * 
     * @return id of description string resource
     */
    public int getDescriptionResourceId() {
        return descriptionResourceId;
    }
    
    /**
     * Returns color resource id for current difficulty
     * @return color resource id for current difficulty
     */
    public int getColorResourceId() {
        return colorResourceId;
    }
    
    /**
     * Returns seekbar thumb image id for current difficulty
     * 
     * @return seekbar thumb image id for current difficulty
     */
    public int getSeekbarThumImageId() {
        return seekbarThumImageId;
    }
    
    /**
     * Returns statistics button image id for current difficulty
     * 
     * @return statistics button image id for current difficulty
     */
    public int getStatisticsRadioButtonResourceId() {
        return statisticsRadioButtonResourceId;
    }

    /**
     * Returns string key for intent extra
     * 
     * @return string key for intent extra
     */
    public static String getIntentKey() {
        return GameDifficulty.class.getName();
    }

    /**
     * Returns difficulty for integer value
     * 
     * @param number integer value for difficulty
     * @return difficulty
     */
    public static GameDifficulty getDifficultyForInt(int number) {
        GameDifficulty[] values = GameDifficulty.values();
        if ((number >= 0) && (number < values.length)) {
            return GameDifficulty.values()[number];
        }
        return null;
    }

    /**
     * Adds {@link GameDifficulty} value to intent extra
     * 
     * @param intent intent to add in
     * @param gameDifficulty specific game difficulty
     */
    public static void addToIntentExtra(Intent intent, GameDifficulty gameDifficulty) {
        intent.putExtra(getIntentKey(), gameDifficulty);
    }

    /**
     * Returns game difficulty extracted from intents extra
     * 
     * @param intent specific intent
     * @return game difficulty extracted from intent
     */
    public static GameDifficulty extractFromIntentExtra(Intent intent) {
        Serializable difficulty = intent.getExtras().getSerializable(getIntentKey());
        return difficulty != null ? (GameDifficulty) difficulty : null;
    }

}
