/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.audio.resources;

/**
 * 
 * @author Matyáš Latner
 */
public class PermanentSounds {
    
    public static final String  BUTTON_SOUND        = SfxSounds.SFX_PATH + "button.mp3";
    public static final String  MENU_CLOSE_SOUND    = SfxSounds.SFX_PATH + "leftmenu_close.mp3";
    public static final String  MENU_OPEN_SOUND     = SfxSounds.SFX_PATH + "leftmenu_open.mp3";
    
    public final static String[] PERMANENT_SOUNDS = new String[] {BUTTON_SOUND,
                                                                  MENU_OPEN_SOUND,
                                                                  MENU_CLOSE_SOUND};
}
