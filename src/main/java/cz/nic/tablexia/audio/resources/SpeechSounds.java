/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.audio.resources;

/**
 * @author Matyáš Latner
 */
public class SpeechSounds {

    private static final String SPEECH_PATH             = "menu/speech/";

    public static final String  LOGIN_HELP_SOUND        = SPEECH_PATH + "login.mp3";
    public static final String  HALLOFFAME_INTRO        = SPEECH_PATH + "halloffame.mp3";
    public static final String  STATISTICS_INTRO        = SPEECH_PATH + "statistics.mp3";
    public static final String  ENCYCLOPEDIA_INTRO      = SPEECH_PATH + "encyclopedia.mp3";

    public static final String  RESULT_BANKOVNILOUPEZ_0 = SPEECH_PATH + "result_bankovniloupez_0.mp3";
    public static final String  RESULT_BANKOVNILOUPEZ_1 = SPEECH_PATH + "result_bankovniloupez_1.mp3";
    public static final String  RESULT_BANKOVNILOUPEZ_2 = SPEECH_PATH + "result_bankovniloupez_2.mp3";
    public static final String  RESULT_BANKOVNILOUPEZ_3 = SPEECH_PATH + "result_bankovniloupez_3.mp3";

    public static final String  RESULT_PRONASLEDOVANI_0 = SPEECH_PATH + "result_pronasledovani_0.mp3";
    public static final String  RESULT_PRONASLEDOVANI_1 = SPEECH_PATH + "result_pronasledovani_1.mp3";
    public static final String  RESULT_PRONASLEDOVANI_2 = SPEECH_PATH + "result_pronasledovani_2.mp3";
    public static final String  RESULT_PRONASLEDOVANI_3 = SPEECH_PATH + "result_pronasledovani_3.mp3";

    public static final String  RESULT_UNOS_0           = SPEECH_PATH + "result_unos_0.mp3";
    public static final String  RESULT_UNOS_1           = SPEECH_PATH + "result_unos_1.mp3";
    public static final String  RESULT_UNOS_2           = SPEECH_PATH + "result_unos_2.mp3";
    public static final String  RESULT_UNOS_3           = SPEECH_PATH + "result_unos_3.mp3";

    public static final String  RESULT_STRELNICE_0      = SPEECH_PATH + "result_strelnice_0.mp3";
    public static final String  RESULT_STRELNICE_1      = SPEECH_PATH + "result_strelnice_1.mp3";
    public static final String  RESULT_STRELNICE_2      = SPEECH_PATH + "result_strelnice_2.mp3";
    public static final String  RESULT_STRELNICE_3      = SPEECH_PATH + "result_strelnice_3.mp3";

    public static final String  RESULT_NOCNISLEDOVANI_0 = SPEECH_PATH + "result_nocnisledovani_0.mp3";
    public static final String  RESULT_NOCNISLEDOVANI_1 = SPEECH_PATH + "result_nocnisledovani_1.mp3";
    public static final String  RESULT_NOCNISLEDOVANI_2 = SPEECH_PATH + "result_nocnisledovani_2.mp3";
    public static final String  RESULT_NOCNISLEDOVANI_3 = SPEECH_PATH + "result_nocnisledovani_3.mp3";

    public static final String  RESULT_POTME_0          = SPEECH_PATH + "result_potme_0.mp3";
    public static final String  RESULT_POTME_1          = SPEECH_PATH + "result_potme_1.mp3";
    public static final String  RESULT_POTME_2          = SPEECH_PATH + "result_potme_2.mp3";
    public static final String  RESULT_POTME_3          = SPEECH_PATH + "result_potme_3.mp3";

    public static final String  LOADING_BANKOVNILOUPEZ  = SPEECH_PATH + "loading_bankovniloupez.mp3";
    public static final String  LOADING_PRONASLEDOVANI  = SPEECH_PATH + "loading_pronasledovani.mp3";
    public static final String  LOADING_UNOS            = SPEECH_PATH + "loading_unos.mp3";
    public static final String  LOADING_STRELNICE       = SPEECH_PATH + "loading_strelnice.mp3";
    public static final String  LOADING_NOCNISLEDOVANI  = SPEECH_PATH + "loading_nocnisledovani.mp3";
    public static final String  LOADING_POTME           = SPEECH_PATH + "loading_potme.mp3";

    public static final String  UNOS_RULEHELP_EASY      = SPEECH_PATH + "unos_rulehelp_easy.mp3";
    public static final String  UNOS_RULEHELP_MEDIUM    = SPEECH_PATH + "unos_rulehelp_medium.mp3";
    public static final String  UNOS_RULEHELP_HARD      = SPEECH_PATH + "unos_rulehelp_hard.mp3";

}
