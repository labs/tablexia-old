/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.audio.resources;


/**
 * 
 * @author Matyáš Latner
 */
public class SfxSounds {
    
    public static final String SFX_PATH                           = "menu/sfx/";
    public static final String  VICTORYSCREEN_SOUND               = SFX_PATH + "victoryscreen_show.mp3";
    public static final String  VICTORYSCREEN_STAR1_SOUND         = SFX_PATH + "victoryscreen_star1.mp3";
    public static final String  VICTORYSCREEN_STAR2_SOUND         = SFX_PATH + "victoryscreen_star2.mp3";
    public static final String  VICTORYSCREEN_STAR3_SOUND         = SFX_PATH + "victoryscreen_star3.mp3";
    public static final String  DIALOG_ALERT                      = SFX_PATH + "dialog_alert.mp3";
    public static final String  SEEKBAR_STEP_1_2                  = SFX_PATH + "seekbar_step_1_2.mp3";
    public static final String  SEEKBAR_STEP_2_3                  = SFX_PATH + "seekbar_step_2_3.mp3";
    public static final String  SEEKBAR_STEP_3_2                  = SFX_PATH + "seekbar_step_3_2.mp3";
    public static final String  SEEKBAR_STEP_2_1                  = SFX_PATH + "seekbar_step_2_1.mp3";
    
    
    private static final String ENCYCLOPEDIA_SFX                  = "encyklopedie/sfx/";
    public  static final String ENCYCLOPEDIA_PAGING               = ENCYCLOPEDIA_SFX + "paging.mp3";
    
    private static final String NEWUSER_SFX                       = "newuser/sfx/";
    public  static final String NEWUSER_SIGNATURE                 = NEWUSER_SFX + "signature.mp3";
    public  static final String NEWUSER_STAMP                     = NEWUSER_SFX + "stamp.mp3";

}
