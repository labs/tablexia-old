/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.audio;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.ArrayUtils;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;

import com.activeandroid.util.Log;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.audio.resources.PermanentSounds;
import cz.nic.tablexia.util.MediaPlayerHelper;

/**
 * Class for controlling sound playback.
 *
 * @author Matyáš Latner
 */
public class SoundControl {
    private static final String      TAG                = SoundControl.class.getSimpleName();
    private static final float       FADEOUT_VOLUMESTEP = 0.1f;

    private final Map<String, Integer>     soundMap;
    private final Map<Integer, Integer>    streamMap;
    private final SoundPool                soundPool;
    private final Context                  context;
    private final Map<String, MediaPlayer> playingMedia;

    /**
     * Create new SoundControll class. Use new class for every new activity.
     *
     * @param context current activity context
     * @param maxSounds number of maximum simultaneously playing sounds
     */
    public SoundControl(Context context, int maxSounds) {
        this.context = context;
        soundPool = new SoundPool(maxSounds, AudioManager.STREAM_MUSIC, 0);
        soundMap = new ConcurrentHashMap<String, Integer>();
        streamMap = new ConcurrentHashMap<Integer, Integer>();
        playingMedia = new ConcurrentHashMap<String, MediaPlayer>();

        soundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {

            @Override
            public void onLoadComplete(SoundPool soundPool, int soundId, int status) {
                Integer streamId = streamMap.get(soundId);
                if ((streamId != null) && (streamId == 0)) {
                    streamMap.put(soundId, soundPool.play(soundId, 1f, 1f, 1, 0, 1));
                }
            }
        });
    }

    public void loadPermanentSounds() {
        for (String permanentSound : PermanentSounds.PERMANENT_SOUNDS) {
            addSound(permanentSound);
        }
    }

    private void removeSound(String sound) {
        if (!ArrayUtils.contains(PermanentSounds.PERMANENT_SOUNDS, sound) && soundMap.containsKey(sound)) {
            soundPool.unload(soundMap.get(sound));
            soundMap.remove(sound);
        }
    }

    /* //////////////////////////////////////////// PUBLIC API */

    /**
     * Prepare and loads new sound from assets directory
     *
     * @param sound new sound from assets directory to load
     */
    public void addSound(String sound) {
        if ((sound != null) && !soundMap.containsKey(sound)) {
            Tablexia tablexia = (Tablexia) context.getApplicationContext();
            soundMap.put(sound, soundPool.load(tablexia.getZipResourceFile().getAssetFileDescriptor(sound), 1));
        }
    }

    /**
     * Play sound by sound asset path. Sound have to be added before.
     *
     * @param soundPath sound path in asset directory
     * @param muteOther for <code>true</code> value mute all other playing sounds
     */
    public void playSound(String soundPath, boolean muteOther) {
        if (soundPath != null) {
            Integer soundId = soundMap.get(soundPath);
            if (soundId != null) {
                if (muteOther) {
                    stop(new ArrayList<String>(soundMap.keySet()), soundPath, false, true);
                }
                streamMap.put(soundId, soundPool.play(soundId, 1f, 1f, 1, 0, 1));
            }
        }
    }

    /**
     * Stops all sounds playback and optionally release resources with optional fade-out effect
     *
     * @param soundPaths sound assets to stop
     * @param exceptSound excepting this sound
     * @param release <code>true</code> release sound
     * @param fadeOut <code>true</code> stops sound with fade-out effect
     */
    public void stop(List<String> soundPaths, String exceptSound, boolean release, boolean fadeOut) {
        List<String> toStop = new ArrayList<String>();
        for (String soundPathToMute : soundPaths) {
            if ((soundPathToMute != null) && !soundPathToMute.equals(exceptSound)) {
                toStop.add(soundPathToMute);
            }
        }

        for (String stop : toStop) {
            stopSound(stop, release, fadeOut);
        }
    }

    /**
     * Stops sound playback and optionally release resource with optional fade-out effect
     *
     * @param soundPath sound asset to stop
     * @param release <code>true</code> release sound
     * @param fadeOut <code>true</code> stops sound with fade-out effect
     */
    public void stopSound(final String soundPath, final boolean release, boolean fadeOut) {
        if (soundMap.keySet().contains(soundPath)) {
            Integer soundId = soundMap.get(soundPath);
            if (soundId != null) {
                if (release) {
                    removeSound(soundPath);
                }
                final Integer streamId = streamMap.get(soundId);
                if (streamId != null) {
                    streamMap.remove(soundId);
                    if (fadeOut) {
                        Thread fadeOutThread = new Thread() {

                            private float volume     = 1f;
                            private final float volumeStep = FADEOUT_VOLUMESTEP;

                            @Override
                            public void run() {
                                while (volume > 0) {
                                    volume = volume - volumeStep;
                                    soundPool.setVolume(streamId, volume, volume);
                                    try {
                                        sleep(100);
                                    } catch (InterruptedException e) {
                                        Log.w(e.getLocalizedMessage());
                                    }
                                }
                                soundPool.stop(streamId);
                            }

                        };
                        fadeOutThread.start();
                    } else {
                        soundPool.stop(streamId);
                    }
                }
            }
        }
        if (playingMedia.keySet().contains(soundPath)) {
            if (fadeOut) {
                Thread fadeOutThread = new Thread() {

                    private float volume     = 1f;
                    private final float volumeStep = FADEOUT_VOLUMESTEP;

                    @Override
                    public void run() {
                        while (volume > 0) {
                            volume = volume - volumeStep;
                            try {
                                if (playingMedia.get(soundPath) != null) {
                                    playingMedia.get(soundPath).setVolume(volume, volume);
                                }
                                sleep(100);
                            } catch (Exception e) {
                                Log.w("Error " + e.getLocalizedMessage());
                            }
                        }
                        stopMedia(soundPath);
                    }

                };
                fadeOutThread.start();
            } else {
                stopMedia(soundPath);
            }
        }
    }

    public void muteAll() {
        muteAllSounds();
        mutelAllMedia();
    }

    public void mutelAllMedia() {
        stop(new ArrayList<String>(playingMedia.keySet()), null, false, true);
    }

    /**
     * Mute all sounds
     */
    public void muteAllSounds() {
        stop(new ArrayList<String>(soundMap.keySet()), null, false, true);
    }

    public void stopMedia(String soundPath) {
        MediaPlayer play = playingMedia.get(soundPath);
        if (play != null) {
            try {
                if (play.isPlaying()) {
                    play.stop();
                }
                play.release();
            } catch (IllegalStateException e) {
                // nothing
            }
        }
        playingMedia.remove(soundPath);
    }

    public void playMusic(final String sound, boolean muteAll) {
        if (muteAll) {
            muteAll();
        }
        if ((context != null) && (context.getApplicationContext() != null)) {
            Tablexia tablexia = (Tablexia) context.getApplicationContext();

            try {
                final MediaPlayer mp = new MediaPlayer();
                AssetFileDescriptor afd = tablexia.getZipResourceFile().getAssetFileDescriptor(sound);
                mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                mp.prepare();
                playingMedia.put(sound, mp);
                mp.setOnCompletionListener(new OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                        playingMedia.remove(sound);
                    }
                });
                mp.start();

            } catch (final IOException e) {
                Log.e(MediaPlayerHelper.class.getSimpleName(), "Error loading sound " + e.getMessage() + " - resource: " + sound, e);
            }
        }
    }
}
