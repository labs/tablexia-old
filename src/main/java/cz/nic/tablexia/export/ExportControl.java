/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.export;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.game.manager.GameManager;
import cz.nic.tablexia.menu.usermenu.User;

/**
 * Service for sending game data to server.
 * Unexported data are send to server as one JSON object.
 *
 * @author Matyáš Latner
 */
public class ExportControl extends Thread {

    private static final boolean PRETTY_FORMATED_OUTPUT = true;

    private static final String  LOGGER_PREFIX          = "[Export Service] ";

    private static final String  EXPORT_URL             = "https://www.tablexia.cz/upload_stats/";

    private static final String  STATS                  = "stats";
    private static final String  DEVICE_ID              = "deviceId";
    private static final String  DB_VERSION             = "dbVersion";
    private static final String  EXPORT_TIME            = "exportTime";
    private static final String  EXPORT_TYPE            = "exportType";
    private static final String  GAMES                  = "games";
    private static final String  VERSION                = "appVersion";
    private static final String  LANGUAGE               = "language";

    private static final String  REMOTE_ID              = "remoteId";
    private static final String  USERS                  = "users";

    private Context              context;

    /**
     * Game type for logging
     *
     * @author Matyáš Latner
     */
    public enum GameExportType {
        TEST_GAME,
        SCHOOL_GAME,
        PUBLIC_GAME;
    }

    private ExportControl(Context context) {
        this.context = context;
    }

    /**
     * Try to send unexported data to server as JSON object.
     *
     * @param context
     */
    public static void tryToSendData(Context context) {
        new ExportControl(context).start();
    }

    @Override
    public void run() {
        try {
            sendData(context);
        } catch (Exception e) {
            Log.w(LOGGER_PREFIX + "Error exporting user data", e);
        }
    }

    private void sendData(Context context) throws Exception {
        Log.i(LOGGER_PREFIX + "Export Service started");
        List<GameManager> notExportedGames = GameManager.getNotExportedGameManagers();
        Log.i(LOGGER_PREFIX + "Not exported game records count: " + notExportedGames.size());
        if (notExportedGames.size() > 0) {
            Map<User, List<GameManager>> notExportedGamesForUser = new HashMap<User, List<GameManager>>();
            for (GameManager gameManager : notExportedGames) {
                if (gameManager.getUser() == null) {
                    Log.e("User is null " + gameManager.toString());
                    continue;
                }
                gameManager.loadGamePauses();
                if (notExportedGamesForUser.get(gameManager.getUser()) != null) {
                    notExportedGamesForUser.get(gameManager.getUser()).add(gameManager);
                } else {
                    List<GameManager> gamesForUser = new ArrayList<GameManager>();
                    gamesForUser.add(gameManager);
                    notExportedGamesForUser.put(gameManager.getUser(), gamesForUser);
                }
            }

            GsonBuilder gsonBuilder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
            if (PRETTY_FORMATED_OUTPUT) {
                gsonBuilder.setPrettyPrinting();
            }
            Gson gson = gsonBuilder.create();

            JsonObject jsonToSend = new JsonObject();

            // jsonToSend.addProperty(DEVICE_ID, Secure.getString(context.getContentResolver(), Secure.ANDROID_ID));
            jsonToSend.addProperty(DEVICE_ID, Build.SERIAL);
            jsonToSend.addProperty(DB_VERSION, ActiveAndroid.getDatabase().getVersion());
            jsonToSend.addProperty(EXPORT_TIME, System.currentTimeMillis());
            jsonToSend.addProperty(EXPORT_TYPE, Tablexia.GAME_EXPORT_TYPE.ordinal());
            jsonToSend.addProperty(LANGUAGE, context.getString(cz.nic.tablexia.R.string.language));

            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            jsonToSend.addProperty(VERSION, pInfo.versionCode);

            // add to root object all users with unexported games
            JsonArray userElements = new JsonArray();
            for (User user : notExportedGamesForUser.keySet()) {
                JsonElement userElement = gson.toJsonTree(user);
                userElement.getAsJsonObject().addProperty(REMOTE_ID, user.getId());
                userElement.getAsJsonObject().add(GAMES, gson.toJsonTree(notExportedGamesForUser.get(user)));
                userElements.add(userElement);
            }
            jsonToSend.add(USERS, userElements);

            String json = jsonToSend.toString();
            Log.i("Request: " + json);
            sendJson(json);
            setGamesAsExported(notExportedGames);
        } else {
            Log.i(LOGGER_PREFIX + "All games already exported. Nothing to send");
        }
    }

    private void setGamesAsExported(List<GameManager> exportedGameManagers) {
        for (GameManager gameManager : exportedGameManagers) {
            gameManager.setExportedAndSave(true);
        }
    }

    private void sendJson(String json) {
        try {
            HttpClient client = getHttpClient();
            // HttpConnectionParams.setConnectionTimeout(client.getParams(), Properties.HTTP_CONNECTION_TIMEOUT);
            // HttpConnectionParams.setSoTimeout(client.getParams(), Properties.HTTP_RESPONSE_TIMEOUT);

            String url = EXPORT_URL;
            HttpPost request = new HttpPost();
            request.setURI(new URI(url));

            // say its json
            request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/x-www-form-urlencoded");
            // add json entity
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
            BasicNameValuePair stats = new BasicNameValuePair(STATS, json);
            nameValuePairList.add(stats);
            request.setEntity(new UrlEncodedFormEntity(nameValuePairList, HTTP.UTF_8));

            HttpResponse httpResponse = client.execute(request);
            Log.i("Response : " + httpResponse.getStatusLine().getStatusCode() + " " + httpResponse.getStatusLine().getReasonPhrase());

            if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                // String result = EntityUtils.toString(httpResponse.getEntity());
                // Log.i("Result entity : " + result);

                String path = context.getExternalCacheDir().getAbsolutePath() + "/errortxt" + System.currentTimeMillis();
                Log.i("Error file : " + path);
                FileOutputStream fos = new FileOutputStream(new File(path));
                httpResponse.getEntity().writeTo(fos);
                httpResponse.getEntity().consumeContent();
                fos.flush();
                fos.close();

                throw new IllegalStateException("Status: " + httpResponse.getStatusLine().getStatusCode());
            } else {
                String result = EntityUtils.toString(httpResponse.getEntity());
                Log.i("Result entity : " + result);

            }
        } catch (Exception e) {
            throw new IllegalStateException("Unable to send data " + e.getMessage());
        }
    }

    protected HttpClient getHttpClient() {
        DefaultHttpClient client = new DefaultHttpClient();

        HostnameVerifier hostnameVerifier = SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
        SchemeRegistry registry = new SchemeRegistry();
        SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
        socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
        registry.register(new Scheme("https", socketFactory, 443));
        SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
        client = new DefaultHttpClient(mgr, client.getParams());

        return client;
    }

}
