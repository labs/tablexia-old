/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import cz.nic.tablexia.menu.MenuEnum;
import cz.nic.tablexia.menu.usermenu.User;

@Table(name = "TablexiaState")
public class TablexiaState extends Model {

    @Column(name = "ActiveUser")
    private User     activeUser;
    @Column(name = "ActiveMenu")
    private MenuEnum activeMenu;
    @Column(name = "ExpandedMenu")
    private int      expandedMenu;

    public TablexiaState() {
        super();
    }

    public User getActiveUser() {
        return activeUser;
    }

    public void setActiveUser(User activeUser) {
        this.activeUser = activeUser;
    }

    public void setAndSaveActiveUser(User activeUser) {
        this.activeUser = activeUser;
        save();
    }

    public MenuEnum getActiveMenu() {
        return activeMenu;
    }

    public void setActiveMenu(MenuEnum activeMenu) {
        this.activeMenu = activeMenu;
    }

    public void setAndSaveActiveMenu(MenuEnum activeMenu) {
        this.activeMenu = activeMenu;
        save();
    }

    public int getExpandedMenu() {
        return expandedMenu;
    }

    public void setExpandedMenu(int expandedMenu) {
        this.expandedMenu = expandedMenu;
    }

    public void setAndSaveExpandedMenu(int expandedMenu) {
        this.expandedMenu = expandedMenu;
        save();
    }

    public static TablexiaState getTablexiaState(boolean restore) {
        TablexiaState state = null;
        if (restore) {
            state = (new Select()).from(TablexiaState.class).executeSingle();
        }
        if (state == null) {
            state = new TablexiaState();
        }
        return state;
    }

}
