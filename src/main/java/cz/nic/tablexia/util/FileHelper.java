/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.util;

import java.io.File;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Environment;

/**
 * @author lubos.horacek
 */
public class FileHelper {

    private static final String DISK_CACHE_IMAGES_DIR = "images";

    public static File getImageFileDir(Context context) {
        return getFileDir(context, DISK_CACHE_IMAGES_DIR);
    }

    /**
     * try get external storage cache path or fallback to internal storage
     * 
     * @param context
     * @param uniqueName
     * @return
     */
    @SuppressLint("NewApi")
    private static File getFileDir(Context context, String subdir) {
        // try getting external storage
        String cachePath = context.getFilesDir().getPath();
        if (Build.VERSION.SDK_INT >= 9) {
            if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
                cachePath = context.getExternalFilesDir(DISK_CACHE_IMAGES_DIR).getPath();
            }
        }
        File cacheFile = new File(cachePath + File.separator + subdir);
        cacheFile.mkdirs();
        return cacheFile;
    }
}
