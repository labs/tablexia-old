/**
 * 
 */

package cz.nic.tablexia.util;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.util.Log;
import cz.nic.tablexia.Tablexia;

/**
 * @author lhoracek
 */
public class MediaPlayerHelper {

    private static Map<String, MediaPlayer> playing = new ConcurrentHashMap<String, MediaPlayer>();

    public static void muteAll() {
        Set<String> toRemove = playing.keySet();
        for (String play : toRemove) {
            stopSound(play);
        }
    }

    public static synchronized void stopSound(String sound) {
        if (playing.keySet().contains(sound)) {
            MediaPlayer play = playing.get(sound);
            if (play.isPlaying()) {
                play.stop();
            }
            play.release();
            playing.remove(sound);
        }
    }

    public static void playSound(String sound, Context context) {
        playSound(sound, false, false, context);
    }

    public static void playSound(String sound, boolean muteAll, Context context) {
        playSound(sound, muteAll, false, context);
    }

    public static void playSound(final String sound, boolean muteAll, boolean ambient, Context context) {
        if (muteAll) {
            muteAll();
        }
        if ((context != null) && (context.getApplicationContext() != null)) {
            Tablexia tablexia = (Tablexia) context.getApplicationContext();

            try {
                final MediaPlayer mp = new MediaPlayer();
                AssetFileDescriptor afd = tablexia.getZipResourceFile().getAssetFileDescriptor(sound);
                mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                mp.prepare();
                playing.put(sound, mp);
                if (ambient) {
                    mp.setLooping(true);
                } else {
                    mp.setOnCompletionListener(new OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            mp.release();
                            playing.remove(sound);
                        }
                    });
                }
                mp.start();

            } catch (final IOException e) {
                Log.e(MediaPlayerHelper.class.getSimpleName(), "Error loading sound " + e.getMessage() + " - resource: " + sound, e);
            }
        }
    }
}
