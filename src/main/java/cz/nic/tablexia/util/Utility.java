/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.util;

import java.lang.reflect.Field;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.view.LayoutInflater;
import android.view.View;

import com.activeandroid.Cache;
import com.activeandroid.Model;
import com.activeandroid.TableInfo;

/**
 * Utility Class
 * 
 * @author Matyáš Latner
 */
public class Utility {

    /**
     * Creates a new view if its needed.
     * 
     * @param itemView
     * @param itemViewId
     * @return recycled or new user item view
     */
    public static View prepareItemView(Context context, View itemView, int itemViewId) {
        // Pri smazani uzivatele muze but convertView inflatovane z jineho view nez aktualni radek predpoklada
        // if (itemView == null) {
        LayoutInflater inflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        itemView = inflater.inflate(itemViewId, null);
        // }
        return itemView;
    }

    /**
     * Return <code>true</code> if current screen size is same as given screen size
     * 
     * @param context
     * @param screenSize screen size for compare with current screen
     * @return <code>true</code> if current screen size is same as given screen size
     */
    public static boolean isScreenSize(Context context, int screenSize) {
        return ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == screenSize);
    }
    
    /**
     * Check if trophy with specified name is set in class from parameter.
     *
     * @param clazz class of trophy container
     * @param fieldName name of trophy
     * @return <code>true</code> if trophy with given name is set
     */
    public static boolean isSetTrophyForName(Model trophyContainer, String fieldName) {
        TableInfo tableInfo = Cache.getTableInfo(trophyContainer.getClass());
        for (Field field : tableInfo.getFields()) {
            if (field.getName().equals(fieldName)) {
                try {
                    return field.getBoolean(trophyContainer);
                } catch (Exception e) {
                    // nothing needed
                }
            }
        }
        return false;
    }
    
    /**
     * Set trophy with given name to class specific in parameter.
     * 
     * @param clazz class of trophy container
     * @param fieldName name of trophy
     */
    public static void setTrophyForName(Model trophyContainer, String fieldName) {
        TableInfo tableInfo = Cache.getTableInfo(trophyContainer.getClass());
        for (Field field : tableInfo.getFields()) {
            if (field.getName().equals(fieldName)) {
                try {
                    field.setBoolean(trophyContainer, true);
                } catch (Exception e) {
                    // nothing needed
                }
            }
        }
    }
    
    /**
     * Show view in specific activity
     * 
     * @param activity activity where to find layer
     * @param layerId id of layer
     * @param animDuration duration of show animation
     * @param animatorListener for registering animation callbacks
     */
    public static void showView(Activity activity, int viewId, long animDuration, AnimatorListener animatorListener) {
        View view = activity.findViewById(viewId);
        if (view != null && view.getVisibility() == View.GONE) {
            view.setAlpha(0f);
            view.setVisibility(View.VISIBLE);
            view.animate().setDuration(animDuration).alpha(1f).setListener(animatorListener);
        } else {
            if (animatorListener != null) {                
                animatorListener.onAnimationStart(null);
                animatorListener.onAnimationEnd(null);
            }
        }
    }
    
    /**
     * Hide view in specific activity
     * 
     * @param activity activity where to find layer
     * @param layerId id of layer
     * @param animDuration duration of hide animation
     * @param animatorListener for registering animation callbacks
     */
    public static void hideView(Activity activity, int viewId, long animDuration, final AnimatorListener animatorListener) {
        final View view = activity.findViewById(viewId);
        if (view != null && view.getVisibility() == View.VISIBLE) {            
            view.animate().setDuration(animDuration).alpha(0f).setListener(new AnimatorListenerAdapter() {
                
                @Override
                public void onAnimationCancel(Animator animation) {
                    if (animatorListener != null) {
                        animatorListener.onAnimationCancel(animation);
                    }
                }
                
                @Override
                public void onAnimationRepeat(Animator animation) {
                    if (animatorListener != null) {                        
                        animatorListener.onAnimationRepeat(animation);
                    }
                }
                
                @Override
                public void onAnimationStart(Animator animation) {
                    if (animatorListener != null) {                        
                        animatorListener.onAnimationStart(animation);
                    }
                }
                
                @Override
                public void onAnimationEnd(Animator animation) {
                    view.setVisibility(View.GONE);
                    if (animatorListener != null) {                        
                        animatorListener.onAnimationEnd(animation);
                    }
                }
                
            });
        } else {
            if (animatorListener != null) {                
                animatorListener.onAnimationStart(null);
                animatorListener.onAnimationEnd(null);
            }
        }
    }
    
    /**
     * Show layer in specific activity
     * 
     * @param activity activity where to find layer
     * @param layerId id of layer
     * @param animDuration duration of show animation
     * @param animatorListener for registering animation callbacks
     */
    public static void showLayer(Activity activity, int layerId, long animDuration, AnimatorListener animatorListener) {
        View layer = activity.findViewById(layerId);
        if (layer != null && layer.getVisibility() == View.GONE) {
            layer.setClickable(true);
            layer.setAlpha(0f);
            layer.setVisibility(View.VISIBLE);
            layer.animate().setDuration(animDuration).alpha(1f).setListener(animatorListener);
        } else {
            if (animatorListener != null) {                
                animatorListener.onAnimationStart(null);
                animatorListener.onAnimationEnd(null);
            }
        }
    }
    
    /**
     * Hide layer in specific activity
     * 
     * @param activity activity where to find layer
     * @param layerId id of layer
     * @param animDuration duration of hide animation
     * @param animatorListener for registering animation callbacks
     */
    public static void hideLayer(Activity activity, int layerId, long animDuration, final AnimatorListener animatorListener) {
        final View layer = activity.findViewById(layerId);
        if (layer != null && layer.getVisibility() == View.VISIBLE) {            
            layer.animate().setDuration(animDuration).alpha(0f).setListener(new AnimatorListenerAdapter() {
                
                @Override
                public void onAnimationCancel(Animator animation) {
                    if (animatorListener != null) {
                        animatorListener.onAnimationCancel(animation);
                    }
                }
                
                @Override
                public void onAnimationRepeat(Animator animation) {
                    if (animatorListener != null) {
                        animatorListener.onAnimationRepeat(animation);
                    }
                }
                
                @Override
                public void onAnimationStart(Animator animation) {
                    if (animatorListener != null) {
                        animatorListener.onAnimationStart(animation);
                    }
                }
                
                @Override
                public void onAnimationEnd(Animator animation) {
                    layer.setClickable(false);
                    layer.setVisibility(View.GONE);
                    if (animatorListener != null) {                        
                        animatorListener.onAnimationEnd(animation);
                    }
                }
                
            });
        } else {
            if (animatorListener != null) {                
                animatorListener.onAnimationStart(null);
                animatorListener.onAnimationEnd(null);
            }
        }
    }
    
    public static boolean isLayerHidden(Activity activity, int layerId) {
        final View layer = activity.findViewById(layerId);
        if (layer != null && layer.getVisibility() == View.VISIBLE) {
            return true;
        } else {
            return false;
        }
    }

}
