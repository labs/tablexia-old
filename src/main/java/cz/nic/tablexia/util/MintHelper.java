/**
 *
 */

package cz.nic.tablexia.util;

import android.content.Context;

import com.splunk.mint.Mint;

import cz.nic.tablexia.Tablexia;

/**
 * @author lhoracek
 */
public class MintHelper {
    public static void startBugSense(Context context) {
        if (Tablexia.BUGSENSE_ENABLED) {
            Mint.initAndStartSession(context, Tablexia.BUGSENSE_KEY);
        }
    }
}
