package cz.nic.tablexia.util;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;

import android.util.Log;

public class PauseableTimerHandler extends TimerHandler {
    private static final String TAG    = PauseableTimerHandler.class.getSimpleName();
    private boolean mPause = false;

    public PauseableTimerHandler(float pTimerSeconds, boolean pAutoReset,
    ITimerCallback pTimerCallback) {
        super(pTimerSeconds, pAutoReset, pTimerCallback);
    }

    public void pause() {
        Log.d(TAG, "PAUSEABLE HANDLER PAUSE");
        mPause = true;
    }

    public void resume() {
        Log.d(TAG, "PAUSEABLE HANDLER RESUME");
        mPause = false;
    }

    public boolean isPaused() {
        return mPause;
    }

    @Override
    public void onUpdate(float pSecondsElapsed) {
        if (!mPause) {
            super.onUpdate(pSecondsElapsed);
        }
    }
}