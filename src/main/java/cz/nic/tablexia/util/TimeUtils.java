
package cz.nic.tablexia.util;

import java.util.Calendar;

public class TimeUtils {
    public static boolean isWinter() {
        Calendar c = Calendar.getInstance();
        return ((c.get(Calendar.MONTH) > 10) || (c.get(Calendar.MONTH) < 2));
    }
}
