/**
 * 
 */

package cz.nic.tablexia.util;

/**
 * @author lhoracek
 */
public class ScalingHelper {

    public static float getHeightFromWidth(float originalWith, float originalHeight, float newWidth) {
        float scale = originalWith / originalHeight;
        float newHeight = (newWidth / scale);
        return newHeight;
    }

    public static float getScale(float originalSize, float newSize) {
        float scale = newSize / originalSize;
        return scale;
    }
}
