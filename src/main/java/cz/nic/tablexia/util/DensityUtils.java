
package cz.nic.tablexia.util;

import android.app.Activity;
import android.util.DisplayMetrics;

public class DensityUtils {
    /**
     * Spočítá, zda je nutné aplikovat šetření s pamětí.
     *
     * @param activity
     * @return
     */
    public static boolean lowMemorySettings(Activity activity) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        long size = metrics.widthPixels * metrics.heightPixels;
        long bytes = size * 4;
        long pages = (bytes * 6 * 3 * 5) / 3;

        Runtime rt = Runtime.getRuntime();
        long maxMemory = rt.maxMemory();

        return ((maxMemory / 3) * 2) < pages;
    }
}
