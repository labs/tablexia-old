/**
 *
 */

package cz.nic.tablexia.util;

import java.io.File;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Environment;
import android.util.Log;
import cz.nic.tablexia.BuildConfig;

/**
 * @author lhoracek
 */
public class ObbHelper {
    private static final String TAG = ObbHelper.class.getSimpleName();

    public static String getExpansionFileName(Context context, boolean mainFile) {
        try {
            int versionCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
            if (BuildConfig.DEBUG) {
                // main.TEST.package.obb - testing without versions interfering
                String filenameTest = Environment.getExternalStorageDirectory().toString() + File.separator + (mainFile ? "main." : "patch.") + "test." + context.getPackageName() + ".obb";
                Log.v(TAG, "Checking existence: " + filenameTest);
                if (expansionFileExists(filenameTest)) {
                    return filenameTest;
                }
            }

            for (int i = versionCode; i >= 1; i--) {
                // main.XXX.package.obb - production file placement
                String filename = getExpansionFilePath(context) + (mainFile ? "main." : "patch.") + i + "." + context.getPackageName() + ".obb";
                Log.v(TAG, "Checking existence: " + filename);
                if (expansionFileExists(filename)) {
                    return filename;
                }
            }

            return null;
        } catch (NameNotFoundException e) {
            Log.e(TAG, "Error getting version number");
            return null;
        }
    }

    private static String getExpansionFilePath(Context context) {
        return Environment.getExternalStorageDirectory().toString() + File.separator + "Android" + File.separator + "obb" + File.separator + context.getPackageName() + File.separator;
    }

    private static boolean expansionFileExists(String filename) {

        File fileForNewFile = new File(filename);
        if (fileForNewFile.exists()) {
            return true;
        }
        return false;
    }
}
