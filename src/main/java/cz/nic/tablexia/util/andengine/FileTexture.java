
package cz.nic.tablexia.util.andengine;

import java.io.IOException;

import org.andengine.opengl.texture.ITextureStateListener;
import org.andengine.opengl.texture.PixelFormat;
import org.andengine.opengl.texture.Texture;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.bitmap.BitmapTextureFormat;
import org.andengine.opengl.util.GLState;
import org.andengine.util.exception.NullBitmapException;
import org.andengine.util.math.MathUtils;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;

/**
 * @author lhoracek
 */
public class FileTexture extends Texture {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private final int                 mWidth;
    private final int                 mHeight;
    private final String              path;
    private final BitmapTextureFormat mBitmapTextureFormat;

    // ===========================================================
    // Constructors
    // ===========================================================

    public FileTexture(final TextureManager pTextureManager, final String path) {
        this(pTextureManager, path, BitmapTextureFormat.RGBA_8888, TextureOptions.DEFAULT, null);
    }

    private FileTexture(final TextureManager pTextureManager, final String path, final BitmapTextureFormat pBitmapTextureFormat, final TextureOptions pTextureOptions, final ITextureStateListener pTextureStateListener) {
        super(pTextureManager, pBitmapTextureFormat.getPixelFormat(), pTextureOptions, pTextureStateListener);

        this.path = path;
        mBitmapTextureFormat = pBitmapTextureFormat;

        final BitmapFactory.Options decodeOptions = new BitmapFactory.Options();
        decodeOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(path, decodeOptions);

        mWidth = decodeOptions.outWidth;
        mHeight = decodeOptions.outHeight;
    }

    @Override
    public int getWidth() {
        return mWidth;
    }

    @Override
    public int getHeight() {
        return mHeight;
    }

    @Override
    protected void writeTextureToHardware(final GLState pGLState) throws IOException {
        final Config bitmapConfig = mBitmapTextureFormat.getBitmapConfig();
        final Bitmap bitmap = onGetBitmap(bitmapConfig);

        if (bitmap == null) {
            throw new NullBitmapException("Caused by: '" + toString() + "'.");
        }

        final boolean useDefaultAlignment = MathUtils.isPowerOfTwo(bitmap.getWidth()) && MathUtils.isPowerOfTwo(bitmap.getHeight()) && (mPixelFormat == PixelFormat.RGBA_8888);
        if (!useDefaultAlignment) {
            /* Adjust unpack alignment. */
            GLES20.glPixelStorei(GLES20.GL_UNPACK_ALIGNMENT, 1);
        }

        final boolean preMultipyAlpha = mTextureOptions.mPreMultiplyAlpha;
        if (preMultipyAlpha) {
            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
        } else {
            pGLState.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0, mPixelFormat);
        }

        if (!useDefaultAlignment) {
            /* Restore default unpack alignment. */
            GLES20.glPixelStorei(GLES20.GL_UNPACK_ALIGNMENT, GLState.GL_UNPACK_ALIGNMENT_DEFAULT);
        }

        bitmap.recycle();
    }

    protected Bitmap onGetBitmap(final Config pBitmapConfig) {
        final BitmapFactory.Options decodeOptions = new BitmapFactory.Options();
        decodeOptions.inPreferredConfig = pBitmapConfig;
        decodeOptions.inDither = false;
        decodeOptions.inInputShareable = true;
        decodeOptions.inPurgeable = true;
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        return bitmap;
    }
}
