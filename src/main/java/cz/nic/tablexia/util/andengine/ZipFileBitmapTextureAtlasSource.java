/**
 * 
 */

package cz.nic.tablexia.util.andengine;

import java.io.IOException;
import java.io.InputStream;

import org.andengine.opengl.texture.atlas.bitmap.source.FileBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.source.BaseTextureAtlasSource;
import org.andengine.util.BitmapUtils;
import org.andengine.util.StreamUtils;
import org.andengine.util.debug.Debug;
import org.andengine.util.system.SystemUtils;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.os.Build;

import com.android.vending.expansion.zipfile.ZipResourceFile;

/**
 * @author lhoracek
 */
public class ZipFileBitmapTextureAtlasSource extends BaseTextureAtlasSource implements IBitmapTextureAtlasSource {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private final ZipResourceFile mZipResourceFile;
    private final String          mResourcePath;

    // ===========================================================
    // Constructors
    // ===========================================================

    public static ZipFileBitmapTextureAtlasSource create(final ZipResourceFile pFile, final String path) {
        return ZipFileBitmapTextureAtlasSource.create(pFile, path, 0, 0);
    }

    public static ZipFileBitmapTextureAtlasSource create(final ZipResourceFile pFile, final String path, final int pTextureX, final int pTextureY) {
        final BitmapFactory.Options decodeOptions = new BitmapFactory.Options();
        decodeOptions.inJustDecodeBounds = true;

        InputStream in = null;
        try {
            in = pFile.getInputStream(path);
            BitmapFactory.decodeStream(in, null, decodeOptions);
        } catch (final IOException e) {
            Debug.e("Failed loading Bitmap in " + FileBitmapTextureAtlasSource.class.getSimpleName() + ". File: " + pFile, e);
        } finally {
            StreamUtils.close(in);
        }

        return new ZipFileBitmapTextureAtlasSource(pFile, path, pTextureX, pTextureY, decodeOptions.outWidth, decodeOptions.outHeight);
    }

    ZipFileBitmapTextureAtlasSource(final ZipResourceFile pFile, final String path, final int pTextureX, final int pTextureY, final int pTextureWidth, final int pTextureHeight) {
        super(pTextureX, pTextureY, pTextureWidth, pTextureHeight);

        mZipResourceFile = pFile;
        mResourcePath = path;
    }

    @Override
    public ZipFileBitmapTextureAtlasSource deepCopy() {
        return new ZipFileBitmapTextureAtlasSource(mZipResourceFile, mResourcePath, mTextureX, mTextureY, mTextureWidth, mTextureHeight);
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    @Override
    public Bitmap onLoadBitmap(final Config pBitmapConfig) {
        return this.onLoadBitmap(pBitmapConfig, false);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public Bitmap onLoadBitmap(final Config pBitmapConfig, final boolean pMutable) {
        final BitmapFactory.Options decodeOptions = new BitmapFactory.Options();
        decodeOptions.inPreferredConfig = pBitmapConfig;
        decodeOptions.inDither = false;

        if (pMutable && SystemUtils.isAndroidVersionOrHigher(Build.VERSION_CODES.HONEYCOMB)) {
            decodeOptions.inMutable = pMutable;
        }

        InputStream in = null;
        try {
            in = mZipResourceFile.getInputStream(mResourcePath);
            final Bitmap bitmap = BitmapFactory.decodeStream(in, null, decodeOptions);

            if (pMutable) {
                return BitmapUtils.ensureBitmapIsMutable(bitmap);
            } else {
                return bitmap;
            }
        } catch (final IOException e) {
            Debug.e("Failed loading Bitmap in " + this.getClass().getSimpleName() + ". File: " + mResourcePath, e);
            return null;
        } finally {
            StreamUtils.close(in);
        }
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "(" + mResourcePath + ")";
    }

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
