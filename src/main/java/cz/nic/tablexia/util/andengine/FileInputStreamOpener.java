/**
 * 
 */

package cz.nic.tablexia.util.andengine;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.andengine.util.adt.io.in.IInputStreamOpener;

import android.util.Log;

/**
 * @author lhoracek
 */
public class FileInputStreamOpener implements IInputStreamOpener {
    private static final String TAG = FileInputStreamOpener.class.getSimpleName();
    private final File          file;

    public FileInputStreamOpener(File file) {
        super();
        this.file = file;
    }

    @Override
    public InputStream open() throws IOException {
        Log.i(TAG, "Opening stream for: " + file.getPath());
        return new BufferedInputStream(new FileInputStream(file));
    }
}
