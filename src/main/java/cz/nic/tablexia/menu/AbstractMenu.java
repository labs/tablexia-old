/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import cz.nic.tablexia.R;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.menu.mainmenu.screen.AbstractMenuFragment;

/**
 * Abstract menu control class.
 * 
 * @author Matyáš Latner
 */
public abstract class AbstractMenu {
    
    public interface MenuChangeListener {
        
        void onMenuChanged(AbstractMenuFragment abstractMenuFragment);
        
    }

    private final static int MAIN_MENU_CONTAINER_ID = R.id.main_menu_container;

    public static final int  MAIN_MENU                = 0;
    public static final int  USER_MENU                = 1;

    protected Tablexia       tablexiaContext;

    public AbstractMenu(Tablexia tablexiaContext) {
        this.tablexiaContext = tablexiaContext;
    }

    /**
     * Return inflated view for current menu
     * 
     * @param inflater
     * @param container
     * @return
     */
    public abstract View getMenuView(LayoutInflater inflater, ViewGroup container);

    /**
     * Process menu initialization
     */
    public abstract void initMenu();

    /**
     * Initialize and show menu
     * 
     * @param fragment
     */
    public void showMenu(final MenuActivity menuActivity) {
        final FrameLayout container = ((FrameLayout) menuActivity.findViewById(MAIN_MENU_CONTAINER_ID));
        if (container != null) {
            container.post(new Runnable() {
                
                @Override
                public void run() {                    
                    container.removeAllViews();
                    
                    LayoutInflater inflater = (LayoutInflater) menuActivity.getTablexiaContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    container.addView(getMenuView(inflater, container));
                    
                    initMenu();
                }
            });
        }
    }

}
