/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.menu;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.view.View;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.game.games.GamesDefinition;
import cz.nic.tablexia.menu.AbstractMenu.MenuChangeListener;
import cz.nic.tablexia.menu.mainmenu.screen.AbstractMenuFragment;



public interface MenuActivity {

    /**
     * Lock menu drawer opened
     */
    public void lockOpenMenuDrawer();
    
    /**
     * Lock menu drawer closed
     */
    public void lockCloseMenuDrawer();

    /**
     * Unlock menu drawer
     */
    public void unlockMenuDrawer();

    /**
     * Closes menu drawer
     */
    public void closeMenuDrawer();
    
    /**
     * Adds new listener for menu drawer
     * 
     * @param drawerListener new listener
     */
    public void addMenuDrawerListener(DrawerListener drawerListener);
    
    /**
     * Removes listener from menu drawer
     * 
     * @param drawerListener listener for remove from
     */
    public void removeMenuDrawerListener(DrawerListener drawerListener);

    /**
     * Finds and returns {@link View} by id
     * 
     * @param viewId id of View
     * @return
     */
    public View findViewById(int viewId);

    /**
     * Returns Tablexia context
     * 
     * @return
     */
    public Tablexia getTablexiaContext();

    /**
     * Returns activity context
     * 
     * @return activity context
     */
    public Context getContext();

    /**
     * Show fragment from parameter in main screen container
     * If this fragment is already in use do nothing.
     * Returns displayed fragment.
     * 
     * @param mainMenuDefinition 
     * @param fragment
     * @param forceReplace
     * @param forceQuickTransaction <code>true</code> cancel fade animation
     * @return displayed fragment
     */
    public Fragment replaceScreenFragment(MenuEnum menuEnum, AbstractMenuFragment fragment, boolean forceReplace, boolean forceQuickTransition, MenuChangeListener menuChangeListener);

    /**
     * Return <code>true</code> if fragment have to be refreshed after menu load
     * 
     * @return <code>true</code> if fragment have to be refreshed after menu load
     */
    public boolean isForceMenuRefresh();
    
    /**
     * Redirection to menu page if is same as current
     * @return <code>true</code> if is forced redirection to current menu from this activity
     */
    public boolean isForceRedirectToCurrentMenu();
    
    /**
     * Return <code>true</code> if game definition is active now
     * @param gameDefinitionToCheck check if this game is active now
     * @return <code>true</code> if game definition is active now
     */
    public boolean isGameActive(GamesDefinition gameDefinitionToCheck);
    
    public Intent getIntent();
}
