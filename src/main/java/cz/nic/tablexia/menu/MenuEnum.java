/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.menu;

import cz.nic.tablexia.menu.AbstractMenu.MenuChangeListener;



public interface MenuEnum {

    /**
     * Show menu screen
     * 
     * @param menuActivity
     * @param forceReplace <code>true</code> for force of menu screen replace
     * @param parameter parameter for screen fragment
     * @param forceQuickTransaction <code>true</code> cancel fade animation
     * @return <code>true</code> if menu item has screen
     */
    boolean showMenuScreen(MenuActivity menuActivity, boolean forceReplace, Integer parameter, boolean forceQuickTransition, MenuChangeListener menuChangeListener);

    /**
     * Performs action assigned to menu item
     * 
     * @param menuActivity
     * @return <code>true</code> if method call was handled
     */
    boolean performMenuAction(MenuActivity menuActivity);

    /**
     * Position of current item
     * 
     * @return position
     */
    int ordinal();
}
