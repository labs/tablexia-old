/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.menu.mainmenu.screen;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cz.nic.tablexia.R;
import cz.nic.tablexia.audio.resources.PermanentSounds;
import cz.nic.tablexia.audio.resources.SpeechSounds;
import cz.nic.tablexia.game.games.GamesDefinition;
import cz.nic.tablexia.game.manager.GameManager;
import cz.nic.tablexia.menu.mainmenu.MainMenuDefinition;
import cz.nic.tablexia.menu.usermenu.User;
import cz.nic.tablexia.trophy.GameTrophy;
import cz.nic.tablexia.trophy.GameTrophyDefinition;
import cz.nic.tablexia.trophy.GameTrophyTypeDefinition;
import cz.nic.tablexia.trophy.UserTrophyDefinition;
import cz.nic.tablexia.util.Utility;
import cz.nic.tablexia.widget.TablexiaBannerImageDialog;
import cz.nic.tablexia.widget.TablexiaDialog;

/**
 * Hall of Fame screen fragment. Shows all user trophies.
 *
 * @author Matyáš Latner
 */
public class HallOfFame extends AbstractMenuFragment {

    /**
     * Defined boarder values and images for trophy heap in hall of fame
     *
     * @author Matyáš Latner
     */
    private static enum TrophyHeapBoarders {

        HALLOFFAME_TROPHYHEAP_00(R.drawable.screen_halloffame_trophyheap_00, 0), //
        HALLOFFAME_TROPHYHEAP_01(R.drawable.screen_halloffame_trophyheap_01, 1), //
        HALLOFFAME_TROPHYHEAP_02(R.drawable.screen_halloffame_trophyheap_02, 2), //
        HALLOFFAME_TROPHYHEAP_03(R.drawable.screen_halloffame_trophyheap_03, 3), //
        HALLOFFAME_TROPHYHEAP_04(R.drawable.screen_halloffame_trophyheap_04, 4), //
        HALLOFFAME_TROPHYHEAP_05(R.drawable.screen_halloffame_trophyheap_05, 5), //
        HALLOFFAME_TROPHYHEAP_06(R.drawable.screen_halloffame_trophyheap_06, 6), //
        HALLOFFAME_TROPHYHEAP_07(R.drawable.screen_halloffame_trophyheap_07, 7), //
        HALLOFFAME_TROPHYHEAP_08(R.drawable.screen_halloffame_trophyheap_08, 8), //
        HALLOFFAME_TROPHYHEAP_09(R.drawable.screen_halloffame_trophyheap_09, 9), //
        HALLOFFAME_TROPHYHEAP_10(R.drawable.screen_halloffame_trophyheap_10, 10), //
        HALLOFFAME_TROPHYHEAP_15(R.drawable.screen_halloffame_trophyheap_15, 15), //
        HALLOFFAME_TROPHYHEAP_20(R.drawable.screen_halloffame_trophyheap_20, 20), //
        HALLOFFAME_TROPHYHEAP_30(R.drawable.screen_halloffame_trophyheap_30, 30), //
        HALLOFFAME_TROPHYHEAP_35(R.drawable.screen_halloffame_trophyheap_35, 35), //
        HALLOFFAME_TROPHYHEAP_40(R.drawable.screen_halloffame_trophyheap_40, 40), //
        HALLOFFAME_TROPHYHEAP_50(R.drawable.screen_halloffame_trophyheap_50, 50), //
        HALLOFFAME_TROPHYHEAP_60(R.drawable.screen_halloffame_trophyheap_60, 60);

        private int imageId;
        private int starCount;

        private TrophyHeapBoarders(int imageId, int starCount) {
            this.imageId = imageId;
            this.starCount = starCount;
        }

        public int getImageId() {
            return imageId;
        }

        public int getStarCount() {
            return starCount;
        }

        public static int getTrophyHeapImageForStarCount(int starCount) {
            for (int i = TrophyHeapBoarders.values().length - 1; i >= 0; i--) {
                TrophyHeapBoarders trophyHeapBoarder = TrophyHeapBoarders.values()[i];
                if (starCount >= trophyHeapBoarder.getStarCount()) {
                    return trophyHeapBoarder.getImageId();
                }
            }
            return TrophyHeapBoarders.values()[0].getImageId();
        }
    }

    private static final int     BACKGROUND_TILE_REPEAT_COUNT = 37;
    private static final int     HALLOFFAME_BACKGROUND_TILE   = R.drawable.screen_halloffame_background_tile;
    private static final int     HALLOFFAME_BACKGROUND_START  = R.drawable.screen_halloffame_background_start;

    private static final int[]   MOUSE_TEXTS_EMPTY            = { R.string.trophy_mouse_0_1, R.string.trophy_mouse_0_2, R.string.trophy_mouse_0_3, R.string.trophy_mouse_0_4, R.string.trophy_mouse_0_5 };
    private static final int[]   MOUSE_TEXTS_1                = { R.string.trophy_mouse_1_1, R.string.trophy_mouse_1_2, R.string.trophy_mouse_1_3 };
    private static final int[]   MOUSE_TEXTS_4                = { R.string.trophy_mouse_4_1, R.string.trophy_mouse_4_2, R.string.trophy_mouse_4_3, R.string.trophy_mouse_4_4 };
    private static final int[]   MOUSE_TEXTS_9                = { R.string.trophy_mouse_9_1, R.string.trophy_mouse_9_2, R.string.trophy_mouse_9_3, R.string.trophy_mouse_9_4, R.string.trophy_mouse_9_5 };
    private static final int[]   MOUSE_TEXTS_13               = { R.string.trophy_mouse_13_1, R.string.trophy_mouse_13_2, R.string.trophy_mouse_13_3, R.string.trophy_mouse_13_4, R.string.trophy_mouse_13_5 };
    private static final int[]   MOUSE_TEXTS_16               = { R.string.trophy_mouse_16_1, R.string.trophy_mouse_16_2, R.string.trophy_mouse_16_3, R.string.trophy_mouse_16_4 };
    private static final int[]   MOUSE_TEXTS_20               = { R.string.trophy_mouse_20_1, R.string.trophy_mouse_20_2, R.string.trophy_mouse_20_3, R.string.trophy_mouse_20_4 };

    private HorizontalScrollView contentHorizontalScrollView;

    public HallOfFame() {
        super(R.layout.screen_halloffame, SpeechSounds.HALLOFFAME_INTRO);
    }

    @Override
    protected void playIntroSound() {
        if (getTablexiaContext().getSelectedUser().getPlayCountHallOfFame() < 3) {
            super.playIntroSound();
            getTablexiaContext().getSelectedUser().addPlayCountHallOfFame();
        }
    }

    @Override
    protected void asyncFragmentDataLoad(View layoutView) {
        contentHorizontalScrollView = (HorizontalScrollView) layoutView;
        prepareDoorButton(layoutView);
        prepareBackground(layoutView);
        prepareStars(layoutView);
        prepareTrophies(layoutView);
        prepareMouse(layoutView);
    }

    @Override
    public void onFragmentResume() {
        scrollToParameterPosition();
    }

    private void scrollToParameterPosition() {

        Bundle arguments = getArguments();
        if (arguments != null) {
            Integer trophyImageId = arguments.getInt(MainMenuDefinition.MAIN_MENU_SCREEN_PARAMETER_KEY);
            if (trophyImageId != null) {
                final ImageView trophyImage = (ImageView) contentHorizontalScrollView.findViewById(trophyImageId);
                final Point displaySize = new Point();
                getActivity().getWindowManager().getDefaultDisplay().getSize(displaySize);

                contentHorizontalScrollView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                    @SuppressWarnings("deprecation")
                    @SuppressLint("NewApi")
                    @Override
                    public void onGlobalLayout() {
                        scrollToPosition((trophyImage.getX() - (displaySize.x / 2)) + (trophyImage.getWidth() / 2));
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            contentHorizontalScrollView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        } else {
                            contentHorizontalScrollView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }

                    }
                });
            }
        }
    }

    private void scrollToPosition(final float position) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                contentHorizontalScrollView.smoothScrollTo((int) position, 0);
            }
        }, 250);
    }

    private void prepareMouse(View layoutView) {
        final ImageView mouseHoleImage = (ImageView) layoutView.findViewById(R.id.screen_halloffame_mouse);
        final TextView mouseHoleBubbleImage = (TextView) layoutView.findViewById(R.id.screen_halloffame_mouse_bubble);

        mouseHoleImage.setOnClickListener(new OnClickListener() {
            private Runnable hider = new Runnable() {
                @Override
                public void run() {
                    mouseHoleImage.setImageResource(R.drawable.screen_halloffame_mouse_unpressed);
                    mouseHoleBubbleImage.setVisibility(View.GONE);
                }
            };

            @Override
            public void onClick(View v) { // zobrazeni bubliny po kliku
                if (mouseHoleBubbleImage.getVisibility() == View.GONE) {
                    mouseHoleBubbleImage.setText(getString(getMouseTexts()[Math.min(mouseTextIndex++, getMouseTexts().length - 1)]));
                    mouseHoleBubbleImage.setVisibility(View.INVISIBLE); // nejdrive pridame neviditelne do layoutu
                    mouseHoleBubbleImage.post(new Runnable() { // po zmene parametru provedeme zviditelneni a nastaveni pozice. musi byt odlozeno, aby se mohly zmeny velikosti projevit
                        @Override
                        public void run() {
                            mouseHoleBubbleImage.setX((int) mouseHoleImage.getX() - mouseHoleBubbleImage.getWidth());
                            mouseHoleBubbleImage.setY((int) mouseHoleImage.getY() - mouseHoleBubbleImage.getHeight());
                            mouseHoleBubbleImage.setVisibility(View.VISIBLE);
                        }
                    });
                }

                mouseHoleImage.removeCallbacks(hider);
                mouseHoleImage.setImageResource(R.drawable.screen_halloffame_mouse_pressed);
                mouseHoleImage.postDelayed(hider, 4000);
            }
        });
    }

    private int mouseTextIndex = 0;

    private int[] getMouseTexts() {
        int trophyCount = getTrophyCount();
        int[] texts = null;
        if (trophyCount == 0) {
            texts = MOUSE_TEXTS_EMPTY;
        } else if (trophyCount < 4) {
            texts = MOUSE_TEXTS_1;
        } else if (trophyCount < 9) {
            texts = MOUSE_TEXTS_4;
        } else if (trophyCount < 13) {
            texts = MOUSE_TEXTS_9;
        } else if (trophyCount < 16) {
            texts = MOUSE_TEXTS_13;
        } else if (trophyCount < 20) {
            texts = MOUSE_TEXTS_16;
        } else {
            texts = MOUSE_TEXTS_20;
        }
        return texts;
    }

    private Integer trophyCountCache = null;

    private int getTrophyCount() {
        if (trophyCountCache == null) {
            User selectedUser = getTablexiaContext().getSelectedUser();
            Long selectedUserId = selectedUser.getId();

            int count = 0;
            for (GamesDefinition gameDefinition : GamesDefinition.getGames()) {
                GameTrophy gameTrophy = GameTrophy.getGameTrophyForUserAndGame(selectedUserId, gameDefinition.ordinal());
                for (GameTrophyTypeDefinition trophyTypeDefinition : GameTrophyTypeDefinition.values()) {
                    GameTrophyDefinition trophyDefinition = GameTrophyDefinition.getTrophyDefinitionByGameAndTrophyType(gameDefinition, trophyTypeDefinition);
                    if (trophyDefinition != null) {
                        if ((gameTrophy != null) && Utility.isSetTrophyForName(gameTrophy, trophyTypeDefinition.getDbFieldName())) {
                            count++;
                        }
                    }
                }

            }

            for (UserTrophyDefinition userTrophyDefinition : UserTrophyDefinition.values()) {
                if (Utility.isSetTrophyForName(selectedUser, userTrophyDefinition.getDbFieldName())) {
                    count++;
                }
            }
            trophyCountCache = count;
        }
        return trophyCountCache;
    }

    private void prepareStars(View layoutView) {
        int starCount = GameManager.getStarNumberForUser(getTablexiaContext().getSelectedUser().getId());

        ImageView starCounterImage = (ImageView) layoutView.findViewById(R.id.screen_halloffame_starcounter);
        starCounterImage.setImageResource(TrophyHeapBoarders.getTrophyHeapImageForStarCount(starCount));
        starCounterImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showStarsDialog();
            }
        });

        TextView starCounterText = (TextView) layoutView.findViewById(R.id.screen_halloffame_starcounter_text);
        starCounterText.setText("" + starCount);
        starCounterText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showStarsDialog();
            }
        });
    }

    private void showStarsDialog() {
        new TablexiaDialog() {

            @Override
            public View onCreateView(android.view.LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
                View view = super.onCreateView(inflater, container, savedInstanceState);

                LinearLayout ll = (LinearLayout) view.findViewById(R.id.tablexiadialog_content);
                for (GamesDefinition gameDefinition : GamesDefinition.getGames()) {
                    LinearLayout line = (LinearLayout) inflater.inflate(R.layout.screen_halloffame_stars_line, null);
                    TextView tvName = (TextView) line.findViewById(R.id.halloffame_stars_name);
                    tvName.setText(gameDefinition.getGameNameResourceId());
                    int gameStarNumber = GameManager.getStarNumberForUserGame(getTablexiaContext().getSelectedUser(), gameDefinition);
                    TextView tvStars = (TextView) line.findViewById(R.id.halloffame_stars_count);
                    tvStars.setText(String.valueOf(gameStarNumber));
                    ll.addView(line);
                }

                // vodorovna linka
                View lineView = new View(getActivity());
                lineView.setBackgroundResource(R.color.black);
                LinearLayout.LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, 1);
                ll.addView(lineView, lp);

                // součet celkem
                LinearLayout line = (LinearLayout) inflater.inflate(R.layout.screen_halloffame_stars_line, null);
                TextView tvName = (TextView) line.findViewById(R.id.halloffame_stars_name);
                tvName.setText(getString(R.string.trophy_count_sum));
                tvName.setTypeface(null, Typeface.BOLD);
                int allStarsNumber = GameManager.getStarNumberForUser(getTablexiaContext().getSelectedUser().getId());
                TextView tvStars = (TextView) line.findViewById(R.id.halloffame_stars_count);
                tvStars.setText(String.valueOf(allStarsNumber));

                ll.addView(line);

                return view;
            }

            @Override
            protected void prepareDialogContent(android.view.LayoutInflater inflater, ViewGroup contenView) {
            };

            @Override
            protected void onDialogClick(View v) {
                dismiss();
            }

        }.show(getActivity().getFragmentManager(), null);
    }

    private void prepareBackground(View layoutView) {
        ViewGroup background = (ViewGroup) layoutView.findViewById(R.id.screen_halloffame_content_background);

        ImageView doorBackground = new ImageView(getActivity());
        doorBackground.setImageResource(HALLOFFAME_BACKGROUND_START);
        doorBackground.setAdjustViewBounds(true);
        background.addView(doorBackground);

        for (int i = 0; i < BACKGROUND_TILE_REPEAT_COUNT; i++) {
            ImageView imageView = new ImageView(getActivity());
            imageView.setImageResource(HALLOFFAME_BACKGROUND_TILE);
            imageView.setAdjustViewBounds(true);
            background.addView(imageView);
        }
    }

    private void prepareTrophies(View layoutView) {
        User selectedUser = getTablexiaContext().getSelectedUser();
        Long selectedUserId = selectedUser.getId();
        RelativeLayout contentLayout = (RelativeLayout) layoutView.findViewById(R.id.screen_halloffame_content);

        for (GamesDefinition gameDefinition : GamesDefinition.getGames()) {
            GameTrophy gameTrophy = GameTrophy.getGameTrophyForUserAndGame(selectedUserId, gameDefinition.ordinal());
            for (GameTrophyTypeDefinition trophyTypeDefinition : GameTrophyTypeDefinition.values()) {
                GameTrophyDefinition trophyDefinition = GameTrophyDefinition.getTrophyDefinitionByGameAndTrophyType(gameDefinition, trophyTypeDefinition);
                if (trophyDefinition != null) {
                    ImageView trophyView = (ImageView) contentLayout.findViewById(trophyDefinition.getTrophyImageId());
                    if ((gameTrophy != null) && Utility.isSetTrophyForName(gameTrophy, trophyTypeDefinition.getDbFieldName())) {
                        trophyView.setImageResource(trophyDefinition.getFullImageResourceId());
                    }
                    int imageId = trophyDefinition.getSmallImageResourceId();
                    prepareTrophyClickListener(trophyView, Html.fromHtml(prepareGameTrophyDescription(gameDefinition, trophyDefinition)), imageId);
                }
            }

        }

        for (UserTrophyDefinition userTrophyDefinition : UserTrophyDefinition.values()) {
            ImageView trophyView = (ImageView) contentLayout.findViewById(userTrophyDefinition.getTrophyImageId());
            if (Utility.isSetTrophyForName(selectedUser, userTrophyDefinition.getDbFieldName())) {
                trophyView.setImageResource(userTrophyDefinition.getFullImageResourceId());
            }
            prepareTrophyClickListener(trophyView, Html.fromHtml(prepareUserTrophyDescription(userTrophyDefinition)), userTrophyDefinition.getSmallImageResourceId());
        }
    }

    private void prepareTrophyClickListener(View trophyView, final CharSequence text, final int trophyImageResourceId) {
        trophyView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                playSound(PermanentSounds.BUTTON_SOUND, false);
                showTrophyDialog(text, trophyImageResourceId);
            }

        });
    }

    private void showTrophyDialog(final CharSequence text, final int imageResourceId) {
        new TablexiaBannerImageDialog() {

            @Override
            protected CharSequence getTextCharSequence() {
                return text;
            }

            @Override
            protected int getImageResource() {
                return imageResourceId;
            }

            @Override
            protected int getBannerResource() {
                return R.drawable.dialog_banner_trofej;
            }

            @Override
            protected int getTextSize() {
                return 16;
            }

            @Override
            protected void onDialogClick(View v) {
                dismiss();
            }

        }.show(getActivity().getFragmentManager(), null);
    }

    /**
     * Prepares description for currently clicked user trophy
     *
     * @param trophyDefinition current user trophy
     * @return description for clicked trophy
     */
    private String prepareUserTrophyDescription(UserTrophyDefinition trophyDefinition) {
        Resources resources = getActivity().getResources();
        return makeTextBold(resources.getString(trophyDefinition.getNameResourceId())) + "<br/>" + resources.getString(trophyDefinition.getDescriptionResourceId());
    }

    /**
     * Prepares description for currently clicked game trophy
     *
     * @param gameDefinition current trophy game
     * @param trophyDefinition current game trophy
     * @return description for clicked trophy
     */
    private String prepareGameTrophyDescription(GamesDefinition gameDefinition, GameTrophyDefinition trophyDefinition) {
        Resources resources = getActivity().getResources();
        return makeTextBold(resources.getString(trophyDefinition.getNameResourceId())) + "<br/>" + resources.getString(trophyDefinition.getDescriptionResourceId()) + " " + resources.getString(gameDefinition.getGameNameResourceId());
    }

    private String makeTextBold(String text) {
        return "<b>" + text + "</b>";
    }

    private void prepareDoorButton(View layoutView) {
        final ImageButton imageButton = (ImageButton) layoutView.findViewById(R.id.screen_halloffame_closebutton);
        imageButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                imageButton.setSelected(true);
                getTablexiaContext().prepareMainMenu().changeMenuItem(MainMenuDefinition.START, true, false);
            }

        });
    }
}
