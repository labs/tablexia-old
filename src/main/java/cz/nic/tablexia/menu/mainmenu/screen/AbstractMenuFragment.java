/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.menu.mainmenu.screen;

import java.util.ArrayList;
import java.util.List;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.menu.MenuActivity;
import cz.nic.tablexia.menu.mainmenu.MainMenuDefinition;

/**
 * Class with common menu fragments functionality.
 *
 * @author Matyáš Latner
 */
public abstract class AbstractMenuFragment extends Fragment {

    public interface MenuFragmentVisibilityListener {
        void onFragmentContentVisible();
    }

    protected int                          layoutId;
    private String                         introSoundPath;
    private boolean                        inflateInUIThread;
    private MenuFragmentVisibilityListener fragmentVisibilityListener;
    protected boolean                      loadingComplete;
    protected boolean                      fragmentVisible;
    private List<String>                   loadedSounds;
    private FragmentDataPreloader          loaderTask;

    /* //////////////////////////////////////////// DATA PRELOADING */

    public void setFragmentVisibilityListener(MenuFragmentVisibilityListener fragmentVisibilityListener) {
        this.fragmentVisibilityListener = fragmentVisibilityListener;
    }

    protected void playIntroSound() {
        playLongSound(introSoundPath, false);
    }

    private class FragmentDataPreloader extends AsyncTask<Integer, Void, View> {

        @Override
        protected View doInBackground(final Integer... params) {
        	
        	View layoutView = null;

            // INTRO SOUND
            if (introSoundPath != null) {
                playIntroSound();
            }

            // LOADING LAYOUT
            if (!inflateInUIThread) {
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                layoutView = inflater.inflate(params[0], null);
            } else {
                layoutView = ((ViewGroup) getView()).getChildAt(0);
            }

            // LOADING FRAGMENT SPECIFIC DATA
            asyncFragmentDataLoad(layoutView);
            return layoutView;
        }

        @Override
        protected void onPostExecute(View layoutView) {

            if (!inflateInUIThread) {
                ViewGroup viewGroup = (ViewGroup) getView();
                viewGroup.addView(layoutView);
            }

            syncFragmentDataLoad(layoutView);
            fragmentDataLoadComplete();
        }

        @Override
        protected void onPreExecute() {
            // nothing needed
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // nothing needed
        }
    }

    protected void fragmentDataLoadComplete() {
        if (!loadingComplete) {
            loadingComplete = true;
            getTablexiaContext().showScreenContent(new AnimatorListenerAdapter() {

                @Override
                public void onAnimationEnd(Animator animation) {
                    fragmentContentVisible();
                    fragmentVisible = true;
                }

            });
            onFragmentResume();
        }
        loaderTask = null;
    }

    protected void fragmentContentVisible() {
        onFragmentContentVisible();
        if (fragmentVisibilityListener != null) {
            fragmentVisibilityListener.onFragmentContentVisible();
        }
    }

    /* //////////////////////////////////////////// CONSTRUCTORS */

    /**
     * Creates new MenuFragment with specific layout loaded in non UI thread
     *
     * @param layoutId fragment layout id
     * @param introSoundPath path of sound file with screen introduction speech
     */
    public AbstractMenuFragment(int layoutId, String introSoundPath) {
        this(layoutId, false, introSoundPath);
    }

    /**
     * Creates new MenuFragment with specific layout
     *
     * @param layoutId fragment layout id
     * @param inflateinUIThread <code>true</code> - layout is inflated in UI thread
     * @param introSoundPath path of sound file with screen introduction speech
     */
    public AbstractMenuFragment(int layoutId, boolean inflateinUIThread, String introSoundPath) {
        this.layoutId = layoutId;
        this.introSoundPath = introSoundPath;
        inflateInUIThread = inflateinUIThread;

        loadedSounds = new ArrayList<String>();

        loadingComplete = false;
        fragmentVisible = false;
    }

    /* //////////////////////////////////////////// TABLEXIA API ACCESS */

    protected Tablexia getTablexiaContext() {
        return ((MenuActivity) getActivity()).getTablexiaContext();
    }

    protected MenuActivity getMenuActivity() {
        return (MenuActivity) getActivity();
    }

    public void onBackPressed() {
        if (fragmentVisible) {
            getTablexiaContext().prepareMainMenu().changeMenuItem(MainMenuDefinition.START, false, false);
        }
    }

    /* //////////////////////////////////////////// TABLEXIA FRAGMENT LIFECYCLE */

    /**
     * Callback method for asynchronous data loading.
     * Method is call while is asynchronous data loading processed.
     * Method is call outside the UI thread.
     *
     * @param layoutView inflated fragment layout
     */
    protected void asyncFragmentDataLoad(View layoutView) {
        // adapter method
    }

    /**
     * Callback method for synchronous data loading.
     * Method is call after asynchronous data loading.
     * Method is call in the UI thread.
     *
     * @param layoutView inflated fragment layout
     */
    protected void syncFragmentDataLoad(View layoutView) {
        // adapter method
    }

    /**
     * Callback method replacement for standard android onResume method.
     * Method is call onResume but only if all data are loaded.
     * If data are not loaded yet, method will be call after loading.
     */
    public void onFragmentResume() {
        // adapter method
    }

    /**
     * Callback method replacement for standard android onResume method.
     * Is call after onFragmentResume when is screen completely faded-out
     * If data are not loaded yet, method will be call after loading.
     */
    public void onFragmentContentVisible() {
        // adapter method
    }

    /**
     * Return <code>true</code> if fragment is loaded
     *
     * @return <code>true</code> if fragment is loaded
     */
    public boolean isLoadingComplete() {
        return loadingComplete;
    }

    protected void startLoading() {
        loaderTask = new FragmentDataPreloader();
        loaderTask.execute(layoutId);
    }

    private void cancelLoading() {
        if (!loadingComplete && (loaderTask != null)) {
            loaderTask.cancel(true);
        }
    }

    /* //////////////////////////////////////////// ANDROID LIFECYCLE */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FrameLayout contentLayout = new FrameLayout(getActivity());
        // some layouts need to be inflated in UI view! (WebView)
        if (inflateInUIThread) {
            contentLayout.addView(inflater.inflate(layoutId, null));
        }
        return contentLayout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        startLoading();
    }

    @Override
    public void onResume() {
        Tracker easyTracker = EasyTracker.getInstance(getActivity());
        easyTracker.set(Fields.SCREEN_NAME, this.getClass().getSimpleName());
        easyTracker.send(MapBuilder.createAppView().build());

        super.onResume();
        reinitLoadedSounds();
        if (loadingComplete) {
            onFragmentResume();
            onFragmentContentVisible();
        }
    }

    @Override
    public void onPause() {
        getTablexiaContext().getGlobalSoundControl().stop(loadedSounds, null, true, true);
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        cancelLoading();
    }

    /* //////////////////////////////////////////// SOUNDS */

    private void reinitLoadedSounds() {
        for (String loadedSound : loadedSounds) {
            getTablexiaContext().getGlobalSoundControl().addSound(loadedSound);
        }
    }

    protected void preloadSound(String sound) {
        if (!loadedSounds.contains(sound)) {
            loadedSounds.add(sound);
            getTablexiaContext().getGlobalSoundControl().addSound(sound);
        }
    }

    protected void playSound(String sound, boolean muteOther) {
        if (!loadedSounds.contains(sound)) {
            loadedSounds.add(sound);
            getTablexiaContext().getGlobalSoundControl().addSound(sound);
        }
        getTablexiaContext().getGlobalSoundControl().playSound(sound, muteOther);
    }

    protected void playLongSound(String sound, boolean muteOther) {
        loadedSounds.add(sound);
        getTablexiaContext().getGlobalSoundControl().playMusic(sound, muteOther);
    }

    protected void muteAllSounds() {
        getTablexiaContext().getGlobalSoundControl().stop(loadedSounds, null, false, true);
    }
}
