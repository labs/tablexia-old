/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.menu.mainmenu;

import android.os.Bundle;
import android.util.Log;
import cz.nic.tablexia.R;
import cz.nic.tablexia.menu.AbstractMenu.MenuChangeListener;
import cz.nic.tablexia.menu.MenuActivity;
import cz.nic.tablexia.menu.MenuEnum;
import cz.nic.tablexia.menu.mainmenu.screen.AbstractMenuFragment;

/**
 * Tablexia main menu items
 *
 * @author Matyáš Latner
 */
public enum MainMenuDefinition implements MenuEnum {

    START(-1, -1, -1, -1, cz.nic.tablexia.menu.mainmenu.screen.GameMenu.class, null, false),
    PROFILE(-1, -1, -1, -1, cz.nic.tablexia.menu.mainmenu.screen.Profile.class, null, false),
    GAMES(0, R.string.mainmenu_games, R.drawable.mainmenu_icon_games, R.drawable.mainmenu_icon_games_pressed, null, null, false),
    HALL_OF_FAME(1, R.string.mainmenu_halloffame, R.drawable.mainmenu_icon_halloffame, R.drawable.mainmenu_icon_halloffame_pressed, cz.nic.tablexia.menu.mainmenu.screen.HallOfFame.class, null, true),
    STATISTICS(2, R.string.mainmenu_statistics, R.drawable.mainmenu_icon_statistics, R.drawable.mainmenu_icon_statistics_pressed, cz.nic.tablexia.menu.mainmenu.screen.Statistics.class, null, true),
    ENCYCLOPEDIA(3, R.string.mainmenu_encyclopedia, R.drawable.mainmenu_icon_encyclopedia, R.drawable.mainmenu_icon_encyclopedia_pressed, cz.nic.tablexia.menu.mainmenu.screen.Encyclopedia.class, null, true),
    ABOUT_APLICATION(4, R.string.mainmenu_aboutapp, R.drawable.mainmenu_icon_aboutapp, R.drawable.mainmenu_icon_aboutapp_pressed, cz.nic.tablexia.menu.mainmenu.screen.AboutAplication.class, null, true),
    LOGOUT(5, R.string.mainmenu_logout,
        R.drawable.mainmenu_icon_logout, R.drawable.mainmenu_icon_logout_pressed, null, cz.nic.tablexia.menu.mainmenu.action.Logout.class, false);
    //            LOADER(6, R.string.mainmenu_loader, -1, -1, cz.nic.tablexia.menu.mainmenu.fragment.LoaderFragment.class, null, true), //
    //    BALCONY(7, R.string.mainmenu_noviny, -1, -1, cz.nic.tablexia.newusers.fragment.AnimationNewUserFragment.class, null, true);

    public static final String              MAIN_MENU_SCREEN_PARAMETER_KEY = "mainMenuScreenParameter";

    private int                                         position;
    private int                                         menuTextResourceId;
    private int                                         menuIconResourceId;
    private int                                         menuIconSelectedResourceId;
    private Class<? extends AbstractMenuFragment>       screenFragment;
    private Class<? extends MainMenuAction>             menuAction;
    private boolean                                     closeMenu;

    private static int                                  count = 0;

    /**
     * Creates new main menu item. Assign to menu screen fragment or menu action.
     *
     * @param position
     *            position in menu
     * @param menuTextResourceId
     *            id of menu text resource
     * @param menuIconResourceId
     *            id of menu icon
     * @param menuIconSelectedResourceId
     *            id of menu selected icon
     * @param screenFragment
     *            class for menu screen fragment, if its <code>null</code> no screen is used
     * @param menuAction
     *            class with menu item action, if its <code>null</code> no menu action is performed on click
     * @param closeMenu
     *            <code>true</code> if menu close all expanded menu
     */
    private MainMenuDefinition(int position,
        int menuTextResourceId,
        int menuIconResourceId,
        int menuIconSelectedResourceId,
        Class<? extends AbstractMenuFragment> screenFragment,
        Class<? extends MainMenuAction> menuAction,
        boolean closeMenu) {

        this.position = position;
        this.menuTextResourceId = menuTextResourceId;
        this.menuIconResourceId = menuIconResourceId;
        this.menuIconSelectedResourceId = menuIconSelectedResourceId;
        this.screenFragment = screenFragment;
        this.menuAction = menuAction;
        this.closeMenu = closeMenu;
    }

    /**
     * Returns <code>true</code> if this menu item closes menu after click
     *
     * @return <code>true</code> if this menu item closes menu after click
     */
    public boolean isCloseMenu() {
        return closeMenu;
    }

    /**
     * Position of current item in menu
     *
     * @return menu position
     */
    public int getPosition() {
        return position;
    }

    /**
     * Menu icon resource of current item
     *
     * @return
     */
    public int getMenuIconResourceId() {
        return menuIconResourceId;
    }

    /**
     * Menu selected icon resource of current item
     *
     * @return
     */
    public int getMenuIconSelectedResourceId() {
        return menuIconSelectedResourceId;
    }

    /**
     * Menu text resource of current item
     *
     * @return
     */
    public int getMenuTextResourceId() {
        return menuTextResourceId;
    }

    @Override
    public boolean showMenuScreen(MenuActivity menuActivity, boolean forceReplace, Integer parameter, boolean forceQuickTransition, final MenuChangeListener menuChangeListener) {
        try {
            if (screenFragment != null) {
                AbstractMenuFragment fragment = screenFragment.newInstance();
                if (parameter != null) {
                    Bundle fragmentParameters = new Bundle();
                    fragmentParameters.putInt(MAIN_MENU_SCREEN_PARAMETER_KEY, parameter);
                    fragment.setArguments(fragmentParameters);
                }
                if (menuActivity.replaceScreenFragment(this, fragment, forceReplace, forceQuickTransition, menuChangeListener) != null) {
                    return true;
                }
            }
        } catch (Exception e) {
            Log.e(name(), "Cannot create fragment for menu itam: " + name(), e);
        }
        return false;
    }

    /**
     * Returns menu item for position from parameter or <code>null</code>
     *
     * @param position
     *            position of menu item
     * @return
     *         menu item for position or <code>null</code>
     */
    public static MainMenuDefinition getMenuItemAtPosition(int position) {
        for (MainMenuDefinition menuItem : MainMenuDefinition.values()) {
            if (menuItem.getPosition() == position) {
                return menuItem;
            }
        }
        return null;
    }

    /**
     * Return number of items in menu.
     *
     * @return number of items in menu
     */
    public static int getMenuSize() {
        if (count < 1) {
            for (MainMenuDefinition menuItem : MainMenuDefinition.values()) {
                if (menuItem.getPosition() >= 0) {
                    count++;
                }
            }
        }
        return count;
    }

    /**
     * Performs action assigned to menu item
     *
     * @return
     *         <code>true</code> if method call was handled
     */
    @Override
    public boolean performMenuAction(MenuActivity menuActivity) {
        if (menuAction != null) {
            try {
                return menuAction.newInstance().menuAction(menuActivity);
            } catch (Exception e) {
                Log.e(name(), "Cannot perform action for menu item: " + name(), e);
            }
        }
        return false;
    }

}
