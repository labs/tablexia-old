/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.menu.mainmenu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import cz.nic.tablexia.R;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.audio.SoundControl;
import cz.nic.tablexia.audio.resources.PermanentSounds;
import cz.nic.tablexia.game.GameActivity;
import cz.nic.tablexia.game.games.GamesDefinition;
import cz.nic.tablexia.main.MainActivity;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.MenuActivity;
import cz.nic.tablexia.menu.MenuEnum;
import cz.nic.tablexia.util.Utility;
import cz.nic.tablexia.widget.TablexiaDrawerLayout.DrawerListenerAdapter;

/**
 * Main menu control class.
 *
 * @author Matyáš Latner
 */
public class MainMenu extends AbstractMenu implements OnGroupClickListener, OnChildClickListener, OnItemSelectedListener, OnGroupCollapseListener, OnGroupExpandListener {

    private ExpandableListView mainMenuListView;
    private MainMenuAdapter    mainMenuAdapter;
    private CommonUserAdapter  userSpinnerAdapter;
    private Spinner            userSpinner;
    private SoundControl       soundControl;
    private boolean            menuClickableLock = true;
    private final Tablexia     tablexiaContext;

    public MainMenu(Tablexia tablexiaContext) {
        super(tablexiaContext);
        userSpinnerAdapter = new CommonUserAdapter(tablexiaContext);
        soundControl = tablexiaContext.getGlobalSoundControl();
        this.tablexiaContext = tablexiaContext;
    }

    @Override
    public View getMenuView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.mainmenu, container, false);
    }

    @Override
    public void initMenu() {

        showMenuToggleButton();

        userSpinner = (Spinner) tablexiaContext.getCurrentMenuActivity().findViewById(R.id.mainmenu_user_spinner);
        userSpinner.setAdapter(userSpinnerAdapter);
        userSpinner.setOnItemSelectedListener(this);

        mainMenuListView = (ExpandableListView) tablexiaContext.getCurrentMenuActivity().findViewById(R.id.main_menu);
        mainMenuAdapter = new MainMenuAdapter(tablexiaContext);
        mainMenuListView.setAdapter(mainMenuAdapter);
        mainMenuListView.setOnGroupClickListener(this);
        mainMenuListView.setOnChildClickListener(this);
        mainMenuListView.setOnGroupCollapseListener(this);
        mainMenuListView.setOnGroupExpandListener(this);

        refreshMainMenu();
    }

    private void showMenuToggleButton() {
        final View menuRightBorder = tablexiaContext.getCurrentMenuActivity().findViewById(R.id.mainmenu_rightborder);
        if (menuRightBorder.getVisibility() != View.VISIBLE) {
            menuRightBorder.setTranslationX(-menuRightBorder.getWidth());
            menuRightBorder.setVisibility(View.VISIBLE);
            menuRightBorder.animate().setInterpolator(new AccelerateInterpolator()).translationX(0);
        }
    }

    /* //////////////////////////////////////////// MENU STATES CONTROL */

    /**
     * Refresh main menu
     */
    public void refreshMainMenu() {
        refreshUserSpinner();
        MenuEnum activeMenu = tablexiaContext.getTablexiaState().getActiveMenu();
        if (activeMenu == null) {
            resetMenu();
        } else {

            Integer parameter = null;
            Intent intent = tablexiaContext.getCurrentMenuActivity().getIntent();
            if (intent != null) {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    parameter = extras.getInt(MainMenuDefinition.MAIN_MENU_SCREEN_PARAMETER_KEY);
                }
            }
            changeMenuItem(activeMenu, false, false, false, parameter, null);
            expandSelectedMenu();
            mainMenuAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Set start page
     */
    public void resetMenu() {
        resetMenu(false, true);
    }

    /**
     * Set start page
     */
    public void resetMenu(boolean forceReplace, boolean forceQuickTransition) {
        changeActiveMenuItem(null);
        changeMenuItem(MainMenuDefinition.START, forceReplace, true, forceQuickTransition, null, null);
        collapseAllMenu(tablexiaContext);
    }

    /**
     * Change selected menu and main screen if its needed
     *
     * @param menuItem menu item to change
     * @param forceReplace <code>true</code> for force of replace menu item
     * @param forceChange force menu change
     * @param redirectToCurrent <code>true</code> force redirection to menu page if its same as current
     */
    public boolean changeMenuItem(MenuEnum mainMenuItem, boolean forceReplace, boolean redirectToCurrent) {
        return changeMenuItem(mainMenuItem, forceReplace, redirectToCurrent, false, null, null);
    }

    /**
     * Change selected menu and main screen if its needed
     *
     * @param menuItem menu item to change
     * @param forceReplace <code>true</code> for force of replace menu item
     * @param forceChange force menu change
     * @param redirectToCurrent <code>true</code> force redirection to menu page if its same as current
     * @param forceQuickTransaction <code>true</code> cancel fade animation
     * @param parameter parameter for screen fragment
     */
    public boolean changeMenuItem(MenuEnum mainMenuItem, boolean forceReplace, boolean redirectToCurrent, boolean forceQuickTransition, Integer parameter, final MenuChangeListener menuChangeListener) {
        boolean result = false;
        MenuEnum activeMenu = tablexiaContext.getTablexiaState().getActiveMenu();
        if ((mainMenuItem != null) && (((activeMenu != mainMenuItem) || redirectToCurrent) || tablexiaContext.getCurrentMenuActivity().isForceMenuRefresh())) {
            // if menu item has screen -> remember as selected
            if (mainMenuItem.showMenuScreen(tablexiaContext.getCurrentMenuActivity(), forceReplace, parameter, forceQuickTransition, menuChangeListener)) {
                changeActiveMenuItem(mainMenuItem);
                result = true;
            }
            result = result || mainMenuItem.performMenuAction(tablexiaContext.getCurrentMenuActivity());
        }
        return result;
    }

    /**
     * Returns <code>true</code> if menu item in parameter is same as active menu item
     *
     * @param mainMenuItem menu item to check if is active
     * @return <code>true</code> if menu item in parameter is same as active menu item
     */
    public boolean isActiveMenuItem(MenuEnum mainMenuItem) {
        return tablexiaContext.getTablexiaState().getActiveMenu() == mainMenuItem;
    }

    /**
     * Returns selected menu item enum
     *
     * @return selected menu item enum
     */
    public MenuEnum getSelectedMenuItem() {
        return tablexiaContext.getTablexiaState().getActiveMenu();
    }

    /**
     * Sets active menu item.
     *
     * @param mainMenuItem menu item to set as active
     */
    public void changeActiveMenuItem(MenuEnum mainMenuItem) {
        tablexiaContext.getTablexiaState().setAndSaveActiveMenu(mainMenuItem);
        if (mainMenuAdapter != null) {
            mainMenuAdapter.selectGame(mainMenuItem);
        }
    }

    /**
     * Collapse all main menu group
     */
    @SuppressWarnings("unused")
    private void collapseAllMenu(Tablexia tablexiaContext) {
        if ((mainMenuListView != null) && (mainMenuAdapter != null)) {
            for (int i = 0; i < mainMenuAdapter.getGroupCount(); i++) {
                mainMenuListView.collapseGroup(i);
            }
        }
        tablexiaContext.getTablexiaState().setAndSaveExpandedMenu(-1);
    }

    /**
     * Try to expand actually selected menu
     */
    private void expandSelectedMenu() {
        int expandedMenuPosition = tablexiaContext.getTablexiaState().getExpandedMenu();
        if (expandedMenuPosition >= 0) {
            mainMenuListView.expandGroup(expandedMenuPosition);
        }
    }

    /**
     * Select in user spinner actually selected user
     */
    private void refreshUserSpinner() {
        userSpinnerAdapter.refresh();
        userSpinner.setSelection(userSpinnerAdapter.getUserPositionInList(tablexiaContext.getSelectedUser().getNickName()));
    }

    /* //////////////////////////////////////////// EXPAND AND COLLAPSE HANDLERS */

    /**
     * On group collapse handler
     */
    @Override
    public void onGroupCollapse(int groupPosition) {
        // expandSelectedMenu();
        tablexiaContext.getTablexiaState().setAndSaveExpandedMenu(-1);
    }

    /**
     * On group expand handler
     */
    @Override
    public void onGroupExpand(int groupPosition) {
        tablexiaContext.getTablexiaState().setAndSaveExpandedMenu(groupPosition);
    }

    /* //////////////////////////////////////////// CLICK HANDLERS */

    /**
     * On game menu click handler
     */
    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        if (menuClickableLock) {
            menuClickableLock = false;
            soundControl.playSound(PermanentSounds.BUTTON_SOUND, false);
            if (changeMenuItem(GamesDefinition.getGameAtPosition(childPosition), false, tablexiaContext.getCurrentMenuActivity().isForceRedirectToCurrentMenu())) {
                tablexiaContext.getCurrentMenuActivity().addMenuDrawerListener(new DrawerListenerAdapter() {

                    @Override
                    public void onDrawerClosed(View drawerView) {
                        tablexiaContext.getCurrentMenuActivity().removeMenuDrawerListener(this);
                        tablexiaContext.getCurrentMenuActivity().unlockMenuDrawer();
                        menuClickableLock = true;
                    }
                });
                tablexiaContext.getCurrentMenuActivity().lockCloseMenuDrawer();
            } else {
                menuClickableLock = true;
            }
        }
        return true;
    }

    /**
     * On menu click handler
     */
    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        boolean result = false;
        if (menuClickableLock) {
            menuClickableLock = false;
            soundControl.playSound(PermanentSounds.BUTTON_SOUND, false);
            // return true if is group collapsed else false because click have to be handled by expandable list
            result = mainMenuListView.collapseGroup(MainMenuDefinition.GAMES.getPosition());
            boolean closeMenu = changeMenuItem(MainMenuDefinition.getMenuItemAtPosition(groupPosition), false, false);
            if (closeMenu && MainMenuDefinition.getMenuItemAtPosition(groupPosition).isCloseMenu()) {
                tablexiaContext.getCurrentMenuActivity().addMenuDrawerListener(new DrawerListenerAdapter() {

                    @Override
                    public void onDrawerClosed(View drawerView) {
                        tablexiaContext.getCurrentMenuActivity().removeMenuDrawerListener(this);
                        tablexiaContext.getCurrentMenuActivity().unlockMenuDrawer();
                        menuClickableLock = true;
                    }
                });
                tablexiaContext.getCurrentMenuActivity().lockCloseMenuDrawer();
            } else {
                menuClickableLock = true;
            }
        }
        return result;
    }

    /**
     * On select user or on click new user handler
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position == userSpinnerAdapter.getUserCount()) {
            tablexiaContext.new LogoutDialog() {

                @Override
                public void positiveAction() {
                    // tablexiaContext.showNewUserScreen();
                    // TODO add code for starting animation on new user click from game menu while user logged in/home/lhoracek/Downloads/Prago Union - Vážná Hudba (2013)
                    Bundle args = new Bundle();
                    args.putString("NEWUSER_GAMEMENU", "NEWUSER_GAMEMENU");

                    // HACK to create new user from game
                    MenuActivity activity = tablexiaContext.getCurrentMenuActivity();
                    if (activity instanceof MainActivity) {
                        ((MainActivity) tablexiaContext.getCurrentMenuActivity()).showLoadingScreen(true, args);
                        ((MainActivity) tablexiaContext.getCurrentMenuActivity()).lockCloseMenuDrawer();
                    }

                    tablexiaContext.changeMenu(AbstractMenu.USER_MENU);
                    userSpinner.setSelection(userSpinnerAdapter.getUserPositionInList(tablexiaContext.getCurrentMenuActivity().getTablexiaContext().getSelectedUser().getNickName()));
                    tablexiaContext.getTablexiaState().setAndSaveActiveUser(null);

                    if (activity instanceof GameActivity) {
                        ((GameActivity) activity).setResult(MainActivity.RESULT_CODE_NEW_USER);
                        tablexiaContext.hardResetUser();
                    }

                    super.positiveAction();
                }

                @Override
                public void negativeAction() {
                    userSpinner.setSelection(userSpinnerAdapter.getUserPositionInList(tablexiaContext.getCurrentMenuActivity().getTablexiaContext().getSelectedUser().getNickName()));
                    super.negativeAction();
                };

            }.show(((Activity) tablexiaContext.getCurrentMenuActivity()).getFragmentManager(), null);

        } else {
            tablexiaContext.doSelectUser(userSpinnerAdapter.getUserAtPosition(position));
            ((TextView) view.findViewById(R.id.usermenu_text)).setTypeface(Typeface.DEFAULT);
            view.findViewById(R.id.usermenu_selector).setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // nothing needed
    }

    /* //////////////////////////////////////////// MAIN MENU ADAPTER */

    /**
     * Main menu adapter.
     *
     * @author Matyáš Latner
     */
    public class MainMenuAdapter extends BaseExpandableListAdapter {

        public static final int  CHILD_ID_START_VALUE            = 100;

        private static final int MAINMENU_GROUPITEM_ID           = R.layout.mainmenu_groupitem;
        private static final int MAINMENU_GROUPITEM_TEXT_ID      = R.id.mainmenu_groupitem_text;
        private static final int MAINMENU_GROUPITEM_IMAGE_ID     = R.id.mainmenu_groupitem_image;
        private static final int MAINMENU_GROUPITEM_SEPARATOR_ID = R.id.mainmenu_groupitem_separator;

        private static final int MAINMENU_CHILDITEM_ID           = R.layout.mainmenu_childitem;
        private static final int MAINMENU_CHILDITEM_TEXT_ID      = R.id.mainmenu_childitem_text;
        private static final int MAINMENU_CHILDITEM_TEXT2_ID     = R.id.mainmenu_childitem_text2;
        private static final int MAINMENU_CHILDITEM_SELECTOR_ID  = R.id.mainmenu_childitem_selector;
        private static final int MAINMENU_CHILDITEM_ARROW        = R.id.mainmenu_childitem_arrow;

        private final int        MAIN_MENU_GAMES_POSITION        = MainMenuDefinition.GAMES.getPosition();

        private Context          context;
        private View             selectedViewSelector;

        private MainMenuAdapter(Context context) {
            this.context = context;
            selectedViewSelector = null;
        }

        @Override
        public int getGroupCount() {
            return MainMenuDefinition.getMenuSize();
        }

        @Override
        public int getChildrenCount(int position) {
            return MAIN_MENU_GAMES_POSITION == position ? GamesDefinition.getGamesCount() : 0;
        }

        @Override
        public View getGroupView(final int position, boolean isExpand, View convertView, ViewGroup parent) {
            convertView = Utility.prepareItemView(context, convertView, MAINMENU_GROUPITEM_ID);

            TextView itemText = (TextView) convertView.findViewById(MAINMENU_GROUPITEM_TEXT_ID);
            itemText.setText(MainMenuDefinition.getMenuItemAtPosition(position).getMenuTextResourceId());

            // show menu selected/unselected icon
            int menuIconResourceId = -1;
            final ImageView itemImage = (ImageView) convertView.findViewById(MAINMENU_GROUPITEM_IMAGE_ID);
            if (isActiveMenuItem(MainMenuDefinition.getMenuItemAtPosition(position)) || (isActiveMenuItem(MainMenuDefinition.START) && (MainMenuDefinition.getMenuItemAtPosition(position) == MainMenuDefinition.GAMES)) || ((getSelectedMenuItem() instanceof GamesDefinition) && (MainMenuDefinition.getMenuItemAtPosition(position) == MainMenuDefinition.GAMES))) {

                menuIconResourceId = MainMenuDefinition.getMenuItemAtPosition(position).getMenuIconSelectedResourceId();
            } else {
                menuIconResourceId = MainMenuDefinition.getMenuItemAtPosition(position).getMenuIconResourceId();
            }
            if (menuIconResourceId > 0) {
                itemImage.setImageResource(menuIconResourceId);
            } else {
                itemImage.setImageBitmap(null);
            }

            convertView.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        itemImage.setImageResource(MainMenuDefinition.getMenuItemAtPosition(position).getMenuIconSelectedResourceId());
                    }
                    return false;
                }
            });

            // hide last menu item separator
            View separator = convertView.findViewById(MAINMENU_GROUPITEM_SEPARATOR_ID);
            if ((MainMenuDefinition.getMenuSize() - 1) == position) {
                separator.setVisibility(View.GONE);
            } else {
                separator.setVisibility(View.VISIBLE);
            }

            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            if (MAIN_MENU_GAMES_POSITION == groupPosition) {
                convertView = Utility.prepareItemView(context, convertView, MAINMENU_CHILDITEM_ID);
                GamesDefinition gameDefinition = GamesDefinition.getGameAtPosition(childPosition);

                TextView itemText = (TextView) convertView.findViewById(MAINMENU_CHILDITEM_TEXT_ID);
                itemText.setText(gameDefinition.getGameNameResourceId());

                TextView itemText2 = (TextView) convertView.findViewById(MAINMENU_CHILDITEM_TEXT2_ID);
                itemText2.setText(gameDefinition.getGameDescriptionResourceId());

                if (tablexiaContext.getCurrentMenuActivity().isGameActive(gameDefinition)) {
                    ((ImageView) convertView.findViewById(MAINMENU_CHILDITEM_ARROW)).setImageResource(R.drawable.mainmenu_childitem_restart);
                } else {
                    ((ImageView) convertView.findViewById(MAINMENU_CHILDITEM_ARROW)).setImageResource(R.drawable.mainmenu_childitem_arrow);
                }

                String viewTag = prepareGameItemTag(groupPosition, childPosition);
                View selector = convertView.findViewById(MAINMENU_CHILDITEM_SELECTOR_ID);
                selector.setTag(viewTag);
                if ((selectedViewSelector != null) && viewTag.equals(selectedViewSelector.getTag())) {
                    selector.setVisibility(View.VISIBLE);
                    selectedViewSelector = selector;
                }

                return convertView;
            }
            return null;
        }

        private String prepareGameItemTag(int groupPosition, int childPosition) {
            return groupPosition + "-" + childPosition;
        }

        public void selectGame(MenuEnum gameDefinition) {
            if (selectedViewSelector != null) {
                View selectedGameMenu = mainMenuListView.findViewWithTag(selectedViewSelector.getTag());
                if (selectedGameMenu != null) {
                    selectedGameMenu.setVisibility(View.INVISIBLE);
                }
            }
            if ((gameDefinition != null) && (gameDefinition instanceof GamesDefinition)) {
                View selector = mainMenuListView.findViewWithTag(prepareGameItemTag(MAIN_MENU_GAMES_POSITION, gameDefinition.ordinal()));
                if (selector != null) {
                    selectedViewSelector = selector;
                    selectedViewSelector.setVisibility(View.VISIBLE);
                }
            }
        }

        @Override
        public Object getChild(int arg0, int arg1) {
            return null;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            if (MAIN_MENU_GAMES_POSITION == groupPosition) {
                return CHILD_ID_START_VALUE + childPosition;
            }
            return 0;
        }

        @Override
        public Object getGroup(int position) {
            return null;
        }

        @Override
        public long getGroupId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int arg0, int arg1) {
            return true;
        }

    }
}
