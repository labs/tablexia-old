/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.menu.mainmenu.screen.encyclopedia;

import android.annotation.TargetApi;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import cz.nic.tablexia.BuildConfig;
import cz.nic.tablexia.Tablexia;

/**
 * Stranka encyklopedie
 * V args dostane jakou stranku ma zobrazit
 *
 * @author lhoracek
 */
public class EncyclopediaPage extends WebViewFragment {
    private static final String ENCYCLOPEDIA_BASE_DIR = "file:///android_asset/encyklopedie/";
    private MediaPlayer         mediaPlayer;
    private String              lastPlayedSound;

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected void syncFragmentDataLoad(View layoutView) {
        super.syncFragmentDataLoad(layoutView);
        mWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        mWebView.getSettings().setBuiltInZoomControls(false);
        mWebView.addJavascriptInterface(new JavascriptInterface(getTablexiaContext(), this), "JSInterface");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (BuildConfig.DEBUG) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
        }
    }

    @Override
    protected String getUrl() {
        String file = getArguments().getString(EncyclopediaPages.class.getName());
        return ENCYCLOPEDIA_BASE_DIR + file;
    }

    public void scaleView(float scaleValue) {
        mWebView.getSettings().setTextZoom((int) (scaleValue * 100));
    }

    @Override
    public void onPause() {
        resetSound();
        super.onPause();
    }

    public MediaPlayer getMediaPlayer() {
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
        }
        return mediaPlayer;
    }

    public class JavascriptInterface {
        private Tablexia         tablexiaContext;
        private EncyclopediaPage encyclopediaPage;

        public JavascriptInterface(Tablexia tablexiaContext, EncyclopediaPage encyclopediaPage) {
            super();
            this.tablexiaContext = tablexiaContext;
            this.encyclopediaPage = encyclopediaPage;
        }

        @android.webkit.JavascriptInterface
        public boolean playSound(String soundPath) {
            Log.d(EncyclopediaPage.class.getSimpleName(), "Button pressed " + soundPath);
            if (soundPath.equals(lastPlayedSound)) {
                Log.d(EncyclopediaPage.class.getSimpleName(), "Resume sound " + soundPath);
                encyclopediaPage.getMediaPlayer().start();
            } else {
                Log.d(EncyclopediaPage.class.getSimpleName(), "Play sound " + soundPath);
                prepareMediaPlayer("encyklopedie/" + soundPath + ".mp3");
                startMediaPlayer(encyclopediaPage.getMediaPlayer());
                lastPlayedSound = soundPath;
            }
            return true;
        }

        @android.webkit.JavascriptInterface
        public boolean pauseSound() {
            Log.d(EncyclopediaPage.class.getSimpleName(), "Button pressed " + lastPlayedSound);
            Log.d(EncyclopediaPage.class.getSimpleName(), "Pause sound " + lastPlayedSound);
            if (encyclopediaPage.getMediaPlayer().isPlaying()) {
                encyclopediaPage.getMediaPlayer().pause();
            }
            return true;
        }

        private void prepareMediaPlayer(String soundPath) {
            encyclopediaPage.releaseMediaPlayer();
            try {
                AssetFileDescriptor afd = tablexiaContext.getZipResourceFile().getAssetFileDescriptor(soundPath);
                Log.d(EncyclopediaPage.class.getSimpleName(), "Setting soundpath to: " + soundPath);
                encyclopediaPage.getMediaPlayer().setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                encyclopediaPage.getMediaPlayer().prepare();
                encyclopediaPage.getMediaPlayer().setLooping(false);
            } catch (Exception e) {
                Log.w("Cannot play sound: " + soundPath, e);
            }
        }

        private void startMediaPlayer(final MediaPlayer mediaPlayer) {
            if (mediaPlayer != null) {
                mediaPlayer.setVolume(1f, 1f);
                mediaPlayer.start();
                mediaPlayer.setOnCompletionListener(new OnCompletionListener() {

                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mWebView.post(new Runnable() {

                            @Override
                            public void run() {
                                mWebView.loadUrl("javascript: resetIcon();");
                            }
                        });
                        lastPlayedSound = null;
                    }
                });
            }
        }
    }

    private void releaseMediaPlayer() {
        lastPlayedSound = null;
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
            }
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public void resetSound() {
        releaseMediaPlayer();
        mWebView.loadUrl("javascript: resetIcon();");
    }

}
