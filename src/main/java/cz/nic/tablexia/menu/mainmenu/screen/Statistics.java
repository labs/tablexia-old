/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.menu.mainmenu.screen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.LineChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.chart.XYChart;
import org.achartengine.model.SeriesSelection;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.Spanned;
import android.text.SpannedString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import cz.nic.tablexia.R;
import cz.nic.tablexia.audio.resources.PermanentSounds;
import cz.nic.tablexia.audio.resources.SpeechSounds;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.GamesDefinition;
import cz.nic.tablexia.game.manager.GameManager;
import cz.nic.tablexia.menu.MenuActivity;
import cz.nic.tablexia.menu.mainmenu.MainMenuDefinition;
import cz.nic.tablexia.util.Utility;

/**
 * Statistics screen fragment.
 *
 * @author Vaclav Tarantik, Matyáš Latner
 */
public class Statistics extends AbstractMenuFragment {

    private static final int             CARDFILE_ANIM_DURATION             = 2000;
    private static final int             CARDFILE_ANIM_STARTOFFSET          = -100;

    private static final int             DETAIL_INFO_PANEL_DELAY            = 2000;

    private static final int             MILISECOUNDS                       = 1000;
    private static final int             ONE_POINT_CHART_OFFSET             = 1;
    private static final GameDifficulty  INITIALLY_SELECTED_DIFFICULTY      = GameDifficulty.EASY;
    private static final GamesDefinition INITIALLY_SELECTED_GAME            = GamesDefinition.BANKOVNILOUPEZ;

    private static final double          CHART_SERIE_PADDING_RATIO          = 10;
    private static final double          CHART_SERIE_MAXIMUM_INCREASE_POINT = 10;
    private static final float           CHART_SERIE_LINE_SIZE              = 4.0f;
    private static final PointStyle      CHART_SERIE_POINT_STYLE            = PointStyle.CIRCLE;

    private Handler                      detailInfoPanelHideHandler;
    private Runnable                     detailInfoPanelHideCallback;
    private ViewPropertyAnimator         detailInfoPanelHideAnimator;

    private enum DisplayType {
        INDIVIDUALLY, DAILYAVERAGE;
    }

    private DisplayType                                displayType;
    private GamesDefinition                            selectedGameDefinition;

    private List<ViewGroup>                            difficultyButtons;

    private XYMultipleSeriesDataset                    mDataset;
    private XYMultipleSeriesRenderer                   chartRenderer;

    private GraphicalView                              mChartView;
    private Switch                                     displayTypeSwitch;

    private Map<GameDifficulty, XYSeries>              difficultySeries;

    private static final Map<GamesDefinition, Integer> GAMELIST_INACTIVEBACKGROUND_MAPPING = new HashMap<GamesDefinition, Integer>();
    static {
        GAMELIST_INACTIVEBACKGROUND_MAPPING.put(GamesDefinition.BANKOVNILOUPEZ, R.drawable.screen_statistics_gamelist_listitem_background1_inactive);
        GAMELIST_INACTIVEBACKGROUND_MAPPING.put(GamesDefinition.PRONASLEDOVANI, R.drawable.screen_statistics_gamelist_listitem_background2_inactive);
        GAMELIST_INACTIVEBACKGROUND_MAPPING.put(GamesDefinition.UNOS, R.drawable.screen_statistics_gamelist_listitem_background3_inactive);
        GAMELIST_INACTIVEBACKGROUND_MAPPING.put(GamesDefinition.NOCNISLEDOVANI, R.drawable.screen_statistics_gamelist_listitem_background2_inactive);
        GAMELIST_INACTIVEBACKGROUND_MAPPING.put(GamesDefinition.STRELNICE, R.drawable.screen_statistics_gamelist_listitem_background3_inactive);
        GAMELIST_INACTIVEBACKGROUND_MAPPING.put(GamesDefinition.POTME, R.drawable.screen_statistics_gamelist_listitem_background1_inactive);
    }
    private static final Map<GamesDefinition, Integer> GAMELIST_ACTIVEBACKGROUND_MAPPING   = new HashMap<GamesDefinition, Integer>();
    static {
        GAMELIST_ACTIVEBACKGROUND_MAPPING.put(GamesDefinition.BANKOVNILOUPEZ, R.drawable.screen_statistics_gamelist_listitem_background1_active);
        GAMELIST_ACTIVEBACKGROUND_MAPPING.put(GamesDefinition.PRONASLEDOVANI, R.drawable.screen_statistics_gamelist_listitem_background2_active);
        GAMELIST_ACTIVEBACKGROUND_MAPPING.put(GamesDefinition.UNOS, R.drawable.screen_statistics_gamelist_listitem_background3_active);
        GAMELIST_ACTIVEBACKGROUND_MAPPING.put(GamesDefinition.NOCNISLEDOVANI, R.drawable.screen_statistics_gamelist_listitem_background2_active);
        GAMELIST_ACTIVEBACKGROUND_MAPPING.put(GamesDefinition.STRELNICE, R.drawable.screen_statistics_gamelist_listitem_background3_active);
        GAMELIST_ACTIVEBACKGROUND_MAPPING.put(GamesDefinition.POTME, R.drawable.screen_statistics_gamelist_listitem_background1_active);
    }

    private Map<GamesDefinition, ImageView>            gameListItems;
    private GameDifficulty                             selectedDifficulty;
    private List<GameManager>                          selectedGameLogs;
    private View                                       detailInfoPanel;

    public Statistics() {
        super(R.layout.screen_statistics, SpeechSounds.STATISTICS_INTRO);
        difficultySeries = new HashMap<GameDifficulty, XYSeries>();
        gameListItems = new HashMap<GamesDefinition, ImageView>();
        selectedGameDefinition = INITIALLY_SELECTED_GAME;
    }

    @Override
    protected void playIntroSound() {
        if (getTablexiaContext().getSelectedUser().getPlayCountStatistics() < 3) {
            super.playIntroSound();
            getTablexiaContext().getSelectedUser().addPlayCountStatistics();
        }
    }

    /* //////////////////////////////////////////// TABLEXIA FRAGMENT LIFECYCLE */

    @Override
    protected void asyncFragmentDataLoad(View layoutView) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());

        // SCREEN COMPONENTS INITIALIZATION
        prepareCloseButton(layoutView);
        prepareGameTypeSwitch(layoutView);
        prepareGameList(layoutView, inflater);
        prepareRadioButtons(inflater, layoutView);

        // CHART COMPONENTS INITIALIZATION
        initializeChartComponents();

        detailInfoPanel = layoutView.findViewById(R.id.screen_statistics_chartdetail_panel);
    }

    @Override
    protected void syncFragmentDataLoad(View layoutView) {
        prepareChartView(layoutView);
        createSeriesForDifficulties();
        prepareCardFile();
    }

    @Override
    public void onFragmentResume() {
        redrawChart();
    }

    @Override
    public void onFragmentContentVisible() {
        openCardfile();
    }

    /* //////////////////////////////////////////// UI METHODS */

    private void switchToDailyAverageGameType() {
        displayType = DisplayType.DAILYAVERAGE;
        displayTypeSwitch.setChecked(true);
        redrawChart();
    }

    private void switchToIndividullyGameType() {
        displayType = DisplayType.INDIVIDUALLY;
        displayTypeSwitch.setChecked(false);
        redrawChart();
    }

    private void prepareCloseButton(View fragmentLayout) {

        final ImageButton closeButtonImage = (ImageButton) fragmentLayout.findViewById(R.id.screen_statistics_closebutton_image);
        if (closeButtonImage != null) {
            closeButtonImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    playSound(PermanentSounds.BUTTON_SOUND, false);
                    closeButtonImage.setPressed(true);
                    getTablexiaContext().prepareMainMenu().changeMenuItem(MainMenuDefinition.START, true, false);
                }

            });
        }
    }

    private void prepareCardFile() {
        View cardfile = getActivity().findViewById(R.id.screen_statistics_cardfile);
        if (cardfile != null) {
            cardfile.setTranslationX(CARDFILE_ANIM_STARTOFFSET);
        }
    }

    private void openCardfile() {
        View cardfile = getActivity().findViewById(R.id.screen_statistics_cardfile);
        if (cardfile != null) {
            cardfile.animate().translationX(0).setDuration(CARDFILE_ANIM_DURATION);
        }
    }

    private void prepareGameTypeSwitch(View fragmentLayout) {
        displayTypeSwitch = (Switch) fragmentLayout.findViewById(R.id.screen_statistics_displaytypeswitch);
        displayTypeSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                playSound(PermanentSounds.BUTTON_SOUND, false);
                if (isChecked) {
                    switchToDailyAverageGameType();
                } else {
                    switchToIndividullyGameType();
                }
            }
        });

        TextView dailyaverageText = (TextView) fragmentLayout.findViewById(R.id.screen_statistics_displaytypebutton_text_dailyaverage);
        dailyaverageText.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                switchToDailyAverageGameType();
            }

        });

        TextView individuallyText = (TextView) fragmentLayout.findViewById(R.id.screen_statistics_displaytypebutton_text_individually);
        individuallyText.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                switchToIndividullyGameType();
            }

        });
    }

    private void prepareGameList(View fragmentLayout, LayoutInflater inflater) {
        for (GamesDefinition gameDefinition : GamesDefinition.getGames()) {
            prepareGameListItem(fragmentLayout, inflater, gameDefinition);
        }
        refreshGameListBackgroundState();
    }

    private void prepareGameListItem(View fragmentLayout, LayoutInflater inflater, final GamesDefinition gameDefinition) {
        LinearLayout gameList = (LinearLayout) fragmentLayout.findViewById(R.id.screen_statistics_gamelist);
        View gameListItem = inflater.inflate(R.layout.screen_statistics_gamelist_item, null, false);
        gameList.addView(gameListItem);

        final ImageView gameListImageButton = (ImageView) gameListItem.findViewById(R.id.screen_statistics_gamelist_item_background);
        gameListItems.put(gameDefinition, gameListImageButton);
        gameListImageButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                selectedGameDefinition = gameDefinition;
                refreshGameListBackgroundState();
                redrawChart();
            }

        });

        TextView gameListTextView = (TextView) gameListItem.findViewById(R.id.screen_statistics_gamelist_item_text);
        gameListTextView.setText(gameDefinition.getGameNameResourceId());
    }

    private void refreshGameListBackgroundState() {
        for (GamesDefinition gameDefinition : GamesDefinition.getGames()) {
            ImageView gameListItem = gameListItems.get(gameDefinition);
            if (gameListItem != null) {
                if (gameDefinition == selectedGameDefinition) {
                    Integer selectedGameResourceId = GAMELIST_ACTIVEBACKGROUND_MAPPING.get(gameDefinition);
                    if (selectedGameResourceId != null) {
                        gameListItem.setImageResource(selectedGameResourceId);
                    }
                } else {
                    Integer unselectGameResourceId = GAMELIST_INACTIVEBACKGROUND_MAPPING.get(gameDefinition);
                    if (unselectGameResourceId != null) {
                        gameListItem.setImageResource(unselectGameResourceId);
                    }
                }
            }
        }
    }

    private void prepareRadioButtons(LayoutInflater inflater, View fragmentLayout) {
        difficultyButtons = new ArrayList<ViewGroup>();
        for (GameDifficulty gameDifficulty : GameDifficulty.values()) {
            prepareRadioButtonForDifficulty(inflater, (ViewGroup) fragmentLayout.findViewById(R.id.screen_statistics_difficultybutton_container), gameDifficulty);
        }
    }

    private void prepareRadioButtonForDifficulty(LayoutInflater inflater, ViewGroup difficultyButtonsContainer, GameDifficulty gameDifficulty) {
        final ViewGroup difficultyButton = (ViewGroup) inflater.inflate(R.layout.screen_statistics_difficultyradio_button, null);
        difficultyButtons.add(difficultyButton);
        difficultyButton.setId(gameDifficulty.ordinal());
        difficultyButton.setTag(gameDifficulty.ordinal());

        setDifficultyButtonText(((TextView) difficultyButton.findViewById(R.id.screen_statistics_difficultybutton_text)), SpannedString.valueOf(getResources().getText(gameDifficulty.getDescriptionResourceId()).toString()));

        ((ImageView) difficultyButton.findViewById(R.id.screen_statistics_difficultybutton_image)).setImageResource(gameDifficulty.getStatisticsRadioButtonResourceId());
        difficultyButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                playSound(PermanentSounds.BUTTON_SOUND, false);
                highliteRadioButton(difficultyButton);
                redrawChart();
            }

        });

        if (gameDifficulty == INITIALLY_SELECTED_DIFFICULTY) {
            highliteRadioButton(difficultyButton);
        }

        difficultyButtonsContainer.addView(difficultyButton);
    }

    private void setDifficultyButtonText(TextView difficultyText, Spanned text) {
        if (Utility.isScreenSize(getActivity(), Configuration.SCREENLAYOUT_SIZE_XLARGE) || Utility.isScreenSize(getActivity(), Configuration.SCREENLAYOUT_SIZE_LARGE)) {
            difficultyText.setText(text);
        }
    }

    private void highliteRadioButton(ViewGroup radioButtonToHighlite) {
        for (ViewGroup difficultyButton : difficultyButtons) {
            String text = getString(GameDifficulty.values()[difficultyButton.getId()].getDescriptionResourceId());
            TextView buttonText = ((TextView) difficultyButton.findViewById(R.id.screen_statistics_difficultybutton_text));
            ImageView buttonImage = ((ImageView) difficultyButton.findViewById(R.id.screen_statistics_difficultybutton_image));
            if (difficultyButton == radioButtonToHighlite) {
                buttonImage.setSelected(true);
                buttonText.setTypeface(null, Typeface.BOLD);
                setDifficultyButtonText(buttonText, Html.fromHtml("<u>" + text + "</u>"));
            } else {
                buttonImage.setSelected(false);
                buttonText.setTypeface(null, Typeface.NORMAL);
                setDifficultyButtonText(buttonText, SpannedString.valueOf(text));
            }
        }
    }

    /* //////////////////////////////////////////// CHART RENDER METHODS */

    /**
     * Initialize components an variables for chart rendering
     */
    private void initializeChartComponents() {
        displayType = DisplayType.INDIVIDUALLY;

        mDataset = new XYMultipleSeriesDataset();
        chartRenderer = new XYMultipleSeriesRenderer();

        chartRenderer.setYLabels(0);
        chartRenderer.setXLabels(0);

        chartRenderer.setMargins(new int[] { 0, 0, 0, 0 });

        chartRenderer.setShowAxes(false);
        chartRenderer.setShowLegend(false);
        chartRenderer.setShowLabels(false);

        chartRenderer.setPointSize(7f);

        chartRenderer.setPanEnabled(false, false);
        chartRenderer.setZoomEnabled(false, false);
    }

    /**
     * Prepares chart view
     *
     * @param fragmentLayout fragments layout view
     */
    private void prepareChartView(View fragmentLayout) {
        if (mChartView == null) {
            FrameLayout layout = (FrameLayout) fragmentLayout.findViewById(R.id.screen_statistics_chart);
            mChartView = ChartFactory.getLineChartView(getActivity(), mDataset, chartRenderer);
            mChartView.setPadding(0, 0, 0, 0);

            chartRenderer.setClickEnabled(true);
            chartRenderer.setSelectableBuffer(30);

            final LineChart lineChart = (LineChart) mChartView.getChart();

            mChartView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // handle the click event on the chart
                    SeriesSelection seriesSelection = ((GraphicalView) v).getCurrentSeriesAndPoint();

                    if (seriesSelection != null) {
                        // display information of the clicked point
                        playSound(PermanentSounds.BUTTON_SOUND, false);
                        prepareDetailInfoPanelValues(selectedGameLogs.get((int) seriesSelection.getXValue()), seriesSelection.getValue());
                        alignDetailInfoPanelToPoint(seriesSelection, lineChart);
                        showDetailInfoPanel();
                    }
                }
            });

            layout.addView(mChartView);
        } else {
            mChartView.repaint();
        }
    }

    private void showDetailInfoPanel() {
        hideDetailInfoPanel();
        detailInfoPanel.setAlpha(0);
        detailInfoPanel.setVisibility(View.VISIBLE);
        detailInfoPanelHideAnimator = detailInfoPanel.animate();
        detailInfoPanelHideAnimator.setDuration(400).alpha(1).setListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(Animator animation) {
                detailInfoPanelHideHandler = new Handler();
                detailInfoPanelHideCallback = new Runnable() {
                    @Override
                    public void run() {
                        FragmentActivity activity = getActivity();
                        if (activity != null) {
                            activity.runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    hideDetailInfoPanelAnim();
                                }

                            });
                        }
                    }
                };
                detailInfoPanelHideHandler.postDelayed(detailInfoPanelHideCallback, DETAIL_INFO_PANEL_DELAY);
            }

        });
    }

    private void hideDetailInfoPanel() {
        if ((detailInfoPanel != null) && (detailInfoPanel.getVisibility() == View.VISIBLE)) {
            if (detailInfoPanelHideAnimator != null) {
                detailInfoPanelHideAnimator.cancel();
            }
            detailInfoPanel.setVisibility(View.GONE);
            if ((detailInfoPanelHideHandler != null) && (detailInfoPanelHideCallback != null)) {
                detailInfoPanelHideHandler.removeCallbacks(detailInfoPanelHideCallback);
            }
        }
    }

    private void hideDetailInfoPanelAnim() {
        detailInfoPanel.animate().setDuration(400).alpha(0).setListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(Animator animation) {
                detailInfoPanel.setVisibility(View.GONE);
            }

        });
    }

    private void alignDetailInfoPanelToPoint(SeriesSelection seriesSelection, XYChart lineChart) {
        XYSeries selectedSerie = difficultySeries.get(selectedDifficulty);
        double x = selectedSerie.getX(seriesSelection.getPointIndex());
        double y = selectedSerie.getY(seriesSelection.getPointIndex());
        double[] screenPoint = lineChart.toScreenPoint(new double[] { x, y }, 0);

        int[] location = new int[2];
        getActivity().findViewById(R.id.screen_statistics_chart).getLocationOnScreen(location);

        detailInfoPanel.setX(((float) screenPoint[0] - (detailInfoPanel.getMeasuredWidth() / 2)) + location[0]);
        detailInfoPanel.setY(((float) screenPoint[1] - (detailInfoPanel.getMeasuredHeight() - (detailInfoPanel.getMeasuredHeight() / 9))) + location[1]);
    }

    private void prepareDetailInfoPanelValues(GameManager gameManager, double pointValue) {
        TextView detailInfoText1 = (TextView) detailInfoPanel.findViewById(R.id.screen_statistics_chartdetail_text1);
        TextView detailInfoText2 = (TextView) detailInfoPanel.findViewById(R.id.screen_statistics_chartdetail_text2);
        switch (displayType) {
            case INDIVIDUALLY:
                setDetailInfoPanelScore(gameManager.getStarsNumber());
                detailInfoText1.setText(DateFormatUtils.format(gameManager.getStartTime(), "dd.MM.yyyy HH:mm:ss"));
                detailInfoText2.setText(getScoreValue(selectedGameDefinition, selectedGameDefinition.getScoreTextResourceId(), pointValue));
                break;
            case DAILYAVERAGE:
                getActivity().findViewById(R.id.screen_statistics_chartdetail_ratingstars).setVisibility(View.GONE);
                detailInfoText1.setText(DateFormatUtils.format(gameManager.getStartTime(), "dd.MM.yyyy"));
                detailInfoText2.setText(getScoreValue(selectedGameDefinition, selectedGameDefinition.getAveragescoreTextResourceId(), pointValue));
                break;
        }
    }

    private void setDetailInfoPanelScore(int score) {
        View ratingStart1 = detailInfoPanel.findViewById(R.id.screen_statistics_ratingstar1);
        View ratingStar2 = detailInfoPanel.findViewById(R.id.screen_statistics_ratingstar2);
        View ratingStar3 = detailInfoPanel.findViewById(R.id.screen_statistics_ratingstar3);

        detailInfoPanel.findViewById(R.id.screen_statistics_chartdetail_ratingstars).setVisibility(View.VISIBLE);
        ratingStart1.findViewById(R.id.screen_statistics_ratingstar_image).setVisibility(View.GONE);
        ratingStar2.findViewById(R.id.screen_statistics_ratingstar_image).setVisibility(View.GONE);
        ratingStar3.findViewById(R.id.screen_statistics_ratingstar_image).setVisibility(View.GONE);
        if (score > 0) {
            ratingStart1.findViewById(R.id.screen_statistics_ratingstar_image).setVisibility(View.VISIBLE);
        }
        if (score > 1) {
            ratingStar2.findViewById(R.id.screen_statistics_ratingstar_image).setVisibility(View.VISIBLE);
        }
        if (score > 2) {
            ratingStar3.findViewById(R.id.screen_statistics_ratingstar_image).setVisibility(View.VISIBLE);
        }
    }

    private String getScoreValue(GamesDefinition gameDefinition, int scoreTextResourceId, double scoreValue) {
        String result = getActivity().getString(scoreTextResourceId);
        if (gameDefinition.isScoreDuration()) {
            result = result + " " + DurationFormatUtils.formatDuration((long) scoreValue * MILISECOUNDS * selectedGameDefinition.getChartOrientation(), "HH:mm:ss");
        } else {
            result = result + " " + String.format("%.0f", scoreValue * selectedGameDefinition.getChartOrientation());
        }
        return result;
    }

    /**
     * Creates chart series for difficulties
     */
    private void createSeriesForDifficulties() {
        for (GameDifficulty difficulty : GameDifficulty.values()) {
            createSerie(difficulty, CHART_SERIE_LINE_SIZE, CHART_SERIE_POINT_STYLE);
        }
    }

    /**
     * Create chart serie and renderer for specific difficulty
     *
     * @param gameDifficulty difficulty for game serie
     * @param color
     * @param lineWidth
     * @param pointStyle
     */
    private void createSerie(GameDifficulty gameDifficulty, float lineWidth, PointStyle pointStyle) {
        XYSeries newSerie = new XYSeries(getResources().getString(gameDifficulty.getDescriptionResourceId()));
        difficultySeries.put(gameDifficulty, newSerie);
    }

    /**
     * Adds new line to chart with selected game and specific game difficulty
     *
     * @param gameDifficulty game difficulty
     */
    private void addUserGamesLine(GameDifficulty gameDifficulty) {
        selectedGameLogs = null;
        switch (displayType) {
            case DAILYAVERAGE:
                selectedGameLogs = GameManager.getDailyAverageForUserAndDifficulty(((MenuActivity) getActivity()).getTablexiaContext().getSelectedUser(), selectedGameDefinition, gameDifficulty);
                break;
            case INDIVIDUALLY:
                selectedGameLogs = GameManager.getGameHistoryForUserAndDifficulty(((MenuActivity) getActivity()).getTablexiaContext().getSelectedUser(), selectedGameDefinition, gameDifficulty);
                break;
        }
        addLine(gameDifficulty, selectedGameLogs);
    }

    /**
     * Adds line for specific difficulty to chart.
     *
     * @param difficult difficulty for add in
     * @param gameLogs list of user games
     */
    private void addLine(GameDifficulty difficulty, List<GameManager> gameLogs) {
        XYSeries xySeries = difficultySeries.get(difficulty);
        xySeries.clear();
        if (gameLogs != null) {
            for (int i = 0; i < gameLogs.size(); i++) {
                xySeries.add(i, gameLogs.get(i).getCounter() * selectedGameDefinition.getChartOrientation());
            }
        }
    }

    /**
     * Redraw all chart series
     */
    private void redrawChart() {

        hideDetailInfoPanel();

        chartRenderer.clearXTextLabels();
        chartRenderer.clearYTextLabels();

        ViewGroup selectedDifficultyButton = null;
        for (ViewGroup difficultyButton : difficultyButtons) {
            if (((ImageView) difficultyButton.findViewById(R.id.screen_statistics_difficultybutton_image)).isSelected()) {
                selectedDifficultyButton = difficultyButton;
            }
        }

        if (selectedDifficultyButton != null) {

            selectedDifficulty = GameDifficulty.values()[(Integer) selectedDifficultyButton.getTag()];
            XYSeries selectedSerie = difficultySeries.get(selectedDifficulty);
            prepareNewSerie(selectedDifficulty, selectedSerie);

            if ((selectedSerie != null) && (selectedSerie.getItemCount() > 0)) {

                double minX = selectedSerie.getMinX();
                double maxX = selectedSerie.getMaxX();
                double minY = selectedSerie.getMinY();
                double maxY = selectedSerie.getMaxY();

                double itemsLimitedCount = Math.min(selectedSerie.getItemCount(), CHART_SERIE_MAXIMUM_INCREASE_POINT) - 1;

                if (minY == maxY) {
                    minY = selectedSerie.getY(0) - ONE_POINT_CHART_OFFSET;
                    maxY = selectedSerie.getY(0) + ONE_POINT_CHART_OFFSET;
                }
                if (minX == maxX) {
                    itemsLimitedCount = 1;
                    maxX = selectedSerie.getX(0) + ONE_POINT_CHART_OFFSET;
                }

                double offset = CHART_SERIE_MAXIMUM_INCREASE_POINT - itemsLimitedCount;

                chartRenderer.setYAxisMin(minY - ((maxY - minY) * (1 / CHART_SERIE_PADDING_RATIO)));
                chartRenderer.setYAxisMax(maxY + ((maxY - minY) * (1 / CHART_SERIE_PADDING_RATIO)));
                chartRenderer.setXAxisMin(minX - ((maxX - minX) / itemsLimitedCount));
                chartRenderer.setXAxisMax(maxX + (offset * ((maxX - minX) / itemsLimitedCount)));

            }

            mChartView.repaint();
        }
    }

    private void prepareNewSerie(GameDifficulty difficulty, XYSeries selectedSerie) {
        mDataset.clear();
        mDataset.addSeries(selectedSerie);

        chartRenderer.removeAllRenderers();
        XYSeriesRenderer serieRenderer = new XYSeriesRenderer();
        serieRenderer.setColor(getActivity().getResources().getColor(difficulty.getColorResourceId()));
        serieRenderer.setLineWidth(CHART_SERIE_LINE_SIZE);
        serieRenderer.setPointStyle(CHART_SERIE_POINT_STYLE);
        serieRenderer.setFillPoints(true);
        serieRenderer.setShowLegendItem(false);
        chartRenderer.addSeriesRenderer(serieRenderer);

        addUserGamesLine(difficulty);
    }

}
