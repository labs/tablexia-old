/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.menu.mainmenu.screen;

import java.util.HashMap;
import java.util.Map;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Scroller;
import cz.nic.tablexia.R;
import cz.nic.tablexia.audio.resources.SfxSounds;
import cz.nic.tablexia.audio.resources.SpeechSounds;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.GamesDefinition;
import cz.nic.tablexia.menu.MenuActivity;
import cz.nic.tablexia.menu.mainmenu.MainMenuDefinition;
import cz.nic.tablexia.menu.mainmenu.screen.AbstractMenuFragment.MenuFragmentVisibilityListener;
import cz.nic.tablexia.menu.mainmenu.screen.gamemenu.BaseGameMenuFragment;
import cz.nic.tablexia.menu.mainmenu.screen.gamemenu.GameMenuFragment;
import cz.nic.tablexia.menu.mainmenu.screen.gamemenu.ParalaxPageTransformer;
import cz.nic.tablexia.menu.mainmenu.screen.gamemenu.StartMenu;
import cz.nic.tablexia.menu.usermenu.User;
import cz.nic.tablexia.newusers.fragment.newuser.SightSeeingBroadcastReceiver;
import cz.nic.tablexia.widget.SlowScroller;

/**
 * Game menu fragment.
 *
 * @author Matyáš Latner
 */
public class GameMenu extends AbstractMenuFragment implements OnPageChangeListener, MenuFragmentVisibilityListener {
    private static final String            TAG                     = GameMenu.class.getSimpleName();
    public static final String             BUNDLE_GAME_KEY         = "game";
    private static final int               GAMES_PAGER_ID          = R.id.screen_games_pager;
    private static final int               GAMES_PAGER_LAYOUT_ID   = R.layout.screen_gamemenu;
    // number of pages with static content
    public static final int                NO_GAME_PAGES_COUNT     = 1;
    private static final int               START_SCREEN_POSITION   = 0;

    private static final String            AMBIENT_SOUND_ASSETPATH = "menu/mfx/gamemenu_ambient.mp3";
    public static final String             MENU_INTRO_SOUND        = "menu/speech/intro.mp3";
    private static final float             FADEOUT_VOLUMESTEP      = 0.1f;

    private ViewPager                      gamesPager;

    private MediaPlayer                    ambientMediaPlayer;
    private MediaPlayer                    menuIntroMediaPlayer;
    private StartMenu                      startMenu;                                                // kvuli kontrole, jestli se vracim zpet do kancelare

    private int                            selectedPageNumber      = 0;

    private Map<Integer, GameMenuFragment> gameMenuPages;
    private View                           coverView;
    private boolean                        ambientStopped          = false;

    public GameMenu() {
        super(GAMES_PAGER_LAYOUT_ID, null);
    }

    BroadcastReceiver receiverAmbient = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (ambientMediaPlayer != null) {
                ambientMediaPlayer.stop();
                getTablexiaContext().getGlobalSoundControl().stopSound(SpeechSounds.LOGIN_HELP_SOUND, false, false);
            }
            ambientStopped = true;
        }
    };
    
    SightSeeingBroadcastReceiver receiver = new SightSeeingBroadcastReceiver() {

        @Override
        protected void onNewUserBroadcastReceivedAction() {
        	if (gamesPager != null) {        		
        		gamesPager.setCurrentItem(gamesPager.getAdapter().getCount() - 1, false);
        	}
        }
    };

    /* //////////////////////////////////////////// TABLEXIA FRAGMENT LIFECYCLE */

    @Override
    protected void asyncFragmentDataLoad(View layoutView) {
        loadMenuPages();

        gamesPager = (ViewPager) layoutView.findViewById(GAMES_PAGER_ID);
        gamesPager.setPageTransformer(false, new ParalaxPageTransformer());
        gamesPager.setAdapter(new GameMenuAdapter(getChildFragmentManager()));

        // need to load all fragments to memory at once for smooth scrolling
        gamesPager.setOffscreenPageLimit(GamesDefinition.getGamesCount() + NO_GAME_PAGES_COUNT);

        User u = getTablexiaContext().getTablexiaState().getActiveUser();

        if ((u == null) || !u.isSightseeingShown()) {
            gamesPager.setCurrentItem(gamesPager.getAdapter().getCount() - 1, false);
        }
        
        IntentFilter filter = new IntentFilter("cz.nic.tablexia.action.NEW_USER_CREATED");
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver, filter);

        IntentFilter filterAmbient = new IntentFilter("cz.nic.tablexia.action.MUTE_AMBIENT");
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiverAmbient, filterAmbient);

        prepareAudioControll();
        coverView = layoutView.findViewById(R.id.screen_games_cover);
        coverView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    @Override
    public void onFragmentResume() {
        User selectedUser = getMenuActivity().getTablexiaContext().getSelectedUser();
        if (selectedUser == null) {
            if (!ambientStopped) {
                playSound(SpeechSounds.LOGIN_HELP_SOUND, false);
                playAmbientSound();
            } else {
                ambientStopped = false;
            }
            initGameMenu();
        } else {
            if (selectedUser.isHelp() && !selectedUser.isSightseeingShown()) {
                playMenuIntroSound();
            } else {
                initGameMenu();
            }

            if (!selectedUser.isSightseeingShown()) {
                doSightseeing();
                selectedUser.setSightseeingShownAndSave(true);
            }
        }

        for (AbstractMenuFragment gameMenuPage : gameMenuPages.values()) {
            if (gameMenuPage.isLoadingComplete()) {
                gameMenuPage.onFragmentResume();
            }
        }
    }

    private void initGameMenu() {
        gamesPager.setOnPageChangeListener(this);
        goToPage(selectedPageNumber, false);
    }
    
    /* //////////////////////////////////////////// ANDROID FRAGMENT LIFECYCLE */

    @Override
    public void onBackPressed() {
        // GOTO TO START MENU OR LOGOUT USER
        if (!resetMenu()) {
            if (getTablexiaContext().getSelectedUser() != null) {
                getTablexiaContext().softResetUser();
            }
        }
    }
    
    @Override
    public void onDestroy() {
    	LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
    	LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiverAmbient);
    	super.onDestroy();
    }

    /* //////////////////////////////////////////// INTRO ANIMATION */

    private void doSightseeing() {
        coverView.setVisibility(View.VISIBLE);
        gamesPager.setCurrentItem(gamesPager.getAdapter().getCount() - 1, false);
        Object page = ((GameMenuAdapter) gamesPager.getAdapter()).getItem(gamesPager.getAdapter().getCount() - 1);
        if (page instanceof BaseGameMenuFragment) {
            ((BaseGameMenuFragment) page).resetParalax();
        }
        // odscrollovano na konec, musime pockat chvilku, jinak se jina udalost nikdy neprojevi
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // a scrollujeme zpet
                final Scroller prevScroller = SlowScroller.setSlowScroller(gamesPager);
                gamesPager.setCurrentItem(0, true);
                // po doscrollovani chceme vratit zpet puvodni vlastnosti
                gamesPager.setOnPageChangeListener(new OnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        // delagate events for sounds
                        GameMenu.this.onPageSelected(position);
                    }

                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                        // nic
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {
                        if (state == ViewPager.SCROLL_STATE_IDLE) {
                            SlowScroller.setScroller(gamesPager, prevScroller);
                            gamesPager.setOnPageChangeListener(GameMenu.this);
                            coverView.setVisibility(View.GONE);

                        }
                    }
                });
                Log.i(GameMenu.this.getClass().getSimpleName(), "Scrolling back");
            }
        }, 750);
    }

    @Override
    public void onPause() {
        if (lastRunnable != null) {
            pageSoundPlayHandler.removeCallbacks(lastRunnable);
            lastRunnable = null;
        }
        muteAllSounds();
        fadeOutAmbientSound();
        fadeOutMenuIntro();
        if (gamesPager != null) {
            gamesPager.setOnPageChangeListener(null);
        }
        super.onPause();
    }

    /* //////////////////////////////////////////// SOUNDS */

    private MediaPlayer prepareMediaPlayer(String soundPath, boolean looping) {
        MediaPlayer mediaPlayer = new MediaPlayer();
        try {
            AssetFileDescriptor afd = getTablexiaContext().getZipResourceFile().getAssetFileDescriptor(soundPath);
            mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mediaPlayer.prepare();
            mediaPlayer.setLooping(looping);
            return mediaPlayer;
        } catch (Exception e) {
            Log.w("Cannot play sound: " + soundPath, e);
            return null;
        }
    }

    private void playAmbientSound() {
        ambientMediaPlayer = prepareMediaPlayer(AMBIENT_SOUND_ASSETPATH, true);
        startMediaPlayer(ambientMediaPlayer);
    }

    private void playMenuIntroSound() {
        menuIntroMediaPlayer = prepareMediaPlayer(MENU_INTRO_SOUND, false);
        startMediaPlayer(menuIntroMediaPlayer);
    }

    private void startMediaPlayer(final MediaPlayer mediaPlayer) {
        if (mediaPlayer != null) {
            mediaPlayer.setVolume(1f, 1f);
            mediaPlayer.start();
        }
    }

    private void fadeoutMediaPlayer(final MediaPlayer mediaPlayer) {
        if (mediaPlayer != null) {
            new Thread() {

                private float volume     = 1f;
                private float volumeStep = FADEOUT_VOLUMESTEP;

                @Override
                public void run() {

                    while ((volume > 0)) {

                        volume = volume - volumeStep;
                        mediaPlayer.setVolume(volume, volume);

                        try {
                            sleep(100);
                        } catch (InterruptedException e) {
                            Log.w("Cannot fadeout ambient sound: " + AMBIENT_SOUND_ASSETPATH, e);
                        }
                    }
                    mediaPlayer.stop();
                    mediaPlayer.release();
                }

            }.start();
        }
    }

    private void fadeOutAmbientSound() {
        fadeoutMediaPlayer(ambientMediaPlayer);
        ambientMediaPlayer = null;
    }

    private void fadeOutMenuIntro() {
        fadeoutMediaPlayer(menuIntroMediaPlayer);
        menuIntroMediaPlayer = null;
    }

    private void prepareAudioControll() {
        prepareGameIntroSounds();
        preloadSound(SfxSounds.SEEKBAR_STEP_1_2);
        preloadSound(SfxSounds.SEEKBAR_STEP_2_3);
        preloadSound(SfxSounds.SEEKBAR_STEP_3_2);
        preloadSound(SfxSounds.SEEKBAR_STEP_2_1);
        preloadSound(SpeechSounds.LOGIN_HELP_SOUND);
    }

    private void prepareGameIntroSounds() {
        for (GamesDefinition gameDefinition : GamesDefinition.getGames()) {
            // preloadSound(gameDefinition.getIntroSoundPath()); // pouzivame MediaPLayer
        }
    }

    /* //////////////////////////////////////////// DIFFICULTY SEEKBAR */

    public void playDifficultySeekbarMoveSound(GameDifficulty selectedDifficulty, GameDifficulty currentDificulty) {
        if (currentDificulty == GameDifficulty.MEDIUM) {
            if (selectedDifficulty == GameDifficulty.EASY) {
                playSound(SfxSounds.SEEKBAR_STEP_1_2, false);
            } else if (selectedDifficulty == GameDifficulty.HARD) {
                playSound(SfxSounds.SEEKBAR_STEP_3_2, false);
            }
        } else if (currentDificulty != selectedDifficulty) {
            if (currentDificulty == GameDifficulty.EASY) {
                playSound(SfxSounds.SEEKBAR_STEP_2_1, false);
            } else if (currentDificulty == GameDifficulty.HARD) {
                playSound(SfxSounds.SEEKBAR_STEP_2_3, false);
            }
        }
    }

    /* //////////////////////////////////////////// VIEW PAGER */

    public class GameMenuAdapter extends FragmentStatePagerAdapter {

        public GameMenuAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            // count of games plus first screen with panorama menu
            return GamesDefinition.getGamesCount() + NO_GAME_PAGES_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            return gameMenuPages.get(Integer.valueOf(position));
        }

    }

    private void loadMenuPages() {
        gameMenuPages = new HashMap<Integer, GameMenuFragment>();

        startMenu = new StartMenu();
        startMenu.setFragmentVisibilityListener(this);
        gameMenuPages.put(Integer.valueOf(0), startMenu);

        for (GamesDefinition gamesDefinition : GamesDefinition.getGames()) {
            Class<? extends Fragment> clazz = gamesDefinition.getGameMenuFragmentClass();
            Bundle bundle = new Bundle();
            bundle.putInt(BUNDLE_GAME_KEY, gamesDefinition.ordinal());
            GameMenuFragment gameMenuPage = (GameMenuFragment) Fragment.instantiate(getActivity(), clazz.getName(), bundle);
            gameMenuPage.setFragmentVisibilityListener(this);
            gameMenuPages.put(Integer.valueOf(gamesDefinition.ordinal() + NO_GAME_PAGES_COUNT), gameMenuPage);
        }
    }

    @Override
    protected void fragmentDataLoadComplete() {
        for (AbstractMenuFragment gameMenuPage : gameMenuPages.values()) {
            if (!gameMenuPage.isLoadingComplete()) {
                return;
            }
        }
        super.fragmentDataLoadComplete();
    }

    @Override
    public void onFragmentContentVisible() {
        fragmentDataLoadComplete();
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
        // nothing needed
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        // nothing needed
    }

    Handler  pageSoundPlayHandler = new Handler();
    Runnable lastRunnable         = null;

    @Override
    public void onPageSelected(final int position) {
        // resetuje hlavni menu, aby byly dvere a ostatni prvky zavrene
        if ((startMenu != null) && (position == 1)) {
            startMenu.resetOffice();
        }
        // position - 1 -> first page is start menu
        final GamesDefinition gameDefinition = GamesDefinition.getGameAtPosition(position - NO_GAME_PAGES_COUNT);
        selectedPageNumber = position;
        selectGameMenuItem((MenuActivity) getActivity(), position - NO_GAME_PAGES_COUNT);

        if (getTablexiaContext().getSelectedUser() != null) {
            muteAllSounds();
        }
        if (lastRunnable != null) {
            pageSoundPlayHandler.removeCallbacks(lastRunnable);
            lastRunnable = null;
        }
        if (gameDefinition != null) { // zpozdeni zvuku o 2s po zobrazeni stranky
            pageSoundPlayHandler.postDelayed(lastRunnable = new Runnable() {
                @Override
                public void run() {
                    playSound(gameDefinition.getIntroSoundPath(), true);
                }
            }, 2000);
        }
    }

    /**
     * Scroll to start page
     *
     * @return
     */
    public boolean resetMenu() {
        if ((gamesPager != null) && (gamesPager.getCurrentItem() == START_SCREEN_POSITION)) {
            return false;
        } else {
            goToPage(START_SCREEN_POSITION, true);
            return true;
        }
    }

    /**
     * Scroll to page number in parameter
     *
     * @param pageNumber number of page
     * @param smoothScroll <code>true</code> use smooth scroll
     */
    private void goToPage(final int pageNumber, final boolean smoothScroll) {
        if (gamesPager != null) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    gamesPager.setCurrentItem(pageNumber, smoothScroll);
                    if (!smoothScroll) { // reset paralax vrstev
                        Object page = ((GameMenuAdapter) gamesPager.getAdapter()).getItem(pageNumber);
                        if (page instanceof BaseGameMenuFragment) {
                            ((BaseGameMenuFragment) page).resetParalax();
                        }
                    }
                }
            });
        } else {
            selectedPageNumber = pageNumber;
        }
    }

    private void selectGameMenuItem(MenuActivity menuActivity, int gamePosition) {
        if (gamePosition == -1) {
            menuActivity.getTablexiaContext().prepareMainMenu().changeActiveMenuItem(MainMenuDefinition.START);
        } else {
            menuActivity.getTablexiaContext().prepareMainMenu().changeActiveMenuItem(GamesDefinition.getGameAtPosition(gamePosition));
        }
    }

    /**
     * Scroll to page at position
     *
     * @param position page position to scroll
     */
    public void selectGame(int position, MenuActivity menuActivity) {
        selectGame(position, menuActivity, true);
    }

    /**
     * Scroll to page at position
     *
     * @param position page position to scroll
     * @param smoothScroll scroll to position
     */
    public void selectGame(final int position, final MenuActivity menuActivity, final boolean smoothScroll) {
        // scroll to i + 1 -> first page is panorama menu
        goToPage(position + GameMenu.NO_GAME_PAGES_COUNT, smoothScroll);
        selectGameMenuItem(menuActivity, position);
    }
}
