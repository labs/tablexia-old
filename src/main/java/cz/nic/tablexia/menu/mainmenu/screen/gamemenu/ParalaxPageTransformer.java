/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.menu.mainmenu.screen.gamemenu;

import android.support.v4.view.ViewPager.PageTransformer;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import cz.nic.tablexia.R;

/**
 * @author lhoracek
 */
public class ParalaxPageTransformer implements PageTransformer {
    public static final String  TAG                = ParalaxPageTransformer.class.getSimpleName();
    private static final float  PARALAX_CONSTRAINT = 0.7f;
    private static final int    MAX_PARALAX_WIDTH  = 1300;
    private static final String NO_PARALAX_TAG     = "no-paralax";

    /*
     * (non-Javadoc)
     * @see android.support.v4.view.ViewPager.PageTransformer#transformPage(android.view.View, float)
     */
    @Override
    public void transformPage(View page, float position) {
        if (page.getWidth() < MAX_PARALAX_WIDTH) {
            if ((position > -1) && (position < 1)) {
                View firstChild = ((ViewGroup) page).getChildAt(1);
                if ((firstChild != null) && (firstChild instanceof ViewGroup) && (((ViewGroup) firstChild).getChildCount() > 0)) {
                    View secondChild = ((ViewGroup) firstChild).getChildAt(0);
                    if (secondChild.getTag() == null) {
                        RelativeLayout paralax = (RelativeLayout) firstChild.findViewById(R.id.screen_gamemenu_paralax_image);
                        if (paralax != null) {
                            Object tag = paralax.getTag();
                            secondChild.setTag(tag);
                        } else {
                            secondChild.setTag(NO_PARALAX_TAG);
                        }
                    }
                    Object tag = secondChild.getTag();
                    if ((tag != null) && (tag instanceof ImageView[])) {
                        ImageView[] backgroundLayers = (ImageView[]) tag;
                        if (backgroundLayers.length > 0) {
                            backgroundLayers[0].setTranslationX(backgroundLayers[0].getMeasuredWidth() * -position * PARALAX_CONSTRAINT);
                        }
                        if (backgroundLayers.length > 2) {
                            backgroundLayers[2].setTranslationX(backgroundLayers[2].getMeasuredWidth() * position * PARALAX_CONSTRAINT);
                        }
                    }
                }
            }
        }
    }
}
