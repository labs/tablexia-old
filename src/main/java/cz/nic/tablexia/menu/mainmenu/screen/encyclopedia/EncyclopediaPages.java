/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.menu.mainmenu.screen.encyclopedia;

import cz.nic.tablexia.R;

/**
 * @author lhoracek
 */
public enum EncyclopediaPages {
    ABOUT_ENCYCLOPEDIA("uvod.html", R.string.encyclopedia_intro),
    DYSGRAFIA("dysgrafie.html", R.string.encyclopedia_dysgrafia), // dysgrafie
    DYSLEXIA("dyslexie.html", R.string.encyclopedia_dyslexia), // dyslexie
    DYSORTOGRAFIA("dysortografie.html", R.string.encyclopedia_dysortografia), // dysortografie
    MEMORY("pamet.html", R.string.encyclopedia_memory), // paměť
    SPACIAL_ORIENTATION("prostorova_orientace.html", R.string.encyclopedia_spacial_orientation), // prostorová orientace
    SERIALITY("serialita.html", R.string.encyclopedia_seriality), // serialita
    FAMOUS_DYSLECTICS("znami_dyslektici.html", R.string.encyclopedia_famous_dyslectics), // známí dyslektici
    SOUND_RESOLUTION("sluchove_rozlisovani.html", R.string.encyclopedia_sound_resolution), // sluchové rozlišování
    LEARNINC_DISABILITIES("specificke_poruchy_uceni.html", R.string.encyclopedia_learning_disabilities), // specifické poruchy učení
    VISUAL_RESOLUTION("zrakove_rozlisovani.html", R.string.encyclopedia_visual_resolution); // zrakové rozlišování

    private final String contentFile;
    private final int    descriptionKey;

    private EncyclopediaPages(String contentFile, int description) {
        this.contentFile = contentFile;
        descriptionKey = description;
    }

    public String getContentFile() {
        return contentFile;
    }

    public int getDescriptionKey() {
        return descriptionKey;
    }

}
