
package cz.nic.tablexia.menu.mainmenu.screen.gamemenu;

import cz.nic.tablexia.menu.mainmenu.screen.AbstractMenuFragment;

/**
 * @author Matyáš Latner
 */
public class GameMenuFragment extends AbstractMenuFragment {

    public GameMenuFragment(int layoutId, boolean inflateinUIThread, String introSoundPath) {
        super(layoutId, inflateinUIThread, introSoundPath);
    }

    public GameMenuFragment(int layoutId, String introSoundPath) {
        super(layoutId, introSoundPath);
    }

    @Override
    protected void fragmentDataLoadComplete() {
        if (!loadingComplete) {
            loadingComplete = true;
            fragmentContentVisible();
            onFragmentResume();
        }
    }

}
