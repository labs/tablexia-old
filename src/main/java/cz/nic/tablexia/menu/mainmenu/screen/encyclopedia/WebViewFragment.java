/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.menu.mainmenu.screen.encyclopedia;

import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import cz.nic.tablexia.R;
import cz.nic.tablexia.menu.mainmenu.screen.AbstractMenuFragment;
import cz.nic.tablexia.util.OpenBrowserWebViewClient;

public abstract class WebViewFragment extends AbstractMenuFragment {

    protected WebView mWebView;
    private boolean   builtInZoomEnabled;

    public WebViewFragment() {
        super(R.layout.tablexia_webview, true, null);
    }

    public WebViewFragment(int layoutID, boolean builtInZoomEnabled) {
        super(layoutID, true, null);
        this.builtInZoomEnabled = builtInZoomEnabled;
    }

    @Override
    protected void syncFragmentDataLoad(View layoutView) {
        View containerView = layoutView.findViewById(R.id.webView_container);
        if (containerView != null) {
        	ViewGroup container = (ViewGroup) containerView;
            mWebView = new WebView(getTablexiaContext());
            container.addView(mWebView);
            mWebView.loadUrl(getUrl());
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.getSettings().setSaveFormData(true);
            mWebView.getSettings().setBuiltInZoomControls(builtInZoomEnabled);
            mWebView.setWebViewClient(new OpenBrowserWebViewClient(getActivity()));
        }
    }
    
    @Override
    public void onDestroyView() {
    	super.onDestroyView();
    	mWebView = null;
    }

    @Override
    public void onFragmentResume() {
        if (mWebView != null) {            
            mWebView.onResume();
        }
    }

    protected abstract String getUrl();

    /**
     * Called when the fragment is visible to the user and actively running. Resumes the WebView.
     */
    @Override
    public void onPause() {
        super.onPause();
        if (mWebView != null) {
            mWebView.onPause();
        }
    }

    /**
     * Called when the fragment is no longer in use. Destroys the internal state of the WebView.
     */
    @Override
    public void onDestroy() {
        if (mWebView != null) {
            mWebView.destroy();
            mWebView = null;
        }
        super.onDestroy();
    }
}
