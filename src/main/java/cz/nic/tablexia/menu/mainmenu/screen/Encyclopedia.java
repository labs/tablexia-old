/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.menu.mainmenu.screen;

import java.util.HashMap;
import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.widget.SlidingPaneLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;

import cz.nic.tablexia.R;
import cz.nic.tablexia.audio.resources.PermanentSounds;
import cz.nic.tablexia.audio.resources.SpeechSounds;
import cz.nic.tablexia.menu.MenuActivity;
import cz.nic.tablexia.menu.mainmenu.MainMenuDefinition;
import cz.nic.tablexia.menu.mainmenu.screen.encyclopedia.EncyclopediaPage;
import cz.nic.tablexia.menu.mainmenu.screen.encyclopedia.EncyclopediaPages;

/**
 * Encyclopedia screen fragment.
 *
 * @author Matyáš Latner
 */
public class Encyclopedia extends AbstractMenuFragment {

    ViewPager         viewPager;
    ListView          pageList;
    ImageButton       btnBackToOffice;
    ImageButton       btnFontPlus;
    ImageButton       btnFontMinus;
    ScaleValues       zoom;
    SlidingPaneLayout slider;
    ImageView         stinPageListTop;
    ImageView         stinPageListBottom;

    private enum ScaleValues {
        DEFAULT(1),
        MEDIUM(1.2f),
        HIGH(1.5f);

        private final float value;

        private ScaleValues(float value) {
            this.value = value;
        }

        public float getValue() {
            return value;
        }

    }

    public Encyclopedia() {
        super(R.layout.screen_encyclopedia, SpeechSounds.ENCYCLOPEDIA_INTRO);
    }

    @Override
    protected void playIntroSound() {
        if (getTablexiaContext().getSelectedUser().getPlayCountEncyclopedia() < 3) {
            super.playIntroSound();
            getTablexiaContext().getSelectedUser().addPlayCountEncyclopedia();
        }
    }

    @Override
    protected void syncFragmentDataLoad(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.screen_encyclopedia);
        pageList = (ListView) view.findViewById(R.id.screen_encyclopedia_listview_pages);
        btnBackToOffice = (ImageButton) view.findViewById(R.id.screen_encyclopedia_previous_button);
        btnFontPlus = (ImageButton) view.findViewById(R.id.screen_encyclopedia_fontplus_button);
        btnFontMinus = (ImageButton) view.findViewById(R.id.screen_encyclopedia_fontminus_button);
        slider = (SlidingPaneLayout) view.findViewById(R.id.screen_encyclopedia_slidingpanel);
        stinPageListTop = (ImageView) view.findViewById(R.id.screen_encyclopedia_imageView_stin_top);
        stinPageListBottom = (ImageView) view.findViewById(R.id.screen_encyclopedia_imageView_stin_bottom);
        zoom = ScaleValues.DEFAULT;

        viewPager.setAdapter(new EncyclopediaAdapter(getChildFragmentManager()));
        viewPager.setScaleX(zoom.getValue());
        viewPager.setScaleY(zoom.getValue());

        if (btnBackToOffice != null) {
            btnBackToOffice.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    backToOffice();

                }
            });
        }

        if (btnFontPlus != null) {
            btnFontPlus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (zoom != ScaleValues.HIGH) {
                        zoom = ScaleValues.values()[zoom.ordinal() + 1];
                        EncyclopediaAdapter adapter = (EncyclopediaAdapter) viewPager.getAdapter();
                        for (int i = 0; i < adapter.getCount(); i++) {
                            EncyclopediaPage currentPage = adapter.getItem(i);
                            currentPage.scaleView(zoom.getValue());
                        }
                    }
                }
            });
        }
        if (btnFontMinus != null) {
            btnFontMinus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (zoom != ScaleValues.DEFAULT) {
                        zoom = ScaleValues.values()[zoom.ordinal() - 1];
                        EncyclopediaAdapter adapter = (EncyclopediaAdapter) viewPager.getAdapter();
                        for (int i = 0; i < adapter.getCount(); i++) {
                            EncyclopediaPage currentPage = adapter.getItem(i);
                            currentPage.scaleView(zoom.getValue());
                        }
                    }
                }
            });
        }

        final ArrayAdapter<EncyclopediaPages> encyclopediaAdapter = new ArrayAdapter<EncyclopediaPages>(getActivity(), R.layout.screen_encyclopedia_list_item, EncyclopediaPages.values()) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                TextView textView = (TextView) v.findViewById(R.id.text1);
                textView.setText(getItem(position).getDescriptionKey());
                if (position == viewPager.getCurrentItem()) {
                    v.setBackgroundResource(R.drawable.screen_encyclopedia_bookmark);
                } else {
                    v.setBackgroundResource(R.color.screen_encyclopedia_background);
                }
                return v;
            }

        };
        pageList.setAdapter(encyclopediaAdapter);
        viewPager.setOffscreenPageLimit(EncyclopediaPages.values().length);
        pageList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                viewPager.setCurrentItem(position);
                encyclopediaAdapter.notifyDataSetChanged();
                if (slider != null) {
                    slider.closePane();
                }
            }

        });
        pageList.setOnScrollListener(new OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // nic
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                stinPageListTop.setVisibility(firstVisibleItem > 0 ? View.VISIBLE : View.GONE);
                stinPageListBottom.setVisibility(((firstVisibleItem + visibleItemCount) >= totalItemCount) ? View.GONE : View.VISIBLE);
            }
        });
        viewPager.setOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                EasyTracker.getInstance(getActivity()).send(MapBuilder.createEvent("ui_action", "encyclopedia_page", String.valueOf(position), 0l).build());
                // playSound(SfxSounds.ENCYCLOPEDIA_PAGING, false);
                getTablexiaContext().getGlobalSoundControl().stopMedia(SpeechSounds.ENCYCLOPEDIA_INTRO);
                encyclopediaAdapter.notifyDataSetChanged();
                for (int i = 0; i < encyclopediaAdapter.getCount(); i++) {
                    ((EncyclopediaAdapter) viewPager.getAdapter()).getItem(i).resetSound();
                }
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // nothing
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // nothing
            }
        });
        if (slider != null) {
            slider.setSliderFadeColor(getResources().getColor(R.color.dark_tansparent));
            slider.openPane();
        }
        fragmentVisible = true;
    }

    private void backToOffice() {
        playSound(PermanentSounds.BUTTON_SOUND, false);
        ((MenuActivity) getActivity()).getTablexiaContext().prepareMainMenu().changeMenuItem(MainMenuDefinition.START, true, false);
    }

    public class EncyclopediaAdapter extends FragmentPagerAdapter {

        public EncyclopediaAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return EncyclopediaPages.values().length;
        }

        Map<EncyclopediaPages, EncyclopediaPage> fragmentInstances = new HashMap<EncyclopediaPages, EncyclopediaPage>();

        @Override
        public EncyclopediaPage getItem(int position) {
            EncyclopediaPages page = EncyclopediaPages.values()[position];
            if (fragmentInstances.get(page) != null) {
                return fragmentInstances.get(page);
            }
            Bundle bundle = new Bundle();
            bundle.putString(EncyclopediaPages.class.getName(), page.getContentFile());
            EncyclopediaPage fragment = (EncyclopediaPage) Fragment.instantiate(getActivity(), EncyclopediaPage.class.getName(), bundle);
            fragmentInstances.put(page, fragment);
            return fragment;
        }
    }

}
