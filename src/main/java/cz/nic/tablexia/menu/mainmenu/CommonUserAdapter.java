/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.menu.mainmenu;

import java.util.List;

import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import cz.nic.tablexia.R;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.menu.usermenu.User;
import cz.nic.tablexia.util.Utility;

public class CommonUserAdapter extends BaseAdapter {

    private static final int USER_MENU_ITEM_ID       = R.layout.mainmenu_userspinner_item;
    private static final int USER_MENU_ITEM_TEXT_ID  = R.id.usermenu_text;
    private static final int USER_MENU_ITEM_IMAGE_ID = R.id.usermenu_image;
    private static final int USER_MENU_NEWITEM_ID    = R.layout.usermenu_newuser;

    private List<User>      users;
    private Tablexia        tablexiaContext;

    public CommonUserAdapter(Tablexia tablexiaContext) {
        this.tablexiaContext = tablexiaContext;
        refresh();
    }

    public CommonUserAdapter refresh() {
        users = User.getAllUsers();
        return this;
    }

    public int getUserCount() {
        return users.size();
    }

    @Override
    public int getCount() {
        // users plus new user button
        return users.size() + 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (position == users.size()) {
            convertView = Utility.prepareItemView(tablexiaContext, convertView, USER_MENU_NEWITEM_ID);
        } else {
            convertView = Utility.prepareItemView(tablexiaContext, convertView, USER_MENU_ITEM_ID);
            User user = users.get(position);

            TextView userText = (TextView) convertView.findViewById(USER_MENU_ITEM_TEXT_ID);
            userText.setText(user.getNickName());

            String avatar = user.getAvatar();
            if (avatar != null) {
                ImageView userImage = (ImageView) convertView.findViewById(USER_MENU_ITEM_IMAGE_ID);
                userImage.setImageResource(tablexiaContext.getResources().getIdentifier(avatar, "drawable", tablexiaContext.getPackageName()));
            }

            User selectedUser = tablexiaContext.getSelectedUser();
            if ((selectedUser != null) && (user.getId() == selectedUser.getId())) {
                userText.setTypeface(Typeface.DEFAULT_BOLD);
                convertView.findViewById(R.id.usermenu_selector).setVisibility(View.VISIBLE);
            } else {
                userText.setTypeface(Typeface.DEFAULT);
                convertView.findViewById(R.id.usermenu_selector).setVisibility(View.GONE);
            }

            if (position == (users.size() - 1)) {
                convertView.findViewById(R.id.usermenu_separator).setVisibility(View.GONE);
            } else {
                convertView.findViewById(R.id.usermenu_separator).setVisibility(View.VISIBLE);
            }

        }
        return convertView;
    }

    /**
     * Returns user at specific position or <code>null</code>
     *
     * @param position position of user
     * @return user at specific position or <code>null</code>
     */
    public User getUserAtPosition(int position) {
        if ((position >= 0) && (position < users.size())) {
            return users.get(position);
        }
        return null;
    }

    /**
     * Returns position of user in list by user nickName
     *
     * @param nickName nickName of user
     * @return position of user in list by user nickName
     */
    public int getUserPositionInList(String nickName) {
        if ((nickName != null) && (nickName.length() > 0)) {
            for (int i = 0; i < users.size(); i++) {
                User user = users.get(i);
                if (nickName.equals(user.getNickName())) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public Object getItem(int arg0) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (position < users.size()) {
            Long id = users.get(position).getId();
            if (id != null) {
                return id.longValue();
            }
        }
        return -1;
    }

}
