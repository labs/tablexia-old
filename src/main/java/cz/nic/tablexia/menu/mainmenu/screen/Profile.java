/**
 *
 */

package cz.nic.tablexia.menu.mainmenu.screen;

import java.io.File;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import cz.nic.tablexia.R;
import cz.nic.tablexia.audio.resources.PermanentSounds;
import cz.nic.tablexia.menu.mainmenu.MainMenuDefinition;
import cz.nic.tablexia.menu.usermenu.User;
import cz.nic.tablexia.util.FileHelper;

/**
 * @author lhoracek
 */
public class Profile extends AbstractMenuFragment {

    private ImageView   imageViewAvatar;
    private ImageView   imageViewSignature;
    private EditText    editTextName;
    private TextView    textViewAge;
    private SeekBar     seekbarSex;
    private View        imageViewSexFemale;
    private View        imageViewSexMale;
    private ImageView   imageViewProfileSex;
    private View        layoutSexSeekBar;
    private ImageView   imageViewStamp;
    private ImageView   imageViewPen;
    private View        buttonAgePlus;
    private View        buttonAgeMinus;
    private View        buttonSubscribe;
    private View        layoutSubscribe;
    private View        imageViewStamper;
    private ImageButton buttonClose;

    private View        layoutHelp;

    public Profile() {
        super(R.layout.newuser_fragment, true, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Find views
        imageViewAvatar = (ImageView) view.findViewById(R.id.newuser_mugshot_image);
        imageViewSignature = (ImageView) view.findViewById(R.id.newuser_signature_image);
        editTextName = (EditText) view.findViewById(R.id.newuser_covername_text);
        textViewAge = (TextView) view.findViewById(R.id.newuser_age_text);
        seekbarSex = (SeekBar) view.findViewById(R.id.newuser_sex_seekbar);
        imageViewStamp = (ImageView) view.findViewById(R.id.newuser_stampplaceholder_image);
        layoutHelp = view.findViewById(R.id.newuser_help_layout);
        imageViewPen = (ImageView) view.findViewById(R.id.newuser_pen_image);
        buttonAgeMinus = view.findViewById(R.id.newuser_minusAge_image);
        buttonAgePlus = view.findViewById(R.id.newuser_plusAge_image);
        buttonSubscribe = view.findViewById(R.id.newuser_button_subscribe);
        layoutSubscribe = view.findViewById(R.id.layout_subscription_field);
        buttonClose = (ImageButton) view.findViewById(R.id.imageButton_closebutton);
        imageViewStamper = view.findViewById(R.id.newuser_stamper_image);

        imageViewSexFemale = view.findViewById(R.id.newuser_sex_female_image);
        imageViewSexMale = view.findViewById(R.id.newuser_sex_male_image);
        imageViewProfileSex = (ImageView) view.findViewById(R.id.newuser_sex_profile_image);
        layoutSexSeekBar = view.findViewById(R.id.newuser_layout_sex_seekbar);

        // Set user values
        User user = getTablexiaContext().getSelectedUser();
        imageViewAvatar.setImageDrawable(getResources().getDrawable(getResources().getIdentifier(user.getAvatar(), "drawable", getActivity().getPackageName())));
        if (imageViewSignature != null) {
            Bitmap bitmap = BitmapFactory.decodeFile(FileHelper.getImageFileDir(getActivity()) + File.separator + user.getSignatureBmpPath());
            imageViewSignature.setImageBitmap(bitmap);
        }
        editTextName.setText(user.getNickName());
        editTextName.setEnabled(false);
        textViewAge.setText(String.valueOf(user.getAge()));

        layoutSexSeekBar.setVisibility(View.GONE);
        imageViewSexFemale.setVisibility(View.GONE);
        imageViewSexMale.setVisibility(View.GONE);
        imageViewProfileSex.setImageDrawable(getResources().getDrawable((user.isMale() ? R.drawable.newuser_sex_male : R.drawable.newuser_sex_female)));
        imageViewProfileSex.setVisibility(View.VISIBLE);

        if (imageViewStamp != null) {
            imageViewStamp.setImageDrawable(getResources().getDrawable(R.drawable.newuser_stamp));
        }
        if (imageViewStamper != null) {
            imageViewStamper.setVisibility(View.GONE);
        }

        // Hide help layout
        layoutHelp.setVisibility(View.GONE);
        if (imageViewPen != null) {
            Point displaySize = new Point();
            getActivity().getWindowManager().getDefaultDisplay().getSize(displaySize);
            imageViewPen.setTranslationX(0.9f * displaySize.x);
            imageViewPen.setTranslationY(0.5f * displaySize.y);
        }

        if (buttonSubscribe != null) {
            buttonSubscribe.setVisibility(View.GONE);
            layoutSubscribe.setVisibility(View.VISIBLE);
        }

        if (buttonClose != null) {
            buttonClose.setVisibility(View.VISIBLE);
            buttonClose.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    playSound(PermanentSounds.BUTTON_SOUND, false);
                    buttonClose.setPressed(true);
                    getTablexiaContext().prepareMainMenu().changeMenuItem(MainMenuDefinition.START, true, false);
                }

            });
        }

        buttonAgePlus.setVisibility(View.GONE);
        buttonAgeMinus.setVisibility(View.GONE);
    }
}
