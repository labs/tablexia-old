/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.menu.mainmenu.screen.gamemenu;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import cz.nic.tablexia.R;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.GamesDefinition;
import cz.nic.tablexia.game.manager.GameManager;
import cz.nic.tablexia.menu.mainmenu.screen.GameMenu;
import cz.nic.tablexia.menu.usermenu.User;
import cz.nic.tablexia.util.DensityUtils;

/**
 * @author lhoracek, Matyáš Latner
 */
public class BaseGameMenuFragment extends GameMenuFragment implements OnClickListener, OnTouchListener {

    public BaseGameMenuFragment() {
        super(R.layout.screen_gamemenu_gamefragment, null);
    }

    private static final GameDifficulty INITIAL_DIFFICULTY = GameDifficulty.EASY;

    private GameDifficulty              selectedDifficulty;
    private GamesDefinition             currentGame;
    private int                         gameNumber;
    private View                        layout;
    private ImageButton                 startButton;
    private boolean                     startButtonPressed;
    private ImageView[]                 backgroundLayers;

    @Override
    protected void asyncFragmentDataLoad(View layoutView) {
        layout = layoutView;
        gameNumber = getArguments().getInt(GameMenu.BUNDLE_GAME_KEY);
        currentGame = GamesDefinition.getGameAtPosition(gameNumber);
        layout.setOnTouchListener(this);

        prepareParalax(layout);
        prepareTitleImage(layout, currentGame.getGameTitleImageResourceId());
        prepareStartButton(layout, currentGame.getGameStartImageResourceId());

        startButtonPressed = false;
    }

    @Override
    public void onFragmentResume() {
        selectedDifficulty = INITIAL_DIFFICULTY;
        User selectedUser = getMenuActivity().getTablexiaContext().getSelectedUser();
        if (selectedUser != null) {
            GameManager lastGameManager = GameManager.getLastGameManagerRegardlessFinishing(selectedUser, currentGame);
            if (lastGameManager != null) {
                selectedDifficulty = lastGameManager.getDifficultyEnum();
            }
        }
        prepareDifficultySeekBar(layout);
    }

    /* //////////////////////////////////////////// FRAGMENT LIFECYCLE */

    private void prepareParalax(View layout) {
        if (!DensityUtils.lowMemorySettings(getActivity())) {
            RelativeLayout paralax = (RelativeLayout) layout.findViewById(R.id.screen_gamemenu_paralax_image);
            backgroundLayers = new ImageView[currentGame.getGameImageResourceIds().length];

            for (int i = 0; i < currentGame.getGameImageResourceIds().length; i++) {

                // hack, protoze obrazky na touhle vrstvou by mohly byt ma sirku vetsi, aby fungoval paralaz i za kraj obrazovky
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(i > 1 ? LayoutParams.WRAP_CONTENT : LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
                lp.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

                ImageView layer = new ImageView(getActivity());
                layer.setAdjustViewBounds(true);
                layer.setImageResource(currentGame.getGameImageResourceIds()[i]);
                layer.setScaleType(ScaleType.CENTER_CROP);
                paralax.addView(layer, lp);
                backgroundLayers[i] = layer;
            }
            paralax.setTag(backgroundLayers);
        } else {
            Bitmap result = Bitmap.createBitmap(640, 360, Bitmap.Config.ARGB_8888);
            Rect destRect = new Rect(0, 0, 640, 360);
            Canvas canvas = new Canvas(result);
            Paint paint = new Paint();

            for (int i = 0; i < currentGame.getGameImageResourceIds().length; i++) {
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), currentGame.getGameImageResourceIds()[i]);
                Rect srcRect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
                canvas.drawBitmap(bitmap, srcRect, destRect, paint);
            }
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            lp.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            RelativeLayout paralax = (RelativeLayout) layout.findViewById(R.id.screen_gamemenu_paralax_image);
            ImageView layer = new ImageView(getActivity());
            layer.setAdjustViewBounds(true);
            layer.setImageBitmap(result);
            layer.setScaleType(ScaleType.CENTER_CROP);
            paralax.addView(layer, lp);
        }
    }

    /* //////////////////////////////////////////// START BUTTON CLICK LISTENER */

    @Override
    public void onClick(View v) {
        getMenuActivity().getTablexiaContext().hideScreenContent(false, new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                GamesDefinition currentGame = GamesDefinition.getGameAtPosition(gameNumber);
                Intent intent = new Intent(getActivity(), currentGame.getGameClass());
                GameDifficulty.addToIntentExtra(intent, selectedDifficulty);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                getActivity().startActivity(intent);
            }
        });
    }

    /*
     * ////////////START BUTTON ON TOUCH LISTENER
     */
    Point firstTouch = new Point();

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                firstTouch.x = (int) event.getX();
                firstTouch.y = (int) event.getY();
                break;

            case MotionEvent.ACTION_MOVE:
                startButton.setPressed(true);
                if (((int) event.getY() < (firstTouch.x - (startButton.getHeight() / 2)))) {
                    if (!startButtonPressed) {
                        getMenuActivity().getTablexiaContext().hideScreenContent(false, new AnimatorListenerAdapter() {

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                GamesDefinition currentGame = GamesDefinition.getGameAtPosition(gameNumber);
                                Intent intent = new Intent(getActivity(), currentGame.getGameClass());
                                GameDifficulty.addToIntentExtra(intent, selectedDifficulty);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                getActivity().startActivity(intent);
                            }

                        });
                        startButtonPressed = true;
                    }
                }
                break;
        }
        return false;
    }

    /* //////////////////////////////////////////// UTILITY */

    /**
     * Prepares start button
     *
     * @param layout
     * @param imageResourceId
     */
    private void prepareTitleImage(View layout, int imageResourceId) {
        ImageView titleImage = (ImageView) layout.findViewById(R.id.screen_gamemenu_titleimage);
        titleImage.setImageResource(imageResourceId);
    }

    /**
     * Prepares start button
     *
     * @param layout
     * @param imageResourceId
     */
    private void prepareStartButton(View layout, int imageResourceId) {
        startButton = (ImageButton) layout.findViewById(R.id.screen_gamemenu_startbutton);
        startButton.setImageResource(imageResourceId);
        startButton.setOnClickListener(this);
        startButton.setOnTouchListener(this);
    }

    /**
     * Prepares difficulty seekbar
     *
     * @param layout
     */
    private void prepareDifficultySeekBar(View layout) {

        final SeekBar difficultySeekbar = (SeekBar) layout.findViewById(R.id.screen_gamemenu_difficultyseekbar);
        difficultySeekbar.incrementProgressBy(1);
        difficultySeekbar.setMax(100);

        difficultySeekbar.setProgress(calculateDifficultyProgress(difficultySeekbar.getMax(), selectedDifficulty));
        difficultySeekbar.setThumb(getActivity().getResources().getDrawable(selectedDifficulty.getSeekbarThumImageId()));
        difficultySeekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                GameDifficulty currentDificulty = computeCurrentDificulty(seekBar.getProgress());
                if (currentDificulty != null) {
                    int progress = calculateDifficultyProgress(difficultySeekbar.getMax(), currentDificulty);
                    seekBar.setProgress(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // nothing needed
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                changeDifficulty(seekBar.getProgress());
            }

            private GameDifficulty computeCurrentDificulty(int progress) {
                for (GameDifficulty gameDifficulty : GameDifficulty.values()) {
                    if (progress <= ((difficultySeekbar.getMax() / GameDifficulty.values().length) * (gameDifficulty.ordinal() + 1))) {
                        return gameDifficulty;
                    }
                }
                return GameDifficulty.values()[GameDifficulty.values().length - 1];
            }

            private void changeDifficulty(int progress) {
                GameDifficulty currentDificulty = computeCurrentDificulty(progress);
                if (currentDificulty != null) {
                    ((GameMenu) getParentFragment()).playDifficultySeekbarMoveSound(selectedDifficulty, currentDificulty);
                    selectedDifficulty = currentDificulty;
                    difficultySeekbar.setThumb(getActivity().getResources().getDrawable(selectedDifficulty.getSeekbarThumImageId()));
                }
            }

        });
    }

    private int calculateDifficultyProgress(int maximum, GameDifficulty currentDificulty) {
        return (int) (maximum * ((double) currentDificulty.ordinal() / (GameDifficulty.values().length - 1)));
    }

    public void resetParalax() {
        if (backgroundLayers != null) {
            for (ImageView imageView : backgroundLayers) {
                imageView.setTranslationX(0);
            }
        }
    }

}
