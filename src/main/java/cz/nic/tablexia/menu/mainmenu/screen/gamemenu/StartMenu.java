/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.menu.mainmenu.screen.gamemenu;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import cz.nic.tablexia.R;
import cz.nic.tablexia.audio.resources.PermanentSounds;
import cz.nic.tablexia.game.games.GamesDefinition;
import cz.nic.tablexia.menu.MenuActivity;
import cz.nic.tablexia.menu.mainmenu.MainMenuDefinition;
import cz.nic.tablexia.menu.usermenu.User;
import cz.nic.tablexia.util.DensityUtils;
import cz.nic.tablexia.widget.TablexiaImageButton;
import cz.nic.tablexia.widget.TablexiaImageButton.ClickSoundEventType;

/**
 * Start screen menu. Clickable image map mechanism and actions.
 *
 * @author Matyáš Latner
 */
public class StartMenu extends GameMenuFragment implements OnTouchListener {
    private static final String TAG = StartMenu.class.getSimpleName();

    public StartMenu() {
        super(GAMES_STARTMENU_LAYOUT_ID, null);
    }

    private static final int    STREET_COLOR                              = Color.YELLOW;
    private static final int    HALLOFFAME_COLOR                          = Color.GREEN;
    private static final int    STATISTICS_COLOR                          = Color.RED;
    private static final int    ENCYCLOPEDIA_COLOR                        = Color.BLACK;
    private static final int    PROFILE_COLOR                             = Color.BLUE;

    private static final int    GAMES_STARTMENU_LAYOUT_ID                 = R.layout.screen_gamemenu_startmenu;
    private static final int    GAMES_STARTMENU_LAYOUT_LOWMEM_ID          = R.layout.screen_gamemenu_startmenu_lowmem;
    private static final int    GAMES_STARTMENU_CLICKABLEMAP_ID           = R.id.screen_gamemenu_startmenu_clickablemap;
    private static final int    GAMES_STARTMENU_HELPLAYER_ID              = R.id.screen_gamemenu_startmenu_helplayer;
    private static final int    GAMES_STARTMENU_HELPLAYER_CLICKABLEMAP_ID = R.id.screen_gamemenu_startmenu_helplayer_clickablemap;
    private static final int    GAMES_STARTMENU_HELPBUTTON_ID             = R.id.screen_gamemenu_startmenu_helpbutton;

    private static final int    GAMES_STARTMENU_BUTTON_STATISTICS_ID      = R.id.screen_gamemenu_startmenu_statistics;
    private static final int    GAMES_STARTMENU_BUTTON_HALLOFFAME_ID      = R.id.screen_gamemenu_startmenu_halloffame;
    private static final int    GAMES_STARTMENU_BUTTON_ENCYCLOPEDIA_ID    = R.id.screen_gamemenu_startmenu_encyclopedia;
    private static final int    GAMES_STARTMENU_BUTTON_GAMES_ID           = R.id.screen_gamemenu_startmenu_streetdoor;
    private static final int    GAMES_STARTMENU_BUTTON_PROFILE_ID         = R.id.screen_gamemenu_startmenu_profile;

    private View                selectedButton;
    private View                statisticsButton;
    private View                hallOfFameButton;
    private View                encyclopediaButton;
    private View                profileButton;
    private View                gamesButton;

    private View                panoramaMenuLayout;
    private View                helpLayer;
    private TablexiaImageButton helpButton;
    private Bitmap              hotspots;

    @Override
    protected void startLoading() {
        layoutId = DensityUtils.lowMemorySettings(getActivity()) ? GAMES_STARTMENU_LAYOUT_LOWMEM_ID : GAMES_STARTMENU_LAYOUT_ID;
        super.startLoading();
    }

    @Override
    protected void asyncFragmentDataLoad(View layoutView) {
        panoramaMenuLayout = layoutView;

        View maskLayer = panoramaMenuLayout.findViewById(GAMES_STARTMENU_CLICKABLEMAP_ID);
        maskLayer.setOnTouchListener(this);

        selectedButton = null;
        statisticsButton = panoramaMenuLayout.findViewById(GAMES_STARTMENU_BUTTON_STATISTICS_ID);
        hallOfFameButton = panoramaMenuLayout.findViewById(GAMES_STARTMENU_BUTTON_HALLOFFAME_ID);
        encyclopediaButton = panoramaMenuLayout.findViewById(GAMES_STARTMENU_BUTTON_ENCYCLOPEDIA_ID);
        gamesButton = panoramaMenuLayout.findViewById(GAMES_STARTMENU_BUTTON_GAMES_ID);
        profileButton = panoramaMenuLayout.findViewById(GAMES_STARTMENU_BUTTON_PROFILE_ID);

        helpButton = (TablexiaImageButton) panoramaMenuLayout.findViewById(GAMES_STARTMENU_HELPBUTTON_ID);
        helpButton.setClickSoundEventType(ClickSoundEventType.ON_SELECT);
        helpButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                toggleHelpLayer();
            }

        });

        helpLayer = panoramaMenuLayout.findViewById(GAMES_STARTMENU_HELPLAYER_ID);

        View helpMaskLayer = panoramaMenuLayout.findViewById(GAMES_STARTMENU_HELPLAYER_CLICKABLEMAP_ID);
        helpMaskLayer.setOnTouchListener(this);
        prepareClickableMap();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * Show or hide help layer
     */
    private void toggleHelpLayer() {
        if (helpLayer.getVisibility() == View.VISIBLE) {
            hideHelpLayer();
        } else {
            showHelpLayer();
        }
    }

    /**
     * Shows help layer
     */
    @SuppressLint("NewApi")
    private void showHelpLayer() {
        if (helpLayer != null) {
            helpLayer.setAlpha(0f);
            helpLayer.setVisibility(View.VISIBLE);
            helpButton.setSelected(true);
            if (Build.VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN) {
                helpLayer.animate().alpha(1).withLayer();
            } else {
                helpLayer.animate().alpha(1).start();
            }
        }
    }

    /**
     * Hides help layer
     */
    @SuppressLint("NewApi")
    private void hideHelpLayer() {
        if (helpLayer.getVisibility() == View.VISIBLE) {
            User selectedUser = ((MenuActivity) getActivity()).getTablexiaContext().getSelectedUser();
            if (selectedUser.isHelp()) {
                selectedUser.setHelpAndSave(false);
            }
            if (Build.VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN) {
                helpLayer.animate().alpha(0).withEndAction(new Runnable() {

                    @Override
                    public void run() {
                        helpButton.setSelected(false);
                        helpLayer.setVisibility(View.GONE);
                    }

                });
            } else {
                helpLayer.animate().alpha(0).setListener(new AnimatorListener() {

                    @Override
                    public void onAnimationStart(Animator animation) {
                        // nic
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                        // nic
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        helpButton.setSelected(false);
                        helpLayer.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        // nic
                    }
                });
            }

        }
    }

    private void prepareClickableMap() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        Bitmap sourceBitmap = ((BitmapDrawable) getResources().getDrawable(R.drawable.screen_gamemenu_startmenu_clickablemap)).getBitmap();
        hotspots = Bitmap.createScaledBitmap(sourceBitmap, size.x, size.y, false);
    }

    @Override
    public void onFragmentResume() {
        super.onFragmentResume();
        User selectedUser = ((MenuActivity) getActivity()).getTablexiaContext().getSelectedUser();
        if ((selectedUser != null) && selectedUser.isHelp()) {
            showHelpLayer();
        }
    }

    @Override
    public boolean onTouch(View imageView, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            boolean clickedActiveArea = true;
            switch (getTouchedColor(imageView, event.getX(), event.getY())) {
                case ENCYCLOPEDIA_COLOR:
                    encyclopediaAction();
                    break;
                case STATISTICS_COLOR:
                    statisticsAction();
                    break;
                case HALLOFFAME_COLOR:
                    halloffameAction();
                    break;
                case STREET_COLOR:
                    gamesAction();
                    break;
                case PROFILE_COLOR:
                    profileAction();
                    break;
                default:
                    clickedActiveArea = false;
                    break;
            }
            if (clickedActiveArea) {
                playSound(PermanentSounds.BUTTON_SOUND, false);
            }
        }
        hideHelpLayer();
        return true;
    }

    private void profileAction() {
        showUserInteraction(profileButton);
        ((MenuActivity) getActivity()).getTablexiaContext().prepareMainMenu().changeMenuItem(MainMenuDefinition.PROFILE, true, false);
    }

    private void encyclopediaAction() {
        showUserInteraction(encyclopediaButton);
        ((MenuActivity) getActivity()).getTablexiaContext().prepareMainMenu().changeMenuItem(MainMenuDefinition.ENCYCLOPEDIA, true, false);
    }

    private void statisticsAction() {
        showUserInteraction(statisticsButton);
        ((MenuActivity) getActivity()).getTablexiaContext().prepareMainMenu().changeMenuItem(MainMenuDefinition.STATISTICS, true, false);
    }

    private void halloffameAction() {
        showUserInteraction(hallOfFameButton);
        ((MenuActivity) getActivity()).getTablexiaContext().prepareMainMenu().changeMenuItem(MainMenuDefinition.HALL_OF_FAME, true, false);
    }

    private void gamesAction() {
        showUserInteraction(gamesButton);
        ((MenuActivity) getActivity()).getTablexiaContext().prepareMainMenu().changeMenuItem(GamesDefinition.BANKOVNILOUPEZ, false, false);
        panoramaMenuLayout.setBackgroundResource(R.drawable.screen_gamemenu_startmenu_background);
    }

    /**
     * Returns color of clicked image part
     *
     * @param view
     * @param positionX
     * @param positionY
     * @return
     */
    private int getTouchedColor(View view, float positionX, float positionY) {
        if ((positionX < hotspots.getWidth()) && (positionY < hotspots.getHeight())) {
            return hotspots.getPixel((int) positionX, (int) positionY);
        } else {
            return -1;
        }
    }

    /*
     * nasteaveni pozadi kancelare a popredi dle predmetu, na ktery uzivatel kliknul pro vyobrazeni interakce
     */
    private void showUserInteraction(View buttonView) {
        if (buttonView != null) {
            buttonView.setVisibility(View.VISIBLE);
            selectedButton = buttonView;
        }
    }

    public void resetOffice() {
        if (selectedButton != null) {
            selectedButton.setVisibility(View.INVISIBLE);
            selectedButton = null;
        }
    }
}
