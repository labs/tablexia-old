/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.menu.mainmenu.screen;

import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.activeandroid.util.Log;

import cz.nic.tablexia.R;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.main.MainActivity;
import cz.nic.tablexia.menu.MenuActivity;
import cz.nic.tablexia.util.TimeUtils;

public class LoaderFragment extends Fragment {
    private static final int LOADER_BACKGROUND_LAYOUT_ID = R.layout.screen_loader;
    private static final int BIGHAND_ID                  = R.id.screen_loader_bighand;
    private static final int SMALLHAND_ID                = R.id.screen_loader_smallhand;

    private static final int CLOCK_CENTER_X              = 599;
    private static final int CLOCK_CENTER_Y              = 563;
    private static final int BACKGROUND_WIDTH            = 2662;
    private static final int BACKGROUND_HEIGHT           = 1562;

    private View             pageLayout;
    private Display          display;
    private Point            displaySize;
    private ImageView        ivBigHand;
    private ImageView        ivSmallHand;
    private double           ratioX;
    private double           ratioY;
    private double           handsPositionX;
    private double           handsPositionY;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        pageLayout = inflater.inflate(LOADER_BACKGROUND_LAYOUT_ID, null);
        checkWinter(pageLayout);
        displaySize = new Point();
        display = getActivity().getWindowManager().getDefaultDisplay();
        display.getSize(displaySize);
        ivBigHand = (ImageView) pageLayout.findViewById(BIGHAND_ID);
        ivSmallHand = (ImageView) pageLayout.findViewById(SMALLHAND_ID);
        return pageLayout;
    }

    private void checkWinter(View loader) {
        if (TimeUtils.isWinter()) {
            View v = loader.findViewById(R.id.screen_loader_background);
            v.setBackgroundResource(R.drawable.screen_loader_background_winter);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setVersionName(view);
        setHandsInPosition();
        startHandsMove();
    }

    private void setVersionName(View view) {
        TextView versionName = (TextView) view.findViewById(R.id.screen_loader_versionname);
        try {
            String tablexiaName = getResources().getString(R.string.app_name);
            String versioName = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
            versionName.setText(tablexiaName + " " + versioName);
        } catch (NameNotFoundException e) {
            Log.i("Cannot set version name", e);
        }
    }

    /*
     * Metoda nastavi hodinove rucicky na stred ciferniku
     * TODO zaridit, aby nebyly potreba kouzelne konstanty
     */
    public void setHandsInPosition() {
        ratioX = (double) BACKGROUND_WIDTH / (double) displaySize.x;
        ratioY = (double) BACKGROUND_HEIGHT / (double) displaySize.y;

        handsPositionX = CLOCK_CENTER_X / ratioX;
        handsPositionY = CLOCK_CENTER_Y / ratioY;

        ivBigHand.setPivotX(0);
        ivBigHand.setPivotY(0);
        ivSmallHand.setPivotX(0);
        ivSmallHand.setPivotY(0);

        float scaleX = (float) (1 / ratioX);
        float scaleY = (float) (1 / ratioY);

        ivBigHand.setScaleX(scaleX);
        ivBigHand.setScaleY(scaleY);
        ivSmallHand.setScaleX(scaleX);
        ivSmallHand.setScaleY(scaleY);

        ivBigHand.setTranslationX((float) (handsPositionX - (ivBigHand.getDrawable().getMinimumWidth() / ratioX / 2)));
        ivBigHand.setTranslationY((float) (handsPositionY - (ivBigHand.getDrawable().getMinimumHeight() / ratioY)));
        ivSmallHand.setTranslationX((float) (handsPositionX - (ivSmallHand.getDrawable().getMinimumWidth() / ratioX / 2)));
        ivSmallHand.setTranslationY((float) (handsPositionY - (ivSmallHand.getDrawable().getMinimumHeight() / ratioY)));

    }

    private void startHandsMove() {
        float rotationCenterX = (float) (handsPositionX);
        float rotationCenterY = (float) (handsPositionY);
        Animation ani = new RotateAnimation(0, /* from degree */
            360, /* to degree */
            Animation.ABSOLUTE, rotationCenterX, Animation.ABSOLUTE, rotationCenterY);
        ani.setDuration(500);
        ani.setFillAfter(false);
        ani.setRepeatCount(Animation.INFINITE);
        ani.setRepeatMode(Animation.RESTART);
        ani.setInterpolator(new LinearInterpolator());
        ivBigHand.startAnimation(ani);

        Animation aniSH = new RotateAnimation(0, /* from degree */
            360, /* to degree */
            Animation.ABSOLUTE, rotationCenterX, Animation.ABSOLUTE, rotationCenterY);
        aniSH.setDuration(1500);
        aniSH.setFillAfter(false);
        aniSH.setRepeatCount(Animation.INFINITE);
        aniSH.setRepeatMode(Animation.RESTART);
        aniSH.setInterpolator(new LinearInterpolator());
        ivSmallHand.startAnimation(aniSH);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getTablexiaContext().showScreenContent(null);
    }

    protected Tablexia getTablexiaContext() {
        return ((MenuActivity) getActivity()).getTablexiaContext();
    }

    protected MenuActivity getMenuActivity() {
        return (MenuActivity) getActivity();
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if ((LoaderFragment.this.getArguments() != null) && LoaderFragment.this.getArguments().containsKey("NEWUSER_GAMEMENU")) {
            Animation anim = AnimationUtils.loadAnimation(getActivity(), nextAnim);

            anim.setAnimationListener(new AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {
                    // nothing
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                    // nothing
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    ((MainActivity) getMenuActivity()).showNewPlayerAnimation();

                }

            });
            return anim;
        }
        return null;
    }
}
