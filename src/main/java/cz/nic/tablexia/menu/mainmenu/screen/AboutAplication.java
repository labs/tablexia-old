/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.menu.mainmenu.screen;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import cz.nic.tablexia.R;
import cz.nic.tablexia.audio.resources.PermanentSounds;
import cz.nic.tablexia.menu.mainmenu.MainMenuDefinition;
import cz.nic.tablexia.menu.mainmenu.screen.encyclopedia.WebViewFragment;

/**
 * About application screen fragment.
 * 
 * @author MatyĂˇĹˇ Latner
 */
public class AboutAplication extends WebViewFragment implements OnClickListener {

    private static final String ABOUT_FILE  = "file:///android_asset/oaplikaci/index.html";
    private static final int    FRAGMENT_ID  = R.layout.screen_about;
    private ImageButton         btnBack;

    public AboutAplication() {
        super(FRAGMENT_ID, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnBack = (ImageButton) view.findViewById(R.id.screen_about_back_button);
        btnBack.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                playSound(PermanentSounds.BUTTON_SOUND, false);
                btnBack.setPressed(true);
                getTablexiaContext().prepareMainMenu().changeMenuItem(MainMenuDefinition.START, true, false);

            }
        });
    }

    @Override
    protected String getUrl() {
        return ABOUT_FILE;
    }

    @Override
    public void onClick(View v) {
        // nic

    }

    @Override
    public void onFragmentResume() {
        super.onFragmentResume();
        mWebView.getSettings().setBuiltInZoomControls(false);
        mWebView.setWebViewClient(getWebViewClient());
    }

    protected WebViewClient getWebViewClient() {
        return new WebViewClient() {

            /**
             * We want to load all links inside the current webview
             */
            @Override
            public boolean shouldOverrideUrlLoading(WebView webView, String url) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                return true;
            }
        };
    }

}
