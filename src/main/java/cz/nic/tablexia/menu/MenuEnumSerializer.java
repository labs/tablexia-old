/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.menu;

import com.activeandroid.serializer.TypeSerializer;
import com.activeandroid.util.Log;

public class MenuEnumSerializer extends TypeSerializer {

    private static final String SEPARATOR = ":";

    @Override
    public Class<?> getDeserializedType() {
        return MenuEnum.class;
    }

    @Override
    public Class<?> getSerializedType() {
        return String.class;
    }

    /* checking not needed */
    @SuppressWarnings("unchecked")
    @Override
    public Object deserialize(Object data) {
        if (data == null) {
            return null;
        }
        String menuStr = (String) data;
        String[] partsStr = menuStr.split(SEPARATOR);
        int position = Integer.valueOf(partsStr[1]);

        Class<MenuEnum> menu;
        try {
            menu = (Class<MenuEnum>) Class.forName(partsStr[0]);
            MenuEnum[] econst = menu.getEnumConstants();
            return econst[position];
        } catch (ClassNotFoundException e) {
            Log.e("Invalid data type for ActiveMenu deserialization", e);
        }
        return null;
    }

    @Override
    public Object serialize(Object data) {
        if (data == null) {
            return null;
        }
        MenuEnum menu = ((MenuEnum) data);
        return menu.getClass().getName() + SEPARATOR + menu.ordinal();
    }

}
