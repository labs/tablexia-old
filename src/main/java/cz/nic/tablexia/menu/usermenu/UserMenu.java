/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.menu.usermenu;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import cz.nic.tablexia.R;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.audio.SoundControl;
import cz.nic.tablexia.audio.resources.PermanentSounds;
import cz.nic.tablexia.main.MainActivity;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.mainmenu.CommonUserAdapter;
import cz.nic.tablexia.util.DensityUtils;
import cz.nic.tablexia.widget.TablexiaDrawerLayout.DrawerListenerAdapter;
import cz.nic.tablexia.widget.TablexiaQuestionBannerDialog;

/**
 * User menu control class.
 *
 * @author Matyáš Latner
 */
public class UserMenu extends AbstractMenu implements OnItemClickListener, OnItemLongClickListener {

    private CommonUserAdapter userMenuAdapter;
    private SoundControl      soundControl;
    private boolean           menuClickableLock = true;

    public UserMenu(Tablexia tablexiaContext) {
        super(tablexiaContext);
        userMenuAdapter = new CommonUserAdapter(tablexiaContext);
        soundControl = tablexiaContext.getGlobalSoundControl();
    }

    @Override
    public View getMenuView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.usermenu, container, false);
    }

    @Override
    public void initMenu() {

        hideMenuToggelButton();

        ListView userMenuListView = (ListView) tablexiaContext.getCurrentMenuActivity().findViewById(R.id.usermenu_listview);
        userMenuListView.setAdapter(userMenuAdapter.refresh());
        userMenuListView.setOnItemClickListener(this);
        userMenuListView.setOnItemLongClickListener(this);
    }

    private void hideMenuToggelButton() {
        final View menuRightBorder = tablexiaContext.getCurrentMenuActivity().findViewById(R.id.mainmenu_rightborder);
        final ViewPropertyAnimator animator = menuRightBorder.animate();
        animator.setInterpolator(new AccelerateInterpolator()).translationX(-menuRightBorder.getWidth()).setListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(Animator animation) {
                animator.setListener(null);
                menuRightBorder.setVisibility(View.GONE);
            }

        });
    }

    /* //////////////////////////////////////////// CLICK HANDLERS */

    /**
     * On user click handler
     */
    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, final int position, long arg3) {
        if (menuClickableLock) {
            menuClickableLock = false;
            soundControl.playSound(PermanentSounds.BUTTON_SOUND, false);
            tablexiaContext.getCurrentMenuActivity().addMenuDrawerListener(new DrawerListenerAdapter() {

                @Override
                public void onDrawerClosed(View drawerView) {
                    menuClickableLock = true;
                }
            });
            tablexiaContext.getCurrentMenuActivity().lockCloseMenuDrawer();
            arg1.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (position == userMenuAdapter.getUserCount()) {
                        if (DensityUtils.lowMemorySettings((MainActivity) tablexiaContext.getCurrentMenuActivity())) {
                            ((MainActivity) tablexiaContext.getCurrentMenuActivity()).getTablexiaContext().showNewUserScreen();
                        } else {
                            ((MainActivity) tablexiaContext.getCurrentMenuActivity()).showNewPlayerAnimation();
                        }
                    } else {
                        tablexiaContext.doSelectUser(userMenuAdapter.getUserAtPosition(position));
                        tablexiaContext.changeMenu(AbstractMenu.MAIN_MENU);
                    }
                }
            }, 1000);
        }
    }

    /**
     * On user long click handler
     */
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        soundControl.playSound(PermanentSounds.BUTTON_SOUND, false);
        final User user = userMenuAdapter.getUserAtPosition(position);
        new TablexiaQuestionBannerDialog() {

            @Override
            protected int getTextResource() {
                return R.string.mainmenu_delete_user;
            }

            @Override
            protected int getBannerResource() {
                return R.drawable.dialog_banner_pozor;
            }

            @Override
            public void positiveAction() {
                dismiss();
                new TablexiaQuestionBannerDialog() {

                    @Override
                    public void positiveAction() {
                        user.setDeleted(true);
                        user.save();
                        userMenuAdapter.refresh();
                        userMenuAdapter.notifyDataSetChanged();
                        dismiss();
                    };

                    @Override
                    protected int getBannerResource() {
                        return R.drawable.dialog_banner_pozor;
                    }
                }.show(((Activity) tablexiaContext.getCurrentMenuActivity()).getFragmentManager(), null);
            };
        }.show(((Activity) tablexiaContext.getCurrentMenuActivity()).getFragmentManager(), null);

        return true;
    }

}
