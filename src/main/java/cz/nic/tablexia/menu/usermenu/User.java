/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.menu.usermenu;

import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;

import cz.nic.tablexia.game.games.GamesDefinition;

@Table(name = "Users")
public class User extends Model {

    public static class UserTutorial extends Model {

        @Column(name = "User")
        private User user;
        @Column(name = "Game")
        private int game;

        private UserTutorial setUser(User user) {
            this.user = user;
            return this;
        }

        private UserTutorial setGame(GamesDefinition gamesDefinition) {
            game = gamesDefinition.getPosition();
            return this;
        }

        public static void createUserTutorialRecord(User user, GamesDefinition gamesDefinition) {
            (new UserTutorial().setUser(user).setGame(gamesDefinition)).save();
        }

        public static boolean existsUserTutorialRecord(User user, GamesDefinition gamesDefinition) {
            return new Select().from(UserTutorial.class).where("User = ? AND Game = ?", user.getId(), gamesDefinition.getPosition()).executeSingle() != null;
        }
    }

    @Expose
    @Column(name = "NickName")
    private String  nickName;
    @Expose
    @Column(name = "Age")
    private int     age;
    @Expose
    @Column(name = "IsMale")
    private boolean isMale;
    @Expose
    @Column(name = "Avatar")
    private String  avatar;
    @Column(name = "deleted")
    private Boolean deleted;
    @Column(name = "signatureFileName")
    private String  signatureFileName;
    @Column(name = "IsHelp")
    private boolean isHelp;
    @Column(name = "SightseeingShown")
    private boolean sightseeingShown;

    // USER SCOPE TROPHIES
    @Column(name = "Consecutively3Days")
    private boolean consecutively3Days;
    @Column(name = "Consecutively5Days")
    private boolean consecutively5Days;
    @Column(name = "Consecutively8Days")
    private boolean consecutively8Days;
    @Column(name = "ConsecutivelyAllGames0Stars")
    private boolean consecutivelyAllGames0Stars;
    @Column(name = "ConsecutivelyAllGames2Stars")
    private boolean consecutivelyAllGames2Stars;
    @Column(name = "ConsecutivelyAllGames3Stars")
    private boolean consecutivelyAllGames3Stars;

    @Column(name = "PlayCountHallOfFame")
    private Integer playCountHallOfFame   = 0;
    @Column(name = "PlayCountStatistics")
    private Integer playCountStatistics   = 0;
    @Column(name = "PlayCountEncyclopedia")
    private Integer playCountEncyclopedia = 0;

    public User() {
        super();
    }

    public User(String nickName, int age, boolean isMale, String avatar, boolean isHelp, boolean sightseeingShown, String signatureFileName) {
        super();
        this.nickName = nickName;
        this.age = age;
        this.isMale = isMale;
        this.avatar = avatar;
        this.isHelp = isHelp;
        this.sightseeingShown = sightseeingShown;
        this.signatureFileName = signatureFileName;
        deleted = false;

        consecutively3Days = false;
        consecutively5Days = false;
        consecutively8Days = false;
        consecutivelyAllGames0Stars = false;
        consecutivelyAllGames2Stars = false;
        consecutivelyAllGames3Stars = false;
    }

    public String getNickName() {
        return nickName;
    }

    public int getAge() {
        return age;
    }

    public boolean isMale() {
        return isMale;
    }

    public String getAvatar() {
        return avatar;
    }

    public boolean isHelp() {
        return isHelp;
    }

    public String getSignatureBmpPath() {
        return signatureFileName;
    }

    public void setHelpAndSave(boolean isHelp) {
        this.isHelp = isHelp;
        save();
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isConsecutively3Days() {
        return consecutively3Days;
    }

    public void setConsecutively3Days(boolean consecutively3Days) {
        this.consecutively3Days = consecutively3Days;
    }

    public boolean isConsecutively5Days() {
        return consecutively5Days;
    }

    public void setConsecutively5Days(boolean consecutively5Days) {
        this.consecutively5Days = consecutively5Days;
    }

    public boolean isConsecutively8Days() {
        return consecutively8Days;
    }

    public void setConsecutively8Days(boolean consecutively8Days) {
        this.consecutively8Days = consecutively8Days;
    }

    public boolean isConsecutivelyAllGames2Stars() {
        return consecutivelyAllGames2Stars;
    }

    public void setConsecutivelyAllGames2Stars(boolean consecutivelyAllGames2Stars) {
        this.consecutivelyAllGames2Stars = consecutivelyAllGames2Stars;
    }

    public boolean isConsecutivelyAllGames3Stars() {
        return consecutivelyAllGames3Stars;
    }

    public void setConsecutivelyAllGames3Stars(boolean consecutivelyAllGames3Stars) {
        this.consecutivelyAllGames3Stars = consecutivelyAllGames3Stars;
    }

    public boolean isConsecutivelyAllGames0Stars() {
        return consecutivelyAllGames0Stars;
    }

    public void setConsecutivelyAllGames0Stars(boolean consecutivelyAllGames0Stars) {
        this.consecutivelyAllGames0Stars = consecutivelyAllGames0Stars;
    }

    public Integer getPlayCountHallOfFame() {
        return playCountHallOfFame;
    }

    public void addPlayCountHallOfFame() {
        playCountHallOfFame++;
        save();
    }

    public Integer getPlayCountStatistics() {
        return playCountStatistics;
    }

    public void addPlayCountStatistics() {
        playCountStatistics++;
        save();
    }

    public Integer getPlayCountEncyclopedia() {
        return playCountEncyclopedia;
    }

    public void addPlayCountEncyclopedia() {
        playCountEncyclopedia++;
        save();
    }

    public static boolean existsUsersForName(String nickName) {
        return (new Select().from(User.class).where("NickName = ? and deleted = 0", nickName).executeSingle()) != null;
    }

    public static User getUsersForId(long id) {
        return new Select().from(User.class).where("Id = ?", id).executeSingle();
    }

    public static List<User> getAllUsers() {
        return new Select().from(User.class).where("deleted = 0").execute();
    }

    public void markTutorialAsPlayed(GamesDefinition gamesDefinition) {
        UserTutorial.createUserTutorialRecord(this, gamesDefinition);
    }

    public boolean wasGameTutorialPlayed(GamesDefinition gamesDefinition) {
        return UserTutorial.existsUserTutorialRecord(this, gamesDefinition);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null) {
            if (obj instanceof User) {
                return ((User) obj).getId() == getId();
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public String toString() {
        return "[" + getId() + "] " + nickName + " (AGE: " + age + ", MALE: " + isMale + ", AVATAR: " + avatar + ")";
    }

    public boolean isSightseeingShown() {
        return sightseeingShown;
    }

    public void setSightseeingShownAndSave(boolean sightseeingShown) {
        this.sightseeingShown = sightseeingShown;
        save();
    }
}
