/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.trophy;

import cz.nic.tablexia.R;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.GamesDefinition;

/**
 * Definitions of game trophies
 *
 * @author Matyáš Latner
 */
public enum GameTrophyDefinition implements TrophyEnum {

    BANKOVNILOUPEZ_PLAY1(GamesDefinition.BANKOVNILOUPEZ, GameTrophyTypeDefinition.PLAY_LIMIT_1, R.drawable.trophy_bankovniloupez_play1_small, R.id.screen_halloffame_trophy_bankovniloupez_play1, R.drawable.trophy_bankovniloupez_play1_full, R.string.trophy_bankovniloupez_play1_name, R.string.trophy_bankovniloupez_play1_description, 1), //
    BANKOVNILOUPEZ_PLAY2(GamesDefinition.BANKOVNILOUPEZ, GameTrophyTypeDefinition.PLAY_LIMIT_2, R.drawable.trophy_bankovniloupez_play2_small, R.id.screen_halloffame_trophy_bankovniloupez_play2, R.drawable.trophy_bankovniloupez_play2_full, R.string.trophy_bankovniloupez_play2_name, R.string.trophy_bankovniloupez_play2_description, 5), //
    BANKOVNILOUPEZ_PLAY3(GamesDefinition.BANKOVNILOUPEZ, GameTrophyTypeDefinition.PLAY_LIMIT_3, R.drawable.trophy_bankovniloupez_play3_small, R.id.screen_halloffame_trophy_bankovniloupez_play3, R.drawable.trophy_bankovniloupez_play3_full, R.string.trophy_bankovniloupez_play3_name, R.string.trophy_bankovniloupez_play3_description, 10), //
    BANKOVNILOUPEZ_DIFF1(GamesDefinition.BANKOVNILOUPEZ, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_1, R.drawable.trophy_bankovniloupez_diff1_small, R.id.screen_halloffame_trophy_bankovniloupez_diff1, R.drawable.trophy_bankovniloupez_diff1_full, R.string.trophy_bankovniloupez_diff1_name, R.string.trophy_bankovniloupez_diff1_description, GameDifficulty.MEDIUM.ordinal()), //
    BANKOVNILOUPEZ_DIFF2(GamesDefinition.BANKOVNILOUPEZ, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_2, R.drawable.trophy_bankovniloupez_diff2_small, R.id.screen_halloffame_trophy_bankovniloupez_diff2, R.drawable.trophy_bankovniloupez_diff2_full, R.string.trophy_bankovniloupez_diff2_name, R.string.trophy_bankovniloupez_diff2_description, GameDifficulty.HARD.ordinal()),

    PRONASLEDOVANI_PLAY1(GamesDefinition.PRONASLEDOVANI, GameTrophyTypeDefinition.PLAY_LIMIT_1, R.drawable.trophy_pronasledovani_play1_small, R.id.screen_halloffame_trophy_pronasledovani_play1, R.drawable.trophy_pronasledovani_play1_full, R.string.trophy_pronasledovani_play1_name, R.string.trophy_pronasledovani_play1_description, 1), //
    PRONASLEDOVANI_PLAY2(GamesDefinition.PRONASLEDOVANI, GameTrophyTypeDefinition.PLAY_LIMIT_2, R.drawable.trophy_pronasledovani_play2_small, R.id.screen_halloffame_trophy_pronasledovani_play2, R.drawable.trophy_pronasledovani_play2_full, R.string.trophy_pronasledovani_play2_name, R.string.trophy_pronasledovani_play2_description, 5), //
    PRONASLEDOVANI_PLAY3(GamesDefinition.PRONASLEDOVANI, GameTrophyTypeDefinition.PLAY_LIMIT_3, R.drawable.trophy_pronasledovani_play3_small, R.id.screen_halloffame_trophy_pronasledovani_play3, R.drawable.trophy_pronasledovani_play3_full, R.string.trophy_pronasledovani_play3_name, R.string.trophy_pronasledovani_play3_description, 10), //
    PRONASLEDOVANI_DIFF1(GamesDefinition.PRONASLEDOVANI, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_1, R.drawable.trophy_pronasledovani_diff1_small, R.id.screen_halloffame_trophy_pronasledovani_diff1, R.drawable.trophy_pronasledovani_diff1_full, R.string.trophy_pronasledovani_diff1_name, R.string.trophy_pronasledovani_diff1_description, GameDifficulty.MEDIUM.ordinal()), //
    PRONASLEDOVANI_DIFF2(GamesDefinition.PRONASLEDOVANI, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_2, R.drawable.trophy_pronasledovani_diff2_small, R.id.screen_halloffame_trophy_pronasledovani_diff2, R.drawable.trophy_pronasledovani_diff2_full, R.string.trophy_pronasledovani_diff2_name, R.string.trophy_pronasledovani_diff2_description, GameDifficulty.HARD.ordinal()),

    UNOS_PLAY1(GamesDefinition.UNOS, GameTrophyTypeDefinition.PLAY_LIMIT_1, R.drawable.trophy_unos_play1_small, R.id.screen_halloffame_trophy_unos_play1, R.drawable.trophy_unos_play1_full, R.string.trophy_unos_play1_name, R.string.trophy_unos_play1_description, 1), //
    UNOS_PLAY2(GamesDefinition.UNOS, GameTrophyTypeDefinition.PLAY_LIMIT_2, R.drawable.trophy_unos_play2_small, R.id.screen_halloffame_trophy_unos_play2, R.drawable.trophy_unos_play2_full, R.string.trophy_unos_play2_name, R.string.trophy_unos_play2_description, 5), //
    UNOS_PLAY3(GamesDefinition.UNOS, GameTrophyTypeDefinition.PLAY_LIMIT_3, R.drawable.trophy_unos_play3_small, R.id.screen_halloffame_trophy_unos_play3, R.drawable.trophy_unos_play3_full, R.string.trophy_unos_play3_name, R.string.trophy_unos_play3_description, 10), //
    UNOS_DIFF1(GamesDefinition.UNOS, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_1, R.drawable.trophy_unos_diff1_small, R.id.screen_halloffame_trophy_unos_diff1, R.drawable.trophy_unos_diff1_full, R.string.trophy_unos_diff1_name, R.string.trophy_unos_diff1_description, GameDifficulty.MEDIUM.ordinal()), //
    UNOS_DIFF2(GamesDefinition.UNOS, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_2, R.drawable.trophy_unos_diff2_small, R.id.screen_halloffame_trophy_unos_diff2, R.drawable.trophy_unos_diff2_full, R.string.trophy_unos_diff2_name, R.string.trophy_unos_diff2_description, GameDifficulty.HARD.ordinal()),

    NOCNISLEDOVANI_PLAY1(GamesDefinition.NOCNISLEDOVANI, GameTrophyTypeDefinition.PLAY_LIMIT_1, R.drawable.trophy_nocnisledovani_1_dohrane, R.id.screen_halloffame_trophy_nocnisledovani_play1, R.drawable.trophy_nocnisledovani_1_dohrane_plny, R.string.trophy_nocnisledovani_play1_name, R.string.trophy_nocnisledovani_play1_description, 1), //
    NOCNISLEDOVANI_PLAY2(GamesDefinition.NOCNISLEDOVANI, GameTrophyTypeDefinition.PLAY_LIMIT_2, R.drawable.trophy_nocnisledovani_5_dohrane, R.id.screen_halloffame_trophy_nocnisledovani_play2, R.drawable.trophy_nocnisledovani_5_dohrane_plny, R.string.trophy_nocnisledovani_play2_name, R.string.trophy_nocnisledovani_play2_description, 5), //
    NOCNISLEDOVANI_PLAY3(GamesDefinition.NOCNISLEDOVANI, GameTrophyTypeDefinition.PLAY_LIMIT_3, R.drawable.trophy_nocnisledovani_10_dohrane, R.id.screen_halloffame_trophy_nocnisledovani_play3, R.drawable.trophy_nocnisledovani_10_dohrane_plny, R.string.trophy_nocnisledovani_play3_name, R.string.trophy_nocnisledovani_play3_description, 10), //
    NOCNISLEDOVANI_DIFF1(GamesDefinition.NOCNISLEDOVANI, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_1, R.drawable.trophy_nocnisledovani_1_limit, R.id.screen_halloffame_trophy_nocnisledovani_diff1, R.drawable.trophy_nocnisledovani_1_limit_plny, R.string.trophy_nocnisledovani_diff1_name, R.string.trophy_nocnisledovani_diff1_description, GameDifficulty.MEDIUM.ordinal()), //
    NOCNISLEDOVANI_DIFF2(GamesDefinition.NOCNISLEDOVANI, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_2, R.drawable.trophy_nocnisledovani_2_limit, R.id.screen_halloffame_trophy_nocnisledovani_diff2, R.drawable.trophy_nocnisledovani_2_limit_plny, R.string.trophy_nocnisledovani_diff2_name, R.string.trophy_nocnisledovani_diff2_description, GameDifficulty.HARD.ordinal()),

    STRELNICE_PLAY1(GamesDefinition.STRELNICE, GameTrophyTypeDefinition.PLAY_LIMIT_1, R.drawable.trophy_strelnice_1_dohrane, R.id.screen_halloffame_trophy_strelnice_play1, R.drawable.trophy_strelnice_1_dohrane_plny, R.string.trophy_strelnice_play1_name, R.string.trophy_strelnice_play1_description, 1), //
    STRELNICE_PLAY2(GamesDefinition.STRELNICE, GameTrophyTypeDefinition.PLAY_LIMIT_2, R.drawable.trophy_strelnice_5_dohrane, R.id.screen_halloffame_trophy_strelnice_play2, R.drawable.trophy_strelnice_5_dohrane_plny, R.string.trophy_strelnice_play2_name, R.string.trophy_strelnice_play2_description, 5), //
    STRELNICE_PLAY3(GamesDefinition.STRELNICE, GameTrophyTypeDefinition.PLAY_LIMIT_3, R.drawable.trophy_strelnice_10_dohrane, R.id.screen_halloffame_trophy_strelnice_play3, R.drawable.trophy_strelnice_10_dohrane_plny, R.string.trophy_strelnice_play3_name, R.string.trophy_strelnice_play3_description, 10), //
    STRELNICE_DIFF1(GamesDefinition.STRELNICE, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_1, R.drawable.trophy_strelnice_1_limit, R.id.screen_halloffame_trophy_strelnice_diff1, R.drawable.trophy_strelnice_1_limit_plny, R.string.trophy_strelnice_diff1_name, R.string.trophy_strelnice_diff1_description, GameDifficulty.MEDIUM.ordinal()), //
    STRELNICE_DIFF2(GamesDefinition.STRELNICE, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_2, R.drawable.trophy_strelnice_2_limit, R.id.screen_halloffame_trophy_strelnice_diff2, R.drawable.trophy_strelnice_2_limit_plny, R.string.trophy_strelnice_diff2_name, R.string.trophy_strelnice_diff2_description, GameDifficulty.HARD.ordinal()),

    POTME_PLAY1(GamesDefinition.POTME, GameTrophyTypeDefinition.PLAY_LIMIT_1, R.drawable.trophy_potme_1_dohrane, R.id.screen_halloffame_trophy_potme_play1, R.drawable.trophy_potme_1_dohrane_plny, R.string.trophy_potme_play1_name, R.string.trophy_potme_play1_description, 1), //
    POTME_PLAY2(GamesDefinition.POTME, GameTrophyTypeDefinition.PLAY_LIMIT_2, R.drawable.trophy_potme_5_dohrane, R.id.screen_halloffame_trophy_potme_play2, R.drawable.trophy_potme_5_dohrane_plny, R.string.trophy_potme_play2_name, R.string.trophy_potme_play2_description, 5), //
    POTME_PLAY3(GamesDefinition.POTME, GameTrophyTypeDefinition.PLAY_LIMIT_3, R.drawable.trophy_potme_10_dohrane, R.id.screen_halloffame_trophy_potme_play3, R.drawable.trophy_potme_10_dohrane_plny, R.string.trophy_potme_play3_name, R.string.trophy_potme_play3_description, 10), //
    POTME_DIFF1(GamesDefinition.POTME, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_1, R.drawable.trophy_potme_1_limit, R.id.screen_halloffame_trophy_potme_diff1, R.drawable.trophy_potme_1_limit_plny, R.string.trophy_potme_diff1_name, R.string.trophy_potme_diff1_description, GameDifficulty.MEDIUM.ordinal()), //
    POTME_DIFF2(GamesDefinition.POTME, GameTrophyTypeDefinition.DIFFICULTY_LIMIT_2, R.drawable.trophy_potme_2_limit, R.id.screen_halloffame_trophy_potme_diff2, R.drawable.trophy_potme_2_limit_plny, R.string.trophy_potme_diff2_name, R.string.trophy_potme_diff2_description, GameDifficulty.HARD.ordinal());


    private GamesDefinition          gameDefinition;
    private GameTrophyTypeDefinition trophyTypeDefinition;
    private int                      smallImageResourceId;
    private int                      trophyImageId;
    private int                      limit;
    private int                      nameResourceId;
    private int                      descriptionResourceId;
    private int                      fullImageResourceId;

    private GameTrophyDefinition(GamesDefinition gameDefinition, GameTrophyTypeDefinition trophyTypeDefinition, int smallImageResourceId, int trophyImageId, int fullImageResourceId, int nameResourceId, int descriptionResourceId, int limit) {

        this.gameDefinition = gameDefinition;
        this.trophyTypeDefinition = trophyTypeDefinition;
        this.smallImageResourceId = smallImageResourceId;
        this.fullImageResourceId = fullImageResourceId;
        this.nameResourceId = nameResourceId;
        this.descriptionResourceId = descriptionResourceId;
        this.trophyImageId = trophyImageId;
        this.limit = limit;
    }

    public GamesDefinition getGameDefinition() {
        return gameDefinition;
    }

    public GameTrophyTypeDefinition getTrophyTypeDefinition() {
        return trophyTypeDefinition;
    }

    @Override
    public int getSmallImageResourceId() {
        return smallImageResourceId;
    }

    public int getFullImageResourceId() {
        return fullImageResourceId;
    }

    @Override
    public int getNameResourceId() {
        return nameResourceId;
    }

    @Override
    public int getDescriptionResourceId() {
        return descriptionResourceId;
    }

    @Override
    public int getTrophyImageId() {
        return trophyImageId;
    }

    public int getLimit() {
        return limit;
    }

    public static GameTrophyDefinition getTrophyDefinitionByGameAndTrophyType(GamesDefinition gameDefinition, GameTrophyTypeDefinition trophyTypeDefinition) {
        for (GameTrophyDefinition trophyDefinition : GameTrophyDefinition.values()) {
            if (trophyDefinition.getGameDefinition().equals(gameDefinition) && (trophyDefinition.getTrophyTypeDefinition() == trophyTypeDefinition)) {
                return trophyDefinition;
            }
        }
        return null;
    }

    public static GameTrophyDefinition getTrophyDefinitionByGameAndLimit(GamesDefinition gameDefinition, TrophyGlobalTypeDefinition trophyGlobalTypeDefinition, int limit) {
        for (GameTrophyDefinition trophyDefinition : GameTrophyDefinition.values()) {
            if (trophyDefinition.getGameDefinition().equals(gameDefinition) && (trophyDefinition.getTrophyTypeDefinition().getTrophyGlobalTypeDefinition() == trophyGlobalTypeDefinition) && (trophyDefinition.getLimit() == limit)) {
                return trophyDefinition;
            }
        }
        return null;
    }

}
