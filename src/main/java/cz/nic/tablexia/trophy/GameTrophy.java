/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.trophy;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.games.GamesDefinition;
import cz.nic.tablexia.game.manager.GameManager;
import cz.nic.tablexia.util.Utility;

/**
 * Database record for user game trophies and static methods for calculating game trophies.
 * @author Matyáš Latner
 *
 */
@Table(name = "GameTrophies")
public class GameTrophy extends Model {
    
    protected static final String GAMETROPHY_LOG_PREFIX = "[GAMETROPHY] ";

    @Column(name = "GameNumber")
    private int     gameNumber;
    @Column(name = "UserId")
    private long    userId;

    @Column(name = "DifficultyTrophy1")
    private boolean difficultyTrophy1;
    @Column(name = "DifficultyTrophy2")
    private boolean difficultyTrophy2;
    
    @Column(name = "PlayTrophy1")
    private boolean playTrophy1;
    @Column(name = "PlayTrophy2")
    private boolean playTrophy2;
    @Column(name = "PlayTrophy3")
    private boolean playTrophy3;
    
    @Column(name = "NumberOfGamePlay")
    private int numberOfGamePlay;
    
    public GameTrophy() {
        super();
    }

    public GameTrophy(long userId, int gameNumber) {
        super();
        this.userId = userId;
        this.gameNumber = gameNumber;
        difficultyTrophy1 = false;
        difficultyTrophy2 = false;
        playTrophy1 = false;
        playTrophy2 = false;
        playTrophy3 = false;
        numberOfGamePlay = 0;
    }
    
    /**
     * Creates new trophy record for specific game and user
     * 
     * @param userId game user for new record
     * @param gameNumber number of game for new record
     * @return user game trophy record instance
     */
    public static GameTrophy createGameTrophyAndSave(long userId, int gameNumber) {
        GameTrophy gameTrophy = new GameTrophy(userId, gameNumber);
        gameTrophy.save();
        return gameTrophy;
    }
    
    /**
     * Returns game trophy record for specific user and game
     * 
     * @param userId user id for selecting game trophy record
     * @param gameNumber game number for selecting game trophy record
     * @return game trophy record instance for specific user and game
     */
    public static GameTrophy getGameTrophyForUserAndGame(long userId, int gameNumber) {
        return new Select().from(GameTrophy.class).where("UserId = ? AND GameNumber = ?", userId, gameNumber).executeSingle();
    }
    
    /**
     * Returns all game trophy records for specific user
     * 
     * @param userId user id for selecting game trophy records
     * @return game trophy record instances for specific user
     */
    public static List<GameTrophy> getAllGameTrophysForUser(long userId) {
        return new Select().from(GameTrophy.class).where("UserId = ?", userId).execute();
    }
    
    /**
     * Logs all game trophies for specific user
     * 
     * @param userID user for logging all game trophies
     */
    public static void logAllGameTrophysForUser(long userID) {
        List<GameTrophy> allGameTrophysForUser = getAllGameTrophysForUser(userID);
        Log.d(GameManager.class.toString(), GAMETROPHY_LOG_PREFIX + " User Games Trophies: ");
        for (GameTrophy gameTrophy : allGameTrophysForUser) {
            Log.d(GameManager.class.toString(), GAMETROPHY_LOG_PREFIX + "\t - " + String.valueOf(gameTrophy));
        }
    }
    
    /**
     * Sets number of played games and save all record
     * 
     * @param numberOfGamePlay number of payed games
     */
    public void setNumberOfGamePlayAndSave(int numberOfGamePlay) {
        this.numberOfGamePlay = numberOfGamePlay;
        save();
    }
    
    /**
     * Increment number of played games and save all record
     */
    public void incrementNumberOfGamePlay() {
        setNumberOfGamePlayAndSave(numberOfGamePlay + 1);
    }
    
    /**
     * Process calculations for complete game and returns resource IDs for game trophies if there are any.
     * 
     * @param gameDifficulty played game difficulty
     * @param score played game score (number of stars)
     * @return resource IDs for game trophies
     */
    public List<TrophyEnum> nextGameComplete(GameDifficulty gameDifficulty, int score) {
        incrementNumberOfGamePlay();
        
        List<TrophyEnum> trophies = new ArrayList<TrophyEnum>();
        
        if (score > 2) {
            
            processTrophy(GameTrophyDefinition.getTrophyDefinitionByGameAndLimit(GamesDefinition.getGameAtPosition(gameNumber),
                                                                             TrophyGlobalTypeDefinition.DIFFICULTY_LIMIT,
                                                                             gameDifficulty.ordinal()),
                          trophies);
        }
        
        processTrophy(GameTrophyDefinition.getTrophyDefinitionByGameAndLimit(GamesDefinition.getGameAtPosition(gameNumber),
                                                                               TrophyGlobalTypeDefinition.PLAY_LIMIT,
                                                                               numberOfGamePlay),
                      trophies);
        
        save();
        return trophies;
    }
    
    /**
     * Set given trophy and add given trophy to list of new trophies in parameter if given trophy is not set.
     * 
     * @param trophyDefinition trophy
     * @param trophies list of new trophies
     */
    private void processTrophy(GameTrophyDefinition trophyDefinition, List<TrophyEnum> trophies) {
        if (trophyDefinition != null) {
            Boolean isTrophy = Utility.isSetTrophyForName(this, trophyDefinition.getTrophyTypeDefinition().getDbFieldName());
            if (isTrophy == null || isTrophy.booleanValue() == false) {
                Utility.setTrophyForName(this, trophyDefinition.getTrophyTypeDefinition().getDbFieldName());
                trophies.add(trophyDefinition);
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null) {
            if (obj instanceof GameTrophy) {
                return ((GameTrophy) obj).getId() == getId();
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public String toString() {
        return "[" + getId() + "] USER: " + userId + ", GAME: " + GamesDefinition.getGameAtPosition(gameNumber) + ", GAMEPLAYS: " + numberOfGamePlay + " (DifficultyTrophy [" + difficultyTrophy1 + " "+ difficultyTrophy2 + "] PlayTrophy: [" + playTrophy1 + ", " + playTrophy2 + ", " + playTrophy3 + " ])";
    }
}
