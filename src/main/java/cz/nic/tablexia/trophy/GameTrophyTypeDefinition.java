/*******************************************************************************
 *     Tablexia	
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.trophy;

/**
 * Trophies types
 * 
 * @author Matyáš Latner
 *
 */
public enum GameTrophyTypeDefinition {

    PLAY_LIMIT_3(TrophyGlobalTypeDefinition.PLAY_LIMIT, "playTrophy3"),
    PLAY_LIMIT_2(TrophyGlobalTypeDefinition.PLAY_LIMIT, "playTrophy2"),
    PLAY_LIMIT_1(TrophyGlobalTypeDefinition.PLAY_LIMIT, "playTrophy1"),
    DIFFICULTY_LIMIT_2(TrophyGlobalTypeDefinition.DIFFICULTY_LIMIT, "difficultyTrophy2"),
    DIFFICULTY_LIMIT_1(TrophyGlobalTypeDefinition.DIFFICULTY_LIMIT, "difficultyTrophy1");
    
    private TrophyGlobalTypeDefinition trophyGlobalTypeDefinition;
    private String dbFieldName;

    private GameTrophyTypeDefinition(TrophyGlobalTypeDefinition trophyGlobalTypeDefinition, String dbFieldName) {
        this.trophyGlobalTypeDefinition = trophyGlobalTypeDefinition;
        this.dbFieldName = dbFieldName;
    }
    
    public TrophyGlobalTypeDefinition getTrophyGlobalTypeDefinition() {
        return trophyGlobalTypeDefinition;
    }
    
    public String getDbFieldName() {
        return dbFieldName;
    }

}
