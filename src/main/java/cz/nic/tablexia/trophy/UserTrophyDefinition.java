/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.trophy;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.R;
import cz.nic.tablexia.game.difficulty.GameDifficulty;
import cz.nic.tablexia.game.manager.GameManager;
import cz.nic.tablexia.menu.usermenu.User;
import cz.nic.tablexia.util.Utility;

/**
 * Definitions of user trophies
 *
 * @author Matyáš Latner
 */
public enum UserTrophyDefinition implements TrophyEnum {

    CONSECUTIVELY_DAYSBACK_3(UserTrophyTypeDefinition.CONSECUTIVELY_DAYSBACK, "consecutively3Days", R.drawable.trophy_user_consecutively3days_small, R.id.screen_halloffame_trophy_user_consecutively3days, R.drawable.trophy_user_consecutively3days, R.string.trophy_user_consecutively3Days_name, R.string.trophy_user_consecutively3Days_description, 3),
    CONSECUTIVELY_DAYSBACK_5(UserTrophyTypeDefinition.CONSECUTIVELY_DAYSBACK, "consecutively5Days", R.drawable.trophy_user_consecutively5days_small, R.id.screen_halloffame_trophy_user_consecutively5days, R.drawable.trophy_user_consecutively5days, R.string.trophy_user_consecutively5Days_name, R.string.trophy_user_consecutively5Days_description, 5),
    CONSECUTIVELY_DAYSBACK_8(UserTrophyTypeDefinition.CONSECUTIVELY_DAYSBACK, "consecutively8Days", R.drawable.trophy_user_consecutively8days_small, R.id.screen_halloffame_trophy_user_consecutively8days, R.drawable.trophy_user_consecutively8days, R.string.trophy_user_consecutively8Days_name, R.string.trophy_user_consecutively8Days_description, 8),
    ONEDAYALLGAMES_0STARS(UserTrophyTypeDefinition.ONEDAYALLGAMES, "consecutivelyAllGames0Stars", R.drawable.trophy_user_consecutivelyallgames0stars_small, R.id.screen_halloffame_trophy_user_consecutivelyallgames0stars, R.drawable.trophy_user_consecutivelyallgames0stars, R.string.trophy_user_consecutivelyAllGames0Stars_name, R.string.trophy_user_consecutivelyAllGames0Stars_description, 0),
    CONSECUTIVELYALLGAMES_MEDIUMDIFFICULTY(UserTrophyTypeDefinition.CONSECUTIVELY_ALLGAMES, "consecutivelyAllGames2Stars", R.drawable.trophy_user_consecutivelyallgames2stars_small, R.id.screen_halloffame_trophy_user_consecutivelyallgames2stars, R.drawable.trophy_user_consecutivelyallgames2stars, R.string.trophy_user_consecutivelyAllGames2Stars_name, R.string.trophy_user_consecutivelyAllGames2Stars_description, GameDifficulty.MEDIUM.ordinal()),
    CONSECUTIVELYALLGAMES_HARDDIFFICULTY(UserTrophyTypeDefinition.CONSECUTIVELY_ALLGAMES, "consecutivelyAllGames3Stars", R.drawable.trophy_user_consecutivelyallgames3stars_small, R.id.screen_halloffame_trophy_user_consecutivelyallgames3stars, R.drawable.trophy_user_consecutivelyallgames3stars, R.string.trophy_user_consecutivelyAllGames3Stars_name, R.string.trophy_user_consecutivelyAllGames3Stars_description, GameDifficulty.HARD.ordinal());

    private UserTrophyTypeDefinition trophyTypeDefinition;
    private String                   dbFieldName;
    private int                      smallImageResourceId;
    private int                      trophyImageId;
    private int                      limit;
    private int                      nameResourceId;
    private int                      descriptionResourceId;
    private int                      fullImageResourceId;

    private UserTrophyDefinition(UserTrophyTypeDefinition trophyTypeDefinition, String dbFieldName, int smallImageResourceId, int trophyImageId, int fullImageResourceId, int nameResourceId, int descriptionResourceId, int limit) {

        this.trophyTypeDefinition = trophyTypeDefinition;
        this.dbFieldName = dbFieldName;
        this.smallImageResourceId = smallImageResourceId;
        this.fullImageResourceId = fullImageResourceId;
        this.nameResourceId = nameResourceId;
        this.descriptionResourceId = descriptionResourceId;
        this.trophyImageId = trophyImageId;
        this.limit = limit;
    }

    public UserTrophyTypeDefinition getTrophyTypeDefinition() {
        return trophyTypeDefinition;
    }

    public String getDbFieldName() {
        return dbFieldName;
    }

    @Override
    public int getSmallImageResourceId() {
        return smallImageResourceId;
    }

    public int getFullImageResourceId() {
        return fullImageResourceId;
    }

    @Override
    public int getNameResourceId() {
        return nameResourceId;
    }

    @Override
    public int getDescriptionResourceId() {
        return descriptionResourceId;
    }

    @Override
    public int getTrophyImageId() {
        return trophyImageId;
    }

    public int getLimit() {
        return limit;
    }

    public static List<TrophyEnum> processUserTrophies(User user) {
        List<TrophyEnum> resultList = new ArrayList<TrophyEnum>();
        for (UserTrophyDefinition userTrophyDefinition : UserTrophyDefinition.getTrophyDefinitionByUserTrophyDefinition(UserTrophyTypeDefinition.CONSECUTIVELY_DAYSBACK)) {
            if (!Utility.isSetTrophyForName(user, userTrophyDefinition.getDbFieldName())) {
                boolean result = true;
                for (int i = 0; i < userTrophyDefinition.getLimit(); i++) {
                    boolean existGame = GameManager.isGameManagerForDayBack(user.getId(), i);
                    result = result && existGame;
                    if (!existGame) {
                        break;
                    }
                }
                if (result) {
                    Utility.setTrophyForName(user, userTrophyDefinition.getDbFieldName());
                    resultList.add(userTrophyDefinition);
                }
            }
        }

        for (UserTrophyDefinition userTrophyDefinition : UserTrophyDefinition.getTrophyDefinitionByUserTrophyDefinition(UserTrophyTypeDefinition.ONEDAYALLGAMES)) {
            if (!Utility.isSetTrophyForName(user, userTrophyDefinition.getDbFieldName())) {
                if (GameManager.existsGameTypeManagersInOneDayWithStars(user.getId(), userTrophyDefinition.getLimit())) {
                    Utility.setTrophyForName(user, userTrophyDefinition.getDbFieldName());
                    resultList.add(userTrophyDefinition);
                }
            }
        }

        for (UserTrophyDefinition userTrophyDefinition : UserTrophyDefinition.getTrophyDefinitionByUserTrophyDefinition(UserTrophyTypeDefinition.CONSECUTIVELY_ALLGAMES)) {
            if (!Utility.isSetTrophyForName(user, userTrophyDefinition.getDbFieldName())) {
                if (GameManager.existsGameTypeManagersConsecutivelyWithStarsNumberAndDifficulty(user.getId(), userTrophyDefinition.getLimit(), 3)) {
                    Utility.setTrophyForName(user, userTrophyDefinition.getDbFieldName());
                    resultList.add(userTrophyDefinition);
                }
            }
        }

        user.save();
        return resultList;
    }

    public static List<UserTrophyDefinition> getTrophyDefinitionByUserTrophyDefinition(UserTrophyTypeDefinition trophyTypeDefinition) {
        List<UserTrophyDefinition> result = new ArrayList<UserTrophyDefinition>();
        for (UserTrophyDefinition trophyDefinition : UserTrophyDefinition.values()) {
            if (trophyDefinition.getTrophyTypeDefinition() == trophyTypeDefinition) {
                result.add(trophyDefinition);
            }
        }
        return result;
    }

}
