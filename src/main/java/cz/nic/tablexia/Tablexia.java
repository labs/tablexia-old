/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.util.Log;
import com.android.vending.expansion.zipfile.ZipResourceFile;
import com.splunk.mint.Mint;

import cz.nic.tablexia.audio.SoundControl;
import cz.nic.tablexia.export.ExportControl;
import cz.nic.tablexia.export.ExportControl.GameExportType;
import cz.nic.tablexia.main.MainActivity;
import cz.nic.tablexia.menu.AbstractMenu;
import cz.nic.tablexia.menu.AbstractMenu.MenuChangeListener;
import cz.nic.tablexia.menu.MenuActivity;
import cz.nic.tablexia.menu.mainmenu.MainMenu;
import cz.nic.tablexia.menu.mainmenu.MainMenuDefinition;
import cz.nic.tablexia.menu.mainmenu.screen.AbstractMenuFragment;
import cz.nic.tablexia.menu.usermenu.User;
import cz.nic.tablexia.menu.usermenu.UserMenu;
import cz.nic.tablexia.newusers.NewUserActivity;
import cz.nic.tablexia.util.ObbHelper;
import cz.nic.tablexia.util.Utility;
import cz.nic.tablexia.widget.TablexiaDrawerLayout;
import cz.nic.tablexia.widget.TablexiaQuestionBannerDialog;

/**
 * Tablexia context class
 *
 * @author Matyáš Latner
 */
public class Tablexia extends com.activeandroid.app.Application {
    private static final String TAG = Tablexia.class.getSimpleName();

    public interface TablexiaGlobalEventListener {

        void onUserSelected(User user);

    }

    private static final int                  USERMENU_HELP_HIDE_DURATION  = 500;
    private static final int                  USERMENU_HELP_SHOW_DURATION  = 800;
    public static final boolean               IS_LOGGING_ENABLED           = BuildConfig.DEBUG;
    public static final String                TESTFLIGHT_APP_TOKEN_ALPHA   = "a0e61111-fdb6-439e-91bc-00b790e284e8";
    public static final String                TESTFLIGHT_APP_TOKEN_BETA    = "19dc6b8e-b2d8-448f-aec1-ef2648aec4f8";
    public static final String                BUGSENSE_KEY                 = "0bcf80f1";
    public static final Boolean               BUGSENSE_ENABLED             = !BuildConfig.DEBUG;
    public static final Boolean               ANALYTICS_ENABLED            = !BuildConfig.DEBUG;

    public static final GameExportType        GAME_EXPORT_TYPE             = GameExportType.TEST_GAME;

    public static final long                  SCREEN_FADE_ANIM_DURATION    = 500;
    public static final long                  PRELOADER_FADE_ANIM_DURATION = 250;

    private TablexiaState                     tablexiaState;
    private List<TablexiaGlobalEventListener> tablexiaGlobalEventListeners;
    private TablexiaGlobalEventListener       userSelectListener;
    private SoundControl                      globalSoundControl;

    private MainMenu                          mainMenu;
    private UserMenu                          userMenu;
    private MenuActivity                      curreActivity;

    public Tablexia() {
        initGlobalEventListener();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.setEnabled(IS_LOGGING_ENABLED);
        ActiveAndroid.setLoggingEnabled(IS_LOGGING_ENABLED);

        tablexiaState = TablexiaState.getTablexiaState(false);
        globalSoundControl = new SoundControl(getApplicationContext(), 10);
        ExportControl.tryToSendData(this);
    }

    public void startMount(final OnDataMountedListener onDataMountedListener) {
        attachObbFile(onDataMountedListener);
    }

    public static interface OnDataMountedListener {
        public void onDataMounted();

        public void onMissingFile();
    }

    private ZipResourceFile zipResourceFile;

    public ZipResourceFile getZipResourceFile() {
        return zipResourceFile;
    }

    public void attachObbFile(final OnDataMountedListener onDataMountedListener) {
        // Get a ZipResourceFile representing a merger of both the main and patch files
        if (zipResourceFile == null) {
            try {
                String filename = ObbHelper.getExpansionFileName(this, true);
                if (filename == null) {
                    onDataMountedListener.onMissingFile();
                }
                zipResourceFile = new ZipResourceFile(filename);
            } catch (IOException e) {
                Mint.logException(e);
            }
            globalSoundControl.loadPermanentSounds();
        }
        if (onDataMountedListener != null) {
            onDataMountedListener.onDataMounted();
        }
    }

    public void restore() {
        tablexiaState = TablexiaState.getTablexiaState(true);
    }

    public TablexiaState getTablexiaState() {
        return tablexiaState;
    }

    public SoundControl getGlobalSoundControl() {
        return globalSoundControl;
    }

    /* //////////////////////////////////////////// USER CONTROL */

    /**
     * Select user without confirmation dialog
     *
     * @param user
     * @param menuActivity
     */
    public void forceSelectUser(final User user) {
        tablexiaState.setActiveUser(null);
        userSelectListener = new TablexiaGlobalEventListener() {

            @Override
            public void onUserSelected(User user) {
                removeTablexiaGlobalEventListener(this);
                userSelectListener = null;
                ((MainActivity) getCurrentMenuActivity()).hideLoadingScreen(false);
                userSelectListener = null;
            }
        };
        addTablexiaGlobalEventListener(userSelectListener);
        doSelectUser(user);
    }

    /**
     * Show switch user dialog. If dialog is confirmed
     * selects user, unlock menu drawer and open start page.
     * If user is already selected then only unlock menu.
     *
     * @param user
     * @param menuActivity
     */
    public void doSelectUser(final User user) {
        if (user != null) {
            if (!user.equals(tablexiaState.getActiveUser())) {
                if (tablexiaState.getActiveUser() == null) {

                    selectUser(user);

                } else {

                    new LogoutDialog() {

                        @Override
                        public void positiveAction() {
                            selectUser(user);
                            super.positiveAction();
                        }

                        @Override
                        public void negativeAction() {
                            prepareMainMenu().refreshMainMenu();
                            super.negativeAction();
                        };

                    }.show(((Activity) getCurrentMenuActivity()).getFragmentManager(), null);

                }
            }
            getCurrentMenuActivity().unlockMenuDrawer();
        }
    }

    /**
     * Selects active user and reset main menu
     */
    private void selectUser(User user) {
        if (tablexiaState.getActiveUser() == null) {
            ((MainActivity) getCurrentMenuActivity()).showLoadingScreen(false, null);
        }
        tablexiaState.setAndSaveActiveUser(User.getUsersForId(user.getId()));
        prepareMainMenu().resetMenu();
        hideUserMenuHelp();
        // TODO remove TestFlight.log("Select user: " + user.getNickName());
        for (TablexiaGlobalEventListener tablexiaGlobalEventListener : tablexiaGlobalEventListeners) {
            tablexiaGlobalEventListener.onUserSelected(user);
        }
    }

    /**
     * Shows switch user dialog. If dialog is confirmed
     * resets current user. Shows user menu and lock menu drawer.
     * If logout menu is already selected force user reset.
     *
     * @param menuActivity
     */
    public void softResetUser() {

        if (prepareMainMenu().isActiveMenuItem(MainMenuDefinition.LOGOUT)) {

            hardResetUser();

        } else {

            new LogoutDialog() {

                @Override
                public void positiveAction() {
                    if (userSelectListener != null) {
                        removeTablexiaGlobalEventListener(userSelectListener);
                        userSelectListener = null;
                    }
                    hardResetUser();
                    super.positiveAction();
                }

            }.show(((Activity) getCurrentMenuActivity()).getFragmentManager(), null);

        }

    }

    @SuppressLint("ValidFragment")
    public class LogoutDialog extends TablexiaQuestionBannerDialog {

        @Override
        protected int getTextResource() {
            return R.string.mainmenu_switchuser_question;
        }

        @Override
        protected int getPositiveButtonText() {
            return R.string.logoutdialog_logout;
        }

        @Override
        protected int getNegativeButtonText() {
            return R.string.logoutdialog_cancel;
        }

        @Override
        protected int getBannerResource() {
            return R.drawable.dialog_banner_pozor;
        }

    }

    /**
     * Resets current user. Shows user menu and lock menu drawer
     *
     * @param menuActivity
     */
    public void hardResetUser() {
        hardResetUser(false);
    }

    /**
     * Resets current user. Shows user menu and lock menu drawer
     *
     * @param menuActivity
     * @param forceQuickTransition force quick home screen transaction
     */
    public void hardResetUser(boolean forceQuickTransition) {

        final MenuActivity menuActivity = getCurrentMenuActivity();
        if (!(menuActivity instanceof MainActivity)) {
            // if reset user is performed from other activity -> set logout menu and start intent
            prepareMainMenu().changeActiveMenuItem(MainMenuDefinition.LOGOUT);
            Intent intent = new Intent(menuActivity.getContext(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
            menuActivity.getContext().startActivity(intent);
        } else {
            // perform reset user
            menuActivity.lockCloseMenuDrawer();
            final MainActivity mainActivity = (MainActivity) menuActivity;
            mainActivity.showLoadingScreen(false, null);
            menuActivity.getTablexiaContext().prepareMainMenu().changeMenuItem(MainMenuDefinition.START, true, true, true, null, new MenuChangeListener() {

                @Override
                public void onMenuChanged(final AbstractMenuFragment menuFragment) {
                    final View gameMenuCover = menuActivity.findViewById(R.id.screen_games_cover);
                    gameMenuCover.setVisibility(View.VISIBLE);
                    showUserMenuHelp();
                    tablexiaState.setAndSaveActiveUser(null);
                    changeMenu(AbstractMenu.USER_MENU);

                    // when user is selected refresh GameMenu

                    if (userSelectListener == null) {
                        userSelectListener = new TablexiaGlobalEventListener() {

                            @Override
                            public void onUserSelected(User user) {
                                menuActivity.findViewById(R.id.screen_games_cover).setVisibility(View.GONE);
                                removeTablexiaGlobalEventListener(this);
                                userSelectListener = null;
                                mainActivity.hideLoadingScreen(user.isSightseeingShown());
                                userSelectListener = null;
                            }
                        };
                        addTablexiaGlobalEventListener(userSelectListener);
                    }

                    // menu delay for smooth animation
                    (new Handler()).postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            menuActivity.lockOpenMenuDrawer();
                        }
                    }, 1000);
                }
            });
        }

    }

    private void showUserMenuHelp() {
        final View userMenuHelp = getCurrentMenuActivity().findViewById(R.id.usermenu_help);
        userMenuHelp.post(new Runnable() {

            @Override
            public void run() {
                userMenuHelp.setAlpha(0);
                userMenuHelp.setVisibility(View.VISIBLE);
                userMenuHelp.animate().setDuration(USERMENU_HELP_SHOW_DURATION).alpha(1).setListener(null);
            }
        });
    }

    public void hideUserMenuHelp() {
        View userMenuHelpView = getCurrentMenuActivity().findViewById(R.id.usermenu_help);
        if ((userMenuHelpView != null) && (userMenuHelpView.getVisibility() == View.VISIBLE)) {
            final FrameLayout userMenuHelpLayout = (FrameLayout) userMenuHelpView;
            final ViewPropertyAnimator animator = userMenuHelpLayout.animate().setDuration(USERMENU_HELP_HIDE_DURATION).alpha(0);
            animator.setListener(new AnimatorListenerAdapter() {

                @Override
                public void onAnimationEnd(Animator animation) {
                    animator.setListener(null);
                    userMenuHelpLayout.setVisibility(View.GONE);
                }

            });
        }
    }

    /**
     * Returns actually selected user
     *
     * @return actually selected user
     */
    public User getSelectedUser() {
        return tablexiaState.getActiveUser();
    }

    /* //////////////////////////////////////////// GLOBAL EVENT LISTENER */

    public void addTablexiaGlobalEventListener(TablexiaGlobalEventListener tablexiaGlobalEventListener) {
        tablexiaGlobalEventListeners.add(tablexiaGlobalEventListener);
    }

    public void removeTablexiaGlobalEventListener(TablexiaGlobalEventListener tablexiaGlobalEventListener) {
        tablexiaGlobalEventListeners.remove(tablexiaGlobalEventListener);
    }

    public boolean containsTablexiaGlobalEventListener(TablexiaGlobalEventListener tablexiaGlobalEventListener) {
        return tablexiaGlobalEventListeners.contains(tablexiaGlobalEventListener);
    }

    private void initGlobalEventListener() {
        tablexiaGlobalEventListeners = new CopyOnWriteArrayList<Tablexia.TablexiaGlobalEventListener>();
    }

    /* //////////////////////////////////////////// CURRENT ACTIVITY */

    public void setCurreActivity(MenuActivity curreActivity) {
        if (this.curreActivity != curreActivity) {
            mainMenu = null;
            userMenu = null;

            initGlobalEventListener();
            this.curreActivity = curreActivity;
            prepareMainMenu();
            prepareUserMenu();

        }
    }

    public void resetCurrentActivity(MenuActivity curreActivity) {
        if ((this.curreActivity != null) && (this.curreActivity == curreActivity)) {
            initGlobalEventListener();
            userSelectListener = null;
            this.curreActivity = null;
        }
    }

    public MenuActivity getCurrentMenuActivity() {
        return curreActivity;
    }

    /* //////////////////////////////////////////// MENU CONTROL */

    /**
     * Initialize main menu
     *
     * @param savedInstanceState
     */
    public void initMenu() {
        if (getSelectedUser() != null) {
            changeMenu(AbstractMenu.MAIN_MENU);
        } else {
            hardResetUser();
        }
    }

    /**
     * Select and change menu in left panel
     *
     * @param menu menu fragment type
     * @return <code>true</code> if menu is changed else <code>false</code>
     */
    public boolean changeMenu(int menu) {
        AbstractMenu abstractMenu;
        switch (menu) {
            case AbstractMenu.MAIN_MENU:
                abstractMenu = prepareMainMenu();
                break;
            case AbstractMenu.USER_MENU:
                abstractMenu = prepareUserMenu();
                break;
            default:
                return false;
        }
        abstractMenu.showMenu(getCurrentMenuActivity());
        return true;
    }

    /**
     * Reset main menu selection
     *
     * @param menuItem
     * @param menuActivity
     */
    public void resetMenu() {
        prepareMainMenu().resetMenu();
    }

    /**
     * Prepares new main menu if its needed
     *
     * @return
     */
    public MainMenu prepareMainMenu() {
        if (mainMenu == null) {
            mainMenu = new MainMenu(this);
        }
        return mainMenu;
    }

    /**
     * Prepares new user menu if its needed
     *
     * @param menuActivity
     * @return
     */
    public UserMenu prepareUserMenu() {
        if (userMenu == null) {
            userMenu = new UserMenu(this);
        }
        return userMenu;
    }

    /* //////////////////////////////////////////// SCREEN CONTENT HIDER */

    public void hideAllScreen(AnimatorListener animatorListener) {
        Utility.showLayer((Activity) getCurrentMenuActivity(), R.id.allscreen_hiderLayer, SCREEN_FADE_ANIM_DURATION, animatorListener);
    }

    public void showAllScreen(AnimatorListener animatorListener) {
        Utility.hideLayer((Activity) getCurrentMenuActivity(), R.id.allscreen_hiderLayer, SCREEN_FADE_ANIM_DURATION, animatorListener);
    }

    public void hideScreenContentFast(boolean useProgressbar) {
        final Activity activity = (Activity) getCurrentMenuActivity();

        View layer = activity.findViewById(R.id.mainscreen_hiderLayer);
        layer.setClickable(true);
        layer.setAlpha(1f);
        layer.setVisibility(View.VISIBLE);

        if (useProgressbar) {
            View view = activity.findViewById(R.id.mainscreen_hiderProgressbar);
            view.setAlpha(1f);
            view.setVisibility(View.VISIBLE);
        }
    }

    public void hideScreenContent(final boolean useProgressbar, final AnimatorListener animatorListener) {
        final Activity activity = (Activity) getCurrentMenuActivity();
        Utility.showLayer(activity, R.id.mainscreen_hiderLayer, SCREEN_FADE_ANIM_DURATION, new AnimatorListenerAdapter() {

            @Override
            public void onAnimationCancel(Animator animation) {
                if (animatorListener != null) {
                    animatorListener.onAnimationCancel(animation);
                }
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                if (animatorListener != null) {
                    animatorListener.onAnimationRepeat(animation);
                }
            }

            @Override
            public void onAnimationStart(Animator animation) {
                if (animatorListener != null) {
                    animatorListener.onAnimationStart(animation);
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (useProgressbar) {
                    Utility.showView(activity, R.id.mainscreen_hiderProgressbar, PRELOADER_FADE_ANIM_DURATION, null);
                }
                if (animatorListener != null) {
                    animatorListener.onAnimationEnd(animation);
                }
            }

        });
    }

    public void showScreenContent(AnimatorListener animatorListener) {
        Activity activity = (Activity) getCurrentMenuActivity();
        Utility.hideView(activity, R.id.mainscreen_hiderProgressbar, PRELOADER_FADE_ANIM_DURATION, null);
        Utility.hideLayer(activity, R.id.mainscreen_hiderLayer, SCREEN_FADE_ANIM_DURATION, animatorListener);
    }

    public boolean isScreenContentHidden() {
        return Utility.isLayerHidden((Activity) getCurrentMenuActivity(), R.id.mainscreen_hiderLayer);
    }

    /* //////////////////////////////////////////// MENU DRAWER COMMON CONTROL */

    public void lockOpenMenuDrawer(final DrawerLayout menuDrawer) {
        menuDrawer.post(new Runnable() {

            @Override
            public void run() {
                menuDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
                ((RelativeLayout) menuDrawer.getParent()).findViewById(R.id.menu_togglebutton).setVisibility(View.GONE);
            }
        });
    }

    public void unlockOpenMenuDrawer(final DrawerLayout menuDrawer) {
        if (menuDrawer != null) {
            menuDrawer.post(new Runnable() {

                @Override
                public void run() {
                    menuDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    ((RelativeLayout) menuDrawer.getParent()).findViewById(R.id.menu_togglebutton).setVisibility(View.VISIBLE);
                }
            });
        }
    }

    public void lockCloseMenuDrawer(final DrawerLayout menuDrawer) {
        menuDrawer.post(new Runnable() {

            @Override
            public void run() {
                menuDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                ((RelativeLayout) menuDrawer.getParent()).findViewById(R.id.menu_togglebutton).setVisibility(View.GONE);
            }
        });
    }

    public void closeMenuDrawer(final DrawerLayout menuDrawer) {
        menuDrawer.post(new Runnable() {

            @Override
            public void run() {
                menuDrawer.closeDrawers();
            }
        });
    }

    public void addMenuDrawerListener(TablexiaDrawerLayout menuDrawer, DrawerListener drawerListener) {
        menuDrawer.addDrawerListener(drawerListener);
    }

    public void removeMenuDrawerListener(TablexiaDrawerLayout menuDrawer, DrawerListener drawerListener) {
        menuDrawer.removeDrawerListener(drawerListener);
    }

    /* //////////////////////////////////////////// NEW USER SCREEN CONTROL */

    public void showNewUserScreen() {
        // TODO tohle tu nemá co dělat - do aktivity
        final Intent intent = new Intent((Activity) getCurrentMenuActivity(), NewUserActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        hideAllScreen(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(Animator animation) {
                ((Activity) getCurrentMenuActivity()).startActivityForResult(intent, MainActivity.REQUEST_CODE_NEW_USER);
                // TODO remove TestFlight.log("Create user button");
            }
        });
    }
}
