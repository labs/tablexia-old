/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.main;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Messenger;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.text.Html;
import android.text.util.Linkify;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import com.activeandroid.util.Log;
import com.google.android.vending.expansion.downloader.DownloadProgressInfo;
import com.google.android.vending.expansion.downloader.DownloaderClientMarshaller;
import com.google.android.vending.expansion.downloader.IDownloaderClient;
import com.google.android.vending.expansion.downloader.IStub;

import cz.nic.tablexia.R;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.Tablexia.OnDataMountedListener;
import cz.nic.tablexia.expansion.TablexiaDownloaderService;
import cz.nic.tablexia.game.games.GamesDefinition;
import cz.nic.tablexia.menu.AbstractMenu.MenuChangeListener;
import cz.nic.tablexia.menu.MenuActivity;
import cz.nic.tablexia.menu.MenuEnum;
import cz.nic.tablexia.menu.mainmenu.screen.AbstractMenuFragment;
import cz.nic.tablexia.menu.mainmenu.screen.AbstractMenuFragment.MenuFragmentVisibilityListener;
import cz.nic.tablexia.menu.mainmenu.screen.LoaderFragment;
import cz.nic.tablexia.menu.usermenu.User;
import cz.nic.tablexia.newusers.fragment.AnimationContainerFragment;
import cz.nic.tablexia.util.MediaPlayerHelper;
import cz.nic.tablexia.util.MintHelper;
import cz.nic.tablexia.widget.TablexiaButtonViewDialog;
import cz.nic.tablexia.widget.TablexiaDrawerLayout;

/**
 * Tablexia main activity. Provides menu accessible from all screens and
 * controls dispatching game fragments.
 *
 * @author Matyáš Latner
 */
public class MainActivity extends FragmentActivity implements MenuActivity, IDownloaderClient {

    private static final String                    TAG                           = MainActivity.class.getSimpleName();

    public static final int                        REQUEST_CODE_NEW_USER         = 1000;
    public static final int                        REQUEST_CODE_GAME             = 2000;
    public static final int                        RESULT_CODE_NEW_USER          = 1000;

    private static final int                       START_PRELOADER_ANIM_DURATION = 2000;
    private static final int                       MAIN_SCREEN_CONTAINER_ID      = R.id.main_screen_container;
    private static final int                       MAIN_SCREEN_PRELOADER_ID      = R.id.main_screen_preloader;
    private static final int                       MAIN_SCREEN_DIVIDER_ID        = R.id.main_screen_divider;
    private static final Class<? extends Fragment> LOADER_FRAGMENR_CLASS         = LoaderFragment.class;

    private TablexiaDrawerLayout                   menuDrawer;

    private Fragment                               loaderFragment;
    private View                                   screenContainerView;
    private View                                   screenDividerView;
    private View                                   startPreloaderView;
    private int                                    displayWidth;
    private AbstractMenuFragment                   actualMenuFragment;
    private IStub                                  downloaderClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new TablexiaButtonViewDialog() {
            @Override
            protected int getTextResource() {
                return R.string.dialog_sk_disabled;
            }
            
            @Override
            protected int getPositiveButtonText() {
                return R.string.dialog_understand;
            }
            
            @Override
            protected void addTextView(LayoutInflater inflater, ViewGroup contentView) {
                TextView textView = (TextView) inflater.inflate(R.layout.tablexiadialog_text, contentView, false);
                if (textView != null) {

                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, getTextSize());
                    textView.setLinksClickable(true);
                    textView.setAutoLinkMask(Linkify.ALL);
                    textView.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=cz.nic.tablexia")));
                        }
                    });
                    int textResource = getTextResource();
                    String textString = getTextString();
                    CharSequence textCharSequence = getTextCharSequence();

                    if (textResource > 0) {
                        textView.setText(Html.fromHtml(getString(textResource)));
                    } else if (textString != null) {
                        textView.setText(Html.fromHtml(textString));
                    } else if (textCharSequence != null) {
                        textView.setText(textCharSequence);
                    }
                }
                contentView.addView(textView);
            }

            @Override
            protected int getQuestionButtonLayoutId() {
                return R.layout.tablexiadialog_questionbutton_single;
            };

        }.show(getFragmentManager(), null);
        
        MintHelper.startBugSense(this);
        getTablexiaContext().setCurreActivity(this);
        if (savedInstanceState != null) {
            getTablexiaContext().restore();
        }
        setContentView(R.layout.main);
        getTablexiaContext().startMount(new OnDataMountedListener() {
            @Override
            public void onDataMounted() {
                onObbDataMounted();
            }

            @Override
            public void onMissingFile() {
                // TODO start expansion file download
                try {
                    Intent notifierIntent = new Intent(MainActivity.this, MainActivity.class);
                    notifierIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 0, notifierIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    int outcome = DownloaderClientMarshaller.startDownloadServiceIfRequired(MainActivity.this, pendingIntent, TablexiaDownloaderService.class);
                    if (outcome != DownloaderClientMarshaller.NO_DOWNLOAD_REQUIRED) {
                        // Instantiate a member instance of IStub
                        downloaderClient = DownloaderClientMarshaller.CreateStub(MainActivity.this, TablexiaDownloaderService.class);
                        // Inflate layout that shows download progress
                        // TODO show download progress

                        lockCloseMenuDrawer();
                        showLoadingScreen(false, null);
                        return;
                    }
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    public void onServiceConnected(Messenger m) {
        // TODO Auto-generated method stub
        lockCloseMenuDrawer();
        showLoadingScreen(false, null);
    }

    @Override
    public void onDownloadStateChanged(int newState) {
        // TODO Auto-generated method stub
        lockCloseMenuDrawer();
        showLoadingScreen(false, null);
    }

    @Override
    public void onDownloadProgress(DownloadProgressInfo progress) {
        // TODO Auto-generated method stub
        Log.d(TAG, "Progress " + progress.mOverallProgress);
        lockCloseMenuDrawer();
        showLoadingScreen(false, null);
    }

    public void onObbDataMounted() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                android.util.Log.i(TAG, "Data mounted, changing content view");
                // init bugsense
                setContentView(R.layout.main);
                menuDrawer = (TablexiaDrawerLayout) findViewById(R.id.drawer_layout);

                screenContainerView = findViewById(MAIN_SCREEN_CONTAINER_ID);
                screenDividerView = findViewById(MAIN_SCREEN_DIVIDER_ID);
                startPreloaderView = findViewById(MAIN_SCREEN_PRELOADER_ID);

                Point displaySize = new Point();
                getWindowManager().getDefaultDisplay().getSize(displaySize);
                displayWidth = displaySize.x;
                getTablexiaContext().hideScreenContentFast(false);
                getTablexiaContext().initMenu();

            }
        });

    }

    @Override
    protected void onPause() {
        MediaPlayerHelper.muteAll();
        if (downloaderClient != null) {
            downloaderClient.disconnect(this);
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        getTablexiaContext().setCurreActivity(this);
        if (downloaderClient != null) {
            downloaderClient.connect(this);
        }
        super.onResume();

        // show menu toggle button when there is selected user
        if (getTablexiaContext().getSelectedUser() != null) {
            View v = findViewById(R.id.mainmenu_rightborder);
            if (v != null) {
                v.setVisibility(View.VISIBLE);
            }
        }

        getTablexiaContext().showAllScreen(null);
    }

    @Override
    protected void onDestroy() {
        getTablexiaContext().resetCurrentActivity(this);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (actualMenuFragment != null) {
            actualMenuFragment.onBackPressed();
        }
        if (loaderFragment != null) {
            if (loaderFragment.getClass() == LOADER_FRAGMENR_CLASS) {
                finish();
            } else {
                getTablexiaContext().initMenu();
            }
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public Tablexia getTablexiaContext() {
        return (Tablexia) getApplicationContext();
    }

    /**
     * Result po vytvoreni noveho uzivatele obsahuje jeho ID, timto ho rovnou
     * prihlasime
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == REQUEST_CODE_NEW_USER) && (data != null) && (resultCode == Activity.RESULT_OK)) {
            hideLoadingScreen(false);

            LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("cz.nic.tablexia.action.NEW_USER_CREATED"));

            final long userId = data.getLongExtra("userId", 0);
            if (userId != 0) {
                User user = User.getUsersForId(userId);
                getTablexiaContext().forceSelectUser(user);
                closeMenuDrawer();
            }
        }
        if ((requestCode == REQUEST_CODE_GAME) && (data != null) && (resultCode == RESULT_CODE_NEW_USER)) {
            // TODO show new user dialog
            Log.d(TAG, "Start new user sequence");
        }
        getTablexiaContext().initMenu();
    }

    /* //////////////////////////////////////////// LOADING SCREEN */

    public void showNewPlayerAnimation() {
        getTablexiaContext().hideUserMenuHelp();
        lockCloseMenuDrawer();
        replaceLoaderFragment(new AnimationContainerFragment(), R.anim.slide_in, R.anim.slide_out);
    }

    private void stopAnimations() {
        if ((screenDividerView != null) && (screenDividerView.animate() != null)) {
            screenDividerView.animate().cancel();
            screenDividerView.animate().setListener(null);
        }
        if ((startPreloaderView != null) && (startPreloaderView.animate() != null)) {
            startPreloaderView.animate().cancel();
            startPreloaderView.setX(0);
        }
        if ((screenContainerView != null) && (screenContainerView.animate() != null)) {
            screenContainerView.animate().cancel();
        }
        canHide = false;
    }

    public void showLoadingScreen(boolean animate, Bundle args) {
        stopAnimations();
        try {
            loaderFragment = LOADER_FRAGMENR_CLASS.newInstance();
            if (args != null) {
                loaderFragment.setArguments(args);
            }
        } catch (Exception e) {
            Log.e("Cannot create instance of: " + LOADER_FRAGMENR_CLASS);
        }
        startPreloaderView.setX(0);
        if (animate) {
            getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in, R.anim.fade_out).replace(R.id.main_screen_preloader, loaderFragment, loaderFragment.getClass().toString()).commit();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.main_screen_preloader, loaderFragment, loaderFragment.getClass().toString()).commit();
        }

    }

    private boolean canHide = false;

    public void hideLoadingScreen(boolean animation) {
        final Fragment fragmentToHide = loaderFragment;
        loaderFragment = null;
        if (animation) {
            canHide = true;
            screenContainerView.setX((displayWidth + screenDividerView.getWidth()) - 20);
            screenDividerView.setX(displayWidth - 20);
            screenDividerView.setVisibility(View.VISIBLE);
            TimeInterpolator timeInterpolator = new AccelerateDecelerateInterpolator();

            screenContainerView.animate().setDuration(START_PRELOADER_ANIM_DURATION).setInterpolator(timeInterpolator).x(0);
            startPreloaderView.animate().setDuration(START_PRELOADER_ANIM_DURATION).setInterpolator(timeInterpolator).x(-(displayWidth));
            screenDividerView.animate().setDuration(START_PRELOADER_ANIM_DURATION).setInterpolator(timeInterpolator).x(-screenDividerView.getWidth()).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    if (canHide) {
                        hideFragment(fragmentToHide);
                        screenDividerView.setVisibility(View.INVISIBLE);
                    }
                    canHide = false;
                }
            });
        } else {
            screenContainerView.setX(0);
            hideFragment(fragmentToHide);
        }
    }

    private void hideFragment(Fragment fragmentToHide) {
        if (fragmentToHide != null) {
            getSupportFragmentManager().beginTransaction().remove(fragmentToHide).commitAllowingStateLoss();
        } else {
            Log.e(TAG, "Hiding null fragment");
        }

    }

    public void replaceLoaderFragment(Fragment fragmentToReplace, int inAnimation, int outAnimation) {
        loaderFragment = fragmentToReplace;
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(inAnimation, outAnimation);
        fragmentTransaction.replace(R.id.main_screen_preloader, loaderFragment, loaderFragment.getClass().toString());
        fragmentTransaction.commit();
    }

    /* //////////////////////////////////////////// MENU DRAWER CONTROL */

    @Override
    public void closeMenuDrawer() {
        getTablexiaContext().closeMenuDrawer(menuDrawer);
    }

    @Override
    public void lockCloseMenuDrawer() {
        getTablexiaContext().lockCloseMenuDrawer(menuDrawer);
    }

    @Override
    public void lockOpenMenuDrawer() {
        getTablexiaContext().lockOpenMenuDrawer(menuDrawer);
    }

    @Override
    public void unlockMenuDrawer() {
        getTablexiaContext().unlockOpenMenuDrawer(menuDrawer);
    }

    @Override
    public void addMenuDrawerListener(DrawerListener drawerListener) {
        getTablexiaContext().addMenuDrawerListener(menuDrawer, drawerListener);
    }

    @Override
    public void removeMenuDrawerListener(DrawerListener drawerListener) {
        getTablexiaContext().removeMenuDrawerListener(menuDrawer, drawerListener);
    }

    /*
     * //////////////////////////////////////////// MAIN SCREEN AND MENU
     * FRAGMENTS
     */

    @Override
    public Fragment replaceScreenFragment(MenuEnum menuEnum, final AbstractMenuFragment fragment, boolean forceReplace, boolean forceQuickTransition, final MenuChangeListener menuChangeListener) {
        if (findViewById(MAIN_SCREEN_CONTAINER_ID) != null) {
            AbstractMenuFragment usedFragment = (AbstractMenuFragment) getSupportFragmentManager().findFragmentByTag(fragment.getClass().toString());
            if ((usedFragment != null) && usedFragment.isVisible() && !forceReplace) {
                return usedFragment;
            }

            if (menuChangeListener != null) {
                fragment.setFragmentVisibilityListener(new MenuFragmentVisibilityListener() {

                    @Override
                    public void onFragmentContentVisible() {
                        fragment.setFragmentVisibilityListener(null);
                        menuChangeListener.onMenuChanged(fragment);
                    }
                });
            }
            if (forceQuickTransition || getTablexiaContext().isScreenContentHidden()) {
                getSupportFragmentManager().beginTransaction().replace(MAIN_SCREEN_CONTAINER_ID, fragment, fragment.getClass().toString()).commit();
            } else {
                getTablexiaContext().hideScreenContent(true, new AnimatorListenerAdapter() {

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        getSupportFragmentManager().beginTransaction().replace(MAIN_SCREEN_CONTAINER_ID, fragment, fragment.getClass().toString()).commitAllowingStateLoss();
                    }

                });
            }
            actualMenuFragment = fragment;
            return fragment;
        }
        return null;
    }

    /**
     * Enable force refresh for TablexiaActivity
     */
    @Override
    public boolean isForceMenuRefresh() {
        return true;
    }

    @Override
    public boolean isForceRedirectToCurrentMenu() {
        return false;
    }

    @Override
    public boolean isGameActive(GamesDefinition gameDefinitionToCheck) {
        // no game is active in main activity
        return false;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}
