/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.newusers.fragment;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.AccelerateInterpolator;
import android.widget.HorizontalScrollView;
import cz.nic.tablexia.R;
import cz.nic.tablexia.main.MainActivity;
import cz.nic.tablexia.menu.mainmenu.screen.AbstractMenuFragment;
import cz.nic.tablexia.util.MediaPlayerHelper;
import cz.nic.tablexia.widget.RatioPositionSizeImageView;

/**
 * Newuseranim swipemenu screen fragment. Enables user to swipe from newspaper to the office door.
 *
 * @author Václav Tarantík
 */
public class SwipeMenuFragment extends AbstractMenuFragment {

    private static final String        TAG                        = SwipeMenuFragment.class.getSimpleName();
    private static final int           TOUCH_AREA_VIEW            = R.id.newuseranim_swipemenu_toucharea;
    private static final long          FINGER_UP_ANIMATION_TIME   = 500;
    private static final long          FINGER_LEFT_ANIMATION_TIME = 1000;
    private static final float         FINGER_ACCELERATION_FACTOR = 2.5f;
    private static final int           FINGER_ANIM_DELAY          = 5000;

    private Display                    display;
    private Point                      displaySize;

    private RatioPositionSizeImageView fingerHint;

    private RatioPositionSizeImageView knock1;
    private RatioPositionSizeImageView knock2;
    private RatioPositionSizeImageView knock3;

    private Handler                    handler;

    private int                        knockCounter;

    private boolean                    hintAnimationStarted;
    private HorizontalScrollView       scrollView;

    private boolean                    dataLoadComplete;

    public SwipeMenuFragment() {
        super(R.layout.screen_swipemenu, null);
    }

    @Override
    protected void asyncFragmentDataLoad(View layoutView) {
        displaySize = new Point();
        display = getActivity().getWindowManager().getDefaultDisplay();
        display.getSize(displaySize);

        knock1 = (RatioPositionSizeImageView) layoutView.findViewById(R.id.newuseranim_knock1_image);
        knock2 = (RatioPositionSizeImageView) layoutView.findViewById(R.id.newuseranim_knock2_image);
        knock3 = (RatioPositionSizeImageView) layoutView.findViewById(R.id.newuseranim_knock3_image);

        fingerHint = (RatioPositionSizeImageView) layoutView.findViewById(R.id.newuseranim_hint_finger);

        scrollView = (HorizontalScrollView) layoutView.findViewById(R.id.newuseranim_scrollview);
        scrollView.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                stopHintFingerAnimation();
                handler.postDelayed(bounceRunnable, FINGER_ANIM_DELAY);
                return false;
            }
        });

        prepareTouchArea(layoutView);
    }

    @Override
    protected void fragmentDataLoadComplete() {
        super.fragmentDataLoadComplete();
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("cz.nic.tablexia.action.STREETFRAGMENT_LOADED"));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        handler = new Handler();
    }

    private void prepareTouchArea(View layoutView) {
        final RatioPositionSizeImageView touchArea = (RatioPositionSizeImageView) layoutView.findViewById(TOUCH_AREA_VIEW);
        touchArea.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "Door touch area clicked..");
                knockCounter++;
                if (knockCounter == 1) {
                    knock1.setVisibility(View.VISIBLE);
                    MediaPlayerHelper.playSound("newuser/sfx/tuk_1.mp3", false, getActivity());
                } else if (knockCounter == 2) {
                    knock1.setVisibility(View.GONE);
                    knock2.setVisibility(View.VISIBLE);
                    MediaPlayerHelper.playSound("newuser/sfx/tuk_2.mp3", true, getActivity());
                } else if (knockCounter == 3) {
                    knock2.setVisibility(View.GONE);
                    knock3.setVisibility(View.VISIBLE);

                    MediaPlayerHelper.playSound("newuser/sfx/tuk_3.mp3", true, getActivity());
                    knock3.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            MediaPlayerHelper.muteAll();
                            ((MainActivity) getActivity()).replaceLoaderFragment(new OfficeFragment(), Integer.valueOf(R.anim.fade_in), Integer.valueOf(R.anim.newuseranim_street_fade_out));
                        }
                    }, 500);

                }
            }

        });
    }

    private final Runnable bounceRunnable = new Runnable() {

        @Override
        public void run() {
            hintAnimationStarted = true;
            // pokud máme doscrollováno na poslední desetinu, už nezobrazíme prst
            fingerHint.setVisibility(scrollView.getScrollX() < (((scrollView.getChildAt(0).getMeasuredWidth() - scrollView.getWidth()) * 9) / 10) ? View.VISIBLE : View.GONE);
            fingerHint.animate().translationYBy(-fingerHint.getHeight()).setDuration(FINGER_UP_ANIMATION_TIME).setListener(new AnimatorListener() {

                @Override
                public void onAnimationStart(Animator animation) {
                    // nothing to do
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                    // nothing to do
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    fingerHint.animate().translationXBy(-(displaySize.x / 2)).setDuration(FINGER_LEFT_ANIMATION_TIME).setInterpolator(new AccelerateInterpolator(FINGER_ACCELERATION_FACTOR)).setListener(new AnimatorListener() {

                        @Override
                        public void onAnimationStart(Animator animation) {
                            // nothing

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                            // nothing

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            fingerHint.clearAnimation();
                            fingerHint.setTranslationX(displaySize.x / 4);
                            fingerHint.setTranslationY(displaySize.y);
                            if (hintAnimationStarted) {
                                fingerHint.postDelayed(bounceRunnable, 100);
                            }

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            // nothing

                        }
                    }).start();
                }

                @Override
                public void onAnimationCancel(Animator animation) {}
            }).start();

        }
    };

    public void animateHintFinger() {
        fingerHint.setTranslationX(displaySize.x / 4);
        fingerHint.setTranslationY(displaySize.y);

        handler.postDelayed(bounceRunnable, FINGER_ANIM_DELAY);
    }

    @Override
    public void onPause() {
        super.onPause();
        stopHintFingerAnimation();
    }

    private void stopHintFingerAnimation() {
        if (fingerHint != null) {
            fingerHint.clearAnimation();
        }
        hintAnimationStarted = false;
        handler.removeCallbacks(bounceRunnable);
    }

    public boolean isDataLoadComplete() {
        return dataLoadComplete;
    }

}
