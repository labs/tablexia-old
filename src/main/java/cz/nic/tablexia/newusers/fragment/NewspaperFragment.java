/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.newusers.fragment;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;

import com.activeandroid.util.Log;

import cz.nic.tablexia.R;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.menu.MenuActivity;
import cz.nic.tablexia.util.MediaPlayerHelper;
import cz.nic.tablexia.util.TimeUtils;
import cz.nic.tablexia.widget.RatioPositionSizeImageView;
import cz.nic.tablexia.widget.RatioPositionTextView;

public class NewspaperFragment extends Fragment implements OnTouchListener, OnClickListener {
    private static final int           NEWUSER_LAYOUT_BALCONY_ID            = R.layout.newuseranim_balcony;
    private static final int           NEWSPAPER_IMAGE_SIZE_X               = 1920;
    private static final int           NEWSPAPER_ANIMATION_TRASITION_TIME   = 100;
    private static final float         TRANSLATE_FINGERS_X_BY               = 180;
    private static final long          TRANSLATE_FINGERS_ANIMATION_DURATION = 500;

    private static final String        TAG                                  = NewspaperFragment.class.getSimpleName();

    private ViewGroup                  pageLayout;
    private Bitmap                     backgroundBitmap;
    private Display                    display;
    private Point                      displaySize;
    private View                       newspaperView;
    private ImageView                  newspaper;
    private View                       newspaperOpenOne;
    float                              originalXDiff;
    float                              actualXDiff;

    private boolean                    animatedFromNewspaper;
    private boolean                    newspaperRotated;

    private Bitmap                     newspaperColorBitmap;

    private RatioPositionSizeImageView imageViewFingerLeft;
    private RatioPositionSizeImageView imageViewFingerRight;
    private RatioPositionSizeImageView arrowNewspaper;

    private RatioPositionTextView      topicBeard;
    private RatioPositionTextView      topicBalloon;
    private RatioPositionTextView      topicMotocycle;
    private RatioPositionTextView      topicSwimwear;
    private RatioPositionTextView      topicDetective;
    private boolean                    detectveBubbleOn;
    private boolean                    newspaperAnimated;

    private ImageView                  newspaperClosed;
    private ImageView                  newspaperSlide1;
    private ImageView                  newspaperSlide2;
    private ImageView                  newspaperSlide3;
    private ImageView                  newspaperSlide4;
    private ImageView                  newspaperOpened;

    private float                      displayRatio;

    private Handler                    handler;
    private Runnable                   runnable;

    private boolean                    fingersAnimationStarted;
    private boolean                    arrowAnimationStarted;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        pageLayout = (ViewGroup) inflater.inflate(NEWUSER_LAYOUT_BALCONY_ID, null);

        pageLayout.setAnimationCacheEnabled(true);
        pageLayout.setDrawingCacheEnabled(true);

        handler = new Handler();

        backgroundBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.newuseranim_balcony);

        pageLayout.setOnTouchListener(this);
        newspaper = (ImageView) pageLayout.findViewById(R.id.newuseranim_newspaper_image);
        newspaperView = pageLayout.findViewById(R.id.newuseranim_newspaper);
        if (TimeUtils.isWinter()) {
            newspaperView.setBackgroundResource(R.drawable.newuseranim_balcony_winter);
        }
        newspaperOpenOne = pageLayout.findViewById(R.id.newuseranim_newspaper_layout);
        newspaperClosed = (ImageView) pageLayout.findViewById(R.id.newuseranim_newspaper_closed);
        newspaperSlide1 = (ImageView) pageLayout.findViewById(R.id.newuseranim_newspaper_slide1);
        newspaperSlide2 = (ImageView) pageLayout.findViewById(R.id.newuseranim_newspaper_slide2);
        newspaperSlide3 = (ImageView) pageLayout.findViewById(R.id.newuseranim_newspaper_slide3);
        newspaperSlide4 = (ImageView) pageLayout.findViewById(R.id.newuseranim_newspaper_slide4);
        newspaperOpened = (ImageView) pageLayout.findViewById(R.id.newuseranim_newspaper_opened_image);

        imageViewFingerLeft = (RatioPositionSizeImageView) pageLayout.findViewById(R.id.newuseranim_fingerleft_image);
        imageViewFingerRight = (RatioPositionSizeImageView) pageLayout.findViewById(R.id.newuseranim_fingerright_image);

        newspaperOpened.setOnClickListener(this);

        originalXDiff = 0;
        actualXDiff = 0;

        topicBeard = (RatioPositionTextView) pageLayout.findViewById(R.id.newuseranim_bubble_beard);
        topicBalloon = (RatioPositionTextView) pageLayout.findViewById(R.id.newuseranim_bubble_balloon);
        topicMotocycle = (RatioPositionTextView) pageLayout.findViewById(R.id.newuseranim_bubble_motocycle);
        topicSwimwear = (RatioPositionTextView) pageLayout.findViewById(R.id.newuseranim_bubble_swimsuit);
        topicDetective = (RatioPositionTextView) pageLayout.findViewById(R.id.newuseranim_bubble_detective);

        handler.removeCallbacks(runnable);
        arrowNewspaper = (RatioPositionSizeImageView) pageLayout.findViewById(R.id.arrow_newspaper);

        newspaperColorBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.newuseranim_newspaper_opened_colormap);

        newspaperOpenOne.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getPointerCount() == 2) {
                    newspaperSlide1.setVisibility(View.VISIBLE);
                    newspaperClosed.setVisibility(View.GONE);
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_MOVE:
                            actualXDiff = Math.abs(event.getX(event.findPointerIndex(event.getPointerId(0))) - event.getX(event.findPointerIndex(event.getPointerId(1))));
                            if (originalXDiff == 0) {
                                originalXDiff = actualXDiff;
                            }

                            if ((actualXDiff > (originalXDiff + (20 / displayRatio))) && !newspaperAnimated) {
                                newspaperAnimated = true;
                                newspaperOpenOne.setOnTouchListener(null);
                                stopFingersAnimation();
                                animateNewspaper();
                                MediaPlayerHelper.playSound("newuser/sfx/noviny_rozbaleni.mp3", true, getActivity());
                            }

                            break;
                        case MotionEvent.ACTION_UP:
                            originalXDiff = 0;
                            actualXDiff = 0;
                            break;
                    }
                } else {
                    newspaperSlide1.setVisibility(View.GONE);
                    newspaperClosed.setVisibility(View.VISIBLE);
                }
                return true;
            }
        });
        newspaperOpened.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideAllTopics();

                double ratioX = (double) newspaperColorBitmap.getWidth() / (double) displaySize.x;
                double ratioY = (double) newspaperColorBitmap.getHeight() / (double) displaySize.y;
                int pixel = newspaperColorBitmap.getPixel((int) (event.getX() * ratioX), (int) (event.getY() * ratioY));

                Log.d("R: " + Color.red(pixel) + " G: " + Color.green(pixel) + " B: " + Color.blue(pixel));

                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    // CERNA-inzerat detektiv
                    if ((Color.red(pixel) == 0) && (Color.blue(pixel) == 0) && (Color.green(pixel) == 0) && (Color.alpha(pixel) == 255)) {
                        showNewspaperDialog(R.drawable.newuser_noviny_detail05, R.id.newuseranim_bubble_detective, true);
                        // topicDetective.setVisibility(View.VISIBLE);
                        MediaPlayerHelper.playSound("newuser/mfx/noviny_tobymohlobyt.mp3", true, getActivity());
                        // detectveBubbleOn = true;
                        // ZLUTA-plavky
                    } else if ((Color.red(pixel) == 255) && (Color.green(pixel) == 255)) {
                        Log.d("YELLOW");
                        showNewspaperDialog(R.drawable.newuser_noviny_detail04, R.id.newuseranim_bubble_swimsuit);
                        // topicSwimwear.setVisibility(View.VISIBLE);
                        MediaPlayerHelper.playSound("newuser/mfx/noviny_nakoupanibude.mp3", true, getActivity());
                        // MODRA-motokolo
                    } else if (Color.blue(pixel) == 255) {
                        Log.d("BLUE");
                        showNewspaperDialog(R.drawable.newuser_noviny_detail03, R.id.newuseranim_bubble_motocycle);
                        // topicMotocycle.setVisibility(View.VISIBLE);
                        MediaPlayerHelper.playSound("newuser/mfx/noviny_peknyalekde.mp3", true, getActivity());
                        // CERVENA - vousy
                    } else if (Color.red(pixel) == 255) {
                        Log.d("RED");
                        showNewspaperDialog(R.drawable.newuser_noviny_detail01, R.id.newuseranim_bubble_beard);
                        // topicBeard.setVisibility(View.VISIBLE);
                        MediaPlayerHelper.playSound("newuser/mfx/noviny_radsisipockam.mp3", true, getActivity());
                        // ZELENA - vzduchoplavec
                    } else if (Color.green(pixel) == 255) {
                        Log.d("GREEN");
                        showNewspaperDialog(R.drawable.newuser_noviny_detail02, R.id.newuseranim_bubble_balloon);
                        // topicBalloon.setVisibility(View.VISIBLE);
                        MediaPlayerHelper.playSound("newuser/mfx/noviny_tenpriletiaz.mp3", true, getActivity());
                    } else {
                        hideAllTopics();
                    }

                    return true;
                }
                return false;
            }
        });

        topicBeard.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                topicBeard.setVisibility(View.GONE);
            }
        });
        topicBalloon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                topicBalloon.setVisibility(View.GONE);
            }
        });
        topicDetective.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!animatedFromNewspaper) {
                    // newspaperView.setBackgroundColor(Color.BLACK);

                    hideAllTopics();
                    newspaperOpened.setEnabled(false);

                    animatedFromNewspaper = true;
                    // hideNewspaper();
                    hideAllTopics();
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("cz.nic.tablexia.action.SLIDE_NEWSPAPER"));
                }
            }
        });
        topicMotocycle.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                topicMotocycle.setVisibility(View.GONE);
            }
        });
        topicSwimwear.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                topicSwimwear.setVisibility(View.GONE);
            }
        });

        displaySize = new Point();
        display = getActivity().getWindowManager().getDefaultDisplay();
        display.getSize(displaySize);

        displayRatio = (float) NEWSPAPER_IMAGE_SIZE_X / (float) displaySize.x;

        return pageLayout;
    }

    private void showNewspaperDialog(int imageResource, int bubbleId) {
        showNewspaperDialog(imageResource, bubbleId, false);
    }

    private void showNewspaperDialog(int imageResource, int bubbleId, boolean detective) {
        DialogFragment newDialogFragment = new NewspaperDialog();

        Bundle bundle = new Bundle();
        bundle.putInt(NewspaperDialog.BUBBLE_KEY, bubbleId);
        bundle.putInt(NewspaperDialog.IMAGE_KEY, imageResource);
        bundle.putBoolean(NewspaperDialog.DETECTIVE_KEY, detective);
        newDialogFragment.setArguments(bundle);

        newDialogFragment.show(getActivity().getFragmentManager(), NewspaperDialog.class.getName());
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getTablexiaContext().showScreenContent(null);
    }

    protected Tablexia getTablexiaContext() {
        return ((MenuActivity) getActivity()).getTablexiaContext();
    }

    protected MenuActivity getMenuActivity() {
        return (MenuActivity) getActivity();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        double ratioX = (double) backgroundBitmap.getWidth() / displaySize.x;
        double ratioY = (double) backgroundBitmap.getHeight() / displaySize.y;
        if (((event.getX() * ratioX) < backgroundBitmap.getWidth()) && ((event.getY() * ratioY) < backgroundBitmap.getHeight())) {
            Log.d("TOUCH", event.getX() + ", " + event.getY());
            int touchX = (int) (event.getX() * ratioX);
            int touchY = (int) (event.getY() * ratioY);
            if ((touchX < 0) || (touchX >= backgroundBitmap.getWidth()) || (touchY < 0) || (touchY >= backgroundBitmap.getHeight())) {
                return false;
            }
            int pixel = backgroundBitmap.getPixel(touchX, touchY);
            if (Color.alpha(pixel) != 255) {
                handler.removeCallbacks(runnable);
                arrowNewspaper.clearAnimation();
                arrowNewspaper.setVisibility(View.GONE);
                Log.d("ALPHA", Integer.toString(Color.alpha(pixel)));
                if (!newspaperRotated) {
                    MediaPlayerHelper.playSound("newuser/sfx/noviny_prilet.mp3", true, getActivity());
                    newspaperView.setVisibility(View.VISIBLE);
                    newspaper.setVisibility(View.VISIBLE);
                    newspaperView.setBackgroundResource(TimeUtils.isWinter() ? R.drawable.newuseranim_balcony_blur_winter : R.drawable.newuseranim_balcony_blur);

                    AnimationSet animationSet = new AnimationSet(true);

                    Animation nPAnim = new ScaleAnimation(0.01f, 1.25f, 0.01f, 1.25f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                    nPAnim.setDuration(500);

                    Animation ani = new RotateAnimation(0, /* from degree */
                        360, /* to degree */
                        Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                    ani.setDuration(100);
                    ani.setFillAfter(false);
                    ani.setRepeatCount(10);
                    ani.setRepeatMode(Animation.RESTART);

                    animationSet.addAnimation(nPAnim);
                    animationSet.addAnimation(ani);
                    animationSet.setInterpolator(new LinearInterpolator());
                    animationSet.setFillAfter(true);
                    animationSet.setAnimationListener(new AnimationListener() {

                        @Override
                        public void onAnimationStart(Animation animation) {
                            // nic
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                            // nic
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            // Set the content view to 0% opacity but visible, so that it is visible
                            // (but fully transparent) during the animation.
                            newspaperClosed.setAlpha(0f);
                            newspaperClosed.setVisibility(View.VISIBLE);

                            // Animate the content view to 100% opacity, and clear any animation
                            // listener set on the view.
                            newspaperClosed.animate().alpha(1f).setDuration(1000).setListener(new AnimatorListener() {

                                @Override
                                public void onAnimationStart(Animator animation) {
                                    newspaperOpenOne.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {
                                    // nothing

                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    animateFingers();

                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {
                                    // nothing

                                }
                            });

                            // Animate the loading view to 0% opacity. After the animation ends,
                            // set its visibility to GONE as an optimization step (it won't
                            // participate in layout passes, etc.)
                            newspaper.animate().alpha(0f).setDuration(1000).setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    newspaper.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                    newspaper.startAnimation(animationSet);
                    newspaperRotated = true;
                }
            }
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        if (detectveBubbleOn) {
            topicDetective.setVisibility(View.GONE);
            detectveBubbleOn = false;
        }

    }

    private void hideAllTopics() {
        topicBalloon.setVisibility(View.GONE);
        topicDetective.setVisibility(View.GONE);
        topicBeard.setVisibility(View.GONE);
        topicMotocycle.setVisibility(View.GONE);
        topicSwimwear.setVisibility(View.GONE);
    }

    private void hideNewspaper() {
        newspaperOpened.setVisibility(View.GONE);
    }

    private Runnable bounceRunnable = new Runnable() {

        @Override
        public void run() {
            Log.d("Runnable", "Arrow");
            arrowAnimationStarted = true;
            arrowNewspaper.animate().translationXBy(1).setDuration(1000).setInterpolator(new OvershootInterpolator(500.0f)).setListener(new AnimatorListener() {

                @Override
                public void onAnimationStart(Animator animation) {
                    // nothing to do
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                    // nothing to do
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    arrowNewspaper.clearAnimation();
                    arrowNewspaper.setTranslationX((Float) arrowNewspaper.getTag());
                    if (arrowAnimationStarted) {
                        handler.postDelayed(bounceRunnable, 100);
                    }
                }

                @Override
                public void onAnimationCancel(Animator animation) {}
            }).start();

        }
    };

    public void animateArrow() {
        int[] arrowInitialPos = new int[2];
        arrowNewspaper.getLocationOnScreen(arrowInitialPos);
        arrowNewspaper.setTag(Float.valueOf(arrowInitialPos[0]));
        final int arrowInitialX = arrowInitialPos[0];
        Log.d(TAG, "ARROW initial pos: " + arrowInitialX);

        int delay = 500; // delay for 5 sec.
        handler.postDelayed(bounceRunnable, delay);
    }

    private void animateNewspaper() {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    Thread.sleep(NEWSPAPER_ANIMATION_TRASITION_TIME);
                } catch (InterruptedException e) {
                    Log.e(TAG, "Error animating newspaper");
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                newspaperSlide1.setVisibility(View.GONE);
                newspaperSlide2.setVisibility(View.VISIBLE);
                AsyncTask<Void, Void, Void> task2 = new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        try {
                            Thread.sleep(NEWSPAPER_ANIMATION_TRASITION_TIME);
                        } catch (InterruptedException e) {
                            Log.e(TAG, "Error animating newspaper");
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        super.onPostExecute(result);
                        newspaperSlide2.setVisibility(View.GONE);
                        newspaperSlide3.setVisibility(View.VISIBLE);
                        AsyncTask<Void, Void, Void> task3 = new AsyncTask<Void, Void, Void>() {

                            @Override
                            protected Void doInBackground(Void... params) {
                                try {
                                    Thread.sleep(NEWSPAPER_ANIMATION_TRASITION_TIME);
                                } catch (InterruptedException e) {
                                    Log.e(TAG, "Error animating newspaper");
                                }
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void result) {
                                super.onPostExecute(result);
                                newspaperSlide3.setVisibility(View.GONE);
                                newspaperSlide4.setVisibility(View.VISIBLE);
                                AsyncTask<Void, Void, Void> task4 = new AsyncTask<Void, Void, Void>() {

                                    @Override
                                    protected Void doInBackground(Void... params) {
                                        try {
                                            Thread.sleep(NEWSPAPER_ANIMATION_TRASITION_TIME);
                                        } catch (InterruptedException e) {
                                            Log.e(TAG, "Error animating newspaper");
                                        }
                                        return null;
                                    }

                                    @Override
                                    protected void onPostExecute(Void result) {
                                        super.onPostExecute(result);
                                        newspaperOpenOne.setVisibility(View.GONE);
                                        newspaperSlide1.setVisibility(View.GONE);
                                        newspaperSlide2.setVisibility(View.GONE);
                                        newspaperSlide3.setVisibility(View.GONE);
                                        newspaperSlide4.setVisibility(View.GONE);
                                        newspaperOpened.setVisibility(View.VISIBLE);
                                    }

                                };
                                task4.execute();
                            }

                        };
                        task3.execute();
                    }

                };
                task2.execute();
            }

        };
        task.execute();
    }

    private Runnable fingersRunnable = new Runnable() {

        @Override
        public void run() {

            Log.d("Runnable", "Fingers");
            fingersAnimationStarted = true;
            imageViewFingerLeft.animate().translationXBy(-TRANSLATE_FINGERS_X_BY).setDuration(TRANSLATE_FINGERS_ANIMATION_DURATION).setInterpolator(new AccelerateInterpolator()).setListener(new AnimatorListener() {

                @Override
                public void onAnimationStart(Animator animation) {
                    // nothing to do
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                    // nothing to do
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    imageViewFingerLeft.animate().translationXBy(TRANSLATE_FINGERS_X_BY).setDuration(200).setInterpolator(new AccelerateInterpolator()).setListener(new AnimatorListener() {

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            // nothing

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            imageViewFingerLeft.clearAnimation();
                            imageViewFingerLeft.setTranslationX((Float) imageViewFingerLeft.getTag());

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                            // nothing

                        }

                        @Override
                        public void onAnimationStart(Animator animation) {
                            // nothing

                        }

                    }).start();
                }

                @Override
                public void onAnimationCancel(Animator animation) {}
            }).start();

            imageViewFingerRight.animate().translationXBy(TRANSLATE_FINGERS_X_BY).setDuration(TRANSLATE_FINGERS_ANIMATION_DURATION).setInterpolator(new AccelerateInterpolator()).setListener(new AnimatorListener() {

                @Override
                public void onAnimationStart(Animator animation) {
                    // nothing to do
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                    // nothing to do
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    imageViewFingerRight.animate().translationXBy(-TRANSLATE_FINGERS_X_BY).setDuration(200).setInterpolator(new AccelerateInterpolator()).setListener(new AnimatorListener() {

                        @Override
                        public void onAnimationStart(Animator animation) {
                            // nothing

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                            // nothing

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            imageViewFingerRight.clearAnimation();
                            imageViewFingerRight.setTranslationX((Float) imageViewFingerRight.getTag());
                            if (fingersAnimationStarted) {
                                handler.postDelayed(fingersRunnable, 100);
                            }
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            // nothing

                        }
                    }).start();
                }

                @Override
                public void onAnimationCancel(Animator animation) {}
            }).start();

        }
    };

    // TODO obnovit v onResume
    @Override
    public void onPause() {
        stopFingersAnimation();
        super.onPause();
    };

    private void animateFingers() {
        int[] leftFingerInitialPos = new int[2];
        imageViewFingerLeft.getLocationInWindow(leftFingerInitialPos);
        imageViewFingerLeft.setTag(Float.valueOf(leftFingerInitialPos[0]));

        int[] rightFingerInitialPos = new int[2];
        imageViewFingerRight.getLocationInWindow(rightFingerInitialPos);
        imageViewFingerRight.setTag(Float.valueOf(rightFingerInitialPos[0]));

        int delay = 500; // delay for 5 sec.
        handler.postDelayed(fingersRunnable, delay);
    }

    private void stopFingersAnimation() {
        imageViewFingerLeft.clearAnimation();
        imageViewFingerRight.clearAnimation();
        imageViewFingerLeft.setVisibility(View.GONE);
        imageViewFingerRight.setVisibility(View.GONE);
        arrowAnimationStarted = false;
        fingersAnimationStarted = false;
        handler.removeCallbacks(bounceRunnable);
        handler.removeCallbacks(fingersRunnable);
    }
}
