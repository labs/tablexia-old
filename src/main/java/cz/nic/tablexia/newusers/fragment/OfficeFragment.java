/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package cz.nic.tablexia.newusers.fragment;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.View.OnClickListener;
import cz.nic.tablexia.R;
import cz.nic.tablexia.menu.MenuActivity;
import cz.nic.tablexia.menu.mainmenu.screen.AbstractMenuFragment;
import cz.nic.tablexia.menu.usermenu.User;
import cz.nic.tablexia.util.MediaPlayerHelper;
import cz.nic.tablexia.widget.RatioPositionTextView;

public class OfficeFragment extends AbstractMenuFragment {
    private static final int NEWUSER_OFFICE_LAYOUT_ID = R.layout.newuseranim_office;

    private RatioPositionTextView dialogBubble;
    private int                   dialogPosition; // pozice, na ktere se v dialogu s detektivem nachazime

    public OfficeFragment() {
        super(NEWUSER_OFFICE_LAYOUT_ID, null);
    }

    @Override
    protected void asyncFragmentDataLoad(View layoutView) {
        dialogPosition = 1;
        User u = getTablexiaContext().getTablexiaState().getActiveUser();

        if(u == null){
            MediaPlayerHelper.playSound("newuser/mfx/detektiv_1.mp3", false, getActivity());
        }

        dialogBubble = (RatioPositionTextView) layoutView.findViewById(R.id.newuseranim_bubble_conversation);
        dialogBubble.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogPosition++;
                switch (dialogPosition) {
                    case 2:
                        dialogBubble.setText(getResources().getString(R.string.newuseranim_dialog_2));
                        MediaPlayerHelper.playSound("newuser/mfx/detektiv_2.mp3", true, getActivity());
                        break;
                    case 3:
                        dialogBubble.setText(getResources().getString(R.string.newuseranim_dialog_3));
                        MediaPlayerHelper.playSound("newuser/mfx/detektiv_3.mp3", true, getActivity());
                        break;
                    case 4:
                        dialogBubble.setText(getResources().getString(R.string.newuseranim_dialog_4));
                        MediaPlayerHelper.playSound("newuser/mfx/detektiv_4.mp3", true, getActivity());
                        break;
                    case 5:
                        ((MenuActivity) getActivity()).getTablexiaContext().showNewUserScreen();
                    default:
                        break;
                }
            }
        });
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("cz.nic.tablexia.action.MUTE_AMBIENT"));
        super.onResume();
    }

    @Override
    public void onPause() {
        MediaPlayerHelper.muteAll();
        super.onPause();
    }
}
