package cz.nic.tablexia.newusers.fragment.newuser;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import cz.nic.tablexia.newusers.fragment.AnimationContainerFragment;

public class StreetFragmentLoadedBroadcastReceiver extends BroadcastReceiver {
    private AnimationContainerFragment containerFragment;

    public StreetFragmentLoadedBroadcastReceiver(AnimationContainerFragment containerFragment) {
        this.containerFragment = containerFragment;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        containerFragment.setStreetFragmentLoaded(true);
        if (containerFragment.isReadyToSwipeToStreet()) {
            containerFragment.swipeToStreet();
            containerFragment.setStreetFragmentLoaded(false);
        }
    }

}
