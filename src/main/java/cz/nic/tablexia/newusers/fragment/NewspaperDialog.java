/**
 *
 */

package cz.nic.tablexia.newusers.fragment;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import cz.nic.tablexia.R;
import cz.nic.tablexia.widget.TablexiaDialog;

/**
 * @author lhoracek
 */
public class NewspaperDialog extends TablexiaDialog {
    public static final String BUBBLE_KEY    = "VIEW_TEXT_BUBBLE";
    public static final String IMAGE_KEY     = "IMAGE_CONTENT";
    public static final String DETECTIVE_KEY = "DETECTIVE";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Holo_Light);
    }

    @Override
    protected int getLayourId() {
        return R.layout.newuser_dialog_newspaper;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        // inflate and adjust layout
        view.setMinimumWidth(displayRectangle.width());
        view.setMinimumHeight(displayRectangle.height());

        int idBubble = getArguments().getInt(BUBBLE_KEY);
        int idImage = getArguments().getInt(IMAGE_KEY);
        View bubble = view.findViewById(idBubble);
        bubble.setVisibility(View.VISIBLE);
        boolean detective = getArguments().getBoolean(DETECTIVE_KEY);
        if (detective) {
            bubble.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("cz.nic.tablexia.action.SLIDE_NEWSPAPER"));
                    dismiss();
                }
            });
        }

        ImageView iv = (ImageView) view.findViewById(R.id.imageView_column);
        iv.setImageResource(idImage);

        iv.setScaleType(ScaleType.FIT_CENTER);
        iv.setAdjustViewBounds(true);

        view.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
