package cz.nic.tablexia.newusers.fragment;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import cz.nic.tablexia.R;
import cz.nic.tablexia.newusers.fragment.newuser.NewUserAnimBroadcastReceiver;
import cz.nic.tablexia.newusers.fragment.newuser.StreetFragmentLoadedBroadcastReceiver;
import cz.nic.tablexia.util.MediaPlayerHelper;

public class AnimationContainerFragment extends Fragment {
    private static final int LAYOUT_ID = R.layout.newuseranim_container_fragment;
    private static final int FRAGMENT_0_LAYOUT_ID = R.id.newuseranim_container_fragment_0;
    private static final int FRAGMENT_1_LAYOUT_ID = R.id.newuseranim_container_fragment_1;

    private Display display;
    private Point displaySize;

    private NewspaperFragment newspaperFragment;
    private SwipeMenuFragment streetFragment;
    private View newspaperView;
    private View streetView;
    private ViewGroup pageLayout;

    private boolean           readyToSwipeToStreet;
    private boolean           streetFragmentLoaded;

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("cz.nic.tablexia.action.MUTE_AMBIENT"));
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    Bundle savedInstanceState) {
        pageLayout = (ViewGroup) inflater.inflate(LAYOUT_ID, null);
        pageLayout.setAnimationCacheEnabled(true);
        pageLayout.setDrawingCacheEnabled(true);

        displaySize = new Point();
        display = getActivity().getWindowManager().getDefaultDisplay();
        display.getSize(displaySize);

        addChildFragmentsToScreen();
        newspaperView = pageLayout.findViewById(FRAGMENT_0_LAYOUT_ID);
        streetView = pageLayout.findViewById(FRAGMENT_1_LAYOUT_ID);
        streetView.setTranslationX(displaySize.x);

        NewUserAnimBroadcastReceiver receiver = new NewUserAnimBroadcastReceiver(
        this);
        IntentFilter filter = new IntentFilter(
        "cz.nic.tablexia.action.SLIDE_NEWSPAPER");
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
        receiver, filter);

        StreetFragmentLoadedBroadcastReceiver streetLoadedReceiver = new StreetFragmentLoadedBroadcastReceiver(this);
        IntentFilter streetFragmentLoadedFilter = new IntentFilter("cz.nic.tablexia.action.STREETFRAGMENT_LOADED");
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(streetLoadedReceiver, streetFragmentLoadedFilter);

        MediaPlayerHelper.playSound("newuser/sfx/atmo_kavarna.mp3", true, true, getActivity());
        return pageLayout;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (nextAnim != 0) {
            Animation anim = AnimationUtils.loadAnimation(getActivity(),
            nextAnim);

            anim.setAnimationListener(new AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {
                    // nothing needed
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                    // nothing needed
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    if (newspaperFragment != null) {
                        newspaperFragment.animateArrow();
                    }
                }
            });
            return anim;
        } else {
            return super.onCreateAnimation(transit, enter, nextAnim);
        }

    }

    private void addChildFragmentsToScreen() {
        newspaperFragment = new NewspaperFragment();
        streetFragment = new SwipeMenuFragment();

        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(FRAGMENT_0_LAYOUT_ID, newspaperFragment);
        ft.add(FRAGMENT_1_LAYOUT_ID, streetFragment);
        ft.commit();
    }

    @Override
    public void onPause() {
        MediaPlayerHelper.muteAll();
        super.onPause();
    }

    public void swipeToStreet() {
        MediaPlayerHelper.stopSound("newuser/sfx/atmo_kavarna.mp3");
        MediaPlayerHelper.playSound("newuser/sfx/cesta_do_kancelare.mp3", true, getActivity());
        MediaPlayerHelper.playSound("newuser/sfx/atmo_mesto.mp3", true, true, getActivity());
        newspaperView.animate().translationX(-displaySize.x).setDuration(1000)
        .start();
        streetView.animate().translationX(0).setDuration(1000)
        .setListener(new AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {
                // nothing
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                // nothing
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                final class NewspaperDeleteAsyncTask extends
                AsyncTask<Void, Void, Void> {

                    @Override
                    protected Void doInBackground(Void... params) {
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                FragmentManager fm = getChildFragmentManager();
                                FragmentTransaction ft = fm
                                .beginTransaction();
                                ft.remove(newspaperFragment);
                                ft.commit();
                            }
                        });
                        return null;
                    }

                }
                streetView.setBackgroundColor(Color.BLACK);
                streetFragment.animateHintFinger();
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                // nothing

            }
        }).start();
    }

    public boolean isReadyToSwipeToStreet() {
        return readyToSwipeToStreet;
    }

    public void setReadyToSwipeToStreet(boolean readyToSwipeToStreet) {
        this.readyToSwipeToStreet = readyToSwipeToStreet;
    }

    public boolean isStreetFragmentLoaded() {
        return streetFragmentLoaded;
    }

    public void setStreetFragmentLoaded(boolean streetFragmentLoaded) {
        this.streetFragmentLoaded = streetFragmentLoaded;
    }

}
