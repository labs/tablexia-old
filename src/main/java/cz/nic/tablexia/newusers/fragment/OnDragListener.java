/*******************************************************************************
 *     Tablexia
 * 
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.newusers.fragment;

import android.graphics.Point;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

/**
 * @author lhoracek
 */
public class OnDragListener implements OnTouchListener {
    private static final int MINIMUM_VISIBLE_SQUARE_OF_VIEW_SIDE = 80;
    private Point startPoint;

    private Point firstTouchPoint;            // bod doteku na prvku pro vypocitani offsetu
    private Point displaySize;

    public OnDragListener(Point displaySize) {
        super();
        this.displaySize = displaySize;
    }

    private OnDropListener onDropListener;

    public void setOnDropListener(OnDropListener onDropListener) {
        this.onDropListener = onDropListener;
        startPoint = null;
    }

    @Override
    public boolean onTouch(final View v, MotionEvent event) {
        // Handle event
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: {
                startPoint = new Point((int) v.getTranslationX(), (int) v.getTranslationY());
                firstTouchPoint = new Point((int) event.getX(), (int) event.getY());
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP: {
                if (onDropListener != null) {
                    onDropListener.onDrop(v, event, startPoint);
                }
                break;
            }

            case MotionEvent.ACTION_MOVE: {
                Point exactTouchPointOnScreen = new Point((int) (v.getX() + event.getX()) + MINIMUM_VISIBLE_SQUARE_OF_VIEW_SIDE, (int) (v.getY() + event.getY()) + MINIMUM_VISIBLE_SQUARE_OF_VIEW_SIDE);
                float centerX = ((v.getTranslationX() + event.getX()));
                float centerY = ((v.getTranslationY() + event.getY()));
                if ((exactTouchPointOnScreen.x >= 0) && (exactTouchPointOnScreen.x <= displaySize.x) && (exactTouchPointOnScreen.y >= 0) && (exactTouchPointOnScreen.y <= displaySize.y)) {
                    v.setTranslationX(centerX - firstTouchPoint.x);
                    v.setTranslationY(centerY - firstTouchPoint.y);
                }
                break;
            }
        }

        // Indicate event was handled
        return true;
    }
}
