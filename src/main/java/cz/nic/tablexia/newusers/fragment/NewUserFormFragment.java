/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.newusers.fragment;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.lang3.StringUtils;

import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;
import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnLayoutChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;

import cz.nic.tablexia.R;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.audio.SoundControl;
import cz.nic.tablexia.audio.resources.SfxSounds;
import cz.nic.tablexia.menu.usermenu.User;
import cz.nic.tablexia.newusers.NewUserActivity;
import cz.nic.tablexia.newusers.fragment.newuser.SignatureView;
import cz.nic.tablexia.util.FileHelper;
import cz.nic.tablexia.util.MediaPlayerHelper;
import cz.nic.tablexia.util.Nullable;
import cz.nic.tablexia.widget.RatioPositionTextView;
import cz.nic.tablexia.widget.TablexiaButtonViewDialog;

/**
 * @author lhoracek
 */
public class NewUserFormFragment extends RoboFragment implements OnClickListener, OnDropListener {

    private static final String                                          TAG                         = NewUserFormFragment.class.getSimpleName();
    private static final String                                          MUGSHOT_FILE_NAME           = "mugshot";
    private static final String                                          MUGSHOT_FILE_EXTENSION      = ".png";
    private static final int                                             FIRST_AVAILABLE_PHOTO_INDEX = 10;
    private static final int                                             NUMBER_OF_PHOTOS            = 8;

    @InjectView(R.id.newuser_mugshot_image) ImageView                    imageViewMugshot;
    @InjectView(R.id.newuser_stamper_image) @Nullable ImageView          imageViewStamper;
    @InjectView(R.id.newuser_pen_image) @Nullable ImageView              imageViewPen;
    @InjectView(R.id.newuser_stampplaceholder_image) @Nullable ImageView imageViewStamPlaceholder;
    @InjectView(R.id.newuser_subscription_image) @Nullable ImageView     imageViewSubscriptionLine;

    @InjectView(R.id.newuser_sex_female_image) ImageView                 imageViewSexFemale;
    @InjectView(R.id.newuser_sex_male_image) ImageView                   imageViewSexMale;

    @InjectView(R.id.newuser_plusAge_image) ImageView                    imageViewBtnPlus;
    @InjectView(R.id.newuser_minusAge_image) ImageView                   imageViewBtnMinus;

    @InjectView(R.id.newuser_sex_text) TextView                          textViewSex;
    @InjectView(R.id.newuser_sex_seekbar) SeekBar                        imageViewSexSeekBar;
    @InjectView(R.id.newuser_signature_image) @Nullable ImageView        imageViewSignaturePicture;
    @InjectView(R.id.newuser_button_subscribe) @Nullable TextView        btnSubscribe;

    private Display                                                      display;
    private Point                                                        displaySize;

    private Sex                                                          sex;
    private int                                                          age;
    private String                                                       avatarName;
    private String                                                       subscriptionFileName;
    private View                                                         mugshotPicker;
    private EditText                                                     coverNameET;
    private String                                                       coverName;
    private TextView                                                     textViewAge;

    private Point                                                        stamperOriginalPosition;
    private Point                                                        penOriginalPosition;

    // Textviews pro napovedu
    private RatioPositionTextView                                        txvHelpAvatar;
    private RatioPositionTextView                                        txvHelpCovername;
    private boolean                                                      covernameHelVisible;
    private RatioPositionTextView                                        txvHelpAge;
    private boolean                                                      ageHelpVisible;
    private RatioPositionTextView                                        txvHelpSex;
    private boolean                                                      sexHelVisible;
    private RatioPositionTextView                                        txvHelpSubscr;
    private boolean                                                      subscrHelVisible;
    private RatioPositionTextView                                        txvHelpStamp;
    private boolean                                                      stampHelVisible;

    private boolean                                                      helpViewInstantiated;
    private boolean                                                      subscriptionDialogShown;

    private boolean                                                      invalidCoverName;

    private Runnable                                                     runnable;

    private Timer                                                        timer;

    protected SoundControl                                               newUserSoundControll;

    private SeekBar                                                      difficultySeekbar;
    private boolean                                                      sexSet;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // INITIALIZE
        initScreen();

        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.newuser_fragment, container, false);

        textViewAge = (TextView) view.findViewById(R.id.newuser_age_text);

        coverNameET = (EditText) view.findViewById(R.id.newuser_covername_text);
        coverNameET.setClickable(true);
        coverNameET.setOnFocusChangeListener(new OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // txvHelpCovername.setVisibility(View.GONE);
            }
        });

        final Drawable icon = getResources().getDrawable(R.drawable.newuser_error);
        icon.setBounds(0, 0, icon.getIntrinsicWidth(), icon.getIntrinsicHeight());
        coverNameET.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {
                coverName = coverNameET.getText().toString();
                invalidCoverName = User.existsUsersForName(coverNameET.getText().toString());
                if (invalidCoverName) {
                    coverNameET.setCompoundDrawables(null, null, icon, null);
                } else {
                    coverNameET.setCompoundDrawables(null, null, null, null);
                }
            }
        });

        coverNameET.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE) || (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {

                    if (invalidCoverName) {
                        Log.d("FORM", "Ivalid user name");
                        Toast.makeText(getActivity(), getResources().getText(R.string.newuser_covername_error), Toast.LENGTH_SHORT).show();
                    } else if ((coverName == null) || coverName.isEmpty()) {
                        Log.d("FORM", "Empty user name");
                        Toast.makeText(getActivity(), getResources().getText(R.string.newuser_covername_empty), Toast.LENGTH_SHORT).show();
                    } else {
                        txvHelpCovername.setVisibility(View.GONE);
                        if (!ageHelpVisible) {
                            txvHelpAge.setVisibility(View.VISIBLE);
                            MediaPlayerHelper.playSound("newuser/mfx/profil_vek.mp3", true, getActivity());
                            ageHelpVisible = true;
                            covernameHelVisible = true;
                            imageViewBtnPlus.setEnabled(true);
                            imageViewBtnMinus.setEnabled(true);
                        }
                    }

                    InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    return true;
                }
                return false;
            }
        });

        mugshotPicker = view.findViewById(R.id.newuser_mugshots);
        mugshotPicker.setVisibility(View.GONE);

        LinearLayout ll = (LinearLayout) mugshotPicker.findViewById(R.id.newuser_avatars);

        for (int i = FIRST_AVAILABLE_PHOTO_INDEX; i < (FIRST_AVAILABLE_PHOTO_INDEX + NUMBER_OF_PHOTOS); i++) {
            View mugshotView = inflater.inflate(R.layout.composite_newuser_mugshot, null);
            ImageView photo = (ImageView) mugshotView.findViewById(R.id.newuser_mugshot_photo);
            String photoNumber;
            if (i < 10) {
                photoNumber = "0" + i;
            } else {
                photoNumber = Integer.toString(i);
            }
            String avatarPictureName = MUGSHOT_FILE_NAME + photoNumber;
            photo.setImageResource((getResources().getIdentifier(avatarPictureName, "drawable", getActivity().getPackageName())));
            photo.setTag(avatarPictureName);
            photo.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    avatarName = (String) v.getTag();
                    imageViewMugshot.setBackgroundResource((getResources().getIdentifier(avatarName, "drawable", getActivity().getPackageName())));
                    mugshotPicker.setVisibility(View.GONE);
                    if (!covernameHelVisible) {
                        txvHelpCovername.setVisibility(View.VISIBLE);
                        MediaPlayerHelper.playSound("newuser/mfx/profil_jmeno.mp3", true, getActivity());
                        coverNameET.setEnabled(true);
                        covernameHelVisible = true;
                    }

                }
            });

            ll.addView(mugshotView, new LayoutParams(getResources().getDimensionPixelSize(R.dimen.newuser_mugshot_picker_photo_width), getResources().getDimensionPixelSize(R.dimen.newuser_mugshot_height)));
        }

        view.addOnLayoutChangeListener(new OnLayoutChangeListener() {

            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (!helpViewInstantiated) {
                    prepareHelpLayer(v);
                }
            }
        });

        prepareSexChooser(view);

        MediaPlayerHelper.playSound("newuser/mfx/profil_foto.mp3", true, getActivity());

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        newUserSoundControll = ((Tablexia) getActivity().getApplicationContext()).getGlobalSoundControl();
        newUserSoundControll.addSound(SfxSounds.NEWUSER_STAMP);
        newUserSoundControll.addSound(SfxSounds.NEWUSER_SIGNATURE);
    }

    @Override
    public void onPause() {
        super.onPause();
        newUserSoundControll.stopSound(SfxSounds.NEWUSER_STAMP, true, false);
        newUserSoundControll.stopSound(SfxSounds.NEWUSER_SIGNATURE, true, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        display = getActivity().getWindowManager().getDefaultDisplay();
        displaySize = new Point();
        display.getSize(displaySize);

        if (imageViewStamper != null) {
            float stamperInitialPositionX = 0.02f * displaySize.x;
            float stamperInitialPositionY = 0.65f * displaySize.y;
            imageViewStamper.setTranslationX(stamperInitialPositionX);
            imageViewStamper.setTranslationY(stamperInitialPositionY);
            stamperOriginalPosition = new Point((int) (stamperInitialPositionX), (int) (stamperInitialPositionY));
        }

        if (imageViewPen != null) {
            float penInitialPositionX = 0.85f * displaySize.x;
            float penInitialPositionY = 0.5f * displaySize.y;
            imageViewPen.setTranslationX(penInitialPositionX);
            imageViewPen.setTranslationY(penInitialPositionY);

            penOriginalPosition = new Point((int) (penInitialPositionX), (int) (penInitialPositionY));
        }

        if (btnSubscribe != null) {
            btnSubscribe.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    MediaPlayerHelper.playSound("newuser/mfx/profil_podpis_prst.mp3", true, getActivity());
                    signatureDialog.show(getActivity().getFragmentManager(), null);
                    subscriptionDialogShown = true;
                }
            });
        }

        imageViewMugshot.setOnClickListener(this);
        imageViewSexFemale.setOnClickListener(this);
        imageViewSexMale.setOnClickListener(this);

        // OBSLUHA POHYBU RAZITKA
        OnDragListener stampDragListener = new OnDragListener(displaySize) {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                super.onTouch(v, event);
                handler.removeCallbacks(runnable);

                if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    if (imageViewStamper != null) {
                        int currentPos[] = new int[2];
                        imageViewStamper.getLocationOnScreen(currentPos);
                        displaySize = new Point();
                        display.getSize(displaySize);
                        if (currentPos[0] < (displaySize.x / 2)) {
                            imageViewStamper.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.newuser_stamp_left_grabbed));
                        } else if (currentPos[0] == (displaySize.x / 2)) {
                            imageViewStamper.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.newuser_stamp_middle_grabbed));
                        } else {
                            imageViewStamper.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.newuser_stamp_right_grabbed));
                        }
                    }

                }
                return true;
            }
        };
        stampDragListener.setOnDropListener(new OnDropListener() {

            @Override
            public boolean onDrop(View v, MotionEvent event, Point start) {
                if (imageViewStamper != null) {
                    int[] stampLoc = new int[2];
                    imageViewStamper.getLocationOnScreen(stampLoc);
                    int[] stampPlaceLoc = new int[2];
                    imageViewStamPlaceholder.getLocationOnScreen(stampPlaceLoc);

                    if (stampLoc[0] < (displaySize.x / 2)) {
                        imageViewStamper.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.newuser_stamp_left_dropped));
                    } else {
                        imageViewStamper.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.newuser_stamp_right_dropped));
                    }
                    coverName = coverNameET.getText().toString();

                    Rect stampPlaceRect = new Rect(stampPlaceLoc[0], stampPlaceLoc[1], stampPlaceLoc[0] + imageViewStamPlaceholder.getWidth(), stampPlaceLoc[1] + imageViewStamPlaceholder.getHeight());
                    Rect stampRect = new Rect(stampLoc[0], stampLoc[1], stampLoc[0] + imageViewStamper.getWidth(), stampLoc[1] + imageViewStamper.getHeight());
                    if (Rect.intersects(stampPlaceRect, stampRect)) {
                        if (!StringUtils.isEmpty(subscriptionFileName) && !StringUtils.isEmpty(avatarName) && !StringUtils.isEmpty(coverName) && !invalidCoverName) {
                            imageViewStamPlaceholder.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.newuser_stamp));
                            newUserSoundControll.playSound(SfxSounds.NEWUSER_STAMP, false);
                            animateViewInOriginalPosition(imageViewStamper, true);
                        } else {
                            Log.d("FORM", "Nevyplneny formular");
                            Toast toast = Toast.makeText(getActivity(), getResources().getText(R.string.newuser_fill_everything), Toast.LENGTH_SHORT);
                            toast.show();
                            animateViewInOriginalPosition(imageViewStamper, false);
                        }
                    } else {
                        animateViewInOriginalPosition(imageViewStamper, false);
                    }
                    return true;
                } else {
                    return false;
                }
            }
        });
        if (imageViewStamper != null) {
            imageViewStamper.setOnTouchListener(stampDragListener);
        }
        // OBSLUHA POHYBU PERA
        if (imageViewPen != null) {
            OnDragListener penDragListener = new OnDragListener(displaySize) {
                int[] penLoc              = new int[2];
                int[] subscriptionLineLoc = new int[2];

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    super.onTouch(v, event);
                    handler.removeCallbacks(runnable);
                    if (timer != null) {
                        timer.cancel();
                    }
                    if (event.getAction() == MotionEvent.ACTION_MOVE) {
                        if (!subscriptionDialogShown) {
                            imageViewPen.getLocationOnScreen(penLoc);
                            imageViewSubscriptionLine.getLocationOnScreen(subscriptionLineLoc);
                            Rect subscriptionLineRect = new Rect(subscriptionLineLoc[0], subscriptionLineLoc[1], (subscriptionLineLoc[0] + imageViewSubscriptionLine.getWidth()) - 30, (subscriptionLineLoc[1] + imageViewSubscriptionLine.getHeight()) - 30);
                            Rect penRect = new Rect(penLoc[0], penLoc[1], penLoc[0] + imageViewPen.getWidth(), penLoc[1] + imageViewPen.getHeight());
                            if (Rect.intersects(subscriptionLineRect, penRect)) {
                                signatureDialog.show(getActivity().getFragmentManager(), null);
                                MediaPlayerHelper.playSound("newuser/mfx/profil_podpis_prst.mp3", true, getActivity());
                                subscriptionDialogShown = true;
                            }
                        }

                    }

                    return true;
                }

            };
            imageViewPen.setOnTouchListener(penDragListener);
            penDragListener.setOnDropListener(new OnDropListener() {

                @Override
                public boolean onDrop(View v, MotionEvent event, Point start) {
                    subscriptionDialogShown = false;
                    animateViewInOriginalPosition(imageViewPen, false);
                    return true;
                }
            });
        }
        // TODO pridat obsluhu pro zobrazeni podpisoveho dialogu pro button
        imageViewBtnMinus.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if ((event.getAction() == MotionEvent.ACTION_DOWN)) {
                    subberRunnable.run();
                }
                if ((event.getAction() == MotionEvent.ACTION_UP) || (event.getAction() == MotionEvent.ACTION_CANCEL)) {
                    handler.removeCallbacks(subberRunnable);
                }
                return false;
            }
        });

        imageViewBtnPlus.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if ((event.getAction() == MotionEvent.ACTION_DOWN)) {
                    adderRunnable.run();
                }
                if ((event.getAction() == MotionEvent.ACTION_UP) || (event.getAction() == MotionEvent.ACTION_CANCEL)) {
                    handler.removeCallbacks(adderRunnable);
                }
                return false;
            }
        });

        if (imageViewStamPlaceholder != null) {
            imageViewStamPlaceholder.setOnClickListener(this);
        }
    }

    private Runnable adderRunnable  = new Runnable() {

        @Override
        public void run() {
            txvHelpAge.setVisibility(View.GONE);
            if (!sexHelVisible) {
                txvHelpSex.setVisibility(View.VISIBLE);
                MediaPlayerHelper.playSound("newuser/mfx/profil_klukholka.mp3", true, getActivity());
                sexHelVisible = true;
                imageViewSexMale.setEnabled(true);
                imageViewSexFemale.setEnabled(true);
                imageViewSexSeekBar.setEnabled(true);
            }

            int actualAge = Integer.parseInt((String) textViewAge.getText());
            if (actualAge <= 98) {
                textViewAge.setText(Integer.toString(++actualAge));
                age = actualAge;
            }
            handler.postDelayed(adderRunnable, 250);
        }
    };

    private Runnable subberRunnable = new Runnable() {

        @Override
        public void run() {
            txvHelpAge.setVisibility(View.GONE);
            if (!sexHelVisible) {
                txvHelpSex.setVisibility(View.VISIBLE);
                MediaPlayerHelper.playSound("newuser/mfx/profil_klukholka.mp3", true, getActivity());
                imageViewSexMale.setEnabled(true);
                imageViewSexFemale.setEnabled(true);
                imageViewSexSeekBar.setEnabled(true);
                sexHelVisible = true;
            }

            int actualAge = Integer.parseInt((String) textViewAge.getText());
            if (actualAge > 1) {
                textViewAge.setText(Integer.toString(--actualAge));
                age = actualAge;
            }
            handler.postDelayed(subberRunnable, 250);
        }
    };

    private Handler  handler        = new Handler();

    @Override
    public void onClick(View v) {
        Log.i(TAG, "onClick");
        if (v.equals(imageViewMugshot)) {
            mugshotPicker.setVisibility(View.VISIBLE);
            txvHelpAvatar.setVisibility(View.GONE);
        } else if (v.equals(imageViewSexFemale)) {
            onSexChanged();
            sex = Sex.FEMALE;
            refreshSexSeekBar();
        } else if (v.equals(imageViewSexMale)) {
            onSexChanged();
            sex = Sex.MALE;
            refreshSexSeekBar();
        }
    }

    @Override
    public boolean onDrop(View v, MotionEvent event, Point start) {
        Log.i(TAG, "onDrop");
        return false;
    }

    /*
     * Inicializace defaultnich parametru obrazovky
     */
    private void initScreen() {
        sex = Sex.FEMALE;
    }

    public DialogFragment signatureDialog = new TablexiaButtonViewDialog() {

        @Override
        protected int getPositiveButtonText() {
            return R.string.subscriptiondialog_sign;
        }

        @Override
        protected int getNegativeButtonText() {
            return R.string.subscriptiondialog_cancel;
        }

        SignatureView signatureView;

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            signatureView = (SignatureView) view.findViewById(R.id.newuser_signature);
            super.onViewCreated(view, savedInstanceState);
        }

        @Override
        protected View getQuestionButtonsView(LayoutInflater inflater) {
            return null;
        };

        @Override
        protected int getLayourId() {
            return R.layout.newuser_signaturedialog;
        };

        @Override
        protected int getBackgroundResource() {
            return R.drawable.newuser_signature_background;
        }

        @Override
        public void onResume() {
            super.onResume();
            DisplayMetrics dp = new DisplayMetrics();
            getDialog().getWindow().getWindowManager().getDefaultDisplay().getMetrics(dp);
            getDialog().getWindow().setLayout(dp.widthPixels, dp.heightPixels);
        };

        @Override
        public void positiveAction() {
            if (signatureView.isAtLeastDot()) {
                int userNumber = User.getAllUsers().size();
                subscriptionFileName = "podpis" + userNumber + ".png";
                signatureView.savePaintedBitmapIntoFile(subscriptionFileName);
                String subscrFileNameWithPath = FileHelper.getImageFileDir(getActivity()) + File.separator + subscriptionFileName;
                Bitmap imageBitmap = BitmapFactory.decodeFile(subscrFileNameWithPath);
                if (imageViewSignaturePicture != null) {
                    imageViewSignaturePicture.setImageBitmap(imageBitmap);
                    imageViewSignaturePicture.setScaleX(1.5f);
                    imageViewSignaturePicture.setScaleY(1.5f);
                }
                if (imageViewPen != null) {
                    animateViewInOriginalPosition(imageViewPen, true);
                }
                newUserSoundControll.playSound(SfxSounds.NEWUSER_SIGNATURE, false);
                dismiss();
                if (btnSubscribe != null) {
                    boolean isMale = sex == Sex.MALE;
                    coverName = coverNameET.getText().toString();
                    User newUser = new User(coverName, age, isMale, avatarName, true, false, subscriptionFileName);
                    newUser.save();

                    Intent data = new Intent();
                    data.putExtra("userId", newUser.getId());
                    getActivity().setResult(Activity.RESULT_OK, data);

                    ((NewUserActivity) getActivity()).hideScreen(new AnimatorListenerAdapter() {

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            ((NewUserActivity) NewUserFormFragment.this.getActivity()).finish();
                        }

                    });
                }
            } else {
                new TablexiaButtonViewDialog() {
                    @Override
                    protected int getTextResource() {
                        return R.string.newuser_emptysubscr;
                    }

                    @Override
                    protected int getQuestionButtonLayoutId() {
                        return R.layout.tablexiadialog_questionbutton_single;
                    };

                }.show(getFragmentManager(), null);
            }

        };

        @Override
        public void negativeAction() {
            if (imageViewPen != null) {
                animateViewInOriginalPosition(imageViewPen, false);
            }
            dismiss();
        }

    };

    private void animateViewInOriginalPosition(final ImageView view, final boolean proceedConditionMet) {
        final Point viewOriginalPosition;
        if (view.equals(imageViewPen)) {
            viewOriginalPosition = penOriginalPosition;
        } else {
            viewOriginalPosition = stamperOriginalPosition;
        }

        final int[] viewLocation = new int[2];
        view.getLocationOnScreen(viewLocation);

        TranslateAnimation animation = new TranslateAnimation(0, viewOriginalPosition.x - viewLocation[0], 0, viewOriginalPosition.y - viewLocation[1]);
        animation.setDuration(500);
        animation.setFillAfter(true);
        animation.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                Log.d(TAG, "Animating view to original position");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (proceedConditionMet) {
                    if (view.equals(imageViewPen)) {
                        penDropPositiveAction();
                    } else {
                        stamperDropPositiveAction();
                    }
                }
                view.clearAnimation();
                view.setTranslationX((view.getTranslationX() + viewOriginalPosition.x) - viewLocation[0]);
                view.setTranslationY((view.getTranslationY() + viewOriginalPosition.y) - viewLocation[1]);

            }
        });
        view.startAnimation(animation);

    }

    private void penDropPositiveAction() {
        txvHelpSubscr.setVisibility(View.GONE);
        if (!stampHelVisible) {
            final Animation shake = AnimationUtils.loadAnimation(getActivity(), R.animator.shake);
            shake.setAnimationListener(new AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {
                    Log.d(TAG, "Started animating imageViewStamper");
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    handler.postDelayed(runnable = new Runnable() {

                        @Override
                        public void run() {
                            if (imageViewStamper != null) {
                                imageViewStamper.startAnimation(shake);
                            }
                        }
                    }, 1500);
                }
            });
            if (imageViewStamper != null) {
                imageViewStamper.startAnimation(shake);
                txvHelpStamp.setVisibility(View.VISIBLE);
                MediaPlayerHelper.playSound("newuser/mfx/profil_razitko.mp3", true, getActivity());
                stampHelVisible = true;
                imageViewStamper.setEnabled(true);
            }
        }
    }

    private void stamperDropPositiveAction() {
        boolean isMale = sex == Sex.MALE;
        User newUser = new User(coverName, age, isMale, avatarName, true, false, subscriptionFileName);
        newUser.save();

        MapBuilder mapBuilder = MapBuilder.createEvent("user_create", "new_user", newUser.getNickName(), 0l);
        mapBuilder.set("avatar", newUser.getAvatar());
        mapBuilder.set("age", String.valueOf(newUser.getAge()));
        mapBuilder.set("sex", newUser.isMale() ? "male" : "female");
        mapBuilder.set("created_user", newUser.getNickName());

        EasyTracker.getInstance(getActivity()).send(mapBuilder.build());

        Intent data = new Intent();
        data.putExtra("userId", newUser.getId());
        getActivity().setResult(Activity.RESULT_OK, data);

        ((NewUserActivity) getActivity()).hideScreen(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(Animator animation) {
                ((NewUserActivity) NewUserFormFragment.this.getActivity()).finish();
            }

        });
    }

    /*
     * Metoda napozicuje bubliny s napovedou a rovnez zakaze komponenty, ktere by nemely byt editovane drive nez ostatni
     */
    private void prepareHelpLayer(View v) {
        txvHelpCovername = (RatioPositionTextView) v.findViewById(R.id.newuser_help_covername);
        txvHelpAvatar = (RatioPositionTextView) v.findViewById(R.id.newuser_help_avatar);
        txvHelpAge = (RatioPositionTextView) v.findViewById(R.id.newuser_help_age);
        txvHelpSex = (RatioPositionTextView) v.findViewById(R.id.newuser_help_sex);
        txvHelpSubscr = (RatioPositionTextView) v.findViewById(R.id.newuser_help_subscription);
        if ((imageViewStamper != null) && (imageViewPen != null)) {
            txvHelpStamp = (RatioPositionTextView) v.findViewById(R.id.newuser_help_stamp);
            txvHelpSex.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    txvHelpSex.setVisibility(View.GONE);
                    if (!subscrHelVisible) {
                        txvHelpSubscr.setVisibility(View.VISIBLE);
                        MediaPlayerHelper.playSound("newuser/mfx/profil_podpis_pero.mp3", true, getActivity());
                        imageViewPen.setEnabled(true);
                        subscrHelVisible = true;
                    }
                    shakePen();

                }
            });
            imageViewPen.setEnabled(false);
            stampHelVisible = false;

            imageViewStamper.setEnabled(false);

            txvHelpStamp.setVisibility(View.GONE);
        } else {
            btnSubscribe.setEnabled(false);
            txvHelpSex.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    txvHelpSex.setVisibility(View.GONE);
                    if (!subscrHelVisible) {
                        txvHelpSubscr.setVisibility(View.VISIBLE);
                        btnSubscribe.setEnabled(true);
                        subscrHelVisible = true;
                    }

                }
            });
        }
        txvHelpAge.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                txvHelpAge.setVisibility(View.GONE);
                if (!sexHelVisible) {
                    txvHelpSex.setVisibility(View.VISIBLE);
                    MediaPlayerHelper.playSound("newuser/mfx/profil_klukholka.mp3", true, getActivity());
                    imageViewSexSeekBar.setEnabled(true);
                    sexHelVisible = true;
                }

            }
        });
        covernameHelVisible = false;
        coverNameET.setEnabled(false);
        ageHelpVisible = false;
        imageViewBtnPlus.setEnabled(false);
        imageViewBtnMinus.setEnabled(false);
        imageViewSexMale.setEnabled(false);
        imageViewSexFemale.setEnabled(false);
        imageViewSexSeekBar.setEnabled(false);
        subscrHelVisible = false;
        helpViewInstantiated = true;

    }

    private void shakePen() {
        int[] penInitialPos = new int[2];
        imageViewPen.getLocationOnScreen(penInitialPos);
        final int penInitialX = penInitialPos[0];

        int delay = 500; // delay for 5 sec.

        int period = 3000; // repeat every sec.

        timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {

                handler.post(runnable = new Runnable() {

                    @Override
                    public void run() {
                        imageViewPen.animate().translationXBy(10).setDuration(1000).setInterpolator(new CycleInterpolator(7)).setListener(new AnimatorListener() {

                            @Override
                            public void onAnimationStart(Animator animation) {
                                // nothing

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {
                                // nothing
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                imageViewPen.setTranslationX(penInitialX);

                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {
                                // nothing

                            }
                        }).start();

                    }
                });

            }

        }, delay, period);
    }

    /**
     * Prepares difficulty seekbar
     * 
     * @param layout
     */

    // TODO play seekbar sounds
    private void prepareSexChooser(View layout) {

        difficultySeekbar = (SeekBar) layout.findViewById(R.id.newuser_sex_seekbar);
        difficultySeekbar.incrementProgressBy(1);
        difficultySeekbar.setMax(100);

        difficultySeekbar.setProgress(50);
        difficultySeekbar.setThumb(getActivity().getResources().getDrawable(sex.getSeekbarThumImageId()));
        difficultySeekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Sex currentSex = computeCurrentSex(seekBar.getProgress());
                if (currentSex != null) {
                    int progress = calculateSexProgress(difficultySeekbar.getMax(), sex);
                    seekBar.setProgress(progress);
                    onSexChanged();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // nothing needed
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                changeSex(seekBar.getProgress());
            }

            private Sex computeCurrentSex(int progress) {
                if (progress <= (difficultySeekbar.getMax() / 2)) {
                    return Sex.FEMALE;
                }
                return Sex.MALE;
            }

            private void changeSex(int progress) {
                Sex currentSex = computeCurrentSex(progress);
                if (currentSex != null) {
                    // ((GameMenu) getParentFragment()).playDifficultySeekbarMoveSound(selectedDifficulty, currentDificulty);
                    sex = currentSex;
                    difficultySeekbar.setThumb(getActivity().getResources().getDrawable(sex.getSeekbarThumImageId()));
                }
            }

        });
    }

    private int calculateSexProgress(int maximum, Sex currentSex) {
        return (int) (maximum * ((double) sex.ordinal()));
    }

    private void refreshSexSeekBar() {
        int progress = calculateSexProgress(difficultySeekbar.getMax(), sex);
        difficultySeekbar.setProgress(progress);
    }

    private void onSexChanged() {
        txvHelpSex.setVisibility(View.GONE);
        if (!subscrHelVisible) {
            txvHelpSubscr.setVisibility(View.VISIBLE);

            if (imageViewPen != null) {
                imageViewPen.setEnabled(true);
                MediaPlayerHelper.playSound("newuser/mfx/profil_podpis_pero.mp3", true, getActivity());
            } else {
                btnSubscribe.setEnabled(true);
            }
            subscrHelVisible = true;
        }
        if ((imageViewPen != null) && !sexSet) {
            sexSet = true;
            shakePen();
        }
    }
}
