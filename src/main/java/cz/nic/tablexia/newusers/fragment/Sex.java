package cz.nic.tablexia.newusers.fragment;

import cz.nic.tablexia.R;

public enum Sex {

	FEMALE(R.drawable.switch_center), MALE(R.drawable.switch_center);

	private int seekbarThumImageId;

	private Sex(int seekbarThumImageId) {
		this.seekbarThumImageId = seekbarThumImageId;
	}

	/**
	 * Returns seekbar thumb image id for current sex
	 * 
	 * @return seekbar thumb image id for current sex
	 */
	public int getSeekbarThumImageId() {
		return seekbarThumImageId;
	}

}
