/*******************************************************************************
 *     Tablexia
 *
 *     Copyright (C) 2013  CZ NIC z.s.p.o. <podpora at nic dot cz>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package cz.nic.tablexia.newusers;

import roboguice.activity.RoboFragmentActivity;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import cz.nic.tablexia.R;
import cz.nic.tablexia.Tablexia;
import cz.nic.tablexia.newusers.fragment.NewUserFormFragment;
import cz.nic.tablexia.util.MintHelper;
import cz.nic.tablexia.util.Utility;
import cz.nic.tablexia.widget.TablexiaQuestionBannerDialog;

/**
 * @author lhoracek
 */
public class NewUserActivity extends RoboFragmentActivity {

    private static final String TAG = NewUserActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstance) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstance);
        // init bugsense
        MintHelper.startBugSense(this);
        setContentView(R.layout.newuser_activity);
        findViewById(R.id.newuser_hiderLayer).setVisibility(View.VISIBLE);
        if (savedInstance == null) {
            showForm();
        }
    }

    public Tablexia getTablexiaContext() {
        return (Tablexia) getApplicationContext();
    }

    @Override
    protected void onResume() {
        // getTablexiaContext().setCurreActivity(null);
        super.onResume();
        showScreen();
    }

    @Override
    protected void onPause() {
        super.onPause();
        // disable activity animation
        overridePendingTransition(0, 0);
    }

    public void showForm() {
        showFragment(new NewUserFormFragment());
    }

    private void showFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.newuser_parent, fragment);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        // osklive osetreni schovavani avatar pickeru
        View picker = findViewById(R.id.newuser_mugshots);
        if ((picker != null) && (picker.getVisibility() == View.VISIBLE)) {
            picker.setVisibility(View.GONE);
        } else {
            new TablexiaQuestionBannerDialog() {
                @Override
                protected int getTextResource() {
                    return R.string.newuser_canceldialog;
                }

                @Override
                public void positiveAction() {
                    hideScreen(new AnimatorListenerAdapter() {

                        @Override
                        public void onAnimationEnd(android.animation.Animator animation) {
                            NewUserActivity.super.onBackPressed();
                        };

                    });
                    dismiss();
                }

                @Override
                protected int getBannerResource() {
                    return R.drawable.dialog_banner_pozor;
                }
            }.show(getFragmentManager(), null);

        }
    }

    public void hideScreen(AnimatorListener animatorListener) {
        Utility.showLayer(this, R.id.newuser_hiderLayer, Tablexia.SCREEN_FADE_ANIM_DURATION, animatorListener);
    }

    public void showScreen() {
        Utility.hideLayer(this, R.id.newuser_hiderLayer, Tablexia.SCREEN_FADE_ANIM_DURATION, null);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}
