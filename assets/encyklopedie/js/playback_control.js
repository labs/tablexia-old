function playSound(sound, image) {
	if (image.src.indexOf('prehrat-button.png') > -1) {
		resetIcon();
		window.JSInterface.playSound(sound);
		image.src = "img/pauza-button.png";
	} else {
		window.JSInterface.pauseSound();
		resetIcon();
	}
}

function resetIcon() {
	var cusid_ele = document.getElementsByClassName('playbutton');
	for (var i = 0; i < cusid_ele.length; i++) {
		var item = cusid_ele[i];
		if (item.src.indexOf('pauza-button.png') > -1) {
			item.src = "img/prehrat-button.png";
		}
	}
}